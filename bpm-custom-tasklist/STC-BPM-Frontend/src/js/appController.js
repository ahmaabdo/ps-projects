define(['ojs/ojcore', 'knockout', 'util/commonhelper', 'config/services', 'knockout-mapping', 'notify', 'ojs/ojmodule-element-utils', 'ojs/ojarraydataprovider', 'ojs/ojmodule-element', 'ojs/ojrouter',
    'ojs/ojknockout', 'ojs/ojarraytabledatasource', 'ojs/ojknockout', 'ojs/ojavatar', 'ojs/ojoffcanvas', 'ojs/ojknockout-validation', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojmenu', 'ojs/ojoption', 'ojs/ojdialog', 'ojs/ojlabel', , 'ojs/ojselectcombobox', 'ojs/ojpopup', 'ojs/ojdialog', 'ojs/ojmessages', 'ojs/ojmessage'],
        function (oj, ko, commonhelper, services, km, n, moduleUtils, ArrayDataProvider) {
            function ControllerViewModel() {
                const self = this;
                const getTranslation = oj.Translations.getTranslatedString;
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
                var pageBody = $('*');
                self.test2 = ko.observable();
                self.tracker = ko.observable();
                self.viewProfileLbl = ko.observable();
                self.disOnLogin = ko.observable(false);
                self.hasErrorOccured = ko.observable(false);
                self.isNotificationCount = ko.observable(false);
                self.isUserLoggedIn = ko.observable(true);
                self.confirmMessage = ko.observable();
                self.oprationMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.showDetails = ko.observable();
                self.signoLbl = ko.observable();
                self.changeLbl = ko.observable();
                self.passwordLabel = ko.observable();
                self.userName = ko.observable('');
                self.password = ko.observable('');
                self.loading = ko.observable(false);
                self.loginLabel = ko.observable();
                self.loginFailureText = ko.observable();
                self.navDataSourceLeft = ko.observable();
                self.navDataSource = ko.observable();
                self.navLeftArray = ko.observableArray();
                self.navTopArray = ko.observableArray();
                self.notificationsCount = ko.observable();
                self.lblSignOut = ko.observableArray();
                self.Usernametext = ko.observableArray();
                self.SignIn = ko.observable();
                self.forgetPassword = ko.observable();
                self.Searchlbl = ko.observable();
                self.userKeywordLable = ko.observable();
                self.recieveType = ko.observable();
                self.avatarSize = ko.observable("xxs");
                self.firstName = ko.observable();
                self.lastName = ko.observable();
                self.initials = ko.observable();
                var timer;
                self.selectedTableRowKeyNotifiation = ko.observable();
                self.reviewNotiType = ko.observable();
                self.ok = ko.observable();
                self.managerType = ko.observable();
                self.projectedStartDate = ko.observable();
                self.assignmentProjectedEndDate = ko.observable();
                self.peopleGroup = ko.observable();
                self.assignmentStatusTypeId = ko.observable();
                self.fax = ko.observable();
                self.faxExt = ko.observable();
                self.allRoles = ko.observableArray();
                self.organizationName = ko.observable();
                self.departmentNamenavDataSource = ko.observable();
                self.PaaSLookup = ko.observableArray([]);
                self.ApprovalCondition = ko.observableArray([]);
                self.EITIcons = ko.observableArray([]);
                self.PersonNumberArr = ko.observableArray([]);
                self.roleOptionType = ko.observableArray([]);
                self.rolesOption = ko.observableArray([]);
                self.validation = {};
                self.N_navDataSource = ko.observable();
                self.HideLogin = ko.observable(true);

                self.xx = ko.observable("N");
                self.allPaaSDefultValuearry = ko.observableArray([]);
                self.allEITArray = ko.observableArray([]);
                self.refreshViewForLanguage = ko.observable(false);
                self.personDetails = ko.observable('');
                self.empName = ko.observable();
                self.globalEitNameReportLength = ko.observable();
                self.jwt = ko.observable('');
                self.hostUrl = ko.observable('');
                self.backBtnVis = ko.observable(false);
                self.isScreenSMorMD = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_DOWN));
                // Media queries for repsonsive layouts
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
                self.screenRange = oj.ResponsiveKnockoutUtils.createScreenRangeObservable();
                self.windowSize = ko.observable();
                self.BusinessMissionRequestLbl = ko.observable();
                self.overtimeRequestLbl = ko.observable();
                self.languageSwitch_lng = ko.observable();
                self.globalEffictiveDate = ko.observable();
                self.globalspecialistEMP = ko.observable('');
                self.viewBulkLbl = ko.observable();//hussein
                self.NavIConsTopArr = ko.observableArray();
                self.specialist = ko.observableArray();
                self.SpecialistLbl = ko.observableArray();
                self.employeeTimeAttandancelbl = ko.observableArray();
                self.loginDateFromSass = ko.observable();
                var deviceIp;
                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var deviceAndBrowserDetails = navigator.userAgent;
                var LogindateTime;
                self.recruitmentRequestArrayData = ko.observable();
                self.reassignRequestGlobal = ko.observableArray([]);
                self.globalFuseModel = ko.observableArray([{
                        "dataType": '',
                        "tableName": '',
                        "colName": '',
                        "rowName": '',
                        "udtValue": '',
                        "lookupCode": '',
                        "lookupValue": '',
                        "lookupType": ''

                    }]);
                self.globalHRCYesNo = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalJobCatagory = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalJobName = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalGeneralGroup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalGroupType = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalJobCodes = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": '',
                        "description": ""
                    }]);
                self.globalHrGrades = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalPositionsName = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalEmployeeEITLookup = ko.observableArray([]);
                self.globalPositionsAction = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalQualifications = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalPDFReport = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalEFFUDT = ko.observableArray([{
                        "displaySequence": '',
                        "rowLowRangeOrName": '',
                        "rowName": '',
                        "userColumnName": "",
                        "userTableName": ""
                    }]);

                self.globalJobsNames = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalEitNameReport = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalEITReportLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": '',
                        "description": ''
                    }]);
                self.globalSpecialistLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": '',
                        "description": ''
                    }]);
                self.globalManagerLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": '',
                        "description": ''
                    }]);
                self.globalEITDefultValueParamaterLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalAbsenceType = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalNationalityLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalMartialStatusLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalNationalIdentifierTypeLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalProjectTaskLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalSalaryTypeLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalEmploymentStatusLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.globalHiringSourceLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalQualifyingUnitsLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalContractTypeLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalPayrollListLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);
                self.globalAllowneceTypeLookup = ko.observableArray([{
                        "value": '',
                        "label": '',
                        "labelAr": ''
                    }]);

                self.userRoleArr = ko.observableArray([]);
                self.bankNameArr = ko.observableArray([]);

                self.tempAuthkey = ko.observable('Basic QXNocmFmLmFidS1zYWFkOldlbGNvbWVAMTIz');

                $('.body-page, #drawerToggleButtonor, #backButton, .header_logo_min').on('click', function () {
                    $('.MYdropdown_menu').hide();
                });

                self._showComponentValidationErrors = function (trackerObj) {
                    if (trackerObj !== undefined) {
                        trackerObj.showMessages();
                        if (trackerObj.focusOnFirstInvalid())
                            return false;
                    }
                    return true;
                };

                self.displaySreenRange = ko.computed(function () {
                    var range = self.screenRange();
                    if (range === oj.ResponsiveUtils.SCREEN_RANGE.SM) {
                        self.windowSize('SM');
                    } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.MD) {
                        self.windowSize('MD');
                    } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.LG) {
                        self.windowSize('LG');
                    } else if (range === oj.ResponsiveUtils.SCREEN_RANGE.XL) {
                        self.windowSize('XL');
                    }
                });

                self.getAllAbsenceTypes = function () {
                    var getReportCBCF = function (data) {
                        self.globalAbsenceType([]);
                        for (var index = 0; index < data.length; index++) {
                            self.globalAbsenceType.push({
                                "value": data[index].value, "label": data[index].label, "labelAr": data[index].label
                            });
                        }

                    };
                    var reportPaylod = {"reportName": "AbsenceTypeReport"};
                    services.getGenericReport(reportPaylod).then(getReportCBCF, self.failCbFn);
                };

                self.viewProfile = function () {
                    self.router.go('profile');
                };
                self.viewHome = function () {
                    self.router.go('workListScreen');
                };

//                self.getBusinessUnits = function () {
//
//                    var searhCbFn = function (data) {
//                        self.arr = ko.observableArray([]);
//                        self.arr(data);
//                        self.globalEitNameReport([]);
//                        for (var i = 0; i < self.arr().length; i++) {
//                            self.globalEitNameReport.push({"value": self.arr()[i].DESCRIPTIVE_FLEX_CONTEXT_CODE, "label": self.arr()[i].DESCRIPTIVE_FLEX_CONTEXT_NAME});
//                        }
//
//                    };
//                    var searhfailCbFn = function () {};
//                    if (self.getLocale() == 'ar') {
//                        self.lang = "AR";
//                    } else {
//                        self.lang = "US";
//                    }
//                    services.getEitNameReportReport(self.lang).then(searhCbFn, searhfailCbFn);
//                };


                self.EmployeeAuthentication = ko.observable("");
                self.adminAuthentication = ko.observable("");
                self.lineManager = ko.observable("");
                self.checkUserLoginSpecialist = ko.observable("");
                self.checkUserLoginEmployee = ko.observable("");

                self.getLocale = function () {
                    return oj.Config.getLocale();

                };
                self.userAuthentication = function () {
                    var searhCbFn = function (data) {
                        self.userRoleArr(data);
                        if (data.length > 1) {

                            for (var i = 0; i < data.length; i++) {

                                if (data[i].ROLE_NAME === "HHA PAAS Employee Custom") {

                                    self.EmployeeAuthentication(data[i].ROLE_NAME);


                                } else if (data[i].ROLE_NAME === "Application Implementation Administrator") {

                                    self.adminAuthentication(data[i].ROLE_NAME);

                                } else if (data[i].ROLE_NAME === "HHA-Line Manager Custom V3") {

                                    self.lineManager(data[i].ROLE_NAME);
                                }
                            }
                            ;
                        } else {
                            var roleName = data.ROLE_NAME;
                            if (roleName === "HHA PAAS Employee Custom") {
                                self.EmployeeAuthentication(roleName);

                            } else if (roleName === "Application Implementation Administrator") {
                                self.adminAuthentication(roleName);

                            } else if (roleName === "HHA-Line Manager Custom V3") {
                                self.lineManager(roleName);
                            }


                        }
                    };

                    var searhfailCbFn = function () {


                    };

                    //services.getGenericAuthentication(self.userName()).then(searhCbFn, searhfailCbFn); 
                };


                self.beforeSelectItem = function (event, ui) {
                    self.closeHeaderNav();
                    self.loading(true);

                };

                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.dispose();
                self.arr2 = ko.observableArray();

                self.router.configure({
                    'home': {label: 'HHA-SSHR', value: "home/home", title: "Home"},
                    'setup': {label: 'Setup', value: "setup/home/home", title: "Setup"},
                    'profile': {label: 'Profile', title: "Profile"},
                    'dashboard': {label: 'dashboard', value: "dashboard", title: "Dashboard"},

                    'about': {label: 'About', title: "About"},

                    'form': {label: 'form', value: "form/form", title: "Form"},
                    'UnAuthorizedScreen': {label: 'UnAuthorized Screen', value: "stcWorklistScreen/UnAuthorizedScreen", title: "UnAuthorized"},
                    'workListScreen': {label: 'BPM Worklist', value: "stcWorklistScreen/workListScreen", title: "BPM Worklist"},
                    'dynamicTaskDetails': {label: 'BPM Worklist', value: "stcWorklistScreen/dynamicTaskDetails", title: "BPM Worklist"},
                    'SetupScreen': {label: 'Setup Screen', value: "SetupScreen", title: "Setup Screen"},
                    'taskLogTracker': {label: 'get All Task Details Screen', value: "tasksLogScreen/taskLogTracker", title: "get All Task Details Screen", isDefault:true},
                    'SetupSummeryScreen': {label: 'Setup Summery Screen', value: "SetupSummaryScreen", title: "Setup Summery Screen"},
                    'dynamicTaskDetailsNewReq': {label: 'Setup Summery Screen', value: "stcWorklistScreen/dynamicTaskDetailsNewReq", title: "New Screen"}


                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});
                ko.computed(function () {
                    self.loading() ? pageBody.css('cursor', 'wait') : pageBody.css('cursor', '');
                    if (self.router._stateId() == 'workListScreen')
                        self.loading(false);
                    if (self.router._stateId() != 'workListScreen'
                            && self.router._stateId() != 'setup'
                            && self.router._stateId() != 'bulkTransactionHome'
                            && self.router._stateId() != 'notificationScreen') {
                        $('#drawerToggleButtonor').hide();
                        $('#backButton').show();
                    } else {
                        $('#backButton').hide();
                        $('#drawerToggleButtonor').show();
                    }
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    var masterPromise = Promise.all([
                        moduleUtils.createView({'viewPath': viewPath}),
                        moduleUtils.createViewModel({'viewModelPath': modelPath})
                    ]);
                    masterPromise.then(
                            function (values) {
                                self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                            },
                            function (reason) {}
                    );

                    document.addEventListener("deviceready", onDeviceReady, false);
                    function onDeviceReady() {
                        document.addEventListener("backbutton", function (e) {
                            e.preventDefault();
                            if (self.router._stateId() == 'workListScreen') {
                                navigator.app.exitApp();
                            } else if (self.router._stateId() == 'setup'
                                    && self.router._stateId() == 'notificationScreen'
                                    || typeof self.router._stateId() == 'undefined') {
                                self.router.go('workListScreen');
                            } else {
                                history.go(-1);
                                return false;
                            }
                        }, false);
                    }
                });

                self.subModule = ko.computed(function () {
                    if (self.router.currentState()) {
                        //constructNavLeftData();
                        self.constructNavTopData();
                        self.navDataSource(new oj.ArrayTableDataSource(self.NavIConsTopArr(), {idAttribute: 'id'}));


                    }
                });



                function pushNav() {

                    var navDataLeft = [];

                    navDataLeft.push({name: getTranslation("pages.home"), id: 'workListScreen',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24 icon-size'},
                            {name: getTranslation("pages.job"), id: 'searchJob',
                                iconClass: 'oj-navigationlist-item-icon fa fa-briefcase icon-size'},
                            {name: getTranslation("pages.actualPosition"), id: 'xx',
                                iconClass: 'oj-navigationlist-item-icon fa fa-user icon-size'},
                            {name: getTranslation("pages.position"), id: 'searchPosition',
                                iconClass: 'oj-navigationlist-item-icon fa fa-user icon-size'}, );


                    self.navLeftArray(navDataLeft);
                    self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), {idAttribute: 'id'}));
                }

                self.selectedMenuItem = ko.observable("");

                self.menuItemAction = function (event) {
                    self.selectedMenuItem(event.target.value);
                };

                function constructNavLeftData() {
//                 
                    if (self.EmployeeAuthentication() === "HHA PAAS Employee Custom" && self.lineManager() === "HHA-Line Manager Custom V3" && self.adminAuthentication() === "Application Implementation Administrator") {

                        var navDataLeft = [
                            {name: getTranslation("pages.Employee"), id: 'home',
                                iconClass: 'oj-navigationlist-item-icon fa fa-male'},
                            {name: getTranslation("pages.LineManager"), id: 'personSearch',
                                iconClass: 'oj-navigationlist-item-icon fa fa-users'},
                            {name: getTranslation("pages.Administrator"), id: 'personSearch',
                                iconClass: 'oj-navigationlist-item-icon fa fa-users'}
                        ];
                        self.navLeftArray(navDataLeft);
                        self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), {idAttribute: 'id'}));
                        self.checkUserLoginSpecialist("Specialist");
                    }
                    if (self.EmployeeAuthentication() === "de" && self.lineManager() === "HHA-Line Manager Custom V3" && self.adminAuthentication() === "") {

                        var navDataLeft = [
                            {name: getTranslation("pages.Employee"), id: 'home',
                                iconClass: 'oj-navigationlist-item-icon fa fa-male'},
//                            {name: getTranslation("pages.LineManager"), id: 'personSearch',
//                                iconClass: 'oj-navigationlist-item-icon fa fa-male'}
                        ];
                        self.navLeftArray(navDataLeft);
                        self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), {idAttribute: 'id'}));
                        self.checkUserLoginSpecialist("Specialist");
                    }
                    if (self.EmployeeAuthentication() === "HHA PAAS Employee Custom" && self.lineManager() === "" && self.adminAuthentication() === "") {

                        var navDataLeft = [
                            {name: getTranslation("pages.Employee"), id: 'home',
                                iconClass: 'oj-navigationlist-item-icon fa fa-male'}
                        ];
                        self.navLeftArray(navDataLeft);
                        self.navDataSourceLeft(new oj.ArrayTableDataSource(self.navLeftArray(), {idAttribute: 'id'}));
                        self.checkUserLoginEmployee("HHA PAAS Employee Custom");
                    }

                }

                function navTopFillData() {
                    var navData = ko.observableArray([]);
                    //---------
                    //hussein
                    //navData.push({name: getTranslation("pages.bulkmenu"), id: 'menu', style: 'font-size: -webkit-xxx-large', iconClass: 'oj-navigationlist-item-icon fa fa-file'});
                    //---------

                    var userRoles;
                    if (JSON.parse(sessionStorage.getItem("userRole"))) {
                        userRoles = JSON.parse(sessionStorage.getItem("userRole"));

                    }
                    if (!userRoles) {
                        userRoles = '';
                    }
                    if (!userRoles.length) {
                        userRoles = [userRoles];
                    }
                    var indexOfRole;
                    navData.splice(0, 0, {name: getTranslation("pages.home"), id: 'workListScreen',
                        iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-home-icon-24'});
                    for (var i = 0; i < userRoles.length; i++) {

                        if (userRoles[i].ROLE_NAME == "Employee") {

                            navData.splice(1, 0, {name: getTranslation("pages.Notification"), id: 'notificationScreen',
                                iconClass: 'oj-navigationlist-item-icon fa fa-bell'});
                            self.isNotificationCount(true);
                        } else if (userRoles[i].ROLE_NAME == "Line Manager") {
                            navData.push({name: getTranslation("pages.setup"), id: 'setup', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-gear'});
                            navData.push({name: getTranslation("pages.reassignSetup"), id: 'baseScreen', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-certificate'});
                            navData.push({name: getTranslation("pages.projectManagerServiceRequest"), id: 'projectManagerServiceRequest', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-male'});
                        } else if (userRoles[i].ROLE_NAME == "HHA Manager Custom") {
                            navData.push({name: getTranslation("pages.reassignSetup"), id: 'baseScreen', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-certificate'});
                        } else if (userRoles[i].ROLE_NAME == "HHA Manager Custom") {
                            navData.push({name: getTranslation("pages.Specialist"), id: 'personSearch', iconClass: 'oj-navigationlist-item-icon fa fa-users'
                            });
                            self.checkUserLoginSpecialist("Specialist");
                        } else if (userRoles[i].ROLE_NAME == "Human Resource Specialist") {
                            navData.push({name: getTranslation("pages.Specialist"), id: 'personSearch', iconClass: 'oj-navigationlist-item-icon fa fa-users'
                            });
                            self.checkUserLoginSpecialist("Specialist");
                        } else if (userRoles[i].ROLE_NAME == 'HHA PAAS Government Relations Specialist Custom') {
                            navData.push({name: getTranslation("pages.bulk"), id: 'bulk', style: 'font-size: -webkit-xxx-large',
                                iconClass: 'oj-navigationlist-item-icon fa fa-file'});
                        }
                    }
                    return navData();
                }
                ;

                // Navigation setup
                var rootViewModel = ko.dataFor(document.getElementById('globalBody'));
                self.constructNavTopData = function () {
//                    var navData = ko.observableArray( navTopFillData());
                    var navData = navTopFillData();
                    self.NavIConsTopArr(navTopFillData());
                    self.navTopArray(navData);
                    self.navDataSource(new oj.ArrayTableDataSource(self.navTopArray(), {idAttribute: 'id'}));
                    // /*new navigation list*/ self.NavIConsTopArr();                                
                    self.N_navDataSource(new ArrayDataProvider(self.NavIConsTopArr(), {keyAttributes: 'id'}));

                    /*new navigation list*/

                }

                // Drawer
                // Close offcanvas on medium and larger screens
                self.drawerParams = {
                    displayMode: 'push',
                    selector: '#navDrawer',
                    content: '#pageContent'
                };
                self.mdScreen.subscribe(function () {
                    oj.OffcanvasUtils.close(self.drawerParams);
                });

                // Called by navigation drawer toggle button and after selection of nav drawer item
                self.toggleDrawer = function () {
                    return oj.OffcanvasUtils.toggle(self.drawerParams);
                };
                // Add a close listener so we can move focus back to the toggle button when the drawer closes
                $("#navDrawer").on("ojclose", function () {
                    $('#drawerToggleButton').focus();
                });
                // Header
                // Application Name used in Branding Area
                self.appName = ko.observable("SSHR");
                // User Info used in Global Navigation area
                self.userLogin = ko.observable("");
                // Footer
                function footerLink(name, id, linkTarget) {
                    this.name = name;
                    this.linkId = id;
                    this.linkTarget = linkTarget;
                }
                self.footerLinks = ko.observableArray([
                    new footerLink('About Appspro', 'about Appspro', 'http://www.appspro-me.com/about-us/'),
                    new footerLink('Contact Us', 'contactUs', 'http://www.appspro-me.com/contact/')

                ]);
                self.failCbFn = function (xhr, thrownError) {
//                   self.hidePreloader();

                };

                //Enter key will trigger login button
                //BUGGY CODE
//                var inputPassword = document.getElementById("password");
//                inputPassword.addEventListener("keyup", function (e) {
//                    e.preventDefault();
//                    if (event.keyCode === 13) {
//                        self.onLogin();
//                    }
//                });
//                var inputUserName = document.getElementById("userName");
//                inputUserName.addEventListener("keyup", function (e) {
//                    e.preventDefault();
//                    if (event.keyCode === 13) {
//                        self.onLogin();
//                    }
//                });

                self.onLogin = function () {
                    self.router.go('setup');
                };
                //-----------------time Out-------------------//  
//                var timer;
//                window.onclick = function () {
//                    window.clearTimeout(timer);
//                    timer = window.setTimeout(function () {
//                        self.signOut();
//                    }, 10 * 60 * 1000);
//                };
                self.resetTimer = function () {
                    clearTimeout(timer);
                    timer = setTimeout(_ => {
                        self.signOut();
                    }, 10 * 60 * 1000);
                };
                window.onclick = function () {
                    self.resetTimer();
                };
                //-----------------time Out-------------------//  


                self.refreshData = ko.computed(function () {
                    if (self.refreshViewForLanguage()) {
                        //    self.constructNavTopData();
                        self.navDataSource(new oj.ArrayTableDataSource(self.NavIConsTopArr(), {idAttribute: 'id'}));
                        //constructNavLeftData();
                        // initFooterLinks();
                    }
                });

                self.onReset = function () {
                    self.isUserLoggedIn(false);
                    self.userName("");
                    self.password("");
                    $("#userName").ojInputText("reset");
                    $("#password").ojInputPassword("reset");
                };
                self.rightDrawerParams = {
                    displayMode: 'push',
                    selector: '#navRightDrawer',
                    content: '#pageContent'
                };
                self.toggleRightDrawer = function () {
                    return oj.OffcanvasUtils.toggle(self.rightDrawerParams);
                };
                self.buttonClick = function () {
                    if (typeof self.showingFront == 'undefined') {
                        self.showingFront = true;
                    }
                    var elem = document.getElementById('animatableCard');
                    // Determine startAngle and endAngle
                    var startAngle = self.showingFront ? '0deg' : '180deg';
                    var endAngle = self.showingFront ? '180deg' : '0deg';
                    // Animate the element
                    oj.AnimationUtils['flipOut'](elem, {'flipTarget': 'children',
                        'persist': 'all',
                        'startAngle': startAngle,
                        'endAngle': endAngle});
                    self.showingFront = !self.showingFront;
                };



                self.navCollMini = function () {
                    $('.DDMenuBody').attr({'style': 'display:block !important;'});
                    self.drawIconsInMenu();
                    self.drawIconsSpecialist();
                };

                self.mobNavDis = function () {
                    $('#mobileNavBar').addClass('app_cont_fst_sec ');
                    $('#mobileNavBar').removeClass('app_cont_fst_sec_col');
                };
                self.closeHeaderNav = function () {
                    $('#mobileNavBar').addClass('app_cont_fst_sec_col');
                    $('#mobileNavBar').removeClass('app_cont_fst_sec');
                };


                self.navCollMax = function () {

                    $('.DDMenuBody').attr({'style': 'display:none !important;'});


                };


                self.setLocale = function (lang) {

                    oj.Config.setLocale(lang,
                            function () {
                                self.refreshViewForLanguage(false);
                                if (lang === 'ar') {
                                    $("html").attr("lang", lang);
                                    $("html").attr("dir", "rtl");
                                    $('.panelstyle').addClass('arabicpanel');
                                    $('.textResponsiveDetails').addClass('textResponsiveDetailsar');
                                    $('#prefxsignoutor').addClass('prefxsignout_2');
                                    $('#prefxsignoutor').removeClass('prefxsignout');
                                    $('#drawerToggleButtonor').addClass('drawerToggleButton_2');
                                    $('#drawerToggleButtonor').removeClass('drawerToggleButton');
                                    $('.approval-card-body').removeClass('app-crd-bdy-border-en');
                                    $('.approval-card-body').addClass('app-crd-bdy-border-ar');
                                    $('.app-crd-type').removeClass('app-card-for-en');
                                    $('.app-crd-type').addClass('app-card-for-ar');
                                    $('.blockquote-footer').removeClass('app-card-for-en');
                                    $('.blockquote-footer').addClass('app-card-for-ar');


                                } else {
                                    $("html").attr("lang", lang);
                                    $("html").attr("dir", "ltr");

                                    $('.panelstyle').removeClass('arabicpanel');
                                    $('.textResponsiveDetails').addClass('textResponsiveDetailseng');
                                    $('#prefxsignoutor').addClass('prefxsignout');
                                    $('#prefxsignoutor').removeClass('prefxsignout_2');
                                    $('#drawerToggleButtonor').addClass('drawerToggleButton');
                                    $('#drawerToggleButtonor').removeClass('drawerToggleButton_2');
                                    $('.approval-card-body').removeClass('app-crd-bdy-border-ar');
                                    $('.approval-card-body').addClass('app-crd-bdy-border-en');
                                    $('.app-crd-type').removeClass('app-card-for-ar');
                                    $('.app-crd-type').addClass('app-card-for-en');
                                    $('.blockquote-footer').removeClass('app-card-for-ar');
                                    $('.blockquote-footer').addClass('app-card-for-en');


                                }
                                initTranslations();
                                //self.getBusinessUnits();

                                self.refreshViewForLanguage(true);
                            }
                    );
                };

                self.openDialog = function () {

                    document.querySelector("#detailsImage").open();
                };
                self.cancelButtonOk = function () {
                    document.querySelector("#detailsImage").close();
                };

                // getAllNotificationsDetails
//                self.notificationsCount = ko.observable();
                function getAllNotificationsDetails() {
                    self.getAllPaaSDefultValue();
                    self.noticationModel = {
                        "MANAGER_ID": self.personDetails().personId,
                        "managerOfManager": "x",
                        "position": self.personDetails().positionId,
                        "emp": self.personDetails().personId,
                        "userRoles": self.userRoles()
                    };
                    var jsonData = ko.toJSON(self.noticationModel);
                    var getNotificationCBF = function (data) {
                        //getBankDetails(services, self, self, commonhelper);
                        self.notificationsCount(data.length);
                    };
                    services.addGeneric(commonhelper.allNotifications, jsonData).then(getNotificationCBF, self.failCbFn);
                }

                self.userRoles = ko.observableArray([]);

                self.getUserRoles = function () {
                    self.getCountEITRequest();
                    var reportPaylod = {"reportName": "USER_ROLES", "personId": self.personDetails().personId};
                    var getRolesCBCF = function (data) {
                        for (i = 0; i < data.length; i++) {
                            self.userRoles.push({
                                roleId: data[i].ROLE_ID
                            });
                        }
                        getAllNotificationsDetails();
                    };
                    services.getGenericReportAsync(reportPaylod).then(getRolesCBCF, self.failCbFn);

//                    self.noticationModel = {
//                        "MANAGER_ID": self.personDetails().personId,
//                        "managerOfManager": "x",
//                        "position": self.personDetails().positionId,
//                        "emp": self.personDetails().personId
//                    };

                };

                //draw menu with eit 
                self.icons = ko.observableArray([]);
                self.EitName = ko.observableArray();
                //------------------------------draw self services----------------------------------//
                self.drawIconsInMenu = function () {
                    self.icons([]);
                    self.EitName(self.globalEitNameReport());
                    var getRoleValidationCbFn = function (data) {
                        var fristValue;
                        var operation;
                        var secondValue;
                        var roleNameArr = [];
                        var validEit = [];
                        var notValidEit = [];
                        var colorPar = 0;
                        for (var i = 0; i < data.length; i++) {
                            fristValue = self.personDetails()[data[i].personInfromation];
                            operation = data[i].operation;
                            secondValue = data[i].staticValue;

                            if (self.compare(fristValue, operation, secondValue)) {
                                validEit.push(data[i].eitCode);
                            } else {
                                notValidEit.push({eitcode: data[i].eitCode, operation: operation});
                            }
                        }
                        for (var i = 0; i < self.EitName().length; i++) {


                            if (self.globalEmployeeEITLookup().indexOf(self.EitName()[i].value) != -1
                                    && validEit.indexOf(self.EitName()[i].value) == -1) {

                                self.icons.push(
                                        {
                                            router: 'dynamicSummary',
                                            text: self.EitName()[i].label,
                                            iconType: self.getEITIcons(self.EitName()[i].value, "FontAwesome"),
                                            visible: true,
                                            label: 'c',
                                            value: 'demo-icon-circle-a',
                                            code: self.EitName()[i].value},
                                        );
                            }

                        }
                        if (notValidEit.length > 0) {

                            for (var j = 0; j < self.globalEitNameReport().length; j++) {

                                for (var x = 0; x < notValidEit.length; x++) {

                                    if (self.globalEitNameReport()[j].value == notValidEit[x].eitcode && notValidEit[x].operation == "!=") {

                                        self.icons.push(
                                                {
                                                    label: 'c',
                                                    value: 'demo-icon-circle-a',
                                                    router: 'dynamicSummary',
                                                    iconType: self.getEITIcons(notValidEit[x].eitcode, "FontAwesome"),
                                                    text: self.globalEitNameReport()[j].label,
                                                    visible: true,
                                                    code: notValidEit[x].eitcode},
                                                );
                                    }
                                }
                            }
                        }
                        self.icons.push(
                                {
                                    label: 'c',
                                    value: 'demo-icon-circle-a',
                                    router: 'employeeTimeAttandanceSummary',
                                    iconType: 'css/images/icons/overtime_assignment.png',
                                    text: self.employeeTimeAttandancelbl(),
                                    visible: true,
                                    code: ''
                                });

                    };

                    var serviceName = commonhelper.getAllRoleSetupValidation;
                    services.getAllRoleValidation(serviceName).then(getRoleValidationCbFn, self.failCbFn);




                };
                //------------------------------End draw self services-------------------------------------//      
                self.icons2 = ko.observableArray();

                self.iconNavigation = function (router, code) {
                    self.navCollMax();
                    oj.Router.rootInstance.store(code);
                    oj.Router.rootInstance.go(router);
                    return true;

                };
                self.drawIconsSpecialist = function () {

                    if (self.personDetails()) {

                        self.icons2([
                            {
                                label: 'b', value: 'demo-icon-circle-b', router: 'personSearch', iconType: 'fa fa-briefcase', text: self.specialist(), visible: true, code: "Job"
                            }


                        ]);


                    }

                };


                // get All PaaS Defult Value
                self.getAllPaaSDefultValue = function () {

                    var getPaasSDefultValueCBF = function (data) {

                        Object.assign(self.allPaaSDefultValuearry(), data);

                    };
                    services.getGeneric(commonhelper.allPaaSDefultValue).then(getPaasSDefultValueCBF, self.failCbFn);
                };

                self.getEITSegments = function (eitCode) {
                    var tempArray = [];
                    for (var i = 0; i < self.allEITArray().length; i++) {
                        if (self.allEITArray()[i].DESCRIPTIVE_FLEX_CONTEXT_CODE == eitCode) {

                            tempArray.push(self.allEITArray()[i]);
                        }
                    }
                    return tempArray;
                };
                if (self.userName() || self.password())
                    self.getAllPaaSDefultValue();
                self.getAllEIT = function () {
                    var getAllEitCBFN = function (data) {
                        self.allEITArray(data);

                    };
                    var lang = '';
                    if (self.getLocale() === "ar") {
                        lang = 'AR';
                    } else {
                        lang = 'US';
                    }
                    //  services.getAllEIT(self.EitCode, lang).then(getAllEitCBFN, self.failCbFn);
                };
                self.signOut = function () {

                    self.loading(true);
                    self.navTopArray([]);

                    var today = new Date();
                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var signOutdateTime = date + ' ' + time;
                    var payloadsignOutHistory = {
                        "personNumber": self.userName(),
                        "browserAndDeviceDetails": deviceAndBrowserDetails,
                        "deviceIp": deviceIp,
                        "loginDate": self.loginDateFromSass(),
                        "signOut": signOutdateTime

                    };
                    var SShrsignOutHistory = function (data) {
                        setTimeout(function () {
                            var getEmpDetails = function () {};
                            services.logOutUser(self.userName(), self.jwt(), self.hostUrl()).then(getEmpDetails, getEmpDetails);
                        }, 1);

                        /* Replace the Fusion-SSHR/ with the project name
                         *  Works only on deployed if the link is (eg. https://example.com/Fusion-SSHR/ )
                         *  This method clears the URL with the project name to avoid any uncaught errors
                         */
                        window.history.pushState({}, document.title, "/" + "AEC-SSHR-PROD/");
                        location.reload();
                        $('.MYdropdown_menu').hide();
                        self.isUserLoggedIn(false);
                        sessionStorage.setItem('username', '');
                        localStorage.setItem('selectedLanguage', '');
                        sessionStorage.clear();

                        if (self.getLocale() === "ar") {
                            $('html').attr({'dir': 'rtl'});
                        } else
                        {
                            $('html').attr({'dir': 'ltr'});
                            $('.noti').removeClass('noti-count-ar');
                            $('.noti').addClass('noti-count');
                        }
                        self.navTopArray([]);
                        self.navDataSource(new oj.ArrayTableDataSource(self.navTopArray(), {idAttribute: 'id'}));
                    };
                    services.addGeneric("SShrLoginHistory/UpdateSShrLoginHistory", JSON.stringify(payloadsignOutHistory)).then(SShrsignOutHistory, self.failCbFn());

                };

                self.refresh = function () {
                    initTranslations();
                    if (!self.personDetails()) {
//                        $.when(showEmplDetails(ko, services, self, km, false, commonhelper)).done(function () {
//                            if (self.personDetails()) {
//                            } else {
//
//                                self.isUserLoggedIn(false);
//                            }
//                        });


                    } else {
                        self.isUserLoggedIn(true);
                        self.globalFuseModel(JSON.parse(sessionStorage.getItem("fuseData")));
                        self.allEITArray(JSON.parse(sessionStorage.getItem("allEitSegment")));
                        populateEitName(this, commonhelper, JSON.parse(sessionStorage.getItem("eitName")));
                        papulateUserRole(this, commonhelper, JSON.parse(sessionStorage.getItem("userRole")));
                    }
                    //self.getAllEIT();
                    //pushNav();
                    //self.getBusinessUnits();
                    //self.getAllAbsenceTypes();
                };

                self.refresh();

                self.switchl = function () {
                    document.querySelector("#switch").open();
                };
                self.switchclose = function () {
                    $('.MYdropdown_menu').hide();
                    document.querySelector("#switch").close();
                };
                self.switchLanguage = function () {

                    $('.MYdropdown_menu').hide();
                    document.querySelector("#switch").close();
                    if (self.getLocale() === "ar") {

                        self.setLocale("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                        addFuseModelToIndexedDB(services, self, self, commonhelper);
                    } else if (self.getLocale() === "en-US") {
                        self.setLocale("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                        addFuseModelToIndexedDB(services, self, self, commonhelper);
                    }
                };
                self.confirm = function () {
                    document.querySelector("#signoutDialog").open();
                };

                self.cancelButton = function () {
                    $('.MYdropdown_menu').hide();
                    document.querySelector("#signoutDialog").close();
                };

//        if (typeof (Storage) !== "undefined") {
                var selectedLanguage = localStorage.getItem("selectedLanguage");

                if (selectedLanguage !== null && (selectedLanguage === "en-US" || selectedLanguage === "ar")) {
                    self.setLocale(selectedLanguage);
                }
//        }

                //for backbtn
                self.BackEFF = function () {
                    oj.Router.rootInstance.go('home');
                };


//End
                self.getPaaSLookup = function (lookupName) {
                    var temparry = [];
                    //self.passReturnArr([]);
                    for (var i = 0; i < self.PaaSLookup().length; i++) {
                        if (self.PaaSLookup()[i].name == lookupName) {
                            if (localStorage.getItem("selectedLanguage") === "ar") {

                                temparry.push({
                                    value: self.PaaSLookup()[i].code, label: self.PaaSLookup()[i].valuAr
                                });
                            } else {
                                temparry.push({
                                    value: self.PaaSLookup()[i].code, label: self.PaaSLookup()[i].valuEn
                                });
                            }
                        }
                    }

                    return temparry;
                };

                self.getEITIcons = function (eitcode, icontype) {

                    for (var i = 0; i < self.EITIcons().length; i++) {
                        if (self.EITIcons()[i].eitCode === eitcode && icontype == "ImageIcone") {

                            return  self.EITIcons()[i].icons;
                        } else if (self.EITIcons()[i].eitCode === eitcode && icontype == "FontAwesome") {

                            return self.EITIcons()[i].leftIcons;

                        }
                    }


                };





                self.getPersonNumber = function (personNumber) {

                    for (var i = 0; i < self.PersonNumberArr().length; i++)
                        if (self.PersonNumberArr()[i].value == personNumber) {
                            return self.PersonNumberArr()[i].label;
                        }

                };
                self.conutEitRequest = ko.observableArray([]);
                self.getCountEITRequest = async function () {
                    self.firstName(self.personDetails().firstName);
                    self.lastName(self.personDetails().lastName);
                    self.initials(oj.IntlConverterUtils.getInitials(self.firstName(), self.lastName()));
                    var getCountEITRequestCBF = function (data) {
                        self.conutEitRequest(data);

                    };
                    await services.getGenericAsync("PeopleExtra/countRequest/" + self.personDetails().personId).then(getCountEITRequestCBF, self.failCbFn);
                };

                function initTranslations() {
                    self.Usernametext(getTranslation("login.userName"));
                    self.passwordLabel(getTranslation("login.Password"));
                    self.loginLabel(getTranslation("login.loginLabel"));
                    self.lblSignOut(getTranslation("login.SignOut"));
                    self.SignIn(getTranslation("login.SignIn"));
                    self.forgetPassword(getTranslation("login.forgetPassword"));
                    self.Searchlbl(getTranslation("login.Search"));
                    self.ok(getTranslation("login.ok"));
                    self.managerType(getTranslation("login.managerType"));
                    self.yes(getTranslation("others.yes"));
                    self.no(getTranslation("others.no"));
                    self.showDetails(getTranslation("login.showDetails"));
                    self.confirmMessage(getTranslation("labels.confirmMessage"));
                    self.oprationMessage(getTranslation("labels.message"));
                    self.projectedStartDate(getTranslation("login.projectedStartDate"));
                    self.assignmentProjectedEndDate(getTranslation("login.assignmentProjectedEndDate"));
                    self.changeLbl(getTranslation("labels.changeLang"));
                    self.signoLbl(getTranslation("labels.signOut"));
                    self.peopleGroup(getTranslation("login.peopleGroup"));
                    self.assignmentStatusTypeId(getTranslation("login.assignmentStatusTypeId"));
                    self.fax(getTranslation("login.workPhoneExt"));
                    self.faxExt(getTranslation("login.faxExt"));
                    self.organizationName(getTranslation("login.organizationName"));
                    self.BusinessMissionRequestLbl(getTranslation("pages.BusinessMissionRequest"));
                    self.overtimeRequestLbl(getTranslation("pages.overtimeRequest"));
                    self.userKeywordLable(getTranslation("login.KeywordLable"));
                    self.viewProfileLbl(getTranslation("common.viewProfile"));
                    self.viewBulkLbl(getTranslation("common.bulk"));
                    self.specialist(getTranslation("common.specialist"));
                    self.SpecialistLbl(getTranslation("common.specialist"));
                    self.employeeTimeAttandancelbl(getTranslation("pages.employeeTimeAttandance"));
                    if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                        self.loginFailureText(getTranslation("login.loginFailureText"));
                    }
                    //constructNavLeftData();
//                  self.constructNavTopData();
                    self.navDataSource(new oj.ArrayTableDataSource(self.NavIConsTopArr(), {idAttribute: 'id'}));

                    self.languageSwitch_lng(getTranslation("common.switchLang"));
                }
                self.compare = function (post, operator, value) {
                    var postType = typeof post;
                    var valueType = typeof value;
                    if (postType == 'boolean' && valueType == 'boolean') {
                        switch (post & value) {
                            case 0 :
                                return false;
                            case 1 :
                                return true;
                        }
                    }
                    switch (operator) {
                        case '>':
                            return post > value;
                        case '<':
                            return post < value;
                        case '>=':
                            return post >= value;
                        case '<=':
                            return post <= value;
                        case '==':
                            return post == value;
                        case '!=':
                            return post != value;
                        case '===':
                            return post === value;
                        case '!==':
                            return post !== value;
                    }
                };
                self.isLastApprover = function (transationId, ss_type) {
                    var roleType, roleId;
                    var isLast;
                    var responseCode;
                    var getLastApprove = function (data) {
                        if (data.length) {
                            roleType = data[0].rolrType;
                            roleId = data[0].roleId;
                            responseCode = data[0].responseCode;
                            if (roleType == "POSITION" && roleId == self.personDetails().positionId) {

                                isLast = true;

                            } else if ((roleType == "LINE_MANAGER" || roleType == "LINE_MANAGER+1") && roleId == self.personDetails().personId) {

                                isLast = true;

                            } else if (roleType == "EMP" && roleId == self.personDetails().personId) {
                                isLast = true;
                            } else if (roleType == "ROLES") {
                                for (var i = 0; i < self.userRoles().length; i++) {
                                    if (self.userRoles()[i].roleId == roleId && responseCode) {
                                        isLast = true;

                                        break;
                                    }
                                }

                            } else if (roleType == "JOB_LEVEL" && roleId == self.personDetails().personId) {
                                isLast = true;
                            } else if (roleType == "AOR" && roleId == self.personDetails().personId) {
                                isLast = true;
                            }
                        } else {
                            isLast = true;

                        }

                    };


                    services.getGeneric(commonhelper.isLastStepApproval + transationId + "/" + ss_type).then(getLastApprove, self.failCbFn);
                    return isLast;
                };

                self.OpenReportSeesion = async function () {
                    var roleType, roleId;
                    var isLast;
                    var geOpenReportSeesionCBFN = function (data) {
                    };
                    await services.getGenericAsync(commonhelper.reportSession).then(geOpenReportSeesionCBFN, self.failCbFn);
                    return isLast;
                };

//                self.OpenReportSeesion();
                self.getApprovalCondetion = function (eitCode, model) {
                    var approvalCode = 'Default';
                    var firstKey;
                    var operation;
                    var secondKey;
                    for (var i = 0; i < self.ApprovalCondition().length; i++) {
                        if (self.ApprovalCondition()[i].eitCode == eitCode) {
                            if (self.ApprovalCondition()[i].firstKeyType == "static") {
                                firstKey = self.ApprovalCondition()[i].firstKeyType;
                            } else if (self.ApprovalCondition()[i].firstKeyType == "eitSegment") {
                                firstKey = model[self.ApprovalCondition()[i].firstEitSegment]()
                            }
                            operation = self.ApprovalCondition()[i].operation;
                            if (self.ApprovalCondition()[i].secondKeyType == "static") {
                                secondKey = self.ApprovalCondition()[i].secondKeyValue
                            } else if (self.ApprovalCondition()[i].secondKeyType == 'eitSegment') {
                                secondKey = model[self.ApprovalCondition()[i].secondKeyType]
                            }
                            if (self.compare(firstKey, operation, secondKey)) {
                                approvalCode = self.ApprovalCondition()[i].approvalCode;
                                return approvalCode;
                            }
                        }
                    }
                    return approvalCode;

                }
            }

            return new ControllerViewModel();
        }

);
