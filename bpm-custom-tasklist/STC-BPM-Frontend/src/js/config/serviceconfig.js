/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery'], function ($) {

    /**
     * The view model for managing service calls
     */
    function serviceConfig() {

        var self = this;
        self.contentTypeApplicationJSON = 'application/json';
        self.contentTypeApplicationXWWw = 'application/x-www-form-urlencoded; charset=UTF-8';
        self.contentTypeMultiPart = 'multipart/form-data';
        var paasAuth = "";
        self.headers = {
        };
        self.callGetService = function (serviceUrl, contentType, headers, asynchronized) {

            var defer = $.Deferred();
            if (headers) {
                headers.Authorization = paasAuth;
            }

            $.ajax({
                type: "GET",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,

                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callPostApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = paasAuth;
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            console.log("callPostApexService", payloadStr)
            var defer = $.Deferred();

            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                    console.log(xhr);
                    console.log(ajaxOptions);
                    console.log(thrownError);
                    console.log(payload);
                    console.log(payloadStr);
                    console.log(serviceUrl);
                    console.log(contentType);
                    console.log(headers);
                }
            });
            return $.when(defer);
        };
    }

    return new serviceConfig();
});