/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['config/serviceconfig', 'util/commonhelper'],
        function (serviceConfig, commonHelper) {

            /**
             * The view model for managing all services
             */
            function services() {

                var self = this;
                var restPath = commonHelper.getInternalRest();
                // POST example

                self.getGeneric = function (serviceName, headers) {
                    var serviceURL = restPath + serviceName;
                    return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, false);
                };
                
                self.addGeneric = function (serviceName, payload) {
                    var serviceURL = restPath + serviceName;
                    console.log(serviceURL);
                    console.log(payload);
                     
                    var headers = {
                    };
                    return serviceConfig.callPostApexService(serviceURL, payload, 'application/json', true, headers);
                };

            };
            return new services();
        });