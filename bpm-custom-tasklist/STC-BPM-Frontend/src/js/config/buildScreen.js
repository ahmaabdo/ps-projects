//
//define(['config/services', 'appController', 'knockout', 'jquery', 'ojs/ojdatetimepicker', 'ojs/ojtimezonedata', 'ojs/ojradioset',
//    'jquery-calendar', 'jquery-calendar-plus', 'jquery-calendar-ummalqura', 'jquery-calendar-plugin', 'jquery-calendar-picker'], function (services, app, ko, $) {
//    function Screen() {
//        var self = this;
//        var inputIdCount = 1;
//        self.buildScreen = function buildScreen(EIT, formName, model, isDisabled, arr) {
//            var varName;
//            var req;
//            var isDisabledName;
//
//            for (var i = 0; i < EIT.length; i++) {
//                if (EIT[i].DISPLAY_FLAG == "Y") {
//                    var div = document.createElement("div");
//                    div.setAttribute('class', 'form_field_dy_style');
//                    var parent = document.getElementById("xx");
//                    parent.appendChild(div);
//                    if (EIT[i].REQUIRED_FLAG == "Y") {
//                        req = true;
//                    } else {
//                        req = false;
//                    }
//
//
//                    //----------------End Div -------------------
//                    // drawLabel(EIT[i].END_USER_COLUMN_NAME);
//
//                    isDisabledName = 'isDisabledx.' + 'isDisabled' + EIT[i].DESCRIPTION;
//                    varName = 'model.' + EIT[i].DESCRIPTION;
//                    if (EIT[i].FLEX_value_SET_NAME == "100 Character No Validation") {
//                        if (EIT[i].DISPLAY_TYPE == 'TEXT_BOX') {
//                            drawInputText(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, EIT[i].DESCRIPTION);
//                        } else if (EIT[i].DISPLAY_TYPE == 'TEXT_AREA') {
//                            drawInputTextArea(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, EIT[i].DESCRIPTION);
//                        }
//                        
//                    } else if (EIT[i].FLEX_value_SET_NAME == "10 Number" || EIT[i].FLEX_value_SET_NAME == "XXX_HR_PAAS_NUMBER"
//                            || EIT[i].FLEX_value_SET_NAME == "HRC_NUMBER_10") {
//                        drawInputNumber(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, EIT[i].DESCRIPTION);
//                    } else if (EIT[i].FLEX_value_SET_NAME == "HRC_STANDARD_DATE" || EIT[i].FLEX_value_SET_NAME == "XXX_HR_PAAS_DATE") {
//                        drawInputDate(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, EIT[i].DESCRIPTION);
//                    } else if (EIT[i].FLEX_value_SET_NAME == "XXX_HR_PAAS_HIJRI_DATE") {
//                        drawInputDateHijri(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, EIT[i].DESCRIPTION);
//                    }
//                    else if (EIT[i].FLEX_value_SET_NAME == "XXX_HR_PAAS_TIME") {
//                        drawInputTime(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, EIT[i].DESCRIPTION);
//                    }
//                    else {
//                        if (EIT[i].FLEX_value_SET_NAME != "XXX_HR_EIT_ATTACHMENTS" && EIT[i].FLEX_value_SET_NAME != "XXX_HR_PAAS_VIEW_REPORT") {
//                            {
//                                var arrName = 'Arrs.' + EIT[i].FLEX_value_SET_NAME + EIT[i].DESCRIPTION + 'Arr';
//                            //    if (arrName.includes('YES_NO')){
//                                    //drawRadioSet(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, arrName, EIT[i].DESCRIPTION);
//                              //  }
//                               // else
//                                  //  drawList(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, arrName, EIT[i].DESCRIPTION);
//                               if(EIT[i].DISPLAY_TYPE == "RADIO_BUTTON_GROUP")
//                                {
//                                     div.setAttribute('class', 'form_field_dy_style');
//                                     drawRadioSet(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, arrName, EIT[i].DESCRIPTION);
//                                }
//                                else
//                                drawList(varName, EIT[i].FORM_ABOVE_PROMPT, req, div, isDisabledName, arrName, EIT[i].DESCRIPTION);
//                                    
//                                
//                            }
//                             
//                        }
//                    }
//
//
//                }
//
//
//
//            }
//        };
//        function drawDiv(div) {
//            var divCreated = document.createElement("div");
//            divCreated.setAttribute('class', 'form-group row formFieldRow');
//            var parent = div;
//            parent.appendChild(divCreated);
//            return divCreated;
//        }
//        function drawInputText(varName, lblValue, required, div, isDisabledName, id) {
//            //drawLabel(lblValue, div);
//            var input = document.createElement("oj-input-text");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id + ' col-sm-8');
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//            var parent = drawLabel(lblValue, div, required);
//            // return input ; 
//            parent.appendChild(input);
//        }
//        function drawInputTextArea(varName, lblValue, required, div, isDisabledName, id) {
//            //drawLabel(lblValue, div);
//            var input = document.createElement("oj-text-area");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id);
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//            var parent = drawLabel(lblValue, div, required);
//            // return input ; 
//            parent.appendChild(input);
//        }
//        function drawInputNumber(varName, lblValue, required, div, isDisabledName, id) {
//            // drawLabel(lblValue,div);
//            var input = document.createElement("oj-input-number");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id+' col-sm-8');
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('min', 0);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
////    input.setAttribute('class','addwidth')
//            var parent = drawLabel(lblValue, div, required);
//            //return input ; 
//            parent.appendChild(input);
//        }
//        function drawLabel(lblValue, div, isRequired) {
//            var label = document.createElement("oj-label");
//            label.setAttribute('class', 'col-sm-4 col-form-label');
//            label.setAttribute('show-required', "[[" + isRequired + "]]");
//            label.innerHTML = lblValue;
//            var parent = drawDiv(div);
//            parent.appendChild(label);
//
//
//            return parent;
//        }
//
//        function drawInputDate(varName, lblValue, required, div, isDisabledName, id) {
//            //drawLabel(lblValue,div);
//            var input = document.createElement("oj-input-date");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id+' col-sm-8');
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//            input.setAttribute('converter', '{{dateConverter}}');
//            //on-value-changed
//            var parent = drawLabel(lblValue, div, required);
//            parent.appendChild(input);
//        }
//        
//         function drawInputTime(varName, lblValue, required, div, isDisabledName, id) {
//            //drawLabel(lblValue,div);
//            var input = document.createElement("oj-input-time");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id+' col-sm-8');
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//           // input.setAttribute('converter', '{{dateConverter}}');
//            //on-value-changed
//            var parent = drawLabel(lblValue, div, required);
//            parent.appendChild(input);
//        }
//
//        function drawInputDateHijri(varName, lblValue, required, div, isDisabledName, id) {
//            var input = document.createElement("oj-input-text");
//            inputIdCount++;
//            var inputId = 'dateId' + inputIdCount;
//            //input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id +' col-sm-8');
//            input.setAttribute('id', inputId);
//            input.setAttribute('min', "1440-02-10");
//            input.setAttribute('max', "1500-02-13");
//            input.setAttribute('autocomplete', "off");
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//            var parent = drawLabel(lblValue, div, required);
//            parent.appendChild(input);
//            $('#' + inputId).calendarsPicker({calendar: $.calendars.instance('ummalqura'),
//                onSelect: function (dates) {
//                    varName = $('#' + inputId).val();
//                }});
//        }
//
//        function drawList(varName, lblValue, required, div, isDisabledName, valueSetName, id) {
//
//            var input = document.createElement("oj-select-one");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id + ' col-sm-8');
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('options', '{{' + valueSetName + '}}');
//            input.setAttribute('placeholder',"Please select value");
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//            //set id to select to fix double click to open drop down list issue
//            input.setAttribute('id', 'select');
//            //input.setAttribute('id', varName);
//            input.setAttribute('name', varName);
//            var parent = drawLabel(lblValue, div, required);
//
//            parent.appendChild(input);
//        }
//        
//        function drawRadioSet(varName, lblValue, required, div, isDisabledName, valueSetName, id) {
//            var input = document.createElement("oj-radioset");
//            input.setAttribute('disabled', '{{' + isDisabledName + '}}');
//            input.setAttribute('class', id + ' oj-choice-direction-row'+' col-sm-8');
//            input.setAttribute('value', '{{' + varName + '}}');
//            input.setAttribute('required', required);
//            input.setAttribute('on-value-changed', '{{segmentChangedHandler}}');
//            input.setAttribute('id', varName);
//            
//            var koForEach = document.createComment("ko foreach: " + valueSetName);
//            var koForEachClose = document.createComment("/ko");
//            var options = document.createElement("oj-option");
//            options.setAttribute('data-bind', 'value: value');
//            var span = document.createElement("span");
//            span.setAttribute('data-bind', 'text: label');
//            span.setAttribute('class', 'span_field_edit');
//            //span.style.margin = "15px 0 !important";
//            //span.style.backgroundColor = "aqua";
//            options.appendChild(span);
//            input.appendChild(koForEach);
//            input.appendChild(options);
//            input.appendChild(koForEachClose);
//            
//            var parent = drawLabel(lblValue, div, required);
//            parent.appendChild(input);
//        }
//
//    }
//    return new Screen();
//});
