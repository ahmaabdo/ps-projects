package common.restHelper;


import com.appspro.db.CommonConfigReader;

import com.appspro.fusionsshr.bean.rest.Link_;

import com.appspro.fusionsshr.dao.EmployeeDetails;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import java.awt.image.BufferedImage;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import javax.imageio.ImageIO;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.ws.rs.core.MediaType;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.joda.time.Chronology;
import org.joda.time.LocalDate;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


public class RestHelper {

    private String InstanceUrl =
        CommonConfigReader.getValue("InstanceUrl"); //;"https://ejcp-test.fa.em2.oraclecloud.com:443";
    private String orgnizationUrl =
        "//hcmCoreSetupApi/resources/latest/organizations";
    private String biReportUrl =
        CommonConfigReader.getValue("biReportUrl"); //"https://ejcp-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService";
    private String SecurityService =
        CommonConfigReader.getValue("SecurityService");
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private String instanceName =
        CommonConfigReader.getValue("instanceName"); //"ejcp-test.fa.em2.oraclecloud.com";
    private String gradeUrl = "/hcmCoreSetupApi/resources/latest/grades";
    private String locationUrl = "/hcmCoreSetupApi/resources/latest/locations";
    private String positionUrl = "/hcmCoreSetupApi/resources/latest/positions";
    private String jobsUrl = "/hcmCoreSetupApi/resources/latest/jobs";
    private String absenceUrl = "/hcmRestApi/resources/11.13.17.11/absences/";
    private String departmentUrl="/hcmRestApi/resources/11.13.18.05/departmentsLov";
    public static String USER_NAME =
        CommonConfigReader.getValue("USER_NAME"); //"mahmoud.essam@appspro-me.com";
    public static String PASSWORD = CommonConfigReader.getValue("PASSWORD");
    public static String Schema_Name =
        CommonConfigReader.getValue("Schema_Name");
    public final static String SAAS_URL =
        "https://" + CommonConfigReader.getValue("instanceName");
    public final static String STATIC_INSTANCE_NAME =
        "ejfz-test.fa.em2.oraclecloud.com";


    public static String getSchema_Name() {
        return Schema_Name;
    }


    public String getAbsenceUrl() {
        return absenceUrl;
    }

    public void setAbsenceUrl(String absenceUrl) {
        this.absenceUrl = absenceUrl;
    }

    public String getJobUrl() {
        return this.jobsUrl;
    }

    public String getPositionUrl() {
        return positionUrl;
    }

    public String getLocationUrl() {
        return this.locationUrl;
    }

    public String getGradeUrl() {
        return this.gradeUrl;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String getEmployeeUrl() {
        return InstanceUrl + employeeServiceUrl;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }
    
    
    

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public String convertToHijri(String d) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");

        java.util.Date dateNew = format1.parse(d);
        String formatedDate = format2.format(dateNew);
        String[] parts = formatedDate.split("/");
        String year = parts[2]; // 004
        String month = parts[1]; // 034556
        String day = parts[0];
        int addDay = Integer.parseInt(day);
        int addMonth = Integer.parseInt(month);
        int addYear = Integer.parseInt(year);
        int number_Of_DaysInMonth = 0;
        switch (addMonth) {
        case 1:
            number_Of_DaysInMonth = 31;
            break;
        case 2:
            if ((addYear % 400 == 0) ||
                ((addYear % 4 == 0) && (addYear % 100 != 0))) {
                number_Of_DaysInMonth = 29;
            } else {
                number_Of_DaysInMonth = 28;
            }

            break;
        case 3:
            number_Of_DaysInMonth = 31;
            break;
        case 4:
            number_Of_DaysInMonth = 30;
            break;
        case 5:
            number_Of_DaysInMonth = 31;
            break;
        case 6:
            number_Of_DaysInMonth = 30;
            break;
        case 7:
            number_Of_DaysInMonth = 31;
            break;
        case 8:
            number_Of_DaysInMonth = 31;
            break;
        case 9:
            number_Of_DaysInMonth = 30;
            break;
        case 10:
            number_Of_DaysInMonth = 31;
            break;
        case 11:
            number_Of_DaysInMonth = 30;
            break;
        case 12:
            number_Of_DaysInMonth = 31;
            break;
        default:
            number_Of_DaysInMonth = 0;
            break;

        }

        if (number_Of_DaysInMonth == 28 || number_Of_DaysInMonth == 29) {
            if (addDay <= 27) {

                addDay += 1;

            } else {
                addDay = 0;
                addMonth += 1;
                addDay += 1;
            }

        }
        if (number_Of_DaysInMonth == 30 || number_Of_DaysInMonth == 31) {
            if (addDay <= 29) {

                addDay += 1;

            } else {
                addDay = 0;
                addMonth += 1;
                addDay += 1;
            }

        }
        day = String.valueOf(addDay);

        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();

        LocalDate todayIso =
            new LocalDate(Integer.parseInt(year), Integer.parseInt(month),
                          Integer.parseInt(day), iso);
        LocalDate todayHijri =
            new LocalDate(todayIso.toDateTimeAtStartOfDay(), hijri);
        return todayHijri.toString();
    }

    public String convertToGregorian(String d) {
        d = d.replace("-", "/");
        String[] parts = d.split("/");
        //        System.out.println(Arrays.toString(parts));
        String year = parts[0]; // 004
        String month = parts[1]; // 034556
        System.out.println(month);
        String day = parts[2];
        int addDay = Integer.parseInt(day);
        addDay += 1;
        day = String.valueOf(addDay);

        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        //1434-05-18
        LocalDate todayHijri =
            new LocalDate(Integer.parseInt(year), Integer.parseInt(month),
                          Integer.parseInt(day), hijri);
        System.out.println("todayHijri" + todayHijri);
        LocalDate todayIso =
            new LocalDate(todayHijri.toDateTimeAtStartOfDay(), iso);

        return todayIso.toString();
    }

    public String callSaaS(String serverURL, String restFrameWorkVersion) {
        SSLContext ctx = null;
        System.setProperty("DUseSunHttpHandler", "true");
        TrustManager[] trustAllCerts =
            new X509TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs,
                                               String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                }
            } };
        try {
            ctx = SSLContext.getInstance("SSL");
            ctx.init(null, trustAllCerts, null);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Error loading ssl context {}" +
                               e.getMessage());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        SSLContext.setDefault(ctx);

        ClientConfig def = new DefaultClientConfig();

        def.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
                                new HTTPSProperties(null, ctx));
        Client client = Client.create(def);

//        client.addFilter(new HTTPBasicAuthFilter(RestHelper.USER_NAME,
//                                                 RestHelper.PASSWORD));
        
        serverURL = serverURL.replaceAll("\\s", "%20");
        WebResource resource = client.resource(serverURL);
        resource.header("Authorization",restFrameWorkVersion);
        resource.header("x-igate-client","c448c5d987327f6314bcd73d85dfc312546cb8d2ecc909e64087bf2c5aea7943\\content-type:text/json\\host:x-igate-API\\useragent:internal/8.1\\version:1.0");

        return resource.accept(MediaType.APPLICATION_JSON_TYPE,
                               MediaType.APPLICATION_XML_TYPE).header("Accept-Charset",
                                                                      "utf-8").get(String.class);
    }

    public Document httpPost(String destUrl,
                             String postData) throws Exception {
        System.out.println(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return doc;
        } else {
            System.out.println("Failed");
        }


        return null;
    }

    public static String getAuth() {
        byte[] message = (USER_NAME + ":" + PASSWORD).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
    //    public static void main (String [] args ){
    //        System.out.println(getAuth());
    //    }

    public void setSecurityService(String SecurityService) {
        this.SecurityService = SecurityService;
    }

    public String getSecurityService() {
        return SecurityService;
    }
    public String getEmailBody(String reqType, String approvalName, String reqName ,String BeneficiaryName ){
        String body = "";
        if(reqType.equals("FYI")){
            body = "Dear "+ approvalName+",\\n\\r" + "Kindly review for your information the below details: " +
                "Request Name " + reqName + "\\n\\r  Beneficiary : " + BeneficiaryName ;
                
        }else if(reqType.equals("FYA")){
            body = "Dear "+ approvalName+", " + 
                   "Your Action required for the \\n\\r following:-  " +
                "Request Name :  " + reqName + "\\n\\\r  Beneficiary : " + BeneficiaryName +
                "  Please connect to HCM.HHA.com.sa";
        }
        return body ; 
    }
    public String getEmailSubject(String reqType ,String reqName , String BeneficiaryName ){
        String subject = "";
        if(reqType.equals("FYI")){
            subject = "FYI  Approval of  "+  reqName +"for  " + BeneficiaryName ;
        }else if(reqType.equals("FYA")){
            subject = "Action Required   Approval of  "+  reqName +"for  " + BeneficiaryName ;
        }          
        return subject ; 
    }

    public void setDepartmentUrl(String departmentUrl) {
        this.departmentUrl = departmentUrl;
    }

    public String getDepartmentUrl() {
        return departmentUrl;
    }
    
    public static String hmacDigest(String msg, String keyString,
                                    String algo) {
        String digest = null;
        try {
            SecretKeySpec key =
                new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }
    public static String CurrentGMTDate() {

        Date date = new Date();
        //                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat dateFormat =
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        //                    System.out.println("UTC Time is: " + dateFormat.format(date).toString());

        return dateFormat.format(date).toString();

    }
    
    public static void main(String arp[]) throws ProtocolException,
                                                 IOException {
        
        String secret = "@@igate-internal-Client@@2016:password2016";
        String httpMethod = "GET";
        String sourceURL = "/igate/v1.0/en/private/users/halmushigih@stc.com.sa/validateTokenen";
        String date = CurrentGMTDate();
        String client = "internal";
        String version = "v1.0";
        
        String message = httpMethod.concat(sourceURL).concat(date).concat(client).concat(version).concat(httpMethod);
        String hashed = hmacDigest(message, secret, "HmacSHA256");
        String unHashed = "\\content-type:text/json\\host:x-igate-API\\useragent:internal/8.1\\version:1.0";
        
        String fullMessage = hashed + unHashed;
        
//        new RestHelper().callSaaS("https://igateapp.stc.com.sa/igate/v1.0/en/private/users/halmushigih@stc.com.sa/validateToken", "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..jByJNpGKMJNTYXYpBJ4z7Q.YXkM-zFgvfFPImc-Jdk_8EXpFmuAjuxZdLh2d-6DKw1dAv-bPSS8jw6UUMRCQhNwnaScQcq_xlwo2lC8hcveruM9MvDEQeqH1VxBETO94nf136Um_IFJCGA8aiBGms6UV-WM-yYPMsjiJ8lrEKVyEq32Cd0w2mhdYMtX-YOyYqwxwBJBVkfdgJIKnfWphZGpEbN0cPR32D046OOCJ4osE2ABfb51S0hNsCuuGtHuWCK6rAmL97BCmJdOpnGRkBkY.zCtHtNyKgR47zQ_qF0w7QA");
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        System.setProperty("DUseSunHttpHandler", "true");
            String serverUrl = "https://igateapp.stc.com.sa/igate/v1.0/en/private/users/halmushigih@stc.com.sa/validateToken";
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(EmployeeDetails.DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = "https://igateapp.stc.com.sa";
            //connection = (HttpsURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                                          "application/json; Charset=UTF-8");
            //  connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            connection.addRequestProperty("Authorization",
                                          "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..jByJNpGKMJNTYXYpBJ4z7Q.YXkM-zFgvfFPImc-Jdk_8EXpFmuAjuxZdLh2d-6DKw1dAv-bPSS8jw6UUMRCQhNwnaScQcq_xlwo2lC8hcveruM9MvDEQeqH1VxBETO94nf136Um_IFJCGA8aiBGms6UV-WM-yYPMsjiJ8lrEKVyEq32Cd0w2mhdYMtX-YOyYqwxwBJBVkfdgJIKnfWphZGpEbN0cPR32D046OOCJ4osE2ABfb51S0hNsCuuGtHuWCK6rAmL97BCmJdOpnGRkBkY.zCtHtNyKgR47zQ_qF0w7QA");
            
        connection.addRequestProperty("Date",
                                      "[{\"key\":\"Date\",\"value\":\"Thu, 23 May 2019 12:31:09 GMT\",\"description\":\"\",\"type\":\"text\",\"enabled\":true}]");
        
        connection.addRequestProperty("x-igate-client",
                                      "c448c5d987327f6314bcd73d85dfc312546cb8d2ecc909e64087bf2c5aea7943\\content-type:text/json\\host:x-igate-API\\useragent:internal/8.1\\version:1.0");
        
            InputStream bi =
                connection.getInputStream();
            connection.disconnect();
        
        
    }
}
