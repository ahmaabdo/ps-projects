package com.appspro.bpm.bean;

public class TaskQueryServiceBean {

    private String title;
    private String TransactionId;
    private String requestor;
    private String identificationKey;
    private String ApplicationId;
    private String ModuleGroup;
    private String ModuleIdentifier;
    private String ObjectId;
    private String SubjectId;
    private String ReentryFunction;
    private String SectionDisplayName;
    private String sensorProcessName;
    private String sensorNameFromData;
    private String sensorDataReferenceCode;
    private String creator;
    private String priority;
    private String assignedDate;
    private String createdDate;
    private String digitalSignatureRequired;
    private String numberOfTimesModified;
    private String passwordRequiredOnUpdate;
    private String state;
    private String action;
    private String displayName;
    private String taskId;
    private String taskNumber;
    private String category;
    private String taskType;
    private String flowInstanceId;
    private String User;
    private String notificationMessage;
    private String checklistInstanceId;
    private String flowInstanceName;
    private String subCategory;
    private String checklistName;
    private String Process;
    private String Message;
    private String referenceId;
    private String Owner;
    private String dueDate;
    private String raisedOwner;
    private String WLType;
    private String flowTaskInstanceId;
    private String approveName;
    private String actionApprove;
    private String displayNameApprove;
    private String actionReject;
    private String displayNameReject;
    private String assignee;
    private String taskNamespace;
    private String updatedDate;
    private String updatedById;
    private String updatedByType;
    private String workflowPattern;
    private String taskState;
    private String originalAssigneeUser;
    private String errorInTask;

    public String getErrorInTask() {
        return errorInTask;
    }

    public void setErrorInTask(String errorInTask) {
        this.errorInTask = errorInTask;
    }

    public String getOriginalAssigneeUser() {
        return originalAssigneeUser;
    }

    public void setOriginalAssigneeUser(String originalAssigneeUser) {
        this.originalAssigneeUser = originalAssigneeUser;
    }

    public String getTaskState() {
        return taskState;
    }

    public void setTaskState(String taskState) {
        this.taskState = taskState;
    }

    public String getWorkflowPattern() {
        return workflowPattern;
    }

    public void setWorkflowPattern(String workflowPattern) {
        this.workflowPattern = workflowPattern;
    }
    

    public String getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(String updatedById) {
        this.updatedById = updatedById;
    }

    public String getUpdatedByType() {
        return updatedByType;
    }

    public void setUpdatedByType(String updatedByType) {
        this.updatedByType = updatedByType;
    }
    

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
    

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTransactionId(String TransactionId) {
        this.TransactionId = TransactionId;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setIdentificationKey(String identificationKey) {
        this.identificationKey = identificationKey;
    }

    public String getIdentificationKey() {
        return identificationKey;
    }

    public void setApplicationId(String ApplicationId) {
        this.ApplicationId = ApplicationId;
    }

    public String getApplicationId() {
        return ApplicationId;
    }

    public void setModuleGroup(String ModuleGroup) {
        this.ModuleGroup = ModuleGroup;
    }

    public String getModuleGroup() {
        return ModuleGroup;
    }

    public void setModuleIdentifier(String ModuleIdentifier) {
        this.ModuleIdentifier = ModuleIdentifier;
    }

    public String getModuleIdentifier() {
        return ModuleIdentifier;
    }

    public void setObjectId(String ObjectId) {
        this.ObjectId = ObjectId;
    }

    public String getObjectId() {
        return ObjectId;
    }

    public void setSubjectId(String SubjectId) {
        this.SubjectId = SubjectId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setReentryFunction(String ReentryFunction) {
        this.ReentryFunction = ReentryFunction;
    }

    public String getReentryFunction() {
        return ReentryFunction;
    }

    public void setSectionDisplayName(String SectionDisplayName) {
        this.SectionDisplayName = SectionDisplayName;
    }

    public String getSectionDisplayName() {
        return SectionDisplayName;
    }

    public void setSensorProcessName(String sensorProcessName) {
        this.sensorProcessName = sensorProcessName;
    }

    public String getSensorProcessName() {
        return sensorProcessName;
    }

    public void setSensorNameFromData(String sensorNameFromData) {
        this.sensorNameFromData = sensorNameFromData;
    }

    public String getSensorNameFromData() {
        return sensorNameFromData;
    }

    public void setSensorDataReferenceCode(String sensorDataReferenceCode) {
        this.sensorDataReferenceCode = sensorDataReferenceCode;
    }

    public String getSensorDataReferenceCode() {
        return sensorDataReferenceCode;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreator() {
        return creator;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }

    public void setAssignedDate(String assignedDate) {
        this.assignedDate = assignedDate;
    }

    public String getAssignedDate() {
        return assignedDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setDigitalSignatureRequired(String digitalSignatureRequired) {
        this.digitalSignatureRequired = digitalSignatureRequired;
    }

    public String getDigitalSignatureRequired() {
        return digitalSignatureRequired;
    }

    public void setNumberOfTimesModified(String numberOfTimesModified) {
        this.numberOfTimesModified = numberOfTimesModified;
    }

    public String getNumberOfTimesModified() {
        return numberOfTimesModified;
    }

    public void setPasswordRequiredOnUpdate(String passwordRequiredOnUpdate) {
        this.passwordRequiredOnUpdate = passwordRequiredOnUpdate;
    }

    public String getPasswordRequiredOnUpdate() {
        return passwordRequiredOnUpdate;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getFlowInstanceId() {
        return flowInstanceId;
    }

    public void setFlowInstanceId(String flowInstanceId) {
        this.flowInstanceId = flowInstanceId;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String User) {
        this.User = User;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getChecklistInstanceId() {
        return checklistInstanceId;
    }

    public void setChecklistInstanceId(String checklistInstanceId) {
        this.checklistInstanceId = checklistInstanceId;
    }

    public String getFlowInstanceName() {
        return flowInstanceName;
    }

    public void setFlowInstanceName(String flowInstanceName) {
        this.flowInstanceName = flowInstanceName;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getChecklistName() {
        return checklistName;
    }

    public void setChecklistName(String checklistName) {
        this.checklistName = checklistName;
    }

    public String getProcess() {
        return Process;
    }

    public void setProcess(String Process) {
        this.Process = Process;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String Owner) {
        this.Owner = Owner;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getRaisedOwner() {
        return raisedOwner;
    }

    public void setRaisedOwner(String raisedOwner) {
        this.raisedOwner = raisedOwner;
    }

    public String getWLType() {
        return WLType;
    }

    public void setWLType(String WLType) {
        this.WLType = WLType;
    }

    public String getFlowTaskInstanceId() {
        return flowTaskInstanceId;
    }

    public void setFlowTaskInstanceId(String flowTaskInstanceId) {
        this.flowTaskInstanceId = flowTaskInstanceId;
    }

    public String getApproveName() {
        return approveName;
    }

    public void setApproveName(String approveName) {
        this.approveName = approveName;
    }

    public String getActionApprove() {
        return actionApprove;
    }

    public void setActionApprove(String actionApprove) {
        this.actionApprove = actionApprove;
    }

    public String getDisplayNameApprove() {
        return displayNameApprove;
    }

    public void setDisplayNameApprove(String displayNameApprove) {
        this.displayNameApprove = displayNameApprove;
    }

    public String getActionReject() {
        return actionReject;
    }

    public void setActionReject(String actionReject) {
        this.actionReject = actionReject;
    }

    public String getDisplayNameReject() {
        return displayNameReject;
    }

    public void setDisplayNameReject(String displayNameReject) {
        this.displayNameReject = displayNameReject;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
    
    public void setTaskNamespace(String taskNamespace) {
        this.taskNamespace = taskNamespace;
    }

    public String getTaskNamespace() {
        return taskNamespace;
    }
    
    
    

}
