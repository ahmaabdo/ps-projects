
package com.appspro.bpm.bean;

public class TaskLogDetailsBean {
  
   private int id; 
   private String taskNumber;
   private String serviceName;
   private String serviceStatus;
   private String serviceActionType;
   private String serviceData;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTaskNumber() {
        return taskNumber;
    }
    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getServiceActionType() {
        return serviceActionType;
    }

    public void setServiceActionType(String serviceActionType) {
        this.serviceActionType = serviceActionType;
    }

    public String getServiceData() {
        return serviceData;
    }

    public void setServiceData(String serviceData) {
        this.serviceData = serviceData;
    }
   
   
}
