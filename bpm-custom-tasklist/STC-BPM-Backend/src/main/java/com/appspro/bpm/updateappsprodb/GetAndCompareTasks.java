/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bpm.updateappsprodb;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.EmployeeDetailsBean;
import com.appspro.bpm.bean.JobExecutionBean;
import com.appspro.bpm.bean.TaskQueryServiceBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.bpm.soap.TaskQueryService;
import static com.appspro.bpm.soap.TaskQueryService.getLastTwoStagesInHistory;
import static com.appspro.bpm.soap.TaskQueryService.getUpdatedTasksQueryFilterByTaskNumber;
import com.appspro.fusionsshr.inbox.InboxOperationsService;
import com.appspro.fusionsshr.rest.STCValidationIgateREST;
import com.appspro.stc.dao.QueryTasksDao;
import com.appspro.stc.database.DatabaseConnection;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author user
 */
public class GetAndCompareTasks {

    public static ArrayList<UpdateAppsproDBBean> getAllTasksForPerson(EmployeeDetailsBean allEmployeesBean,String companyName) throws AppsProException, Exception {

        TaskQueryService taskQueryObj2 = new TaskQueryService(companyName);
        ArrayList<UpdateAppsproDBBean> listQueryFilter = new ArrayList<UpdateAppsproDBBean>();

        listQueryFilter = taskQueryObj2.getAllTasksQueryFilterByPersonNameFinalWithHistoryTest2(allEmployeesBean,companyName);
        return listQueryFilter;

    }

    public static ArrayList<UpdateAppsproDBBean> getUpdatedTasksForPerson(EmployeeDetailsBean allEmployeesBean, DateTime dtStart, DateTime dtStop,String companyName) throws AppsProException {

        TaskQueryService taskQueryObj2 = new TaskQueryService(companyName);
        ArrayList<UpdateAppsproDBBean> listQueryFilter = new ArrayList<UpdateAppsproDBBean>();

        listQueryFilter = taskQueryObj2.getUpdatedTasksQueryFilterByPersonName(allEmployeesBean, dtStart, dtStop,companyName);

        return listQueryFilter;

    }

    public static ArrayList<UpdateAppsproDBBean> getAllEmpsTasks(String companyName) throws AppsProException, Exception {

        ArrayList<UpdateAppsproDBBean> allFusionTasks = new ArrayList<UpdateAppsproDBBean>();

        ArrayList<EmployeeDetailsBean> allEmpList = getAllEmployees(companyName);

        for (int i = 0; i < allEmpList.size(); i++) {

            ArrayList<UpdateAppsproDBBean> getTasks = getAllTasksForPerson(allEmpList.get(i),companyName);

            allFusionTasks.addAll(getTasks);
        }

        return allFusionTasks;
    }

    public static ArrayList<UpdateAppsproDBBean> getAllEmpsUpdatedTasks(DateTime dtStart, DateTime dtStop,String companyName) throws AppsProException {

        ArrayList<UpdateAppsproDBBean> allFusionTasks = new ArrayList<UpdateAppsproDBBean>();

        ArrayList<EmployeeDetailsBean> allEmpList = getAllEmployees(companyName);

        for (int i = 0; i < allEmpList.size(); i++) {

            ArrayList<UpdateAppsproDBBean> getTasks = getUpdatedTasksForPerson(allEmpList.get(i), dtStart, dtStop,companyName);

            allFusionTasks.addAll(getTasks);
            
        }

        return allFusionTasks;
    }

    public static ArrayList<EmployeeDetailsBean> getAllEmployees(String companyName) throws AppsProException {
        ArrayList<EmployeeDetailsBean> empUserNames = new ArrayList<EmployeeDetailsBean>();
        try {

            TaskQueryService taskQueryService = new TaskQueryService(companyName);

            int count = 0;
            int offsetResult = 0;
            String checkHasMore = "true";

            while (checkHasMore == "true") {

                String resp
                        = taskQueryService.callGetRest(taskQueryService.getHostName() + "/hcmRestApi/resources/11.13.18.05/emps?limit="
                                + 100 + "&&offset=" + offsetResult);

                JSONObject reportResponse = new JSONObject(resp.toString());

                Object jsonTokneritems
                        = new JSONTokener(reportResponse.get("items").toString()).nextValue();

                JSONArray items = new JSONArray();
                if (jsonTokneritems instanceof JSONObject) {
                    JSONObject obj = reportResponse.getJSONObject("items");

                    items.put(obj);

                } else {
                    items = reportResponse.getJSONArray("items");

                }

                JSONObject data = null;

                for (int i = 0; i < items.length(); i++) {

                    EmployeeDetailsBean allEmployeesBean = new EmployeeDetailsBean();
                    data = items.getJSONObject(i);
//                    String userName = data.get("UserName").toString();

                    allEmployeesBean.setUserName(data.get("UserName").toString());
                    allEmployeesBean.setPersonNumber(data.get("PersonNumber").toString());

                    empUserNames.add(allEmployeesBean);
                    count++;
                }
                Object hasMore2
                        = new JSONTokener(reportResponse.get("hasMore").toString()).nextValue();
                offsetResult = offsetResult + 100;

                if (hasMore2.toString() == "false") {
                    checkHasMore = "false";
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(STCValidationIgateREST.class.getName()).log(Level.SEVERE, null, ex);

        }

        return empUserNames;

    }

    public static void getAndInsertAllTasks(String companyName) throws AppsProException, Exception {

        InboxOperationsService operationsService = new InboxOperationsService();
        QueryTasksDao queryTasksDao = new QueryTasksDao();

        ArrayList<UpdateAppsproDBBean> fusionApplicationTasks = getAllEmpsTasks(companyName);

        // add new tasks
//        for (int i = 0; i < fusionApplicationTasks.size(); i++) {
//
//            System.out.println("New task ==> " + fusionApplicationTasks.get(i).getTaskNumber());
//
//            queryTasksDao.postNewTasks(fusionApplicationTasks.get(i));
//
//        }

    }

    public static void compareCurrentAndLastFiredDate(String startDate,String companyName) throws Exception {

        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        SimpleDateFormat newDFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

        DateTime dtStart = formatter.parseDateTime(startDate);
        DateTime dtStop = formatter.parseDateTime(newDFormat.format(ts));

        int timeDiffInSecs = Seconds.secondsBetween(dtStart, dtStop).getSeconds();

        System.out.println("=============================================================");
        System.out.println("Start Date = " + dtStart);
        System.out.println("End Date = " + dtStop);

        System.out.println("Seconds difference = " + timeDiffInSecs);

        try {
            JobExecutionBean jobExecutionBean = new JobExecutionBean();

            jobExecutionBean.setJobExecutionTime(newDFormat.format(ts));
            QueryTasksDao queryTasksDao = new QueryTasksDao();
            queryTasksDao.insertJobExecutionTime(jobExecutionBean);

            getAndInsertUpdatedTasks(dtStart, dtStop,companyName);

            //add , update or delete task
        } catch (AppsProException ex) {
            Logger.getLogger(GetAndCompareTasks.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void insertTasksFirsJobtExecution(String companyName) throws Exception {

        try {
            QueryTasksDao queryTasksDao = new QueryTasksDao();
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            SimpleDateFormat newDFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            System.out.println(newDFormat.format(ts));

            JobExecutionBean jobExecutionBean = new JobExecutionBean();

            jobExecutionBean.setJobExecutionTime(newDFormat.format(ts));

            queryTasksDao.insertJobExecutionTime(jobExecutionBean);

            getAndInsertAllTasks(companyName);

        } catch (AppsProException ex) {
            Logger.getLogger(GetAndCompareTasks.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void jobExecutionFun(String companyName) throws Exception {

        try {

            QueryTasksDao queryTasksDao = new QueryTasksDao();

            JobExecutionBean jobExeTime = queryTasksDao.getLastFiredJob();

            if (jobExeTime.getJobExecutionTime() == null) {
                //it is a new project and we will import all of the tasks
                System.out.println("Null");
                GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();
                getAndCompareTasks.insertTasksFirsJobtExecution(companyName);

            } else {
                //import the updated tasks
                System.out.println("Not Null");
                GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();

//                getAndCompareTasks.compareCurrentAndLastFiredDate(jobExeTime.getJobExecutionTime());
                getAndCompareTasks.compareCurrentAndLastFiredDate(jobExeTime.getJobExecutionTime(),companyName);

            }
        } catch (AppsProException ex) {
            java.util.logging.Logger.getLogger(GetAndCompareTasks.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void getAndInsertUpdatedTasks(DateTime dtStart, DateTime dtStop, String companyName) throws AppsProException, Exception {

        InboxOperationsService operationsService = new InboxOperationsService();
        QueryTasksDao queryTasksDao = new QueryTasksDao();
        InboxOperationsService inboxOperationsService = new InboxOperationsService();

        ArrayList<UpdateAppsproDBBean> fusionApplicationTasks = getAllEmpsUpdatedTasks(dtStart, dtStop, companyName);

        System.out.println("SIZEEEEEEEEEEEEEe = " + fusionApplicationTasks.size());

        for (int i = 0; i < fusionApplicationTasks.size(); i++) {

            ArrayList<UpdateAppsproDBBean> dbTask = queryTasksDao.checkOldTasks(fusionApplicationTasks.get(i));

            if (dbTask.size() > 0) {

                System.out.println("update task stage  -- > " + fusionApplicationTasks.get(i).getTaskNumber());

                if (dbTask.get(dbTask.size() - 1).getUserName().equals(fusionApplicationTasks.get(i).getUserName())) {

                    //the below statement handle all of the rejected tasks wether complete or not because once the task rejected in the fusion it will be completed by default 
                    if ("TASK_VERSION_REASON_COMPLETED".equals(fusionApplicationTasks.get(i).getTaskVersionReason())  && !"ACCEPTED".equals(dbTask.get(dbTask.size() - 1).getTaskStatus()) && !"REJECTED".equals(dbTask.get(dbTask.size() - 1).getTaskStatus())) {

                        System.out.println("update last status in inbox api  -- > " + fusionApplicationTasks.get(i).getTaskNumber());

                        //will update the inbox with the and change the status from pending to approved
                        try {
                            inboxOperationsService.callUpdateInbox(fusionApplicationTasks.get(i).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_COMPLETED", fusionApplicationTasks.get(i).getTaskOutcome(), fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        try {
                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(fusionApplicationTasks.get(i).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_COMPLETED", fusionApplicationTasks.get(i).getTaskOutcome(), fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    } else {

//                        System.out.println("No need for update");
                    }

                } else {
                    //all the cases should be handled here

                    System.out.println("this task navigated from the original user  -- > " + fusionApplicationTasks.get(i).getTaskNumber());

                    if ("TASK_VERSION_REASON_ROUTED".equals(fusionApplicationTasks.get(i).getTaskVersionReason())) {

                        try {
                            inboxOperationsService.callUpdateInbox(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_ROUTED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        try {
                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_ROUTED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));
                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        //update all tasks table 
                    } else if ("TASK_VERSION_REASON_DELEGATED".equals(fusionApplicationTasks.get(i).getTaskVersionReason())) {

                        //pending create the delegate object
                        System.out.println("THE TASK WILL BE DELEGATED AND PREPARE THE OBJECT");
                        System.out.println("THE ORIGINAL USER : " + fusionApplicationTasks.get(i).getOriginalAssigneeUser());
                        try {
                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(fusionApplicationTasks.get(i).getOriginalAssigneeUser(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_DELEGATED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));
                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                        try {
                            inboxOperationsService.callUpdateInbox(fusionApplicationTasks.get(i).getOriginalAssigneeUser(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_DELEGATED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {

                        try {

                            inboxOperationsService.callUpdateInbox(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                        try {

                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));
                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }

                }

            } else {
                try {
                    queryTasksDao.postNewTasks(fusionApplicationTasks.get(i));
                } catch (Exception e) {

                    e.printStackTrace();

                }

                // create inbox item handled and will work once the server is working and they added the requirements from their side ( Form Type )
                try {
                    operationsService.callCreateInbox(fusionApplicationTasks.get(i));
                } catch (Exception e) {

                    e.printStackTrace();

                }
            }

        }

    }
    
    
    //The below method will be called for every navigation action in the custom bpm ( every action that will transfer the task or complete it )
    
    public static void getAndUpdateTasksFromCustomBPM(String userName, String taskNumber, String companyName) throws AppsProException, Exception {

        InboxOperationsService operationsService = new InboxOperationsService();
        QueryTasksDao queryTasksDao = new QueryTasksDao();
        InboxOperationsService inboxOperationsService = new InboxOperationsService();

        ArrayList<UpdateAppsproDBBean> fusionApplicationTasks = getUpdatedTasksQueryFilterByTaskNumber(userName, taskNumber, companyName);

        System.out.println("SIZEEEEEEEEEEEEEe = " + fusionApplicationTasks.size());

        for (int i = 0; i < fusionApplicationTasks.size(); i++) {

            ArrayList<UpdateAppsproDBBean> dbTask = queryTasksDao.checkOldTasks(fusionApplicationTasks.get(i));

            if (dbTask.size() > 0) {

                System.out.println("update task stage  -- > " + fusionApplicationTasks.get(i).getTaskNumber());

                if (dbTask.get(dbTask.size() - 1).getUserName().equals(fusionApplicationTasks.get(i).getUserName())) {

                    //the below statement handle all of the rejected tasks wether complete or not because once the task rejected in the fusion it will be completed by default 
                    if ("TASK_VERSION_REASON_COMPLETED".equals(fusionApplicationTasks.get(i).getTaskVersionReason())) {

                        System.out.println("update last status in inbox api  -- > " + fusionApplicationTasks.get(i).getTaskNumber());

                        //will update the inbox with the and change the status from pending to approved
                        try {
                            inboxOperationsService.callUpdateInbox(fusionApplicationTasks.get(i).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_COMPLETED", fusionApplicationTasks.get(i).getTaskOutcome(), fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        try {
                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(fusionApplicationTasks.get(i).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_COMPLETED", fusionApplicationTasks.get(i).getTaskOutcome(), fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    } else {

                        System.out.println("No need for update");
                    }

                } else {
                    //all the cases should be handled here

                    System.out.println("this task navigated from the original user  -- > " + fusionApplicationTasks.get(i).getTaskNumber());

                    if ("TASK_VERSION_REASON_ROUTED".equals(fusionApplicationTasks.get(i).getTaskVersionReason())) {

                        try {
                            inboxOperationsService.callUpdateInbox(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_ROUTED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        try {
                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_ROUTED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));
                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        //update all tasks table 
                    } else if ("TASK_VERSION_REASON_DELEGATED".equals(fusionApplicationTasks.get(i).getTaskVersionReason())) {

                        //pending create the delegate object
                        System.out.println("THE TASK WILL BE DELEGATED AND PREPARE THE OBJECT");
                        System.out.println("THE ORIGINAL USER : " + fusionApplicationTasks.get(i).getOriginalAssigneeUser());
                        try {
                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(fusionApplicationTasks.get(i).getOriginalAssigneeUser(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_DELEGATED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));
                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                        try {
                            inboxOperationsService.callUpdateInbox(fusionApplicationTasks.get(i).getOriginalAssigneeUser(), fusionApplicationTasks.get(i).getUserName(), "TASK_VERSION_REASON_DELEGATED", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    } else {

                        try {

                            inboxOperationsService.callUpdateInbox(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber());

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                        try {

                            UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(dbTask.get(dbTask.size() - 1).getUserName(), fusionApplicationTasks.get(i).getUserName(), "", "APPROVE", fusionApplicationTasks.get(i).getTaskNumber(), fusionApplicationTasks.get(i));
                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }

                }

            } else {
                try {
                    queryTasksDao.postNewTasks(fusionApplicationTasks.get(i));
                } catch (Exception e) {

                    e.printStackTrace();

                }

                // create inbox item handled and will work once the server is working and they added the requirements from their side ( Form Type )
                try {
                    operationsService.callCreateInbox(fusionApplicationTasks.get(i));
                } catch (Exception e) {

                    e.printStackTrace();

                }
            }

        }

    }
    
    
    
    
    public static void main(String ar[]) throws AppsProException {
        
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        
        
        System.out.println("TS1 "+ts);
        
        
        System.out.println("TEST");
       System.out.println("TEST");
       System.out.println("TEST");
       System.out.println("TEST");
       System.out.println("TEST");
       System.out.println("TEST");
       System.out.println("TEST");
       System.out.println("TEST");
       
       Date date2 = new Date();
       
       Timestamp ts2 = new Timestamp(date2.getTime());
       
       System.out.println("TS2 "+ts2);
       
        
        
//        try {
//            //        DatabaseConnection databaseConnection = new DatabaseConnection();
////
////            Connection connection;
////
////    
////    connection = databaseConnection.getConnection();
////    
////    System.out.println(connection);
//
////            getAndUpdateTasksFromCustomBPM("stami@tawal.com.sa", "204924", "tawal");
//
//QueryTasksDao queryTasksDao = new QueryTasksDao();
//
//UpdateAppsproDBBean updateAppsproDBBean = new UpdateAppsproDBBean();
//updateAppsproDBBean.setTaskNumber("205039");
//
//
//ArrayList<UpdateAppsproDBBean> dbTask = queryTasksDao.checkOldTasks(updateAppsproDBBean);
//
//System.out.println(dbTask.get(0).getTaskStatus());
//
//if(!"ACCEPTED".equals(dbTask.get(0).getTaskStatus())){
//
//System.out.println("NOOOOOOOOOOOOOOO");
//
//}else{
//
//System.out.println("YESSSSSSSSSSSSS");
//
//}
//
//        } catch (Exception ex) {
//            Logger.getLogger(GetAndCompareTasks.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }

}
