package com.appspro.bpm.soap;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.EmployeeDetailsBean;
import com.appspro.bpm.bean.ReportDetailsBean;
import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.bpm.bean.TaskQueryServiceBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.bpm.payload.TaskQueryServicePayload;
import com.appspro.bpm.payload.TaskServicePortPayload;
import com.appspro.fusionsshr.inbox.InboxOperationsService;
import com.appspro.stc.biReport.BIReportModel;
import com.appspro.stc.dao.QueryTasksDao;
import com.appspro.stc.dao.SetupProjectDao;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import org.w3c.dom.Element;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import org.w3c.dom.Element;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;

import org.json.JSONObject;
import org.json.XML;

import org.w3c.dom.Document;

import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class TaskQueryService {

    public String soapRequest = null;
    public String soapTaskNumberRequest = null;
    public String username = "";
    public String password = "";
    public String endPointURL = "";
    public String hostName = "";
    public String tasksEndPointURI = "/bpm/api/4.0/tasks/";

    // get instance details from DB under companyName and pass this data in the constructor
    public TaskQueryService(String companyName) throws AppsProException {
        SetupProjectDao setupProjectDao = new SetupProjectDao();
        SetupProjectBean setupProjectBean = setupProjectDao.getByCompanyName(companyName);
        // check if data returned or not
        SetupProjectBean returnedBean = setupProjectBean.getAdminUsername()==null && getCompnayName(companyName) !=null ? setupProjectDao.getByCompanyName(getCompnayName(companyName)):setupProjectBean;
        // assign returned data from DB 
        this.username = returnedBean.getAdminUsername();
        this.password = returnedBean.getAdminPassword();
        this.endPointURL = returnedBean.getHostName() + "/bpm/api/4.0/tasks/";
        this.hostName = returnedBean.getHostName();
    }
    
    public String getCompnayName(String employeeName){
         String companyName="";
         String user=employeeName.length()>0 && employeeName!=null?employeeName:"null";
        boolean haveCompany=user.contains("@")?true:false;
        if(haveCompany){
         String textAfter=user.substring(user.indexOf('@') + 1, user.length());
         companyName=textAfter.contains(".")?textAfter.substring(0, textAfter.indexOf(".")):textAfter;
         return companyName;
        }else{
        return null;
        }
    }

    ////////////////////////
    public static void trustAllHosts() throws AppsProException {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts
                = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }
            }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            throw new AppsProException(e);
        }
    }

    public Document httpPost(String destUrl, String soapRequest) throws AppsProException, Exception {
        System.setProperty("DUseSunHttpHandler", "true");
//        System.out.println(soapRequest);

                        System.setProperty("https.proxyHost", "proxy.stc.com.sa");
            System.setProperty("https.proxyPort", "8080");
            System.setProperty("https.proxyUser", "aosalman.c@stc.com.sa");
            System.setProperty("https.proxyPassword", "oracle@1234");
        byte[] buffer = new byte[soapRequest.length()];
        buffer = soapRequest.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url
                = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https
                    = (HttpsURLConnection) url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection) url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
//        System.out.println("Auth = " + getAuth());
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);
        String myResponse = http.getResponseCode() + " " + http.getResponseMessage();
//        System.out.println("connection status: " + http.getResponseCode()
//                + "; connection response: "
//                + http.getResponseMessage());

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
//            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0) {
                response
                        = response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>")
                                + 15);
            }
//            System.out.println(response);
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder
                    = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);

            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();

            return doc;
        } else {
            return null;
//            System.out.println("Failed");
        }

//        return null;
    }

    public String httpPostTest(String destUrl, String soapRequest) throws AppsProException, Exception {
        JSONObject responseObject = new JSONObject();

        System.setProperty("DUseSunHttpHandler", "true");
        System.setProperty("https.proxyHost", "proxy.stc.com.sa");
        System.setProperty("https.proxyPort", "8080");
        System.setProperty("https.proxyUser", "aosalman.c@stc.com.sa");
        
        System.setProperty("https.proxyPassword", "oracle@1234");
        // reading soap and convert it to array of bytes
        byte[] buffer = new byte[soapRequest.length()];
        buffer = soapRequest.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        // url for connection
        java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        // check url's protocol
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https = (HttpsURLConnection) url.openConnection();
            http = https;
        } else {
            http = (HttpURLConnection) url.openConnection();
        }
        String SOAPAction = "";
        //add reuqest header
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        // Send post request
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);
        // print status and response 
        System.out.println("connection status: " + http.getResponseCode() + "; connection response: " + http.getResponseMessage());
        // define stream for response
        InputStream inputStream;
        if (http.getResponseCode() == 200) {
            inputStream = http.getInputStream();
        } else {
            inputStream = http.getErrorStream();
        }
        // store status code for request
        responseObject.put("responseCode", http.getResponseCode());
        if (inputStream != null) {
            InputStreamReader iReader = new InputStreamReader(inputStream);
            BufferedReader bReader = new BufferedReader(iReader);
            String line;
            String response = "";
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            iReader.close();
            bReader.close();
            inputStream.close();
            // store doc returned 
            responseObject.put("responseData", response);
        } else {
            responseObject.put("responseData", "error");
        }
        http.disconnect();
        return responseObject.toString();
    }
    
    public Document stringToDocument(String data) throws AppsProException, Exception{
      if (data.indexOf("<?xml") > 0) {
            data = data.substring(data.indexOf("<?xml"), data.indexOf("</env:Envelope>") + 15);
        }
        // make document for data
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
        fact.setNamespaceAware(true);
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(data));
        Document doc = builder.parse(src);  
        return doc;
    }
    
    public  String callGetRest(String serverUrl) throws AppsProException, IOException {
        StringBuffer response;
        try {
            System.setProperty("https.proxyHost", "proxy.stc.com.sa");
            System.setProperty("https.proxyPort", "8080");
            System.setProperty("https.proxyUser", "aosalman.c@stc.com.sa");
            System.setProperty("https.proxyPassword", "oracle@1234");
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization",
                    "Basic " + getAuth());
            http.setDoOutput(true);
            http.setRequestMethod("GET");
            if ("Not Found".equalsIgnoreCase(http.getResponseMessage()) 
                    || "Internal Server Error".equalsIgnoreCase(http.getResponseMessage())) {
                response = new StringBuffer("noTaskFound");
            } else {
                try (//        http.setRequestProperty("Content-Type", "application/octet-stream");
                        //            http.setRequestProperty("id", id);
                        BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
                    System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                    System.out.println("response message below");
                    String inputLine;
                    response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
//            JSONObject reportResponse = new JSONObject(response.toString());
                }
            }
            
        } catch (MalformedURLException e) {

            throw new AppsProException(e);

        }
        return response.toString();
    }
    
        public String callGetRest2(String serverUrl) throws AppsProException, IOException {
        StringBuffer response;
        
        int respCode;
        try {
//            System.setProperty("DUseSunHttpHandler", "true");


                        System.setProperty("https.proxyHost", "proxy.stc.com.sa");
            System.setProperty("https.proxyPort", "8080");
            System.setProperty("https.proxyUser", "aosalman.c@stc.com.sa");
            System.setProperty("https.proxyPassword", "oracle@1234");
            java.net.URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization",
                    "Basic " + DatatypeConverter.printBase64Binary(("stami@tawal.com.sa" + ":" + "Oracle@123").getBytes()));
            http.setDoOutput(true);
            http.setRequestMethod("GET");
            if ("Not Found".equalsIgnoreCase(http.getResponseMessage()) 
                    || "Internal Server Error".equalsIgnoreCase(http.getResponseMessage())) {
                response = new StringBuffer("noTaskFound");
            } else {
                try (//        http.setRequestProperty("Content-Type", "application/octet-stream");
                        //            http.setRequestProperty("id", id);
                        BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
                    System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                    System.out.println("response message below");
                    String inputLine;
                    response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
            JSONObject reportResponse = new JSONObject(response.toString());
            
            System.out.println(reportResponse);
                }
            }
            respCode = http.getResponseCode();
        } catch (MalformedURLException e) {

            throw new AppsProException(e);

        }
        return response.toString();
    }
        
        
                public int callGetRestInboxApi(String serverUrl , String hashed , String date) throws AppsProException, IOException {
        StringBuffer response;
        
        int respCode;
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url
                    = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            String SOAPAction = "";
            http.setRequestProperty("SOAPAction", SOAPAction);
            http.setRequestProperty("Authorization",
                    "Basic " + DatatypeConverter.printBase64Binary(("stami@tawal.com.sa" + ":" + "Oracle@123").getBytes()));
            http.setDoOutput(true);
            http.setRequestMethod("GET");
            http.addRequestProperty("Date", date);
            http.addRequestProperty("x-igate-client", hashed);
//            if ("Not Found".equalsIgnoreCase(http.getResponseMessage()) 
//                    || "Internal Server Error".equalsIgnoreCase(http.getResponseMessage())) {
//                response = new StringBuffer("noTaskFound");
//            } else {
//                try (//        http.setRequestProperty("Content-Type", "application/octet-stream");
//                        //            http.setRequestProperty("id", id);
//                        BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
//                    System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
//                    System.out.println("response message below");
//                    String inputLine;
//                    response = new StringBuffer();
//                    while ((inputLine = in.readLine()) != null) {
//                        response.append(inputLine);
//                    }
////            JSONObject reportResponse = new JSONObject(response.toString());
//                }
//            }
            respCode = http.getResponseCode();
        } catch (MalformedURLException e) {

            throw new AppsProException(e);

        }
        return respCode;
    }

    public String callGetAttachment(String serverUrl, String httpMethod) throws AppsProException, IOException {
        String encodedString;
        try {
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url
                    = new URL(null, serverUrl.replaceAll(" ", "%20"), new sun.net.www.protocol.https.Handler());
            System.out.println("URL --> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            http.setRequestProperty("Authorization",
                    "Basic " + getAuth());
            http.setDoOutput(true);
            http.setRequestMethod(httpMethod);
            http.setRequestProperty("Content-Type",
                    "application/json");
            if ("Not Found".equalsIgnoreCase(http.getResponseMessage()) 
                    || "Internal Server Error".equalsIgnoreCase(http.getResponseMessage())) {
                encodedString ="noTaskFound";
            } else {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
//Start
                    byte[] bytes = IOUtils.toByteArray(http.getInputStream());
                    encodedString = Base64.getEncoder().encodeToString(bytes);
                    System.out.println("encodedString == >  " + encodedString);
//end
                    System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                    System.out.println("response message below");
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }
            }
        } catch (MalformedURLException e) {

            throw new AppsProException(e);

        }
        return encodedString;
    }

    public String callDeleteAttachment(String serverUrl) throws AppsProException, IOException {
        try {

            System.setProperty("DUseSunHttpHandler", "true");

            java.net.URL url
                    = new URL(null, serverUrl.replaceAll(" ", "%20"), new sun.net.www.protocol.https.Handler());

            System.out.println("URL --> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https
                        = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }

            http.setRequestProperty("Authorization",
                    "Basic " + getAuth());
            http.setDoOutput(true);
            http.setRequestMethod("DELETE");
            http.setRequestProperty("Content-Type",
                    "application/json");
            StringBuffer response;
            String encodedString;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {

                System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                System.out.println("response message below");
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
//            JSONObject reportResponse = new JSONObject(response.toString());
            }

            return response.toString();

        } catch (MalformedURLException e) {

            throw new AppsProException(e);

        }
    }

    public String getAuth() {
        byte[] message = (username + ":" + password).getBytes();
//        byte[] message = ("HCMUser" + ":" + "Oracle@123").getBytes();    
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public String getEndpointURL() {

        return endPointURL;
    }

    public String getHostName() {

        return hostName;
    }

    public String getTasksEndPointURI() {

        return tasksEndPointURI;
    }

    public String getAdminUsername() {

        return username;
    }

    public String getAdminPassword() {

        return password;
    }


    public ArrayList<TaskQueryServiceBean> getAllTasksQuery(String employeeName) throws AppsProException {
        TaskQueryService obj = new TaskQueryService(employeeName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        String returnTokenFromAuth = "";
        String returnTokenFromWorkflowContext = "";
        String returnQueryList = "";
        TaskQueryServiceBean bean = null;
        ArrayList<TaskQueryServiceBean> list = new ArrayList<TaskQueryServiceBean>();
        Document doc;
        try {
            doc = obj.httpPost(obj2.getURL(employeeName), obj2.getAuthenticateSoapRequest(employeeName));

            returnTokenFromAuth = doc.getElementsByTagName("token").item(0).getTextContent();
            System.out.println("Authonticate : " + returnTokenFromAuth);

            doc = obj.httpPost(obj2.getURL(employeeName), obj2.getWorkflowContextSoapRequest(returnTokenFromAuth));

            returnTokenFromWorkflowContext = doc.getElementsByTagName("token").item(0).getTextContent();
            System.out.println("Workfolw Context : " + returnTokenFromWorkflowContext);

//            doc = obj.httpPost(obj2.getURL(), obj2.getQueryListSoapRequest(returnTokenFromWorkflowContext));
            NodeList nList = doc.getElementsByTagName("task");

            for (int i = 0; i < nList.getLength(); i++) {
                bean = new TaskQueryServiceBean();
                try {
                    bean.setTitle(doc.getElementsByTagName("title").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTitle(null);
                }
                try {
                    bean.setCreator(doc.getElementsByTagName("creator").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreator(null);
                }
                try {
                    bean.setPriority(doc.getElementsByTagName("priority").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setPriority(null);
                }
                try {
                    bean.setAssignedDate(doc.getElementsByTagName("assignedDate").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setAssignedDate(null);
                }
                try {
                    bean.setCreatedDate(doc.getElementsByTagName("createdDate").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreatedDate(null);
                }
                try {
                    bean.setState(doc.getElementsByTagName("state").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setState(null);
                }
                try {
                    bean.setAction(doc.getElementsByTagName("action").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setAction(null);
                }
                try {
                    bean.setDisplayName(doc.getElementsByTagName("displayName").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setDisplayName(null);
                }
                try {
                    bean.setTaskId(doc.getElementsByTagName("taskId").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }
                try {
                    bean.setTaskNumber(doc.getElementsByTagName("taskNumber").item(i).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
//            bean.setCategory(doc.getElementsByTagName("category").item(i).getTextContent());
//            bean.setDigitalSignatureRequired(doc.getElementsByTagName("digitalSignatureRequired").item(i).getTextContent());
//            bean.setNumberOfTimesModified(doc.getElementsByTagName("numberOfTimesModified").item(i).getTextContent());
//            bean.setPasswordRequiredOnUpdate(doc.getElementsByTagName("passwordRequiredOnUpdate").item(i).getTextContent());
//            bean.setTransactionId(doc.getElementsByTagName("TransactionId").item(i).getTextContent().isEmpty() ? "c": doc.getElementsByTagName("TransactionId").item(i).getTextContent());
//            bean.setRequestor(doc.getElementsByTagName("requestor").item(i).getTextContent());
//            bean.setIdentificationKey(doc.getElementsByTagName("identificationKey").item(i).getTextContent());
//            bean.setApplicationId(doc.getElementsByTagName("ApplicationId").item(i).getTextContent());
//            bean.setModuleGroup(doc.getElementsByTagName("ModuleGroup").item(i).getTextContent());
//            bean.setModuleIdentifier(doc.getElementsByTagName("ModuleIdentifier").item(i).getTextContent());
//            bean.setObjectId(doc.getElementsByTagName("ObjectId").item(i).getTextContent());
//            bean.setSubjectId(doc.getElementsByTagName("SubjectId").item(i).getTextContent());
//            bean.setReentryFunction(doc.getElementsByTagName("ReentryFunction").item(i).getTextContent());
//            bean.setSectionDisplayName(doc.getElementsByTagName("SectionDisplayName").item(i).getTextContent());
//            bean.setSensorProcessName(doc.getElementsByTagName("sensorProcessName").item(i).getTextContent());
//            bean.setSensorNameFromData(doc.getElementsByTagName("sensorNameFromData").item(i).getTextContent());
//            bean.setSensorDataReferenceCode(doc.getElementsByTagName("sensorDataReferenceCode").item(i).getTextContent());
                list.add(bean);
            }
        } catch (Exception ex) {
            throw new AppsProException(ex);
        }
        return list;

    }

    public TaskQueryServiceBean getTaskDetailsByNumber(String taskNumber, String employeeName) throws AppsProException {
        TaskQueryService obj = new TaskQueryService(employeeName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        TaskQueryServiceBean bean = null;
        Document doc = null;

        try {
            //            doc = obj.httpPost(obj2.getURL(), obj2.getTaskDetailsByNumber(taskNumber, employeeName));
            JSONObject detailsObject = new JSONObject(obj.httpPostTest(obj2.getURL(employeeName), obj2.getTaskDetailsByNumber(taskNumber, employeeName)));
            // check returned status from http request
            if ("200".equalsIgnoreCase(detailsObject.get("responseCode").toString())) {
                // assign returned data to document
                if(!"error".equalsIgnoreCase(detailsObject.get("responseData").toString())){
                 doc = stringToDocument(detailsObject.get("responseData").toString());   
                }
            } else {
                if (!"error".equalsIgnoreCase(detailsObject.get("responseData").toString())) {
                    doc = stringToDocument(detailsObject.get("responseData").toString());
                }
                String returnTokenFromAuth;
                if (doc != null && doc.getElementsByTagName("errorMessage").getLength() > 0) {
                    returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                } else if (doc != null && doc.getElementsByTagName("message").getLength() > 0) {
                    returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                } else {
                    returnTokenFromAuth = "This task can not be found";
                }
              bean = new TaskQueryServiceBean();
              bean.setErrorInTask(returnTokenFromAuth);
              doc=null;
            }
            if (doc != null) {
                NodeList taskList = doc.getElementsByTagName("task");

                bean = new TaskQueryServiceBean();

                Element element = (Element) taskList.item(0);

                try {

                    bean.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTitle(null);
                }
                try {
                    // search for creator name if mot found get display name
                    bean.setCreator(element.getElementsByTagName("creatorDisplayName").getLength()>0 ?
                            element.getElementsByTagName("creatorDisplayName").item(0).getTextContent():
                            element.getElementsByTagName("PersonName").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreator(null);
                }
                try {

                    bean.setIdentificationKey(element.getElementsByTagName("identificationKey").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setIdentificationKey(null);
                }

                NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");

                Element sysElement = (Element) sysAttributsList.item(0);

                try {

                    bean.setTaskId(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }
                try {

                    bean.setTaskNumber(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
                try {

                    bean.setWorkflowPattern(sysElement.getElementsByTagName("workflowPattern").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setWorkflowPattern(null);
                }
                try {

                    bean.setTaskNamespace(sysElement.getElementsByTagName("taskNamespace").item(0).getTextContent().toString().substring(sysElement.getElementsByTagName("taskNamespace").item(0).getTextContent().toString().lastIndexOf('/') + 1));
                } catch (NullPointerException e) {
                    bean.setTaskNamespace(null);
                }
                try {
                    bean.setUpdatedDate(sysElement.getElementsByTagName("updatedDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setUpdatedDate(null);
                }
                try {
                    bean.setTaskState(sysElement.getElementsByTagName("customActions").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskState(null);
                }
                try {
                    bean.setOriginalAssigneeUser(sysElement.getElementsByTagName("originalAssigneeUser").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setOriginalAssigneeUser(null);
                }
                try {
                    bean.setState(sysElement.getElementsByTagName("state").item(sysElement.getElementsByTagName("state").getLength()-1).getTextContent());
                } catch (NullPointerException e) {
                    bean.setState(null);
                }
                try {
                    bean.setCreatedDate(sysElement.getElementsByTagName("createdDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreatedDate(null);
                }
                

                NodeList assigneeUsersList = sysElement.getElementsByTagName("assigneeUsers");

                Element assigneeUsers = (Element) assigneeUsersList.item(0);

                try {
                    bean.setAssignee(assigneeUsers.getElementsByTagName("id").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setAssignee(null);
                }
            }
        }catch (Exception ex) {
            throw new AppsProException(ex);
        }

        return bean;
    }

    public ArrayList<TaskQueryServiceBean> getAllTasksQueryFilter(String assigenedFilter, String employeeName) throws AppsProException {

        TaskQueryService obj = new TaskQueryService(employeeName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        ArrayList<TaskQueryServiceBean> list = new ArrayList<TaskQueryServiceBean>();
        Document doc;
        try {

            doc = obj.httpPost(obj2.getURL(employeeName), obj2.getQueryListFilterSoapRequest(assigenedFilter, employeeName));
            NodeList tasksList = doc.getElementsByTagName("task");

            for (int i = 0; i < tasksList.getLength(); i++) {
                TaskQueryServiceBean bean = new TaskQueryServiceBean();

                Element element = (Element) tasksList.item(i);

                try {
                    bean.setCreator(element.getElementsByTagName("creator").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreator(null);
                }
                try {
                    bean.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTitle(null);
                }
                try {
                    bean.setPriority(element.getElementsByTagName("priority").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setPriority(null);
                }

                NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");
                Element sysElement = (Element) sysAttributsList.item(0);

                try {
                    bean.setTaskNumber(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
                try {
                    bean.setTaskId(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }
                try {
                    bean.setState(sysElement.getElementsByTagName("state").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setState(null);
                }
                try {
                    bean.setAssignedDate(sysElement.getElementsByTagName("assignedDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setAssignedDate(null);
                }
                try {
                    bean.setCreatedDate(sysElement.getElementsByTagName("createdDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreatedDate(null);
                }
                try {
                    bean.setUpdatedDate(sysElement.getElementsByTagName("updatedDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setUpdatedDate(null);
                }

                NodeList updateByList = sysElement.getElementsByTagName("updatedBy");

                Element updateByElement = (Element) updateByList.item(0);

                try {
                    bean.setUpdatedById(updateByElement.getElementsByTagName("id").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setUpdatedById(null);
                }
                try {
                    bean.setUpdatedByType(updateByElement.getElementsByTagName("type").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setUpdatedByType(null);
                }

                list.add(bean);
            }
        } catch (Exception ex) {
            throw new AppsProException(ex);
        }
        return list;

    }

    public static LinkedHashMap<String, List<String>> parseXml(String xml) throws XMLStreamException {
        StringBuilder content = null;
        LinkedHashMap<String, List<String>> dataMap = new LinkedHashMap<>();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        InputStream stream = new ByteArrayInputStream(xml.getBytes());
        XMLStreamReader reader = factory.createXMLStreamReader(stream);

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    content = new StringBuilder();
                    break;

                case XMLStreamConstants.CHARACTERS:
                    if (content != null) {
                        content.append(reader.getText().trim());
                    }
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (content != null) {
                        String leafText = content.toString();
                        if (dataMap.get(reader.getLocalName()) == null) {
                            List<String> values = new ArrayList<>();
                            values.add(leafText);
                            dataMap.put(reader.getLocalName(), values);
                        } else {
                            dataMap.get(reader.getLocalName()).add(leafText);
                        }
                    }
                    content = null;
                    break;

                case XMLStreamConstants.START_DOCUMENT:
                    break;
            }

        }

        return dataMap;
    }
// related to job 
    public static ArrayList<UpdateAppsproDBBean> getUpdatedTasksQueryFilterByPersonName(EmployeeDetailsBean allEmployeesBean, DateTime startTime, DateTime stopTime, String companyName) throws AppsProException {

        TaskQueryService obj = new TaskQueryService(companyName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        ArrayList<UpdateAppsproDBBean> list = new ArrayList<UpdateAppsproDBBean>();
        Document doc;
        try {
                Date date = new Date();
                 Timestamp ts = new Timestamp(date.getTime());

            

            doc = obj.httpPost(obj2.getURL(companyName), obj2.getLastUpdatedListFilterByPersonSoapRequest(allEmployeesBean.getUserName(), startTime, stopTime, companyName));
            
            Date date2 = new Date();
                 Timestamp ts2 = new Timestamp(date2.getTime());
            
            
            System.out.println("Request Start Time at : "+ts + "** Username : "+allEmployeesBean.getUserName()+" ** " +"Request End Time at : "+ts2);
            
            NodeList tasksList = doc.getElementsByTagName("task");

            for (int i = 0; i < tasksList.getLength(); i++) {
                UpdateAppsproDBBean bean = new UpdateAppsproDBBean();

                bean.setUserName(allEmployeesBean.getUserName());
                bean.setPersonNumber(allEmployeesBean.getPersonNumber());

                Element element = (Element) tasksList.item(i);

                try {
                    bean.setCreatorId(element.getElementsByTagName("creator").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreatorId(null);
                }
                try {
                    bean.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTitle(null);
                    continue;
                }

                NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");
                Element sysElement = (Element) sysAttributsList.item(0);

                try {
                    bean.setTaskNumber(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
                try {
                    bean.setTaskId(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }

                try {
                    bean.setWorkflowPattern(sysElement.getElementsByTagName("workflowPattern").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setWorkflowPattern(null);
                }
                NodeList assigneeUsersList = sysElement.getElementsByTagName("assigneeUsers");
                Element assigneeUsersElement = (Element) assigneeUsersList.item(0);

                try {
                    bean.setId(assigneeUsersElement.getElementsByTagName("id").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setId(null);
                }

                try {
                    bean.setTaskVersionReason(sysElement.getElementsByTagName("versionReason").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskVersionReason(null);
                }
                try {
                    bean.setOriginalAssigneeUser(sysElement.getElementsByTagName("originalAssigneeUser").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setOriginalAssigneeUser(null);
                }

                if (!bean.getId().toUpperCase().equals(allEmployeesBean.getUserName().toUpperCase())) {

                    continue;

                }
                try {
                    bean.setTaskOutcome(sysElement.getElementsByTagName("outcome").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskOutcome(null);
                }

                list.add(bean);
            }
        } catch (Exception ex) {
            Logger.getLogger(TaskQueryService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    public Map<String, String> getTaskDetailsByNumberDynamic(String taskNumber, ReportDetailsBean reportDetailsBean, String employeeName) throws AppsProException {
        TaskQueryService obj = new TaskQueryService(employeeName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        Map<String, String> paramMap = new HashMap<String, String>();
        Document doc;
        try {
            doc = obj.httpPost(obj2.getURL(employeeName), obj2.getTaskDetailsByNumber(taskNumber, employeeName));
            if (reportDetailsBean.getReportParameters().size()>0 && doc != null) {
                NodeList tasksList = doc.getElementsByTagName("task");
                Element element = (Element) tasksList.item(0);
                for (int i = 0; i < reportDetailsBean.getReportParameters().size(); i++) {
                    if ("true".equalsIgnoreCase(reportDetailsBean.getReportParameters().get(i).getMappedParameter()) || "false".equalsIgnoreCase(reportDetailsBean.getReportParameters().get(i).getMappedParameter())) {
                        paramMap.put(reportDetailsBean.getReportParameters().get(i).getReportParameter(), reportDetailsBean.getReportParameters().get(i).getMappedParameter());
                    } else {
                        paramMap.put(reportDetailsBean.getReportParameters().get(i).getReportParameter(), element.getElementsByTagName(reportDetailsBean.getReportParameters().get(i).getMappedParameter()).item(0).getTextContent());
                    }

                }
            } else {
                if (doc != null & doc.getElementsByTagName("notificationDetail").getLength() > 0) {
                    System.out.println(doc.getElementsByTagName("notificationDetail").item(0).getTextContent());
                    paramMap.put("otherData", doc.getElementsByTagName("notificationDetail").item(0).getTextContent());
                } else {
                    return null;
                }
            }
        } catch (Exception ex) {
            throw new AppsProException(ex);
        }
        return paramMap;
    }
// related to job
    public static UpdateAppsproDBBean getUpdatedTaskOutcom(UpdateAppsproDBBean updateAppsproDBBean,String companyName) throws AppsProException {

        TaskQueryService obj = new TaskQueryService(companyName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();

        UpdateAppsproDBBean bean = new UpdateAppsproDBBean();
        try {

            Document doc2 = obj.httpPost(obj2.getURL(companyName), obj2.getTaskHistoryForSingleTask(updateAppsproDBBean.getUserName(), updateAppsproDBBean.getTaskId(),companyName));

            NodeList taskHistoryFullList = null;
            try {

                taskHistoryFullList = doc2.getElementsByTagName("taskListResponse");

            } catch (NullPointerException e) {

                e.printStackTrace();

            }

            if (taskHistoryFullList != null) {

                Element taskHistoryFullElement = (Element) taskHistoryFullList.item(0);

                NodeList taskHistoryList = taskHistoryFullElement.getElementsByTagName("task");

                for (int j = taskHistoryList.getLength() - 1; j >= 0; j--) {

                    Element taskHistoryElement = (Element) taskHistoryList.item(j);

                    NodeList sysAttributesHistoryList = taskHistoryElement.getElementsByTagName("systemAttributes");

                    Element sysAttributesHistoryElement = (Element) sysAttributesHistoryList.item(0);

                    NodeList assigneeUsersHistoryList = sysAttributesHistoryElement.getElementsByTagName("assigneeUsers");

                    if (assigneeUsersHistoryList.getLength() < 1) {
                        continue;

                    }

                    Element assigneeUsersHistoryElement = (Element) assigneeUsersHistoryList.item(0);

                    if (assigneeUsersHistoryElement.getElementsByTagName("id").item(0).getTextContent().toUpperCase().equals(updateAppsproDBBean.getUserName().toUpperCase())) {

                        try {
                            bean.setUpdatedDate(sysAttributesHistoryElement.getElementsByTagName("updatedDate").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setUpdatedDate(null);
                        }

                        try {
                            bean.setTaskVersion(sysAttributesHistoryElement.getElementsByTagName("version").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setTaskVersion(null);
                        }
                        try {
                            bean.setTaskVersionReason(sysAttributesHistoryElement.getElementsByTagName("versionReason").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setTaskVersionReason(null);
                        }
                        try {
                            bean.setTaskOutcome(sysAttributesHistoryElement.getElementsByTagName("outcome").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setTaskOutcome(null);
                        }
                        try {
                            bean.setTaskStatus(sysAttributesHistoryElement.getElementsByTagName("state").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setTaskStatus(null);
                        }
                        try {
                            bean.setSubState(sysAttributesHistoryElement.getElementsByTagName("substate").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setSubState(null);
                        }

                        NodeList updatedByHistoryList = sysAttributesHistoryElement.getElementsByTagName("updatedBy");
                        Element updatedByHistoryElement = (Element) updatedByHistoryList.item(0);

                        try {
                            bean.setUpdatedBy(updatedByHistoryElement.getElementsByTagName("id").item(0).getTextContent());
                        } catch (NullPointerException e) {
                            bean.setUpdatedBy(null);
                        }

                        break;

                    }

                }
            }

        } catch (Exception ex) {
            Logger.getLogger(TaskQueryService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bean;

    }
// related to job
    public static ArrayList<UpdateAppsproDBBean> getAllTasksQueryFilterByPersonNameFinalWithHistory(EmployeeDetailsBean allEmployeesBean,String companyName) throws AppsProException, Exception {
        TaskQueryService obj = new TaskQueryService(companyName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        ArrayList<UpdateAppsproDBBean> list = new ArrayList<UpdateAppsproDBBean>();
        Document doc;
        try {

            doc = obj.httpPost(obj2.getURL(companyName), obj2.getQueryListFilterByPersonSoapRequest(allEmployeesBean.getUserName(),companyName));
            NodeList tasksList = doc.getElementsByTagName("task");

            for (int i = 0; i < tasksList.getLength(); i++) {
                UpdateAppsproDBBean bean = new UpdateAppsproDBBean();

                bean.setUserName(allEmployeesBean.getUserName());
                bean.setPersonNumber(allEmployeesBean.getPersonNumber());

                Element element = (Element) tasksList.item(i);

                try {
                    bean.setCreatorId(element.getElementsByTagName("creator").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreatorId(null);
                }
                try {
                    bean.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTitle(null);
                    continue;
                }

                NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");
                Element sysElement = (Element) sysAttributsList.item(0);

                try {
                    bean.setTaskNumber(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
                try {
                    bean.setTaskId(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }

                try {
                    bean.setCreationDate(sysElement.getElementsByTagName("createdDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreationDate(null);
                }

                try {
                    bean.setAssignedDate(sysElement.getElementsByTagName("assignedDate").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setAssignedDate(null);
                }
                try {
                    bean.setWorkflowPattern(sysElement.getElementsByTagName("workflowPattern").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setWorkflowPattern(null);
                }

                Document doc2 = obj.httpPost(obj2.getURL(companyName), obj2.getTaskHistoryForSingleTask(bean.getUserName(), bean.getTaskId(),companyName));

                NodeList taskHistoryFullList = null;
                try {

                    taskHistoryFullList = doc2.getElementsByTagName("taskListResponse");

                } catch (NullPointerException e) {

                    e.printStackTrace();

                }

                if (taskHistoryFullList != null) {

                    Element taskHistoryFullElement = (Element) taskHistoryFullList.item(0);

                    NodeList taskHistoryList = taskHistoryFullElement.getElementsByTagName("task");

                    for (int j = taskHistoryList.getLength() - 1; j >= 0; j--) {

                        Element taskHistoryElement = (Element) taskHistoryList.item(j);

                        NodeList sysAttributesHistoryList = taskHistoryElement.getElementsByTagName("systemAttributes");

                        Element sysAttributesHistoryElement = (Element) sysAttributesHistoryList.item(0);

                        NodeList assigneeUsersHistoryList = sysAttributesHistoryElement.getElementsByTagName("assigneeUsers");

                        if (assigneeUsersHistoryList.getLength() < 1) {
                            continue;

                        }

                        Element assigneeUsersHistoryElement = (Element) assigneeUsersHistoryList.item(0);

                        if (assigneeUsersHistoryElement.getElementsByTagName("id").item(0).getTextContent().toUpperCase().equals(allEmployeesBean.getUserName().toUpperCase())) {

                            try {
                                bean.setUpdatedDate(sysAttributesHistoryElement.getElementsByTagName("updatedDate").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setUpdatedDate(null);
                            }

                            try {
                                bean.setTaskVersion(sysAttributesHistoryElement.getElementsByTagName("version").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setTaskVersion(null);
                            }
                            try {
                                bean.setTaskVersionReason(sysAttributesHistoryElement.getElementsByTagName("versionReason").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setTaskVersionReason(null);
                            }
                            try {
                                bean.setTaskOutcome(sysAttributesHistoryElement.getElementsByTagName("outcome").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setTaskOutcome(null);
                            }
                            try {
                                bean.setTaskStatus(sysAttributesHistoryElement.getElementsByTagName("state").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setTaskStatus(null);
                            }
                            try {
                                bean.setSubState(sysAttributesHistoryElement.getElementsByTagName("substate").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setSubState(null);
                            }

                            NodeList updatedByHistoryList = sysAttributesHistoryElement.getElementsByTagName("updatedBy");
                            Element updatedByHistoryElement = (Element) updatedByHistoryList.item(0);

                            try {
                                bean.setUpdatedBy(updatedByHistoryElement.getElementsByTagName("id").item(0).getTextContent());
                            } catch (NullPointerException e) {
                                bean.setUpdatedBy(null);
                            }
                            break;

                        }

                    }

                }

                list.add(bean);
            }

        } catch (Exception ex) {
            throw new AppsProException(ex);
        }
  
        return list;
    }
    
    public void updateTaskDetailsMySql(String taskId, String employeeName) throws AppsProException {
        // get updated last state of the task 
        UpdateAppsproDBBean bBean = new UpdateAppsproDBBean();
        UpdateAppsproDBBean appsproDBBean = new UpdateAppsproDBBean();
        bBean.setTaskId(taskId);
        bBean.setUserName(employeeName);
        appsproDBBean = getUpdatedTaskOutcom(bBean,"");
        if (appsproDBBean != null) {
            QueryTasksDao queryTasksDao = new QueryTasksDao();
            //update task details in my sql
            queryTasksDao.updateTaskOutcome(appsproDBBean);
        }
    }
    
    
    public static UpdateAppsproDBBean getLastTwoStagesInHistory(String userName , String taskId ) throws AppsProException, Exception {

        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        TaskQueryService obj = new TaskQueryService("tawal");
        UpdateAppsproDBBean bean = new UpdateAppsproDBBean();

//        bean.setUserName("stami@tawal.com.sa");
//        bean.setPersonNumber("1000");

        Document doc2 = obj.httpPost(obj2.getURL("tawal"), obj2.getTaskHistoryForSingleTask(userName, taskId, "tawal"));

        NodeList taskHistoryFullList = null;
        try {

            taskHistoryFullList = doc2.getElementsByTagName("taskListResponse");

        } catch (NullPointerException e) {

            e.printStackTrace();

        }

        if (taskHistoryFullList != null) {

            Element taskHistoryFullElement = (Element) taskHistoryFullList.item(0);

            NodeList taskHistoryList = taskHistoryFullElement.getElementsByTagName("task");

            for (int j = taskHistoryList.getLength() - 1; j >= 0; j--) {

                Element taskHistoryElement = (Element) taskHistoryList.item(j);

                NodeList sysAttributesHistoryList = taskHistoryElement.getElementsByTagName("systemAttributes");

                Element sysAttributesHistoryElement = (Element) sysAttributesHistoryList.item(0);

                NodeList assigneeUsersHistoryList = sysAttributesHistoryElement.getElementsByTagName("assigneeUsers");

                if (assigneeUsersHistoryList.getLength() < 1) {
                    continue;

                }

                Element assigneeUsersHistoryElement = (Element) assigneeUsersHistoryList.item(0);

                if (assigneeUsersHistoryElement.getElementsByTagName("id").item(0).getTextContent().toUpperCase().equals(userName.toUpperCase())) {

                  

                    
                    try {
                        bean.setTaskVersionReason(sysAttributesHistoryElement.getElementsByTagName("versionReason").item(0).getTextContent());

                        System.out.println(sysAttributesHistoryElement.getElementsByTagName("versionReason").item(0).getTextContent());
               
                    } catch (NullPointerException e) {
                        bean.setTaskVersionReason(null);
                    }
                    try {
                        bean.setTaskOutcome(sysAttributesHistoryElement.getElementsByTagName("outcome").item(0).getTextContent());
                        
                        System.out.println(sysAttributesHistoryElement.getElementsByTagName("outcome").item(0).getTextContent());
                    } catch (NullPointerException e) {
                        bean.setTaskOutcome(null);
                    }

                    break;

                }

            }

        }
        
        return bean;

    }
    
    public static ArrayList<UpdateAppsproDBBean> getAllTasksQueryFilterByPersonNameFinalWithHistoryTest2(EmployeeDetailsBean allEmployeesBean, String companyName) throws AppsProException, Exception {
        TaskQueryService obj = new TaskQueryService(companyName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        ArrayList<UpdateAppsproDBBean> list = new ArrayList<UpdateAppsproDBBean>();
        Document doc;
        try {

            doc = obj.httpPost(obj2.getURL(companyName), obj2.getQueryListFilterByPersonSoapRequest(allEmployeesBean.getUserName(), companyName));
            NodeList tasksList = doc.getElementsByTagName("task");

            System.out.println("tasksList = " + tasksList.getLength());

            for (int i = 0; i < tasksList.getLength(); i++) {
                UpdateAppsproDBBean bean = new UpdateAppsproDBBean();

                bean.setUserName(allEmployeesBean.getUserName());
                bean.setPersonNumber(allEmployeesBean.getPersonNumber());

                Element element = (Element) tasksList.item(i);

                NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");
                Element sysElement = (Element) sysAttributsList.item(0);

                try {
                    bean.setTaskNumber(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());

                    System.out.println("***** " + bean.getTaskNumber() + " *****");

                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
                try {
                    bean.setTaskId(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                    System.out.println("tasksID = " + bean.getTaskId());

                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }

                Document doc2 = obj.httpPost(obj2.getURL(companyName), obj2.getTaskHistoryForSingleTask(bean.getUserName(), bean.getTaskId(), companyName));

                NodeList taskHistoryFullList = null;
                try {

                    taskHistoryFullList = doc2.getElementsByTagName("taskListResponse");

                } catch (NullPointerException e) {

                    e.printStackTrace();

                }

                if (taskHistoryFullList != null) {

                    Element taskHistoryFullElement = (Element) taskHistoryFullList.item(0);

                    NodeList taskHistoryList = taskHistoryFullElement.getElementsByTagName("task");

                    for (int j = 0; j < taskHistoryList.getLength(); j++) {

                        Element taskHistoryElement = (Element) taskHistoryList.item(j);

                        NodeList sysAttributesHistoryList = taskHistoryElement.getElementsByTagName("systemAttributes");

                        Element sysAttributesHistoryElement = (Element) sysAttributesHistoryList.item(0);

                        NodeList assigneeUsersHistoryList = sysAttributesHistoryElement.getElementsByTagName("assigneeUsers");

                        if (assigneeUsersHistoryList.getLength() < 1) {
                            continue;

                        }

                        //start check if it is the first stage of the history to create a new task
                        if (j == 0) {

                            // create new inbox and new row in the db
                            UpdateAppsproDBBean createInboxBean = new UpdateAppsproDBBean();

                            //the below is the creator
                            NodeList updatedByHistoryList = sysAttributesHistoryElement.getElementsByTagName("updatedBy");
                            Element updatedByHistoryElement = (Element) updatedByHistoryList.item(0);

                            try {
                                createInboxBean.setCreatorId(updatedByHistoryElement.getElementsByTagName("id").item(0).getTextContent());

                                System.out.println("the creator = " + createInboxBean.getCreatorId());
                            } catch (NullPointerException e) {
                                createInboxBean.setCreatorId(null);
                            }

                            Element assigneeUsersHistoryElement = (Element) assigneeUsersHistoryList.item(0);

                            try {
                                createInboxBean.setUserName(assigneeUsersHistoryElement.getElementsByTagName("id").item(0).getTextContent());

                                System.out.println("the current user = " + createInboxBean.getUserName());
                            } catch (NullPointerException e) {
                                createInboxBean.setUpdatedBy(null);
                            }

                            try {
                                createInboxBean.setTaskStatus(sysAttributesHistoryElement.getElementsByTagName("state").item(0).getTextContent());
                                System.out.println("the current Status = " + createInboxBean.getTaskStatus());
                            } catch (NullPointerException e) {
                                createInboxBean.setTaskStatus(null);
                            }
                            try {
                                createInboxBean.setTaskNumber(bean.getTaskNumber());

                            } catch (NullPointerException e) {
                                createInboxBean.setTaskNumber(null);
                            }

                            //then call create inbox api 
                            InboxOperationsService inboxOperationsService = new InboxOperationsService();
                            QueryTasksDao queryTasksDao = new QueryTasksDao();
                            try {

                                queryTasksDao.postNewTasks(createInboxBean);
                            } catch (Exception e) {

                                e.printStackTrace();
                            }

                            try {
//                                inboxOperationsService.callCreateInbox(createInboxBean);

                            } catch (Exception e) {

                                e.printStackTrace();
                            }

                        } else {

                            QueryTasksDao queryTasksDao = new QueryTasksDao();
                            InboxOperationsService inboxOperationsService = new InboxOperationsService();
                            UpdateAppsproDBBean currentUserBean = new UpdateAppsproDBBean();
                            UpdateAppsproDBBean prevUserBean = new UpdateAppsproDBBean();
                            //Current user secction
                            Element assigneeUsersHistoryElement = (Element) assigneeUsersHistoryList.item(0);

                            try {
                                currentUserBean.setUserName(assigneeUsersHistoryElement.getElementsByTagName("id").item(0).getTextContent());

                            } catch (NullPointerException e) {
                                currentUserBean.setUserName(null);
                            }

                            System.out.println(i);
                            System.out.println("Current user bean : " + currentUserBean.getUserName());

                            try {
                                currentUserBean.setTaskVersionReason(sysAttributesHistoryElement.getElementsByTagName("versionReason").item(0).getTextContent());

                                System.out.println("***** " + currentUserBean.getTaskVersionReason() + " *****");

                            } catch (NullPointerException e) {
                                bean.setTaskNumber(null);
                            }

                            //Current user secction
                            //previous user secction   
                            Element taskHistoryElementPrev = (Element) taskHistoryList.item(j - 1);

                            NodeList sysAttributesHistoryListPrev = taskHistoryElementPrev.getElementsByTagName("systemAttributes");

                            Element sysAttributesHistoryElementPrev = (Element) sysAttributesHistoryListPrev.item(0);

                            NodeList assigneeUsersHistoryListPrev = sysAttributesHistoryElementPrev.getElementsByTagName("assigneeUsers");

                            Element assigneeUsersHistoryElementPrev = (Element) assigneeUsersHistoryListPrev.item(0);

                            try {
                                prevUserBean.setUserName(assigneeUsersHistoryElementPrev.getElementsByTagName("id").item(0).getTextContent());

                            } catch (NullPointerException e) {
                                prevUserBean.setUserName(null);
                            }

                            System.out.println("previous user bean : " + prevUserBean.getUserName());

                            try {
                                prevUserBean.setTaskOutcome(sysAttributesHistoryElementPrev.getElementsByTagName("outcome").item(0).getTextContent());

                            } catch (NullPointerException e) {
                                prevUserBean.setTaskOutcome(null);
                            }

                            //previous user secction
                            if (currentUserBean.getUserName().toUpperCase().equals(prevUserBean.getUserName().toUpperCase())) {

                                if ("TASK_VERSION_REASON_COMPLETED".equals(currentUserBean.getTaskVersionReason())) {

                                } else {

                                    System.out.println("no need for update");
                                }
                            } else {

                                System.out.println("the task need for update --> navigated");
                                if ("TASK_VERSION_REASON_ROUTED".equals(currentUserBean.getTaskVersionReason())) {

                                    System.out.println("TASK_VERSION_REASON_ROUTED" + "***** OUTCOME" + prevUserBean.getTaskOutcome() + " *****");
                                    try {
                                        queryTasksDao.updateTasks(prevUserBean.getUserName(), currentUserBean.getUserName(), "TASK_VERSION_REASON_ROUTED", prevUserBean.getTaskOutcome(), bean.getTaskNumber(), prevUserBean);
                                    } catch (Exception e) {

                                        e.printStackTrace();
                                    }
                                    try {
//                                        inboxOperationsService.callUpdateInbox(prevUserBean.getUserName(), currentUserBean.getUserName(), "TASK_VERSION_REASON_ROUTED", prevUserBean.getTaskOutcome(), bean.getTaskNumber());
                                    } catch (Exception e) {

                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        UpdateAppsproDBBean updateDb = queryTasksDao.updateTasks(prevUserBean.getUserName(), currentUserBean.getUserName(), "", "APPROVE", bean.getTaskNumber(), currentUserBean);

                                    } catch (Exception e) {

                                        e.printStackTrace();
                                    }
                                    try {
//                                        inboxOperationsService.callUpdateInbox(prevUserBean.getUserName(), currentUserBean.getUserName(), "", "APPROVE", bean.getTaskNumber());

                                    } catch (Exception e) {

                                        e.printStackTrace();
                                    }

                                }

                            }

                        }

                    }

                }
                System.out.println("================================================================================");
                list.add(bean);
            }

        } catch (Exception ex) {
            throw new AppsProException(ex);
        }

        return list;
    }
    
        public static ArrayList<UpdateAppsproDBBean> getUpdatedTasksQueryFilterByTaskNumber(String userName, String taskNumber, String companyName) throws AppsProException {

        TaskQueryService obj = new TaskQueryService(companyName);
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();
        ArrayList<UpdateAppsproDBBean> list = new ArrayList<UpdateAppsproDBBean>();
        Document doc;
        try {

            doc = obj.httpPost(obj2.getURL(companyName), obj2.getUpdatedTasksQueryFilterByTaskNumberSoapRequest(userName,taskNumber, companyName));
            NodeList tasksList = doc.getElementsByTagName("task");
            
            System.out.println(doc);
            

            for (int i = 0; i < tasksList.getLength(); i++) {
                UpdateAppsproDBBean bean = new UpdateAppsproDBBean();

                

                Element element = (Element) tasksList.item(i);

                try {
                    bean.setCreatorId(element.getElementsByTagName("creator").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setCreatorId(null);
                }
                try {
                    bean.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTitle(null);
                    continue;
                }

                NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");
                Element sysElement = (Element) sysAttributsList.item(0);

                try {
                    bean.setTaskNumber(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskNumber(null);
                }
                try {
                    bean.setTaskId(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskId(null);
                }

                try {
                    bean.setWorkflowPattern(sysElement.getElementsByTagName("workflowPattern").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setWorkflowPattern(null);
                }
                NodeList assigneeUsersList = sysElement.getElementsByTagName("assigneeUsers");
                Element assigneeUsersElement = (Element) assigneeUsersList.item(0);

                try {
                    bean.setId(assigneeUsersElement.getElementsByTagName("id").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setId(null);
                }
                try {
                    bean.setUserName(assigneeUsersElement.getElementsByTagName("id").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setUserName(null);
                }

                try {
                    bean.setTaskVersionReason(sysElement.getElementsByTagName("versionReason").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskVersionReason(null);
                }
                try {
                    bean.setOriginalAssigneeUser(sysElement.getElementsByTagName("originalAssigneeUser").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setOriginalAssigneeUser(null);
                }

//                if (!bean.getId().toUpperCase().equals(allEmployeesBean.getUserName().toUpperCase())) {
//
//                    continue;
//
//                }
                try {
                    bean.setTaskOutcome(sysElement.getElementsByTagName("outcome").item(0).getTextContent());
                } catch (NullPointerException e) {
                    bean.setTaskOutcome(null);
                }

                list.add(bean);
            }
        } catch (Exception ex) {
            Logger.getLogger(TaskQueryService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }
    
    
    
    public static void main(String[] args) throws AppsProException, IOException {
        
        
     ArrayList<UpdateAppsproDBBean> aList=   getUpdatedTasksQueryFilterByTaskNumber("stami@tawal.com.sa","205151","tawal");
     
     for(int i = 0 ; i< aList.size() ; i++){
     
     
     System.out.print("TASKKKKKK VERSION"+aList.get(i).getTaskVersionReason());
     
     }
     
     
    }

}
