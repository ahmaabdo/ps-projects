
package com.appspro.bpm.bean;


public class ReportParametersBean {
  
    private String reportParameter;
    private String mappedParameter;
    
    public String getReportParameter() {
        return reportParameter;
    }

    public void setReportParameter(String reportParameter) {
        this.reportParameter = reportParameter;
    }

    public String getMappedParameter() {
        return mappedParameter;
    }

    public void setMappedParameter(String mappedParameter) {
        this.mappedParameter = mappedParameter;
    }
}
