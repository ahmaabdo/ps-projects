package com.appspro.bpm.bean;

import java.util.ArrayList;

public class TasksLogBean {

    private String taskNumber;
    private String previousApprover;
    private String nextApprover;
    private String companyName;
    private String status;
    private String systemName;
    private String type;
    private String display_status;
    private String action;
    private String delegatee;
    private ArrayList<TaskLogDetailsBean> taskDetails;
    private TaskLogDetailsBean taskLogDetails;

    public TaskLogDetailsBean getTaskLogDetails() {
        return taskLogDetails;
    }

    public void setTaskLogDetails(TaskLogDetailsBean taskLogDetails) {
        this.taskLogDetails = taskLogDetails;
    }

    public String getPreviousApprover() {
        return previousApprover;
    }

    public void setPreviousApprover(String previousApprover) {
        this.previousApprover = previousApprover;
    }

    public String getNextApprover() {
        return nextApprover;
    }

    public void setNextApprover(String nextApprover) {
        this.nextApprover = nextApprover;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplay_status() {
        return display_status;
    }

    public void setDisplay_status(String display_status) {
        this.display_status = display_status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDelegatee() {
        return delegatee;
    }

    public void setDelegatee(String delegatee) {
        this.delegatee = delegatee;
    }

    public ArrayList<TaskLogDetailsBean> getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(ArrayList<TaskLogDetailsBean> taskDetails) {
        this.taskDetails = taskDetails;
    }

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }


}
