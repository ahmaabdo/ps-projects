package com.appspro.bpm.payload;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.ReportDetailsBean;
import com.appspro.bpm.bean.TaskQueryServiceBean;
import com.appspro.bpm.bean.TasksQueryBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.bpm.soap.TaskQueryService;
import com.appspro.stc.biReport.BIReportModel;
import com.appspro.stc.dao.SetupProjectDao;

import org.w3c.dom.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TaskQueryServicePayload {

    public String soapRequest = null;
    //STC Auth
//    public static String username = "HCMUser";
//    public static String password = "Welcome@123";
//    private final String URL = "https://ejvm-dev1.fa.em2.oraclecloud.com:443/integration/services/TaskQueryService/TaskQueryService";
    //AEC Auth

//    TaskQueryService taskQuerService = new TaskQueryService();
//    public static String username = "2554";
//    public static String password = "HHA_123456";

//    private final String URL = taskQuerService.getHostName() + "/integration/services/TaskQueryService/TaskQueryService";

    public String getAuthenticateSoapRequest(String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest
                = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <com:credential>\n"
                + "         <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "         <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "      </com:credential>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String getWorkflowContextSoapRequest(String token) {
        this.soapRequest
                = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:workflowContextRequest>\n"
                + "         <tas:token>" + token + "</tas:token>\n"
                + "      </tas:workflowContextRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String getQueryListFilterSoapRequest(String assignmentFilter,String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\">\"\n"
                + "                  <soapenv:Header/>\n"
                + "                  <soapenv:Body>\n"
                + "                     <tas:taskListRequest>\n"
                + "                        <com:workflowContext>\n"
                + "                           <com:credential>\n"
                + "                              <com:login>" + taskQuerService.getAdminUsername()+ "</com:login>\n"
                + "                              <com:password>" + taskQuerService.getAdminPassword()+ "</com:password>\n"
                + "               <com:onBehalfOfUser>" + employeeName +"</com:onBehalfOfUser>\n"
                + "                           </com:credential>\n"
                + "                        </com:workflowContext>\n"
                + "                        <tas1:taskPredicateQuery>\n"
                + "                           <tas1:predicate>\n"
                + "                              <tas1:assignmentFilter>" + assignmentFilter + "</tas1:assignmentFilter>\n"
                + "                           </tas1:predicate>\n"
                + "                        </tas1:taskPredicateQuery>\n"
                + "                     </tas:taskListRequest>\n"
                + "                  </soapenv:Body>\n"
                + "               </soapenv:Envelope>";
        return soapRequest;
    }

    public String getTaskDetailsByNumber(String taskNumber,String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:taskDetailsByNumberRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskNumber>" + taskNumber + "</tas:taskNumber>\n"
                + "      </tas:taskDetailsByNumberRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String getTaskPatternDetails(String taskId,String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\" xmlns:tas3=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:getTaskSequenceRequest>\n"
                + "   <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" +  employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:getTaskSequenceRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String getURL(String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        String URL = taskQuerService.getHostName() + "/integration/services/TaskQueryService/TaskQueryService";
        return URL;
    }
    //related to job
    public String getQueryListFilterByPersonSoapRequest(String personName, String companyName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(companyName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:taskListRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + personName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas1:taskPredicateQuery>\n"

                + "            <tas1:predicate>\n"
                + "               <tas1:assignmentFilter>My</tas1:assignmentFilter>\n"
                + "               <tas1:predicate>\n"
                + "                  <tas1:clause>\n"
                + "                     <tas1:column tableName=\"WFTask\">\n"
                + "                        <tas1:columnName>state</tas1:columnName>\n"
                + "                     </tas1:column>\n"
                + "                     <tas1:operator>IN</tas1:operator>\n"
                + "                     <tas1:valueList>\n"
                + "                        <tas1:value>ASSIGNED</tas1:value>\n"
                + "                        <tas1:value>SUSPENDED</tas1:value>\n"
                + "                        <tas1:value>WITHDRAW</tas1:value>\n"
                + "                     </tas1:valueList>\n"
                + "                  </tas1:clause>\n"
                + "               </tas1:predicate>\n"
                + "            </tas1:predicate>\n"
                + "         </tas1:taskPredicateQuery>"
                + "      </tas:taskListRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }
// related to job
        public String getLastUpdatedListFilterByPersonSoapRequest(String personName, DateTime startDate, DateTime stopDate, String companyName) throws AppsProException {

        TaskQueryService taskQuerService = new TaskQueryService(companyName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:taskListRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "               <com:onBehalfOfUser>" + personName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas1:taskPredicateQuery>\n"
                + "<tas1:displayColumnList>\n"
                + "<tas1:displayColumn>versionReason</tas1:displayColumn>\n"
                + "</tas1:displayColumnList>\n"
                + "            <tas1:predicate>\n"
                + "               <tas1:assignmentFilter>My</tas1:assignmentFilter>\n"
                + "               <tas1:predicate>\n"
                + "                  <tas1:clause>\n"
                + "                     <tas1:column tableName=\"WFTask\">\n"
                + "                        <tas1:columnName>updatedDate</tas1:columnName>\n"
                + "                     </tas1:column>\n"
                + "                     <tas1:operator>Before</tas1:operator>\n"
                + "                     <tas1:dateValue>" + stopDate + "</tas1:dateValue>\n"
                + //stop time
                "                  </tas1:clause>\n"
                + "				      <tas1:clause joinOperator=\"AND\">\n"
                + "                     <tas1:column tableName=\"WFTask\">\n"
                + "                        <tas1:columnName>updatedDate</tas1:columnName>\n"
                + "                     </tas1:column>\n"
                + "                     <tas1:operator>After</tas1:operator>\n"
                + "                     <tas1:dateValue>" + startDate + "</tas1:dateValue>\n"
                + //start time
                "                  </tas1:clause>\n"
                + "               </tas1:predicate>\n"
                + "            </tas1:predicate>	\n"
                + "         </tas1:taskPredicateQuery>\n"
                + "      </tas:taskListRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public static boolean hasAttribute(Element element, String value) {
        NamedNodeMap attributes = element.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Node node = attributes.item(i);
            if (value.equals(node.getNodeValue())) {
                return true;
            }
        }
        return false;
    }
    
    // related to job
    public String getUpdatedTaskOutcomSoapRequest(UpdateAppsproDBBean updateAppsproDBBean) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService("");
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:taskListRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>"+taskQuerService.getAdminUsername()+"</com:login>\n"
                + "               <com:password>"+taskQuerService.getAdminPassword()+"</com:password>\n"
                + "               <com:onBehalfOfUser>"+updateAppsproDBBean.getUserName()+"</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas1:taskPredicateQuery>\n"
                                              +"<tas1:optionalInfoList>\n"
                 +"<tas1:taskOptionalInfo>ShortHistory</tas1:taskOptionalInfo>\n"
                 +"</tas1:optionalInfoList>\n"
                + "            <tas1:predicate>\n"
                + "               <tas1:assignmentFilter>All</tas1:assignmentFilter>\n"
                + "               <tas1:predicate>\n"
                + "                  <tas1:clause>\n"
                + "                     <tas1:column tableName=\"WFTask\">\n"
                + "                        <tas1:columnName>taskNumber</tas1:columnName>\n"
                + "                     </tas1:column>\n"
                + "                     <tas1:operator>EQ</tas1:operator>\n"
                + "                     <tas1:value>"+updateAppsproDBBean.getTaskNumber()+"</tas1:value>\n"
                + "                  </tas1:clause>\n"
                + "               </tas1:predicate>\n"
                + "            </tas1:predicate>\n"
                + "         </tas1:taskPredicateQuery>\n"
                + "      </tas:taskListRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }
    
    // related to job
    public String getTaskSequenseForSingleTask(String userName , String taskId) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService("");
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\" xmlns:tas3=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:getTaskSequenceRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "               <com:onBehalfOfUser>" + userName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:getTaskSequenceRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }
    // related to job
        public String getTaskHistoryForSingleTask(String userName , String taskId,String companyName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(companyName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\" xmlns:tas3=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:taskHistoryRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "               <com:onBehalfOfUser>" + userName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:taskHistoryRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }
        
    public String getUpdatedTasksQueryFilterByTaskNumberSoapRequest(String userName, String taskNumber, String companyName) throws AppsProException {

        TaskQueryService taskQuerService = new TaskQueryService(companyName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskQueryService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/taskQuery\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:taskListRequest>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>"+taskQuerService.getAdminUsername()+"</com:login>\n"
                + "               <com:password>"+taskQuerService.getAdminPassword()+"</com:password>\n"
                + "               <com:onBehalfOfUser>"+userName+"</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas1:taskPredicateQuery>\n"
                + "            <tas1:displayColumnList>\n"
                + "               <tas1:displayColumn>versionReason</tas1:displayColumn>\n"
                + "            </tas1:displayColumnList>\n"
                + "            <tas1:predicate>\n"
                + "               <tas1:assignmentFilter>All</tas1:assignmentFilter>\n"
                + "               <tas1:predicate>\n"
                + "                  <tas1:clause>\n"
                + "                     <tas1:column tableName=\"WFTask\">\n"
                + "                        <tas1:columnName>taskNumber</tas1:columnName>\n"
                + "                     </tas1:column>\n"
                + "                     <tas1:operator>EQ</tas1:operator>\n"
                + "                     <tas1:value>"+taskNumber+"</tas1:value>\n"
                + "                  </tas1:clause>\n"
                + "               </tas1:predicate>\n"
                + "            </tas1:predicate>\n"
                + "         </tas1:taskPredicateQuery>\n"
                + "      </tas:taskListRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    
    public static void main(String[] args) throws AppsProException {

        TaskQueryService taskQueryObj2 = new TaskQueryService("");
        ArrayList<TasksQueryBean> list = new ArrayList<TasksQueryBean>();
        TaskQueryService obj = new TaskQueryService("");
        TaskQueryServicePayload obj2 = new TaskQueryServicePayload();

        Document doc=null;

        try {
//            doc = obj.httpPost(obj2.getURL(), obj2.getQueryListFilterSoapRequest("My","employeeName"));
            JSONObject object = new JSONObject(obj.httpPostTest(obj2.getURL(""), obj2.getQueryListFilterSoapRequest("My", "employeeName")));
            if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                NodeList tasksList = doc.getElementsByTagName("task");

                for (int i = 0; i < tasksList.getLength(); i++) {

                    Element element = (Element) tasksList.item(i);

                    System.out.println(element.getElementsByTagName("creator").item(0).getTextContent());
                    System.out.println(element.getElementsByTagName("title").item(0).getTextContent());
                    System.out.println(element.getElementsByTagName("priority").item(0).getTextContent());

                    //system attributes
                    NodeList sysAttributsList = element.getElementsByTagName("systemAttributes");

                    Element sysElement = (Element) sysAttributsList.item(0);

                    System.out.println(sysElement.getElementsByTagName("taskNumber").item(0).getTextContent());
                    System.out.println(sysElement.getElementsByTagName("taskId").item(0).getTextContent());
                    System.out.println(sysElement.getElementsByTagName("state").item(0).getTextContent());
                    System.out.println(sysElement.getElementsByTagName("assignedDate").item(0).getTextContent());
                    System.out.println(sysElement.getElementsByTagName("createdDate").item(0).getTextContent());
                    System.out.println(sysElement.getElementsByTagName("updatedDate").item(0).getTextContent());

                    //system attributes
                    //updated by & data 
                    NodeList updateByList = sysElement.getElementsByTagName("updatedBy");

                    Element updateByElement = (Element) updateByList.item(0);

                    System.out.println(updateByElement.getElementsByTagName("id").item(0).getTextContent());
                    System.out.println(updateByElement.getElementsByTagName("type").item(0).getTextContent());

                    //updated by & data 
                    System.out.println("=========================================================");

                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TaskQueryServicePayload.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
   

}
