/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bpm.bean;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class ReportDetailsBean {

    private String taskNameSpace;
    private String reportPath;
    private String reportName;
    private String reportTitle;
    private ArrayList<ReportParametersBean> reportParameters;

    public ArrayList<ReportParametersBean> getReportParameters() {
        return reportParameters;
    }

    public void setReportParameters(ArrayList<ReportParametersBean> reportParameters) {
        this.reportParameters = reportParameters;
    }

    public String getTaskNameSpace() {
        return taskNameSpace;
    }

    public void setTaskNameSpace(String taskNameSpace) {
        this.taskNameSpace = taskNameSpace;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

 

}
