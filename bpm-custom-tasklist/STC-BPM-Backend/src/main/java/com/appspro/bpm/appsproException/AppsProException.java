/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bpm.appsproException;

import com.appspro.stc.database.DatabaseConnection;

/**
 *
 * @author Maha 
 */



public class AppsProException extends Exception {
   public AppsProException(Throwable throwable) {
       super(throwable);
       //Insert database log
       DatabaseConnection.log.error("ERROR", throwable);
       throwable.printStackTrace();
   }
   public AppsProException(String message,Throwable throwable) {
       super(throwable);
       DatabaseConnection.log.error("ERROR", throwable);
       throwable.printStackTrace();
   }
   public AppsProException(String message){
           super(message);
       }
}