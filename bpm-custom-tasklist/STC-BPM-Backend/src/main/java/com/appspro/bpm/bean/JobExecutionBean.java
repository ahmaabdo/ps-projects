/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bpm.bean;

/**
 *
 * @author user
 */
public class JobExecutionBean {
    
    private String id;
    private String jobExecutionTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobExecutionTime() {
        return jobExecutionTime;
    }

    public void setJobExecutionTime(String jobExecutionTime) {
        this.jobExecutionTime = jobExecutionTime;
    }
    
    
    
}
