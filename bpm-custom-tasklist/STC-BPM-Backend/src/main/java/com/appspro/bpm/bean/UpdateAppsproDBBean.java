/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bpm.bean;

/**
 *
 * @author user
 */
public class UpdateAppsproDBBean {

    private String taskNumber;
    private String taskId;
    private String userName;
    private String taskStatus;
    private String creationDate;
    private String creatorId;
    private String updatedDate;
    private String updatedBy;
    private String personNumber;
    private String taskExistFlag;
    private String assignedDate;
    private String workflowPattern;
    private String taskOutcome;
    private String taskVersion;
    private String taskVersionReason;
    private String title;
    private String historyState;
    private String updatedByHistory;
    private String id;
    private String subState;
    private String originalAssigneeUser;

    public String getTaskExistFlag() {
        return taskExistFlag;
    }

    public void setTaskExistFlag(String taskExistFlag) {
        this.taskExistFlag = taskExistFlag;
    }

    UpdateAppsproDBBean(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public UpdateAppsproDBBean() {
        //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof UpdateAppsproDBBean) {
            UpdateAppsproDBBean ptr = (UpdateAppsproDBBean) v;
            retVal = (ptr.taskNumber == null ? this.taskNumber == null : ptr.taskNumber.equals(this.taskNumber));
        }

        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.taskNumber != null ? this.taskNumber.hashCode() : 0);
        return hash;
    }
    



    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(String assignedDate) {
        this.assignedDate = assignedDate;
    }

    public String getWorkflowPattern() {
        return workflowPattern;
    }

    public void setWorkflowPattern(String workflowPattern) {
        this.workflowPattern = workflowPattern;
    }

    public String getTaskOutcome() {
        return taskOutcome;
    }

    public void setTaskOutcome(String taskOutcome) {
        this.taskOutcome = taskOutcome;
    }

    public String getTaskVersion() {
        return taskVersion;
    }

    public void setTaskVersion(String taskVersion) {
        this.taskVersion = taskVersion;
    }

    public String getTaskVersionReason() {
        return taskVersionReason;
    }

    public void setTaskVersionReason(String taskVersionReason) {
        this.taskVersionReason = taskVersionReason;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHistoryState() {
        return historyState;
    }

    public void setHistoryState(String historyState) {
        this.historyState = historyState;
    }

    public String getUpdatedByHistory() {
        return updatedByHistory;
    }

    public void setUpdatedByHistory(String updatedByHistory) {
        this.updatedByHistory = updatedByHistory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubState() {
        return subState;
    }

    public void setSubState(String subState) {
        this.subState = subState;
    }

    public String getOriginalAssigneeUser() {
        return originalAssigneeUser;
    }

    public void setOriginalAssigneeUser(String originalAssigneeUser) {
        this.originalAssigneeUser = originalAssigneeUser;
    }
    
    
    

}
