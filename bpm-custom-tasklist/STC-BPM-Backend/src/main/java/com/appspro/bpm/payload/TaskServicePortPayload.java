/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bpm.payload;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.soap.TaskQueryService;
import com.appspro.fusionsshr.rest.STCValidationIgateREST;
import com.appspro.stc.biReport.BIReportModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;

/**
 *
 * @author Shadi Mansi-PC
 */
public class TaskServicePortPayload {

//    TaskQueryService taskQuerService = new TaskQueryService();
    public String soapRequest = null;
    private String taskId = null;
    private String outcome = null;
//    private final String URL = taskQuerService.getHostName() + "/integration/services/TaskService/TaskServicePort";

//        public static String username = "2554";
//    public static String password = "HHA_123456";
    public String getUpdateTaskOutcomeSoapRequest(String taskId, String outcome, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest
                = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:updateTaskOutcome>\n"
                + "         <!--Optional:-->\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "         <tas:outcome>" + outcome + "</tas:outcome>\n"
                + "      </tas:updateTaskOutcome>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String withDrawSoapRequest(String taskId) {
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:withdrawTask>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:withdrawTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;

    }

    public String suspendTaskSoapRequest(String taskId) {
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:suspendTask>        \n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:suspendTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;

    }

    public String getReassignSoapRequest(JSONArray jsRassignArr, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);

        String isGroup = null;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < jsRassignArr.length(); i++) {

            JSONObject lineJO = new JSONObject(jsRassignArr.get(i).toString());

            if (lineJO.get("scope").equals("user")) {
                isGroup = "false";
            } else {
                isGroup = "true";
            }

            String addStr = "            <tas:taskAssignee isGroup=\"" + isGroup + "\" type=\"" + lineJO.get("scope") + "\">" + lineJO.get("userName") + "</tas:taskAssignee>\n";
            sb.append(addStr);

        }

        JSONObject lineJOId = new JSONObject(jsRassignArr.get(0).toString());

        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:reassignTask>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + lineJOId.get("taskId") + "</tas:taskId>\n"
                + "         <tas:taskAssignees>\n"
                + sb.toString()
                + "         </tas:taskAssignees>\n"
                + "      </tas:reassignTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String getDelegateSoapRequest(JSONArray jsRassignArr, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < jsRassignArr.length(); i++) {

            JSONObject lineJO = new JSONObject(jsRassignArr.get(i).toString());

            String addStr = "            <tas:taskAssignee isGroup=\"false\" type=\"user\">" + lineJO.get("userName") + "</tas:taskAssignee>\n";
            sb.append(addStr);
        }

        JSONObject lineJOId = new JSONObject(jsRassignArr.get(0).toString());

        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:delegateTask>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + lineJOId.get("taskId") + "</tas:taskId>\n"
                + "         <tas:taskAssignees>\n"
                + sb.toString()
                + "         </tas:taskAssignees>\n"
                + "      </tas:delegateTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String getEscalateTaskSoapRequest(String taskId, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:escalateTask>   \n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>       \n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:escalateTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String getAddCommentSoapRequest(String taskId, String Comment, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:addComment>         \n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>    \n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "         <tas:comment>" + Comment + "</tas:comment>\n"
                + "      </tas:addComment>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String getAddAttachmentSoapRequest(String taskId, String encodedAttachment, String attachmentName, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:addAttachment>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "         <task:attachment>\n"
                + "            <task:content>" + encodedAttachment + "</task:content>\n"
                //                    + "            <task:mimeType>"+contentType+"</task:mimeType>\n"
                + "            <task:name>" + attachmentName + "</task:name>\n"
                + "            <task:taskId>" + taskId + "</task:taskId>\n"
                + "            <task:isContentEncoded>true</task:isContentEncoded>\n"
                + "         </task:attachment>\n"
                + "      </tas:addAttachment>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String getRequestMoreInfoSoapRequest(String taskId, String userComment, String assigneeId, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:requestInfoForTask>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <task:task>\n"
                + "            <task:userComment>\n"
                + "               <task:comment>" + userComment + "</task:comment>\n"
                + "            </task:userComment>\n"
                + "            <task:systemAttributes>\n"
                + "               <task:isGroup>false</task:isGroup>\n"
                + "               <task:taskId>" + taskId + "</task:taskId>\n"
                + "               <task:assignees>\n"
                + "                  <task:id>" + assigneeId + "</task:id>\n"
                + "               </task:assignees>\n"
                + "            </task:systemAttributes>\n"
                + "         </task:task>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "         <tas:taskAssignee isGroup=\"false\" type=\"user\">" + assigneeId + "</tas:taskAssignee>\n"
                + "      </tas:requestInfoForTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String getAddAttachmentByURLSoapRequest(String taskId, String attachmentURL, String attachmentName, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:addAttachment>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "<com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "         <task:attachment>          \n"
                + "            <task:name>" + attachmentName + "</task:name>\n"
                + "            <task:taskId>" + taskId + "</task:taskId>\n"
                + "		<task:URI>" + attachmentURL + "</task:URI>\n"
                + "         </task:attachment>\n"
                + "      </tas:addAttachment>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";

        return soapRequest;

    }

    public String resumeTaskSoapRequest(String taskId, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:resumeTask>\n"
                + "         <com:workflowContext>\n"
                + "            <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "               <com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:resumeTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String releaseTaskSoapRequest(String taskId, String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        this.soapRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tas=\"http://xmlns.oracle.com/bpel/workflow/taskService\" xmlns:com=\"http://xmlns.oracle.com/bpel/workflow/common\" xmlns:task=\"http://xmlns.oracle.com/bpel/workflow/task\" xmlns:tas1=\"http://xmlns.oracle.com/bpel/workflow/TaskEvidenceService\" xmlns:tas2=\"http://xmlns.oracle.com/bpel/workflow/taskError\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <tas:releaseTask>\n"
                + "         <com:workflowContext>\n"
                + "               <com:credential>\n"
                + "               <com:login>" + taskQuerService.getAdminUsername() + "</com:login>\n"
                + "               <com:password>" + taskQuerService.getAdminPassword() + "</com:password>\n"
                + "               <com:onBehalfOfUser>" + employeeName + "</com:onBehalfOfUser>\n"
                + "            </com:credential>\n"
                + "         </com:workflowContext>\n"
                + "         <tas:taskId>" + taskId + "</tas:taskId>\n"
                + "      </tas:releaseTask>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        return soapRequest;
    }

    public String getURL(String employeeName) throws AppsProException {
        TaskQueryService taskQuerService = new TaskQueryService(employeeName);
        String URL = taskQuerService.getHostName() + "/integration/services/TaskService/TaskServicePort";
        return URL;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getOutcome() {
        return outcome;
    }

    public static void main(String[] args) {
//        
//        String reportResponse = null;
//        
//        Map<String, String> paramMap = new HashMap<String, String>();
//        
//        
//        paramMap.put("bindTransactionDetailsLink", "300000003313182");
//                           paramMap.put("pAbsenceEntryID", "300000003313182");
//            
//                           reportResponse =BIReportModel.runReport(BIReportModel.REPORT_NAME.AbsenceApprovalNotificationReport.getValue(),
//                                        paramMap);
//                           
//                         System.out.print(reportResponse);

    }
}
