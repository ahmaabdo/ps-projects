/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.ReportDetailsBean;
import com.appspro.bpm.bean.ReportParametersBean;
import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.bpm.bean.TaskQueryServiceBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.bpm.payload.TaskQueryServicePayload;
import com.appspro.bpm.payload.TaskServicePortPayload;
import com.appspro.bpm.soap.TaskQueryService;
import static com.appspro.bpm.soap.TaskQueryService.parseXml;
import static com.appspro.bpm.soap.TaskQueryService.trustAllHosts;
import com.appspro.bpm.updateappsprodb.GetAndCompareTasks;
import com.appspro.stc.biReport.BIReportModel;
import com.appspro.stc.dao.QueryTasksDao;
import com.appspro.stc.dao.SetupProjectDao;
import com.appspro.stc.database.DatabaseConnection;
import com.appspro.stc.igate.STCValidationIgate;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.stream.XMLStreamException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * REST Web Service
 *
 * @author Shadi Mansi-PC
 */
@Path("/bpmWorklist")
public class STCValidationIgateREST {

//    TaskQueryService taskQueryObj = new TaskQueryService();
    ArrayList<TaskQueryServiceBean> listQuery = new ArrayList<TaskQueryServiceBean>();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of STCValidationIgateREST
     */
    public STCValidationIgateREST() {
    }

    /**
     * Retrieves representation of an instance of
     * com.appspro.fusionsshr.rest.STCValidationIgateREST
     *
     * @return an instance of java.lang.String
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getJWTAuthentication(@Context HttpHeaders httpHeaders, String jwtToken) throws AppsProException {
        
        try {

            JSONObject payload = new JSONObject(jwtToken);
            STCValidationIgate stcValidation = new STCValidationIgate();
            if (payload.has("username") && payload.has("language") && payload.has("jwtToken")) {
                int status = stcValidation.ValidateJWT(payload.get("username").toString(), payload.get("language").toString(), payload.get("jwtToken").toString());

                if (status == 204) {
                    System.out.print("created");
                    //TODO Shady   
                    TaskQueryService taskQueryObj = new TaskQueryService(payload.getString("employeeName"));
                    
                    // check if their is service datails returned from DB under this user or not
                    if(taskQueryObj.getAdminUsername() != null){
                     listQuery = taskQueryObj.getAllTasksQuery(payload.getString("employeeName"));   
                      String message = "Valid Token";
                    return new JSONArray(listQuery).toString();
                    }else{
                       return "CompanyName is not correct";  
                    }
                    
                } else {
                    System.out.print("not valid");
                    return "UNAUTHORIZED";
                }
            } else {
                return "Missing data in payload";
            }
        } catch (Exception e) {
            throw new AppsProException(e);
        }
    }

    
    @POST
        @Path("/asFilter")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAssigneeFilter(@Context HttpHeaders httpHeaders, String AssignedFilter) throws AppsProException {
        
        ArrayList<TaskQueryServiceBean> listQueryFilter = new ArrayList<TaskQueryServiceBean>();
        JSONObject payload = new JSONObject(AssignedFilter);
        if (payload.has("AssignedFilter") && payload.has("employeeName")) {
            if (payload.get("employeeName").toString().length() > 0 && payload.get("employeeName").toString() != "null") {

                TaskQueryService taskQueryObj2 = new TaskQueryService(payload.getString("employeeName"));

                // check if their is service datails returned from DB under this user or not
                if (taskQueryObj2.getAdminUsername() != null) {
                    listQueryFilter = taskQueryObj2.getAllTasksQueryFilter(payload.getString("AssignedFilter"), payload.getString("employeeName"));
                    return new JSONArray(listQueryFilter).toString();
                } else {
                    return "CompanyName is not correct";
                }

            } else {
                return "Pelease check your user";
            }
        } else {
            return "Missing payload data";
        }
    }

//    @GET
//    @Path("/{taskNumber}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public String getTaskDetailsREST(@PathParam("taskNumber") String taskNumber) {
//        TaskQueryServiceBean bean = new TaskQueryServiceBean();
//        try {
//            bean = taskQueryObj.getTaskDetailsByNumber(taskNumber);
//            return new JSONObject(bean).toString();
//        } catch (Exception e) {
//            System.out.println(e);
////           return Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode(), e.getMessage()).build();
//            String message = "There was an internal server error";
//            return "INTERNAL_SERVER_ERROR";
//        }
//
//    }
    //// check on bean data
    @POST
    @Path("/taskNumber")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskDetails(@Context HttpHeaders httpHeaders, String jwtToken) throws AppsProException {
        TaskQueryServiceBean bean = new TaskQueryServiceBean();
        
        try {
            JSONObject payload = new JSONObject(jwtToken);
            STCValidationIgate stcValidation = new STCValidationIgate();
            if (payload.has("username") && payload.has("language") && payload.has("jwtToken")) {
//                int status = stcValidation.ValidateJWT("shrkhan.c@stc.com.sa", payload.get("language").toString(), payload.get("jwtToken").toString());
                int status = 204;
                if (status == 204) {
                    // check user and task number first
                    String employee=payload.get("employeeName").toString() !="null" ? payload.get("employeeName").toString():"null";
                    String taskNumber = payload.get("taskNumber").toString() !="null" ? payload.get("taskNumber").toString():"null";
                   
                    TaskQueryService taskQueryObj = new TaskQueryService(employee);
                    // check if their is service datails returned from DB under this user or not
                    if(taskQueryObj.getAdminUsername() != null){
                    bean = taskQueryObj.getTaskDetailsByNumber(taskNumber,employee);
                    }else{
                       bean.setErrorInTask("CompanyName is not correct");  
                    }
                    
                } else {
                    System.out.print("not valid");
                    bean.setAction("UNAUTHORIZED");
                    bean.setErrorInTask("Authorization failed");
                }
            } else {
                bean.setErrorInTask("There was an internal server error please check your data");
            }
        } catch (Exception e) {
           throw new AppsProException(e);
        }
        return new JSONObject(bean).toString();
    }

    @POST
    @Path("/updateTaskOutcome")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String updateTaskOutcomeREST(@Context HttpHeaders httpHeaders, String body) throws AppsProException {
        
        TaskServicePortPayload bean = new TaskServicePortPayload();
        TaskQueryServiceBean queryServiceBean = new TaskQueryServiceBean();
        JSONObject response = new JSONObject();
        
        try {
            Document doc = null;
            JSONObject payload = new JSONObject(body);
            // check payload has all data needed or not
            if (payload.has("taskId") && payload.has("outcome") && payload.has("employeeName") && payload.has("taskNumber")) {
                String taskId = payload.get("taskId").toString();
                String outcome = payload.get("outcome").toString();
                String employeeName = payload.get("employeeName").toString();
                String taskNumber = payload.get("taskNumber").toString();
                bean.setTaskId(taskId);
                bean.setOutcome(outcome);
                // check if task exists or not
                try {
                    TaskQueryService taskQueryObj = new TaskQueryService(employeeName);
                    queryServiceBean = taskQueryObj.getTaskDetailsByNumber(taskNumber, employeeName);
                    // check if task have error or not
                    if (queryServiceBean.getErrorInTask() != null) {
                        response.put("taskExist", "No");
                        response.put("error", queryServiceBean.getErrorInTask());
                    } else {
                        if ((queryServiceBean.getTaskState() != null
                                && (employeeName.equalsIgnoreCase(queryServiceBean.getAssignee())
                                || employeeName.equalsIgnoreCase(queryServiceBean.getOriginalAssigneeUser())))
                                || "ACKNOWLEDGE".equalsIgnoreCase(outcome)) {
                            response.put("taskExist", "Yes");
                        } else {
                            response.put("taskExist", "No");
                            response.put("error", "This task can't be found");
                        }
                    }
                } catch (AppsProException ex) {
                    throw new AppsProException(ex);
                }
                if ("Yes".equalsIgnoreCase(response.get("taskExist").toString())) {
                    // make action if task exist
//                doc = taskQueryObj.httpPost(bean.getURL(), bean.getUpdateTaskOutcomeSoapRequest(taskId, outcome, employeeName));
                    TaskQueryService taskQueryObj = new TaskQueryService(employeeName);
                    JSONObject object = new JSONObject(taskQueryObj.httpPostTest(bean.getURL(employeeName), bean.getUpdateTaskOutcomeSoapRequest(taskId, outcome, employeeName)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");
                        if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                            // update task details in mySql
//                            taskQueryObj.updateTaskDetailsMySql(taskId, employeeName);
                            
                            GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();
                            
                            getAndCompareTasks.getAndUpdateTasksFromCustomBPM(employeeName,  taskNumber,  "tawal");
                            
                        }
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryObj.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }
                    
                    //call update database and inbox api
                }
            }// doesn't have all data to make action
            else {
                response.put("error", "Internal server error missing data");
            }
        } catch (Exception e) {
            throw new AppsProException(e);
        }
        return response.toString();
    }

    @GET
    @Path("/getComments/{taskNumber}/{employeeName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllCommentsREST(@PathParam("taskNumber") String taskNumber,@PathParam("employeeName") String employeeName) throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        try {
            String resp = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/comments");
            if("noTaskFound".equalsIgnoreCase(resp)){
            return resp;   
            }else{
             JSONObject reportResponse = new JSONObject(resp);
            System.out.print(reportResponse.get("items"));
            return reportResponse.get("items").toString();   
            }
        } catch (IOException ex) {
           throw new AppsProException(ex);
        }
    }

    @POST
    @Path("/addAttachment/{employeeName}/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String addTaskAttachment(@Context HttpHeaders httpHeaders, String body, @PathParam("employeeName") String employeeName, @PathParam("taskNumber") String taskNumber) throws AppsProException {
        
        TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
        Document doc = null;
        TaskQueryServiceBean queryServiceBean = new TaskQueryServiceBean();
        JSONObject response = new JSONObject();
        JSONObject object = null;
        try {
            //check if task exists  
            try {
                TaskQueryService taskQueryService = new TaskQueryService(employeeName);
                queryServiceBean = taskQueryService.getTaskDetailsByNumber(taskNumber, employeeName);
                // check if task have error or not
                if (queryServiceBean.getErrorInTask() != null) {
                    response.put("taskExist", "No");
                    response.put("error", queryServiceBean.getErrorInTask());
                } else {
                    if (queryServiceBean.getTaskState() != null
                            && (employeeName.equalsIgnoreCase(queryServiceBean.getAssignee())
                            || employeeName.equalsIgnoreCase(queryServiceBean.getOriginalAssigneeUser()))) {
                        response.put("taskExist", "Yes");
                    } else {
                        response.put("taskExist", "No");
                        response.put("error", "This task can't be found");
                    }
                }
            } catch (AppsProException ex) {
                throw new AppsProException(ex);
            }
            
            if ("Yes".equalsIgnoreCase(response.get("taskExist").toString())) {
                
                 TaskQueryService taskQueryService = new TaskQueryService(employeeName);
                // add attachment for this task if exists
                JSONArray payload = new JSONArray(body);
                for (int i = 0; i < payload.length(); i++) {
                    JSONObject attachmentDetails = new JSONObject(payload.get(i).toString());
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getAddAttachmentSoapRequest(attachmentDetails.getString("taskId"), attachmentDetails.getString("encodedAttachment"), attachmentDetails.getString("title"), employeeName));
                    object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.getAddAttachmentSoapRequest(attachmentDetails.getString("taskId"), attachmentDetails.getString("encodedAttachment"), attachmentDetails.getString("title"), employeeName)));
                }
                // check returned status from http request
                if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                    response.put("status", "Success");
                } else {
                    response.put("status", "fail");
                    if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                        doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                        if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                            String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                            response.put("error", returnTokenFromAuth);
                        } else if (doc.getElementsByTagName("message").getLength() > 0) {
                            String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                            response.put("error", returnTokenFromAuth);
                        } else {
                            response.put("error", "Internal Server Error");
                        }
                    } else {
                        response.put("error", "You have an error on adding attachment");
                    }
                }
            }
        } catch (Exception e) {
            throw new AppsProException(e);
        }
        return response.toString();
    }

    @POST
    @Path("/addComment/{employeeName}/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String addTaskComment(@Context HttpHeaders httpHeaders, String body, @PathParam("employeeName") String employeeName, @PathParam("taskNumber") String taskNumber) throws AppsProException {
        
        TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
        TaskQueryServiceBean queryServiceBean = new TaskQueryServiceBean();
        JSONObject response = new JSONObject();
        Document doc = null;
        JSONObject object = null;
        
        try {
            
            // check if task exists
            TaskQueryService taskQueryService = new TaskQueryService(employeeName);
            queryServiceBean = taskQueryService.getTaskDetailsByNumber(taskNumber, employeeName);
            // check if task have error or not
            if (queryServiceBean.getErrorInTask() != null) {
                response.put("taskExist", "No");
                response.put("error", queryServiceBean.getErrorInTask());
            } else {
                if (queryServiceBean.getTaskState() != null
                        && (employeeName.equalsIgnoreCase(queryServiceBean.getAssignee())
                        || employeeName.equalsIgnoreCase(queryServiceBean.getOriginalAssigneeUser()))) {
                    response.put("taskExist", "Yes");
                } else {
                    response.put("taskExist", "No");
                    response.put("error", "This task can't be found");
                }
            }
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        
        if ("Yes".equalsIgnoreCase(response.get("taskExist").toString())) {
             TaskQueryService taskQueryService = new TaskQueryService(employeeName);
            // add comment if task exists
            try {
                
                JSONArray payload = new JSONArray(body);
                for (int i = 0; i < payload.length(); i++) {
                    JSONObject comment = new JSONObject(payload.get(i).toString());
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getAddCommentSoapRequest(comment.get("taskId").toString(), comment.get("commentStr").toString(), employeeName));
                    object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.getAddCommentSoapRequest(comment.get("taskId").toString(), comment.get("commentStr").toString(), employeeName)));
                }
                // check returned status from http request
                if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                    response.put("status", "Success");
                } else {
                    response.put("status", "fail");
                    if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                        doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                        if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                            String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                            response.put("error", returnTokenFromAuth);
                        } else if (doc.getElementsByTagName("message").getLength() > 0) {
                            String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                            response.put("error", returnTokenFromAuth);
                        } else {
                            response.put("error", "Internal Server Error");
                        }
                    } else {
                        response.put("error", "You have an error on adding comment");
                    }
                }
            } catch (Exception e) {
                throw new AppsProException(e);
            }
        }
        return response.toString();
    }

    @GET
    @Path("/getAttachments/{taskNumber}/{employeeName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllAttachmentsREST(@PathParam("taskNumber") String taskNumber,@PathParam("employeeName") String employeeName) throws AppsProException {
        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        try {
            String resp = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/attachments");
            if ("noTaskFound".equalsIgnoreCase(resp)) {
                return resp;
            } else {
                JSONObject obj = new JSONObject(resp);
                JSONArray jSONArray = obj.getJSONArray("items");
                System.out.println(jSONArray.length());
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject tc = jSONArray.getJSONObject(i);
                    System.out.println(tc.get("title"));
                    String response = taskQueryService.callGetAttachment(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/attachments/" + tc.get("title") + "/stream", "GET");
                    System.out.println(response);
                    tc.put("encodedAttachment", response);
                }
                return obj.get("items").toString();
            }
        } catch (IOException ex) {
            throw new AppsProException(ex);
        }
    }

    @DELETE
    @Path("/getAttachments/{taskNumber}/deleteAttachment/{attachmentTitle}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteAllAttachmentsREST(@PathParam("taskNumber") String taskNumber, @PathParam("attachmentTitle") String attachmentTitle) throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService("");
        try {

            String response = taskQueryService.callGetAttachment(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/attachments/" + attachmentTitle + "", "DELETE");

            return "Success";
        } catch (IOException ex) {
          throw new AppsProException(ex);

        }

    }

    @POST
    @Path("/Search/{employeeName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String employeeSearchREST(@Context HttpHeaders httpHeaders, String body,@PathParam("employeeName") String employeeName) throws AppsProException {

        JSONObject payload = new JSONObject(body.toString());

        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        try {
            String firstName = payload.get("firstName").toString();
            String lastName = payload.get("lastName").toString();
            String userName = payload.get("userName").toString();
            String workMail = payload.get("workMail").toString();
            String scope = payload.get("scope").toString();

            if (scope.equals("user")) {
                int limit = 1;
                String checkHasMore = "true";
                String response = null;
                while (checkHasMore == "true") {
                    System.out.println(limit);
                    response = taskQueryService.callGetRest(taskQueryService.getHostName() + "/hcmRestApi/resources/11.13.18.05/emps?limit=" + (limit * 500) + "&q=FirstName=" + firstName + ";LastName=" + lastName + ";UserName=" + userName + ";WorkEmail=" + workMail + "&expand=assignments");
                    
                    JSONObject payload2 = new JSONObject(response);
                    System.out.println(payload2.get("hasMore"));
                    limit++;
                    if (payload2.get("hasMore").toString() == "false") {

                        break;
                    }

                }
                return response;
            }
            if (scope.equals("group")) {
                String response = null;
                response = taskQueryService.callGetRest(taskQueryService.getHostName() + "/bpm/api/4.0/identities/groups");

                return response;
            } else {

                return "Please send Valid Scope";
            }
        } catch (IOException ex) {
           throw new AppsProException(ex);

        }

    }

    @GET
    @Path("/getHistory/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskHistoryREST(@PathParam("taskNumber") String taskNumber) throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService("");
        try {
            String resp = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/history");
            if ("noTaskFound".equalsIgnoreCase(resp)) {
                return resp;
            } else {
                JSONObject reportResponse = new JSONObject(resp);
                System.out.print(reportResponse.get("items"));
                return reportResponse.get("items").toString();
            }
        } catch (IOException ex) {
            throw new AppsProException(ex);

        }

    }

    @GET
    @Path("/getActionList/{taskNumber}/{employeeName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskActionsREST(@PathParam("taskNumber") String taskNumber,@PathParam("employeeName") String employeeName) throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        try {
            String reportResponse;
            System.out.print(taskNumber);

            reportResponse = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber);
            if ("noTaskFound".equalsIgnoreCase(reportResponse)) {
                return reportResponse;
            } else {
                JSONObject actionObj = new JSONObject(reportResponse);
                System.out.println(actionObj.get("actionList").toString());
                return actionObj.get("actionList").toString();
            }
        } catch (IOException ex) {
            throw new AppsProException(ex);

        }

    }

    @POST
    @Path("/actionType/{actionName}/{employeeName}/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String chooseTask(@PathParam("actionName") String actionName, @Context HttpHeaders httpHeaders, String body, @PathParam("employeeName") String employeeName, @PathParam("taskNumber") String taskNumber) throws AppsProException {
        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        TaskQueryServiceBean queryServiceBean = new TaskQueryServiceBean();
        JSONObject response = new JSONObject();
        try {

            //check if task exists or not
            queryServiceBean = taskQueryService.getTaskDetailsByNumber(taskNumber, employeeName);
            // check if task have error or not
            if (queryServiceBean.getErrorInTask() != null) {
                response.put("taskExist", "No");
                response.put("error", queryServiceBean.getErrorInTask());
            } else {
                if (queryServiceBean.getTaskState() != null
                        && (employeeName.equalsIgnoreCase(queryServiceBean.getAssignee())
                        || employeeName.equalsIgnoreCase(queryServiceBean.getOriginalAssigneeUser()))) {
                    response.put("taskExist", "Yes");
                } else {
                    response.put("taskExist", "No");
                    response.put("error", "This task can't be found");
                }
            }

        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        // start action if task exist
        if ("Yes".equalsIgnoreCase(response.get("taskExist").toString())) {

            if (actionName.equals("escalateTask")) {

                TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
                Document doc = null;
                try {

                    JSONObject payload = new JSONObject(body);
                    String taskId = payload.get("taskId").toString();
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getEscalateTaskSoapRequest(taskId, employeeName));
                    JSONObject object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.getEscalateTaskSoapRequest(taskId, employeeName)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");
                        // update status of task in mySQL DB
                        if ("Success".equalsIgnoreCase(response.get("status").toString())) {
//                            taskQueryService.updateTaskDetailsMySql(taskId, employeeName);

                            GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();

                            getAndCompareTasks.getAndUpdateTasksFromCustomBPM(employeeName, taskNumber, "tawal");

                            //update database and inbox api
                        }
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }

                } catch (Exception e) {
                    throw new AppsProException(e);
                }

            } else if (actionName.equals("reassignTask")) {

                TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
                String isGroup = null;
                Document doc = null;

                try {

                    JSONArray payloadArr = new JSONArray(body);
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getReassignSoapRequest(payloadArr, employeeName));
                    JSONObject object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.getReassignSoapRequest(payloadArr, employeeName)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");
                        // update status of task in mySQL DB

                        if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                            String taskId = new JSONObject(payloadArr.get(0).toString()).get("taskId").toString();
//                            taskQueryService.updateTaskDetailsMySql(taskId, employeeName);

                            GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();

                            getAndCompareTasks.getAndUpdateTasksFromCustomBPM(employeeName, taskNumber, "tawal");

                            //update database and inbox api
                        }
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }

                } catch (Exception e) {
                    throw new AppsProException(e);
                }

            } else if (actionName.equals("DelegateTask")) {

                TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
                Document doc = null;

                try {
                    JSONArray payload = new JSONArray(body);
                    System.out.print(taskServicePortPayload.getDelegateSoapRequest(payload, employeeName));
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getDelegateSoapRequest(payload, employeeName));
                    JSONObject object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.getDelegateSoapRequest(payload, employeeName)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");
                        // update status of task in mySQL DB
                        if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                            String taskId = new JSONObject(payload.get(0).toString()).get("taskId").toString();
//                            taskQueryService.updateTaskDetailsMySql(taskId, employeeName);

                            GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();

                            getAndCompareTasks.getAndUpdateTasksFromCustomBPM(employeeName, taskNumber, "tawal");

                            //update database and inbox api
                        }
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }

                } catch (Exception e) {
                    throw new AppsProException(e);
                }
            } else if (actionName.equals("requestMoreInfo")) {
                TaskServicePortPayload bean = new TaskServicePortPayload();
                Document doc = null;
                try {
                    JSONObject payload = new JSONObject(body);
                    String taskId = payload.get("taskId").toString();
                    String userComment = payload.get("userComment").toString();
                    String assigneeId = payload.get("assigneeId").toString();
//                    doc = taskQueryObj.httpPost(bean.getURL(), bean.getRequestMoreInfoSoapRequest(taskId, userComment, assigneeId, employeeName));
                    JSONObject object = new JSONObject(taskQueryService.httpPostTest(bean.getURL(employeeName), bean.getRequestMoreInfoSoapRequest(taskId, userComment, assigneeId, employeeName)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");

                        GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();

                        getAndCompareTasks.getAndUpdateTasksFromCustomBPM(employeeName, taskNumber, "tawal");
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }
                } catch (Exception e) {
                    throw new AppsProException(e);
                }
            } else if (actionName.equals("suspendTask")) {
                TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
                Document doc = null;

                try {
                    JSONObject payload = new JSONObject(body);
                    String taskId = payload.get("taskId").toString();
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.suspendTaskSoapRequest(taskId));
                    JSONObject object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.suspendTaskSoapRequest(taskId)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");
                        // update status of task in DB
                        if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                            taskQueryService.updateTaskDetailsMySql(taskId, employeeName);
                        }
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }

                } catch (Exception e) {
                    throw new AppsProException(e);
                }
            } else if (actionName.equals("withDrawTask")) {

                TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
                Document doc;

                try {
                    JSONObject payload = new JSONObject(body);
                    String taskId = payload.get("taskId").toString();
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.withDrawSoapRequest(taskId));
                    JSONObject object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.withDrawSoapRequest(taskId)));
                    // check returned status from http request
                    if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                        response.put("status", "Success");
                        // update status of task in DB
                        if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                            taskQueryService.updateTaskDetailsMySql(taskId, employeeName);
                        }
                    } else {
                        response.put("status", "fail");
                        if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                            doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                            if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else if (doc.getElementsByTagName("message").getLength() > 0) {
                                String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                                response.put("error", returnTokenFromAuth);
                            } else {
                                response.put("error", "Internal Server Error");
                            }
                        } else {
                            response.put("error", "You have an error in performing this action");
                        }
                    }

                } catch (Exception e) {
                    throw new AppsProException(e);
                }
            }
        }
        return response.toString();
    }

    @POST
    @Path("/getTaskFullPayload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskFullPayloadREST(@Context HttpHeaders httpHeaders, String body) throws XMLStreamException, AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService("");
        try {

            JSONObject payload = new JSONObject(body);

            String taskId = payload.get("taskId").toString();

            JSONObject jsOb = new JSONObject();
            // check on response
            String respose = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskId + "/payload");
            if ("noTaskFound".equalsIgnoreCase(respose)) {
                return respose;
            } else {
                Map<String, List<String>> map = parseXml(respose);

                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
//		    System.out.println(i+" == > "+entry.getKey() + " = " + entry.getValue().get(0));

                    jsOb.put(entry.getKey(), entry.getValue().get(0));

                }
                return jsOb.toString();
            }
        } catch (IOException ex) {
            throw new AppsProException(ex);

        }

    }

    @POST
    @Path("/addAttachmentURL/{employeeName}/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String addTaskAttachmentURL(@Context HttpHeaders httpHeaders, String body, @PathParam("employeeName") String employeeName, @PathParam("taskNumber") String taskNumber) throws AppsProException {
        TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
        Document doc = null;
        TaskQueryServiceBean queryServiceBean = new TaskQueryServiceBean();
        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        JSONObject response = new JSONObject();
         JSONObject object=null;
        
        try {
            
            //check if this task exists or not
            queryServiceBean = taskQueryService.getTaskDetailsByNumber(taskNumber, employeeName);
            // check if task have error or not
            if (queryServiceBean.getErrorInTask() != null) {
                response.put("taskExist", "No");
                response.put("error", queryServiceBean.getErrorInTask());
            } else {
                if (queryServiceBean.getTaskState() != null
                        && (employeeName.equalsIgnoreCase(queryServiceBean.getAssignee())
                        || employeeName.equalsIgnoreCase(queryServiceBean.getOriginalAssigneeUser()))) {
                    response.put("taskExist", "Yes");
                } else {
                    response.put("taskExist", "No");
                    response.put("error", "This task can't be found");
                }
            }
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        
        if ("Yes".equalsIgnoreCase(response.get("taskExist").toString())) {
            
            try {
                
                JSONArray payload = new JSONArray(body);
                for (int i = 0; i < payload.length(); i++) {
                    JSONObject attachmentURLDetails = new JSONObject(payload.get(i).toString());
//                    doc = taskQueryService.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getAddAttachmentByURLSoapRequest(attachmentURLDetails.get("taskId").toString(), attachmentURLDetails.get("href").toString(), attachmentURLDetails.get("title").toString(), employeeName));
                    object = new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.getAddAttachmentByURLSoapRequest(attachmentURLDetails.get("taskId").toString(), attachmentURLDetails.get("href").toString(), attachmentURLDetails.get("title").toString(), employeeName)));
                    System.out.println(doc);
                }
                // check returned status from http request
                if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                    response.put("status", "Success");
                } else {
                    response.put("status", "fail");
                    if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                        doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                        if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                            String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                            response.put("error", returnTokenFromAuth);
                        } else if (doc.getElementsByTagName("message").getLength() > 0) {
                            String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                            response.put("error", returnTokenFromAuth);
                        } else {
                            response.put("error", "Internal Server Error");
                        }
                    } else {
                        response.put("error", "You have an error on adding attachment");
                    }
                }
            } catch (Exception e) {
                throw new AppsProException(e);
            }
        }
        return response.toString();
    }

    @GET
    @Path("/getHistoryPattern/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getHistoryPatternREST(@PathParam("taskNumber") String taskNumber) throws AppsProException {

        try {
            TaskQueryServicePayload taskServicePortPayload = new TaskQueryServicePayload();

            Document document = null;
            TaskQueryService obj = new TaskQueryService("");
            TaskQueryService taskQueryService = new TaskQueryService("");
//            document = obj.httpPost(taskServicePortPayload.getURL(), taskServicePortPayload.getTaskPatternDetails(taskNumber,"employeeNumber"));
            JSONObject object = new JSONObject(obj.httpPostTest(taskServicePortPayload.getURL(""), taskServicePortPayload.getTaskPatternDetails(taskNumber, "employeeNumber")));
            // check returned status from http request
            if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                document = taskQueryService.stringToDocument(object.get("responseData").toString());
                document.getDocumentElement().normalize();
                System.out.println("Root element name :- " + document.getDocumentElement().getNodeName());
                NodeList myNode = document.getElementsByTagName("parallel");
                NodeList nodeList = document.getElementsByTagName("taskSequenceRecord");
                JSONArray arr = new JSONArray();
                for (int i = 0; i < myNode.getLength(); i++) {
                    Node node = myNode.item(i);
                }
                for (int i = 0; i < nodeList.getLength(); i++) {

                    Node node = nodeList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {

                        Element element = (Element) node;

                        JSONObject ob1 = new JSONObject();

                        ob1.put("isCurrentTaskSequenceRecord", element.getAttribute("isCurrentTaskSequenceRecord"));
                        ob1.put("sequenceNumber", element.getAttribute("sequenceNumber"));
                        try {
                            ob1.put("actionDisplayName", element.getElementsByTagName("actionDisplayName").item(0).getChildNodes().item(0).getTextContent());
                        } catch (Exception e) {

                            continue;
                        }

                        if (element.getElementsByTagName("collectionTarget").item(0) == null) {
                            ob1.put("id", "");
                        } else {
                            ob1.put("id", element.getElementsByTagName("collectionTarget").item(0).getChildNodes().item(0).getTextContent());
                        }
                        if (element.getElementsByTagName("subTaskGroupInstanceId").item(0) == null) {
                            ob1.put("subTaskGroupInstanceId", "");
                        } else {
                            ob1.put("subTaskGroupInstanceId", element.getElementsByTagName("subTaskGroupInstanceId").item(0).getChildNodes().item(0).getTextContent());
                        }
                        if (element.getElementsByTagName("assignees").item(0) == null) {
                            ob1.put("EmployeeID", "");
                            ob1.put("EmployeeName", "");
                        } else {
                            ob1.put("EmployeeID", element.getElementsByTagName("assignees").item(0).getChildNodes().item(0).getTextContent());
                            ob1.put("EmployeeName", element.getElementsByTagName("assignees").item(0).getChildNodes().item(1).getTextContent());
                        }
//                    ob1.put("EmployeeID", element.getElementsByTagName("assignees").item(0).getChildNodes().item(0).getTextContent());
//                    ob1.put("EmployeeName", element.getElementsByTagName("assignees").item(0).getChildNodes().item(1).getTextContent());

                        arr.put(ob1);
                    }
//                System.out.println(arr);
                }
                if (document != null) {
                    return arr.toString();
                }
            } else {
                return "INTERNAL_SERVER_ERROR";
            }
        } catch (Exception ex) {
            throw new AppsProException(ex);
        }
        return null;
    }

    @GET
    @Path("/getHistoryNewStr/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskHistoryRestructuredREST(@PathParam("taskNumber") String taskNumber) throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService("");
        try {
            String resp = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/history");
            if ("noTaskFound".equalsIgnoreCase(resp)) {
                return resp;
            } else {
                JSONObject reportResponse = new JSONObject(resp);
                JSONArray items = (JSONArray) reportResponse.get("items");

                JSONArray bodyJson = new JSONArray();
                JSONArray tempJson = new JSONArray();

                for (int i = 0; i < items.length(); i++) {

                    if (i == 0) {
                        tempJson.put(items.getJSONObject(i));

                        try {
                            items.getJSONObject(1);

                        } catch (JSONException e) {

                            bodyJson.put(tempJson);

                        }

                    } else {

                        if (items.getJSONObject(i).get("userId").equals(items.getJSONObject(i - 1).get("userId"))) {

                            tempJson.put(items.getJSONObject(i));

                            if (i == (items.length() - 1)) {

                                bodyJson.put(tempJson);
                            }

                        } else {
                            bodyJson.put(tempJson);

                            tempJson = new JSONArray(new ArrayList<String>());

                            tempJson.put(items.getJSONObject(i));

                            if (i == (items.length() - 1)) {

                                bodyJson.put(tempJson);
                            }

                        }

                    }

                }

                return bodyJson.toString();
            }
        } catch (IOException ex) {
            throw new AppsProException(ex);

        }

    }

    @GET
    @Path("/getTaskListSTC/{userName}/assignmentFilter/{assignmentFilter}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskListREST(@PathParam("userName") String taskNumber, @PathParam("assignmentFilter") String AssignedFilter) throws AppsProException {

        TaskQueryService taskQueryObj2 = new TaskQueryService("");
        ArrayList<TaskQueryServiceBean> listQueryFilter = new ArrayList<TaskQueryServiceBean>();
//        JSONObject payload = new JSONObject(AssignedFilter);
        listQueryFilter = taskQueryObj2.getAllTasksQueryFilter(AssignedFilter,"employeeName");

        return new JSONArray(listQueryFilter).toString();

    }

    @GET
    @Path("segmentName/{segmentName}/taskNumber/{taskNumber}/{employeeName}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_HTML)
    public String getNotificationReport(@PathParam("segmentName") String segmentName, @PathParam("taskNumber") String taskNumber, @PathParam("employeeName") String employeeName) throws AppsProException {
        try {
            String reportResponse="";
            SetupProjectDao setupProjectDao = new SetupProjectDao();
            TaskQueryService queryService = new TaskQueryService(employeeName);
            JSONObject jSONObject = new JSONObject(); 
            //arrayList of report and mapped parameters
            ReportDetailsBean reportDetailsBean
                    = setupProjectDao.getReportDetails(segmentName);
            Map<String, String> paramMap = queryService.getTaskDetailsByNumberDynamic(taskNumber, reportDetailsBean, employeeName);
            String reportName = reportDetailsBean.getReportName();
            String reportPath = reportDetailsBean.getReportPath();
            if (paramMap != null) {
                if (paramMap.get("otherData") != null) {
                    reportResponse = paramMap.get("otherData");
                    jSONObject.put("hasHistory", "false");
                } else {
                    reportResponse = BIReportModel.runReport(reportName, reportPath,
                            paramMap,employeeName);
                    jSONObject.put("hasHistory", "true");
                    System.out.println(reportResponse);
                }
                jSONObject.put("title", reportDetailsBean.getReportTitle());
                jSONObject.put("report", reportResponse);
                return jSONObject.toString();
            } else {
                return "fail";
            }

        } catch (Exception e) {
            throw new AppsProException(e);
        }

    }

    @GET
    @Path("getReportName/{taskNameSpace}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getReportName(@PathParam("taskNameSpace") String taskNameSpace) throws AppsProException {
        try {
            SetupProjectDao sp = new SetupProjectDao();

            ReportDetailsBean bean
                    = sp.getReportDetails(taskNameSpace);
            System.out.println(bean.getReportName() + " ==> " + bean.getReportPath());

            return "success";

        } catch (Exception e) {
           throw new AppsProException(e);
        }

    }
    
    @GET
    @Path("/getHistoryTest/{taskNumber}/{employeeName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getHistoryTest(@PathParam("taskNumber") String taskNumber,@PathParam("employeeName") String employeeName) throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        try {
            String resp = taskQueryService.callGetRest(taskQueryService.getHostName() + taskQueryService.getTasksEndPointURI() + taskNumber + "/history");
            if ("noTaskFound".equalsIgnoreCase(resp)) {
                return "fail";
            } else {
                JSONObject reportResponse = new JSONObject(resp);
                JSONArray items = (JSONArray) reportResponse.get("items");
                return items.toString();
            }
        } catch (Exception e) {
            return "fail";
        }

    }
    
    @POST
    @Path("/resumeTask")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String resumeTask(@Context HttpHeaders httpHeaders, String body) throws AppsProException {
        
        JSONObject response = new JSONObject();
        JSONObject payload = new JSONObject(body);
        String taskId = payload.get("taskId").toString();
        String employeeName=payload.get("employeeName").toString();
        TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
        Document doc = null;
        try {
            TaskQueryService taskQueryService = new TaskQueryService(employeeName);
            JSONObject object=new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.resumeTaskSoapRequest(taskId, employeeName)));
            // check response code for task
            if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                response.put("status", "Success");
                // update status of task in DB
                if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                    taskQueryService.updateTaskDetailsMySql(taskId, employeeName);
                }
            } else {
                response.put("status", "fail");
                if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                    doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                    // check returned error message
                    if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                        String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                        response.put("error", returnTokenFromAuth);
                    } else if (doc.getElementsByTagName("message").getLength() > 0) {
                        String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                        response.put("error", returnTokenFromAuth);
                    } else {
                        response.put("error", "Internal Server Error");
                    }
                } else {
                    response.put("error", "You have an error in performing this action");
                }
            }
            
        } catch (Exception e) {
            throw new AppsProException(e);
        }
        return response.toString();
    }
    
    @POST
    @Path("/releaseTask")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String releaseTask(@Context HttpHeaders httpHeaders, String body) throws AppsProException {
        
        JSONObject response = new JSONObject();
        JSONObject payload = new JSONObject(body);
        String taskId = payload.get("taskId").toString();
        String employeeName=payload.get("employeeName").toString();
        String taskNumber=payload.get("taskNumber").toString();
        TaskServicePortPayload taskServicePortPayload = new TaskServicePortPayload();
        Document doc = null;
         TaskQueryServiceBean queryServiceBean = new TaskQueryServiceBean();
         TaskQueryService taskQueryService = new TaskQueryService(employeeName);
        try {
            
            //check if this task exists or not
            queryServiceBean = taskQueryService.getTaskDetailsByNumber(taskNumber, employeeName);
            // check if task have error or not
                    if (queryServiceBean.getErrorInTask() != null) {
                        response.put("taskExist", "No");
                        response.put("error", queryServiceBean.getErrorInTask());
                    } else {
           if(queryServiceBean.getTaskState() != null 
                        && (employeeName.equalsIgnoreCase(queryServiceBean.getAssignee()) 
                        || employeeName.equalsIgnoreCase(queryServiceBean.getOriginalAssigneeUser()))){
                response.put("taskExist", "Yes");
            } else {
                response.put("taskExist", "No");
                response.put("error", "This task can't be found");
            }
                    } 
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        if ("Yes".equalsIgnoreCase(response.get("taskExist").toString())) {
        try {
            JSONObject object=new JSONObject(taskQueryService.httpPostTest(taskServicePortPayload.getURL(employeeName), taskServicePortPayload.releaseTaskSoapRequest(taskId, employeeName)));
            // check response code for task
            if ("200".equalsIgnoreCase(object.get("responseCode").toString())) {
                response.put("status", "Success");
                // update status of task in DB
                if ("Success".equalsIgnoreCase(response.get("status").toString())) {
                    taskQueryService.updateTaskDetailsMySql(taskId, employeeName);
                }
            } else {
                response.put("status", "fail");
                if (!"error".equalsIgnoreCase(object.get("responseData").toString())) {
                    doc = taskQueryService.stringToDocument(object.get("responseData").toString());
                    // check returned error message
                    if (doc.getElementsByTagName("errorMessage").getLength() > 0) {
                        String returnTokenFromAuth = doc.getElementsByTagName("errorMessage").item(0).getTextContent();
                        response.put("error", returnTokenFromAuth);
                    } else if (doc.getElementsByTagName("message").getLength() > 0) {
                        String returnTokenFromAuth = doc.getElementsByTagName("message").item(0).getTextContent();
                        response.put("error", returnTokenFromAuth);
                    } else {
                        response.put("error", "Internal Server Error");
                    }
                } else {
                    response.put("error", "You have an error in performing this action");
                }
            }
            
        } catch (Exception e) {
            throw new AppsProException(e);
        }
        }
        return response.toString();
    }
    
//    https://ejvm-dev1.fa.em2.oraclecloud.com/bpm/api/4.0/tasks/203030/history
    
    @GET
    @Path("testDeplyment")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String testDeployment() throws AppsProException {
        try {
//            System.out.println("hereeeeeee");

            return "success";

        } catch (Exception e) {
            throw new AppsProException(e);
        }

    }

    @GET
    @Path("/testDeplymentFusionURL")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTestFusionREST() throws AppsProException {

        TaskQueryService taskQueryService = new TaskQueryService("");
        try {
            String resp = taskQueryService.callGetRest2("https://ejvm-dev1.fa.em2.oraclecloud.com:443/bpm/api/4.0/tasks/204602");

            JSONObject reportResponse = new JSONObject(resp);
            
            System.out.print(reportResponse);
            return resp;

        } catch (IOException ex) {
            throw new AppsProException(ex);
        }
    }
    
//        @GET
//    @Path("/testDeplymentFusionIP")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public int getTestFusionRESTIP() throws AppsProException {
//
//        TaskQueryService taskQueryService = new TaskQueryService("");
//        try {
//            int resp = taskQueryService.callGetRest2("https://104.114.217.148:443/bpm/api/4.0/tasks/204602 ");
//
//            JSONObject reportResponse = new JSONObject(resp);
//            
//            System.out.print(reportResponse);
//            return resp;
//
//        } catch (IOException ex) {
//            throw new AppsProException(ex);
//        }
//    }

    @GET
    @Path("/testSelectFromDB")
//    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTestDBConnection() throws AppsProException {
        DatabaseConnection databaseConnection = new DatabaseConnection();

        Connection connection;
        PreparedStatement ps;
        ResultSet rs;
        String test_string = null;
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "SELECT test_column FROM test_table";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                test_string = rs.getString("test_column");
            }

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        return test_string;
    }

    @GET
    @Path("/testInsertOnDB")
//    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTestInsertDBConnection() throws AppsProException {
        DatabaseConnection databaseConnection = new DatabaseConnection();

        Connection connection;
        PreparedStatement ps;
        ResultSet rs;
        String test_string = "testing";
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "INSERT INTO test_table (test_column)\n"
                    + "                    VALUES (?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, test_string);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        return test_string;
    }

    @GET
    @Path("/testUpdateOnDB")
//    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTestUpdateDBConnection() throws AppsProException {
        DatabaseConnection databaseConnection = new DatabaseConnection();

        Connection connection;
        PreparedStatement ps;
        ResultSet rs;
        String test_string = "testupdate";
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE test_table \n"
                    + "set test_column=?  \n"
                    + "WHERE idtest_table = 1";

            ps = connection.prepareStatement(query);
            ps.setString(1, test_string);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        }
        return test_string;
    }

}
