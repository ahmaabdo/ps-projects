/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.bpm.appsproException.AppsProException;
import static com.appspro.stc.igate.STCValidationIgate.callPostInboxAPIHTTP;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;

/**
 *
 * @author Eslam
 */
@Path("/retryInbox")
public class STCIgateInboxREST {

    @POST
    @Path("/resendInbox")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createNewIgateInbox(String body) throws AppsProException {

        JSONObject object = new JSONObject(body);
        System.out.println(object.toString());
        String response = null;
        String httpMethod = object.has("httpMethod") ? object.get("httpMethod").toString() : null;
        if (httpMethod != null) {
            response = callPostInboxAPIHTTP(object.get("distUrl").toString(),
                    object.get("fullMessage").toString(), object.get("date").toString(),
                    object.get("body").toString(), httpMethod,object.get("taskNumber").toString());
            if(response.equalsIgnoreCase("200") ||
               response.equalsIgnoreCase("204")){
              return "Success";   
            }else{
                return "Fail";   
            }
        } else {
            return "Method type not found";
        }

    }
}
