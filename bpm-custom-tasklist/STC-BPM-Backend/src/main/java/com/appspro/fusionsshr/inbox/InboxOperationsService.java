/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.inbox;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.TaskLogDetailsBean;
import com.appspro.bpm.bean.TasksLogBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import static com.appspro.bpm.soap.TaskQueryService.trustAllHosts;
import com.appspro.stc.dao.TasksLogDao;
import com.appspro.stc.igate.STCValidationIgate;
import static com.appspro.stc.igate.STCValidationIgate.callGetInboxAPIHTTP;
import static com.appspro.stc.igate.STCValidationIgate.callPostInboxAPIHTTP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Eslam
 */
public class InboxOperationsService {
    
    public static String getAuth(String username, String password) {
        byte[] message = (username + ":" + password).getBytes();
//        byte[] message = ("HCMUser" + ":" + "Oracle@123").getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
    
    public String getAllIgateInboxData(JSONObject object) throws IOException, AppsProException {
        try {
            String lang = object.get("lang").toString();
            String usrId = object.get("userId").toString();
            String inboxType = object.get("inboxType").toString();
            String inboxId = object.get("inboxId").toString();
            String serverUrl = "https://igateapp.stc.com.sa/igate/v1.0/" + lang
                    + "/private/users/" + usrId + "/inbox/" + inboxType + "/" + inboxId;
            
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url = new URL(null, serverUrl.replaceAll(" ", "%20"), new sun.net.www.protocol.https.Handler());
            System.out.println("URL --> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            
            http.setRequestProperty("Authorization", "Basic " + getAuth("userName", "password"));
            http.setDoOutput(true);
            http.setRequestMethod("GET");
            http.setRequestProperty("Content-Type", "application/json");
            StringBuffer response;
            String encodedString;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
//Start
                byte[] bytes = IOUtils.toByteArray(http.getInputStream());
                encodedString = Base64.getEncoder().encodeToString(bytes);
                System.out.println("encodedString == >  " + encodedString);
//end
                System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                System.out.println("response message below");
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            return encodedString;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String createNewIgateInbox(String lang, String usrId, JSONObject object) throws IOException, AppsProException {
        try {
            String serverUrl = "https://igateapp.stc.com.sa/igate/v1.0/" + lang
                    + "/private/users/" + usrId + "/inbox";
            
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url = new URL(null, serverUrl.replaceAll(" ", "%20"), new sun.net.www.protocol.https.Handler());
            System.out.println("URL --> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            
            http.setRequestProperty("Authorization", "Basic " + getAuth("userName", "password"));
            http.setDoOutput(true);
            http.setRequestMethod("POST");
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");
            try (OutputStream os = http.getOutputStream()) {
                byte[] input = object.toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            StringBuffer response;
            String encodedString;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
//Start
                byte[] bytes = IOUtils.toByteArray(http.getInputStream());
                encodedString = Base64.getEncoder().encodeToString(bytes);
                System.out.println("encodedString == >  " + encodedString);

                System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                System.out.println("response message below");
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            return encodedString;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return object.toString();
    }
    
    public String editInbox(String lang, String usrId, String inboxId, JSONObject object) throws IOException, AppsProException {
        try {
            String serverUrl = "https://igateapp.stc.com.sa/igate/v1.0/" + lang
                    + "/private/users/" + usrId + "/inbox/" + inboxId;
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url = new URL(null, serverUrl.replaceAll(" ", "%20"), new sun.net.www.protocol.https.Handler());
            System.out.println("URL --> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            
            http.setRequestProperty("Authorization", "Basic " + getAuth("userName", "password"));
            http.setDoOutput(true);
            http.setRequestMethod("PUT");
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");
            try (OutputStream os = http.getOutputStream()) {
                byte[] input = object.toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            StringBuffer response;
            String encodedString;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
//Start
                byte[] bytes = IOUtils.toByteArray(http.getInputStream());
                encodedString = Base64.getEncoder().encodeToString(bytes);
                System.out.println("encodedString == >  " + encodedString);
//end
                System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                System.out.println("response message below");
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            return encodedString;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    public String deleteIgateInboxData(JSONObject object) throws IOException, AppsProException {
        try {
            String serverUrl = "https://igateapp.stc.com.sa/igate/v1.0/" + object.get("lang").toString()
                    + "/private/users/" + object.get("userId").toString() + "/inbox/" + object.get("indexId").toString();
            
            System.setProperty("DUseSunHttpHandler", "true");
            java.net.URL url = new URL(null, serverUrl.replaceAll(" ", "%20"), new sun.net.www.protocol.https.Handler());
            System.out.println("URL --> " + url);
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                java.net.HttpURLConnection https = (HttpsURLConnection) url.openConnection();
                http = https;
            } else {
                http = (HttpURLConnection) url.openConnection();
            }
            
            http.setRequestProperty("Authorization", "Basic " + getAuth("userName", "password"));
            http.setDoOutput(true);
            http.setRequestMethod("DELETE");
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Accept", "application/json");
            StringBuffer response;
            String encodedString;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
//Start
                byte[] bytes = IOUtils.toByteArray(http.getInputStream());
                encodedString = Base64.getEncoder().encodeToString(bytes);
                System.out.println("encodedString == >  " + encodedString);
//end
                System.out.print("getHeaderField --> " + http.getHeaderField(4) + "getContentType --> " + http.getHeaderField("Content-Type"));
                System.out.println("response message below");
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            return encodedString;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
 
    
    public static void callCreateInbox(UpdateAppsproDBBean bean) throws AppsProException {
        
        JSONObject createInboxBodyJSON = new JSONObject();
        System.out.println(bean.getTaskNumber());
        //we will add FUS instead fo CNA once they added it to their system
        createInboxBodyJSON.put("id", "CNA" + bean.getTaskNumber());
        createInboxBodyJSON.put("displayName", "CNA" + bean.getTaskNumber());

        //the below value will still static because when you create any new task the status should be pending                
        createInboxBodyJSON.put("status", "PENDING");
        createInboxBodyJSON.put("systemName", "iGate");
        createInboxBodyJSON.put("type", "CNA");
        createInboxBodyJSON.put("displayStatus", bean.getTaskStatus());
        createInboxBodyJSON.put("email", bean.getUserName());
        
        String tempUser = null;
        
        if(bean.getCreatorId() == null){
        System.out.println("the Creator == null");
        createInboxBodyJSON.put("requester", "mabulaban.c@tawal.com" );
        tempUser = "mabulaban.c@tawal.com";
        }else{
        System.out.println("the Creator != null");
        createInboxBodyJSON.put("requester",  bean.getCreatorId());
        tempUser =bean.getCreatorId();
        }
        System.out.println("TEMP USER NOT EQUAL NULL == "+tempUser);
        STCValidationIgate sTCValidationIgate = new STCValidationIgate();
        
        try{
        String response = sTCValidationIgate.callInboxApi(tempUser, "en", "POST", createInboxBodyJSON.toString(), "");
        }catch(Exception e){
        
        e.printStackTrace();
        
        }
    }
    
    public static void callUpdateInbox(String prevUser, String nextUser, String taskVersion, String outCome, String taskNumber) throws AppsProException {

        JSONObject updateInboxObject = new JSONObject();

        STCValidationIgate sTCValidationIgate = new STCValidationIgate();
        // update inbox
        switch (taskVersion) {

            //the get case will be removed we just created it for testing
            case "TASK_VERSION_REASON_COMPLETED":

                updateInboxObject.put("action", outCome);
                updateInboxObject.put("status", "REJECT".equals(outCome) ? "REJECTED" : "ACCEPTED");
                updateInboxObject.put("email", prevUser);
                updateInboxObject.put("nextStep", "B");//random character
                updateInboxObject.put("isHidden", "false");
                updateInboxObject.put("displayStatus", "REJECT".equals(outCome) ? "REJECTED" : "ACCEPTED");

                break;

            case "TASK_VERSION_REASON_ROUTED":

                updateInboxObject.put("action", outCome);
                updateInboxObject.put("status", "PENDING");
                updateInboxObject.put("email", nextUser);
                updateInboxObject.put("nextStep", "C");//random character
                updateInboxObject.put("isHidden", "false");
                updateInboxObject.put("displayStatus", "PENDING");

                break;

            case "TASK_VERSION_REASON_DELEGATED":

                JSONObject delegationObject = new JSONObject();
                updateInboxObject.put("action", outCome);
                updateInboxObject.put("status", "PENDING");
                updateInboxObject.put("email", prevUser);
                delegationObject.put("id", "1234");
                delegationObject.put("email", nextUser);
                updateInboxObject.put("delegatee", delegationObject);
                updateInboxObject.put("nextStep", "C");//random character
                updateInboxObject.put("isHidden", "false");
                updateInboxObject.put("displayStatus", "PENDING");

                break;

            default:

                updateInboxObject.put("action", outCome);
                updateInboxObject.put("status", "PENDING");
                updateInboxObject.put("email", nextUser);
                updateInboxObject.put("nextStep", "D");//random character
                updateInboxObject.put("isHidden", "false");
                updateInboxObject.put("displayStatus", "PENDING");

        }

        System.out.println(updateInboxObject);
        
        try{

        String response = sTCValidationIgate.callInboxApi(prevUser, "en", "PUT", updateInboxObject.toString(), taskNumber);
        }catch(Exception e){
        
        e.printStackTrace();
        
        }

    }
    
    
    public static void main(String[] args) throws AppsProException {
//        UpdateAppsproDBBean bean = new UpdateAppsproDBBean();
//        
//        bean.setTaskNumber("50001115");
//        bean.setUserName("stami@tawal.com.sa");
//        bean.setCreatorId("Sfmadawi@tawal.com.sa");
//        bean.setTaskStatus("ASSIGNED");
////        callCreateInbox(bean);
//
//
//
//
//
//        callCreateInbox( bean);
        
        
        callUpdateInbox("stami@tawal.com.sa", "nsuhaimi@tawal.com.sa", "TASK_VERSION_REASON_ROUTED", "APPROVE", "50001115");


        
    }
    
}
