
package com.appspro.fusionsshr.rest;

import com.appspro.bpm.bean.TaskLogDetailsBean;
import com.appspro.bpm.bean.TasksLogBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.stc.dao.QueryTasksDao;
import com.appspro.stc.dao.TasksLogDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

@Path("/tasksLog")
public class TasksLogREST {

    @GET
    @Path("/getAllTaskslog")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllTaskslog() {
        
        List<TasksLogBean> tasksLogList = new ArrayList();
        TasksLogDao tasksLogDao = new TasksLogDao();
        try {
            tasksLogList = tasksLogDao.getAllTaskDetails();
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
        return new JSONArray(tasksLogList).toString();
    }

    @GET
    @Path("/getTaskLog/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllTasksLog(@PathParam("taskNumber") String taskNumber) {
        
        TasksLogBean tasksLogBean = new TasksLogBean();
        TasksLogDao tasksLogDao = new TasksLogDao();
        try {
            tasksLogBean = tasksLogDao.getAllTasksLogByTaskNumber(taskNumber);
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
        return new JSONObject(tasksLogBean).toString();
    }
    
    @GET
    @Path("/getLogByTaskNumber/{taskNumber}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getLogByTaskNumber(@PathParam("taskNumber") String taskNumber) {
        
        TasksLogBean tasksLogBean = new TasksLogBean();
        TasksLogDao tasksLogDao = new TasksLogDao();
        try {
            tasksLogBean = tasksLogDao.getLogByTaskNumber(taskNumber);
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
        return new JSONObject(tasksLogBean).toString();
    }

    @POST
    @Path("/addTaskLog")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addTaskDetail(String body) {

        TasksLogBean returnedTasksLogBean = new TasksLogBean();
        TasksLogDao tasksLogDao = new TasksLogDao();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TasksLogBean tasksLogBean = mapper.readValue(body, TasksLogBean.class);
            if (tasksLogBean.getTaskDetails() != null && tasksLogBean.getTaskDetails().size() > 0) {
                TaskLogDetailsBean taskLogDetailsBean = new TaskLogDetailsBean();
                taskLogDetailsBean.setTaskNumber(tasksLogBean.getTaskDetails().get(0).getTaskNumber());
                taskLogDetailsBean.setServiceName(tasksLogBean.getTaskDetails().get(0).getServiceName());
                taskLogDetailsBean.setServiceStatus(tasksLogBean.getTaskDetails().get(0).getServiceStatus());
                taskLogDetailsBean.setServiceActionType(tasksLogBean.getTaskDetails().get(0).getServiceActionType());
                taskLogDetailsBean.setServiceData(tasksLogBean.getTaskDetails().get(0).getServiceData());
                tasksLogBean.setTaskLogDetails(taskLogDetailsBean);
                returnedTasksLogBean =tasksLogDao.insertNewLog(tasksLogBean);
            }
            return new JSONObject(returnedTasksLogBean).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
    }

    //details rest 
    @POST
    @Path("/addTaskLogDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addTaskLogDetails(String body) {

        TaskLogDetailsBean returnedtaskLogDetailsBean = new TaskLogDetailsBean();
        TasksLogDao Dao = new TasksLogDao();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TaskLogDetailsBean taskLogDetailsBean = mapper.readValue(body, TaskLogDetailsBean.class);
            returnedtaskLogDetailsBean = Dao.insertLogDetails(taskLogDetailsBean);
            return new JSONObject(returnedtaskLogDetailsBean).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
    }

    @POST
    @Path("/editTaskLogDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateTaskLogDetails(String body) {

        TaskLogDetailsBean returnedtaskLogDetailsBean = new TaskLogDetailsBean();
        TasksLogDao Dao = new TasksLogDao();
        try {
            ObjectMapper mapper = new ObjectMapper();
            TaskLogDetailsBean taskLogDetailsBean = mapper.readValue(body, TaskLogDetailsBean.class);
            returnedtaskLogDetailsBean = Dao.editTaskLogDetails(taskLogDetailsBean);
            return new JSONObject(returnedtaskLogDetailsBean).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
            
        }
    }
    
    // add and update and delete for task details in my sql
    
    //insert
    @POST
    @Path("/addTaskDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addTaskDetails(String body) {

        UpdateAppsproDBBean returnedTasksLogBean = new UpdateAppsproDBBean();
        QueryTasksDao tasksDao = new QueryTasksDao();
        try {
            ObjectMapper mapper = new ObjectMapper();
            UpdateAppsproDBBean tasksLogBean = mapper.readValue(body, UpdateAppsproDBBean.class);
            returnedTasksLogBean = tasksDao.postNewTasks(tasksLogBean);
            return new JSONObject(returnedTasksLogBean).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
    }
    
    //update 
    
//     @POST
//    @Path("/updateTaskDetails")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public String updateTaskDetails(String body) {
//
//        UpdateAppsproDBBean returnedTasksLogBean = new UpdateAppsproDBBean();
//        QueryTasksDao tasksDao = new QueryTasksDao();
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//            UpdateAppsproDBBean tasksLogBean = mapper.readValue(body, UpdateAppsproDBBean.class);
//            returnedTasksLogBean = tasksDao.updateTasks(tasksLogBean , "T");
//            return new JSONObject(returnedTasksLogBean).toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "INTERNAL_SERVER_ERROR";
//        }
//    }
    
    //delete 
   
    @POST
    @Path("/deleteTaskDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteTaskDetails(String body) {

        UpdateAppsproDBBean returnedTasksLogBean = new UpdateAppsproDBBean();
        QueryTasksDao tasksDao = new QueryTasksDao();
        try {
            ObjectMapper mapper = new ObjectMapper();
            UpdateAppsproDBBean tasksLogBean = mapper.readValue(body, UpdateAppsproDBBean.class);
            returnedTasksLogBean = tasksDao.updateNonExistTask(tasksLogBean);
            return new JSONObject(returnedTasksLogBean).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "INTERNAL_SERVER_ERROR";
        }
    }
}
