/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.rest;

import com.appspro.bpm.appsproException.AppsProException;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.stc.dao.SetupProjectDao;
import com.appspro.stc.handler.SetupProjectHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import org.json.JSONObject;

/**
 *
 * @author Lenovo
 */
@Path("/setup")
public class SetupProjectRest {

    private SetupProjectHandler handler = new SetupProjectHandler();

    @GET
    @Path("/getAllSetup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SetupProjectBean> getAll(@Context HttpServletRequest request, @Context HttpServletResponse response) throws AppsProException {

        List<SetupProjectBean> list = new ArrayList<SetupProjectBean>();

        list = handler.getAll();
        return list;

    }

    @POST
    @Path("/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertOrUpdatePosition(SetupProjectBean bean,
            @PathParam("transactionType") String transactionType, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {

        try {
            handler.insertOrUpdatePosition(bean, transactionType);
        } catch (Exception e) {
            //(e);
            return Response.status(500).entity(e.getMessage()).build();
        }

        return Response.ok(bean, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("getDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getCompanyNameById(@QueryParam("companyName") String companyName) {
        SetupProjectBean list = new SetupProjectBean();
        try {
            list = handler.getByCompanyName(companyName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
}
