/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.fusionsshr.inbox;

import com.appspro.bpm.appsproException.AppsProException;
import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;
import sun.applet.AppletIOException;

/**
 *
 * @author Eslam
 */
 @Path("/inbox")
public class InboxOperationsREST {
   
    @GET
    @Path("/getAllIgateInbox")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON) 
    public String getAllIgateInbox() throws AppsProException{
       InboxOperationsService operationsService=new InboxOperationsService();
       String returnedData = null;
       JSONObject object=new  JSONObject();
            object.put("lang", "en");
            object.put("userId", "tmushaigh@stc.com.sa");
            object.put("inboxType", "pending");
            object.put("inboxId","PGP1000287"); 
       try {
            returnedData = operationsService.getAllIgateInboxData(object);
        } catch (IOException ex) {
           throw new AppsProException(ex);
        }
        return object.toString();
    }
   
    
    @POST
    @Path("/createNewIgateInbox")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON) 
    public String createNewIgateInbox() throws AppsProException{
            InboxOperationsService operationsService=new InboxOperationsService();
            String returnedData = null;
            JSONObject object =new JSONObject();
            JSONObject delegateeObject =new JSONObject();
            JSONObject rolesObject =new JSONObject();
            JSONObject briefsObject =new JSONObject();
            object.put("id", "LJA500011121");
            object.put("displayName", "LJA500011121");
            object.put("status", "PENDING");
            object.put("systemName", "RASEL");
            object.put("type", "LJA");
            object.put("displayStatus", "PENDING");
            object.put("email", "tmushaigh@stc.com.sa");
            delegateeObject.put("id", "1234");
            delegateeObject.put("email", "rzahrani@stc.com.sa");
            object.put("delegatee", delegateeObject);
            rolesObject.put("key", "GM");
            rolesObject.put("descEn", "General Manager");
            rolesObject.put("descAr", "");
            object.put("roles", rolesObject);
            object.put("nextStep", "B");
            object.put("isHidden", "false");
            object.put("attachmentId", "12345");
            object.put("severity", "Normal");
            object.put("secret", "false");
            briefsObject.put("briefEn", "Brief comment");
            briefsObject.put("briefAr", "");
            object.put("briefs", briefsObject);
        try {
            //lan,usrId,objectData
            returnedData = operationsService.createNewIgateInbox("en","tmushaigh@stc.com.sa",object);
        } catch (IOException ex) {
            throw new AppsProException(ex);
        }
        return returnedData;
    }
    
    @POST
    @Path("/editIgateInbox")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON) 
    public String editIgateInbox() throws AppsProException{
        InboxOperationsService operationsService=new InboxOperationsService();
            String returnedData = null;
            JSONObject object =new JSONObject();
            JSONObject delegateeObject =new JSONObject();
            JSONObject rolesObject =new JSONObject();
            object.put("action", "approved");
            object.put("status", "PENDING");
            object.put("email", "tmushaigh@stc.com.sa");
            delegateeObject.put("id", "1234");
            delegateeObject.put("email", "rzahrani@stc.com.sa");
            object.put("delegatee", delegateeObject);
            rolesObject.put("key", "GM");
            rolesObject.put("descEn", "General Manager");
            rolesObject.put("descAr", "");
            object.put("roles", rolesObject);
            object.put("nextStep", "B");
            object.put("isHidden", "false");
            object.put("displayStatus", "PENDING");
            object.put("comments", "Testing Update Inbox Item");
            object.put("attachmentId", "12345");
        try {
            //lan,usrId,objectData
            returnedData = operationsService.editInbox("en","tmushaigh@stc.com.sa","PGP1000287",object);
        } catch (IOException ex) {
            throw new AppsProException(ex);
        }
        return returnedData;
    }
    
    @POST
    @Path("/deleteIgateInbox")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON) 
    public String deleteIgateInbox() throws AppsProException{
        InboxOperationsService operationsService=new InboxOperationsService();
        String returnedData = null;
        try {
            JSONObject object=new JSONObject();
            object.put("lang","en");
            object.put("userId","tmushaigh@stc.com.sa");
            object.put("indexId","PGP1000287");
            //lan,usrId,objectData
            returnedData = operationsService.deleteIgateInboxData(object);
        } catch (IOException ex) {
            throw new AppsProException(ex);
        }
        return returnedData;
    }
}
