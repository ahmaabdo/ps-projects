/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.stc.igate;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.TaskLogDetailsBean;
import com.appspro.bpm.bean.TasksLogBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.bpm.soap.TaskQueryService;
import static com.appspro.bpm.soap.TaskQueryService.trustAllHosts;
import com.appspro.stc.dao.QueryTasksDao;
import com.appspro.stc.dao.TasksLogDao;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import java.security.cert.X509Certificate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import javax.net.ssl.HttpsURLConnection;

import javax.net.ssl.TrustManager;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.JSONObject;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;

/**
 *
 * @author user
 */
public class STCValidationIgate {

//public static void main(String [] args){
//        String language = "en";
//        String uMail = "shrkhan.c@stc.com.sa";
//        String jwtToken = "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..jByJNpGKMJNTYXYpBJ4z7Q.YXkM-zFgvfFPImc-Jdk_8EXpFmuAjuxZdLh2d-6DKw1dAv-bPSS8jw6UUMRCQhNwnaScQcq_xlwo2lC8hcveruM9MvDEQeqH1VxBETO94nf136Um_IFJCGA8aiBGms6UV-WM-yYPMsjiJ8lrEKVyEq32Cd0w2mhdYMtX-YOyYqwxwBJBVkfdgJIKnfWphZGpEbN0cPR32D046OOCJ4osE2ABfb51S0hNsCuuGtHuWCK6rAmL97BCmJdOpnGRkBkY.zCtHtNyKgR47zQ_qF0w7QA";
//
//int status = ValidateJWT(uMail ,language ,jwtToken  );
//}
    public static int ValidateJWT(String uMail, String language, String jwtToken) {

//        String language = "en";
//        String uMail = "halmushigih@stc.com.sa";
//        String jwtToken = "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIiwiY3R5IjoiSldUIn0..jByJNpGKMJNTYXYpBJ4z7Q.YXkM-zFgvfFPImc-Jdk_8EXpFmuAjuxZdLh2d-6DKw1dAv-bPSS8jw6UUMRCQhNwnaScQcq_xlwo2lC8hcveruM9MvDEQeqH1VxBETO94nf136Um_IFJCGA8aiBGms6UV-WM-yYPMsjiJ8lrEKVyEq32Cd0w2mhdYMtX-YOyYqwxwBJBVkfdgJIKnfWphZGpEbN0cPR32D046OOCJ4osE2ABfb51S0hNsCuuGtHuWCK6rAmL97BCmJdOpnGRkBkY.zCtHtNyKgR47zQ_qF0w7QA";
        String secret = "@@igate-internal-Client@@2016:password2016";
        String httpMethod = "GET";

        String date = CurrentGMTDate();
        String client = "internal";
        String version = "v1.0";

        String sourceURL = "/igate/v1.0/" + language + "/private/users/" + uMail + "/validateTokenen";

        String message = httpMethod.concat(sourceURL).concat(date).concat(client).concat(version).concat(httpMethod);
        String hashed = hmacDigest(message, secret, "HmacSHA256");
        String unHashed = "\\content-type:text/json\\host:x-igate-API\\useragent:internal/8.1\\version:1.0";

        String fullMessage = hashed + unHashed;

        System.out.println(fullMessage);
        System.out.println(date);

        try {
            int status = httpPost2("https://igateapp.stc.com.sa/igate/v1.0/en/private/users/" + uMail + "/validateToken", fullMessage, date, jwtToken);

            return status;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return 0;
    }

    public static String callInboxApi(String uMail, String language, String httpMethod, String requestJsonString, String taskNumber) {

        //in case create inbox the uMail will be the task creator and will be send in the create inbox API URL
//        http://dev-esb-cs.stc.com.sa:4001/igate/v1.0/en/private/users/ftharbi@stc.com.sa/inbox
        String secret = "@@Subsidiary-Client##:$$Sub2019@@Oct##";
        String client = "Subsidiary";
        String version = "v1.0";
        String inboxAPIHost = "http://172.21.14.92:2001";
        String unHashed = "\\content-type:text/json\\host:x-igate-API\\useragent:Subsidiary/8.1\\version:1.0";
        String date = CurrentGMTDate();

        String sourceURL = null;

        if ("POST".equals(httpMethod) || "GET".equals(httpMethod)) {

            sourceURL = "/igate/v1.0/" + language + "/private/users/" + uMail + "/inbox";
        }
        if ("PUT".equals(httpMethod)) {
            sourceURL = "/igate/v1.0/" + language + "/private/users/" + uMail + "/inbox/" + "CNA" + taskNumber;

        }

        System.out.println("Source URL : " + sourceURL);

        String message = httpMethod.concat(sourceURL).concat(language).concat(date).concat(client).concat(version).concat(httpMethod);

        String hashed = hmacDigest(message, secret, "HmacSHA256");

        //full message is the complete hashed message for x-igate-client
        String fullMessage = hashed + unHashed;
        System.out.println("fullMessage : " + fullMessage);

        String response = null;
        switch (httpMethod) {

            //the get case will be removed we just created it for testing
            case "GET":
                try {
                    response = callGetInboxAPIHTTP(inboxAPIHost + sourceURL, fullMessage, date);
                } catch (Exception e) {
                }

                break;

            case "POST":
                try {
                    response = callPostInboxAPIHTTP(inboxAPIHost + sourceURL, fullMessage, date, requestJsonString, "POST",taskNumber);
                } catch (Exception e) {
                }
                break;

            case "PUT":
                try {
                    response = callPostInboxAPIHTTP(inboxAPIHost + sourceURL, fullMessage, date, requestJsonString, "PUT",taskNumber);
                } catch (Exception e) {
                }
                break;

            default:
            // Statements
        }

        return response;

//        try {
//
//            String response = callPostInboxAPIHTTP(inboxAPIHost + sourceURL, fullMessage, date, httpMethod);
//
//            return response;
//        } catch (Exception ex) {
//            Logger.getLogger(STCValidationIgate.class.getName()).log(Level.SEVERE, null, ex);
//
//            return ex.toString();
//        }
//return null;
    }

    public static int httpPost2(String destUrl, String hashed, String date, String jwtTaken) throws Exception {

        System.setProperty("DUseSunHttpHandler", "true");

        java.net.URL url
                = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
//             trustAllHttpsCertificates();
            java.net.HttpURLConnection https
                    = (HttpsURLConnection) url.openConnection();

            http = https;
        } else {
            http = (HttpURLConnection) url.openConnection();
        }

        http.setRequestMethod("GET");
        http.setDoOutput(true);
        http.setDoInput(true);
//        http.addRequestProperty("Authorization", jwtTaken);
        http.addRequestProperty("Date", date);
        http.addRequestProperty("x-igate-client", hashed);
//        http.addRequestProperty("Accept", "*/*");
//        http.addRequestProperty("Cache-Control", "no-cache");
//        http.addRequestProperty("Host", "igateapp.stc.com.sa");
//        http.addRequestProperty("accept-encoding", "gzip, deflate");
//        http.addRequestProperty("Connection", "keep-alive");
//        http.setRequestProperty("Content-Type", "application/json");

        System.out.println("connection status: " + http.getResponseCode()
                + "; connection response: "
                + http.getResponseMessage());

        http.disconnect();

        return http.getResponseCode();

    }

    private static void trustAllHttpsCertificates() throws Exception {

        //  Create a trust manager that does not validate certificate chains:
        javax.net.ssl.TrustManager[] trustAllCerts
                = new javax.net.ssl.TrustManager[1];

        javax.net.ssl.TrustManager tm = new miTM();

        trustAllCerts[0] = tm;

        javax.net.ssl.SSLContext sc
                = javax.net.ssl.SSLContext.getInstance("SSL");

        sc.init(null, trustAllCerts, null);

        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
                sc.getSocketFactory());

    }

    public static class miTM implements javax.net.ssl.TrustManager,
            javax.net.ssl.X509TrustManager {

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(
                java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(
                java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }

        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }
    }

    public static String CurrentGMTDate() {

        Date date = new Date();
        //                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat dateFormat
                = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        //                    System.out.println("UTC Time is: " + dateFormat.format(date).toString());

        return dateFormat.format(date).toString();

    }

    public static String hmacDigest(String msg, String keyString,
            String algo) {
        String digest = null;
        try {
            SecretKeySpec key
                    = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }

    public static String callGetInboxAPIHTTP(String distURL, String fullMessage, String date) {

        StringBuffer response;
        try {

            URL url = new URL(distURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("x-igate-client", fullMessage);
            conn.setRequestProperty("Date", date);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            String inputLine;
            System.out.println("Response Code -----> " + conn.getResponseCode());
            System.out.println("Output from Server .... \n");
            response = new StringBuffer();
            while ((output = br.readLine()) != null) {
                System.out.println(output);

                response.append(output);
            }

            conn.disconnect();

            return response.toString();

        } catch (MalformedURLException e) {

            return e.toString();

        } catch (IOException e) {

            e.printStackTrace();

            return e.toString();

        }

    }

    public static String callPostInboxAPIHTTP(String distURL, String fullMessage, String date, String body, String httpMethod,String taskNumber) {
        JSONObject jsonObject=null;
        JSONObject object=null;
        try {
//            System.out.print(body);
            URL url = new URL(distURL);
            UpdateAppsproDBBean myBean = new UpdateAppsproDBBean();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(httpMethod);
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("x-igate-client", fullMessage);
            conn.setRequestProperty("Date", date);
            // asign data for log details
            object = new JSONObject();
            object.put("distUrl", distURL);
            object.put("fullMessage", fullMessage);
            object.put("date", date);
            object.put("body", body);
            object.put("httpMethod", httpMethod);
            object.put("taskNumber", taskNumber);
            jsonObject = new JSONObject(body);
            ///////////////////
            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
          
            System.out.println("***************************"+ body.toString());
            System.out.println(conn.getResponseCode());
            
            if (conn.getResponseCode() == 200 || 
                conn.getResponseCode() == 204) {
                // data inbox table 
                // task numberb is missed
                insertIntoInboxLog(taskNumber, jsonObject.has("requester")?jsonObject.get("requester").toString():"", 
                        jsonObject.get("email").toString(),"tawal", jsonObject.get("status").toString(),
                        jsonObject.has("systemName")?jsonObject.get("systemName").toString():"",
                        jsonObject.has("type")?jsonObject.get("type").toString():"",
                        jsonObject.get("displayStatus").toString(),"success", "inboxSend", object.toString());
            } else {
               insertIntoInboxLog(taskNumber, jsonObject.has("requester")?jsonObject.get("requester").toString():"", 
                        jsonObject.get("email").toString(),"tawal", jsonObject.get("status").toString(),
                        jsonObject.has("systemName")?jsonObject.get("systemName").toString():"",
                        jsonObject.has("type")?jsonObject.get("type").toString():"",jsonObject.get("displayStatus").toString()
                       ,"fail", "inboxSend", object.toString());
            }

            System.out.println("Response Code -----> " + conn.getResponseCode());

//            if (conn.getResponseCode() != 200) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + conn.getResponseCode());
//            }
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(
//                    (conn.getInputStream())));
//
//            String output;
//            String inputLine;
//            System.out.println("Response Code -----> " + conn.getResponseCode());
//            System.out.println("Output from Server .... \n");
//            response = new StringBuffer();
//            while ((output = br.readLine()) != null) {
//                System.out.println(output);
//
//                response.append(output);
//            }
            conn.disconnect();

            return String.valueOf(conn.getResponseCode());

        } catch (MalformedURLException e) {
            // insert log details 
            insertIntoInboxLog(taskNumber, jsonObject.has("requester") ? jsonObject.get("requester").toString() : "",
                    jsonObject.get("email").toString(), "tawal", jsonObject.get("status").toString(),
                    jsonObject.has("systemName") ? jsonObject.get("systemName").toString() : "",
                    jsonObject.has("type") ? jsonObject.get("type").toString() : "", jsonObject.get("displayStatus").toString(),
                     "fail", "inboxSend", object.toString());
            return e.toString();

        } catch (IOException e) {
            e.printStackTrace();
            // insert log details 
            insertIntoInboxLog(taskNumber, jsonObject.has("requester") ? jsonObject.get("requester").toString() : "",
                    jsonObject.get("email").toString(), "tawal", jsonObject.get("status").toString(),
                    jsonObject.has("systemName") ? jsonObject.get("systemName").toString() : "",
                    jsonObject.has("type") ? jsonObject.get("type").toString() : "", jsonObject.get("displayStatus").toString(),
                     "fail", "inboxSend", object.toString());
            return e.toString();

        }

    }

    public static void insertIntoInboxLog(String taskNumber, String previous_approvers, String next_approver,
            String company_name, String status, String system_name, String type, String display_status,
            String inboxResponse, String inboxActionType, String inboxData) {
        
        TasksLogDao taskDao = new TasksLogDao();
        TasksLogBean taskBean = new TasksLogBean();
        TaskLogDetailsBean detailsBean = new TaskLogDetailsBean();
        // set master data
        taskBean.setTaskNumber(taskNumber);
        taskBean.setPreviousApprover(previous_approvers);
        taskBean.setNextApprover(next_approver);
        taskBean.setCompanyName(company_name);
        taskBean.setStatus(status);
        taskBean.setSystemName(system_name);
        taskBean.setType(type);
        taskBean.setDisplay_status(display_status);
        //set details data
        detailsBean.setTaskNumber(taskNumber);
        detailsBean.setServiceName("inboxService");
        detailsBean.setServiceStatus(inboxResponse);
        detailsBean.setServiceActionType(inboxActionType);
        detailsBean.setServiceData(inboxData);
       
        taskBean.setTaskLogDetails(detailsBean);
       
        try {
            //inbox table
            taskDao.insertNewLog(taskBean);
        } catch (AppsProException ex) {
            ex.printStackTrace();
        }

    }
    
    
     public static void main(String[] args) throws AppsProException {
        
    }

}


