/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.stc.dao;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.EmployeeDetailsBean;
import com.appspro.bpm.bean.JobExecutionBean;
import com.appspro.bpm.bean.ReportDetailsBean;
import com.appspro.bpm.bean.ReportParametersBean;
import com.appspro.bpm.bean.TaskLogDetailsBean;
import com.appspro.bpm.bean.TaskQueryServiceBean;
import com.appspro.bpm.bean.TasksLogBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import com.appspro.stc.database.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class QueryTasksDao extends DatabaseConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public ReportDetailsBean getReportDetails(String taskNameSpace) throws AppsProException {

        ReportDetailsBean reportDetailsBean = new ReportDetailsBean();
        ArrayList<ReportParametersBean> reportParamList = new ArrayList<>();

        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "SELECT * FROM xx_stc_segment_details, xx_stc_segment_report_details WHERE xx_stc_segment_details.id=xx_stc_segment_report_details.xx_segment_details_id and xx_stc_segment_details.task_name_space = ?";

            ps = connection.prepareStatement(query);
            ps.setString(1, taskNameSpace);
            rs = ps.executeQuery();
            while (rs.next()) {
                ReportParametersBean bean = new ReportParametersBean();
                reportDetailsBean.setReportName(rs.getString("report_name"));
                reportDetailsBean.setReportPath(rs.getString("report_path"));
                reportDetailsBean.setTaskNameSpace(rs.getString("task_name_space"));
                reportDetailsBean.setReportTitle(rs.getString("report_title"));
                bean.setReportParameter(rs.getString("report_parameter"));
                bean.setMappedParameter(rs.getString("mapped_parameter"));

                reportParamList.add(bean);
            }
            reportDetailsBean.setReportParameters(reportParamList);
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return reportDetailsBean;
    }

    public UpdateAppsproDBBean postNewTasks(UpdateAppsproDBBean taskQueryServiceBean) throws AppsProException {
        String status = null;

        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "INSERT INTO xx_all_inbox_details (task_number , previous_approver, next_approver , company_name , status , system_name , type , display_status)\n"
                    + "                    VALUES ( ?, ? ,? ,?,?,?,?,?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, taskQueryServiceBean.getTaskNumber());

            if (taskQueryServiceBean.getCreatorId() == null || "workflowsystem".equals(taskQueryServiceBean.getCreatorId())) {
                ps.setString(2, "mabulaban.c@tawal.com");
            } else {
                ps.setString(2, taskQueryServiceBean.getCreatorId());
            }
            ps.setString(3, taskQueryServiceBean.getUserName());
            ps.setString(4, "tawal");
            ps.setString(5, "PENDING");
            ps.setString(6, "iGate");
            ps.setString(7, "CNA");
            ps.setString(8, taskQueryServiceBean.getTaskStatus());

            ps.executeUpdate();

        } catch (SQLException e) {
            status = "fail";
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            status = "fail";
            throw new AppsProException(ex);
        } finally {
            TasksLogDao logDao = new TasksLogDao();
            TaskLogDetailsBean detailsBean = new TaskLogDetailsBean();
            TasksLogBean inboxBean = new TasksLogBean();
            // set data for inbox table
            inboxBean.setTaskNumber(taskQueryServiceBean.getTaskNumber());
            inboxBean.setCompanyName("tawal");
            inboxBean.setDisplay_status(taskQueryServiceBean.getTaskStatus());
            inboxBean.setNextApprover(taskQueryServiceBean.getUserName());
            if (taskQueryServiceBean.getCreatorId() == null || "workflowsystem".equals(taskQueryServiceBean.getCreatorId())) {
                inboxBean.setPreviousApprover("mabulaban.c@tawal.com");
            } else {
                inboxBean.setPreviousApprover(taskQueryServiceBean.getCreatorId());
            }
            inboxBean.setStatus("PENDING");
            inboxBean.setSystemName("iGate");
            inboxBean.setType("CNA");
            // check status for saving log details
            if ("fail".equalsIgnoreCase(status)) {
                detailsBean.setTaskNumber(taskQueryServiceBean.getTaskNumber());
                detailsBean.setServiceActionType("mySqlInsert");
                detailsBean.setServiceName("mySqlService");
                detailsBean.setServiceStatus("fail");
                detailsBean.setServiceData(new JSONObject(inboxBean).toString());
                inboxBean.setTaskLogDetails(detailsBean);
                logDao.insertNewLog(inboxBean);
            } else {
                detailsBean.setTaskNumber(taskQueryServiceBean.getTaskNumber());
                detailsBean.setServiceActionType("mySqlInsert");
                detailsBean.setServiceName("mySqlService");
                detailsBean.setServiceStatus("success");
                detailsBean.setServiceData(new JSONObject(taskQueryServiceBean).toString());
                inboxBean.setTaskLogDetails(detailsBean);
                logDao.insertNewLog(inboxBean);
            }
            closeResources(connection, ps, rs);
        }
        return taskQueryServiceBean;
    }

    public ArrayList<UpdateAppsproDBBean> getOldTasks() throws AppsProException {

        ArrayList<UpdateAppsproDBBean> list = new ArrayList();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * from xx_all_tasks_details where  task_status ='ASSIGNED'";
            UpdateAppsproDBBean bean = null;
            ps = connection.prepareStatement(query);
//            ps.setString(1, userName);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new UpdateAppsproDBBean();
                bean.setTaskNumber(rs.getString("task_number"));
                bean.setTaskId(rs.getString("task_id"));
                bean.setUserName(rs.getString("user_name"));
                //emp bean
                bean.setTaskStatus(rs.getString("task_status"));
                bean.setCreationDate(rs.getString("creation_date"));
                bean.setCreatorId(rs.getString("created_by"));
                bean.setUpdatedDate(rs.getString("updated_date"));
                bean.setUpdatedBy(rs.getString("updated_by"));
//                person_number
                bean.setPersonNumber(rs.getString("person_number"));

                list.add(bean);
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public JobExecutionBean getLastFiredJob() throws AppsProException {

        ArrayList<String> list = new ArrayList();

        JobExecutionBean bean = new JobExecutionBean();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * From xx_job_execution_time ORDER BY ID DESC LIMIT 1";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {

                bean.setId(query);
                bean.setJobExecutionTime(rs.getString("job_execution_time"));

                list.add(rs.getString("job_execution_time").toString());
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public void insertJobExecutionTime(JobExecutionBean jobExecutionBean) throws AppsProException {

        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "INSERT INTO xx_job_execution_time (job_execution_time)\n"
                    + "                    VALUES (?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, jobExecutionBean.getJobExecutionTime());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public UpdateAppsproDBBean updateTasks(String prevUser, String nextUser, String taskVersion, String outCome, String taskNumber, UpdateAppsproDBBean taskQueryServiceBean) throws AppsProException {
        String status = null;
        String inboxStatus = "";
        try {

            System.out.println("prevUser : " + prevUser);
            System.out.println("nextUser : " + nextUser);
            System.out.println("outCome : " + outCome);
            System.out.println("taskNumber : " + taskNumber);

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE xx_all_inbox_details \n"
                    + "set task_number=? , action = ? , status=? , previous_approver=? , next_approver=? , display_status=? , delegatee =? \n"
                    + "WHERE task_number = ? ";

            System.out.println(query);

            ps = connection.prepareStatement(query);
            ps.setString(1, taskNumber);

            switch (taskVersion) {

                //the get case will be removed we just created it for testing
                case "TASK_VERSION_REASON_COMPLETED":

                    ps.setString(2, outCome);
                    ps.setString(3, "REJECT".equals(outCome) ? "REJECTED" : "ACCEPTED");
                    ps.setString(4, prevUser);
                    ps.setString(5, prevUser);
                    ps.setString(6, "REJECT".equals(outCome) ? "REJECTED" : "ACCEPTED");
                    ps.setString(7, "");
                    ps.setString(8, taskNumber);
                    inboxStatus = "REJECT".equals(outCome) ? "REJECTED" : "ACCEPTED";

                    break;

                case "TASK_VERSION_REASON_ROUTED":

                    ps.setString(2, outCome);
                    ps.setString(3, "PENDING");
                    ps.setString(4, prevUser);
                    ps.setString(5, nextUser);
                    ps.setString(6, "PENDING");
                    ps.setString(7, "");
                    ps.setString(8, taskNumber);
                    inboxStatus = "PENDING";
                    break;

                case "TASK_VERSION_REASON_DELEGATED":

                    ps.setString(2, outCome);
                    ps.setString(3, "PENDING");
                    ps.setString(4, prevUser);
                    ps.setString(5, nextUser);
                    ps.setString(6, "PENDING");
                    ps.setString(7, nextUser);
                    ps.setString(8, taskNumber);
                    inboxStatus = "PENDING";
                    break;

                default:

                    ps.setString(2, outCome);
                    ps.setString(3, "PENDING");
                    ps.setString(4, prevUser);
                    ps.setString(5, nextUser);
                    ps.setString(6, "PENDING");
                    ps.setString(7, "");
                    ps.setString(8, taskNumber);
                    inboxStatus = "PENDING";

            }

            ps.executeUpdate();

        } catch (SQLException e) {
            status = "fail";
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            status = "fail";
            throw new AppsProException(ex);
        } finally {

            TasksLogDao logDao = new TasksLogDao();
            TasksLogBean inboxBean = new TasksLogBean();
            TaskLogDetailsBean detailsBean = new TaskLogDetailsBean();
            // set data for inbox table
            inboxBean.setTaskNumber(taskNumber);
            inboxBean.setAction(outCome);
            inboxBean.setDisplay_status(inboxStatus);
            inboxBean.setNextApprover(nextUser==null?prevUser:nextUser);
            inboxBean.setPreviousApprover(prevUser);
            inboxBean.setStatus(inboxStatus);
            inboxBean.setDelegatee(nextUser==null?"":nextUser);
            if ("fail".equalsIgnoreCase(status)) {
                detailsBean.setTaskNumber(taskQueryServiceBean.getTaskNumber());
                detailsBean.setServiceActionType("mySqlUpdate");
                detailsBean.setServiceName("mySqlService");
                detailsBean.setServiceStatus("fail");
                detailsBean.setServiceData(new JSONObject(inboxBean).toString());
                inboxBean.setTaskLogDetails(detailsBean);
                logDao.insertNewLog(inboxBean);
            } else {
                detailsBean.setTaskNumber(taskQueryServiceBean.getTaskNumber());
                detailsBean.setServiceActionType("mySqlUpdate");
                detailsBean.setServiceName("mySqlService");
                detailsBean.setServiceStatus("success");
                detailsBean.setServiceData(new JSONObject(inboxBean).toString());
                inboxBean.setTaskLogDetails(detailsBean);
                logDao.insertNewLog(inboxBean);
            }
            closeResources(connection, ps, rs);
        }
        return taskQueryServiceBean;
    }

    public UpdateAppsproDBBean updateNonExistTask(UpdateAppsproDBBean taskQueryServiceBean) throws AppsProException {
        String status = null;
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE xx_all_tasks_details \n"
                    + "set task_exist_flag='F'\n"
                    + "WHERE task_number = ? ";

            ps = connection.prepareStatement(query);
//             ps.setString(1, taskQueryServiceBean.getTaskStatus());
            ps.setString(1, taskQueryServiceBean.getTaskNumber());
//            ps.setString(3, taskQueryServiceBean.getPersonNumber());

            ps.executeUpdate();

        } catch (SQLException e) {
            status = "fail";
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            status = "fail";
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }

        return taskQueryServiceBean;
    }

    // udate task details table in mySQL
    public UpdateAppsproDBBean updateTaskOutcome(UpdateAppsproDBBean taskQueryServiceBean) throws AppsProException {
        String status = null;
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE xx_all_tasks_details \n"
                    + "set task_number=IFNULL(?,task_number) , task_id = IFNULL(?,task_id) , user_name=IFNULL(?,user_name) , "
                    + "task_status = IFNULL(?,task_status) , updated_date = IFNULL(?,updated_date), updated_by  = IFNULL(?,updated_by) , "
                    + "person_number = IFNULL(?,person_number) ,  assigned_date=IFNULL(?,assigned_date) , workflow_pattern=IFNULL(?,workflow_pattern) ,"
                    + "task_outcome=IFNULL(?,task_outcome) , task_version=IFNULL(?,task_version) , task_version_reason=IFNULL(?,task_version_reason) ,"
                    + "sub_state=IFNULL(?,sub_state) WHERE task_number = ? ";

            ps = connection.prepareStatement(query);
            ps.setString(1, taskQueryServiceBean.getTaskNumber());
            ps.setString(2, taskQueryServiceBean.getTaskId());
            ps.setString(3, taskQueryServiceBean.getUserName());
            ps.setString(4, taskQueryServiceBean.getTaskStatus());
            ps.setString(5, taskQueryServiceBean.getUpdatedDate());
            ps.setString(6, taskQueryServiceBean.getUpdatedBy());
            ps.setString(7, taskQueryServiceBean.getPersonNumber());
            ps.setString(8, taskQueryServiceBean.getAssignedDate());
            ps.setString(9, taskQueryServiceBean.getWorkflowPattern());
            ps.setString(10, taskQueryServiceBean.getTaskOutcome());
            ps.setString(11, taskQueryServiceBean.getTaskVersion());
            ps.setString(12, taskQueryServiceBean.getTaskVersionReason());
            ps.setString(13, taskQueryServiceBean.getSubState());
            ps.setString(14, taskQueryServiceBean.getTaskNumber());
            ps.executeUpdate();

        } catch (SQLException e) {
            status = "fail";
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            status = "fail";
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return taskQueryServiceBean;
    }

    public ArrayList<UpdateAppsproDBBean> checkOldTasks(UpdateAppsproDBBean updateAppsproDBBean) throws AppsProException {

        ArrayList<UpdateAppsproDBBean> list = new ArrayList();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * from xx_all_inbox_details where  task_number =?";
            UpdateAppsproDBBean bean = null;
            ps = connection.prepareStatement(query);
            ps.setString(1, updateAppsproDBBean.getTaskNumber());
//            ps.setString(2, updateAppsproDBBean.getUserName());
//            ps.setString(3, updateAppsproDBBean.getAssignedDate());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new UpdateAppsproDBBean();
                bean.setTaskNumber(rs.getString("task_number"));
                bean.setUserName(rs.getString("next_approver"));
                //emp bean
                bean.setTaskStatus(rs.getString("status"));
//                bean.setCreationDate(rs.getString("creation_date"));
//                bean.setCreatorId(rs.getString("created_by"));
//                bean.setUpdatedDate(rs.getString("updated_date"));
//                bean.setUpdatedBy(rs.getString("updated_by"));
////                person_number
//                bean.setPersonNumber(rs.getString("person_number"));
//                bean.setWorkflowPattern(rs.getString("workflow_pattern"));
//                bean.setAssignedDate(rs.getString("assigned_date"));
//                bean.setId(rs.getString("id"));

                list.add(bean);
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public ArrayList<UpdateAppsproDBBean> checkNonUpdatedOldTasks(UpdateAppsproDBBean updateAppsproDBBean) throws AppsProException {

        ArrayList<UpdateAppsproDBBean> list = new ArrayList();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * from xx_all_tasks_details where  task_number =?";
            UpdateAppsproDBBean bean = null;
            ps = connection.prepareStatement(query);
            ps.setString(1, updateAppsproDBBean.getTaskNumber());
//            ps.setString(2, updateAppsproDBBean.getAssignedDate());

            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new UpdateAppsproDBBean();
                bean.setTaskNumber(rs.getString("task_number"));
                bean.setTaskId(rs.getString("task_id"));
                bean.setUserName(rs.getString("user_name"));
                //emp bean
//                bean.setTaskStatus(rs.getString("task_status"));
                bean.setCreationDate(rs.getString("creation_date"));
                bean.setCreatorId(rs.getString("created_by"));
//                bean.setUpdatedDate(rs.getString("updated_date"));
//                bean.setUpdatedBy(rs.getString("updated_by"));
//                person_number
                bean.setPersonNumber(rs.getString("person_number"));
                bean.setWorkflowPattern(rs.getString("workflow_pattern"));
                bean.setAssignedDate(rs.getString("assigned_date"));
                bean.setId(rs.getString("id"));

                list.add(bean);
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public void checkVisibleOrHiddenUpdateFlag(String taskNumber) throws AppsProException {
        String status = null;
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE xx_all_tasks_details \n"
                    + "set task_exist_flag='F'\n"
                    + "WHERE task_number = ?";

            ps = connection.prepareStatement(query);
//             ps.setString(1, taskQueryServiceBean.getTaskStatus());
            ps.setString(1, taskNumber);

//            ps.setString(3, taskQueryServiceBean.getPersonNumber());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public void updateNonExistTaskStatus(String taskNumber, String taskStatus) throws AppsProException {
        String status = null;
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE xx_all_tasks_details \n"
                    + "set task_status= ? \n"
                    + "WHERE task_number = ? AND task_status != 'COMPLETED' ";

            ps = connection.prepareStatement(query);
//             ps.setString(1, taskQueryServiceBean.getTaskStatus());
            ps.setString(1, taskStatus);
            ps.setString(2, taskNumber);
//            ps.setString(3, taskQueryServiceBean.getPersonNumber());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public void updateTaskFromTaskSequence(UpdateAppsproDBBean taskDetailsFromDb, UpdateAppsproDBBean taskDetailsFromWS, String id) throws AppsProException {
        String status = null;
        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "UPDATE xx_all_tasks_details \n"
                    + "set task_status = ? , updated_date = ? , updated_by  = ? , task_outcome = ? , task_version = ? , task_version_reason = ? , sub_state = ? \n"
                    + "WHERE task_number = ? AND user_name=? AND id = ?";;

            ps = connection.prepareStatement(query);
            ps.setString(1, taskDetailsFromWS.getTaskStatus());
            ps.setString(2, taskDetailsFromWS.getUpdatedDate());
            ps.setString(3, taskDetailsFromWS.getUpdatedBy());
            ps.setString(4, taskDetailsFromWS.getTaskOutcome());
            ps.setString(5, taskDetailsFromWS.getTaskVersion());
            ps.setString(6, taskDetailsFromWS.getTaskVersionReason());
            ps.setString(7, taskDetailsFromWS.getSubState());
            ps.setString(8, taskDetailsFromDb.getTaskNumber());
            ps.setString(9, taskDetailsFromDb.getUserName());
            ps.setString(10, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            status = "fail";
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            status = "fail";
            throw new AppsProException(ex);
        } finally {

            closeResources(connection, ps, rs);
        }
//        return taskQueryServiceBean;
    }

    public ArrayList<UpdateAppsproDBBean> checkDelegatee(UpdateAppsproDBBean updateAppsproDBBean) throws AppsProException {

        ArrayList<UpdateAppsproDBBean> list = new ArrayList();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * from xx_all_inbox_details where  task_number =?";
            UpdateAppsproDBBean bean = null;
            ps = connection.prepareStatement(query);
            ps.setString(1, updateAppsproDBBean.getTaskNumber());
//            ps.setString(2, updateAppsproDBBean.getUserName());
//            ps.setString(3, updateAppsproDBBean.getAssignedDate());
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new UpdateAppsproDBBean();
//                bean.setTaskNumber(rs.getString("task_number"));  
                bean.setUserName(rs.getString("delegatee"));

                list.add(bean);
            }

            if (list.get(0).getUserName() != null) {

                System.out.print("YES THERE IS DELEGATEE");

            } else {
                System.out.print("NO THERE IS NO DELEGATEE");

            }

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public static void main(String[] args) throws AppsProException {

        UpdateAppsproDBBean updateAppsproDBBean = new UpdateAppsproDBBean();

        updateAppsproDBBean.setTaskNumber("204396");

//       System.out.print("NO THERE IS NO DELEGATEE");
        QueryTasksDao queryTasksDao = new QueryTasksDao();

        queryTasksDao.checkDelegatee(updateAppsproDBBean);

    }

}
