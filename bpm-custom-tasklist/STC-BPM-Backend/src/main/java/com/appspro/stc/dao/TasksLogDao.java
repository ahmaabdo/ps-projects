
package com.appspro.stc.dao;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.TaskLogDetailsBean;
import com.appspro.bpm.bean.TasksLogBean;
import com.appspro.stc.database.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


public class TasksLogDao extends DatabaseConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    PreparedStatement ps2;
    ResultSet rs2;

    public List<TasksLogBean> getAllTaskDetails() throws AppsProException {
        List<TasksLogBean> list = new ArrayList();

        String sql = "";
        try {
            connection = DatabaseConnection.getConnection();
            sql = "SELECT * FROM xx_all_inbox_details";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            TasksLogBean B = null;
            while (rs.next()) {
                B = new TasksLogBean();
                B.setTaskNumber(rs.getString("task_number"));
                // getting details for this task number
                String sql2 = "SELECT * FROM xx_stc_log_details WHERE taskNumber=?";
                ps2 = connection.prepareStatement(sql2);
                ps2.setString(1, rs.getString("task_number"));
                rs2 = ps2.executeQuery();
                TaskLogDetailsBean dBean = null;
                ArrayList<TaskLogDetailsBean> detailsList = new ArrayList();
                while (rs2.next()) {
                    dBean = new TaskLogDetailsBean();
                    dBean.setId(rs2.getInt("id"));
                    dBean.setTaskNumber(rs2.getString("taskNumber"));
                    dBean.setServiceName(rs2.getString("serviceName"));
                    dBean.setServiceStatus(rs2.getString("serviceStatus"));
                    dBean.setServiceActionType(rs2.getString("serviceActionType"));
                    dBean.setServiceData(rs2.getString("serviceData"));
                    detailsList.add(dBean);
                }
                /////////////////////////////////
                B.setTaskNumber(rs.getString("task_number"));
                B.setPreviousApprover(rs.getString("previous_approver"));
                B.setNextApprover(rs.getString("next_approver"));
                B.setCompanyName(rs.getString("company_name"));
                B.setStatus(rs.getString("status"));
                B.setSystemName(rs.getString("system_name"));
                B.setType(rs.getString("type"));
                B.setDisplay_status(rs.getString("display_status"));
                B.setAction(rs.getString("action"));
                B.setDelegatee(rs.getString("delegatee"));
                B.setTaskDetails(detailsList);
                list.add(B);
            }

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public TasksLogBean getLogByTaskNumber(String taskNumber) throws AppsProException {
        TasksLogBean B = new TasksLogBean();
        String sql = "";
        try {
            connection = DatabaseConnection.getConnection();
            sql = "SELECT * FROM xx_all_inbox_details WHERE task_number=?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, taskNumber);
            rs = ps.executeQuery();
            while (rs.next()) {
                B.setTaskNumber(rs.getString("task_number"));
                B.setPreviousApprover(rs.getString("previous_approver"));
                B.setNextApprover(rs.getString("next_approver"));
                B.setCompanyName(rs.getString("company_name"));
                B.setStatus(rs.getString("status"));
                B.setSystemName(rs.getString("system_name"));
                B.setType(rs.getString("type"));
                B.setDisplay_status(rs.getString("display_status"));
                B.setAction(rs.getString("action"));
                B.setDelegatee(rs.getString("delegatee"));
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return B;
    }
    
    public TasksLogBean getAllTasksLogByTaskNumber(String taskNumber) throws AppsProException {
        ArrayList<TaskLogDetailsBean> detailsList = new ArrayList();
        TasksLogBean B = new TasksLogBean();
        String sql = "";
        try {
            connection = DatabaseConnection.getConnection();
            sql = "SELECT * FROM xx_all_inbox_details, xx_stc_log_details WHERE xx_all_inbox_details.task_number=xx_stc_log_details.taskNumber and xx_stc_log_details.taskNumber=?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, taskNumber);
            rs = ps.executeQuery();

            TaskLogDetailsBean dBean = null;
            while (rs.next()) {
                B.setTaskNumber(rs.getString("task_number"));
                B.setPreviousApprover(rs.getString("previous_approver"));
                B.setNextApprover(rs.getString("next_approver"));
                B.setCompanyName(rs.getString("company_name"));
                B.setStatus(rs.getString("status"));
                B.setSystemName(rs.getString("system_name"));
                B.setType(rs.getString("type"));
                B.setDisplay_status(rs.getString("display_status"));
                B.setAction(rs.getString("action"));
                B.setDelegatee(rs.getString("delegatee"));
                dBean = new TaskLogDetailsBean();
                dBean.setId(rs.getInt("id"));
                dBean.setTaskNumber(rs.getString("taskNumber"));
                dBean.setServiceName(rs.getString("serviceName"));
                dBean.setServiceStatus(rs.getString("serviceStatus"));
                dBean.setServiceActionType(rs.getString("serviceActionType"));
                dBean.setServiceData(rs.getString("serviceData"));
                detailsList.add(dBean);
            }
            B.setTaskDetails(detailsList);
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return B;
    }

    public TasksLogBean insertNewLog(TasksLogBean Body) throws AppsProException {
        String sql = "";
        boolean taskFound=false;
        try {
            connection = DatabaseConnection.getConnection();
            // check for task existance
            sql = "SELECT * FROM xx_all_inbox_details WHERE task_number=? ";
            ps = connection.prepareStatement(sql);
            ps.setString(1, Body.getTaskNumber());
            rs = ps.executeQuery();
            while (rs.next()) {
               taskFound=true; 
            }
            if(taskFound){
              // insert int log details table 
              insertLogDetails(Body.getTaskLogDetails());
            }else{
                // insert into inbox table and log details
              insertInboxDetails(Body);  
              insertLogDetails(Body.getTaskLogDetails()); 
            }
            
        } catch (SQLException e) {
            throw new AppsProException(e);
        }
        return Body;
    }

    public TasksLogBean insertInboxDetails(TasksLogBean Body) throws AppsProException {
        String sql = "";
        try {
            connection = DatabaseConnection.getConnection();
            sql = " insert into xx_all_inbox_details "
                        + " (task_number,previous_approver,next_approver,company_name,status,system_name,type, "
                        + " display_status,action,delegatee)"
                        + "values (?,?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(sql);
                ps.setString(1, Body.getTaskNumber());
                ps.setString(2, Body.getPreviousApprover());
                ps.setString(3, Body.getNextApprover());
                ps.setString(4, Body.getCompanyName());
                ps.setString(5, Body.getStatus());
                ps.setString(6, Body.getSystemName());
                ps.setString(7, Body.getType());
                ps.setString(8, Body.getDisplay_status());
                ps.setString(9, Body.getAction());
                ps.setString(10, Body.getDelegatee());
                ps.executeUpdate(); 
        } catch (SQLException e) {
            throw new AppsProException(e);
        }
        return Body;
    }
    
    public TaskLogDetailsBean insertLogDetails(TaskLogDetailsBean Body) throws AppsProException {
        String sql = "";
        try {
            connection = DatabaseConnection.getConnection();
            sql = " insert into xx_stc_log_details "
                    + " (taskNumber,serviceName,serviceStatus,serviceActionType,serviceData)"
                    + "values (?,?,?,?,?)";
            ps = connection.prepareStatement(sql);
            ps.setString(1, Body.getTaskNumber());
            ps.setString(2, Body.getServiceName());
            ps.setString(3, Body.getServiceStatus());
            ps.setString(4, Body.getServiceActionType());
            ps.setString(5, Body.getServiceData());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new AppsProException(e);
        }
        return Body;
    }

    public TaskLogDetailsBean editTaskLogDetails(TaskLogDetailsBean Body) throws AppsProException {
        String sql = "";
        try {
            connection = DatabaseConnection.getConnection();
            sql = " update xx_stc_log_details set "
                    + " taskNumber =IFNULL(?,taskNumber),serviceName =IFNULL(?,serviceName),serviceStatus=IFNULL(?,serviceStatus),"
                    + "serviceActionType=IFNULL(?,serviceActionType),serviceData=IFNULL(?,serviceData)"
                    + " where id = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, Body.getTaskNumber());
            ps.setString(2, Body.getServiceName());
            ps.setString(3, Body.getServiceStatus());
            ps.setString(4, Body.getServiceActionType());
            ps.setString(5, Body.getServiceData());
            ps.setInt(6, Body.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new AppsProException(e);
        }
        return Body;
    }

    public static void main(String ar[]) throws AppsProException {
        TasksLogDao dw = new TasksLogDao();
        TasksLogBean Body=new TasksLogBean();
        Body.setTaskNumber("44555");
        dw.insertInboxDetails(Body);
//        List<TasksLogBean> ls=new ArrayList<>();
//        for (int i = 0; i < ls.size(); i++) {
//          ls = dw.getAllTaskDetails();   
//        System.out.println(new JSONArray(ls).toString());
//        }
    }
}
