/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.stc.database;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.stc.dao.SetupProjectDao;
import java.sql.*;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import log4j.log4jExample;

/**
 *
 * @author Lenovo
 */
public class DatabaseConnection {
    
    
//    private static String url = "jdbc:mysql://10.21.60.127:3306/";
//    private static String dbName = "stc_fusion_bpm_schema";
//    private static String driver = "com.mysql.jdbc.Driver";
//    private static String userName = "root";
//    private static String password = "P@ssw0rd";
    

    private static String url = "jdbc:mysql://localhost:3306/";
    private static String dbName = "stc_schema";
    private static String driver = "com.mysql.jdbc.Driver";
    private static String userName = "root";
    private static String password = "admin";
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(log4jExample.class.getName());

    protected static String CONNECTION_TYPE = "JDBC";
    private static String schemaName = "DEV";

    public static Connection getConnection(boolean commit) throws AppsProException {
        if (CONNECTION_TYPE.equalsIgnoreCase("JDBC")) {
            return getJDBCConnection(commit);
        } else {
            return getDSConnection(commit);
        }
    }

    public static Connection getConnection() throws AppsProException {
        if (CONNECTION_TYPE.equalsIgnoreCase("JDBC")) {
            return getJDBCConnection();
        } else {
            return getDSConnection();
        }
    }

    private static Connection getJDBCConnection(boolean autoCommit) throws AppsProException {

        Connection connection = null;

        try {
            Class.forName(driver).newInstance();
            connection
                    = (Connection) DriverManager.getConnection(url + dbName, userName, password);
            connection.setAutoCommit(autoCommit);
        } catch (ClassNotFoundException e) {
            throw new AppsProException(e);
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (InstantiationException ex) {
            throw new AppsProException(ex);
        } catch (IllegalAccessException ex) {
            throw new AppsProException(ex);
        }

        return connection;
    }

    private static Connection getJDBCConnection() throws AppsProException {

        Connection connection = null;

        try {
            Class.forName(driver).newInstance();
            connection
                    = (Connection) DriverManager.getConnection(url + dbName, userName, password);
        } catch (ClassNotFoundException e) {
            throw new AppsProException(e);
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (InstantiationException ex) {
            throw new AppsProException(ex);
        } catch (IllegalAccessException ex) {
            throw new AppsProException(ex);
        }

        return connection;
    }

    private static Connection getDSConnection() throws AppsProException {
        return getDSConnection(false);//CommonConfigReader.getValue("DS_NAME"));
    }

    private static Connection getDSConnection(boolean autoCommit) throws AppsProException {
        Connection connection = getDSConnection(false);//CommonConfigReader.getValue("DS_NAME"));
        try {
            connection.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            throw new AppsProException(e);
        }

        return connection;
    }

    public void closeResources(Connection connection, Statement stmt, ResultSet rs) throws AppsProException {
        closeResources(rs);
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, PreparedStatement stmt, ResultSet rs) throws AppsProException {
        closeResources(rs);
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, Statement stmt) throws AppsProException {
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, PreparedStatement stmt) throws AppsProException {
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Statement stmt, ResultSet rs) throws AppsProException {
        closeResources(rs);
        closeResources(stmt);
    }

    public void closeResources(PreparedStatement stmt, ResultSet rs) throws AppsProException {
        closeResources(rs);
        closeResources(stmt);
    }

    public void closeResources(Connection connection) throws AppsProException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
            // TODO
        }
    }

    public void closeResources(PreparedStatement preparedStatement) throws AppsProException {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
            // TODO
        }
    }

    public void closeResources(Statement stmt) throws AppsProException {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
            // TODO
        }
    }

    public void closeResources(ResultSet rs) throws AppsProException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
            // TODO
        }
    }
    
    public static void main(String ar[]) throws AppsProException{
        try{
            int x = 1/0;
        }catch(Exception e){
        throw new AppsProException(e);
        }
    }
    
    

}
