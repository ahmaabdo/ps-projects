/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.stc.dao;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.EmployeeDetailsBean;
import com.appspro.bpm.bean.JobExecutionBean;
import com.appspro.bpm.bean.ReportDetailsBean;
import com.appspro.bpm.bean.ReportParametersBean;
import com.appspro.stc.database.DatabaseConnection;

import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.bpm.bean.TaskQueryServiceBean;
import com.appspro.bpm.bean.UpdateAppsproDBBean;
import java.sql.CallableStatement;

import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class SetupProjectDao extends DatabaseConnection {

    private static SetupProjectDao instance = null;
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public List<SetupProjectBean> getAll() throws AppsProException {

        List<SetupProjectBean> list = new ArrayList();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * from stc_config_details";
            SetupProjectBean bean = null;
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new SetupProjectBean();
                bean.setHostName(rs.getString("host_name"));
                bean.setCompanyName(rs.getString("company_name"));
                bean.setAdminUsername(rs.getString("hcm_admin_user"));
                bean.setAdminPassword(rs.getString("hcm_admin_pass"));
                bean.setId(rs.getString("id"));

                list.add(bean);
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }

    public SetupProjectBean insertOrUpdatePosition(SetupProjectBean bean,
            String transactionType) throws AppsProException {
        try {
            connection = DatabaseConnection.getConnection();

            if (transactionType.equals("ADD")) {
                String query
                        = "INSERT INTO stc_config_details (host_name,company_name,hcm_admin_user,hcm_admin_pass)\n"
                        + "                    VALUES ( ?, ? ,? ,?)";
                ps = connection.prepareStatement(query);

                ps.setString(1, bean.getHostName());
                ps.setString(2, bean.getCompanyName());
                ps.setString(3, bean.getAdminUsername());
                ps.setString(4, bean.getAdminPassword());

                ps.executeUpdate();

            } else if (transactionType.equals("EDIT")) {
                connection = DatabaseConnection.getConnection();

                String query = "update   stc_config_details set company_name=? ,host_name=?,hcm_admin_user=?,hcm_admin_pass=?\n"
                        + " where ID=?";
                ;
                ps = connection.prepareStatement(query);
                ps.setString(1, bean.getCompanyName());
                ps.setString(2, bean.getHostName());
                ps.setString(3, bean.getAdminUsername());
                ps.setString(4, bean.getAdminPassword());

                ps.setInt(5, Integer.parseInt(bean.getId()));
                ps.executeUpdate();

            }

        } catch (Exception e) {
            //("Error: ");
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }

    public SetupProjectBean getByCompanyName(String companyName) throws AppsProException {
        SetupProjectBean bean = new SetupProjectBean();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = " SELECT * from stc_config_details WHERE company_name = ?";
            ps = connection.prepareStatement(query);

            ps.setString(1, companyName);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean.setCompanyName(rs.getString("company_name"));
                bean.setHostName(rs.getString("host_name"));
                bean.setAdminUsername(rs.getString("hcm_admin_user"));
                bean.setAdminPassword(rs.getString("hcm_admin_pass"));

            }
        } catch (Exception e) {
            throw new AppsProException(e);
        } finally {
            closeResources(connection, ps, rs);
        }
        return bean;
    }
    
    public ReportDetailsBean getReportDetails(String taskNameSpace) throws AppsProException {
        
        ReportDetailsBean reportDetailsBean = new ReportDetailsBean();
        ArrayList<ReportParametersBean> reportParamList = new ArrayList<>();

        try {

            connection = DatabaseConnection.getConnection();
            String query
                    = "SELECT * FROM xx_stc_segment_details, xx_stc_segment_report_details WHERE xx_stc_segment_details.id=xx_stc_segment_report_details.xx_segment_details_id and xx_stc_segment_details.task_name_space = ?";

            System.out.println(query);

            ps = connection.prepareStatement(query);
            ps.setString(1, taskNameSpace);
            rs = ps.executeQuery();
            while (rs.next()) {
                ReportParametersBean bean=new ReportParametersBean();
                reportDetailsBean.setReportName(rs.getString("report_name"));
                reportDetailsBean.setReportPath(rs.getString("report_path"));
                reportDetailsBean.setReportTitle(rs.getString("report_title"));
                reportDetailsBean.setTaskNameSpace(rs.getString("task_name_space"));
                bean.setReportParameter(rs.getString("report_parameter"));
                bean.setMappedParameter(rs.getString("mapped_parameter")); 
                
                reportParamList.add(bean);
            }
            reportDetailsBean.setReportParameters(reportParamList);
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return reportDetailsBean;
    }
    
    
        public void postNewTasks(UpdateAppsproDBBean taskQueryServiceBean , EmployeeDetailsBean employeeDetails) throws AppsProException {
        

        try {

            connection = DatabaseConnection.getConnection();
            String query
                        = "INSERT INTO xx_all_tasks_details (task_number,task_id,user_name,task_status,creation_date,created_by,updated_date,updated_by,person_number)\n"
                        + "                    VALUES ( ?, ? ,? ,?,?,?,?,?,?)";

            
            
            System.out.println(taskQueryServiceBean.getTaskNumber());
            System.out.println(employeeDetails.getPersonNumber());
            System.out.println(employeeDetails.getUserName());

            ps = connection.prepareStatement(query);
            ps.setString(1, taskQueryServiceBean.getTaskNumber());
            ps.setString(2, taskQueryServiceBean.getTaskId());
            ps.setString(3, employeeDetails.getUserName());
            ps.setString(4, taskQueryServiceBean.getTaskStatus());
            ps.setString(5, taskQueryServiceBean.getCreationDate());
            ps.setString(6, taskQueryServiceBean.getCreatorId());
            ps.setString(7, taskQueryServiceBean.getUpdatedDate());
            ps.setString(8, taskQueryServiceBean.getUpdatedBy());
            ps.setString(9, employeeDetails.getPersonNumber());
            
            
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        
    }
        
        
      public  ArrayList<UpdateAppsproDBBean> getOldTasks(String userName) throws AppsProException {

        ArrayList<UpdateAppsproDBBean> list = new ArrayList();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * from xx_all_tasks_details where user_name = ?";
            UpdateAppsproDBBean bean = null;
            ps = connection.prepareStatement(query);
            ps.setString(1, userName);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new UpdateAppsproDBBean();
                bean.setTaskNumber(rs.getString("task_number"));
                bean.setTaskId(rs.getString("task_id"));
//                bean.setUserName(rs.getString("user_name"));
                //emp bean
                bean.setTaskStatus(rs.getString("task_status"));
                bean.setCreationDate(rs.getString("creation_date"));
                bean.setCreatorId(rs.getString("created_by"));
                  bean.setUpdatedDate(rs.getString("updated_date"));
                bean.setUpdatedBy(rs.getString("updated_by"));
//                person_number
//                bean.setPersonNumber(rs.getString("person_number"));

                list.add(bean);
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        return list;
    }
      
      
       public  JobExecutionBean getLastFiredJob() throws AppsProException {

        ArrayList<String> list = new ArrayList();
        
        JobExecutionBean bean = new JobExecutionBean();
        try {
            connection = DatabaseConnection.getConnection();
            String query
                    = "select * From xx_job_execution_time ORDER BY ID DESC LIMIT 1";
           
            ps = connection.prepareStatement(query);
      
            rs = ps.executeQuery();
            while (rs.next()) {
                
                bean.setId(query);
                 bean.setJobExecutionTime(rs.getString("job_execution_time"));
                System.out.println(rs.getString("job_execution_time"));

                 list.add(rs.getString("job_execution_time").toString());
            }
        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
       return bean;
    }
       
       
               public void insertJobExecutionTime(JobExecutionBean jobExecutionBean) throws AppsProException {
        

        try {

            connection = DatabaseConnection.getConnection();
            String query
                        = "INSERT INTO xx_job_execution_time (job_execution_time)\n"
                        + "                    VALUES (?)";

            
            


            ps = connection.prepareStatement(query);
            ps.setString(1, jobExecutionBean.getJobExecutionTime());

            
            
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new AppsProException(e);
        } catch (AppsProException ex) {
            throw new AppsProException(ex);
        } finally {
            closeResources(connection, ps, rs);
        }
        
    }

}
