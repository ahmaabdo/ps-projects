/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.stc.handler;

import com.appspro.bpm.appsproException.AppsProException;
import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.stc.dao.SetupProjectDao;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lenovo
 */
public class SetupProjectHandler {

    private SetupProjectDao dao = new SetupProjectDao();

    public List<SetupProjectBean> getAll() throws AppsProException {

        List<SetupProjectBean> list = dao.getAll();

        return list;

    }

    public SetupProjectBean getByCompanyName(String companyName) throws AppsProException {
        SetupProjectBean bean = new SetupProjectBean();
        bean = dao.getByCompanyName(companyName);

        return bean;
    }

    public SetupProjectBean insertOrUpdatePosition(SetupProjectBean bean,
            String transactionType) throws AppsProException {
        return dao.insertOrUpdatePosition(bean, transactionType);
    }

}
