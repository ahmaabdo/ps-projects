/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QueryTasksJobScheduler;

/**
 *
 * @author user
 */

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class QueryTasksScheduler {
    
        public static void main (String [] args ){
        
        
        try {
           // specify the job' s details..
           JobDetail job = JobBuilder.newJob(QueryTasksJob.class).withIdentity("Query Tasks Job")
                                     .build();
           // specify the running period of the job
           Trigger trigger = TriggerBuilder.newTrigger()
                   .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                                      .withIntervalInMinutes(1)
                                                      .repeatForever())
                                                      .build();
           //schedule the job
        //      SchedulerFactory schFactory = new StdSchedulerFactory();
           Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
           sch.start();
           sch.scheduleJob(job, trigger);
           
        } catch (SchedulerException e) {
            System.out.println(e);
        }
    }
    
}
