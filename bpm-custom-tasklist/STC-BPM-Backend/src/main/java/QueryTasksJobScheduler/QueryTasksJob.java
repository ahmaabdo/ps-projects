/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QueryTasksJobScheduler;

import com.appspro.bpm.bean.SetupProjectBean;
import com.appspro.bpm.updateappsprodb.GetAndCompareTasks;
import com.appspro.stc.dao.SetupProjectDao;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;


/**
 *
 * @author user
 */
public class QueryTasksJob implements Job {

//    private final Logger log = LoggerFactory.getLogger(QueryTasksJob.class);

    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {

        try {
                    
        
            
            
            GetAndCompareTasks getAndCompareTasks = new GetAndCompareTasks();
            SetupProjectDao setupProjectDao=new SetupProjectDao();
             List<SetupProjectBean> list = setupProjectDao.getAll();
             for(SetupProjectBean bean:list){
                 
                 Date date = new Date();
                 Timestamp ts = new Timestamp(date.getTime());
                 System.out.print("START FIRED JOB FUNCTION AT : " + ts);
                 
               getAndCompareTasks.jobExecutionFun(bean.getCompanyName());
               Date date2 = new Date();
               
               Timestamp ts2 = new Timestamp(date2.getTime());
               
               System.out.print("END FIRED JOB FUNCTION AT : " + ts2);
               System.out.print("============================================================================================================================================");
             }
        } catch (Exception ex) {
            Logger.getLogger(QueryTasksJob.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }
    
    
    public static void runJob() {
        try {
            // specify the job' s details..
            JobDetail job = JobBuilder.newJob(QueryTasksJob.class).withIdentity("Query Tasks Job")
                    .build();
            // specify the running period of the job
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInMinutes(1)
                            .repeatForever())
                    .build();
            //schedule the job
            //      SchedulerFactory schFactory = new StdSchedulerFactory();
            Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
            sch.start();
            sch.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            System.out.println(e);
        }

    }

}
