package filter;

import com.gtech.common.util.StringUtilities;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import login.LoginBean;

public class SecurityFilter implements Filter {
    private FilterConfig _filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }

    public void destroy() {
        _filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException,
                                                   ServletException {

        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        String requestURI = req.getRequestURI();

        if (requestURI.contains("Login")) {
            chain.doFilter(request, response);
            return;
        }
    
        if (requestURI.contains("/faces/resources") ||
            requestURI.contains(".css") ||
            requestURI.contains(".js") ||
            requestURI.contains(".png") ||
            requestURI.contains(".xml") ||
            requestURI.contains("/adf/") ||
            requestURI.contains("/afr/") ||
            requestURI.contains("/bi/") ||
            requestURI.contains("/webservice/rest") ||
            requestURI.contains("/InsertPaymentOrderServicePort")) {
            
            chain.doFilter(request, response);
            return;
        }

        HttpSession session = req.getSession(false);
        if(requestURI.contains("/faces/calender/HijriCalender.jsp")){
            chain.doFilter(request, response);
            return;
        }

        if (session == null || session.getAttribute("loggedInUser") == null || StringUtilities.isEmpty(session.getAttribute("loggedInUser")+"")) {
            res.sendRedirect(req.getContextPath() + "/faces/Login");
            return;
        } else {
            
            LoginBean loginBean = (LoginBean)session.getAttribute("loggedBean");

            if(checkPermition(requestURI,loginBean)){
                chain.doFilter(request, response);
            } else {
                res.sendRedirect(req.getContextPath() + "/faces/MainPage");
                chain.doFilter(request, response);
            }
        }
    }
    
    
    private boolean checkPermition(String urlContent, LoginBean loginBean){
        boolean result= true;
        String listOfPermition= loginBean.getPermition();
        
        return result;
    }
}
