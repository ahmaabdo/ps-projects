package com.gtech.faces.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;


public class MessageUtilities {
    
    private MessageUtilities() {
    }
    
    public static void createInfoMessage(String s) {
        createMessage(s ,FacesMessage.SEVERITY_INFO) ;
    }

    public static void createErrorMessage(String s) {
        createMessage(s,FacesMessage.SEVERITY_ERROR) ;
    }
    
    public static void createWarnMessage(String s) {
        createMessage(s,FacesMessage.SEVERITY_WARN) ;
    }
    
    public static void createInfoMessageWithRedirect(String s) {
        createMessageWithRedirect(s ,FacesMessage.SEVERITY_INFO) ;
    }

    public static void createErrorMessageWithRedirect(String s) {
        createMessageWithRedirect(s,FacesMessage.SEVERITY_ERROR) ;
    }
    
    public static void createWarnMessageWithRedirect(String s) {
        createMessageWithRedirect(s,FacesMessage.SEVERITY_WARN) ;
    }
    
    public static void createFatalMessage(String s) {
        createMessage(s,FacesMessage.SEVERITY_FATAL) ;
    }

    public static void createMessage(String s , Severity severity) {
        
        FacesContext facescontext = FacesContext.getCurrentInstance() ;
        facescontext.addMessage(null, new FacesMessage(severity,s, null)) ;
    }
    
    public static void createMessageWithRedirect(String s , Severity severity) {
        
        FacesContext facescontext = FacesContext.getCurrentInstance() ;
        facescontext.addMessage(null, new FacesMessage(severity,s, null)) ;
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setRedirect(true);
    }
}
