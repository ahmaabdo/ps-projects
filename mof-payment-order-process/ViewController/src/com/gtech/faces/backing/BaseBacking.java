package com.gtech.faces.backing;

import com.gtech.common.exception.GTechException;

public interface BaseBacking {
    
   public String initialize() throws GTechException;
   
}