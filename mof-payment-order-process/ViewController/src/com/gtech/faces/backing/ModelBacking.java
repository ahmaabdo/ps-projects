package com.gtech.faces.backing;

import com.gtech.common.exception.GTechException;
import com.gtech.faces.beans.FacesContextMessageBean;

import com.gtech.faces.util.MessageReader;
import com.gtech.faces.util.MessageUtilities;

import java.util.List;

public abstract class ModelBacking implements BaseBacking {
    
    public ModelBacking() {
    }

    public abstract String initialize() throws GTechException;

    public void createMessages(List messageList) {

        if (messageList == null) {
            return;
        }
        
        for (int i = 0; i < messageList.size(); i++) {
            
            FacesContextMessageBean messageBean = (FacesContextMessageBean)messageList.get(i);
            
            if (messageBean.getMessageType() == FacesContextMessageBean.MessageType.ERROR) {
                MessageUtilities.createErrorMessage(MessageReader.getMessage(messageBean));
            } else if (messageBean.getMessageType() == FacesContextMessageBean.MessageType.INFO) {
                MessageUtilities.createInfoMessage(MessageReader.getMessage(messageBean));
            } else if (messageBean.getMessageType() == FacesContextMessageBean.MessageType.WARNING) {
                MessageUtilities.createWarnMessage(MessageReader.getMessage(messageBean));
            }
        }
    }

}
