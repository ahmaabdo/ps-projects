package com.gtech.faces.lifecycle;

import java.util.HashMap;
import java.util.Map;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.*;

import oracle.adfinternal.view.faces.lifecycle.LifecycleFactoryImpl;


public class FacesLifeCycleFactory extends LifecycleFactoryImpl {

    private Map<String, Lifecycle> cache;
    LifecycleFactory p1 ;

    public FacesLifeCycleFactory() {
        super(null);
        cache = new HashMap<String, Lifecycle>(3);
    }

    public Lifecycle getLifecycle(String lifecycleId) {
        Lifecycle lifecycle = cache.get(lifecycleId);
        if (lifecycle == null) {
            lifecycle = super.getLifecycle(lifecycleId);

            if (lifecycle == null) {
                return null;
            }

            lifecycle = new FacesLifecycleDecorator(lifecycle);
            cache.put(lifecycleId, lifecycle);
        }
        return lifecycle;
    }
}