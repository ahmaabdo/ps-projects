package com.gtech.web.section.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.section.dao.SectionDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

public class SectionBusiness extends GTechModelBusiness {

    public SectionBean getSection(String sectionCode) throws GTechException {
        SectionDAO sectionDAO = new SectionDAO();
        return sectionDAO.getSection(sectionCode);
    }

    public SectionBean getSection(String sectionCode, Connection connection) throws GTechException, SQLException {
        SectionDAO sectionDAO = new SectionDAO();
        return sectionDAO.getSection(sectionCode, connection);
    }
    
    public SectionBean getSection(String sectionCode,String deptCode,String entityCode) throws GTechException, SQLException {
        SectionDAO sectionDAO = new SectionDAO();
        return sectionDAO.getSection(sectionCode, deptCode,entityCode);
    }
    
    public List<SectionBean> getDepartmentSectionsList(Long departmentId) throws GTechException {
        SectionDAO sectionDao = new SectionDAO();
        return sectionDao.getDepartmentSectionsList(departmentId);
    }
}
