package com.gtech.web.section.bean;

import com.gtech.common.base.*;

public class SectionBean extends BaseBean {
    private String code;
    private String nameAr;
    private String nameEn;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }
}
