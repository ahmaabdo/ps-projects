package com.gtech.web.section.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.section.bean.SectionBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SectionDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(SectionDAO.class);


    public SectionBean getSection(String sectionCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        SectionBean sectionBean = null;
        ResultSet rs = null;
        String query = "SELECT PTN_ID FROM POG_SECTION  WHERE PTN_CODE = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, sectionCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                sectionBean = new SectionBean();
                sectionBean.setCode(sectionCode);
                sectionBean.setId(rs.getInt("TTG_ID"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return sectionBean;

    }

    public SectionBean getSection(String sectionCode, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        SectionBean sectionBean = new SectionBean();
        ResultSet rs = null;
        String query = "SELECT PTN_ID FROM POG_SECTION  WHERE PTN_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sectionCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            sectionBean = new SectionBean();
            sectionBean.setCode(sectionCode);
            sectionBean.setId(rs.getInt("PTN_ID"));
        }
        return sectionBean;
    }

    public List<SectionBean> getDepartmentSectionsList(Long departmentId) throws GTechException {
        List<SectionBean> SectionsList = new ArrayList<SectionBean>();
        PreparedStatement statement = null;
        ResultSet rs = null;
        String query = "SELECT PTN_ID, PTN_CODE, PTN_NAME_AR, PTN_NAME_EN FROM POG_SECTION WHERE PTN_TTG_ID = ?";
        Connection connection = getConnection();
        try {
            statement = connection.prepareStatement(query);
            statement.setLong(1, departmentId);
            rs = statement.executeQuery();
            while (rs.next()) {
                SectionBean sectionBean = new SectionBean();
                sectionBean.setId(rs.getLong("PTN_ID"));
                sectionBean.setCode(rs.getString("PTN_CODE"));
                sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                SectionsList.add(sectionBean);
            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statement, rs);
        }
        return SectionsList;
    }
    
    public SectionBean getSection(String sectionCode, String deptCode,String entityCode) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        Connection connection = getConnection();
        SectionBean sectionBean = new SectionBean();
        ResultSet rs = null;
            try {
        
        String query = "SELECT PTN_ID,PTN_NAME_EN FROM POG_SECTION,POG_DEPARTMENTS,POG_ENTITIES  " +
                        " WHERE TTG_EEO_ID = EEO_ID " +
                        " AND PTN_TTG_ID = TTG_ID " +
                        " AND EEO_CODE = ? " +
                        " AND TTG_CODE = ? " +
                        " AND PTN_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, entityCode);
        preparedStatement.setString(2, deptCode);
        preparedStatement.setString(3, sectionCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            sectionBean = new SectionBean();
            sectionBean.setCode(sectionCode);
            sectionBean.setId(rs.getInt("PTN_ID"));
            sectionBean.setNameAr(rs.getString("PTN_NAME_EN"));
        }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return sectionBean;
    }

}
