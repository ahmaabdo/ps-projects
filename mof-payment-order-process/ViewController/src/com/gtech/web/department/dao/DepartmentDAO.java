package com.gtech.web.department.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.department.bean.DepartmentBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DepartmentDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(DepartmentDAO.class);

    public DepartmentBean getDepartment(String departmentCode,String entityCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        DepartmentBean departmentBean= new DepartmentBean();
        ResultSet rs = null;
        String query = "SELECT TTG_ID,TTG_NAME_AR FROM " +
                            "POG_DEPARTMENTS,POG_ENTITIES  WHERE TTG_CODE = ? " +
                            " AND EEO_ID = TTG_EEO_ID" +
                            " AND EEO_CODE = ?";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, departmentCode);
            preparedStatement.setString(2, entityCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                departmentBean = new DepartmentBean();
                departmentBean.setCode(departmentCode);
                departmentBean.setId(rs.getInt("TTG_ID"));
                departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return departmentBean;

    }

    public DepartmentBean getDepartment(String departmentCode, Connection connection) throws GTechException,
                                                                                             SQLException {
        PreparedStatement preparedStatement = null;
        DepartmentBean departmentBean = null;
        ResultSet rs = null;
        String query = "SELECT TTG_ID FROM POG_DEPARTMENTS  WHERE TTG_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, departmentCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            departmentBean = new DepartmentBean();
            departmentBean.setCode(departmentCode);
            departmentBean.setId(rs.getInt("TTG_ID"));
        }


        return departmentBean;

    }

    public List<DepartmentBean> getEntityDepartmentsList(Long entityId) throws GTechException {
        List<DepartmentBean> departmentsList = new ArrayList<DepartmentBean>();
        PreparedStatement statement = null;
        ResultSet rs = null;
        String query = "SELECT TTG_ID, TTG_CODE, TTG_NAME_AR, TTG_NAME_EN FROM POG_DEPARTMENTS WHERE TTG_EEO_ID = ?";
        Connection connection = getConnection();
        try {
            statement = connection.prepareStatement(query);
            statement.setLong(1, entityId);
            rs = statement.executeQuery();
            while (rs.next()) {
                DepartmentBean departmentBean = new DepartmentBean();
                departmentBean.setId(rs.getLong("TTG_ID"));
                departmentBean.setCode(rs.getString("TTG_CODE"));
                departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                departmentsList.add(departmentBean);
            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statement, rs);
        }
        return departmentsList;
    }
}
