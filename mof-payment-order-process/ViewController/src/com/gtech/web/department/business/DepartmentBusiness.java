package com.gtech.web.department.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.department.dao.DepartmentDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;


public class DepartmentBusiness extends GTechModelBusiness {

    public DepartmentBean getDepartment(String departmentCode,String entityCode) throws GTechException {
        DepartmentDAO departmentDAO = new DepartmentDAO();
        return departmentDAO.getDepartment(departmentCode,entityCode);
    }

    public DepartmentBean getDepartment(String departmentCode, Connection connection) throws GTechException,
                                                                                             SQLException {
        DepartmentDAO departmentDAO = new DepartmentDAO();
        return departmentDAO.getDepartment(departmentCode, connection);
    }

    public List<DepartmentBean> getEntityDepartmentsList(Long entityId) throws GTechException {
        DepartmentDAO departmentDAO = new DepartmentDAO();
        return departmentDAO.getEntityDepartmentsList(entityId);
    }

}
