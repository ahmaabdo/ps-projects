package com.gtech.web.nationality.bean;

import com.gtech.common.base.BaseBean;

public class NationalityBean extends BaseBean {
    
    private String nationalityCode;
    private String nationalityID;
    private String nationalityNameAr;
    private String nationalityNameEn;

    public NationalityBean() {
        super();
    }


    public void setNationalityCode(String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }

    public String getNationalityCode() {
        return nationalityCode;
    }

    public void setNationalityNameAr(String nationalityNameAr) {
        this.nationalityNameAr = nationalityNameAr;
    }

    public String getNationalityNameAr() {
        return nationalityNameAr;
    }

    public void setNationalityNameEn(String nationalityNameEn) {
        this.nationalityNameEn = nationalityNameEn;
    }

    public String getNationalityNameEn() {
        return nationalityNameEn;
    }


    public void setNationalityID(String nationalityID) {
        this.nationalityID = nationalityID;
    }

    public String getNationalityID() {
        return nationalityID;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof NationalityBean)) {
            return false;
        }
        final NationalityBean other = (NationalityBean) object;
        if (!(nationalityID == null ? other.nationalityID == null : nationalityID.equals(other.nationalityID))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((nationalityID == null) ? 0 : nationalityID.hashCode());
        return result;
    }
}
