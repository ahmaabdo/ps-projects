package com.gtech.web.nationality.dao;

import com.gtech.common.dao.*;
import com.gtech.common.exception.*;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.nationality.bean.*;

import java.sql.*;

import java.util.*;

public class NationalityDao extends GTechDAO {
    public NationalityDao() {
        super();
    }

    public List<NationalityBean> getNationalityList() throws GTechException {
        List<NationalityBean> nationalityList = new ArrayList<NationalityBean>();
        PreparedStatement statement = null;
        ResultSet rs = null;
        String query = "SELECT IIP_ID, IIP_CODE, IIP_NAME_AR, IIP_NAME_EN FROM POG_NATIONALITIES";
        Connection connection = getConnection();
        try {
            statement = connection.prepareStatement(query);
            rs = statement.executeQuery();
            while (rs.next()) {
                NationalityBean nationalityBean = new NationalityBean();
                nationalityBean.setId(rs.getLong("IIP_ID"));
                
                nationalityBean.setNationalityID(rs.getString("IIP_ID"));
                nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                nationalityList.add(nationalityBean);
            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statement, rs);
        }
        return nationalityList;
    }
    
    public NationalityBean getNationality(String nationalityCode, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        NationalityBean nationalityBean = new NationalityBean();
        ResultSet rs = null;
        String query = "SELECT IIP_ID FROM POG_NATIONALITIES  WHERE IIP_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, nationalityCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            nationalityBean = new NationalityBean();
            nationalityBean.setNationalityCode(nationalityCode);
            nationalityBean.setId(rs.getInt("IIP_ID"));
        }


        return nationalityBean;

    }
}
