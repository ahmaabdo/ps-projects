package com.gtech.web.nationality.nationalitybusiness;

import com.gtech.common.exception.*;
import com.gtech.common.model.*;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.nationality.bean.*;
import com.gtech.web.nationality.dao.*;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.*;

public class NationalityBusiness extends GTechModelBusiness {
    public NationalityBusiness() {
        super();
    }

    public List<NationalityBean> getNationalityList() throws GTechException {
        NationalityDao nationalityDao = new NationalityDao();
        return nationalityDao.getNationalityList();
    }
    
    public NationalityBean getNationality(String nationalityCode, Connection connection) throws GTechException, SQLException {
        NationalityDao nationalityDao = new NationalityDao();
        return nationalityDao.getNationality(nationalityCode, connection);
    }
}
