package com.gtech.web.confirmPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;


@ManagedBean(name = "confirmPaymentOrderBean")
@SessionScoped
public class ConfirmPaymentOrderBacking extends GTechModelBacking {
    private ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String date;


    public void fillReviewPaymentOrder(ActionEvent event) {
        try {
            if(isValidSearch()) {
                fillReviewPaymentOrder();
            }
        } catch (GTechException e) {
        }
    }

    public String backFromReviewPaymentOrder() {
        try {
            fillReviewPaymentOrder();
        } catch (GTechException e) {
        }

        return "PO-F6";
    }

    public void fillReviewPaymentOrder() throws GTechException {
        reviewPOBeanList.clear();
        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatus(100402, getPoDateFrom() ,getPoDateTo(),
                                                                            getPoNumber(),
                                                                            getBenNumber(),
                                                                            getIbanAccNumber(), 
                                                                            getLogedInUser());
        if(reviewPOBeanList.isEmpty()){
            numberOfPaymentOrder = 0 ;
        }else{
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }

    @Override
    public String initialize() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        numberOfPaymentOrder = 0;
        date = "";
        // TODO Implement this method
        return "PO-F6";
    }

    public String doSave() throws GTechException {
        responseContainer = new ResponseContainer();
        boolean check =false;
        for (PaymentOrderBean paymentOrderBean : reviewPOBeanList) {

            if (paymentOrderBean.isCheck()) {
                check = true;
                break;
            }

        }
        
        if(check) {
            if (actionLogBusiness.insertFile(reviewPOBeanList, 100503, getLogedInUser().getUserID())) {
                fillReviewPaymentOrder();
                responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
            } else {
                responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
            }            
        } else {
            responseContainer.addInfoMessage("PLEASE_SELECT_ONE_RECORED");
        }
        
        createMessages(responseContainer);
        return null;
    }

    public String doRest() throws GTechException {
        reviewPOBeanList.clear();
        resetSearchParam();
        numberOfPaymentOrder = null;
        return null;
    }

    public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
        this.paymentOrderBusiness = paymentOrderBusiness;
    }

    public PaymentOrderBusiness getPaymentOrderBusiness() {
        return paymentOrderBusiness;
    }

    public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
        this.reviewPOBeanList = reviewPOBeanList;
    }

    public List<PaymentOrderBean> getReviewPOBeanList() {
        return reviewPOBeanList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
