package com.gtech.web.ecomomicClassifications.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.ecomomicClassifications.dao.EcomomicClassificationDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

public class EcomomicClassificationBusiness extends GTechModelBusiness {

    public EcomomicClassificationBean getEcomomicClassification(String ecomomicClassification) throws GTechException {
        EcomomicClassificationDAO ecomomicClassificationDAO = new EcomomicClassificationDAO();
        return ecomomicClassificationDAO.getEcomomicClassification(ecomomicClassification);
    }

    public EcomomicClassificationBean getEcomomicClassification(String ecomomicClassification,
                                                                Connection connection) throws GTechException,
                                                                                              SQLException {
        EcomomicClassificationDAO ecomomicClassificationDAO = new EcomomicClassificationDAO();
        return ecomomicClassificationDAO.getEcomomicClassification(ecomomicClassification, connection);
    }

    public List<EcomomicClassificationBean> getEconomicClassificationsList() throws GTechException {
        EcomomicClassificationDAO ecomomicClassificationDAO = new EcomomicClassificationDAO();
        return ecomomicClassificationDAO.getEconomicClassificationsList();
    }
}
