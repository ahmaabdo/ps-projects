package com.gtech.web.ecomomicClassifications.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EcomomicClassificationDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(EcomomicClassificationDAO.class);


    public EcomomicClassificationBean getEcomomicClassification(String ecomomicClassification) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        EcomomicClassificationBean ecomomicClassificationBean = null;
        ResultSet rs = null;
        String query = "SELECT SCO_ID FROM POG_ECOMOMIC_CLASSIFICATIONS  WHERE SCO_CODE = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, ecomomicClassification);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                ecomomicClassificationBean = new EcomomicClassificationBean();
                ecomomicClassificationBean.setCode(ecomomicClassification);
                ecomomicClassificationBean.setId(rs.getInt("SCO_ID"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return ecomomicClassificationBean;

    }

    public EcomomicClassificationBean getEcomomicClassification(String ecomomicClassification,
                                                                Connection connection) throws GTechException,
                                                                                              SQLException {

        PreparedStatement preparedStatement = null;
        EcomomicClassificationBean ecomomicClassificationBean = new EcomomicClassificationBean();
        ResultSet rs = null;
        String query = "SELECT SCO_ID FROM POG_ECOMOMIC_CLASSIFICATIONS  WHERE SCO_CODE = ? ";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, ecomomicClassification);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            ecomomicClassificationBean = new EcomomicClassificationBean();
            ecomomicClassificationBean.setCode(ecomomicClassification);
            ecomomicClassificationBean.setId(rs.getInt("SCO_ID"));
        }

        return ecomomicClassificationBean;

    }

    public List<EcomomicClassificationBean> getEconomicClassificationsList() throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<EcomomicClassificationBean> economicClassificationsList = new ArrayList<EcomomicClassificationBean>();
        String query = "SELECT SCO_ID, SCO_CODE, SCO_DESC_AR, SCO_DESC_EN FROM POG_ECOMOMIC_CLASSIFICATIONS";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                economicClassificationsList.add(classificationBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return economicClassificationsList;
    }


}
