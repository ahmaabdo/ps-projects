package com.gtech.web.beneficiary.bean;

import com.gtech.common.base.*;
import com.gtech.web.currency.bean.*;
import com.gtech.web.nationality.bean.*;

public class BeneficiaryBean extends BaseBean {
    private String code;
    private String descAr;
    private String descEn;
    private String nameAr;
    private String nameEn;
    private NationalityBean nationalityBean;
    private CurrencyBean currencyBean;
    private Integer paymentMethod;
    private Integer beneficiaryType;
    private Integer documentType;
    private String iban;
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    public void setDescAr(String descAr) {
        this.descAr = descAr;
    }

    public String getDescAr() {
        return descAr;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getDescEn() {
        return descEn;
    }
     
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNationalityBean(NationalityBean nationalityBean) {
        this.nationalityBean = nationalityBean;
    }

    public NationalityBean getNationalityBean() {
        return nationalityBean;
    }

    public void setCurrencyBean(CurrencyBean currencyBean) {
        this.currencyBean = currencyBean;
    }

    public CurrencyBean getCurrencyBean() {
        return currencyBean;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setBeneficiaryType(Integer beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    public Integer getBeneficiaryType() {
        return beneficiaryType;
    }

    public void setDocumentType(Integer documentType) {
        this.documentType = documentType;
    }

    public Integer getDocumentType() {
        return documentType;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getIban() {
        return iban;
    }
}
