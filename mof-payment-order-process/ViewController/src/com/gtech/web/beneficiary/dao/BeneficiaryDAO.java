package com.gtech.web.beneficiary.dao;

import com.gtech.common.dao.*;
import com.gtech.common.exception.*;
import com.gtech.web.beneficiary.bean.*;
import com.gtech.web.currency.bean.*;
import com.gtech.web.nationality.bean.*;

import java.sql.*;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.logging.*;

public class BeneficiaryDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(BeneficiaryDAO.class);

    public BeneficiaryBean getBeneficiary(String beneficiaryCode) throws GTechException {
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        BeneficiaryBean beneficiaryBean = null;
        String query = "SELECT * FROM POG_BENEFICIARIES WHERE EII_CODE = ? ";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, beneficiaryCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setId(rs.getLong("EII_ID"));
                beneficiaryBean.setCode(rs.getString("EII_CODE"));
                String name_ar = rs.getString("EII_NAME_AR");
                if (name_ar != null && name_ar.indexOf("*") > 0) {
                    beneficiaryBean.setNameAr(name_ar.substring(0,name_ar.indexOf("*")));    
                } else {
                    beneficiaryBean.setNameAr(name_ar);    
                }
                
                String name_en = rs.getString("EII_NAME_EN");
                if (name_ar != null && name_ar.indexOf("*") > 0) {
                    beneficiaryBean.setNameEn(name_en.substring(0,name_en.indexOf("*")));    
                } else {
                    beneficiaryBean.setNameEn(name_en);    
                }
                NationalityBean nationalityBean = new NationalityBean();
                nationalityBean.setId(rs.getLong("EII_IIP_ID"));
                beneficiaryBean.setNationalityBean(nationalityBean);
                CurrencyBean currencyBean = new CurrencyBean();
                currencyBean.setId(rs.getLong("EII_CRN_ID"));
                beneficiaryBean.setCurrencyBean(currencyBean);
                beneficiaryBean.setPaymentMethod(rs.getInt("EII_PAYMENT_METHOD"));
                beneficiaryBean.setBeneficiaryType(rs.getInt("EII_BENEFICIARY_TYPE"));
                beneficiaryBean.setDocumentType(rs.getInt("EII_DOCUMENT_TYPE"));
                beneficiaryBean.setIban(rs.getString("EII_IBAN_CODE"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return beneficiaryBean;

    }

    public BeneficiaryBean getBeneficiary(String beneficiaryCode, Connection connection) throws GTechException,
                                                                                                SQLException {
        PreparedStatement preparedStatement = null;
        BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
        String query = "SELECT EII_ID FROM POG_BENEFICIARIES  WHERE EII_CODE = ? ";
        ResultSet rs = null;
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, beneficiaryCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            beneficiaryBean = new BeneficiaryBean();
            beneficiaryBean.setCode(beneficiaryCode);
            beneficiaryBean.setId(rs.getInt("EII_ID"));
        }


        return beneficiaryBean;

    }
    
    public BeneficiaryBean getBeneficiary(Long beneficiaryId, Connection connection) throws GTechException,
                                                                                                SQLException {
        PreparedStatement preparedStatement = null;
        BeneficiaryBean beneficiaryBean = null;
        String query = "SELECT * FROM POG_BENEFICIARIES  WHERE EII_ID = ? ";
        ResultSet rs = null;
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, beneficiaryId);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            beneficiaryBean = new BeneficiaryBean();
            beneficiaryBean.setCode(rs.getString("EII_CODE"));
            beneficiaryBean.setDescAr(rs.getString("EII_NAME_AR"));
            beneficiaryBean.setDescEn(rs.getString("EII_NAME_EN"));
            beneficiaryBean.setId(rs.getInt("EII_ID"));
        }

        return beneficiaryBean;

    }
    
    public List<String> getIbanByBenCode(String beneficiaryCode) {
        PreparedStatement preparedStatement = null;
        String query = "SELECT EII_IBAN_CODE FROM POG_BENEFICIARIES  WHERE EII_CODE = ? ";
        ResultSet rs = null;
 
        Connection connection = null;
        List<String> ibanList = new ArrayList<>();
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, beneficiaryCode);
            rs = preparedStatement.executeQuery();
            
            while (rs.next()) {
                ibanList.add(rs.getString("EII_IBAN_CODE").replaceAll("SA", ""));
            }
        } catch (GTechException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeResources(connection, preparedStatement, rs);
        }


        return ibanList;

    }
    
    public List<String[]> getSNOCODE() {
        PreparedStatement preparedStatement = null;
        String query = "SELECT CODE,DESCRIPTION FROM APPS.xxx_mof_cofog_v";
        ResultSet rs = null;
    
        Connection connection = null;
        List<String[]> ibanList = new ArrayList<>();
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
           rs = preparedStatement.executeQuery();
            String ar[];
            while (rs.next()) {
                ar = new String[2];
                ar[0] = rs.getString("CODE");
                ar[1] =   rs.getString("DESCRIPTION")  ;
                ibanList.add(ar);
            }
        } catch (GTechException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeResources(connection, preparedStatement, rs);
        }


        return ibanList;

    }

}
