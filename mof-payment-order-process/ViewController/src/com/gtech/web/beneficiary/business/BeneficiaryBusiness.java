package com.gtech.web.beneficiary.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.beneficiary.dao.BeneficiaryDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

public class BeneficiaryBusiness extends GTechModelBusiness {

    public BeneficiaryBean getBeneficiary(String beneficiaryCode) throws GTechException {
        BeneficiaryDAO beneficiaryDAO = new BeneficiaryDAO();
        return beneficiaryDAO.getBeneficiary(beneficiaryCode);
    }

    public BeneficiaryBean getBeneficiary(String beneficiaryCode, Connection connection) throws GTechException,
                                                                                                SQLException {
        BeneficiaryDAO beneficiaryDAO = new BeneficiaryDAO();
        return beneficiaryDAO.getBeneficiary(beneficiaryCode, connection);
    }
    
    public List<String> getIbanByBenCode(String beneficiaryCode) {
        BeneficiaryDAO beneficiaryDAO = new BeneficiaryDAO();
        return beneficiaryDAO.getIbanByBenCode(beneficiaryCode);
    }
    
    public List<String[]> getSNOCODE() {
        BeneficiaryDAO beneficiaryDAO = new BeneficiaryDAO();
        return beneficiaryDAO.getSNOCODE();
    }
}
