package com.gtech.web.expense.bean;

import com.gtech.common.base.BaseBean;

import java.util.List;

public class ExpenseBean extends BaseBean {

    /** Code. */
    private String code;
    private String descAr;
    private String descEn;
    private boolean check;

    /**
     * Setter Code.
     *
     * @param Code : Code.
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * Getter code.
     *
     * @return code.
     */
    public String getCode() {
        return code;
    }


    public void setDescAr(String descAr) {
        this.descAr = descAr;
    }

    public String getDescAr() {
        return descAr;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public boolean isCheck() {
        return check;
    }
}
