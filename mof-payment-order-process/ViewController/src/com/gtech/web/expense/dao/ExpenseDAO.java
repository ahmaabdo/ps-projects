package com.gtech.web.expense.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.expense.bean.ExpenseBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ExpenseDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(ExpenseDAO.class);


    public ExpenseBean getExpense(String expenseCode, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        ExpenseBean expenseBean = new ExpenseBean();
        String query = "SELECT EPP_ID FROM POG_EXPENSE_TYPES  WHERE EPP_CODE = ? ";
        ResultSet rs = null;
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, expenseCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            expenseBean = new ExpenseBean();
            expenseBean.setCode(expenseCode);
            expenseBean.setId(rs.getInt("EPP_ID"));
        }
        return expenseBean;
    }

    
    public ExpenseBean getExpense(String expenseCode) throws GTechException, SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        ExpenseBean expenseBean = new ExpenseBean();
        ResultSet rs = null;
        try {
                String query = "SELECT EPP_ID,EPP_CODE,EPP_DESC_AR FROM POG_EXPENSE_TYPES  WHERE EPP_CODE = ? ";
                
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, expenseCode);
                rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    expenseBean = new ExpenseBean();
                    expenseBean.setCode(expenseCode);
                    expenseBean.setId(rs.getInt("EPP_ID"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                }
            } catch (SQLException e) {
                log.error("Exception :", e);
                try {
                    connection.rollback();
                } catch (SQLException f) {
                    throw new GTechException(e);
                }
                throw new GTechException(e);
            } finally {
                closeResources(connection, preparedStatement, rs);
            }
        
        return expenseBean;
    }
    
    public List<ExpenseBean> getExpenseList() throws GTechException {
        PreparedStatement preparedStatement = null;
        List<ExpenseBean> expenseBeanList = new ArrayList<>();
        ExpenseBean expenseBean = null;
        ResultSet rs = null;
        String query = "SELECT * FROM POG_EXPENSE_TYPES T WHERE T.EEP_TO_DISPLAY  = 1 ";
        Connection connection = getConnection();
        try {
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                expenseBean.setId(rs.getInt("EPP_ID"));
                expenseBeanList.add(expenseBean);
            }
        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }

        return expenseBeanList;
    }

    public List<ExpenseBean> getExpensesList() throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<ExpenseBean> expensesList = new ArrayList<ExpenseBean>();
        String query = "SELECT EPP_ID, EPP_CODE, EPP_DESC_AR, EPP_DESC_EN FROM POG_EXPENSE_TYPES ";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setId(rs.getLong("EPP_ID"));
                expenseBean.setCode(rs.getString("EPP_CODE"));
                expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                expensesList.add(expenseBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return expensesList;
    }

}
