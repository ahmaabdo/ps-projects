package com.gtech.web.expense.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.expense.dao.ExpenseDAO;

import java.sql.SQLException;

import java.util.List;

public class ExpenseBusiness extends GTechModelBusiness {
    ExpenseDAO expenseDAO = new ExpenseDAO();

    public List<ExpenseBean> getExpenseList() throws GTechException {
        return expenseDAO.getExpenseList();
    }

    public List<ExpenseBean> getExpensesList() throws GTechException {
        return expenseDAO.getExpensesList();
    }
    
    public ExpenseBean getExpense(String expenseCode) throws GTechException, SQLException {
        return expenseDAO.getExpense(expenseCode);
    }
}
