package com.gtech.web.report;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.adminhijridate.dao.CalendarDAO;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.project.dao.ProjectDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import login.LoginBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(ProjectDAO.class);
    
    public List<ReportBean> getNumberOfPaymentOrderPerUser(String startDate, String endStart,LoginBean bean) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        CalendarDAO calendarDAO = new CalendarDAO();
        ResultSet rs = null;
        connection = getConnection();
        List<ReportBean> beanList = new ArrayList<>();
        String query = "SELECT COUNT(*) AS NUM,USR.SSC_NAME_AR " + 
                       "    FROM POG_PAYMENT_ORDERS SGR, POG_USERS USR WHERE " +
                       " 1 = 1 " +
                       " AND SGR.SGR_EEO_ID = ? " +
                       " AND SGR.SGR_TTG_ID = ? " +
                       " AND SGR.SGR_PTN_ID = ? ";
        try {
            
            if ( startDate != null ) {
               query += " AND SGR.SGR_CREATED_ON_GR >= TO_DATE('"+calendarDAO.getGrDateByHijriDate(connection, startDate) +"', 'DD/MM/YYYY')";
            }
            
            if (endStart != null) {
                query += " AND SGR.SGR_CREATED_ON_GR <= TO_DATE('"+calendarDAO.getGrDateByHijriDate(connection, endStart) +"', 'DD/MM/YYYY')";
            }
            
            query += "  AND SGR.SGR_CREATED_BY_SSC_ID = USR.SSC_ID " + 
                     "    GROUP BY SGR.SGR_CREATED_BY_SSC_ID,USR.SSC_NAME_AR";
            
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, bean.getEntityBean().getId());
            preparedStatement.setLong(2, bean.getDepartmentBean().getId());
            preparedStatement.setLong(3, bean.getSectionBean().getId());
            rs = preparedStatement.executeQuery();
            ReportBean reportBean = null;
            while (rs.next()) {
                reportBean = new ReportBean();
                reportBean.setData(rs.getInt("NUM"));
                reportBean.setLabel(rs.getString("SSC_NAME_AR"));
                beanList.add(reportBean);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return beanList;

    }
    
    public List<PaymentOrderBean> getPaymentOrderReport(String startDate, String endStart,LoginBean bean,String paymentOrderNo,String benNumber, String ibanAccNumber) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        CalendarDAO calendarDAO = new CalendarDAO();
        ResultSet rs = null;
        List<PaymentOrderBean> beanList = new ArrayList<>();
        connection = getConnection();
        String query = "SELECT  \n" + 
                        "    po.sgr_code ponum,  \n" + 
                        "    SGR_DATE_HI creationDate,\n" + 
                        "    PO.SGR_PAYMENT_AMOUNT,\n" + 
                        "    EII.EII_NAME_AR,\n" + 
                        "    EII.EII_CODE,\n" + 
                        "    lok.GAO_DESC_AR status,\n" + 
                        "    DECODE (po.sgr_status,'100401','PO_STATUS_4','100402','PO_STATUS_3','100403','PO_STATUS_2','100404','PO_STATUS_5','100410','PO_STATUS_6','100405',(SELECT APPS.XXX_FIN_PO_DETAIL_PKG.F_PODETAIL(po.sgr_code) FROM DUAL)) ACTION   \n" + 
                        "FROM POG_PAYMENT_ORDERS po,  \n" + 
                        "    POG_DMG_VALUES lok,\n" + 
                        "    POG_BENEFICIARIES EII\n" + 
                        "                                  \n" + 
                        "WHERE     po.sgr_status = lok.GAO_ID  \n" + 
                        "    AND GAO_DMG_ID = 1004\n" + 
                        "    AND PO.SGR_EII_ID = EII.EII_ID" +
                        "         AND po.SGR_EEO_ID = ? " +
                        "         AND po.SGR_TTG_ID = ? " +
                        "         AND po.SGR_PTN_ID = ? ";
        try {
            
            if ( startDate != null ) {
               query += " AND po.SGR_CREATED_ON_GR >= TO_DATE('"+calendarDAO.getGrDateByHijriDate(connection, startDate) +"', 'DD/MM/YYYY')";
            }
            
            if (endStart != null) {
                query += " AND po.SGR_CREATED_ON_GR <= TO_DATE('"+calendarDAO.getGrDateByHijriDate(connection, endStart) +"', 'DD/MM/YYYY')";
            }
            
            if (paymentOrderNo != null && !paymentOrderNo.isEmpty()) {
                query += " AND po.SGR_CODE = '" + paymentOrderNo+"'";
            }
            
            if (ibanAccNumber != null && !ibanAccNumber.isEmpty()) {
                query += " AND  po.SGR_IBAN_CODE = '" + ibanAccNumber+"'";
            }

            if (benNumber != null && !benNumber.isEmpty()) {
                query += " AND  EII.EII_CODE = '" + benNumber+"'";
            }
            
            query += "  ORDER BY SGR_DATE_GR  DESC ";
            
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, bean.getEntityBean().getId());
            preparedStatement.setLong(2, bean.getDepartmentBean().getId());
                preparedStatement.setLong(3, bean.getSectionBean().getId());
            rs = preparedStatement.executeQuery();
            PaymentOrderBean paymentOrderBean = null;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setPaymentOrderNumber(rs.getString("ponum"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("creationDate"));
                paymentOrderBean.setIban(rs.getString("status"));
                paymentOrderBean.setPaymentOrderDateG(rs.getString("ACTION"));
                
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                
                paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
                paymentOrderBean.getBeneficiaryBean().setCode(rs.getString("EII_CODE"));
                paymentOrderBean.getBeneficiaryBean().setDescAr(rs.getString("EII_NAME_AR"));
                
                beanList.add(paymentOrderBean);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return beanList;

    }
    
    public List<ReportBean> getNumberOfPoEgovVsPortal(String entityCode,String deptCode,String sectionCode,String dateFrom,String dateTo) throws GTechException {
        List<ReportBean> reportBeanList = new ArrayList<>();
        
        reportBeanList.add(getNumberOfEgov(entityCode, deptCode, sectionCode,dateFrom,dateTo));
        reportBeanList.add(getNumberOfPortal(entityCode, deptCode, sectionCode,dateFrom,dateTo));
        
        return reportBeanList;
    }
    
    private ReportBean getNumberOfEgov(String entityCode,String deptCode,String sectionCode,String dateFrom,String dateTo) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        connection = getConnection();
        CalendarDAO calendarDAO = new CalendarDAO();
        
        String query = 
        "  SELECT COUNT (AIA.INVOICE_ID) AS NUM\n" + 
        "    FROM AP.AP_INVOICES_ALL AIA,\n" + 
        "         AP.AP_INVOICE_DISTRIBUTIONS_ALL AID,\n" + 
        "         GL.GL_CODE_COMBINATIONS GCC\n" + 
        "   WHERE     AIA.ATTRIBUTE7 IS NULL\n" + 
        "         AND AIA.INVOICE_ID = AID.INVOICE_ID\n" + 
        "         AND GCC.CODE_COMBINATION_ID = AID.DIST_CODE_COMBINATION_ID\n" + 
        "         AND GCC.SEGMENT4 = ?\n";
//        "         AND AIA.CREATION_DATE >=\n" + 
//        "                (SELECT MIN (POG.SGR_CREATED_ON_GR)\n" + 
//        "                   FROM POG_PAYMENT_ORDERS POG, POG_ENTITIES EII,POG_DEPARTMENTS DEPT,POG_SECTION SEC\n" + 
//        "                  WHERE POG.SGR_EEO_ID = EII.EEO_ID AND EII.EEO_CODE = ? \n" + 
//        "                        AND POG.SGR_TTG_ID = DEPT.TTG_ID AND DEPT.TTG_CODE = ?\n" + 
//        "                        AND POG.SGR_PTN_ID = SEC.PTN_ID AND SEC.PTN_CODE = ?)\n" + 
//        "GROUP BY SEGMENT4";
        try {
            if (dateFrom != null ) {
                String gergDateFrom = calendarDAO.getGrDateByHijriDate(connection, dateFrom);
                query +=
                    " AND TRUNC(AIA.CREATION_DATE) >=  TO_DATE('" + gergDateFrom +
                    "', 'DD/MM/YYYY') ";
            }
            
            if (dateTo != null) {
                
                String gergDateTo = calendarDAO.getGrDateByHijriDate(connection, dateTo);
                
                query +=
                     " AND TRUNC(AIA.CREATION_DATE) <=  TO_DATE('" + gergDateTo +
                    "', 'DD/MM/YYYY') ";
            }
            
            preparedStatement = connection.prepareStatement(query);
            
            preparedStatement.setString(1, entityCode + deptCode + sectionCode + "000000");
//            preparedStatement.setString(2, entityCode);
//            preparedStatement.setString(3, deptCode);
//            preparedStatement.setString(4, sectionCode);
            
            
            rs = preparedStatement.executeQuery();
            ReportBean reportBean = null;
            while (rs.next()) {
                reportBean = new ReportBean();
                reportBean.setData(rs.getInt("NUM"));
                reportBean.setLabel("E-Gov");
            }
            return reportBean;

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }

    }
    
    private ReportBean getNumberOfPortal(String entityCode,String deptCode,String sectionCode,String dateFrom,String dateTo) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        connection = getConnection();
        CalendarDAO calendarDAO = new CalendarDAO();
        
        String query = 
        "SELECT COUNT(*) AS NUM\n" + 
        "       FROM POG_PAYMENT_ORDERS POG, POG_ENTITIES EII,POG_DEPARTMENTS DEPT,POG_SECTION SEC\n" + 
        "           WHERE 1 = 1 " +
        "               POG.SGR_EEO_ID = EII.EEO_ID AND EII.EEO_CODE = ? \n" + 
        "                 AND POG.SGR_TTG_ID = DEPT.TTG_ID AND DEPT.TTG_CODE = ? \n" + 
        "                 AND POG.SGR_PTN_ID = SEC.PTN_ID AND SEC.PTN_CODE = ?"; 
        try {
            
            if (entityCode != null && deptCode != null && sectionCode != null ) {
                
            }
            if (dateFrom != null ) {
                String gergDateFrom = calendarDAO.getGrDateByHijriDate(connection, dateFrom);
                query +=
                    " AND TRUNC(NVL(POG.SGR_LTD_MODIFIED_ON_GR,POG.SGR_CREATED_ON_GR)) >=  TO_DATE('" + gergDateFrom +
                    "', 'DD/MM/YYYY') ";
            }
            
            if (dateTo != null) {
                
                String gergDateTo = calendarDAO.getGrDateByHijriDate(connection, dateTo);
                
                query +=
                    " AND TRUNC(NVL(POG.SGR_LTD_MODIFIED_ON_GR,POG.SGR_CREATED_ON_GR)) <=  TO_DATE('" + gergDateTo +
                    "', 'DD/MM/YYYY') ";
            }
            
            preparedStatement = connection.prepareStatement(query);
            
            preparedStatement.setString(1, entityCode);
            preparedStatement.setString(2, deptCode);
            preparedStatement.setString(3, sectionCode);
            
            rs = preparedStatement.executeQuery();
            ReportBean reportBean = null;
            while (rs.next()) {
                reportBean = new ReportBean();
                reportBean.setData(rs.getInt("NUM"));
                reportBean.setLabel("New-Portal");
            }
            
            return reportBean;
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
    }
    
    public List<ReportBean> getNumberOfPO(String entityCode,String deptCode,String sectionCode,String dateFrom,String dateTo) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        connection = getConnection();
        CalendarDAO calendarDAO = new CalendarDAO();
        
        List<ReportBean> list = new ArrayList<>();
        
        String query = ""; 
        
        try {
            
            if (entityCode != null && deptCode != null && sectionCode != null ) {
                query =         "SELECT COUNT(*) AS NUM,EII.EEO_NAME_AR AS NAME,EEO_CODE AS CODE" + 
                                "       FROM POG_PAYMENT_ORDERS POG, POG_ENTITIES EII,POG_DEPARTMENTS DEPT,POG_SECTION SEC\n" + 
                                "           WHERE 1 = 1 ";
                query +=" AND POG.SGR_EEO_ID = EII.EEO_ID AND EII.EEO_CODE = ? \n" + 
                        " AND POG.SGR_TTG_ID = DEPT.TTG_ID AND DEPT.TTG_CODE = ? \n" + 
                        " AND POG.SGR_PTN_ID = SEC.PTN_ID AND SEC.PTN_CODE = ?"; 
            } else {
                query =         "SELECT COUNT(*) AS NUM,EII.EEO_NAME_AR AS NAME,EEO_CODE AS CODE" + 
                                "       FROM POG_PAYMENT_ORDERS POG, POG_ENTITIES EII " + 
                                "           WHERE POG.SGR_EEO_ID = EII.EEO_ID "; 
            }
            if (dateFrom != null ) {
                String gergDateFrom = calendarDAO.getGrDateByHijriDate(connection, dateFrom);
                query +=
                    " AND TRUNC(NVL(POG.SGR_LTD_MODIFIED_ON_GR,POG.SGR_CREATED_ON_GR)) >=  TO_DATE('" + gergDateFrom +
                    "', 'DD/MM/YYYY') ";
            }
            
            if (dateTo != null) {
                
                String gergDateTo = calendarDAO.getGrDateByHijriDate(connection, dateTo);
                
                query +=
                    " AND TRUNC(NVL(POG.SGR_LTD_MODIFIED_ON_GR,POG.SGR_CREATED_ON_GR)) <=  TO_DATE('" + gergDateTo +
                    "', 'DD/MM/YYYY') ";
            }
            query += " AND SGR_STATUS <> 100411";
            query += " GROUP BY EII.EEO_NAME_AR,EEO_CODE";
            preparedStatement = connection.prepareStatement(query);
            
            if (entityCode != null && deptCode != null && sectionCode != null ) {
                preparedStatement.setString(1, entityCode);
                preparedStatement.setString(2, deptCode);
                preparedStatement.setString(3, sectionCode);
            }
            
            
            rs = preparedStatement.executeQuery();
            ReportBean reportBean = null;
            while (rs.next()) {
                reportBean = new ReportBean();
                reportBean.setData(rs.getInt("NUM"));
                reportBean.setLabel(rs.getString("NAME") + "(" +rs.getString("CODE") +")");
                
                list.add(reportBean);
            }
            
            return list;
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
    }
    
    
}
