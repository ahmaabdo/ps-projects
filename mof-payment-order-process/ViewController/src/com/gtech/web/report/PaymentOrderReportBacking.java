package com.gtech.web.report;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "paymentOrderReportBacking")
@ViewScoped
public class PaymentOrderReportBacking extends GTechModelBacking{
    
    private Date startDate;

    private Date endDate;
    
    private String startDateScreen;

    private String endDateScreen;
    
    private List<PaymentOrderBean> list = new ArrayList<>();
    
    private String paymentOrderNo;
    
    public String fillPaymentOrderReport() throws GTechException {
        list.clear();
        ReportBusiness reportBusiness = new ReportBusiness();
        
        if(!validateCraitira()) {
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addErrorMessage("PO_CRAITRIA_VALIDATION");
            createMessages(responseContainer);
            
            list.clear();
            
            return null;
        }
        
        list = reportBusiness.getPaymentOrderReport(startDateScreen, endDateScreen, getLogedInUser(), paymentOrderNo,getBenNumber(),getIbanAccNumber());
        
        return null;
    }
    
    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }
    
    private boolean validateCraitira(){
        
        return endDateScreen != null ||
               startDateScreen != null ||
               paymentOrderNo != null ||
               getBenNumber() != null ||
               getIbanAccNumber() != null;
    }

    public void setList(List<PaymentOrderBean> list) {
        this.list = list;
    }

    public List<PaymentOrderBean> getList() {
        return list;
    }

    public void setPaymentOrderNo(String paymentOrderNo) {
        this.paymentOrderNo = paymentOrderNo;
    }

    public String getPaymentOrderNo() {
        return paymentOrderNo;
    }

    public void setStartDateScreen(String startDateScreen) {
        this.startDateScreen = startDateScreen;
    }

    public String getStartDateScreen() {
        return startDateScreen;
    }

    public void setEndDateScreen(String endDateScreen) {
        this.endDateScreen = endDateScreen;
    }

    public String getEndDateScreen() {
        return endDateScreen;
    }

}
