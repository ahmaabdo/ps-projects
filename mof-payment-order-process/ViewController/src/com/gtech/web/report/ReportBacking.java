package com.gtech.web.report;

import com.gtech.common.beans.container.ReportContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.reader.CommonConfigReader;

import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@ManagedBean(name = "reportBacking")
@ViewScoped
public class ReportBacking extends GTechModelBacking {

    private ReportBean reportBean = new ReportBean();

    private Date startDate;

    private Date endDate;

    private String jsonChartData = "EMPTY";
    
    private List<ReportBean> list = new ArrayList<>();
    
    private String startDateScreen;

    private String endDateScreen;
    
    public ReportBacking() {
        super();
    }

    public String parseJSONNumberOfPaymentOrderPerUser() throws GTechException, IOException, JsonGenerationException,
                                                              JsonMappingException {
        list.clear();
        ReportBusiness reportBusiness = new ReportBusiness();
        if(!validateCraitira()) {
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addErrorMessage("PO_CRAITRIA_VALIDATION");
            createMessages(responseContainer);
            
            list.clear();
            
            return null;
        }
        
        list = reportBusiness.getNumberOfPaymentOrderPerUser(startDateScreen, endDateScreen, getLogedInUser());
        System.out.println(list.size());
        if(list == null || list.isEmpty()) {
            jsonChartData = "'EMPTY'";
            callJavaScriptFunction("draw('NO_DATA');");
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        jsonChartData = "[";
        for (int i =0 ;i < list.size();i++) {
            ReportBean bean = list.get(i);
            if(i == (list.size() - 1)) {
                jsonChartData += "{label :'" + bean.getLabel()+"'";
                jsonChartData += ",data :" + bean.getData()+"}";
                continue;
            }
            jsonChartData += "{label :'" + bean.getLabel()+"'";
            jsonChartData += ",data :" + bean.getData()+"},";
            
        }
        
        jsonChartData += "]";
        callJavaScriptFunction("draw("+jsonChartData+");");
        return null;
    }
    
    private boolean validateCraitira(){
        
        return (endDateScreen != null &&
               startDateScreen != null);
    }


    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }

    public void printReport() throws Exception {
        ReportContainer reportDataContainer = new ReportContainer();
        reportDataContainer.setReportDataList(new ArrayList());
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        reportDataContainer.addParameter("currentDate", currentDate);

        reportDataContainer.setTargetReport("prepareOutgoingLetter");

        String realPath = CommonConfigReader.getValue("REPORT_FOLDER");
        String reportFileName = reportDataContainer.getTargetReport() + ".jasper";
        String reportFullPath = realPath + reportFileName;
        reportDataContainer.addParameter("REALPATH", realPath);


        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("application/pdf");

        ServletOutputStream servletOutputStream;
        try {
            servletOutputStream = response.getOutputStream();
        } catch (IOException ioExp) {
            throw new Exception(ioExp);
        }


        JRPdfExporter reportExporter = new JRPdfExporter();
        reportExporter.setParameter(JRPdfExporterParameter.CHARACTER_ENCODING, "UTF-8");
        reportExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        try {
            JasperPrint jasperPrint;
            jasperPrint =
                JasperFillManager.fillReport(reportFullPath, reportDataContainer.getParametersMap(),
                                             new JRBeanCollectionDataSource(reportDataContainer.getReportDataList()));
            reportExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            reportExporter.exportReport();
            context.responseComplete();
        } catch (Exception e) {
            throw new Exception(e);
        }

    }


    public void setReportBean(ReportBean reportBean) {
        this.reportBean = reportBean;
    }

    public ReportBean getReportBean() {
        return reportBean;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setJsonChartData(String jsonChartData) {
        this.jsonChartData = jsonChartData;
    }

    public String getJsonChartData() {
        return jsonChartData;
    }

    public void setList(List<ReportBean> list) {
        this.list = list;
    }

    public List<ReportBean> getList() {
        return list;
    }

    public void setStartDateScreen(String startDateScreen) {
        this.startDateScreen = startDateScreen;
    }

    public String getStartDateScreen() {
        return startDateScreen;
    }

    public void setEndDateScreen(String endDateScreen) {
        this.endDateScreen = endDateScreen;
    }

    public String getEndDateScreen() {
        return endDateScreen;
    }

}
