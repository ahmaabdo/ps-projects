package com.gtech.web.report;


public class ReportBean {
    @SuppressWarnings("compatibility:-1127679993918903979")
    private static final long serialVersionUID = 1L;
    
    private String label;
    private int data;
    
    public ReportBean() {
        super();
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setData(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }
}
