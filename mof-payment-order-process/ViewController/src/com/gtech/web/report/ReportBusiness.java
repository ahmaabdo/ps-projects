package com.gtech.web.report;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;

import java.util.List;

import login.LoginBean;


public class ReportBusiness extends GTechModelBusiness {

    public List<ReportBean> getNumberOfPaymentOrderPerUser(String startDate, String endStart,LoginBean bean) throws GTechException {
        ReportDAO dao = new ReportDAO();
        return dao.getNumberOfPaymentOrderPerUser(startDate, endStart, bean);
    }
    
    public List<PaymentOrderBean> getPaymentOrderReport(String startDate, String endStart,
                                                        LoginBean bean,String paymentOrderNo,
                                                        String benNumber, String ibanAccNumber) throws GTechException {
        ReportDAO dao = new ReportDAO();
        return dao.getPaymentOrderReport(startDate, endStart, bean, paymentOrderNo, benNumber, ibanAccNumber);
    }
    
    public List<ReportBean> getNumberOfPoEgovVsPortal(String entityCode,String deptCode,String sectionCode,String dateFrom,String dateTo) throws GTechException {
        ReportDAO dao = new ReportDAO();
        return dao.getNumberOfPoEgovVsPortal(entityCode, deptCode, sectionCode,dateFrom,dateTo);
    }
    
    public List<ReportBean> getNumberOfPO(String entityCode,String deptCode,String sectionCode,String dateFrom,String dateTo) throws GTechException {
        ReportDAO dao = new ReportDAO();
        return dao.getNumberOfPO(entityCode, deptCode, sectionCode, dateFrom, dateTo);
    }
}
