package com.gtech.web.report;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.department.business.DepartmentBusiness;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.business.EntityBusiness;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.section.business.SectionBusiness;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@ManagedBean(name = "egovVsnewPortal")
@ViewScoped
public class EgovVsnewPortal extends GTechModelBacking {

    private ReportBean reportBean = new ReportBean();

    private String jsonChartData = "EMPTY";
    
    private List<ReportBean> list = new ArrayList<>();
    
    private String entityCode;

    private String deptCode;
    
    private String sectionCode;
    
    private List<SelectItem> entityList = new ArrayList<>();
    private List<SelectItem> deptList = new ArrayList<>();
    private List<SelectItem> sectionList = new ArrayList<>();
    private EntityBusiness entityBusiness = new EntityBusiness();
    public EgovVsnewPortal() {
        super();
    }
    
    public void init(ComponentSystemEvent event){
       
        if(entityList.isEmpty()) {
            try {
                List<EntityBean> eList = entityBusiness.getEntityList();
                deptList.clear();
                sectionList.clear();
                SelectItem si = null;
                for(EntityBean bean : eList) {
                    si = new SelectItem();
                    si.setLabel(bean.getNameAr() + " (" +bean.getCode()+ " )");
                    si.setValue(bean.getCode());
                    
                    entityList.add(si);
                }
                
            } catch (GTechException e) {
                e.printStackTrace();
            }
        }
    }
    
    public String parseJSONNumberOfPaymentOrderPerUser() throws GTechException, IOException, JsonGenerationException,
                                                              JsonMappingException {
        list.clear();
        ReportBusiness reportBusiness = new ReportBusiness();
        if(!validateCraitira()) {
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addErrorMessage("PO_CRAITRIA_VALIDATION");
            createMessages(responseContainer);
            
            list.clear();
            
            return null;
        }
        
        list = reportBusiness.getNumberOfPoEgovVsPortal(entityCode, deptCode, sectionCode,getPoDateFrom(),getPoDateTo());
        
        if(list == null || list.isEmpty()) {
            jsonChartData = "'EMPTY'";
            callJavaScriptFunction("draw('NO_DATA');");
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        jsonChartData = "[";
        for (int i =0 ;i < list.size();i++) {
            ReportBean bean = list.get(i);
            if(i == (list.size() - 1)) {
                jsonChartData += "{label :'" + bean.getLabel()+"'";
                jsonChartData += ",data :" + bean.getData()+"}";
                continue;
            }
            jsonChartData += "{label :'" + bean.getLabel()+"'";
            jsonChartData += ",data :" + bean.getData()+"},";
            
        }
        
        jsonChartData += "]";
        callJavaScriptFunction("draw("+jsonChartData+");");
        return null;
    }
    
    private boolean validateCraitira(){
        return true;
        //return (entityCode != null &&
          //      deptCode != null &&
            //    sectionCode != null);
    }


    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }
    
    public void retriveDept(ValueChangeEvent  ce) {
        try {
            deptList = new ArrayList<>();
            sectionList = new ArrayList<>();
            setDeptCode(null);
            setSectionCode(null);
            entityCode = ce.getNewValue() + "";
            EntityBean e = entityBusiness.getEntity(entityCode);
            
            if(e == null || e.getCode() == null) {
                return;
            }
            DepartmentBusiness departmentBusiness = new DepartmentBusiness();
            List<DepartmentBean> dlist = departmentBusiness.getEntityDepartmentsList(e.getId());
            
            SelectItem si = null;
            for(DepartmentBean bean : dlist) {
                si = new SelectItem();
                si.setLabel(bean.getNameAr()+ " (" +bean.getCode()+ " )");
                si.setValue(bean.getCode());
                
                deptList.add(si);
                
            }
            
            
        } catch (GTechException f) {
            f.printStackTrace();
        }

    }
    
    public void retriveSection(ValueChangeEvent  ce) {
        try {
            sectionList = new ArrayList<>();
            setSectionCode(null);
            deptCode = ce.getNewValue() + "";
            DepartmentBusiness departmentBusiness = new DepartmentBusiness();
            DepartmentBean e = departmentBusiness.getDepartment(deptCode,entityCode);
            
            if(e == null || e.getCode() == null) {
                return;
            }
            
            SectionBusiness sectionBusiness = new SectionBusiness();
            List<SectionBean> dlist = sectionBusiness.getDepartmentSectionsList(e.getId());
            
            SelectItem si = null;
            for(SectionBean bean : dlist) {
                si = new SelectItem();
                si.setLabel(bean.getNameAr()+ " (" +bean.getCode()+ " )");
                si.setValue(bean.getCode());
                
                sectionList.add(si);
                
            }
            
            
        } catch (GTechException f) {
            f.printStackTrace();
        }

    }
    
    public void setReportBean(ReportBean reportBean) {
        this.reportBean = reportBean;
    }

    public ReportBean getReportBean() {
        return reportBean;
    }

    public void setJsonChartData(String jsonChartData) {
        this.jsonChartData = jsonChartData;
    }

    public String getJsonChartData() {
        return jsonChartData;
    }

    public void setList(List<ReportBean> list) {
        this.list = list;
    }

    public List<ReportBean> getList() {
        return list;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setEntityList(List<SelectItem> entityList) {
        this.entityList = entityList;
    }

    public List<SelectItem> getEntityList() {
        return entityList;
    }

    public void setDeptList(List<SelectItem> deptList) {
        this.deptList = deptList;
    }

    public List<SelectItem> getDeptList() {
        return deptList;
    }

    public void setSectionList(List<SelectItem> sectionList) {
        this.sectionList = sectionList;
    }

    public List<SelectItem> getSectionList() {
        return sectionList;
    }
}
