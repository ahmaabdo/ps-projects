package com.gtech.web.organization.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.organization.bean.OrganizationBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class OrganizationDAO extends GTechDAO{
    
    public void insertOrganization(OrganizationBean bean) throws GTechException {
        Connection connection = null;
        PreparedStatement ps = null;
        
        String query = "INSERT INTO POG_ORGANIZATION (ORG_ID,ORG_NAME_AR,ORG_NAME_EN,EEO_ID,TTG_ID,PTN_ID,ORG_CODE,ACTIVE) " +
                        " VALUES (?,?,?,?,?,?,?,?)";
        try {
            
            connection = getConnection();
            Integer poId = getSeq("ORG_PK_SEQ", connection);
            ps = connection.prepareStatement(query);
            
            ps.setLong(1, poId); 
            ps.setString(2, bean.getNameAr()); 
            ps.setString(3, bean.getNameEn()); 
            ps.setLong(4, bean.getEntityId());
            ps.setLong(5, bean.getDeptId());
            ps.setLong(6, bean.getSectionId());
            ps.setString(7, bean.getOrgCode());
            ps.setBoolean(8, true);
            
            ps.executeUpdate();
            
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, ps);
        }

    }
    
    public List<OrganizationBean> getAll(long entityId,long deptId,long secId) throws GTechException {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<OrganizationBean> organizationBeanList = new ArrayList<>();
        
        String query = "SELECT * FROM POG_ORGANIZATION WHERE EEO_ID = ? AND TTG_ID = ? AND PTN_ID = ?";
        try {
            
            connection = getConnection();
            ps = connection.prepareStatement(query);
            
            ps.setLong(1, entityId);
            ps.setLong(2, deptId);
            ps.setLong(3, secId);
            
            rs = ps.executeQuery();
            
            OrganizationBean bean = null;
            while(rs.next()) {
                bean = new OrganizationBean();
                bean.setId(rs.getLong("ORG_ID"));
                bean.setNameAr(rs.getString("ORG_NAME_AR"));
                bean.setNameEn(rs.getString("ORG_NAME_EN"));
                bean.setEntityId(rs.getLong("EEO_ID"));
                bean.setDeptId(rs.getLong("TTG_ID"));
                bean.setSectionId(rs.getLong("PTN_ID"));
                bean.setOrgCode(rs.getString("ORG_CODE"));
                bean.setActive(rs.getBoolean("ACTIVE"));
                
                organizationBeanList.add(bean);
            }
            
            return organizationBeanList;
            
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    public void updateActive(OrganizationBean bean) throws GTechException {
        Connection connection = null;
        PreparedStatement ps = null;
        
        String query = "UPDATE POG_ORGANIZATION SET ACTIVE = ? WHERE ORG_ID = ?";
        try {
            
            connection = getConnection();
            ps = connection.prepareStatement(query);
            
            ps.setBoolean(1, bean.isActive()); 
            ps.setLong(2, bean.getId()); 
            
            ps.executeUpdate();
            
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, ps);
        }

    }
    
    public OrganizationDAO() {
        super();
    }
}
