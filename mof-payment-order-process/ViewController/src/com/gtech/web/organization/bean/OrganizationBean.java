package com.gtech.web.organization.bean;

import com.gtech.common.base.BaseBean;

public class OrganizationBean extends BaseBean{
    
    private String nameAr;
    private String nameEn;
    private Long entityId;
    private Long deptId;
    private Long sectionId;
    private String orgCode;
    private boolean active = true;
    
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgCode() {
        return orgCode;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof OrganizationBean)) {
            return false;
        }
        final OrganizationBean other = (OrganizationBean) object;
        if (!(orgCode == null ? other.orgCode == null : orgCode.equals(other.orgCode))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + ((orgCode == null) ? 0 : orgCode.hashCode());
        return result;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

}
