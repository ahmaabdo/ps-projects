package com.gtech.web.organization.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.admin.user.UserBean;
import com.gtech.web.admin.user.UserBusiness;
import com.gtech.web.organization.bean.OrganizationBean;
import com.gtech.web.organization.business.OrganizationBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.event.DialogEvent;

@ManagedBean (name = "addOrganizationBacking")
@ViewScoped
public class AddOrganizationBacking extends GTechModelBacking {
    
    private OrganizationBusiness organizationBusiness = new OrganizationBusiness();
    
    private List<SelectItem> siList = new ArrayList<>();
    
    private OrganizationBean addedBean = new OrganizationBean();
    
    private List<OrganizationBean> orgList = new ArrayList<>();
    
    private List<UserBean> userList = new ArrayList<>();
    
    private UserBusiness userBusiness = new UserBusiness();
    @PostConstruct
    public void init() {

        try {

            userList = userBusiness.getUsersByRef(getLogedInUser().getEntityBean().getId(),
                                                  getLogedInUser().getDepartmentBean().getId(),
                                                  getLogedInUser().getSectionBean().getId());
           orgList =
                organizationBusiness.getAll(getLogedInUser().getEntityBean().getId(),
                                            getLogedInUser().getDepartmentBean().getId(),
                                            getLogedInUser().getSectionBean().getId());
    
            siList.clear();
            
            SelectItem si = null;
            if(!orgList.isEmpty()) {
                
                for(OrganizationBean bean : orgList) {
                    si = new SelectItem();
                    si.setLabel(bean.getNameAr());
                    si.setValue(bean.getId());
                    siList.add(si);
                }   
            }
        } catch (GTechException e) {
            e.printStackTrace();
        }
    }
    @Override
    public String initialize() throws GTechException {
        siList.clear();
        userList.clear();
        addedBean = new OrganizationBean();
        if(!havaPermition("PO-F21")){
            return "goToCustomerInfo";
        }
        return "PO-F21";
    }
    
    public String initA() throws GTechException {
        siList.clear();
        userList.clear();
        addedBean = new OrganizationBean();
        if(!havaPermition("PO-F22")){
            return "goToCustomerInfo";
        }
        
        return "PO-F22";
    }
    
    public void addOrg(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {

            try {
                addedBean.setEntityId(getLogedInUser().getEntityBean().getId());
                addedBean.setDeptId(getLogedInUser().getDepartmentBean().getId());
                addedBean.setSectionId(getLogedInUser().getSectionBean().getId());
                
                organizationBusiness.insertOrganization(addedBean);
                addedBean = new OrganizationBean();
                
                orgList =
                     organizationBusiness.getAll(getLogedInUser().getEntityBean().getId(),
                                                 getLogedInUser().getDepartmentBean().getId(),
                                                 getLogedInUser().getSectionBean().getId());
                
                reloadComponent("panelCollection");
                
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
                createMessages(responseContainer);
            } catch (GTechException e) {
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
                createMessages(responseContainer);
                e.printStackTrace();
            }
        }
    }
    
    public String saveOrg() {
        try {
            userBusiness.updateUserOrg(userList);
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
            createMessages(responseContainer);
        } catch (GTechException e) {
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
            createMessages(responseContainer);
            e.printStackTrace();
        }
        
        return null;
    }
    
    public String updateActive() {
        try {
            organizationBusiness.updateActive(orgList);
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
            createMessages(responseContainer);
        } catch (GTechException e) {
            ResponseContainer responseContainer = new ResponseContainer();
            responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
            createMessages(responseContainer);
            e.printStackTrace();
        }
        
        return null;
    }
    
    public void setSiList(List<SelectItem> siList) {
        this.siList = siList;
    }

    public List<SelectItem> getSiList() {
        return siList;
    }

    public void setAddedBean(OrganizationBean addedBean) {
        this.addedBean = addedBean;
    }

    public OrganizationBean getAddedBean() {
        return addedBean;
    }

    public void setOrgList(List<OrganizationBean> orgList) {
        this.orgList = orgList;
    }

    public List<OrganizationBean> getOrgList() {
        return orgList;
    }

    public void setUserList(List<UserBean> userList) {
        this.userList = userList;
    }

    public List<UserBean> getUserList() {
        return userList;
    }
}
