package com.gtech.web.organization.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.organization.bean.OrganizationBean;
import com.gtech.web.organization.dao.OrganizationDAO;

import java.util.List;

public class OrganizationBusiness extends GTechModelBusiness {
    
    private OrganizationDAO dao = new OrganizationDAO();
    
    public void insertOrganization(OrganizationBean bean) throws GTechException {
        dao.insertOrganization(bean);
    }
    
    public List<OrganizationBean> getAll(long entityId,long deptId,long secId) throws GTechException {
        return dao.getAll(entityId,deptId,secId);
    }
    
    public void updateActive(List<OrganizationBean> beanList) throws GTechException {
        for(OrganizationBean bean : beanList) {
            dao.updateActive(bean);
        }
        
    }
}
