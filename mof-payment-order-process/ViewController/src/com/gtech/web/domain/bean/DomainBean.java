package com.gtech.web.domain.bean;

import com.gtech.common.base.*;

public class DomainBean extends BaseBean {


    /** Code. */
    private String code;
    private String descAr;
    private String descEn;
    private String nameAr;
    private String nameEn;
    
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }
    public void setDescAr(String descAr) {
        this.descAr = descAr;
    }

    public String getDescAr() {
        return descAr;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getDescEn() {
        return descEn;
    }
}
