package com.gtech.web.domain.dao;

import com.bea.core.repackaged.apache.commons.logging.Log;
import com.bea.core.repackaged.apache.commons.logging.LogFactory;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.domain.bean.DomainBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;


public class DomainDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(DomainDAO.class);


    public Integer getDomainIdByDescAr(String descAr, Connection connection) throws GTechException, SQLException {

        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = "SELECT DMG_ID FROM POG_DOMAINS WHERE DMG_DESC_AR = ? ";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, descAr);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            return rs.getInt("DMG_DESC_AR");
        }
        return null;

    }

    public DomainBean getDomainById(Integer id, Connection connection) throws GTechException, SQLException {

        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = "SELECT * FROM POG_DOMAINS WHERE DMG_ID = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                DomainBean domainBean = new DomainBean();
                domainBean.setId(rs.getInt("DMG_ID"));
                domainBean.setDescAr(rs.getString("DMG_DESC_AR"));
                domainBean.setDescAr(rs.getString("DMG_DESC_EN"));

                return domainBean;
            }
        } finally {
            closeResources(connection,preparedStatement,rs);
        }
        
        return null;
    }

    public List<DomainBean> getDomainValuesByDomainId(Long domainId) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<DomainBean> domainsList = new ArrayList<DomainBean>();
        String query = "SELECT GAO_ID, GAO_DESC_AR, GAO_DESC_EN FROM POG_DMG_VALUES WHERE GAO_DMG_ID = ? ";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, domainId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DomainBean domainBean = new DomainBean();
                domainBean.setId(rs.getLong("GAO_ID"));
                domainBean.setNameAr(rs.getString("GAO_DESC_AR"));
                domainBean.setNameEn(rs.getString("GAO_DESC_EN"));
                domainsList.add(domainBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return domainsList;
    }

    public DomainBean getDomainValuesByValueId(Long valueId) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        DomainBean domainBean = new DomainBean();
        String query = "SELECT GAO_ID, GAO_DESC_AR, GAO_DESC_EN FROM POG_DMG_VALUES WHERE GAO_ID = ? ";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, valueId);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                domainBean.setId(rs.getLong("GAO_ID"));
                domainBean.setNameAr(rs.getString("GAO_DESC_AR"));
                domainBean.setNameEn(rs.getString("GAO_DESC_EN"));
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return domainBean;
    }

}
