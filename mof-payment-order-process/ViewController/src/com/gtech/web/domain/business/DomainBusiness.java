package com.gtech.web.domain.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.domain.bean.DomainBean;
import com.gtech.web.domain.dao.DomainDAO;

import java.util.List;

public class DomainBusiness extends GTechModelBusiness {
    DomainDAO domainDao = new DomainDAO();

    public List<DomainBean> getDomainValuesByDomainId(Long domainId) throws GTechException {
        return domainDao.getDomainValuesByDomainId(domainId);
    }

    public DomainBean getDomainValuesByValueId(Long valueId) throws GTechException {
        return domainDao.getDomainValuesByValueId(valueId);
    }

}
