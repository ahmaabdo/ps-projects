package com.gtech.web.auditing.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.auditingPaymentOrder.backing.AuditPaymentOrderBacking;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.render.ClientEvent;

@ManagedBean(name = "singlePoAuditingBackedBean")
@SessionScoped
public class SinglePoAuditingBacking extends GTechModelBacking {
    ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean;
    private String remark;
    private String statusName;
    private String payMethodName;
    private String paramSelected;

    public SinglePoAuditingBacking() throws GTechException {
        super();
    }

    @Override
    public String initialize() throws GTechException {
        return null;
    }

    public String init() throws GTechException {
        rest();
        DomainBusiness domainBusiness = new DomainBusiness();
        paymentOrderBean = paymentOrderBusiness.getFullDataPaymentOrder(new Long(paramSelected));
        payMethodName = domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentMethod()).getNameAr();
        statusName =  domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentOrderStatus().longValue()).getNameAr();
        return "PO-F13";
    }

    private void rest() {
        remark = null;
        statusName = null;
        payMethodName = null;
        paymentOrderBean = new PaymentOrderBean();
    }

    public void handleAutoFillExpense(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
             ExpenseBean expenseBean = getPaymentOrderBean().getExpenseBean();
            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(expenseBean.getCode(),null,"POG_EXPENSE_TYPES");
            expenseBean.setCode(lookupBean.getCode());
            expenseBean.setDescAr(lookupBean.getNameAr());
            expenseBean.setDescEn(lookupBean.getNameEn());
            expenseBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        reloadComponent("EXPENSE_ID_FIELD");
        reloadComponent("EXPENSE_CODE_FIELD");
        reloadComponent("EXPENSE_DESC_FIELD");
    }


    public String returnToPaymentOrderAuditingPage() throws GTechException {

        return "PO-F11";
    }


    public void auditPaymentOrder(ActionEvent event) throws GTechException {
        
        
        if(paymentOrderBean.getExpenseBean()==null||paymentOrderBean.getExpenseBean().getId()==0){
            ResponseContainer container= new ResponseContainer();
            container.addErrorMessage("MANDETORY_EXPENCE");
            createMessages(container);
            return ;
            }
        boolean isOk = 
        paymentOrderBusiness.updatePaymentOrderExpence(paymentOrderBean.getId(), paymentOrderBean.getExpenseBean().getId())&&       
        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark,
                                              Long.parseLong(getLogedInUser().getUserID()), 100504);
    }

    public void returnPaymentOrder(ActionEvent event) throws GTechException {
        if(remark==null||remark.isEmpty()){
            ResponseContainer container= new ResponseContainer();
            container.addErrorMessage("MANDETORY_REMARKS");
            createMessages(container);
            return ;
            }
        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100510);
    }


    public String auditPaymentOrder() throws GTechException {

//        if(paymentOrderBean.getExpenseBean()==null||paymentOrderBean.getExpenseBean().getId()==0){
//            ResponseContainer container= new ResponseContainer();
//            container.addErrorMessage("MANDETORY_EXPENCE");
//            createMessages(container);
//            return null;
//            }
        boolean isOk =  new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark,
                                              Long.parseLong(getLogedInUser().getUserID()), 100504);
        
//        boolean isOk = 
//        paymentOrderBusiness.updatePaymentOrderExpence(paymentOrderBean.getId(), paymentOrderBean.getExpenseBean().getId())&&       
//        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark,
//                                              Long.parseLong(getLogedInUser().getUserID()), 100504);
        
        String checkFunds = paymentOrderBusiness.isHaveCheckFunds(paymentOrderBean.getPaymentOrderNumber());
        
        if(isOk){
            HttpSession  httpSession= (HttpSession)  FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            AuditPaymentOrderBacking auditPaymentOrderBacking = (AuditPaymentOrderBacking) httpSession.getAttribute("auditPaymentOrderBackedBean");
            auditPaymentOrderBacking.setCheckFunds(checkFunds);
            return auditPaymentOrderBacking.backFromAuditSinglePaymentOrder();  
        }

        return null;
    }

    public String returnPaymentOrder() throws GTechException {
        if(remark==null||remark.isEmpty()){
            ResponseContainer container= new ResponseContainer();
            container.addErrorMessage("MANDETORY_REMARKS");
            createMessages(container);
            return null;
            }
        ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
        boolean isOk = actionLogBusiness.insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100510);
        
        if(isOk){
            HttpSession  httpSession= (HttpSession)  FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            AuditPaymentOrderBacking auditPaymentOrderBacking = (AuditPaymentOrderBacking) httpSession.getAttribute("auditPaymentOrderBackedBean");
            return auditPaymentOrderBacking.backFromAuditSinglePaymentOrder();  
        }
        
        return  null;
        
    }



    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setPayMethodName(String payMethodName) {
        this.payMethodName = payMethodName;
    }

    public String getPayMethodName() {
        return payMethodName;
    }

    public void setParamSelected(String paramSelected) {
        this.paramSelected = paramSelected;
    }

    public String getParamSelected() {
        return paramSelected;
    }
}
