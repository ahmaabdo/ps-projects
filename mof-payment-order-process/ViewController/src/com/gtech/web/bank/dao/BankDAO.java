package com.gtech.web.bank.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.bank.bean.BankBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BankDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(BankDAO.class);

    public BankBean getBank(String iban) throws GTechException {
        BankBean bankBean = null;
        Connection connection = getConnection();
        try {
            bankBean = getBank(iban, connection);
        } finally {
            closeResources(connection);
        }
        return bankBean;
    }

    public BankBean getBank(String iban, Connection connection) throws GTechException {
        CallableStatement statement = null;
        BankBean bankBean = null;
        ResultSet rs = null;
        String query = "{ ? = call pog_util.get_bank_by_iban (?) } ";
        try {
            statement = connection.prepareCall(query);
            statement.registerOutParameter(1, Types.VARCHAR);
            statement.setString(2, iban);
            statement.executeUpdate();
            bankBean = new BankBean();
            bankBean.setName(statement.getString(1));
           
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(statement, rs);
        }
        return bankBean;
    }
}
