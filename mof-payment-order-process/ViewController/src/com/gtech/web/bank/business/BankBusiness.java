package com.gtech.web.bank.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.bank.bean.BankBean;
import com.gtech.web.bank.dao.BankDAO;

public class BankBusiness extends GTechModelBusiness {


    public BankBean getBank(String iban) throws GTechException {
        BankDAO bankDAO = new BankDAO();
        return bankDAO.getBank(iban);
    }
}
