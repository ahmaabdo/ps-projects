package com.gtech.web.bank.bean;

import com.gtech.common.base.BaseBean;

public class BankBean extends BaseBean {


    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
