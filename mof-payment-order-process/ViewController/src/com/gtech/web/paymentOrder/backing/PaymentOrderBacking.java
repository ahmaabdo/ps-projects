package com.gtech.web.paymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.util.StringUtilities;
import com.gtech.web.adminhijridate.business.CalendarBusiness;
import com.gtech.web.bank.bean.BankBean;
import com.gtech.web.bank.business.BankBusiness;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.currency.business.CurrencyBusiness;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.domain.bean.DomainBean;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.generalinfo.business.GeneralInfoBusiness;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.nationality.nationalitybusiness.NationalityBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;
import com.gtech.web.project.bean.ProjectBean;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;

@ManagedBean(name = "paymentOrderBacking")
@SessionScoped
public class PaymentOrderBacking extends GTechModelBacking {
    private long distSeq = -1;
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean = new PaymentOrderBean();
    private CurrencyBusiness currencyBusiness = new CurrencyBusiness();
    private List<NationalityBean> nationailtiesList;
    private List<DomainBean> paymentsMethodList;
    private String poID;
    private boolean refundPaymentOrderNavigation;
    private boolean checkSection;

    public PaymentOrderBacking() throws GTechException {
        doInitPropartis();

    }

    private void doInitPropartis() throws GTechException {
        checkSection = false;
        paymentOrderBean = new PaymentOrderBean();
        paymentOrderBean.setOrgId(getLogedInUser().getOrganizationBean().getId());
        paymentOrderBean.setOrg(getLogedInUser().getOrganizationBean().getOrgCode());
        paymentOrderBean.setPaymentOrderDateG(getCurrentDate());
        paymentOrderBean.setPaymentOrderDateH(new CalendarBusiness().getCurrentHijriDate());
        paymentOrderBean.setPaymentOrderYear(new GeneralInfoBusiness()
                                             .getGeneralInfoYears(new Date(System.currentTimeMillis())));
        //EntityBean entityBean = new EntityBusiness().getUserEntity(getLogedInUser().getUserID());

        paymentOrderBean.setDepartmentBean(getLogedInUser().getDepartmentBean());
        if(getLogedInUser().getSectionBean() != null && getLogedInUser().getSectionBean().getId() > 0) {
            checkSection = false;
            paymentOrderBean.setSectionBean(getLogedInUser().getSectionBean());
        } else {
            checkSection = true;
            paymentOrderBean.setSectionBean(new SectionBean());
        }
        
        paymentOrderBean.setEntityBean(getLogedInUser().getEntityBean());

//        paymentOrderBean.setDepartmentBean(new DepartmentBean());
//        paymentOrderBean.setSectionBean(new SectionBean());


        paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
        paymentOrderBean.setCurrencyBean(new CurrencyBean());
        paymentOrderBean.setExpenseBean(new ExpenseBean());
        nationailtiesList = new NationalityBusiness().getNationalityList();
        paymentOrderBean.setNationalityBean(new NationalityBean());
        paymentsMethodList = new DomainBusiness().getDomainValuesByDomainId(1001L);
        paymentOrderBean.setPaymentMethod(paymentsMethodList.get(0).getId());
        paymentOrderBean.setBankBean(new BankBean());
        paymentOrderBean.getNationalityBean().setNationalityID("1");
        paymentOrderBean.getNationalityBean().setNationalityCode("SA");
        CurrencyBean currencyBean = new GeneralInfoBusiness().getSaudiRialCurrency();
        paymentOrderBean.setCurrencyBean(currencyBean);
        reloadComponent("paymentOrderFrom");

    }

    @Override
    public String initialize() throws GTechException {

        if(!havaPermition("PO-F1")){
            return null;
        }

        paymentOrderBean = new PaymentOrderBean();
        doInitPropartis();
        setRefundPaymentOrderNavigation(false);
        return "PO-F1";
    }

    public void handleAutoFillBranch(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();


        try {
            DepartmentBean departmentBean = getPaymentOrderBean().getDepartmentBean();

            if(StringUtilities.isEmpty(departmentBean.getCode())){

                getPaymentOrderBean().setDepartmentBean(new DepartmentBean());

                reloadComponent("BRANCH_CODE_FIELD");
                reloadComponent("BRANCH_ID_FIELD");
                reloadComponent("BRANCH_DESC_FIELD");

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                runJS();
                return;
            }

            LookupBean lookupBean = lookupBusiness.getLookupValueAutoFilled(departmentBean.getCode(),
                                                        String.valueOf(getPaymentOrderBean().getEntityBean().getId()),
                                                        "POG_DEPARTMENTS");
            departmentBean.setCode(lookupBean.getCode());
            departmentBean.setNameAr(lookupBean.getNameAr());
            departmentBean.setNameEn(lookupBean.getNameEn());
            departmentBean.setId(lookupBean.getId());



            runJS();
            getPaymentOrderBean().setSectionBean(new SectionBean());

        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("BRANCH_CODE_FIELD");
        reloadComponent("BRANCH_ID_FIELD");
        reloadComponent("BRANCH_DESC_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }

    public void handleBranch(ClientEvent ce) {
        getPaymentOrderBean().setSectionBean(new SectionBean());
        runJS();
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }

    public void handleAutoFillSection(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {

            SectionBean sectionBean = getPaymentOrderBean().getSectionBean();

            if(StringUtilities.isEmpty(sectionBean.getCode())){
                getPaymentOrderBean().setSectionBean(new SectionBean());

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                runJS();
                return;
            }

            LookupBean lookupBean = lookupBusiness.getLookupValueAutoFilled(sectionBean.getCode(), String.valueOf(paymentOrderBean.getDepartmentBean().getId()), "POG_SECTION");
            sectionBean.setCode(lookupBean.getCode());
            sectionBean.setNameAr(lookupBean.getNameAr());
            sectionBean.setNameEn(lookupBean.getNameEn());
            sectionBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("SECTION_CODE_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }

    public void handleAutoFillCurrency(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
            CurrencyBean currencyBean = getPaymentOrderBean().getCurrencyBean();
            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilledCur(currencyBean.getCode(), null, "POG_CURRENCIES");
            currencyBean.setCode(lookupBean.getCode());
            currencyBean.setNameAr(lookupBean.getNameAr());
            currencyBean.setNameEn(lookupBean.getNameEn());
            currencyBean.setCurId(lookupBean.getCurCode());
            if(currencyBean.getCode() != null && paymentOrderBean.getAmount() != null && paymentOrderBean.getAmount() > 0) {
                callTafkeet(String.valueOf(paymentOrderBean.getAmount()),currencyBean.getCode());
            } 

        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("CURRENCY_CODE_FIELD");
        reloadComponent("CURRENCY_ID_FIELD");
        reloadComponent("CURRENCY_DESC_FIELD");
    }
    
    
    private EcomomicClassificationBean fillCalssifcation(String classId ){
        
        EcomomicClassificationBean ecomomicClassificationBean = new EcomomicClassificationBean();
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
        LookupBean lookupBean =
            lookupBusiness.getbyClassFicationId(classId);
            ecomomicClassificationBean.setCode(lookupBean.getCode());
            ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
            ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
            ecomomicClassificationBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        return  ecomomicClassificationBean;
    }
    
    
    
    /*
     *
     * add lookup in table AutoFill
     * */
    public void handleAutoFillProject(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        runJS();
        try {

            Double index = (Double) ce.getParameters().get("index");
            DistributionsPaymentOrderBean distributionsPaymentOrderBean = paymentOrderBean.getDistributionsPoBeanList().get(index.intValue());
            ProjectBean projectBean = distributionsPaymentOrderBean.getProjectBean();
            if(projectBean.getCode() == null ||projectBean.getCode().isEmpty()){
                
                distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
                distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
                reloadComponent("dateTableId");
                return;
            }
            LookupBean lookupBean =
                lookupBusiness.getLookupProjectValueAutoFilled(projectBean.getCode(), paymentOrderBean.getEntityBean().getCode(), paymentOrderBean.getSectionBean().getCode() + "",paymentOrderBean.getDepartmentBean().getCode() + "");
            if(lookupBean.getClassificationsId() != null) {
                distributionsPaymentOrderBean.setEcomomicClassificationBean(fillCalssifcation(lookupBean.getClassificationsId()));    
            } else {
                distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            }
            
            
            projectBean.setCode(lookupBean.getCode());
            distributionsPaymentOrderBean.setProgDesc(lookupBean.getNameAr());
            projectBean.setNameAr(lookupBean.getNameAr());
            projectBean.setNameEn(lookupBean.getNameEn());
            projectBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        reloadComponent("dateTableId");

    }
    /*
     *
     * add lookup in table open dialog
     * */
    public void handleProject(ClientEvent ce) {
        Double index = (Double) ce.getParameters().get("index");
        String filedId = (String) ce.getParameters().get("filedId");
        String filedDesc = (String) ce.getParameters().get("filedDesc");
        String filedCode = (String) ce.getParameters().get("filedCode");
        try {
            LookupBusiness lookupBusiness = new LookupBusiness();
            LookupBean lookupBean =
                lookupBusiness.getLookupProjectValueAutoFilled(filedCode, paymentOrderBean.getEntityBean().getCode(), paymentOrderBean.getSectionBean().getId() + "",paymentOrderBean.getDepartmentBean().getId() + "");
        
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                paymentOrderBean.getDistributionsPoBeanList().get(index.intValue());
            ProjectBean projectBean = distributionsPaymentOrderBean.getProjectBean();
                if(lookupBean.getClassificationsId() != null ) {
                    distributionsPaymentOrderBean.setEcomomicClassificationBean(fillCalssifcation(lookupBean.getClassificationsId()));
                    
                }else {
                    distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
                    
                }
            projectBean.setCode(filedCode.toString());
            projectBean.setNameAr(filedDesc);
        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("dateTableId");
    }


    public void handleAutoFillClassification(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        runJS();
        try {

            Double index = (Double) ce.getParameters().get("index");
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                paymentOrderBean.getDistributionsPoBeanList().get(index.intValue());
            EcomomicClassificationBean ecomomicClassificationBean = distributionsPaymentOrderBean.getEcomomicClassificationBean();

            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(ecomomicClassificationBean.getCode(), null, "POG_ECOMOMIC_CLASSIFICATIONS");
            ecomomicClassificationBean.setCode(lookupBean.getCode());
            ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
            ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
            ecomomicClassificationBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        reloadComponent("dateTableId");

    }
    /*
     *
     * add lookup in table open dialog
     * */
    public void handleClassification(ClientEvent ce) {
        Double index = (Double) ce.getParameters().get("index");
        String filedId = (String) ce.getParameters().get("filedId");
        String filedDesc = (String) ce.getParameters().get("filedDesc");
        String filedCode = (String) ce.getParameters().get("filedCode");
        runJS();
        DistributionsPaymentOrderBean distributionsPaymentOrderBean =
            paymentOrderBean.getDistributionsPoBeanList().get(index.intValue());
        EcomomicClassificationBean ecomomicClassificationBean = distributionsPaymentOrderBean.getEcomomicClassificationBean();
        ecomomicClassificationBean.setCode(filedCode.toString());
        ecomomicClassificationBean.setNameAr(filedDesc);

    }

    public void handlex(ValueChangeEvent event) {




    }

    public void paymentMethodOnChangeListener(ValueChangeEvent event) throws GTechException {
        Long paymentMethod = (Long) event.getNewValue();
        paymentOrderBean.setPaymentMethod(paymentMethod);
        if (paymentMethod.intValue() == 100102) {
            CurrencyBean currencyBean = new GeneralInfoBusiness().getSaudiRialCurrency();
            paymentOrderBean.setCurrencyBean(currencyBean);
            paymentOrderBean.getNationalityBean().setNationalityID("1");
            paymentOrderBean.getNationalityBean().setNationalityCode("SA");
        } else {
            paymentOrderBean.setIban("");
            paymentOrderBean.setBankBean(new BankBean());
        }
        reloadComponent("IBAN_NUMBER");
        reloadComponent("BANK_CODE_LIST");

        reloadComponent("NATIONALITY_LIST");
        reloadComponent("PO_BENEFICARY_NAME");
        reloadComponent("BEN_NUMBER");
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
         FacesContext facesContext = FacesContext.getCurrentInstance();
         UIComponent component = facesContext.getViewRoot().findComponent("CURRENCY_CODE_FIELD");
        RichInputText richInputText = (RichInputText) component;
        richInputText.clearCachedClientIds();
        richInputText.clearInitialState();
        richInputText.resetValue();
        richInputText.setValue(paymentOrderBean.getCurrencyBean().getCode());
        adfFacesContext.addPartialTarget(richInputText);
        
        runJS();
        
        reloadComponent("CURRENCY_CODE_BUTTON");
        reloadComponent("CURRENCY_CODE_FIELD");
        reloadComponent("CURRENCY_ID_FIELD");
        reloadComponent("CURRENCY_DESC_FIELD");
    }

    public void handleServerEvent(ClientEvent ce) throws GTechException {
        String poNumber = (String) ce.getParameters().get("poNumber");
        ResponseContainer responseContainer = new ResponseContainer();
        if (poNumber.length() == 8) {
            String ePattern = "[0-9]*";
            java.util.regex.Pattern p = java.util
                                            .regex
                                            .Pattern
                                            .compile(ePattern);
            java.util.regex.Matcher m = p.matcher(poNumber);
            if (!m.matches()) {
                responseContainer.addErrorMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8");
                runJS();
                createMessages(responseContainer);
                return;
            }
        } else {
            responseContainer.addErrorMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8");
            createMessages(responseContainer);
            runJS();
            return;
        }
        runJS();
//        PaymentOrderBean savedPo = paymentOrderBusiness.getFullDataPaymentOrderByPONumberWithoutId(poNumber, getLogedInUser().getEecID());
//
//        if (savedPo != null) {
//            setPaymentOrderBean(savedPo);
//        }
        reloadComponent("paymentOrderFrom");
    }


    public String getFullDataPaymentOrderByPOID() throws GTechException {
        PaymentOrderBean savedPo = paymentOrderBusiness.getFullDataPaymentOrderByPOID(getPoID());
        savedPo.setOrgId(getLogedInUser().getOrganizationBean().getId());
        savedPo.setOrg(getLogedInUser().getOrganizationBean().getOrgCode());
        if (savedPo != null) {
            setPaymentOrderBean(savedPo);
        }
        return "PO-F1";
    }

    public String goToRefundPaymentOrderNavigation() throws GTechException {
        setRefundPaymentOrderNavigation(false);
        return "PO-F10";
    }


    public void validateBeneficiaryNumber(ValueChangeEvent event) throws GTechException {
        String beneficiaryCode = (String) event.getNewValue();
        System.out.println(beneficiaryCode);
        ResponseContainer container = paymentOrderBusiness.validateBeneficiaryNumber(beneficiaryCode);

        
        if (!container.containsErrorMessages()) {
            BeneficiaryBean beneficiaryBean = (BeneficiaryBean) container.getDataBean();
            paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
            paymentOrderBean.setBeneficaryName(beneficiaryBean.getNameAr());
        } else {
            paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
            paymentOrderBean.setBeneficaryName("");
        }
        
        if(beneficiaryCode.equals("999")){
            paymentOrderBean.setBeneficaryName("");
        }
        
        reloadComponent("REC_NAME_GROUP");
        reloadComponent("PO_BENEFICARY_NAME");
        runJS();
        createMessages(container);
    }
    
    public void runJS(){
        callJavaScriptFunction(" $('input').keydown( function(e) {\n" + 
        "                    \n" + 
        "        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;\n" + 
        "        if(key == 13) {\n" + 
        "            e.preventDefault();\n" + 
        "            var inputs = $(this).closest('form').find(':input:enabled:visible');\n" + 
        "            console.log(inputs.eq( inputs.index(this)+ 1 ));\n" + 
        "            inputs.eq( inputs.index(this)+ 1 ).focus();\n" + 
        "        }\n" + 
        "    });\n" + 
        "    $('select').keydown( function(e) {       \n" + 
        "            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;\n" + 
        "            if(key == 13) {\n" + 
        "                e.preventDefault();\n" + 
        "                var inputs = $(this).closest('form').find(':input:enabled:visible');\n" + 
        "                console.log(inputs.eq( inputs.index(this)+ 1 ));\n" + 
        "                inputs.eq( inputs.index(this)+ 1 ).focus();\n" + 
        "            }\n" + 
        "        });\n" + 
        "        $('textarea').keydown( function(e) {\n" + 
        "                var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;\n" + 
        "                if(key == 13) {\n" + 
        "                    e.preventDefault();\n" + 
        "                    var inputs = $(this).closest('form').find(':input:enabled:visible');\n" + 
        "                    console.log(inputs.eq( inputs.index(this)+ 1 ));\n" + 
        "                    inputs.eq( inputs.index(this)+ 1 ).focus();\n" + 
        "                }\n" + 
        "            });");
    }
    public void validateIbanNumber(ValueChangeEvent event) throws GTechException {
        String iban = (String) event.getNewValue();
        ResponseContainer container =
            paymentOrderBusiness.validateIbanNumber(iban, paymentOrderBean.getBeneficiaryBean().getCode());
        if (container.containsErrorMessages()) {
            createMessages(container);
            runJS();
            return;
        }

        BankBean bankBean = new BankBusiness().getBank(iban);
        if (bankBean != null) {
            paymentOrderBean.setBankBean(bankBean);
            reloadComponent("BANK_CODE_LIST");
        }
        
        runJS();
    }

    public void addNewDistripution(ActionEvent event) throws GTechException {
        boolean isOk = true;
        runJS();
        List<DistributionsPaymentOrderBean> distributionsPaymentOrderBeanList =
            paymentOrderBean.getDistributionsPoBeanList();
        if (distributionsPaymentOrderBeanList != null && !distributionsPaymentOrderBeanList.isEmpty()) {
            int lastIndex = distributionsPaymentOrderBeanList.size() - 1;
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                distributionsPaymentOrderBeanList.get(lastIndex);
            ResponseContainer responseContainer = new ResponseContainer();
            Double amount = distributionsPaymentOrderBean.getAmount();
            if (amount == null || amount.equals(0.0)) {
                isOk = false;
                responseContainer.addErrorMessage("AMOUNT_SHOULD_NOT_EQUALS_ZERO");
            }
            if (!isOk) {
                createMessages(responseContainer);
                return;
            }
            if (!paymentOrderBusiness.validatePaymentDistribution(distributionsPaymentOrderBean)) {
                return;
            }
        }

        DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
        distributionsPaymentOrderBean.setId(distSeq);
        distSeq--;
        distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
        distributionsPaymentOrderBean.setRefundAmount(0.0);
        distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
        paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);
        reloadComponent("dateTableId");
    }

    public void deleteDistriputionLine(ActionEvent event) throws GTechException {
        DistributionsPaymentOrderBean distributionsPaymentOrderBean =
            (DistributionsPaymentOrderBean) event.getComponent()
                                                                                                           .getAttributes()
                                                                                                           .get("distribution");
        runJS();        
        paymentOrderBean.getDistributionsPoBeanList().remove(distributionsPaymentOrderBean);
        reloadComponent("dateTableId");
    }

    public void savePaymentOrder(ActionEvent event) throws GTechException {
        ResponseContainer responseContainer = new ResponseContainer();
        if(paymentOrderBean == null){
           return;
        }
        paymentOrderBean.setUserId(getLogedInUser().getUserID() != null ? Integer.parseInt(getLogedInUser().getUserID()) : 0);
        
        paymentOrderBean.getCurrencyBean().setCurId(paymentOrderBean.getCurrencyBean().getCode());
        if(paymentOrderBean.getCurrencyBean() == null || paymentOrderBean.getCurrencyBean().getCurId() == null || paymentOrderBean.getCurrencyBean().getCurId().isEmpty()){
            responseContainer.addErrorMessage("MANDETORY_CUR");
            createMessages(responseContainer);
            return;
        }
        if(paymentOrderBean.getDepartmentBean() == null || paymentOrderBean.getDepartmentBean().getId() == 0){
            responseContainer.addErrorMessage("MANDETORY_DEP");
            createMessages(responseContainer);
            return;
        }
        if(paymentOrderBean.getSectionBean() == null || paymentOrderBean.getSectionBean().getId() == 0){
            responseContainer.addErrorMessage("MANDETORY_SEC");
            createMessages(responseContainer);
            return;
        }

        String poNumber = paymentOrderBean.getPaymentOrderNumber();
        if (poNumber.length() == 8) {
            String ePattern = "[0-9]*";
            java.util.regex.Pattern p = java.util
                                            .regex
                                            .Pattern
                                            .compile(ePattern);
            java.util.regex.Matcher m = p.matcher(poNumber);
            if (!m.matches()) {
                responseContainer.addErrorMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8");
                createMessages(responseContainer);
                return;
            }
        } else {
            responseContainer.addErrorMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8");
            createMessages(responseContainer);
            return;
        }

        responseContainer = paymentOrderBusiness.paymentOrderSaveBusiness(paymentOrderBean, getLogedInUser());
        

        if (responseContainer.containsErrorMessages() || responseContainer.containsInfoMessages()) {
            createMessages(responseContainer);
            
            if(responseContainer.containsInfoMessages()) {
//                ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
//                actionLogBusiness.insereLogLine(paymentOrderBean.getId(),"",Long.parseLong(getLogedInUser().getUserID()),100501);
//                
                doInitPropartis();
                setRefundPaymentOrderNavigation(false);
                reloadComponent("paymentOrderFrom");
            }
        }
    }


    public String resetForm() throws GTechException {
        doInitPropartis();
        setRefundPaymentOrderNavigation(false);
        return null;
    }

    public void fillAmountInWord(ValueChangeEvent event) throws GTechException {
        double amount = (Double) event.getNewValue();
        callTafkeet(String.valueOf(amount), paymentOrderBean.getCurrencyBean().getCode());
        runJS();
    }

    public void callTafkeet(String amount , String currency) throws GTechException {
        String textAmount = currencyBusiness.getTafkeetCurrency(amount,currency);
        paymentOrderBean.setAmountInWord(textAmount);
        reloadComponent("AMOUNT_TEXT_FIELD");
    }
    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setNationailtiesList(List<NationalityBean> nationailtiesList) {
        this.nationailtiesList = nationailtiesList;
    }

    public List<NationalityBean> getNationailtiesList() {
        return nationailtiesList;
    }

    public void setPaymentsMethodList(List<DomainBean> paymentsMethodList) {
        this.paymentsMethodList = paymentsMethodList;
    }

    public List<DomainBean> getPaymentsMethodList() {
        return paymentsMethodList;
    }

    public void setPoID(String poID) {
        this.poID = poID;
    }

    public String getPoID() {
        return poID;
    }

    public void setRefundPaymentOrderNavigation(boolean refundPaymentOrderNavigation) {
        this.refundPaymentOrderNavigation = refundPaymentOrderNavigation;
    }

    public boolean isRefundPaymentOrderNavigation() {
        return refundPaymentOrderNavigation;
    }

    public void setCheckSection(boolean checkSection) {
        this.checkSection = checkSection;
    }

    public boolean isCheckSection() {
        return checkSection;
    }

}
