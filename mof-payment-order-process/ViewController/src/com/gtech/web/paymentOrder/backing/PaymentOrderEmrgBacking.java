package com.gtech.web.paymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.util.StringUtilities;
import com.gtech.faces.util.MessageReader;
import com.gtech.web.bank.bean.BankBean;
import com.gtech.web.bank.business.BankBusiness;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.beneficiary.business.BeneficiaryBusiness;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.currency.business.CurrencyBusiness;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.department.business.DepartmentBusiness;
import com.gtech.web.domain.bean.DomainBean;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.business.EntityBusiness;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.generalinfo.business.GeneralInfoBusiness;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;
import com.gtech.web.project.bean.ProjectBean;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.section.business.SectionBusiness;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.chrono.HijrahChronology;
import java.time.chrono.HijrahDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.model.AutoSuggestUIHints;
import oracle.adf.view.rich.render.ClientEvent;

@ManagedBean(name = "paymentOrderEmrgBacking")
@SessionScoped
public class PaymentOrderEmrgBacking extends GTechModelBacking {
    private long distSeq = -1;
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean = new PaymentOrderBean();
    private CurrencyBusiness currencyBusiness = new CurrencyBusiness();
    private List<NationalityBean> nationailtiesList;
    private List<DomainBean> paymentsMethodList;
    private String poID;
    private boolean refundPaymentOrderNavigation;
    private boolean checkSection;
    private List<DistributionsPaymentOrderBean> distributionsPoBeanHasmyat = new ArrayList<>();
    private List<DistributionsPaymentOrderBean> distributionsPoBean = new ArrayList<>();
    private boolean saved = false;
    private String savePONumber;
    private static final String[] MOF_SPECIAL_USERS = new String[] {
        "2441-1", "7500-1", "2558-1", "9560-1", "5646-1", "2540-1", "5413-1", "5877-1", "10102-1", "6983-1", "10772-1",
        "7039-1", "7111-1", "9985-1", "11133-1", "8157-1", "9359-1", "8083-1", "8001-1", "8036-1", "8273-1", "10451-1",
        "9449-1", "admin"
    };
    private static final String[] MOF_SPECIAL_USERS_IBAN = new String[] {
        "admin", "2441-1", "7500-1", "2558-1", "9560-1", "5646-1", "2441", "7500", "2558", "9560", "5646"
    };
    private static final String[] MOF_SPECIAL_USERS_IS_RECIVED = new String[] {
        "admin", "2441", "7500", "2558", "9560", "5646", "2558", "3917", "2540", "9560", "5413", "5646", "5877",
        "10102", "6983", "10772", "7039", "7111", "9985", "11133", "8157", "9359", "8083", "8001", "8036", "8273",
        "10451", "1-9449", "9449-1", "9449"
    };

    private BeneficiaryBusiness beneficiaryBusiness = new BeneficiaryBusiness();
    private List<String[]> GFSSOArrayList = new ArrayList<>();

    public PaymentOrderEmrgBacking() throws GTechException {
        doInitPropartis();

    }

    private void doInitPropartis() throws GTechException {
        checkSection = false;
        paymentOrderBean = new PaymentOrderBean();
        GFSSOArrayList = beneficiaryBusiness.getSNOCODE();
        paymentOrderBean.setOrgId(getLogedInUser().getOrganizationBean().getId());
        paymentOrderBean.setOrg(getLogedInUser().getOrganizationBean().getOrgCode());
        paymentOrderBean.setPaymentOrderDateG(getCurrentDate());
        paymentOrderBean.setPaymentOrderDateH("");
        if (!Arrays.asList(MOF_SPECIAL_USERS).contains(getLogedInUser().getUserName())) {
            paymentOrderBean.setPaymentOrderYear(new GeneralInfoBusiness()
                                                 .getGeneralInfoYears(new Date(System.currentTimeMillis())));

        } else {
            paymentOrderBean.setPaymentOrderYear("1439/1440");
        }
        //EntityBean entityBean = new EntityBusiness().getUserEntity(getLogedInUser().getUserID());

        paymentOrderBean.setDepartmentBean(getLogedInUser().getDepartmentBean());
        if (getLogedInUser().getSectionBean() != null && getLogedInUser().getSectionBean().getId() > 0) {
            checkSection = false;
            paymentOrderBean.setSectionBean(getLogedInUser().getSectionBean());
        } else {
            checkSection = true;
            paymentOrderBean.setSectionBean(new SectionBean());
        }

        paymentOrderBean.setSectionBean(new SectionBean());
        paymentOrderBean.setEntityBean(new EntityBean());

        paymentOrderBean.setGfsCode(null);
        paymentOrderBean.setDepartmentBean(new DepartmentBean());
        //        paymentOrderBean.setSectionBean(new SectionBean());

        getDistributionsPoBean().clear();
        for (int i = 0; i < 5; i++) {
            DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
            distributionsPaymentOrderBean.setId(distSeq);
            distSeq--;
            distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
            distributionsPaymentOrderBean.setRefundAmount(0.0);
            distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            getDistributionsPoBean().add(distributionsPaymentOrderBean);
        }

        getDistributionsPoBeanHasmyat().clear();
        for (int i = 0; i < 5; i++) {
            DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
            distributionsPaymentOrderBean.setId(distSeq);
            distSeq--;
            distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
            distributionsPaymentOrderBean.setRefundAmount(0.0);
            distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            getDistributionsPoBeanHasmyat().add(distributionsPaymentOrderBean);
        }
        paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
        paymentOrderBean.setCurrencyBean(new CurrencyBean());
        paymentOrderBean.setExpenseBean(new ExpenseBean());
        //nationailtiesList = new NationalityBusiness().getNationalityList();
        paymentOrderBean.setNationalityBean(new NationalityBean());
        paymentsMethodList = new DomainBusiness().getDomainValuesByDomainId(1001L);
        paymentOrderBean.setPaymentMethod(100102L);
        paymentOrderBean.setPaymentMethodMof(2);
        paymentOrderBean.setBankBean(new BankBean());
        paymentOrderBean.getNationalityBean().setNationalityID("1");
        paymentOrderBean.getNationalityBean().setNationalityCode("SA");
        CurrencyBean currencyBean = new GeneralInfoBusiness().getSaudiRialCurrency();
        paymentOrderBean.setCurrencyBean(currencyBean);
        paymentOrderBean.setExchangeOf(null);
        //        ExpenseBusiness expenseBusiness = new ExpenseBusiness();
        //        try {
        //            paymentOrderBean.setExpenseBean(expenseBusiness.getExpense("40"));
        //        } catch (SQLException e) {
        //            e.printStackTrace();
        //        }
        //        reloadComponent("paymentOrderFrom");

    }

    @Override
    public String initialize() throws GTechException {

        if (!havaPermition("PO-F01")) {
            return null;
        }
        setSaved(false);
        paymentOrderBean = new PaymentOrderBean();
        doInitPropartis();
        setRefundPaymentOrderNavigation(false);
        return "PO-F01";
    }

    public void handleAutoFillBranch(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();


        try {
            DepartmentBean departmentBean = getPaymentOrderBean().getDepartmentBean();

            if (StringUtilities.isEmpty(departmentBean.getCode())) {

                getPaymentOrderBean().setDepartmentBean(new DepartmentBean());

                reloadComponent("BRANCH_CODE_FIELD");
                reloadComponent("BRANCH_ID_FIELD");
                reloadComponent("BRANCH_DESC_FIELD");

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                runJS();
                return;
            }

            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(departmentBean.getCode(),
                                                        String.valueOf(getPaymentOrderBean().getEntityBean().getId()),
                                                        "POG_DEPARTMENTS");
            departmentBean.setCode(lookupBean.getCode());
            departmentBean.setNameAr(lookupBean.getNameAr());
            departmentBean.setNameEn(lookupBean.getNameEn());
            departmentBean.setId(lookupBean.getId());

            getPaymentOrderBean().setSectionBean(new SectionBean());

        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("BRANCH_DESC_FIELD");
        reloadComponent("BRANCH_CODE_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }

    public void fillEntity(ValueChangeEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();

        getPaymentOrderBean().getEntityBean().setCode(ce.getNewValue() + "");

        try {
            EntityBean entityBean = getPaymentOrderBean().getEntityBean();

            if (StringUtilities.isEmpty(entityBean.getCode())) {

                getPaymentOrderBean().setEntityBean(new EntityBean());

                runJS();
                reloadComponent("BRANCH_CODE_FIELD");
                reloadComponent("BRANCH_ID_FIELD");
                reloadComponent("BRANCH_DESC_FIELD");

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");

                reloadComponent("ENTITY_DESC_LIST");
                reloadComponent("ENTITY_DESC_LABEL");
                reloadComponent("ENTITY_CODE_LIST");
                reloadComponent("ENTITY_CODE_LABEL");
                return;
            }

            LookupBean lookupBean = lookupBusiness.getLookupValueAutoFilled(entityBean.getCode(), null, "POG_ENTITIES");
            entityBean.setCode(lookupBean.getCode());
            entityBean.setNameAr(lookupBean.getNameAr());
            entityBean.setNameEn(lookupBean.getNameEn());
            entityBean.setId(lookupBean.getId());

            getPaymentOrderBean().setDepartmentBean(new DepartmentBean());
            getPaymentOrderBean().setSectionBean(new SectionBean());

        } catch (GTechException e) {
            runJS();
            e.printStackTrace();
        }
        runJS();
        reloadComponent("BRANCH_CODE_FIELD");
        reloadComponent("BRANCH_ID_FIELD");
        reloadComponent("BRANCH_DESC_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");

        reloadComponent("ENTITY_DESC_LIST");
        reloadComponent("ENTITY_DESC_LABEL");
        reloadComponent("ENTITY_CODE_LIST");
        reloadComponent("ENTITY_CODE_LABEL");
    }

    public void handleBranch(ClientEvent ce) {
        getPaymentOrderBean().setSectionBean(new SectionBean());
        runJS();
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }

    public void handleAutoFillSection(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {

            SectionBean sectionBean = getPaymentOrderBean().getSectionBean();

            if (StringUtilities.isEmpty(sectionBean.getCode())) {
                getPaymentOrderBean().setSectionBean(new SectionBean());

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                runJS();
                return;
            }

            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(sectionBean.getCode(),
                                                        String.valueOf(paymentOrderBean.getDepartmentBean().getId()),
                                                        "POG_SECTION");
            sectionBean.setCode(lookupBean.getCode());
            sectionBean.setNameAr(lookupBean.getNameAr());
            sectionBean.setNameEn(lookupBean.getNameEn());
            sectionBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("SECTION_CODE_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }

    public void handleAutoFillCurrency(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
            CurrencyBean currencyBean = getPaymentOrderBean().getCurrencyBean();
            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilledCur(currencyBean.getCode(), null, "POG_CURRENCIES");
            currencyBean.setCode(lookupBean.getCode());
            currencyBean.setNameAr(lookupBean.getNameAr());
            currencyBean.setNameEn(lookupBean.getNameEn());
            currencyBean.setCurId(lookupBean.getCurCode());
            if (currencyBean.getCode() != null && paymentOrderBean.getAmount() != null &&
                paymentOrderBean.getAmount() > 0) {
                if (currencyBean.getNameEn().contains("SAR")) {
                    paymentOrderBean.setAmount(round(paymentOrderBean.getAmount(),
                                                     currencyBusiness.getPrecision("SAR")));
                } else {
                    paymentOrderBean.setAmount(round(paymentOrderBean.getAmount(),
                                                     currencyBusiness.getPrecision(paymentOrderBean.getCurrencyBean()
                                                                                                                                 .getNameEn()
                                                                                                                                 .substring(paymentOrderBean.getCurrencyBean()
                                                                                                                                                            .getNameEn()
                                                                                                                                                            .indexOf(',') +
                                                                                                                                            1, paymentOrderBean.getCurrencyBean()
                                                                                                                                                               .getNameEn()
                                                                                                                                                               .lastIndexOf(','))
                                                                                                   .trim())));
                }

                System.out.println(paymentOrderBean.getAmount());
            }

        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("CURRENCY_CODE_FIELD");
        reloadComponent("CURRENCY_ID_FIELD");
        reloadComponent("CURRENCY_DESC_FIELD");
        reloadComponent("AMOUNT_FIELD");
    }


    private EcomomicClassificationBean fillCalssifcation(String classId) {

        EcomomicClassificationBean ecomomicClassificationBean = new EcomomicClassificationBean();
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
            LookupBean lookupBean = lookupBusiness.getbyClassFicationId(classId);
            ecomomicClassificationBean.setCode(lookupBean.getCode());
            ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
            ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
            ecomomicClassificationBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        return ecomomicClassificationBean;
    }


    /*
     *
     * add lookup in table AutoFill
     * */
    public void handleAutoFillProject(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        runJS();
        try {

            Double index = (Double) ce.getParameters().get("index");
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                getDistributionsPoBean().get(index.intValue());
            ProjectBean projectBean = distributionsPaymentOrderBean.getProjectBean();
            EcomomicClassificationBean ecomomicClassificationBean =
                distributionsPaymentOrderBean.getEcomomicClassificationBean();
            if (distributionsPaymentOrderBean.getProjectClassCode() == null ||
                distributionsPaymentOrderBean.getProjectClassCode().isEmpty()) {

                distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
                distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
                reloadComponent("dateTableId");
                reloadComponent("dateTableId1");

                runJS();
                return;
            }
            LookupBean lookupBean =
                lookupBusiness.getLookupProjectUnionClass(distributionsPaymentOrderBean.getProjectClassCode(),
                                                          paymentOrderBean.getEntityBean().getCode(),
                                                          paymentOrderBean.getSectionBean().getCode() + "",
                                                          paymentOrderBean.getDepartmentBean().getCode() + "");

            if (lookupBean.getClassificationsId() == null && lookupBean.getCode() == null) {
                distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
                distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
                distributionsPaymentOrderBean.setProjectClassCode(null);
                reloadComponent("dateTableId");
                reloadComponent("dateTableId1");
                runFocusJSDist(ce.getComponent().getClientId() + "::content");
                runJS();
                return;
            }
            if (lookupBean.getClassificationsId() != null && Integer.parseInt(lookupBean.getClassificationsId()) > 0) {
                distributionsPaymentOrderBean.setEcomomicClassificationBean(fillCalssifcation(lookupBean.getClassificationsId()));

                projectBean.setCode(lookupBean.getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean.getNameAr());
                projectBean.setNameAr(lookupBean.getNameAr());
                projectBean.setNameEn(lookupBean.getNameEn());
                projectBean.setId(lookupBean.getId());
            } else {
                projectBean.setCode(null);
                ecomomicClassificationBean.setCode(lookupBean.getCode());
                ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
                ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
                System.out.println(lookupBean.getClassificationsId());
                if (lookupBean.getClassificationsId() != null) {
                    ecomomicClassificationBean.setId(Long.parseLong(lookupBean.getClassificationsId()));
                }

            }


        } catch (GTechException e) {
            runJS();
        }
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
        runJS();

    }

    public void handleAutoFillProjectHasmyat(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        runJS();
        try {

            Double index = (Double) ce.getParameters().get("index");
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                getDistributionsPoBeanHasmyat().get(index.intValue());
            ProjectBean projectBean = distributionsPaymentOrderBean.getProjectBean();
            EcomomicClassificationBean ecomomicClassificationBean =
                distributionsPaymentOrderBean.getEcomomicClassificationBean();
            if (distributionsPaymentOrderBean.getProjectClassCode() == null ||
                distributionsPaymentOrderBean.getProjectClassCode().isEmpty()) {

                distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
                distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
                reloadComponent("dateTableId");
                reloadComponent("dateTableId1");
                runJS();
                return;
            }
            LookupBean lookupBean =
                lookupBusiness.getLookupProjectUnionClass(distributionsPaymentOrderBean.getProjectClassCode(),
                                                          paymentOrderBean.getEntityBean().getCode(),
                                                          paymentOrderBean.getSectionBean().getCode() + "",
                                                          paymentOrderBean.getDepartmentBean().getCode() + "");
            if (lookupBean.getClassificationsId() != null && Integer.parseInt(lookupBean.getClassificationsId()) > 0) {
                distributionsPaymentOrderBean.setEcomomicClassificationBean(fillCalssifcation(lookupBean.getClassificationsId()));

                projectBean.setCode(lookupBean.getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean.getNameAr());
                projectBean.setNameAr(lookupBean.getNameAr());
                projectBean.setNameEn(lookupBean.getNameEn());
                projectBean.setId(lookupBean.getId());
            } else {
                projectBean.setCode(null);
                ecomomicClassificationBean.setCode(lookupBean.getCode());
                ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
                ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
                ecomomicClassificationBean.setId(Long.parseLong(lookupBean.getClassificationsId()));
            }
        } catch (GTechException e) {
        }
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
        runJS();

    }
    /*
     *
     * add lookup in table open dialog
     * */
    public void handleProject(ClientEvent ce) {
        Double index = (Double) ce.getParameters().get("index");
        String filedId = (String) ce.getParameters().get("filedId");
        String filedDesc = (String) ce.getParameters().get("filedDesc");
        String filedCode = (String) ce.getParameters().get("filedCode");
        try {
            LookupBusiness lookupBusiness = new LookupBusiness();
            LookupBean lookupBean =
                lookupBusiness.getLookupProjectValueAutoFilled(filedCode, paymentOrderBean.getEntityBean().getCode(),
                                                               paymentOrderBean.getSectionBean().getId() + "",
                                                               paymentOrderBean.getDepartmentBean().getId() + "");

            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                getDistributionsPoBean().get(index.intValue());
            ProjectBean projectBean = distributionsPaymentOrderBean.getProjectBean();
            if (lookupBean.getClassificationsId() != null) {
                distributionsPaymentOrderBean.setEcomomicClassificationBean(fillCalssifcation(lookupBean.getClassificationsId()));

            } else {
                distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());

            }
            projectBean.setCode(filedCode.toString());
            projectBean.setNameAr(filedDesc);
        } catch (GTechException e) {
        }
        runJS();
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
    }


    public void handleAutoFillClassification(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        runJS();
        try {

            Double index = (Double) ce.getParameters().get("index");
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                getDistributionsPoBean().get(index.intValue());
            EcomomicClassificationBean ecomomicClassificationBean =
                distributionsPaymentOrderBean.getEcomomicClassificationBean();

            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(ecomomicClassificationBean.getCode(), null,
                                                        "POG_ECOMOMIC_CLASSIFICATIONS");
            ecomomicClassificationBean.setCode(lookupBean.getCode());
            ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
            ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
            ecomomicClassificationBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");

    }

    public void handleAutoFillClassificationHasmyat(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        runJS();
        try {

            Double index = (Double) ce.getParameters().get("index");
            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
                getDistributionsPoBeanHasmyat().get(index.intValue());
            EcomomicClassificationBean ecomomicClassificationBean =
                distributionsPaymentOrderBean.getEcomomicClassificationBean();

            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(ecomomicClassificationBean.getCode(), null,
                                                        "POG_ECOMOMIC_CLASSIFICATIONS");
            ecomomicClassificationBean.setCode(lookupBean.getCode());
            ecomomicClassificationBean.setNameAr(lookupBean.getNameAr());
            ecomomicClassificationBean.setNameEn(lookupBean.getNameEn());
            ecomomicClassificationBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");

    }
    /*
     *
     * add lookup in table open dialog
     * */
    public void handleClassification(ClientEvent ce) {
        Double index = (Double) ce.getParameters().get("index");
        String filedId = (String) ce.getParameters().get("filedId");
        String filedDesc = (String) ce.getParameters().get("filedDesc");
        String filedCode = (String) ce.getParameters().get("filedCode");
        runJS();
        DistributionsPaymentOrderBean distributionsPaymentOrderBean = getDistributionsPoBean().get(index.intValue());
        EcomomicClassificationBean ecomomicClassificationBean =
            distributionsPaymentOrderBean.getEcomomicClassificationBean();
        ecomomicClassificationBean.setCode(filedCode.toString());
        ecomomicClassificationBean.setNameAr(filedDesc);

    }

    public void handlex(ValueChangeEvent event) {


    }

    public void paymentMethodOnChangeListener(ValueChangeEvent event) throws GTechException {
        Integer paymentMethodId = (Integer) event.getNewValue();
        ResponseContainer responseContainer = new ResponseContainer();
        if (paymentMethodId.intValue() <= 0 || paymentMethodId.intValue() >= 4) {
            responseContainer.addErrorMessage("VALID_PAYMENT_METHOD");
            createMessages(responseContainer);
            runJS();
            return;
        }
        Long paymentMethod = paymentMethodId == 1 ? 100101L : (paymentMethodId == 2 ? 100102L : 100103L);

        paymentOrderBean.setPaymentMethodMof(paymentMethodId.intValue());
        paymentOrderBean.setPaymentMethod(paymentMethod);
        if (paymentMethod.intValue() == 100102) {
            CurrencyBean currencyBean = new GeneralInfoBusiness().getSaudiRialCurrency();
            paymentOrderBean.setCurrencyBean(currencyBean);
            paymentOrderBean.getNationalityBean().setNationalityID("1");
            paymentOrderBean.getNationalityBean().setNationalityCode("SA");
        } else {
            paymentOrderBean.setIban("");
            paymentOrderBean.setBankBean(new BankBean());
        }
        reloadComponent("IBAN_NUMBER");
        reloadComponent("BANK_CODE_LIST");

        reloadComponent("NATIONALITY_LIST");
        reloadComponent("PO_BENEFICARY_NAME");
        reloadComponent("BEN_NUMBER");
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent component = facesContext.getViewRoot().findComponent("CURRENCY_CODE_FIELD");
        RichInputText richInputText = (RichInputText) component;
        richInputText.clearCachedClientIds();
        richInputText.clearInitialState();
        richInputText.resetValue();
        richInputText.setValue(paymentOrderBean.getCurrencyBean().getCode());
        adfFacesContext.addPartialTarget(richInputText);

        reloadComponent("CURRENCY_CODE_BUTTON");
        reloadComponent("CURRENCY_CODE_FIELD");
        reloadComponent("CURRENCY_ID_FIELD");
        reloadComponent("CURRENCY_DESC_FIELD");
        runJS();
    }

    public void handleServerEvent(ClientEvent ce) throws GTechException {
        String poNumber = (String) ce.getParameters().get("poNumber");
        ResponseContainer responseContainer = new ResponseContainer();
        if (poNumber.length() == 8) {
            String ePattern = "[0-9]*";
            java.util.regex.Pattern p = java.util
                                            .regex
                                            .Pattern
                                            .compile(ePattern);
            java.util.regex.Matcher m = p.matcher(poNumber);
            if (!m.matches()) {
                FacesMessage msg = new FacesMessage(MessageReader.getMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage("PO_NUMBER_FIELD", msg);
                runJS();
                return;
            }
        } else {
            FacesMessage msg = new FacesMessage(MessageReader.getMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("PO_NUMBER_FIELD", msg);
            paymentOrderBean.setPaymentOrderNumber(null);
            paymentOrderBean.setEntityBean(new EntityBean());
            paymentOrderBean.setDepartmentBean(new DepartmentBean());
            paymentOrderBean.setSectionBean(new SectionBean());
            reloadComponent("paymentOrderFrom");
            runJS();
            return;
        }
        runJS();
        Date isRecived = paymentOrderBusiness.isPORecived(poNumber);
        LocalDate isRecivedLocal = null;

        if (isRecived != null) {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            Instant instant = isRecived.toInstant();
            isRecivedLocal = instant.atZone(defaultZoneId).toLocalDate();
        }
        System.out.println(isRecivedLocal);
        System.out.println(LocalDate.now());
        if (!Arrays.asList(MOF_SPECIAL_USERS_IS_RECIVED).contains(getLogedInUser().getUserName())) {
            if (!Arrays.asList(MOF_SPECIAL_USERS).contains(getLogedInUser().getUserName())) {
                LocalDate today = LocalDate.now();
                if (isRecivedLocal == null || isRecivedLocal.equals(today)) {
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("PO_NUMBER_NOT_RECIVED"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_NUMBER_FIELD", msg);
                    paymentOrderBean.setPaymentOrderNumber(null);
                    paymentOrderBean.setEntityBean(new EntityBean());
                    paymentOrderBean.setDepartmentBean(new DepartmentBean());
                    paymentOrderBean.setSectionBean(new SectionBean());
                    reloadComponent("paymentOrderFrom");
                    runJS();
                    return;
                }
            } else {
                if (isRecivedLocal == null) {
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("PO_NUMBER_NOT_RECIVED"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_NUMBER_FIELD", msg);
                    paymentOrderBean.setPaymentOrderNumber(null);
                    paymentOrderBean.setEntityBean(new EntityBean());
                    paymentOrderBean.setDepartmentBean(new DepartmentBean());
                    paymentOrderBean.setSectionBean(new SectionBean());
                    reloadComponent("paymentOrderFrom");
                    runJS();
                    return;
                }
            }
        } else {
            if (isRecivedLocal == null) {
                FacesMessage msg = new FacesMessage(MessageReader.getMessage("PO_NUMBER_NOT_RECIVED"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage("PO_NUMBER_FIELD", msg);
                paymentOrderBean.setPaymentOrderNumber(null);
                paymentOrderBean.setEntityBean(new EntityBean());
                paymentOrderBean.setDepartmentBean(new DepartmentBean());
                paymentOrderBean.setSectionBean(new SectionBean());
                reloadComponent("paymentOrderFrom");
                runJS();
                return;
            }
        }

        String agencyCode = paymentOrderBusiness.getAgencyCodeByNumber(poNumber);
        if (agencyCode != null) {
            try {
                EntityBusiness entityBusiness = new EntityBusiness();
                paymentOrderBean.setEntityBean(entityBusiness.getEntity(agencyCode.substring(0, 3)));

                DepartmentBusiness departmentBusiness = new DepartmentBusiness();
                paymentOrderBean.setDepartmentBean(departmentBusiness.getDepartment(agencyCode.substring(3, 6),
                                                                                    paymentOrderBean.getEntityBean()
                                                                                    .getCode()));

                SectionBusiness sectionBusiness = new SectionBusiness();
                paymentOrderBean.setSectionBean(sectionBusiness.getSection(agencyCode.substring(6, 9),
                                                                           paymentOrderBean.getDepartmentBean()
                                                                           .getCode(),
                                                                           paymentOrderBean.getEntityBean().getCode()));


            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        //        PaymentOrderBean savedPo = paymentOrderBusiness.getFullDataPaymentOrderByPONumberWithoutId(poNumber, getLogedInUser().getEecID());
        //
        //        if (savedPo != null) {
        //            setPaymentOrderBean(savedPo);
        //        }
        runJS();
        reloadComponent("paymentOrderFrom");
    }


    public String getFullDataPaymentOrderByPOID() throws GTechException {
        setPaymentOrderBean(null);
        PaymentOrderBean savedPo = paymentOrderBusiness.getFullDataPaymentOrderByPOID(getPoID());
        if (getLogedInUser().getOrganizationBean() != null && getLogedInUser().getOrganizationBean() != null &&
            getLogedInUser().getOrganizationBean().getOrgCode() != null) {
            savedPo.setOrgId(getLogedInUser().getOrganizationBean().getId());
            savedPo.setOrg(getLogedInUser().getOrganizationBean().getOrgCode());
        }
        long paymentMethod = savedPo.getPaymentMethod();
        if (paymentMethod == 100101L) {
            savedPo.setPaymentMethodMof(1);
        } else if (paymentMethod == 100102L) {
            savedPo.setPaymentMethodMof(1);
        } else {
            savedPo.setPaymentMethodMof(3);
        }
        getDistributionsPoBean().clear();
        getDistributionsPoBeanHasmyat().clear();
        for (DistributionsPaymentOrderBean dsBean : savedPo.getDistributionsPoBeanList()) {
            if (dsBean.getProjectBean().getCode() == null) {
                dsBean.setProjectClassCode(dsBean.getEcomomicClassificationBean().getCode());
            }
            if (dsBean.getAmount() > 0) {
                getDistributionsPoBean().add(dsBean);
            } else {
                getDistributionsPoBeanHasmyat().add(dsBean);
            }
        }

        if (savedPo != null) {
            setPaymentOrderBean(savedPo);
        }
        runJS();
        return "PO-F01";
    }

    public String goToRefundPaymentOrderNavigation() throws GTechException {
        setRefundPaymentOrderNavigation(false);
        runJS();
        return "PO-F26";
    }


    public void validateBeneficiaryNumber(ValueChangeEvent event) throws GTechException {
        String beneficiaryCode = (String) event.getNewValue();
        ResponseContainer container = paymentOrderBusiness.validateBeneficiaryNumber(beneficiaryCode);


        if (!container.containsErrorMessages()) {
            BeneficiaryBean beneficiaryBean = (BeneficiaryBean) container.getDataBean();
            paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
            paymentOrderBean.setBeneficaryName(beneficiaryBean.getNameAr());
        } else {
            paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
            paymentOrderBean.setBeneficaryName("");
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            FacesMessage msg = new FacesMessage(MessageReader.getMessage("BENEFICIARY_CODE_NOT_EXIST"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("BEN_NUMBER", msg);
            runJS();
            runFocusJSBEN_NUMBER();
            reloadComponent("paymentOrderFrom");
            return;
        }

        if (beneficiaryCode.equals("999")) {
            paymentOrderBean.setBeneficaryName("");
            paymentOrderBean.setPaymentMethodMof(1);
            paymentOrderBean.setPaymentMethod(100101L);
            paymentOrderBean.setIban("");
        } else {
            paymentOrderBean.setPaymentMethodMof(2);
            paymentOrderBean.setPaymentMethod(100102L);
        }

        reloadComponent("REC_NAME_GROUP");
        reloadComponent("PO_BENEFICARY_NAME");
        reloadComponent("PAYMENT_METHOD_LIST");
        runJS();
    }


    public void validateRefNumber(ValueChangeEvent event) throws GTechException {
        String refNumber = (String) event.getNewValue();
        String container = paymentOrderBusiness.validateRefNumber(refNumber);
        String validateVendorWs = paymentOrderBusiness.validateVendorWs(refNumber);

        System.out.println(validateVendorWs);

        if (container == null) {
            System.out.println("NULL");
        } else {
            paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
            paymentOrderBean.setBeneficaryName("");
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("PO_REF_LABEL_VALUE");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            FacesMessage msg = new FacesMessage(container);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("PO_REF_LABEL_VALUE", msg);

            runJS();
            runFocusJSPO_REF_LABEL_VALUE();
            reloadComponent("paymentOrderFrom");
            return;
        }

        if (validateVendorWs == null) {
            System.out.println("NULL");
        } else {
            paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
            paymentOrderBean.setBeneficaryName("");
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("PO_REF_LABEL_VALUE");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);

            FacesMessage msg = new FacesMessage(validateVendorWs);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("PO_REF_LABEL_VALUE", msg);

            runJS();
            runFocusJSPO_REF_LABEL_VALUE();
            reloadComponent("paymentOrderFrom");
            return;
        }


        reloadComponent("REC_NAME_GROUP");
        reloadComponent("PO_BENEFICARY_NAME");
        reloadComponent("PAYMENT_METHOD_LIST");
        runJS();
    }

    public void validateLocation(ValueChangeEvent event) throws GTechException {
        String locNumber = (String) event.getNewValue();
        String container = paymentOrderBusiness.validateLocation(locNumber);

        System.out.println(container);
        if (container == null) {
            System.out.println("NULL");
        } else {
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("PO_LOC_LABEL_VALUE");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            FacesMessage msg = new FacesMessage(container);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("PO_LOC_LABEL_VALUE", msg);

            runJS();
            runFocusJSPO_LOC_LABEL_VALUE();
            reloadComponent("paymentOrderFrom");
            return;
        }
        runJS();
    }

    public void validateSonCofogCode(ValueChangeEvent event) throws GTechException {
        String validateSonCofogCode = (String) event.getNewValue();
        String container = paymentOrderBusiness.validateSonCofogCode(validateSonCofogCode);
        UIComponent c = event.getComponent();
        System.out.println(c.getClientId());
        System.out.println(container);
        if (container == null) {
            System.out.println("NULL");
        } else {
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = c;
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            FacesMessage msg = new FacesMessage(container);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(c.getClientId(), msg);

            runJS();
            runFocusGFSO(c.getClientId());
            reloadComponent("paymentOrderFrom");
            return;
        }
        runJS();
    }

    public void runJS() {
        callJavaScriptFunction("$('input').keydown( function(e) {\n" + "                                  \n" +
                               "                                    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;\n" +
                               "                                    if(key == 13) {\n" +
                               "                                    \n" +
                               "                                        e.preventDefault();\n" +
                               "                                       var filedVal = this.value;\n" +
                               "                                    if(!filedVal) {\n" +
                               "                                       \n" +
                               "                                        \n" +
                               "                                        if(this.id.indexOf('ot3') >= 0 ||\n" +
                               "                                            this.id.indexOf('PROJECT_CODE_FIELD') >= 0 ||\n" +
                               "                                            this.id.indexOf('removeDist') >= 0) {\n" +
                               "                                            document.getElementById('saveButton').focus();\n" +
                               "                                            return;\n" +
                               "                                            }else {\n" +
                               "                                            return;\n" +
                               "                                            }\n" +
                               "                                    }\n" +
                               "                                    var amountField =AdfPage.PAGE.findComponent(\"PAYMENT_METHOD_LIST\");\n" +
                               "                                    var benField =AdfPage.PAGE.findComponent(\"BEN_NUMBER\");\n" +
                               "                                    var benNumber = document.getElementById('BEN_NUMBER::content').value + '';\n" +
                               "                                    \n" +
                               "                                    if(this.id == 'BEN_NUMBER::content' && (this.value.startsWith('1') || this.value.startsWith('2')) ) {\n" +
                               "                                            \n" +
                               "                                            var inputs = $(this).closest('form').find(':input:enabled:visible').not(':button');\n" +
                               "                                            inputs.eq( inputs.index(this)+ 2 ).focus().delay(300);\n" +
                               "                                            inputs.eq( inputs.index(this)+ 2 ).select().delay(300);\n" +
                               "                                    } else if ((benNumber.startsWith('1') || benNumber.startsWith('2')) && this.id == 'IBAN_NUMBER::content' ) {\n" +
                               "                                            var inputs = $(this).closest('form').find(':input:enabled:visible').not(':button');\n" +
                               "                                            \n" +
                               "                                            inputs.eq( inputs.index(this) +2).focus().delay(300);\n" +
                               "                                            inputs.eq( inputs.index(this) +2).select().delay(300);\n" +
                               "                                    } else if (benField.getValue() && (benNumber = '999') && this.id == 'PO_BENEFICARY_NAME::content') {\n" +
                               "                                            var inputs = $(this).closest('form').find(':input:enabled:visible').not(':button');\n" +
                               "                                            inputs.eq( inputs.index(this)+ 1 ).focus().delay(300);\n" +
                               "                                            inputs.eq( inputs.index(this)+ 1 ).select().delay(300);\n" +
                               "                                    \n" +
                               "                                    } else if (this.id == 'EXPENSE_CODE_FIELD::content') {\n" +
                               "                                              var inputs = $(this).closest('form').find(':input:enabled:visible').not(':button');\n" +
                               "                                            inputs.eq( inputs.index(this)+ 2 ).focus().delay(300);\n" +
                               "                                            inputs.eq( inputs.index(this)+ 2 ).select().delay(300);\n" +
                               "                                    \n" +
                               "                                    } else if($('[id*=\"ot3::content\"]')[$('[id*=\"ot3::content\"]').length -1].id == this.id) {\n" +
                               "                                        document.getElementById('saveButton').focus();\n" +
                               "                                    }else if($('[id*=\"ot31::content\"]')[$('[id*=\"ot31::content\"]').length -1].id == this.id) {\n" +
                               "                                        document.getElementById('saveButton').focus();\n" +
                               "                                    }else  {\n" +
                               "                                            var inputs = $(this).closest('form').find(':input:enabled:visible').not(':button');\n" +
                               "                                            inputs.eq( inputs.index(this)+ 1 ).focus().delay(300);\n" +
                               "                                            inputs.eq( inputs.index(this)+ 1 ).select().delay(300);\n" +
                               "\n" + "                                        }\n" +
                               "                                    } else if(key == 9) {\n" +
                               "                                        e.preventDefault();if(this.id.indexOf('ot3') >= 0 ||\n" +
                               "                                            this.id.indexOf('PROJECT_CODE_FIELD') >= 0 ||\n" +
                               "                                            this.id.indexOf('removeDist') >= 0) {\n" +
                               "                                            var inputs = 'mainRe1:0:PROJECT_CODE_FIELD1::content';\n" +
                               "                                            document.getElementById(inputs).focus();\n" +
                               "                                        }\n" +
                               "                                    }\n" + "                                });");
    }

    public void runFocusJS() {
        callJavaScriptFunction("AdfPage.PAGE.findComponent(\"IBAN_NUMBER\").focus();");
    }

    public void runFocusJSBEN_NUMBER() {
        callJavaScriptFunction("AdfPage.PAGE.findComponent(\"BEN_NUMBER\").focus();");
    }

    public void runFocusJSPO_REF_LABEL_VALUE() {
        callJavaScriptFunction("AdfPage.PAGE.findComponent(\"PO_REF_LABEL_VALUE\").focus();");
    }

    public void runFocusJSPO_LOC_LABEL_VALUE() {
        callJavaScriptFunction("AdfPage.PAGE.findComponent(\"PO_LOC_LABEL_VALUE\").focus();");
    }

    public void runFocusGFSO(String id) {
        callJavaScriptFunction("AdfPage.PAGE.findComponent(\"" + id + "\").focus();");
    }

    public void runFocusJSPO_DATE_FIELDH() {
        callJavaScriptFunction("AdfPage.PAGE.findComponent(\"PO_DATE_FIELDH\").focus();");
    }

    public void runFocusJSDist(String Id) {
        System.out.println(Id);
        callJavaScriptFunction("document.getElementById('" + Id + "').focus();");
    }

    public void runFocusFisrtEmptyHasmyat() {
        callJavaScriptFunction("getFirstEmptyFieldHasmyat();");
    }

    public void runFocusFisrtEmpty() {
        callJavaScriptFunction("getFirstEmptyField();");
    }

    public void validateIbanNumber(ValueChangeEvent event) throws GTechException {
        String iban = (String) event.getNewValue();
        ResponseContainer container =
            paymentOrderBusiness.validateIbanNumber(iban, paymentOrderBean.getBeneficiaryBean().getCode());
        if (container.containsErrorMessages()) {
            paymentOrderBean.setIban("");
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("IBAN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            reloadComponent("IBAN_NUMBER");
            reloadComponent("BANK_CODE_LIST");
            runJS();
            runFocusJS();
            FacesMessage msg = new FacesMessage(MessageReader.getMessage("IBAN_NOT_EXIST"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("IBAN_NUMBER", msg);
            //createMessages(container);
            return;
        } else {
            runJS();
        }

        BankBean bankBean = new BankBusiness().getBank(iban);
        if (bankBean != null) {
            paymentOrderBean.setBankBean(bankBean);
            reloadComponent("BANK_CODE_LIST");

        }
        runJS();

    }

    public void addNewDistripution(ActionEvent event) throws GTechException {
        boolean isOk = true;

        //        List<DistributionsPaymentOrderBean> distributionsPaymentOrderBeanList =
        //            getDistributionsPoBean();
        //        if (distributionsPaymentOrderBeanList != null && !distributionsPaymentOrderBeanList.isEmpty()) {
        //            int lastIndex = distributionsPaymentOrderBeanList.size() - 1;
        //            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
        //                distributionsPaymentOrderBeanList.get(lastIndex);
        //            ResponseContainer responseContainer = new ResponseContainer();
        //            Double amount = distributionsPaymentOrderBean.getAmount();
        //            if (amount == null || amount.equals(0.0)) {
        //                isOk = false;
        //                responseContainer.addErrorMessage("AMOUNT_SHOULD_NOT_EQUALS_ZERO");
        //            }
        //            if (!isOk) {
        //                createMessages(responseContainer);
        //                return;
        //            }
        //            if (!paymentOrderBusiness.validatePaymentDistribution(distributionsPaymentOrderBean)) {
        //                return;
        //            }
        //        }
        for (int i = 0; i < 5; i++) {
            DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
            distributionsPaymentOrderBean.setId(distSeq);
            distSeq--;
            distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
            distributionsPaymentOrderBean.setRefundAmount(0.0);
            distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            getDistributionsPoBean().add(distributionsPaymentOrderBean);
        }
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
        runJS();
        runFocusFisrtEmpty();
    }

    public void addNewDistriputionHasmyat(ActionEvent event) throws GTechException {
        boolean isOk = true;

        //        List<DistributionsPaymentOrderBean> distributionsPaymentOrderBeanList =
        //            getDistributionsPoBeanHasmyat();
        //        if (distributionsPaymentOrderBeanList != null && !distributionsPaymentOrderBeanList.isEmpty()) {
        //            int lastIndex = distributionsPaymentOrderBeanList.size() - 1;
        //            DistributionsPaymentOrderBean distributionsPaymentOrderBean =
        //                distributionsPaymentOrderBeanList.get(lastIndex);
        //            ResponseContainer responseContainer = new ResponseContainer();
        //            Double amount = distributionsPaymentOrderBean.getAmount();
        //            if (amount == null || amount.equals(0.0)) {
        //                isOk = false;
        //                responseContainer.addErrorMessage("AMOUNT_SHOULD_NOT_EQUALS_ZERO");
        //            }
        //            if (!isOk) {
        //                createMessages(responseContainer);
        //                return;
        //            }
        //            if (!paymentOrderBusiness.validatePaymentDistribution(distributionsPaymentOrderBean)) {
        //                return;
        //            }
        //        }
        for (int i = 0; i < 5; i++) {
            DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
            distributionsPaymentOrderBean.setId(distSeq);
            distSeq--;
            distributionsPaymentOrderBean.setProjectBean(new ProjectBean());
            distributionsPaymentOrderBean.setRefundAmount(0.0);
            distributionsPaymentOrderBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            getDistributionsPoBeanHasmyat().add(distributionsPaymentOrderBean);
        }
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
        runJS();
        runFocusFisrtEmptyHasmyat();
    }

    public void deleteDistriputionLine(ActionEvent event) throws GTechException {
        DistributionsPaymentOrderBean distributionsPaymentOrderBean =
            (DistributionsPaymentOrderBean) event.getComponent()
                                                                                                           .getAttributes()
                                                                                                           .get("distribution");
        runJS();
        getDistributionsPoBean().remove(distributionsPaymentOrderBean);
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
    }

    public void deleteDistriputionLineHasmyat(ActionEvent event) throws GTechException {
        DistributionsPaymentOrderBean distributionsPaymentOrderBean =
            (DistributionsPaymentOrderBean) event.getComponent()
                                                                                                           .getAttributes()
                                                                                                           .get("distribution");
        runJS();
        getDistributionsPoBeanHasmyat().remove(distributionsPaymentOrderBean);
        reloadComponent("dateTableId");
        reloadComponent("dateTableId1");
    }

    public void savePaymentOrder(ActionEvent event) throws GTechException {

        setSaved(false);
        ResponseContainer responseContainer = new ResponseContainer();
        if (paymentOrderBean == null) {
            return;
        }
        System.out.println(paymentOrderBean.getRefNumber());
        paymentOrderBean.setUserId(getLogedInUser().getUserID() != null ?
                                   Integer.parseInt(getLogedInUser().getUserID()) : 0);
        System.out.println(paymentOrderBean.getCurrencyBean().getCurId());
        paymentOrderBean.setCurrencyBean(currencyBusiness.getCurrency(paymentOrderBean.getCurrencyBean().getCode()));
        System.out.println(paymentOrderBean.getCurrencyBean().getCurId());
        if (paymentOrderBean.getCurrencyBean() == null || paymentOrderBean.getCurrencyBean().getCurId() == null ||
            paymentOrderBean.getCurrencyBean()
                                                                                                                                   .getCurId()
                                                                                                                                   .isEmpty()) {
            responseContainer.addErrorMessage("MANDETORY_CUR");
            createMessages(responseContainer);
            return;
        }
        if (paymentOrderBean.getDepartmentBean() == null || paymentOrderBean.getDepartmentBean().getId() == 0) {
            responseContainer.addErrorMessage("MANDETORY_DEP");
            createMessages(responseContainer);
            return;
        }
        if (paymentOrderBean.getSectionBean() == null || paymentOrderBean.getSectionBean().getId() == 0) {
            responseContainer.addErrorMessage("MANDETORY_SEC");
            createMessages(responseContainer);
            return;
        }

        String poNumber = paymentOrderBean.getPaymentOrderNumber();
        if (poNumber.length() == 8) {
            String ePattern = "[0-9]*";
            java.util.regex.Pattern p = java.util
                                            .regex
                                            .Pattern
                                            .compile(ePattern);
            java.util.regex.Matcher m = p.matcher(poNumber);
            if (!m.matches()) {
                responseContainer.addErrorMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8");
                createMessages(responseContainer);
                return;
            }
        } else {
            responseContainer.addErrorMessage("PAYMENT_ORDER_NUMBER_SHOULD_BE_8");
            createMessages(responseContainer);
            return;
        }
        //        String checkFun = paymentOrderBusiness.isHaveCheckFunds(poNumber);
        //        if(checkFun != null) {
        //            MessageUtilities.createInfoMessage(paymentOrderBean.getPaymentOrderNumber() + " : " + checkFun);
        //             return;
        //        }
        List<DistributionsPaymentOrderBean> addedDistributionsPoBeanList = new ArrayList<>();
        for (DistributionsPaymentOrderBean distributionsPaymentOrderBean : getDistributionsPoBean()) {


            if (distributionsPaymentOrderBean.getEcomomicClassificationBean().getCode() != null) {
                addedDistributionsPoBeanList.add(distributionsPaymentOrderBean);
            }
        }
        for (DistributionsPaymentOrderBean distributionsPaymentOrderBean : getDistributionsPoBeanHasmyat()) {


            if (distributionsPaymentOrderBean.getEcomomicClassificationBean().getCode() != null) {
                distributionsPaymentOrderBean.setAmount(distributionsPaymentOrderBean.getAmount() * -1);
                addedDistributionsPoBeanList.add(distributionsPaymentOrderBean);
            }
        }
        paymentOrderBean.setDistributionsPoBeanList(new ArrayList<>());
        paymentOrderBean.getDistributionsPoBeanList().addAll(addedDistributionsPoBeanList);
        paymentOrderBean.setPaymentEntryMethod(100603);
        responseContainer = paymentOrderBusiness.paymentOrderSaveBusiness(paymentOrderBean, getLogedInUser());
        addedDistributionsPoBeanList.clear();
        paymentOrderBean.getDistributionsPoBeanList().clear();
        if (responseContainer.containsErrorMessages()) {
            createMessages(responseContainer);


        }
        if (responseContainer.containsInfoMessages()) {
            //                ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
            //                actionLogBusiness.insereLogLine(paymentOrderBean.getId(),"",Long.parseLong(getLogedInUser().getUserID()),100505);
            setSaved(true);
            setSavePONumber(paymentOrderBean.getPaymentOrderNumber());
            doInitPropartis();
            setRefundPaymentOrderNavigation(false);
            reloadComponent("paymentOrderFrom");
        }
    }


    public String resetForm() throws GTechException {
        doInitPropartis();
        setRefundPaymentOrderNavigation(false);
        return null;
    }

    public void fillAmountInWord(ValueChangeEvent event) throws GTechException {
        double amount = (Double) event.getNewValue();
        int precision = 2;
        if (paymentOrderBean.getCurrencyBean().getNameEn() != null && paymentOrderBean.getCurrencyBean()
                                                                                      .getNameEn()
                                                                                      .contains("SAR")) {
            precision = currencyBusiness.getPrecision("SAR");
        } else {
            precision = currencyBusiness.getPrecision(paymentOrderBean.getCurrencyBean()
                                                                      .getNameEn()
                                                                      .substring(paymentOrderBean.getCurrencyBean()
                                                                                                 .getNameEn()
                                                                                                 .indexOf(',') + 1, paymentOrderBean.getCurrencyBean()
                                                                                                                                    .getNameEn()
                                                                                                                                    .lastIndexOf(','))
                                                                      .trim());
        }

        System.out.println(precision);
        //callTafkeet(String.valueOf(amount), paymentOrderBean.getCurrencyBean().getCode());
        paymentOrderBean.setAmount(round(amount, precision));
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent component = facesContext.getViewRoot().findComponent("AMOUNT_FIELD");
        RichInputText richInputText = (RichInputText) component;
        richInputText.clearCachedClientIds();
        richInputText.clearInitialState();
        richInputText.resetValue();
        richInputText.setValue(paymentOrderBean.getAmount());
        adfFacesContext.addPartialTarget(richInputText);
        runJS();
    }

    public void formatDate(ValueChangeEvent event) throws GTechException {
        String date = (String) event.getNewValue();

        if (date != null && date.length() == 8) {
            Date dateG = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date firstDateOfTheYear = null;
            try {
                firstDateOfTheYear = sdf.parse("31/12/2018");
            } catch (ParseException e) {
                e.printStackTrace();
            }


            Calendar cl = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            cl.setTime(dateG);
            c2.setTime(firstDateOfTheYear);
            try {
                HijrahDate islamyDate =
                    HijrahChronology.INSTANCE.date(LocalDate.of(cl.get(Calendar.YEAR), cl.get(Calendar.MONTH) + 1,
                                                                cl.get(Calendar.DATE)));
                HijrahDate islamyDateFirstYear =
                    HijrahChronology.INSTANCE.date(LocalDate.of(c2.get(Calendar.YEAR), c2.get(Calendar.MONTH) + 1,
                                                                c2.get(Calendar.DATE)));
                System.out.println(islamyDateFirstYear);

                HijrahDate islamyDatePO =
                    HijrahChronology.INSTANCE.date(Integer.parseInt(date.substring(4, date.length())),
                                                   Integer.parseInt(date.substring(2, 4)),
                                                   Integer.parseInt(date.substring(0, 2)));
                System.out.println(islamyDatePO);
                if (islamyDatePO.isAfter(islamyDate)) {
                    paymentOrderBean.setPaymentOrderDateH(null);
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_DATE"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_DATE_FIELDH", msg);
                    RichInputText richInputText = (RichInputText) event.getComponent();
                    richInputText.clearCachedClientIds();
                    richInputText.clearInitialState();
                    richInputText.resetValue();
                    reloadComponent("PO_DATE_FIELDH");
                    runJS();
                    runFocusJSPO_DATE_FIELDH();
                    return;
                }

                if (!Arrays.asList(MOF_SPECIAL_USERS).contains(getLogedInUser().getUserName())) {

                    if ((!islamyDatePO.isAfter(islamyDateFirstYear))) {
                        if (!islamyDatePO.isEqual(islamyDateFirstYear)) {
                            paymentOrderBean.setPaymentOrderDateH(null);
                            FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_YEAR"));
                            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                            FacesContext fctx = FacesContext.getCurrentInstance();
                            fctx.addMessage("PO_DATE_FIELDH", msg);
                            RichInputText richInputText = (RichInputText) event.getComponent();
                            richInputText.clearCachedClientIds();
                            richInputText.clearInitialState();
                            richInputText.resetValue();
                            reloadComponent("PO_DATE_FIELDH");
                            runJS();
                            runFocusJSPO_DATE_FIELDH();
                            return;
                        }
                    }
                }
            } catch (java.time.DateTimeException e) {
                paymentOrderBean.setPaymentOrderDateH(null);
                FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_DATE"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage("PO_DATE_FIELDH", msg);
                RichInputText richInputText = (RichInputText) event.getComponent();
                richInputText.clearCachedClientIds();
                richInputText.clearInitialState();
                richInputText.resetValue();
                reloadComponent("PO_DATE_FIELDH");
                runJS();
                runFocusJSPO_DATE_FIELDH();
                return;
            }

            if (!Arrays.asList(MOF_SPECIAL_USERS).contains(getLogedInUser().getUserName())) {

                if (Integer.parseInt(date.substring(4, date.length())) >= 1440 &&
                    Integer.parseInt(date.substring(4, date.length())) <= 1441) {
                    paymentOrderBean.setPaymentOrderDateH(date.substring(0, 2) + "/" + date.substring(2, 4) + "/" +
                                                          date.substring(4, date.length()));
                    runJS();
                } else {
                    paymentOrderBean.setPaymentOrderDateH(null);
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_YEAR"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_DATE_FIELDH", msg);
                    RichInputText richInputText = (RichInputText) event.getComponent();
                    richInputText.clearCachedClientIds();
                    richInputText.clearInitialState();
                    richInputText.resetValue();
                    reloadComponent("PO_DATE_FIELDH");
                    runJS();
                    runFocusJSPO_DATE_FIELDH();
                    return;
                }
            } else {
                if (Integer.parseInt(date.substring(4, date.length())) >= 1439 &&
                    Integer.parseInt(date.substring(4, date.length())) <= 1440) {
                    paymentOrderBean.setPaymentOrderDateH(date.substring(0, 2) + "/" + date.substring(2, 4) + "/" +
                                                          date.substring(4, date.length()));
                    runJS();
                } else {
                    paymentOrderBean.setPaymentOrderDateH(null);
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_YEAR"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_DATE_FIELDH", msg);
                    RichInputText richInputText = (RichInputText) event.getComponent();
                    richInputText.clearCachedClientIds();
                    richInputText.clearInitialState();
                    richInputText.resetValue();
                    reloadComponent("PO_DATE_FIELDH");
                    runJS();
                    runFocusJSPO_DATE_FIELDH();
                    return;
                }
            }


        } else if (date != null && date.length() == 10) {
            Date dateG = new Date();
            Calendar cl = Calendar.getInstance();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date firstDateOfTheYear = null;
            try {
                firstDateOfTheYear = sdf.parse("31/12/2018");
            } catch (ParseException e) {
                e.printStackTrace();
            }

            cl.setTime(dateG);
            HijrahDate islamyDate =
                HijrahChronology.INSTANCE.date(LocalDate.of(cl.get(Calendar.YEAR), cl.get(Calendar.MONTH) + 1,
                                                            cl.get(Calendar.DATE)));
            HijrahDate islamyDatePO =
                HijrahChronology.INSTANCE.date(Integer.parseInt(date.substring(6, date.length())),
                                               Integer.parseInt(date.substring(3, 5)),
                                               Integer.parseInt(date.substring(0, 2)));
            Calendar c2 = Calendar.getInstance();
            c2.setTime(firstDateOfTheYear);
            HijrahDate islamyDateFirstYear =
                HijrahChronology.INSTANCE.date(LocalDate.of(c2.get(Calendar.YEAR), c2.get(Calendar.MONTH) + 1,
                                                            c2.get(Calendar.DATE)));
            if (islamyDatePO.isAfter(islamyDate)) {
                paymentOrderBean.setPaymentOrderDateH(null);
                FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_DATE"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage("PO_DATE_FIELDH", msg);
                RichInputText richInputText = (RichInputText) event.getComponent();
                richInputText.clearCachedClientIds();
                richInputText.clearInitialState();
                richInputText.resetValue();
                reloadComponent("PO_DATE_FIELDH");
                runJS();
                runFocusJSPO_DATE_FIELDH();
                return;
            }

            if (!Arrays.asList(MOF_SPECIAL_USERS).contains(getLogedInUser().getUserName())) {

                if ((!islamyDatePO.isAfter(islamyDateFirstYear))) {
                    if (!islamyDatePO.isEqual(islamyDateFirstYear)) {
                        paymentOrderBean.setPaymentOrderDateH(null);
                        FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_YEAR"));
                        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                        FacesContext fctx = FacesContext.getCurrentInstance();
                        fctx.addMessage("PO_DATE_FIELDH", msg);
                        RichInputText richInputText = (RichInputText) event.getComponent();
                        richInputText.clearCachedClientIds();
                        richInputText.clearInitialState();
                        richInputText.resetValue();
                        reloadComponent("PO_DATE_FIELDH");
                        runJS();
                        runFocusJSPO_DATE_FIELDH();
                        return;
                    }
                }
            }
            if (!Arrays.asList(MOF_SPECIAL_USERS).contains(getLogedInUser().getUserName())) {
                if (Integer.parseInt(date.substring(6, date.length())) >= 1440 &&
                    Integer.parseInt(date.substring(6, date.length())) <= 1441) {
                    paymentOrderBean.setPaymentOrderDateH(date);
                    runJS();
                } else {
                    paymentOrderBean.setPaymentOrderDateH(null);
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_YEAR"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_DATE_FIELDH", msg);
                    RichInputText richInputText = (RichInputText) event.getComponent();
                    richInputText.clearCachedClientIds();
                    richInputText.clearInitialState();
                    richInputText.resetValue();
                    reloadComponent("PO_DATE_FIELDH");
                    runJS();
                    runFocusJSPO_DATE_FIELDH();
                    return;
                }
            } else {
                if (Integer.parseInt(date.substring(6, date.length())) >= 1439 &&
                    Integer.parseInt(date.substring(6, date.length())) <= 1440) {
                    paymentOrderBean.setPaymentOrderDateH(date);
                    runJS();
                } else {
                    paymentOrderBean.setPaymentOrderDateH(null);
                    FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI_YEAR"));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    fctx.addMessage("PO_DATE_FIELDH", msg);
                    RichInputText richInputText = (RichInputText) event.getComponent();
                    richInputText.clearCachedClientIds();
                    richInputText.clearInitialState();
                    richInputText.resetValue();
                    reloadComponent("PO_DATE_FIELDH");
                    runJS();
                    runFocusJSPO_DATE_FIELDH();
                    return;
                }
            }


        } else {
            paymentOrderBean.setPaymentOrderDateH(null);
            FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_HIJRI"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("PO_DATE_FIELDH", msg);
            RichInputText richInputText = (RichInputText) event.getComponent();
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            reloadComponent("PO_DATE_FIELDH");
            runJS();
            runFocusJSPO_DATE_FIELDH();
            return;
        }

        RichInputText richInputText = (RichInputText) event.getComponent();
        richInputText.clearCachedClientIds();
        richInputText.clearInitialState();
        richInputText.resetValue();
        richInputText.setValue(paymentOrderBean.getPaymentOrderDateH());
        // System.out.println(paymentOrderBean.getPaymentOrderDateH());
        reloadComponent("PO_DATE_FIELDH");
        runJS();
    }

    public void callTafkeet(String amount, String currency) throws GTechException {
        String textAmount = currencyBusiness.getTafkeetCurrency(amount, currency);
        paymentOrderBean.setAmountInWord(textAmount);
        reloadComponent("AMOUNT_TEXT_FIELD");
    }

    public void handleAutoFillExpense(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
            ExpenseBean expenseBean = getPaymentOrderBean().getExpenseBean();
            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(expenseBean.getCode(), null, "POG_EXPENSE_TYPES");

            if (lookupBean.getCode() == null || lookupBean.getCode().isEmpty()) {

                expenseBean = new ExpenseBean();
                AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                FacesContext facesContext = FacesContext.getCurrentInstance();
                UIComponent component = facesContext.getViewRoot().findComponent("EXPENSE_CODE_FIELD");
                RichInputText richInputText = (RichInputText) component;
                richInputText.clearCachedClientIds();
                richInputText.clearInitialState();
                richInputText.resetValue();
                richInputText.setValue("");
                adfFacesContext.addPartialTarget(richInputText);
                FacesMessage msg = new FacesMessage(MessageReader.getMessage("INVALID_EXPENSE"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage("EXPENSE_CODE_FIELD", msg);
            } else {
                expenseBean.setCode(lookupBean.getCode());
                expenseBean.setDescAr(lookupBean.getNameAr());
                expenseBean.setDescEn(lookupBean.getNameEn());
                expenseBean.setId(lookupBean.getId());
            }

        } catch (GTechException e) {
        }
        reloadComponent("EXPENSE_ID_FIELD");
        reloadComponent("EXPENSE_CODE_FIELD");
        reloadComponent("EXPENSE_DESC_FIELD");
        runJS();
    }

    public List getBenIbans(FacesContext facesContext, AutoSuggestUIHints autoSuggestUIHints) {

        List<SelectItem> siList = new ArrayList<>();
        if (Arrays.asList(MOF_SPECIAL_USERS_IBAN).contains(getLogedInUser().getUserName())) {

            if (paymentOrderBean.getBeneficiaryBean().getCode() != "999") {

                List<String> ibanList =
                    beneficiaryBusiness.getIbanByBenCode(paymentOrderBean.getBeneficiaryBean().getCode());

                for (String iban : ibanList) {
                    SelectItem si = new SelectItem();
                    si.setLabel(iban);
                    si.setValue(iban);
                    siList.add(si);
                }
            }
        }
        return siList;
    }
    
    public List getGFSSOList(FacesContext facesContext, AutoSuggestUIHints autoSuggestUIHints) {

        List<SelectItem> siList = new ArrayList<>();
        if (Arrays.asList(MOF_SPECIAL_USERS_IBAN).contains(getLogedInUser().getUserName())) {

           

            

                for (String[] iban : GFSSOArrayList) {
                    SelectItem si = new SelectItem();
                    si.setLabel(iban[1] + "-" + iban[0]);
                    si.setValue(iban[0]);
                    siList.add(si);
                }
            
        }
        return siList;
    }

    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setNationailtiesList(List<NationalityBean> nationailtiesList) {
        this.nationailtiesList = nationailtiesList;
    }

    public List<NationalityBean> getNationailtiesList() {
        return nationailtiesList;
    }

    public void setPaymentsMethodList(List<DomainBean> paymentsMethodList) {
        this.paymentsMethodList = paymentsMethodList;
    }

    public List<DomainBean> getPaymentsMethodList() {
        return paymentsMethodList;
    }

    public void setPoID(String poID) {
        this.poID = poID;
    }

    public String getPoID() {
        return poID;
    }

    public void setRefundPaymentOrderNavigation(boolean refundPaymentOrderNavigation) {
        this.refundPaymentOrderNavigation = refundPaymentOrderNavigation;
    }

    public boolean isRefundPaymentOrderNavigation() {
        return refundPaymentOrderNavigation;
    }

    public void setCheckSection(boolean checkSection) {
        this.checkSection = checkSection;
    }

    public boolean isCheckSection() {
        return checkSection;
    }


    public void setDistributionsPoBeanHasmyat(List<DistributionsPaymentOrderBean> distributionsPoBeanHasmyat) {
        this.distributionsPoBeanHasmyat = distributionsPoBeanHasmyat;
    }

    public List<DistributionsPaymentOrderBean> getDistributionsPoBeanHasmyat() {
        return distributionsPoBeanHasmyat;
    }

    public void setDistributionsPoBean(List<DistributionsPaymentOrderBean> distributionsPoBean) {
        this.distributionsPoBean = distributionsPoBean;
    }

    public List<DistributionsPaymentOrderBean> getDistributionsPoBean() {
        return distributionsPoBean;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSavePONumber(String savePONumber) {
        this.savePONumber = savePONumber;
    }

    public String getSavePONumber() {
        return savePONumber;
    }

    public double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
