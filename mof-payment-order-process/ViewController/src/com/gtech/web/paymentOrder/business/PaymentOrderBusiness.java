package com.gtech.web.paymentOrder.business;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.common.util.CollectionUtilities;
import com.gtech.faces.util.MessageReader;
import com.gtech.web.admin.user.UserBean;
import com.gtech.web.auditingPaymentOrder.bean.AmountBean;
import com.gtech.web.auditingPaymentOrder.bean.DateBean;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.beneficiary.business.BeneficiaryBusiness;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderSearchBean;
import com.gtech.web.paymentOrder.dao.PaymentOrderDAO;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import login.LoginBean;

public class PaymentOrderBusiness extends GTechModelBusiness {

    private PaymentOrderDAO paymentOrderDAO = new PaymentOrderDAO();

    public boolean doSave(UploadPaymentOrderBean uploadPaymentOrderBean,String userId) throws GTechException {
        return paymentOrderDAO.doSave(uploadPaymentOrderBean,userId);
    }

    public boolean doSavePaymentOrder(PaymentOrderBean paymentOrderBean,String userId) throws GTechException {
        return paymentOrderDAO.doSave(paymentOrderBean,userId);
    }

    public boolean doUpdatePaymentOrder(PaymentOrderBean paymentOrderBean,String userId) throws GTechException {
        return paymentOrderDAO.doUpadatePaymentOrder(paymentOrderBean,userId);
    }

    public List<PaymentOrderBean> getPaymentOrderBeanByStatus(Integer status, String dateFrom, String dateTo,
                                                              String poNumber, String benNumber, String ibanAccNumber,
                                                              LoginBean userBean) throws GTechException {
        return paymentOrderDAO.getPaymentOrderBeanByStatus(status, dateFrom, dateTo, poNumber, benNumber, ibanAccNumber, userBean);
    }
    
    public List<PaymentOrderBean> getPaymentOrderBeanByStatusForAuditScreenPayment(Integer status, String date, LoginBean userBean) throws GTechException {
        return paymentOrderDAO.getPaymentOrderBeanByStatusForAuditScreenPayment(status, date, userBean);
    }

    public List<PaymentOrderBean> getPaymentOrderBeanByStatusForMof(Integer status, String date) throws GTechException {
        return paymentOrderDAO.getPaymentOrderBeanByStatusForMof(status, date);
    }

    public List<PaymentOrderBean> getPaymentOrderBeanForAuditing(List<ExpenseBean> expenseBeanList,
                                                                 List<AmountBean> amountBeanList,
                                                                 List<DateBean> dateBeanList,
                                                                 List<EntityBean> entityBeanTable) throws GTechException {

        return paymentOrderDAO.getPaymentOrderBeanForAuditing(expenseBeanList, amountBeanList, dateBeanList,
                                                              entityBeanTable);

    }

    public ResponseContainer<PaymentOrderBean> getPaymentOrderBeanByStatusAndEntityId(RequestContainer<PaymentOrderSearchBean> requestContainer,String userId,
                                                                                        String dateFrom, String dateTo,
                                                                                        String poNumber, String benNumber, String ibanAccNumber) throws GTechException {
//        boolean check = validateOneRequiredValue(requestContainer.getDataBean().getToDate(),
//                                                 requestContainer.getDataBean().getEntityID(),
//                                                 requestContainer.getDataBean().getFromPaymentOrderCode());


//        if(!check){
//            getResponseContainer().addErrorMessage("ONE_VALUE_REQUIRED");
//
//            return getResponseContainer();
//        }

        return paymentOrderDAO.getPaymentOrderBeanList(requestContainer, userId, dateFrom, dateTo, poNumber, benNumber, ibanAccNumber);
    }

    public ResponseContainer<PaymentOrderBean> getPaymentOrderBeanForAuditedMofScreen(RequestContainer<PaymentOrderSearchBean> requestContainer,String userId
                                                                                       , String dateFrom, String dateTo,
                                                                                        String poNumber, String benNumber, String ibanAccNumber) throws GTechException {
//        boolean check = validateOneRequiredValue(requestContainer.getDataBean().getToDate(),
//                                                 requestContainer.getDataBean().getFromPaymentOrderCode());
//
//
//        if(!check){
//            getResponseContainer().addErrorMessage("ONE_VALUE_REQUIRED");
//
//            return getResponseContainer();
//        }

        return paymentOrderDAO.getPaymentOrderBeanList(requestContainer, userId, dateFrom, dateTo, poNumber, benNumber, ibanAccNumber);
        
    }


    public void doValidatePaymentOrder(PaymentOrderBean paymentOrderBean, LoginBean loginBean) throws GTechException {
        paymentOrderDAO.doValidatePaumentOrder(paymentOrderBean, loginBean);
    }

    public void fillDistributionsListData(PaymentOrderBean paymentOrderBean) throws GTechException {
        paymentOrderDAO.fillDistributionsListData(paymentOrderBean.getDistributionsPoBeanList());
    }


    public PaymentOrderBean getPaymentOrderByPONumber(String poNumber) throws GTechException {
        return paymentOrderDAO.getPaymentOrderByPONumber(poNumber);
    }

    public PaymentOrderBean getFullDataPaymentOrderByPOID(String poNumber) throws GTechException {
        return paymentOrderDAO.getFullDataPaymentOrderByPOID(poNumber);
    }


    public PaymentOrderBean getFullDataPaymentOrder(Long paymentOrderId) throws GTechException {
        return paymentOrderDAO.getFullDataPaymentOrder(paymentOrderId);
    }

    public ResponseContainer getFullDataPaymentOrderByPONumber(String paymentOrderNumber,
                                                               String entityId) throws GTechException {
        ResponseContainer responseContainer = new ResponseContainer();
        
        PaymentOrderBean paymentOrderBean = paymentOrderDAO.getFullDataPaymentOrderByPONumber(paymentOrderNumber);
        responseContainer.setDataBean(paymentOrderBean);
        if (paymentOrderBean != null) {
            if (paymentOrderBean.getPaymentOrderStatus().intValue() != 100401) {
                responseContainer.addErrorMessage("PAYMENT_ORDER_STATUS_NOT_REGISTERED");
            } else if (!entityId.equals(String.valueOf(paymentOrderBean.getEntityBean().getId()))) {
                responseContainer.addErrorMessage("PAYMENT_ORDER_DEFINED_IN_OTHER_ENTITY");
            }
        }
        return responseContainer;
    }

    public ResponseContainer validateBeneficiaryNumber(String beneficiaryCode) throws GTechException {
        ResponseContainer responseContainer = new ResponseContainer();
        BeneficiaryBean beneficiaryBean = new BeneficiaryBusiness().getBeneficiary(beneficiaryCode);
        if (beneficiaryBean != null) {
            responseContainer.setDataBean(beneficiaryBean);
        } else {
            responseContainer.addErrorMessage("BENEFICIARY_CODE_NOT_EXIST");
        }
        return responseContainer;
    }
    
    public String validateRefNumber(String refNumber) throws GTechException {
        return paymentOrderDAO.doValidateRefNumber(refNumber);
    }
    
    public String validateLocation(String refNumber) throws GTechException {
        return paymentOrderDAO.doValidateLocation(refNumber);
    }
    
    public String validateSonCofogCode(String validateSonCofogCode) throws GTechException {
        return paymentOrderDAO.doValidateSonCofogCode(validateSonCofogCode);
    }
    
    public String validateVendorWs(String refNumber) throws GTechException {
        if(refNumber != null && !refNumber.equals("999")) {
            return paymentOrderDAO.validateVendorWs(refNumber);    
        } else {
            return null;
        }
        
    }
    
    public ResponseContainer validateIbanNumber(String enterdIban, String beneficiaryIban) {
        ResponseContainer responseContainer = new ResponseContainer();
        enterdIban = "SA" + enterdIban;
        try {
            if (!paymentOrderDAO.checkIban(enterdIban, beneficiaryIban)) {
                responseContainer.addErrorMessage("IBAN_NOT_EXIST");
            }
        } catch (GTechException e) {
            e.printStackTrace();
        }
        return responseContainer;
    }

    public boolean validatePaymentDistribution(DistributionsPaymentOrderBean distributionsPaymentOrderBean) throws GTechException {
        String msg = paymentOrderDAO.validatePaymentDistribution(distributionsPaymentOrderBean);
        if (msg != null && !msg.isEmpty()) {
            FacesContext facescontext = FacesContext.getCurrentInstance();
            facescontext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
            return false;
        }
        return true;
    }

    public ResponseContainer paymentOrderSaveBusiness(PaymentOrderBean paymentOrderBean, LoginBean loginBean) throws GTechException {
        boolean isOk = true;
        ResponseContainer container = new ResponseContainer();

        if(CollectionUtilities.isEmptyList(paymentOrderBean.getDistributionsPoBeanList())){
            container.addErrorMessage("DITREPUTION_LIST_SHOULD_BE_FILLED_MESSAGE");
            return container;
        }

        doValidatePaymentOrder(paymentOrderBean,loginBean);
        if(paymentOrderBean.getGfsCode() != null && !paymentOrderBean.getGfsCode().isEmpty()) {
            boolean check = validateGFSCode(paymentOrderBean.getGfsCode());    
            if(!check) {
                if(paymentOrderBean.getRemark() != null) {
                    paymentOrderBean.setRemark(paymentOrderBean.getRemark() + "--" + MessageReader.getMessage("VALIDATE_GFS_CODE"));    
                } else {
                    paymentOrderBean.setRemark("--" + MessageReader.getMessage("VALIDATE_GFS_CODE"));    
                }
                
            }
        }
       // System.out.println(paymentOrderBean.getRemark());
        fillDistributionsListData(paymentOrderBean);
        if (paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()) {
            isOk = false;
            FacesContext facescontext = FacesContext.getCurrentInstance();
            List<String> remarks = Arrays.asList(paymentOrderBean.getRemark().split("--"));
            for (String remark : remarks) {
                facescontext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, remark, null));
            }
        } else {
            if (paymentOrderBean.getId() == 0) {
                isOk = doSavePaymentOrder(paymentOrderBean,loginBean.getUserID());
            } else {
                isOk = doUpdatePaymentOrder(paymentOrderBean,loginBean.getUserID());
            }
        }
        if (isOk) {
            container.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        } else {
            container.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
        }
        return container;
    }

    public List<PaymentOrderBean> getPaymentOrderBeanByStatusAndEntityIdAndPyamentOrder(Integer status,Integer paymentOrder, String date,
                                                                         Long entityId) throws GTechException {
        return paymentOrderDAO.getPaymentOrderBeanByStatusAndEntityIdAndPyamentOrder(status,paymentOrder, date, entityId);
    }
    public boolean updatePaymentOrderExpence(long paymentOrderId,long expencesId) throws GTechException {
       return paymentOrderDAO.updatePaymentOrderExpence(paymentOrderId, expencesId);}
    
    public PaymentOrderBean getFullDataPaymentOrderByPONumberWithoutId(String paymentOrderNumber,String entityId) throws GTechException {
        
        PaymentOrderBean paymentOrderBean = paymentOrderDAO.getFullDataPaymentOrderByPONumberWithoutId(paymentOrderNumber,entityId);
       
        return paymentOrderBean;
    }
    public boolean checkStatusAndUpdateToReview(List<PaymentOrderBean> list,String userId) throws GTechException {
        return paymentOrderDAO.checkStatusAndUpdateToReview(list, userId);
    }
    
    public Integer getNumberOFPOBeanByStatus(Integer status,LoginBean userBean) throws GTechException {
        return paymentOrderDAO.getNumberOFPOBeanByStatus(status, userBean);
    }
    
    public PaymentOrderBean getFullDataPaymentOrderByPONumber(String paymentOrderId) throws GTechException {
        return paymentOrderDAO.getFullDataPaymentOrderByNumber(paymentOrderId);
    }
    
    public boolean distributPaymentOrder(List<UserBean> userList,List<PaymentOrderBean> paymentOrderList) throws GTechException {
        
        int numberOfDisPayment = 0;
        
        int mod = 0;
        if(userList.size() == 1) {
            numberOfDisPayment = paymentOrderList.size();
        } else {
            numberOfDisPayment = paymentOrderList.size() / userList.size();
            mod = paymentOrderList.size() % userList.size();
        }
        int increament = numberOfDisPayment;
        
        int i=0;
        List<String>  pONumber = null;
        
        for(UserBean user : userList) {
            pONumber = new ArrayList<>();
            
            for(;i<numberOfDisPayment;i++) {
                pONumber.add(paymentOrderList.get(i).getPaymentOrderNumber());
            }
            numberOfDisPayment += increament;
            paymentOrderDAO.distributPaymentOrder(user.getUserIndex(), pONumber);
        }
        
        if(mod > 0) {
            pONumber = new ArrayList<>();
            for(;i<paymentOrderList.size();i++) {
                pONumber.add(paymentOrderList.get(i).getPaymentOrderNumber());
            }
            paymentOrderDAO.distributPaymentOrder(userList.get(userList.size()-1).getUserIndex(), pONumber);
        }
        
        return true;
    }

    public ResponseContainer<PaymentOrderBean> getUnassignedPaymentOrderBeanList(RequestContainer<PaymentOrderSearchBean> requestContainer) throws GTechException {
        boolean check = validateOneRequiredValue(requestContainer.getDataBean().getToDate(),
                                                 requestContainer.getDataBean().getEntityID(),
                                                 requestContainer.getDataBean().getFromPaymentOrderCode());


        if(!check){
            getResponseContainer().addErrorMessage("ONE_VALUE_REQUIRED");

            return getResponseContainer();
        }

        return paymentOrderDAO.getUnassignedPaymentOrderBeanList(requestContainer);
    }
    
    public boolean unDistributPaymentOrder(List<UserBean> userList) throws GTechException {
        
        paymentOrderDAO.unDistributPaymentOrder(userList);
        
        return true;
    }
    
    public ResponseContainer<PaymentOrderBean> getAssignedPaymentOrderBeanList(RequestContainer<PaymentOrderSearchBean> requestContainer) throws GTechException {
        boolean check = validateOneRequiredValue(requestContainer.getDataBean().getToDate(),
                                                 requestContainer.getDataBean().getEntityID(),
                                                 requestContainer.getDataBean().getFromPaymentOrderCode());


        if(!check){
            getResponseContainer().addErrorMessage("ONE_VALUE_REQUIRED");

            return getResponseContainer();
        }

        return paymentOrderDAO.getAssignedPaymentOrderBeanList(requestContainer);
    }
    
    public String isHaveCheckFunds(String paymentNo) throws GTechException {
        return paymentOrderDAO.isHaveCheckFunds(paymentNo);
    }
    
    public boolean deletePO(String poId,String poCode) throws GTechException {
        return paymentOrderDAO.deletePO(poId,poCode);    
    }
    
    public boolean validateGFSCode(String gfsCode) throws GTechException {
        return paymentOrderDAO.validateGFSCode(gfsCode);
    }
    
    public String getAgencyCodeByNumber(String paymentNo) throws GTechException {
        return paymentOrderDAO.getAgencyCodeByNumber(paymentNo);
    }
    
    public java.util.Date isPORecived(String poNumber) throws GTechException {
        return paymentOrderDAO.isPORecived(poNumber);
    }
    
    public PaymentOrderBean getFullDataPaymentOrderMOF(Long paymentOrderId,LoginBean userBean) throws GTechException {
        return paymentOrderDAO.getFullDataPaymentOrderMOF(paymentOrderId, userBean);
    }
}

