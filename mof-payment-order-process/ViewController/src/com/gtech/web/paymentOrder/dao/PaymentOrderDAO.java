package com.gtech.web.paymentOrder.dao;


import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.common.util.StringUtilities;
import com.gtech.web.actionLog.dao.ActionLogDAO;
import com.gtech.web.admin.user.UserBean;
import com.gtech.web.adminhijridate.dao.CalendarDAO;
import com.gtech.web.auditingPaymentOrder.bean.AmountBean;
import com.gtech.web.auditingPaymentOrder.bean.DateBean;
import com.gtech.web.bank.bean.BankBean;
import com.gtech.web.bank.dao.BankDAO;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.beneficiary.dao.BeneficiaryDAO;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.currency.dao.CurrencyDAO;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.department.dao.DepartmentDAO;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.ecomomicClassifications.dao.EcomomicClassificationDAO;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.dao.EntityDAO;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.expense.dao.ExpenseDAO;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.dao.LookupDao;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.nationality.dao.NationalityDao;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderSearchBean;
import com.gtech.web.project.bean.ProjectBean;
import com.gtech.web.project.dao.ProjectDAO;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.section.dao.SectionDAO;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import login.LoginBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PaymentOrderDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(PaymentOrderDAO.class);

    public List<PaymentOrderBean> getPaymentOrderBeanByStatus(Integer status, String dateFrom, String dateTo,
                                                              String poNumber, String benNumber, String ibanAccNumber,
                                                              LoginBean userBean) throws GTechException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<>();
        try {
            CalendarDAO calendarDAO = new CalendarDAO();
            ActionLogDAO actionLogDAO = new ActionLogDAO();
            System.out.println("hi");
            PaymentOrderBean paymentOrderBean = null;
            connection = getConnection();
            String query =
                " SELECT O.SGR_ID , O.SGR_CODE , O.SGR_DATE_HI , NVL2(SGR_BENEFICIARY_NAME, SGR_BENEFICIARY_NAME, B.EII_NAME_AR) AS BENEFICIARY_NAME , B.EII_NAME_EN ,B.EII_NAME_AR , O.SGR_BENEFICIARY_NAME," +
                " (SELECT EPP_CODE FROM POG_EXPENSE_TYPES WHERE EPP_ID = O.SGR_EPP_ID ) EPP_CODE, " +
                " O.SGR_LTD_MODIFIED_ON_HI ,O.SGR_PAYMENT_AMOUNT , O.SGR_REMARKS,O.SGR_ENTRY_METHOD " +
                " FROM POG_PAYMENT_ORDERS O , POG_BENEFICIARIES  B  " + " WHERE O.SGR_EII_ID = B.EII_ID(+)" +
                " AND   O.SGR_STATUS = ? ";

            if (userBean.getEntityBean() == null || userBean.getEntityBean().getId() <= 0) {
                if (status == 100410) {
                    query += " AND (SGR_ENTRY_METHOD  = 100603 OR SGR_ENTRY_METHOD  = 100605) ";
                } else {
                    System.out.println("hi");
                    query += " AND (SGR_ENTRY_METHOD  = 100603 OR SGR_ENTRY_METHOD  = 100605) AND SGR_CREATED_BY_SSC_ID <> " + userBean.getUserID();
                    System.out.println(query);
                }
                
            } else {    
                query +=
                    " AND (SGR_ENTRY_METHOD  <> 100603 OR SGR_ENTRY_METHOD  <> 100605) AND O.SGR_EEO_ID = ?" +
                    " AND O.SGR_TTG_ID = ?" + " AND O.SGR_PTN_ID = ? AND O.SGR_ORG_ID = ? ";
            }

           
            if(dateTo != null){
              String gergDateTo = calendarDAO.getGrDateByHijriDate(connection, dateTo);
                    query +=
                                " AND TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) <=  TO_DATE('" + gergDateTo +
                                    "', 'DD/MM/YYYY') ";
                }
            
            if(dateFrom != null) {
                String gergDateFrom = calendarDAO.getGrDateByHijriDate(connection, dateFrom);
                query +=
                    " AND TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) >=  TO_DATE('" + gergDateFrom +
                    "', 'DD/MM/YYYY') ";
            }
            
            if (poNumber != null && !poNumber.isEmpty()) {
                query += " AND  O.SGR_CODE = '" + poNumber + "'";
            }

            if (ibanAccNumber != null && !ibanAccNumber.isEmpty()) {
                query += " AND  O.SGR_IBAN_CODE = '" + ibanAccNumber + "'";
            }

            if (benNumber != null && !benNumber.isEmpty()) {
                query += " AND  B.EII_CODE = '" + benNumber + "'";
            }

            query += " ORDER BY TO_NUMBER(O.SGR_CODE) ASC ";
           
            
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, status);
            if (userBean.getEntityBean() == null || userBean.getEntityBean().getId() <= 0) {
            } else {
                preparedStatement.setLong(2, userBean.getEntityBean().getId());
                preparedStatement.setLong(3, userBean.getDepartmentBean().getId());
                preparedStatement.setLong(4, userBean.getSectionBean().getId());
                preparedStatement.setLong(5, userBean.getOrganizationBean().getId());
            }


            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                paymentOrderBean.setRemark(rs.getString("SGR_REMARKS"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("BENEFICIARY_NAME"));
                paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));

                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBean.setActionLogBean(actionLogDAO.getPaymentActionLog(paymentOrderBean.getId()));
                paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                paymentOrderBeanList.add(paymentOrderBean);

                paymentOrderBean.setDistributionsPaymentOrderBean(getDistributionsPaymentOrderBeanByPaymentOrderID(rs.getString("SGR_ID"),
                                                                                                                   userBean.getEntityBean()
                                                                                                                   .getCode(),
                                                                                                                   userBean.getDepartmentBean()
                                                                                                                   .getCode(),
                                                                                                                   userBean.getSectionBean()
                                                                                                                   .getCode(),
                                                                                                                   connection));


            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return paymentOrderBeanList;

    }

    public List<PaymentOrderBean> getPaymentOrderBeanByStatusForAuditScreenPayment(Integer status, String date,
                                                                                   LoginBean userBean) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        ActionLogDAO actionLogDAO = new ActionLogDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<>();

        String query =
            " SELECT SGR_LTD_MODIFIED_ON_HI, SGR_ID,SGR_CODE,SGR_DATE_HI,SGR_DATE_GR,SGR_YEAR,SGR_IBAN_CODE,SGR_PAYMENT_AMOUNT,SGR_REMARKS,EII_NAME_AR BENEFICIARY_NAME,SGR_BENEFICIARY_NAME,NVL(PROJECT_NAME_AR,SCO_DESC_AR) PROGRAM_NAME                 " +
            "          FROM POG_PAYMENT_ORDERS,   " + "               POG_BENEFICIARIES,   " +
            "               POG_SGR_LINES,  " + "               POG_PROJECTS POG,   " +
            "               POG_ECOMOMIC_CLASSIFICATIONS, " + "               POG_ENTITIES, " +
            "               POG_DEPARTMENTS, " + "               POG_SECTION                        " +
            "         WHERE SGR_ID = SON_SGR_ID(+)   " + "           AND SGR_EII_ID = EII_ID(+)   " +
            "           AND SON_PPR_ID = PPR_ID(+)   " + "           AND SON_SCO_ID = SCO_ID(+) " +
            "           AND SGR_EEO_ID = EEO_ID " + "           AND SGR_TTG_ID = TTG_ID " +
            "           AND SGR_PTN_ID = PTN_ID              " + "           AND POG_PAYMENT_ORDERS.SGR_STATUS = ?   " +
            "           AND TRUNC(NVL(POG_PAYMENT_ORDERS.SGR_LTD_MODIFIED_ON_GR,POG_PAYMENT_ORDERS.SGR_CREATED_ON_GR)) <=  TO_DATE(?, 'DD/MM/YYYY')   " +
            "           AND POG_PAYMENT_ORDERS.SGR_EEO_ID = ?  AND POG_PAYMENT_ORDERS.SGR_TTG_ID = ?  AND POG_PAYMENT_ORDERS.SGR_PTN_ID = ?  " +
            "              AND POG.ENTITY_CODE = EEO_CODE " + "             AND POG.DEPT_CODE  = TTG_CODE  " +
            "             AND POG.SECTION_CODE = PTN_CODE " + "             AND SON_PPR_ID IS NOT NULL " +
            "           AND SON_PPR_ID IS NOT NULL  " +
            "           AND SON_ID = (SELECT MIN(SON_ID) FROM POG_SGR_LINES WHERE SON_SGR_ID = SGR_ID AND SON_PPR_ID IS NOT NULL) " +
            "         UNION ALL   " +
            "  SELECT SGR_LTD_MODIFIED_ON_HI, SGR_ID,SGR_CODE,SGR_DATE_HI,SGR_DATE_GR,SGR_YEAR,SGR_IBAN_CODE,SGR_PAYMENT_AMOUNT,SGR_REMARKS,EII_NAME_AR BENEFICIARY_NAME,SGR_BENEFICIARY_NAME,NVL(PROJECT_NAME_AR,SCO_DESC_AR) PROGRAM_NAME   " +
            "          FROM POG_PAYMENT_ORDERS,   " + "               POG_BENEFICIARIES,   " +
            "               POG_SGR_LINES,  " + "               POG_PROJECTS POG,   " +
            "               POG_ECOMOMIC_CLASSIFICATIONS   " + "         WHERE SGR_ID = SON_SGR_ID(+)   " +
            "           AND SGR_EII_ID = EII_ID(+)   " + "           AND SON_PPR_ID = PPR_ID(+)   " +
            "           AND SON_SCO_ID = SCO_ID(+)   " + "           AND POG_PAYMENT_ORDERS.SGR_STATUS =?    " +
            "           AND TRUNC(NVL(POG_PAYMENT_ORDERS.SGR_LTD_MODIFIED_ON_GR,POG_PAYMENT_ORDERS.SGR_CREATED_ON_GR)) <=  TO_DATE(?, 'DD/MM/YYYY')   " +
            "           AND POG_PAYMENT_ORDERS.SGR_EEO_ID = ?  AND POG_PAYMENT_ORDERS.SGR_TTG_ID = ?  AND POG_PAYMENT_ORDERS.SGR_PTN_ID = ?  " +
            "           AND SON_ID = (SELECT MIN(SON_ID) FROM POG_SGR_LINES WHERE SON_SGR_ID = SGR_ID) " +
            "           AND SON_PPR_ID IS  NULL";


        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, status);
            preparedStatement.setString(2, calendarDAO.getGrDateByHijriDate(connection, date));
            preparedStatement.setString(3, userBean.getEecID());
            preparedStatement.setString(4, userBean.getDepartment());
            preparedStatement.setString(5, userBean.getSection());

            preparedStatement.setInt(6, status);
            preparedStatement.setString(7, calendarDAO.getGrDateByHijriDate(connection, date));
            preparedStatement.setString(8, userBean.getEecID());
            preparedStatement.setString(9, userBean.getDepartment());
            preparedStatement.setString(10, userBean.getSection());

            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                paymentOrderBean.setRemark(rs.getString("SGR_REMARKS"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("BENEFICIARY_NAME"));
                paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBean.setDistributionsName(rs.getString("PROGRAM_NAME"));
                paymentOrderBean.setActionLogBean(actionLogDAO.getPaymentActionLog(paymentOrderBean.getId()));
                paymentOrderBeanList.add(paymentOrderBean);

            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return paymentOrderBeanList;

    }


    private DistributionsPaymentOrderBean getDistributionsPaymentOrderBeanByPaymentOrderID(String paymentID,
                                                                                           String entityCode,
                                                                                           String deptCode,
                                                                                           String sectionCode,
                                                                                           Connection connection) throws GTechException {
        DistributionsPaymentOrderBean distributionsPaymentOrderBean = null;

        String query =
            " SELECT SCO.SCO_DESC_AR, " + "       SON.SON_SGR_ID, " + "       (SELECT PPR.PROJECT_NAME_AR " +
            "          FROM POG_PROJECTS PPR " + "         WHERE     1 = 1 " +
            "               AND SON.SON_PPR_ID = PPR.PPR_ID " + "               AND SCO.SCO_ID = PPR_SCO_ID" +
            "               AND PPR.ENTITY_CODE = ?" + "               AND PPR.DEPT_CODE = ? " +
            "               AND PPR.SECTION_CODE = ? ) " + "          PROJECT_NAME_AR " + "  FROM POG_SGR_LINES SON, " +
            "       POG_PAYMENT_ORDERS SGR, " + "       POG_ECOMOMIC_CLASSIFICATIONS SCO " +
            " WHERE     SON.SON_SGR_ID = SGR.SGR_ID " + "       AND SGR.SGR_ID = ? " +
            "       AND SON.SON_SCO_ID = SCO.SCO_ID ";

        PreparedStatement statment = null;
        ResultSet rs = null;
        try {

            statment = connection.prepareStatement(query.toString());
            statment.setString(1, entityCode);
            statment.setString(2, deptCode);
            statment.setString(3, sectionCode);
            statment.setString(4, paymentID);
            rs = statment.executeQuery();
            if (rs.next()) {
                distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();

                ProjectBean projectBean = new ProjectBean();
                projectBean.setNameAr(rs.getString("PROJECT_NAME_AR"));

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));

                distributionsPaymentOrderBean.setProjectBean(projectBean);
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);
            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(statment, rs);
        }
        return distributionsPaymentOrderBean;
    }

    public List<PaymentOrderBean> getPaymentOrderBeanByStatusForMof(Integer status, String date) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<>();
        String query =
            " SELECT O.SGR_ID , O.SGR_CODE , O.SGR_DATE_HI , B.EII_NAME_AR , B.EII_NAME_EN ," +
            " (SELECT EPP_CODE FROM POG_EXPENSE_TYPES WHERE EPP_ID = O.SGR_EPP_ID ) EPP_CODE, " +
            " O.SGR_LTD_MODIFIED_ON_HI ,O.SGR_PAYMENT_AMOUNT , O.SGR_REMARKS " +
            " FROM POG_PAYMENT_ORDERS O , POG_BENEFICIARIES  B  " + " WHERE O.SGR_EII_ID = B.EII_ID(+)" +
            " AND   O.SGR_STATUS = ? " + " AND TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) <=  TO_DATE(?, 'DD/MM/YYYY') " +

            " order by TO_NUMBER(O.SGR_CODE) ASC ";

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, status);
            //            java.sql.Date sqlDate = new java.sql.Date(calendarDAO.getGrDateByHijriDate(connection, date).getTime());
            preparedStatement.setString(2, calendarDAO.getGrDateByHijriDate(connection, date));

            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                paymentOrderBean.setRemark(rs.getString("SGR_REMARKS"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBeanList.add(paymentOrderBean);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return paymentOrderBeanList;

    }

    public boolean checkStatusAndUpdateToReview(List<PaymentOrderBean> list, String userId) throws GTechException {

        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            boolean isInserted = false;
            for (PaymentOrderBean paymentOrderBean : list) {
                if (paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()) {
                    continue;
                }
                isExists("" + paymentOrderBean.getId(), userId, connection);
            }
            if (isInserted) {
                connection.setAutoCommit(true);
                connection.commit();
            } else {
                connection.rollback();
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return true;

    }

    public boolean isExists(String id, String userId, Connection connection) throws GTechException {
        boolean isExists = false;
        PreparedStatement preparedStatement = null;
        ActionLogDAO actionLogDAO = new ActionLogDAO();
        ResultSet rs = null;
        String query =
            " SELECT count(1) AS COUN" + " FROM POG_PAYMENT_ORDERS " + " WHERE SGR_STATUS = 100401 " +
            " AND SGR_ID = ? ";

        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                if (rs.getInt("COUN") >= 1) {
                    isExists = true;
                    actionLogDAO.insereLogLine(id, "", userId, 100502, connection);
                }
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(preparedStatement, rs);
        }
        return isExists;

    }

    public List<PaymentOrderBean> getPaymentOrderBeanForAuditing(List<ExpenseBean> expenseBeanList,
                                                                 List<AmountBean> amountBeanList,
                                                                 List<DateBean> dateBeanList,
                                                                 List<EntityBean> entityBeanTable) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<>();
        String query =
            "SELECT P.SGR_ID,P.SGR_CODE,B.EII_NAME_AR,E.EPP_CODE,P.SGR_LTD_MODIFIED_ON_HI,P.SGR_PAYMENT_AMOUNT " +
            " FROM   POG_PAYMENT_ORDERS P," + "       POG_BENEFICIARIES  B," + "       POG_EXPENSE_TYPES  E" +
            " WHERE  P.SGR_EPP_ID = E.EPP_ID(+)" + "  AND  P.SGR_EII_ID = B.EII_ID(+)" + "  AND  P.SGR_STATUS = 100403";
        try {

            connection = getConnection();
            if (entityBeanTable != null && !entityBeanTable.isEmpty()) {
                String entityIds = "";
                boolean isExists = false;
                for (EntityBean entityBean : entityBeanTable) {
                    if (entityBean.getId() != 0) {
                        isExists = true;
                        if (entityIds.isEmpty()) {
                            entityIds = " " + entityBean.getId();

                        } else {
                            entityIds = " " + entityIds + "," + entityBean.getId();
                        }

                    }
                }
                if (isExists) {
                    query = query + " AND SGR_EPP_ID IN(" + entityIds + ")";

                }
            }

            if (dateBeanList != null && !dateBeanList.isEmpty()) {
                String dateIds = "";
                boolean isMulti = false;
                for (DateBean dateBean : dateBeanList) {
                    if (dateBean.getFromDate() != null && dateBean.getToDate() != null) {
                        if (dateIds.isEmpty()) {
                            dateIds =
                                " ( P.SGR_DATE_GR BETWEEN  TO_DATE('" +
                                calendarDAO.getGrDateByHijriDate(connection, dateBean.getFromDate()) +
                                "','DD/MM/YYYY') " + " AND TO_DATE('" +
                                calendarDAO.getGrDateByHijriDate(connection, dateBean.getToDate()) + "','DD/MM/YYYY')";

                        } else {
                            dateIds =
                                dateIds + "  OR P.SGR_DATE_GR BETWEEN TO_DATE('" +
                                calendarDAO.getGrDateByHijriDate(connection, dateBean.getFromDate()) +
                                "','DD/MM/YYYY') " + "  AND TO_DATE('" +
                                calendarDAO.getGrDateByHijriDate(connection, dateBean.getToDate()) + "','DD/MM/YYYY')";
                            isMulti = true;
                        }
                    }
                }
                if (!dateIds.isEmpty()) {
                    query = query + " AND " + dateIds + ")";
                }

            }
            if (amountBeanList != null && !amountBeanList.isEmpty()) {
                String amountIds = "";
                for (AmountBean amountBean : amountBeanList) {
                    if (amountBean.getFromAmount() != null && amountBean.getToAmount() != null) {
                        if (amountIds.isEmpty()) {
                            amountIds =
                                " ( P.SGR_PAYMENT_AMOUNT BETWEEN " + amountBean.getFromAmount() + " AND " +
                                amountBean.getToAmount();

                        } else {
                            amountIds =
                                amountIds + "  OR P.SGR_PAYMENT_AMOUNT  BETWEEN " + amountBean.getFromAmount() +
                                " AND " + amountBean.getToAmount();
                        }
                    }

                }
                if (!amountIds.isEmpty()) {
                    query = query + " AND " + amountIds + ")";

                }

            }
            if (expenseBeanList != null && !expenseBeanList.isEmpty()) {
                String expenseIds = "";
                boolean isExists = false;
                for (ExpenseBean expenseBean : expenseBeanList) {
                    if (expenseBean.isCheck()) {
                        isExists = true;
                        if (expenseIds.isEmpty()) {
                            expenseIds = "" + expenseBean.getId();

                        } else {
                            expenseIds = expenseIds + "," + expenseBean.getId();
                        }
                    }

                }
                if (isExists) {
                    query = query + " AND SGR_EPP_ID IN(" + expenseIds + ")";

                }
            }

            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBeanList.add(paymentOrderBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return paymentOrderBeanList;

    }

    public ResponseContainer<PaymentOrderBean> getPaymentOrderBeanList(RequestContainer<PaymentOrderSearchBean> requestContainer,
                                                                       String userId, String dateFrom, String dateTo,
                                                                       String poNumber, String benNumber,
                                                                       String ibanAccNumber) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        PaymentOrderSearchBean searchPaymentOrderBean = requestContainer.getDataBean();
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<PaymentOrderBean>();
        ResponseContainer<PaymentOrderBean> responseContainer = new ResponseContainer<PaymentOrderBean>();
        String query =
            " SELECT O.SGR_ID , O.SGR_CODE , O.SGR_DATE_HI , NVL2(SGR_BENEFICIARY_NAME, SGR_BENEFICIARY_NAME, B.EII_NAME_AR) AS BENEFICIARY_NAME , B.EII_NAME_EN ," +
            " X.EPP_CODE, O.SGR_LTD_MODIFIED_ON_HI ,O.SGR_PAYMENT_AMOUNT ," +
            " P.EEO_ID , P.EEO_NAME_AR , P.EEO_NAME_EN,USR.SSC_NAME_AR USRNAME  " +
            " FROM POG_PAYMENT_ORDERS O , POG_BENEFICIARIES  B , POG_EXPENSE_TYPES X , POG_ENTITIES P,POG_USERS USR" +
            " WHERE O.SGR_EII_ID = B.EII_ID(+)" + " AND   O.SGR_EPP_ID = X.EPP_ID(+)" +
            " AND   O.SGR_EEO_ID = P.EEO_ID AND (O.SGR_ASSIGNED_USER IS NULL OR O.SGR_ASSIGNED_USER = ? ) AND USR.SSC_ID(+) = O.SGR_ASSIGNED_USER";

        try {
            connection = getConnection();
            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getPaymentOrderStatus())) {
                query += " AND   O.SGR_STATUS = " + searchPaymentOrderBean.getPaymentOrderStatus();
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getEntityID())) {
                query += " AND   O.SGR_EEO_ID = " + searchPaymentOrderBean.getEntityID();
            }

            //edit by mohiee
            if (dateFrom != null && dateTo != null) {
                String gergDateFrom = calendarDAO.getGrDateByHijriDate(connection, dateFrom);
                String gergDateTo = calendarDAO.getGrDateByHijriDate(connection, dateTo);
                query +=
                    " AND TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) >=  TO_DATE('" + gergDateFrom +
                    "', 'DD/MM/YYYY') ";
                query +=
                    " AND TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) <=  TO_DATE('" + gergDateTo +
                    "', 'DD/MM/YYYY') ";
            }

            if (poNumber != null && !poNumber.isEmpty()) {
                query += " AND  O.SGR_CODE = '" + poNumber + "'";
            }

            if (ibanAccNumber != null && !ibanAccNumber.isEmpty()) {
                query += " AND  O.SGR_IBAN_CODE = '" + ibanAccNumber + "'";
            }

            if (benNumber != null && !benNumber.isEmpty()) {
                query += " AND  B.EII_CODE = '" + benNumber + "'";
            }


            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getToDate())) {
                query +=
                    " AND  TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) <=  TO_DATE('" +
                    calendarDAO.getGrDateByHijriDate(connection, searchPaymentOrderBean.getToDate()) +
                    "', 'DD/MM/YYYY') ";

            }

            query += " ORDER BY O.SGR_ASSIGNED_USER ASC,TO_NUMBER(O.SGR_CODE) ASC ";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, userId);
            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("BENEFICIARY_NAME"));
                beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                EntityBean entityBean = new EntityBean();
                entityBean.setId(rs.getLong("EEO_ID"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                paymentOrderBean.setEntityBean(entityBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBean.setAssignedUserName(rs.getString("USRNAME"));
                paymentOrderBeanList.add(paymentOrderBean);
            }


            responseContainer.setDataList(paymentOrderBeanList);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return responseContainer;

    }

    public boolean doSave(UploadPaymentOrderBean uploadPaymentOrderBean, String userId) throws GTechException {

        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            boolean isInserted = false;
            for (PaymentOrderBean paymentOrderBean : uploadPaymentOrderBean.getPaymentOrderBeanList()) {
                if (paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()) {
                    continue;
                }
                isInserted = doSave(paymentOrderBean, connection, userId);

                if (!isInserted) {
                    break;
                }
            }
            if (isInserted) {
                connection.setAutoCommit(true);
                connection.commit();
            } else {
                connection.rollback();
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return true;

    }

    public boolean doSave(PaymentOrderBean paymentOrderBean, String userId) throws GTechException {

        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            boolean isInserted = false;

            isInserted = doSave(paymentOrderBean, connection, userId);

            if (isInserted) {
                connection.setAutoCommit(true);
                connection.commit();
            } else {
                connection.rollback();
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return true;

    }

    public String validatePaymentDistribution(DistributionsPaymentOrderBean distributionsBean) throws GTechException {
        String msg = null;
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.validate_prog_class(?,?,?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, "AR");
            String projcetCode = distributionsBean.getProjectBean().getCode();
            if (projcetCode == null || projcetCode.isEmpty()) {
                callableStatement.setNull(3, Types.VARCHAR);
            } else {
                callableStatement.setString(3, projcetCode);
            }
            callableStatement.setString(4, distributionsBean.getEcomomicClassificationBean().getCode());
            callableStatement.executeUpdate();
            msg = callableStatement.getString(1);
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return msg;
    }


    public boolean doUpadatePaymentOrder(PaymentOrderBean paymentOrderBean, String userId) throws GTechException {
        boolean isOk = true;
        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            isOk =
                deletePaymentOrderDistributions(paymentOrderBean.getId(), connection) &&
                updatePaymentOrder(paymentOrderBean, connection, userId);

            for (DistributionsPaymentOrderBean distributionsPaymentOrderBean :
                 paymentOrderBean.getDistributionsPoBeanList()) {
                if (!insertDistribution(distributionsPaymentOrderBean, (int) paymentOrderBean.getId(), connection,paymentOrderBean.getRefNumber())) {
                    isOk = false;
                    break;
                }
            }
            if (isOk) {

                connection.commit();
            } else {
                connection.rollback();
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return isOk;

    }

    private boolean updatePaymentOrder(PaymentOrderBean paymentOrderBean, Connection connection,
                                       String userId) throws GTechException {
        boolean isOk = true;
        PreparedStatement preparedStatement = null;
        try {
            CalendarDAO calendarDAO = new CalendarDAO();

            String query = "UPDATE POG_PAYMENT_ORDERS SET  " + "SGR_DATE_HI = ?, " + //----1
                "SGR_YEAR = ?, " + //----2
                "SGR_EEO_ID = ?," + //----3
                " SGR_TTG_ID = ?, " + //----4
                "SGR_PTN_ID = ?, " + //----5
                "SGR_EII_ID = ?, " + //----6
                "SGR_PAYMENT_METHOD = ?, " + //----7
                "SGR_CRN_ID = ?, " + //----8
                "SGR_GSK_ID = ?," + //----9
                " SGR_IBAN_CODE = ?, " + //----10
                "SGR_PAYMENT_AMOUNT = ?, " + //----11
                "SGR_PAYMENT_AMOUNT_TEXT = ?, " + //----12
                "SGR_IN_EXCHANGE_OF = ?," + //----13
                " SGR_ENTRY_METHOD = ?, " + //----14
                "SGR_STATUS = ?, " + //----15
                "SGR_LTD_MODIFIED_BY_SSC_ID = ?," + //----16
                " SGR_LTD_MODIFIED_ON_HI = ?, " + //----17
                "SGR_LTD_MODIFIED_ON_GR = SYSDATE, " + //----
                "SGR_REMARKS = ?, " + //----18
                "SGR_EPP_ID = ? , " + //----19
                "SGR_BENEFICIARY_IIP_ID = ?," + //----20
                "SGR_BENEFICIARY_NAME = ?," + //----21
                "SGR_ORG_ID = ? ," + //----22
                "SGR_CODE = ?" + // ----23
                " WHERE SGR_ID =?"; //----24

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, paymentOrderBean.getPaymentOrderDateH());
            preparedStatement.setString(2, paymentOrderBean.getPaymentOrderYear());
            preparedStatement.setLong(3, paymentOrderBean.getEntityBean().getId());
            preparedStatement.setLong(4, paymentOrderBean.getDepartmentBean().getId());
            preparedStatement.setLong(5, paymentOrderBean.getSectionBean().getId());
            if (paymentOrderBean.getBeneficiaryBean().getId() == 0) {
                preparedStatement.setString(6, null);
            } else {
                preparedStatement.setLong(6, paymentOrderBean.getBeneficiaryBean().getId());
            }
            preparedStatement.setLong(7, paymentOrderBean.getPaymentMethod());
            preparedStatement.setString(8, paymentOrderBean.getCurrencyBean().getCurId());
            preparedStatement.setString(9, "");
            String iban = paymentOrderBean.getIban();
            preparedStatement.setString(10,
                                        iban != null && !iban.isEmpty() && !iban.startsWith("SA") ? "SA" + iban : iban);
            preparedStatement.setString(11, BigDecimal.valueOf(paymentOrderBean.getAmount()).toPlainString());
            preparedStatement.setString(12, paymentOrderBean.getAmountInWord());
            preparedStatement.setString(13, paymentOrderBean.getExchangeOf());

            if (paymentOrderBean.getPaymentEntryMethod() != null && paymentOrderBean.getPaymentEntryMethod() > 0) {
                preparedStatement.setInt(14, paymentOrderBean.getPaymentEntryMethod());
            } else {
                preparedStatement.setInt(14, 100602);
            }

            preparedStatement.setInt(15, 100401);
            preparedStatement.setString(16, userId);
            preparedStatement.setString(17, calendarDAO.getCurrentHijriDate(connection));
            preparedStatement.setString(18, paymentOrderBean.getRemark());

            String expenseID = "";
            if (paymentOrderBean.getExpenseBean().getId() != 0) {
                expenseID = String.valueOf(paymentOrderBean.getExpenseBean().getId());
            }
            preparedStatement.setString(19, expenseID);
            preparedStatement.setString(20, paymentOrderBean.getNationalityBean().getNationalityID());
            preparedStatement.setString(21, paymentOrderBean.getBeneficaryName());

            preparedStatement.setString(23, paymentOrderBean.getPaymentOrderNumber());
            preparedStatement.setLong(24, paymentOrderBean.getId());
            System.out.println(paymentOrderBean.getId());
            if (paymentOrderBean.getOrgId() > 0) {
                preparedStatement.setLong(22, paymentOrderBean.getOrgId());
            } else {
                preparedStatement.setString(22, null);
            }
            preparedStatement.executeUpdate();

            if (!new ActionLogDAO()
                .insereLogLine(paymentOrderBean.getId(), "", paymentOrderBean.getUserId().longValue(), 100501,
                               connection)) {
                return false;
            }

        } catch (SQLException exception) {
            isOk = false;
            throw new GTechException(exception);
        } finally {
            closeResources(preparedStatement);
        }
        return isOk;
    }


    private boolean doSave(PaymentOrderBean paymentOrderBean, Connection connection,
                           String userId) throws GTechException, SQLException {
        CalendarDAO calendarDAO = new CalendarDAO();
        PreparedStatement preparedStatement = null;
        Integer poId = getSeq("SGR_PK_SEQ", connection);
        String query = "INSERT INTO POG_PAYMENT_ORDERS " + "(SGR_ID," + //--1
            "SGR_CODE," + //--2
            "SGR_DATE_HI," + //--3
            "SGR_YEAR," + //--4
            "SGR_EEO_ID," + //--5
            "SGR_TTG_ID," + //--6
            "SGR_PTN_ID," + //--7
            "SGR_EII_ID," + //--8
            "SGR_PAYMENT_METHOD," + //--9
            "SGR_CRN_ID," + //--10
            "SGR_GSK_ID," + //--11
            "SGR_IBAN_CODE," + //--12
            "SGR_PAYMENT_AMOUNT," + //--13
            "SGR_PAYMENT_AMOUNT_TEXT," + //--14
            "SGR_IN_EXCHANGE_OF," + //--15
            "SGR_ENTRY_METHOD," + //--16
            "SGR_STATUS," + //--17
            "SGR_CREATED_BY_SSC_ID," + //--18
            "SGR_CREATED_ON_HI," + //--19
            "SGR_CREATED_ON_GR," + "SGR_LTD_MODIFIED_BY_SSC_ID," + //--20
            "SGR_LTD_MODIFIED_ON_HI," + //--21
            "SGR_LTD_MODIFIED_ON_GR," + "SGR_REMARKS," + //--22
            "SGR_EPP_ID , " + //--23
            "SGR_BENEFICIARY_IIP_ID," + //--24
            "SGR_BENEFICIARY_NAME," + //--25
            "SGR_ORG_ID," +//--26
            "SGR_LOCATION_CODE " + ") " + //--27
            "VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,?,?,SYSDATE,?,?,?,?,?,?)";

        preparedStatement = connection.prepareStatement(query);

        preparedStatement.setInt(1, poId);
        preparedStatement.setString(2, paymentOrderBean.getPaymentOrderNumber());
        preparedStatement.setString(3, paymentOrderBean.getPaymentOrderDateH());
        preparedStatement.setString(4, paymentOrderBean.getPaymentOrderYear());
        preparedStatement.setLong(5, paymentOrderBean.getEntityBean().getId());
        preparedStatement.setLong(6, paymentOrderBean.getDepartmentBean().getId());
        preparedStatement.setLong(7, paymentOrderBean.getSectionBean().getId());
        if (paymentOrderBean.getBeneficiaryBean().getId() == 0) {
            preparedStatement.setString(8, null);
        } else {
            preparedStatement.setLong(8, paymentOrderBean.getBeneficiaryBean().getId());
        }

        preparedStatement.setLong(9, paymentOrderBean.getPaymentMethod());
        preparedStatement.setString(10, paymentOrderBean.getCurrencyBean().getCurId());
        preparedStatement.setString(11, ""); //paymentOrderBean.getBankBean().getId());
        String iban = paymentOrderBean.getIban();
        preparedStatement.setString(12, iban != null && !iban.isEmpty() && !iban.startsWith("SA") ? "SA" + iban : iban);
        System.out.println(BigDecimal.valueOf(paymentOrderBean.getAmount()).toPlainString());
        preparedStatement.setString(13, BigDecimal.valueOf(paymentOrderBean.getAmount()).toPlainString());
        preparedStatement.setString(14, paymentOrderBean.getAmountInWord());

//        if (paymentOrderBean.getExchangeOf() == null || paymentOrderBean.getExchangeOf().equals("")) {
//            paymentOrderBean.setExchangeOf("SADASSDA");
//        }

        preparedStatement.setString(15, paymentOrderBean.getExchangeOf());

        if (paymentOrderBean.getPaymentEntryMethod() != null && paymentOrderBean.getPaymentEntryMethod() > 0) {
            preparedStatement.setInt(16, paymentOrderBean.getPaymentEntryMethod());
        } else {
            preparedStatement.setInt(16, 100602);
        }

        preparedStatement.setInt(17, 100401);
        preparedStatement.setString(18, userId);
        preparedStatement.setString(19, calendarDAO.getCurrentHijriDate(connection));
        preparedStatement.setString(20, userId);
        preparedStatement.setString(21, calendarDAO.getCurrentHijriDate(connection));
        preparedStatement.setString(22, paymentOrderBean.getRemark());

        String expenseID = "";
        if (paymentOrderBean.getExpenseBean() != null && paymentOrderBean.getExpenseBean().getId() != 0) {
            expenseID = String.valueOf(paymentOrderBean.getExpenseBean().getId());
        }
        preparedStatement.setString(23, expenseID);
        preparedStatement.setString(24, paymentOrderBean.getNationalityBean().getNationalityID());
        preparedStatement.setString(25, paymentOrderBean.getBeneficaryName());
        if (paymentOrderBean.getOrgId() > 0) {
            preparedStatement.setLong(26, paymentOrderBean.getOrgId());
        } else {
            preparedStatement.setString(26, null);
        }
        preparedStatement.setString(27, paymentOrderBean.getLocationCode());
        System.out.println(paymentOrderBean.getPaymentOrderStatus() );
        // execute insert SQL stetement
        preparedStatement.execute();
        for (DistributionsPaymentOrderBean distributionsPaymentOrderBean :
             paymentOrderBean.getDistributionsPoBeanList()) {

            boolean isInsertDistribution = insertDistribution(distributionsPaymentOrderBean, poId, connection,paymentOrderBean.getRefNumber());
            if (!isInsertDistribution) {
                return isInsertDistribution;
            }
        }

        if (poId == null) {
            return false;
        }
        if (userId == null) {
            return false;
        }

        if (connection == null) {
            return false;
        }

        if (!new ActionLogDAO().insertLogLine(poId.longValue(), "", Long.valueOf(userId), 100501, connection)) {
            return false;
        }
        paymentOrderBean.setId(poId);
        return true;
    }

    private boolean insertDistribution(DistributionsPaymentOrderBean distributionsPaymentOrderBean, Integer poId,
                                       Connection connection,String refumber) throws GTechException, SQLException {

        PreparedStatement preparedStatement = null;

//        String query =
//            "INSERT INTO POG_SGR_LINES  " + "(SON_SGR_ID," + "SON_PPR_ID," + "SON_SCO_ID," + "SON_AMOUNT_IN_SAR," +
//            "SON_REFUND_CATEGORY_CODE," + "SON_REFUND_AMOUNT_IN_SAR) VALUES" + "(?,?,?,?,?,?)";
        
        String query =
            "INSERT INTO POG_SGR_LINES  " + "(SON_SGR_ID," + "SON_PPR_ID," + "SON_SCO_ID," + "SON_AMOUNT_IN_SAR," +
            "SON_REFUND_CATEGORY_CODE," + "SON_REFUND_AMOUNT_IN_SAR,CASH_REF_NUMBER,SON_COFOG_CODE) VALUES" + "(?,?,?,?,?,?,?,?)";
        preparedStatement = connection.prepareStatement(query);
        

        String projectID = "";
        if (distributionsPaymentOrderBean.getProjectBean().getId() != 0) {
            projectID = String.valueOf(distributionsPaymentOrderBean.getProjectBean().getId());
        }

        preparedStatement.setInt(1, poId);
        preparedStatement.setString(2, projectID);
        preparedStatement.setLong(3, distributionsPaymentOrderBean.getEcomomicClassificationBean().getId());
        preparedStatement.setString(4, BigDecimal.valueOf(distributionsPaymentOrderBean.getAmount()).toPlainString());
        preparedStatement.setString(5, "");
        preparedStatement.setDouble(6, 0);
        preparedStatement.setString(7, refumber);
        preparedStatement.setString(8, distributionsPaymentOrderBean.getSonCofogCode());
       
        // execute insert SQL stetement
        return preparedStatement.executeUpdate() > 0;
    }

    public boolean deletePaymentOrderDistributions(Long poId, Connection connection) throws GTechException,
                                                                                            SQLException {
        boolean isOk = true;
        PreparedStatement preparedStatement = null;
        try {
            String query = "DELETE FROM POG_SGR_LINES WHERE SON_SGR_ID = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, poId);
            preparedStatement.executeUpdate();
        } catch (Exception exception) {
            isOk = false;
            throw new GTechException(exception);
        } finally {
            closeResources(preparedStatement);
        }
        return isOk;
    }

    public PaymentOrderBean getPaymentOrderByPONumber(String poNumber) throws GTechException {
        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        String query = "SELECT * FROM POG_PAYMENT_ORDERS, POG_SGR_LINES WHERE SGR_ID = SON_SGR_ID(+) AND SGR_CODE = ?";
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, poNumber);
            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));

                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);


                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setId(rs.getLong("SGR_BENEFICIARY_IIP_ID"));
                    paymentOrderBean.setNationalityBean(nationalityBean);

                    BankBean bankBean = new BankBean();
                    bankBean.setId(rs.getLong("SGR_GSK_ID"));
                    paymentOrderBean.setBankBean(bankBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_REMARKS"));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                distributionsPaymentOrderBean.setProjectBean(projectBean);

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);

            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }

    public void doValidatePaumentOrder(PaymentOrderBean paymentOrderBean, LoginBean loginBean) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.validate_po(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, paymentOrderBean.getPaymentOrderNumber());
            callableStatement.setString(3, paymentOrderBean.getPaymentOrderDateH());
            callableStatement.setString(4, "" + paymentOrderBean.getPaymentOrderYear());
            callableStatement.setString(5, paymentOrderBean.getEntityBean().getCode());
            callableStatement.setString(6, paymentOrderBean.getDepartmentBean().getCode());
            callableStatement.setString(7, paymentOrderBean.getSectionBean().getCode());
            callableStatement.setString(8, paymentOrderBean.getBeneficiaryBean().getCode());
            callableStatement.setString(9,
                                        paymentOrderBean.getIban() == null || paymentOrderBean.getIban().isEmpty() ?
                                        "" : "SA" + paymentOrderBean.getIban());
            callableStatement.setString(10, "" + paymentOrderBean.getPaymentMethod());
            callableStatement.setString(11, paymentOrderBean.getCurrencyBean().getCode());
            callableStatement.setString(12, "" + paymentOrderBean.getAmount());
            callableStatement.setString(13, paymentOrderBean.getAmountInWord());
            callableStatement.setString(14, paymentOrderBean.getExpenseBean().getCode());
            callableStatement.setString(16, paymentOrderBean.getNationalityBean().getNationalityCode());
            callableStatement.setString(17, paymentOrderBean.getExchangeOf());
            callableStatement.setString(18, "AR");
            callableStatement.setString(19, paymentOrderBean.getId() == 0 ? "1" : "0");
            callableStatement.setString(20, loginBean.getUserID());
            callableStatement.setString(21, paymentOrderBean.getBeneficaryName());
            if(loginBean.getEntityBean() == null || loginBean.getEntityBean().getId() <= 0) {
                callableStatement.setString(22, "ENTRY_MOF");    
            } else {
                callableStatement.setString(22, "ENTRY");
            }
            
            callableStatement.setString(23, paymentOrderBean.getOrg());
            callableStatement.setString(24, paymentOrderBean.getRefNumber());
            callableStatement.setString(25, paymentOrderBean.getLocationCode());
            StringBuffer distribution = new StringBuffer();
            if (paymentOrderBean.getDistributionsPoBeanList() != null &&
                paymentOrderBean.getDistributionsPoBeanList().size() > 0) {
                for (DistributionsPaymentOrderBean distributionsBean : paymentOrderBean.getDistributionsPoBeanList()) {
                    distribution.append(distributionsBean.getProjectBean().getCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getEcomomicClassificationBean().getCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getAmount());
                    distribution.append("#");
                    distribution.append(distributionsBean.getRefundCategoryCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getRefundAmount() == null ? "0.0" :
                                        distributionsBean.getRefundAmount());
                    distribution.append("#");
                    String refNumber = paymentOrderBean.getRefNumber() != null && !paymentOrderBean.getRefNumber().isEmpty() ? paymentOrderBean.getRefNumber() :  "null";
                    distribution.append(refNumber);
                    distribution.append("#");
                    String location = paymentOrderBean.getLocationCode() != null && !paymentOrderBean.getLocationCode().isEmpty() ? paymentOrderBean.getLocationCode() :  "null";
                    distribution.append(location);
                    distribution.append("#@");
                    System.out.println(distribution);
                }
                callableStatement.setString(15, distribution.toString());
            } else {
                callableStatement.setString(15, "no deac ");
            }


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            paymentOrderBean.setRemark(callableStatement.getString(1));
            if (paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()) {
                String[] parts = paymentOrderBean.getRemark().split("--");
                if (parts != null && parts.length > 0) {
                    String part = parts[0];
                    if (part == null && part.isEmpty()) {
                        paymentOrderBean.setRemarkRows(parts.length - 1);
                    } else {
                        paymentOrderBean.setRemarkRows(parts.length);
                    }
                }
            } else {
                paymentOrderBean.setRemarkRows(1);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }
    
    
    public String doValidateRefNumber(String refNumber) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.validate_letter_no(?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, refNumber);
            


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            return callableStatement.getString(1);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        
    }
    
    public String doValidateLocation(String refNumber) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.VALIDATE_LOCATION_CODE(?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, refNumber);
            


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            return callableStatement.getString(1);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        
    }
    
    public String doValidateSonCofogCode(String refNumber) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.VALIDATE_COFOG_CODE (?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, refNumber);
            


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            return callableStatement.getString(1);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        
    }
    public String validateVendorWs(String refNumber) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call pog_util.validate_vendor_contract (?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, refNumber);
            


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            return callableStatement.getString(1);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        
    }    

    public void fillDistributionsListData(List<DistributionsPaymentOrderBean> distributionsPoBeanList) throws GTechException {
        ProjectDAO projectDao = new ProjectDAO();
        EcomomicClassificationDAO ecomomicClassificationDAO = new EcomomicClassificationDAO();
        Connection connection = null;
        try {
            connection = getConnection();
            for (DistributionsPaymentOrderBean distributionsPaymentOrderBean : distributionsPoBeanList) {
                ProjectBean projectBean = new ProjectBean();
                projectBean.setCode(distributionsPaymentOrderBean.getProjectBean().getCode());
                if (distributionsPaymentOrderBean.getProjectBean().getCode() != null) {
                    projectBean.setId(Long.parseLong(distributionsPaymentOrderBean.getProjectBean().getCode()));
                }
                distributionsPaymentOrderBean.setProjectBean(projectDao.getProject(distributionsPaymentOrderBean.getProjectBean()
                                                                                   .getCode(), connection));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(ecomomicClassificationDAO.getEcomomicClassification(distributionsPaymentOrderBean.getEcomomicClassificationBean()
                                                                                                                                .getCode(),
                                                                                                                                connection));
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }

    public void fillUploadPaymentOrder(UploadPaymentOrderBean uploadPaymentOrderBean) throws GTechException {

        Connection connection = null;
        BeneficiaryDAO beneficiaryDao = new BeneficiaryDAO();
        ExpenseDAO expenseDAO = new ExpenseDAO();
        CurrencyDAO currencyDao = new CurrencyDAO();
        DepartmentDAO departmentDao = new DepartmentDAO();
        EntityDAO entityDao = new EntityDAO();
        SectionDAO sectionDao = new SectionDAO();
        ProjectDAO projectDao = new ProjectDAO();
        NationalityDao nationalityDao = new NationalityDao();

        EcomomicClassificationDAO ecomomicClassificationDAO = new EcomomicClassificationDAO();
        try {
            connection = getConnection();
            for (PaymentOrderBean paymentOrderBean : uploadPaymentOrderBean.getPaymentOrderBeanList()) {
                paymentOrderBean.setBeneficiaryBean(beneficiaryDao.getBeneficiary(paymentOrderBean.getBeneficiaryBean()
                                                                                  .getCode(), connection));
                paymentOrderBean.setCurrencyBean(currencyDao.getCurrency(paymentOrderBean.getCurrencyBean().getCode(),
                                                                         connection));
                paymentOrderBean.setDepartmentBean(departmentDao.getDepartment(paymentOrderBean.getDepartmentBean()
                                                                               .getCode(), connection));
                paymentOrderBean.setEntityBean(entityDao.getEntity(paymentOrderBean.getEntityBean().getCode(),
                                                                   connection));
                paymentOrderBean.setSectionBean(sectionDao.getSection(paymentOrderBean.getSectionBean().getCode(),
                                                                      connection));
                paymentOrderBean.setExpenseBean(expenseDAO.getExpense(paymentOrderBean.getExpenseBean().getCode(),
                                                                      connection));
                paymentOrderBean.setNationalityBean(nationalityDao.getNationality(paymentOrderBean.getNationalityBean()
                                                                                  .getNationalityCode(), connection));
                for (DistributionsPaymentOrderBean distributionsPaymentOrderBean :
                     paymentOrderBean.getDistributionsPoBeanList()) {
                    distributionsPaymentOrderBean.setProjectBean(projectDao.getProject(distributionsPaymentOrderBean.getProjectBean()
                                                                                       .getCode(), connection));
                    distributionsPaymentOrderBean.setEcomomicClassificationBean(ecomomicClassificationDAO.getEcomomicClassification(distributionsPaymentOrderBean.getEcomomicClassificationBean()
                                                                                                                                    .getCode(),
                                                                                                                                    connection));
                }
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }


    public PaymentOrderBean getFullDataPaymentOrder(Long paymentOrderId) throws GTechException {
        LookupDao lookupDao = new LookupDao();

        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        ActionLogDAO actionLogDAO = new ActionLogDAO();

        String query =
            "SELECT * " + "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_ID = ? " +
            "   AND POG.ENTITY_CODE = EEO_CODE " + "   AND POG.DEPT_CODE  = TTG_CODE " +
            "   AND POG.SECTION_CODE = PTN_CODE " + "   AND SON_PPR_ID IS NOT NULL " + " UNION ALL " + " SELECT * " +
            "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_ID = ? " +
            "   AND SON_PPR_ID IS  NULL ";
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, String.valueOf(paymentOrderId));
            statment.setString(2, String.valueOf(paymentOrderId));
            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    entityBean.setCode(rs.getString("EEO_CODE"));
                    entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                    entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    departmentBean.setCode(rs.getString("TTG_CODE"));
                    departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                    departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    sectionBean.setCode(rs.getString("PTN_CODE"));
                    sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                    sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    beneficiaryBean.setCode(rs.getString("EII_CODE"));
                    beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                    beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);


                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    currencyBean.setCode(rs.getString("CRN_CODE"));
                    currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                    currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setId(rs.getLong("SGR_BENEFICIARY_IIP_ID"));
                    nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                    nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                    nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                    paymentOrderBean.setNationalityBean(nationalityBean);


                    ExpenseBean expenseBean = new ExpenseBean();
                    expenseBean.setId(rs.getLong("SGR_EPP_ID"));
                    expenseBean.setCode(rs.getString("EPP_CODE"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                    expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                    paymentOrderBean.setExpenseBean(expenseBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setBankBean(new BankDAO()
                                                 .getBank(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                                          iban.replace("SA", "") : iban, connection));
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setExchangeOf(rs.getString("SGR_IN_EXCHANGE_OF"));
                    paymentOrderBean.setActionLogBean(actionLogDAO.getAuditActionLog(paymentOrderBean.getId()));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                projectBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                distributionsPaymentOrderBean.setProjectBean(projectBean);

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                LookupBean lookupBean =
                    lookupDao.getLookupProjectValueAutoFilled(projectBean.getCode(),
                                                              paymentOrderBean.getEntityBean().getCode(),
                                                              paymentOrderBean.getSectionBean().getCode(),
                                                              paymentOrderBean.getDepartmentBean().getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean != null ? lookupBean.getNameAr() : "");


                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);

            }

        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }


    public PaymentOrderBean getFullDataPaymentOrderByPOID(String paymentOrderId) throws GTechException {
        LookupDao lookupDao = new LookupDao();
        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        String query =
            "SELECT *" + "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_ID = ? " +
            "   AND POG.ENTITY_CODE = EEO_CODE " + "   AND POG.DEPT_CODE  = TTG_CODE " +
            "   AND POG.SECTION_CODE = PTN_CODE " + "   AND SON_PPR_ID IS NOT NULL " + " UNION ALL " + " SELECT * " +
            "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_ID = ? " +
            "   AND SON_PPR_ID IS  NULL ";
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, paymentOrderId);
            statment.setString(2, paymentOrderId);
            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    entityBean.setCode(rs.getString("EEO_CODE"));
                    entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                    entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    departmentBean.setCode(rs.getString("TTG_CODE"));
                    departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                    departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    sectionBean.setCode(rs.getString("PTN_CODE"));
                    sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                    sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    beneficiaryBean.setCode(rs.getString("EII_CODE"));
                    beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                    beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);

                    paymentOrderBean.setOrgId(rs.getLong("SGR_ORG_ID"));
                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    currencyBean.setCode(rs.getString("CRN_CODE"));
                    currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                    currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setNationalityID(rs.getString("SGR_BENEFICIARY_IIP_ID"));
                    nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                    nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                    nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                    paymentOrderBean.setNationalityBean(nationalityBean);

                    ExpenseBean expenseBean = new ExpenseBean();
                    expenseBean.setId(rs.getLong("SGR_EPP_ID"));
                    expenseBean.setCode(rs.getString("EPP_CODE"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                    expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                    paymentOrderBean.setExpenseBean(expenseBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setBankBean(new BankDAO()
                                                 .getBank(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                                          iban.replace("SA", "") : iban, connection));
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setExchangeOf(rs.getString("SGR_IN_EXCHANGE_OF"));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                projectBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                distributionsPaymentOrderBean.setProjectBean(projectBean);

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                LookupBean lookupBean =
                    lookupDao.getLookupProjectValueAutoFilled(projectBean.getCode(),
                                                              paymentOrderBean.getEntityBean().getCode(),
                                                              paymentOrderBean.getSectionBean().getCode(),
                                                              paymentOrderBean.getDepartmentBean().getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean != null ? lookupBean.getNameAr() : "");
                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);
            }

        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }

    public PaymentOrderBean getFullDataPaymentOrderByPONumber(String paymentOrderNumber) throws GTechException {
        LookupDao lookupDao = new LookupDao();

        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        String query =
            "SELECT * FROM POG_PAYMENT_ORDERS, " + "POG_SGR_LINES, " + "POG_ENTITIES, " + "POG_DEPARTMENTS, " +
            "POG_SECTION, " + "POG_BENEFICIARIES, " + "POG_CURRENCIES, " + "POG_NATIONALITIES, " + "POG_BANKS, " +
            "POG_EXPENSE_TYPES, " + "POG_PROJECTS, " + "POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE " +
            "SGR_ID = SON_SGR_ID " + "AND SGR_EEO_ID = EEO_ID " + "AND SGR_TTG_ID = TTG_ID " +
            "AND SGR_PTN_ID = PTN_ID " + "AND SGR_EII_ID = EII_ID(+) " + "AND SGR_CRN_ID = CRN_ID " +
            "AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "AND SGR_GSK_ID = GSK_ID(+) " + "AND SGR_EPP_ID = EPP_ID(+) " +
            "AND SON_PPR_ID = PPR_ID(+) " + "AND SON_SCO_ID = SCO_ID(+) " + "AND SGR_CODE = ?";
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, paymentOrderNumber);
            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    entityBean.setCode(rs.getString("EEO_CODE"));
                    entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                    entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    departmentBean.setCode(rs.getString("TTG_CODE"));
                    departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                    departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    sectionBean.setCode(rs.getString("PTN_CODE"));
                    sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                    sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    beneficiaryBean.setCode(rs.getString("EII_CODE"));
                    beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                    beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);


                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    currencyBean.setCode(rs.getString("CRN_CODE"));
                    currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                    currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setNationalityID(rs.getString("SGR_BENEFICIARY_IIP_ID"));
                    nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                    nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                    nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                    paymentOrderBean.setNationalityBean(nationalityBean);

                    ExpenseBean expenseBean = new ExpenseBean();
                    expenseBean.setId(rs.getLong("SGR_EPP_ID"));
                    expenseBean.setCode(rs.getString("EPP_CODE"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                    expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                    paymentOrderBean.setExpenseBean(expenseBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setBankBean(new BankDAO()
                                                 .getBank(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                                          iban.replace("SA", "") : iban, connection));
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setExchangeOf(rs.getString("SGR_IN_EXCHANGE_OF"));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                projectBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                distributionsPaymentOrderBean.setProjectBean(projectBean);

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                LookupBean lookupBean =
                    lookupDao.getLookupProjectValueAutoFilled(projectBean.getCode(),
                                                              paymentOrderBean.getEntityBean().getCode(),
                                                              paymentOrderBean.getSectionBean().getCode(),
                                                              paymentOrderBean.getDepartmentBean().getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean != null ? lookupBean.getNameAr() : "");


                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);

            }

        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }

    public List<PaymentOrderBean> getPaymentOrderBeanByStatusAndEntityIdAndPyamentOrder(Integer status,
                                                                                        Integer paymentOrder,
                                                                                        String date,
                                                                                        Long entityId) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<>();
        String query =
            " SELECT O.SGR_ID , O.SGR_CODE , O.SGR_DATE_HI , B.EII_NAME_AR , B.EII_NAME_EN ," +
            " X.EPP_CODE, O.SGR_LTD_MODIFIED_ON_HI ,O.SGR_PAYMENT_AMOUNT ," +
            " P.EEO_ID , P.EEO_NAME_AR , P.EEO_NAME_EN " +
            " FROM POG_PAYMENT_ORDERS O , POG_BENEFICIARIES  B , POG_EXPENSE_TYPES X , POG_ENTITIES P" +
            " WHERE O.SGR_EII_ID = B.EII_ID" + " AND   O.SGR_EPP_ID = X.EPP_ID" + " AND   O.SGR_EEO_ID = P.EEO_ID " +
            " AND   O.SGR_STATUS = ? " + " AND   O.SGR_EEO_ID   = ? " +
            " AND   NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR) <=  TO_DATE(?, 'DD/MM/YYYY') " +
            " AND   O.SGR_CODE   = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, status);
            preparedStatement.setLong(2, entityId);
            //            java.sql.Date sqlDate = new java.sql.Date(calendarDAO.getGrDateByHijriDate(connection, date).getTime());
            preparedStatement.setString(3, calendarDAO.getGrDateByHijriDate(connection, date));
            preparedStatement.setInt(4, paymentOrder);
            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                EntityBean entityBean = new EntityBean();
                entityBean.setId(rs.getLong("EEO_ID"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                paymentOrderBean.setEntityBean(entityBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBeanList.add(paymentOrderBean);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return paymentOrderBeanList;

    }

    public boolean updatePaymentOrderExpence(long paymentOrderId, long expencesId) throws GTechException {
        boolean isOk = true;
        Connection connection = getConnection();
        ;
        PreparedStatement preparedStatement = null;
        try {
            String query = "UPDATE POG_PAYMENT_ORDERS SET SGR_EPP_ID = ? WHERE SGR_ID =?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, expencesId != 0 ? String.valueOf(expencesId) : "");
            preparedStatement.setString(2, paymentOrderId != 0 ? String.valueOf(paymentOrderId) : "");
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException exception) {
            isOk = false;
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(f);
            }
            throw new GTechException(exception);
        } finally {
            closeResources(connection, preparedStatement);
        }
        return isOk;
    }

    public boolean checkIban(String enterdIban, String beneficiaryIban) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = " SELECT 1 FROM POG_BENEFICIARIES o WHERE o.EII_IBAN_CODE = ? AND O.EII_CODE = ?";

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, enterdIban);
            preparedStatement.setString(2, beneficiaryIban);
            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                return true;
            }

            return false;
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }

    }

    public PaymentOrderBean getFullDataPaymentOrderByPONumberWithoutId(String paymentOrderNumber,
                                                                       String ecoId) throws GTechException {
        LookupDao lookupDao = new LookupDao();

        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        String query =
            "SELECT * FROM POG_PAYMENT_ORDERS, " + "POG_SGR_LINES, " + "POG_ENTITIES, " + "POG_DEPARTMENTS, " +
            "POG_SECTION, " + "POG_BENEFICIARIES, " + "POG_CURRENCIES, " + "POG_NATIONALITIES, " + "POG_BANKS, " +
            "POG_EXPENSE_TYPES, " + "POG_PROJECTS, " + "POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE " +
            "SGR_ID = SON_SGR_ID " + "AND SGR_EEO_ID = EEO_ID " + "AND SGR_TTG_ID = TTG_ID " +
            "AND SGR_PTN_ID = PTN_ID " + "AND SGR_EII_ID = EII_ID(+) " + "AND SGR_CRN_ID = CRN_ID " +
            "AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "AND SGR_GSK_ID = GSK_ID(+) " + "AND SGR_EPP_ID = EPP_ID(+) " +
            "AND SON_PPR_ID = PPR_ID(+) " + "AND SON_SCO_ID = SCO_ID(+) " + "AND SGR_CODE = ?" + "AND SGR_STATUS = ?" +
            "AND SGR_EEO_ID = ?";
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, paymentOrderNumber);
            statment.setString(2, "100401");
            statment.setString(3, ecoId);

            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    entityBean.setCode(rs.getString("EEO_CODE"));
                    entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                    entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    departmentBean.setCode(rs.getString("TTG_CODE"));
                    departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                    departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    sectionBean.setCode(rs.getString("PTN_CODE"));
                    sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                    sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    beneficiaryBean.setCode(rs.getString("EII_CODE"));
                    beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                    beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);


                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    currencyBean.setCode(rs.getString("CRN_CODE"));
                    currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                    currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setNationalityID(rs.getString("SGR_BENEFICIARY_IIP_ID"));
                    nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                    nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                    nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                    paymentOrderBean.setNationalityBean(nationalityBean);

                    ExpenseBean expenseBean = new ExpenseBean();
                    expenseBean.setId(rs.getLong("SGR_EPP_ID"));
                    expenseBean.setCode(rs.getString("EPP_CODE"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                    expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                    paymentOrderBean.setExpenseBean(expenseBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setBankBean(new BankDAO()
                                                 .getBank(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                                          iban.replace("SA", "") : iban, connection));
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setExchangeOf(rs.getString("SGR_IN_EXCHANGE_OF"));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                projectBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                distributionsPaymentOrderBean.setProjectBean(projectBean);

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                LookupBean lookupBean =
                    lookupDao.getLookupProjectValueAutoFilled(projectBean.getCode(),
                                                              paymentOrderBean.getEntityBean().getCode(),
                                                              paymentOrderBean.getSectionBean().getCode(),
                                                              paymentOrderBean.getDepartmentBean().getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean != null ? lookupBean.getNameAr() : "");


                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);

            }

        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }

    public Integer getNumberOFPOBeanByStatus(Integer status, LoginBean userBean) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        String query =
            " SELECT COUNT(SGR_ID) AS COUNTS" + "  FROM POG_PAYMENT_ORDERS SGR " + "  WHERE SGR.SGR_STATUS = ? " +
            " AND SGR.SGR_EEO_ID = ?" + " AND SGR.SGR_TTG_ID = ?" + " AND SGR.SGR_PTN_ID = ? AND SGR.SGR_ORG_ID = ? ";

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, status);
            preparedStatement.setLong(2, userBean.getEntityBean().getId());
            preparedStatement.setLong(3, userBean.getDepartmentBean().getId());
            preparedStatement.setLong(4, userBean.getSectionBean().getId());
            preparedStatement.setLong(5, userBean.getOrganizationBean().getId());

            rs = preparedStatement.executeQuery();
            int i = 0;
            if (rs.next()) {
                return rs.getInt("COUNTS");

            }

            return 0;
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }

    }

    public PaymentOrderBean getFullDataPaymentOrderByNumber(String paymentOrderId) throws GTechException {
        LookupDao lookupDao = new LookupDao();

        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        ActionLogDAO actionLogDAO = new ActionLogDAO();

        String query =
            "SELECT * " + "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_CODE = ? " +
            "   AND POG.ENTITY_CODE = EEO_CODE " + "   AND POG.DEPT_CODE  = TTG_CODE " +
            "   AND POG.SECTION_CODE = PTN_CODE " + "   AND SON_PPR_ID IS NOT NULL " + " UNION ALL " + " SELECT * " +
            "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_CODE = ? " +
            "   AND SON_PPR_ID IS  NULL ";
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, paymentOrderId);
            statment.setString(2, paymentOrderId);
            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    entityBean.setCode(rs.getString("EEO_CODE"));
                    entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                    entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    departmentBean.setCode(rs.getString("TTG_CODE"));
                    departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                    departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    sectionBean.setCode(rs.getString("PTN_CODE"));
                    sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                    sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    beneficiaryBean.setCode(rs.getString("EII_CODE"));
                    beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                    beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);


                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    currencyBean.setCode(rs.getString("CRN_CODE"));
                    currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                    currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setId(rs.getLong("SGR_BENEFICIARY_IIP_ID"));
                    nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                    nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                    nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                    paymentOrderBean.setNationalityBean(nationalityBean);


                    ExpenseBean expenseBean = new ExpenseBean();
                    expenseBean.setId(rs.getLong("SGR_EPP_ID"));
                    expenseBean.setCode(rs.getString("EPP_CODE"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                    expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                    paymentOrderBean.setExpenseBean(expenseBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setBankBean(new BankDAO()
                                                 .getBank(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                                          iban.replace("SA", "") : iban, connection));
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setExchangeOf(rs.getString("SGR_IN_EXCHANGE_OF"));
                    paymentOrderBean.setActionLogBean(actionLogDAO.getAuditActionLog(paymentOrderBean.getId()));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                projectBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                distributionsPaymentOrderBean.setProjectBean(projectBean);
                if(paymentOrderBean.getRefNumber() == null) {
                    paymentOrderBean.setRefNumber(rs.getString("CASH_REF_NUMBER")); 
                }
                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                LookupBean lookupBean =
                    lookupDao.getLookupProjectValueAutoFilled(projectBean.getCode(),
                                                              paymentOrderBean.getEntityBean().getCode(),
                                                              paymentOrderBean.getSectionBean().getCode(),
                                                              paymentOrderBean.getDepartmentBean().getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean != null ? lookupBean.getNameAr() : "");


                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);

            }

        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }

    public boolean distributPaymentOrder(String userId, List<String> paymentOrderList) throws GTechException {
        PreparedStatement statment = null;
        ResultSet rs = null;
        Connection connection = getConnection();
        try {
            String query = "UPDATE POG_PAYMENT_ORDERS PPO SET PPO.SGR_ASSIGNED_USER = ? WHERE PPO.SGR_CODE IN (";

            for (int i = 0; i < paymentOrderList.size(); i++) {
                if (i != (paymentOrderList.size() - 1)) {
                    query = query + "?,";
                } else {
                    query = query + "?";
                }

            }

            query = query + ")";
            statment = connection.prepareStatement(query);

            //            Array array = connection.createArrayOf("varchar", paymentOrderList.toArray());

            statment.setLong(1, Long.parseLong(userId));
            for (int i = 0; i < paymentOrderList.size(); i++) {
                statment.setString(i + 2, paymentOrderList.get(i));

            }


            statment.executeUpdate();

            return true;
        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }

    }

    public ResponseContainer<PaymentOrderBean> getUnassignedPaymentOrderBeanList(RequestContainer<PaymentOrderSearchBean> requestContainer) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        PaymentOrderSearchBean searchPaymentOrderBean = requestContainer.getDataBean();
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<PaymentOrderBean>();
        ResponseContainer<PaymentOrderBean> responseContainer = new ResponseContainer<PaymentOrderBean>();
        String query =
            " SELECT O.SGR_ID , O.SGR_CODE , O.SGR_DATE_HI , NVL2(SGR_BENEFICIARY_NAME, SGR_BENEFICIARY_NAME, B.EII_NAME_AR) AS BENEFICIARY_NAME , B.EII_NAME_EN ," +
            " X.EPP_CODE, O.SGR_LTD_MODIFIED_ON_HI ,O.SGR_PAYMENT_AMOUNT ," +
            " P.EEO_ID , P.EEO_NAME_AR , P.EEO_NAME_EN " +
            " FROM POG_PAYMENT_ORDERS O , POG_BENEFICIARIES  B , POG_EXPENSE_TYPES X , POG_ENTITIES P" +
            " WHERE O.SGR_EII_ID = B.EII_ID(+)" + " AND   O.SGR_EPP_ID = X.EPP_ID(+)" +
            " AND   O.SGR_EEO_ID = P.EEO_ID AND O.SGR_ASSIGNED_USER IS NULL";

        try {
            connection = getConnection();
            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getPaymentOrderStatus())) {
                query += " AND   O.SGR_STATUS = " + searchPaymentOrderBean.getPaymentOrderStatus();
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getEntityID())) {
                query += " AND   O.SGR_EEO_ID = " + searchPaymentOrderBean.getEntityID();
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getFromPaymentOrderCode())) {
                query += " AND   O.SGR_CODE = '" + searchPaymentOrderBean.getFromPaymentOrderCode() + "'";
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getToDate())) {
                query +=
                    " AND  TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) <=  TO_DATE('" +
                    calendarDAO.getGrDateByHijriDate(connection, searchPaymentOrderBean.getToDate()) +
                    "', 'DD/MM/YYYY') ";

            }

            query += " ORDER BY TO_NUMBER(O.SGR_CODE) ASC ";
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("BENEFICIARY_NAME"));
                beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                EntityBean entityBean = new EntityBean();
                entityBean.setId(rs.getLong("EEO_ID"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                paymentOrderBean.setEntityBean(entityBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBeanList.add(paymentOrderBean);
            }


            responseContainer.setDataList(paymentOrderBeanList);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return responseContainer;

    }

    public ResponseContainer<PaymentOrderBean> getAssignedPaymentOrderBeanList(RequestContainer<PaymentOrderSearchBean> requestContainer) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PaymentOrderBean paymentOrderBean = null;
        PaymentOrderSearchBean searchPaymentOrderBean = requestContainer.getDataBean();
        ResultSet rs = null;
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<PaymentOrderBean>();
        ResponseContainer<PaymentOrderBean> responseContainer = new ResponseContainer<PaymentOrderBean>();
        String query =
            " SELECT O.SGR_ID , O.SGR_CODE , O.SGR_DATE_HI , NVL2(SGR_BENEFICIARY_NAME, SGR_BENEFICIARY_NAME, B.EII_NAME_AR) AS BENEFICIARY_NAME , B.EII_NAME_EN ," +
            " X.EPP_CODE, O.SGR_LTD_MODIFIED_ON_HI ,O.SGR_PAYMENT_AMOUNT ," +
            " P.EEO_ID , P.EEO_NAME_AR , P.EEO_NAME_EN,USR.SSC_NAME_AR USRNAME " +
            " FROM POG_PAYMENT_ORDERS O , POG_BENEFICIARIES  B , POG_EXPENSE_TYPES X , POG_ENTITIES P,POG_USERS USR" +
            " WHERE O.SGR_EII_ID = B.EII_ID(+)" + " AND   O.SGR_EPP_ID = X.EPP_ID(+)" +
            " AND   O.SGR_EEO_ID = P.EEO_ID AND O.SGR_ASSIGNED_USER IS NOT NULL AND USR.SSC_ID = O.SGR_ASSIGNED_USER";

        try {
            connection = getConnection();
            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getPaymentOrderStatus())) {
                query += " AND   O.SGR_STATUS = " + searchPaymentOrderBean.getPaymentOrderStatus();
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getEntityID())) {
                query += " AND   O.SGR_EEO_ID = " + searchPaymentOrderBean.getEntityID();
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getFromPaymentOrderCode())) {
                query += " AND   O.SGR_CODE = '" + searchPaymentOrderBean.getFromPaymentOrderCode() + "'";
            }

            if (StringUtilities.isNotEmpty(searchPaymentOrderBean.getToDate())) {
                query +=
                    " AND  TRUNC(NVL(O.SGR_LTD_MODIFIED_ON_GR,O.SGR_CREATED_ON_GR)) <=  TO_DATE('" +
                    calendarDAO.getGrDateByHijriDate(connection, searchPaymentOrderBean.getToDate()) +
                    "', 'DD/MM/YYYY') ";

            }


            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            int i = 0;
            while (rs.next()) {
                paymentOrderBean = new PaymentOrderBean();
                paymentOrderBean.setIndex(++i);
                paymentOrderBean.setId(rs.getLong("SGR_ID"));
                paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                beneficiaryBean.setNameAr(rs.getString("BENEFICIARY_NAME"));
                beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                EntityBean entityBean = new EntityBean();
                entityBean.setId(rs.getLong("EEO_ID"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                paymentOrderBean.setEntityBean(entityBean);
                ExpenseBean expenseBean = new ExpenseBean();
                expenseBean.setCode(rs.getString("EPP_CODE"));
                paymentOrderBean.setExpenseBean(expenseBean);
                paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                paymentOrderBean.setUpdateDate(rs.getString("SGR_LTD_MODIFIED_ON_HI"));
                paymentOrderBean.setAssignedUserName(rs.getString("USRNAME"));
                paymentOrderBeanList.add(paymentOrderBean);
            }


            responseContainer.setDataList(paymentOrderBeanList);

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return responseContainer;

    }

    public boolean unDistributPaymentOrder(List<UserBean> userList) throws GTechException {
        PreparedStatement statment = null;
        ResultSet rs = null;
        Connection connection = getConnection();
        try {
            String query =
                "UPDATE POG_PAYMENT_ORDERS PPO SET PPO.SGR_ASSIGNED_USER = null WHERE PPO.SGR_ASSIGNED_USER IN (";

            for (int i = 0; i < userList.size(); i++) {
                if (i != (userList.size() - 1)) {
                    query = query + "?,";
                } else {
                    query = query + "?";
                }

            }

            query = query + ")";
            statment = connection.prepareStatement(query);

            for (int i = 0; i < userList.size(); i++) {
                statment.setString(i + 1, userList.get(i).getUserIndex());

            }


            statment.executeUpdate();

            return true;
        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }

    }

    public void doValidatePaymentOrderService(PaymentOrderBean paymentOrderBean,
                                              LoginBean loginBean) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.validate_po_webservice(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, paymentOrderBean.getPaymentOrderNumber());
            callableStatement.setString(3, paymentOrderBean.getPaymentOrderDateH());
            callableStatement.setString(4, "" + paymentOrderBean.getPaymentOrderYear());
            callableStatement.setString(5, paymentOrderBean.getEntityBean().getCode());
            callableStatement.setString(6, paymentOrderBean.getDepartmentBean().getCode());
            callableStatement.setString(7, paymentOrderBean.getSectionBean().getCode());
            callableStatement.setString(8, paymentOrderBean.getBeneficiaryBean().getCode());
            callableStatement.setString(9,
                                        paymentOrderBean.getIban() == null || paymentOrderBean.getIban().isEmpty() ?
                                        "" : paymentOrderBean.getIban());
            callableStatement.setString(10, "" + paymentOrderBean.getPaymentMethod());
            callableStatement.setString(11, paymentOrderBean.getCurrencyBean().getCode());
            callableStatement.setString(12, "" + paymentOrderBean.getAmount());
            callableStatement.setString(13, paymentOrderBean.getAmountInWord());
            callableStatement.setString(14, paymentOrderBean.getExpenseBean().getCode());
            callableStatement.setString(16, paymentOrderBean.getNationalityBean().getNationalityCode());
            callableStatement.setString(17, paymentOrderBean.getExchangeOf());
            callableStatement.setString(18, "AR");
            callableStatement.setString(19, paymentOrderBean.getId() == 0 ? "1" : "0");
            callableStatement.setString(20, loginBean.getUserID());
            callableStatement.setString(21, paymentOrderBean.getBeneficaryName());
            callableStatement.setString(22, "ENTRY");
            callableStatement.setString(23, paymentOrderBean.getOrg());
            StringBuffer distribution = new StringBuffer();
            if (paymentOrderBean.getDistributionsPoBeanList() != null &&
                paymentOrderBean.getDistributionsPoBeanList().size() > 0) {
                for (DistributionsPaymentOrderBean distributionsBean : paymentOrderBean.getDistributionsPoBeanList()) {
                    distribution.append(distributionsBean.getProjectBean().getCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getEcomomicClassificationBean().getCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getAmount());
                    distribution.append("#");
                    distribution.append(distributionsBean.getRefundCategoryCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getRefundAmount() == null ? "0.0" :
                                        distributionsBean.getRefundAmount());
                    distribution.append("#@");
                }
                callableStatement.setString(15, distribution.toString());
            } else {
                callableStatement.setString(15, "no deac ");
            }


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            paymentOrderBean.setRemark(callableStatement.getString(1));
            if (paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()) {
                String[] parts = paymentOrderBean.getRemark().split("--");
                if (parts != null && parts.length > 0) {
                    String part = parts[0];
                    if (part == null && part.isEmpty()) {
                        paymentOrderBean.setRemarkRows(parts.length - 1);
                    } else {
                        paymentOrderBean.setRemarkRows(parts.length);
                    }
                }
            } else {
                paymentOrderBean.setRemarkRows(1);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }
/*---------------------------------------doValidateDraftPaymentOrderService*-----------------*/
    public void doValidateDraftPaymentOrderService(PaymentOrderBean paymentOrderBean,
                                              LoginBean loginBean) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.validate_po_draft_webservice(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setString(2, paymentOrderBean.getPaymentOrderNumber());
            callableStatement.setString(3, paymentOrderBean.getPaymentOrderDateH());
            callableStatement.setString(4, "" + paymentOrderBean.getPaymentOrderYear());
            callableStatement.setString(5, paymentOrderBean.getEntityBean().getCode());
            callableStatement.setString(6, paymentOrderBean.getDepartmentBean().getCode());
            callableStatement.setString(7, paymentOrderBean.getSectionBean().getCode());
            callableStatement.setString(8, paymentOrderBean.getBeneficiaryBean().getCode());
            callableStatement.setString(9,
                                        paymentOrderBean.getIban() == null || paymentOrderBean.getIban().isEmpty() ?
                                        "" : paymentOrderBean.getIban());
            callableStatement.setString(10, "" + paymentOrderBean.getPaymentMethod());
            callableStatement.setString(11, paymentOrderBean.getCurrencyBean().getCode());
            callableStatement.setString(12, "" + paymentOrderBean.getAmount());
            callableStatement.setString(13, paymentOrderBean.getAmountInWord());
            callableStatement.setString(14, paymentOrderBean.getExpenseBean().getCode());
            System.out.println(paymentOrderBean.getExpenseBean().getCode());
            callableStatement.setString(16, paymentOrderBean.getNationalityBean().getNationalityCode());
            callableStatement.setString(17, paymentOrderBean.getExchangeOf());
            callableStatement.setString(18, "AR");
            callableStatement.setString(19, paymentOrderBean.getId() == 0 ? "1" : "0");
            callableStatement.setString(20, loginBean.getUserID());
            callableStatement.setString(21, paymentOrderBean.getBeneficaryName());
            callableStatement.setString(22, "ENTRY");
            callableStatement.setString(23, paymentOrderBean.getOrg());
            StringBuffer distribution = new StringBuffer();
            if (paymentOrderBean.getDistributionsPoBeanList() != null &&
                paymentOrderBean.getDistributionsPoBeanList().size() > 0) {
                for (DistributionsPaymentOrderBean distributionsBean : paymentOrderBean.getDistributionsPoBeanList()) {
                    distribution.append(distributionsBean.getProjectBean().getCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getEcomomicClassificationBean().getCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getAmount());
                    distribution.append("#");
                    distribution.append(distributionsBean.getRefundCategoryCode());
                    distribution.append("#");
                    distribution.append(distributionsBean.getRefundAmount() == null ? "0.0" :
                                        distributionsBean.getRefundAmount());
                    distribution.append("#@");
                }
                callableStatement.setString(15, distribution.toString());
            } else {
                callableStatement.setString(15, "no deac ");
            }


            // execute getDBUSERByUserId store procedure
            callableStatement.execute();
            paymentOrderBean.setRemark(callableStatement.getString(1));
            if (paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()) {
                String[] parts = paymentOrderBean.getRemark().split("--");
                if (parts != null && parts.length > 0) {
                    String part = parts[0];
                    if (part == null && part.isEmpty()) {
                        paymentOrderBean.setRemarkRows(parts.length - 1);
                    } else {
                        paymentOrderBean.setRemarkRows(parts.length);
                    }
                }
            } else {
                paymentOrderBean.setRemarkRows(1);
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }

    public String isHaveCheckFunds(String paymentNo) throws GTechException {

        String query =
            "SELECT SGR_REMARKs " + "  FROM POG_PAYMENT_ORDERS " + " WHERE SGR_STATUS = '100410'" +
            "    AND SGR_CODE = ? ";
        PreparedStatement statment = null;
        ResultSet rs = null;
        Connection connection = null;
        try {
            connection = getConnection();
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, paymentNo);
            rs = statment.executeQuery();
            if (rs.next()) {

                return rs.getString("SGR_REMARKs");
            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return null;
    }

    public boolean deletePO(String poId, String poCode) throws GTechException {
        Connection connection = getConnection();
        ;
        PreparedStatement preparedStatement = null;
        try {
            String query = "UPDATE POG_PAYMENT_ORDERS SET SGR_CODE = ? ,SGR_STATUS = ? WHERE SGR_ID =?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, poCode + "-" + poId);
            preparedStatement.setInt(2, 100411);
            preparedStatement.setString(3, poId);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException exception) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throw new GTechException(exception);
        } finally {
            closeResources(connection, preparedStatement);
        }
        return true;
    }
    
    public boolean validateGFSCode(String gfsCode) throws GTechException {
            Connection connection = getConnection();
            ;
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            try {
                String query = "SELECT * FROM POG_PAYMENT_ORDERS WHERE SGR_GFS_CODE =?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, gfsCode);
                rs = preparedStatement.executeQuery();
                if(rs.next()){
                    return false;
                }
            } catch (SQLException exception) {
                try {
                    connection.rollback();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } finally {
                closeResources(connection, preparedStatement);
            }
            return true;
        }
        
        public String getAgencyCodeByNumber(String paymentNo) throws GTechException {

            String query =
                "         SELECT CURRENT_SUBINVENTORY_CODE\n" + 
                "           FROM APPS.MTL_SERIAL_NUMBERS_ALL_V\n" + 
                "          WHERE     current_organization_id =\n" + 
                "                       NVL (121, current_organization_id)\n" + 
                "                AND serial_number = ?";
            PreparedStatement statment = null;
            ResultSet rs = null;
            Connection connection = null;
            try {
                connection = getConnection();
                statment = connection.prepareStatement(query.toString());
                statment.setString(1, paymentNo);
                rs = statment.executeQuery();
                if (rs.next()) {

                    return rs.getString("CURRENT_SUBINVENTORY_CODE");
                }

            } catch (Exception e) {
                throw new GTechException(e.getCause());
            } finally {
                closeResources(connection, statment, rs);
            }
            return null;
        }
        
    public java.util.Date isPORecived(String poNumber) throws GTechException {
        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.isReceived(?) }";
        try {
            connection = getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.registerOutParameter(1, Types.DATE);
            callableStatement.setString(2, poNumber);
            callableStatement.executeUpdate();
            return callableStatement.getTimestamp(1);
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }
    
    public PaymentOrderBean getFullDataPaymentOrderMOF(Long paymentOrderId,LoginBean userBean) throws GTechException {
        LookupDao lookupDao = new LookupDao();

        PaymentOrderBean paymentOrderBean = null;
        PreparedStatement statment = null;
        ResultSet rs = null;
        ActionLogDAO actionLogDAO = new ActionLogDAO();

        String query =
            "SELECT * " + "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_CODE = ? " +
            "   AND POG.ENTITY_CODE = EEO_CODE " + "   AND POG.DEPT_CODE  = TTG_CODE " +
            "   AND POG.SECTION_CODE = PTN_CODE " + "   AND SON_PPR_ID IS NOT NULL " + 
            "   AND (SGR_ENTRY_METHOD  = 100603 OR SGR_ENTRY_METHOD  = 100605) AND SGR_CREATED_BY_SSC_ID <> " + userBean.getUserID() +
            "   AND   SGR_STATUS = ?" +
            " UNION ALL " + " SELECT * " +
            "  FROM POG_PAYMENT_ORDERS, " + "       POG_SGR_LINES, " + "       POG_ENTITIES, " +
            "       POG_DEPARTMENTS, " + "       POG_SECTION, " + "       POG_BENEFICIARIES, " +
            "       POG_CURRENCIES, " + "       POG_NATIONALITIES, " + "       POG_EXPENSE_TYPES, " +
            "       POG_PROJECTS POG, " + "       POG_ECOMOMIC_CLASSIFICATIONS " + " WHERE SGR_ID = SON_SGR_ID(+) " +
            "   AND SGR_EEO_ID = EEO_ID " + "   AND SGR_TTG_ID = TTG_ID " + "   AND SGR_PTN_ID = PTN_ID " +
            "   AND SGR_EII_ID = EII_ID(+) " + "   AND SGR_CRN_ID = CRN_ID(+) " +
            "   AND SGR_BENEFICIARY_IIP_ID = IIP_ID(+) " + "   AND SGR_EPP_ID = EPP_ID(+) " +
            "   AND SON_PPR_ID = PPR_ID(+) " + "   AND SON_SCO_ID = SCO_ID(+) " + "   AND SGR_CODE = ? " +
            "   AND SON_PPR_ID IS  NULL AND   SGR_STATUS = ?";
        query += " AND (SGR_ENTRY_METHOD  = 100603 OR SGR_ENTRY_METHOD  = 100605) AND SGR_CREATED_BY_SSC_ID <> " + userBean.getUserID();
        Connection connection = getConnection();
        try {
            statment = connection.prepareStatement(query);
            statment.setString(1, String.valueOf(paymentOrderId));
            statment.setString(2, String.valueOf(100401));
            statment.setString(3, String.valueOf(paymentOrderId));
            statment.setString(4, String.valueOf(100401));
            
            rs = statment.executeQuery();
            while (rs.next()) {
                if (paymentOrderBean == null) {
                    paymentOrderBean = new PaymentOrderBean();
                    paymentOrderBean.setId(rs.getLong("SGR_ID"));
                    paymentOrderBean.setPaymentOrderNumber(rs.getString("SGR_CODE"));
                    paymentOrderBean.setBeneficaryName(rs.getString("SGR_BENEFICIARY_NAME"));
                    paymentOrderBean.setPaymentOrderDateG(rs.getString("SGR_DATE_GR"));
                    paymentOrderBean.setPaymentOrderDateH(rs.getString("SGR_DATE_HI"));
                    paymentOrderBean.setPaymentOrderYear(rs.getString("SGR_YEAR"));
                    EntityBean entityBean = new EntityBean();
                    entityBean.setId(rs.getLong("SGR_EEO_ID"));
                    entityBean.setCode(rs.getString("EEO_CODE"));
                    entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                    entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                    paymentOrderBean.setEntityBean(entityBean);

                    DepartmentBean departmentBean = new DepartmentBean();
                    departmentBean.setId(rs.getLong("SGR_TTG_ID"));
                    departmentBean.setCode(rs.getString("TTG_CODE"));
                    departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
                    departmentBean.setNameEn(rs.getString("TTG_NAME_EN"));
                    paymentOrderBean.setDepartmentBean(departmentBean);

                    SectionBean sectionBean = new SectionBean();
                    sectionBean.setId(rs.getLong("SGR_PTN_ID"));
                    sectionBean.setCode(rs.getString("PTN_CODE"));
                    sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
                    sectionBean.setNameEn(rs.getString("PTN_NAME_EN"));
                    paymentOrderBean.setSectionBean(sectionBean);

                    BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                    beneficiaryBean.setId(rs.getLong("SGR_EII_ID"));
                    beneficiaryBean.setCode(rs.getString("EII_CODE"));
                    beneficiaryBean.setNameAr(rs.getString("EII_NAME_AR"));
                    beneficiaryBean.setNameEn(rs.getString("EII_NAME_EN"));
                    paymentOrderBean.setBeneficiaryBean(beneficiaryBean);


                    paymentOrderBean.setPaymentMethod(rs.getLong("SGR_PAYMENT_METHOD"));

                    CurrencyBean currencyBean = new CurrencyBean();
                    currencyBean.setCurId(rs.getString("SGR_CRN_ID"));
                    currencyBean.setCode(rs.getString("CRN_CODE"));
                    currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                    currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                    paymentOrderBean.setCurrencyBean(currencyBean);

                    NationalityBean nationalityBean = new NationalityBean();
                    nationalityBean.setId(rs.getLong("SGR_BENEFICIARY_IIP_ID"));
                    nationalityBean.setNationalityCode(rs.getString("IIP_CODE"));
                    nationalityBean.setNationalityNameAr(rs.getString("IIP_NAME_AR"));
                    nationalityBean.setNationalityNameEn(rs.getString("IIP_NAME_EN"));
                    paymentOrderBean.setNationalityBean(nationalityBean);


                    ExpenseBean expenseBean = new ExpenseBean();
                    expenseBean.setId(rs.getLong("SGR_EPP_ID"));
                    expenseBean.setCode(rs.getString("EPP_CODE"));
                    expenseBean.setDescAr(rs.getString("EPP_DESC_AR"));
                    expenseBean.setDescEn(rs.getString("EPP_DESC_EN"));
                    paymentOrderBean.setExpenseBean(expenseBean);
                    String iban = rs.getString("SGR_IBAN_CODE");
                    paymentOrderBean.setIban(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                             iban.replace("SA", "") : iban);
                    paymentOrderBean.setBankBean(new BankDAO()
                                                 .getBank(iban != null && !iban.isEmpty() && iban.startsWith("SA") ?
                                                          iban.replace("SA", "") : iban, connection));
                    paymentOrderBean.setAmount(rs.getDouble("SGR_PAYMENT_AMOUNT"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setPaymentEntryMethod(rs.getInt("SGR_ENTRY_METHOD"));
                    paymentOrderBean.setPaymentOrderStatus(rs.getInt("SGR_STATUS"));
                    paymentOrderBean.setAmountInWord(rs.getString("SGR_PAYMENT_AMOUNT_TEXT"));
                    paymentOrderBean.setExchangeOf(rs.getString("SGR_IN_EXCHANGE_OF"));
                    paymentOrderBean.setActionLogBean(actionLogDAO.getAuditActionLog(paymentOrderBean.getId()));
                }

                DistributionsPaymentOrderBean distributionsPaymentOrderBean = new DistributionsPaymentOrderBean();
                distributionsPaymentOrderBean.setId(rs.getLong("SON_ID"));
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("SON_PPR_ID"));
                projectBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                if(paymentOrderBean.getRefNumber() == null) {
                    paymentOrderBean.setRefNumber(rs.getString("CASH_REF_NUMBER")); 
                }
                
                distributionsPaymentOrderBean.setProjectBean(projectBean);

                EcomomicClassificationBean classificationBean = new EcomomicClassificationBean();
                classificationBean.setId(rs.getLong("SON_SCO_ID"));
                classificationBean.setCode(rs.getString("SCO_CODE"));
                classificationBean.setNameAr(rs.getString("SCO_DESC_AR"));
                classificationBean.setNameEn(rs.getString("SCO_DESC_EN"));
                distributionsPaymentOrderBean.setEcomomicClassificationBean(classificationBean);

                distributionsPaymentOrderBean.setAmount(rs.getDouble("SON_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundAmount(rs.getDouble("SON_REFUND_AMOUNT_IN_SAR"));
                distributionsPaymentOrderBean.setRefundCategoryCode(rs.getString("SON_REFUND_CATEGORY_CODE"));

                LookupBean lookupBean =
                    lookupDao.getLookupProjectValueAutoFilled(projectBean.getCode(),
                                                              paymentOrderBean.getEntityBean().getCode(),
                                                              paymentOrderBean.getSectionBean().getCode(),
                                                              paymentOrderBean.getDepartmentBean().getCode());
                distributionsPaymentOrderBean.setProgDesc(lookupBean != null ? lookupBean.getNameAr() : "");


                paymentOrderBean.addDistributionsPoBeanList(distributionsPaymentOrderBean);

            }

        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statment, rs);
        }
        return paymentOrderBean;
    }
}
