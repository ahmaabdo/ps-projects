package com.gtech.web.paymentOrder.bean;

import com.gtech.common.base.BaseBean;
import com.gtech.web.actionLog.bean.ActionLogBean;
import com.gtech.web.bank.bean.BankBean;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import login.LoginBean;

public class PaymentOrderBean extends BaseBean {
    private String paymentOrderNumber;
    private String paymentOrderDateG;
    private String paymentOrderDateH;
    private String paymentOrderYear;
    private EntityBean entityBean;
    private DepartmentBean departmentBean;
    private SectionBean sectionBean;
    private BeneficiaryBean beneficiaryBean;
    private NationalityBean nationalityBean;
    private CurrencyBean currencyBean;
    private String iban;
    private Long paymentMethod;
    private BankBean bankBean;
    private Double amount;
    private String amountInWord;
    private ExpenseBean expenseBean;
    private String remark;
    private Integer remarkRows;
    private String updateDate;
    private Integer paymentOrderStatus;
    private Integer paymentEntryMethod;
    private String exchangeOf;
    private boolean check;
    private Integer index;
    private String beneficaryName;
    private LoginBean loginBean;
    private ActionLogBean actionLogBean;
    private String distributionsName;
    private DistributionsPaymentOrderBean distributionsPaymentOrderBean ;
    private Long orgId;
    private String org;
    private Integer paymentMethodMof;
    private List<DistributionsPaymentOrderBean> distributionsPoBeanList;
    private String assignedUserName;
    private String gfsCode;
    private String refNumber;
    private String locationCode;
    
    public void setPaymentOrderNumber(String paymentOrderNumber) {
        this.paymentOrderNumber = paymentOrderNumber;
    }

    public String getPaymentOrderNumber() {
        return paymentOrderNumber;
    }

    public void setPaymentOrderDateG(String paymentOrderDateG) {
        this.paymentOrderDateG = paymentOrderDateG;
    }

    public String getPaymentOrderDateG() {
        return paymentOrderDateG;
    }

    public void setPaymentOrderDateH(String paymentOrderDateH) {
        this.paymentOrderDateH = paymentOrderDateH;
    }

    public String getPaymentOrderDateH() {
        return paymentOrderDateH;
    }

    public void setPaymentOrderYear(String paymentOrderYear) {
        this.paymentOrderYear = paymentOrderYear;
    }

    public String getPaymentOrderYear() {
        return paymentOrderYear;
    }

    public void setEntityBean(EntityBean entityBean) {
        this.entityBean = entityBean;
    }

    public EntityBean getEntityBean() {
        return entityBean;
    }

    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }

    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }

    public void setSectionBean(SectionBean sectionBean) {
        this.sectionBean = sectionBean;
    }

    public SectionBean getSectionBean() {
        return sectionBean;
    }

    public void setBeneficiaryBean(BeneficiaryBean beneficiaryBean) {
        this.beneficiaryBean = beneficiaryBean;
    }

    public BeneficiaryBean getBeneficiaryBean() {
        return beneficiaryBean;
    }

    public void setNationalityBean(NationalityBean nationalityBean) {
        this.nationalityBean = nationalityBean;
    }

    public NationalityBean getNationalityBean() {
        return nationalityBean;
    }

    public void setCurrencyBean(CurrencyBean currencyBean) {
        this.currencyBean = currencyBean;
    }

    public CurrencyBean getCurrencyBean() {
        return currencyBean;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getIban() {
            return iban;
    }

    public void setPaymentMethod(Long paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Long getPaymentMethod() {
        return paymentMethod;
    }

    public void setBankBean(BankBean bankBean) {
        this.bankBean = bankBean;
    }

    public BankBean getBankBean() {
        return bankBean;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmountInWord(String amountInWord) {
        this.amountInWord = amountInWord;
    }

    public String getAmountInWord() {
        return amountInWord;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setPaymentOrderStatus(Integer paymentOrderStatus) {
        this.paymentOrderStatus = paymentOrderStatus;
    }

    public Integer getPaymentOrderStatus() {
        return paymentOrderStatus;
    }

    public void setPaymentEntryMethod(Integer paymentEntryMethod) {
        this.paymentEntryMethod = paymentEntryMethod;
    }

    public Integer getPaymentEntryMethod() {
        return paymentEntryMethod;
    }

    public void setDistributionsPoBeanList(List<DistributionsPaymentOrderBean> distributionsPoBeanList) {
        this.distributionsPoBeanList = distributionsPoBeanList;
    }

    public List<DistributionsPaymentOrderBean> getDistributionsPoBeanList() {
        return distributionsPoBeanList;
    }

    public void setExpenseBean(ExpenseBean expenseBean) {
        this.expenseBean = expenseBean;
    }

    public ExpenseBean getExpenseBean() {
        return expenseBean;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void addDistributionsPoBeanList(DistributionsPaymentOrderBean distributionsPoBean) {
        if (this.distributionsPoBeanList == null) {
            this.distributionsPoBeanList = new ArrayList<DistributionsPaymentOrderBean>();
        }
        this.distributionsPoBeanList.add(distributionsPoBean);
    }

    public void setExchangeOf(String exchangeOf) {
        this.exchangeOf = exchangeOf;
    }

    public String getExchangeOf() {
        return exchangeOf;
    }

    public void setRemarkRows(Integer remarkRows) {
        this.remarkRows = remarkRows;
    }

    public Integer getRemarkRows() {
        return remarkRows;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public boolean isCheck() {
        return check;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setBeneficaryName(String beneficaryName) {
        this.beneficaryName = beneficaryName;
    }

    public String getBeneficaryName() {
        return beneficaryName;
    }
    
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setActionLogBean(ActionLogBean actionLogBean) {
        this.actionLogBean = actionLogBean;
    }

    public ActionLogBean getActionLogBean() {
        return actionLogBean;
    }

    public void setDistributionsPaymentOrderBean(DistributionsPaymentOrderBean distributionsPaymentOrderBean) {
        this.distributionsPaymentOrderBean = distributionsPaymentOrderBean;
    }

    public DistributionsPaymentOrderBean getDistributionsPaymentOrderBean() {
        return distributionsPaymentOrderBean;
    }

    public void setDistributionsName(String distributionsName) {
        this.distributionsName = distributionsName;
    }

    public String getDistributionsName() {
        return distributionsName;
    }

    public void setAssignedUserName(String assignedUserName) {
        this.assignedUserName = assignedUserName;
    }

    public String getAssignedUserName() {
        return assignedUserName;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getOrg() {
        return org;
    }

    public void setPaymentMethodMof(Integer paymentMethodMof) {
        this.paymentMethodMof = paymentMethodMof;
    }

    public Integer getPaymentMethodMof() {
        return paymentMethodMof;
    }

    public void setGfsCode(String gfsCode) {
        this.gfsCode = gfsCode;
    }

    public String getGfsCode() {
        return gfsCode;
    }
    
    public static void main(String[] args) {
       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date d = sdf.parse("01/01/2018");
            System.out.println(d);
        } catch (ParseException e) {
        }
        
   }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getRefNumber() {
        return refNumber;
    }


    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationCode() {
        return locationCode;
    }


}
