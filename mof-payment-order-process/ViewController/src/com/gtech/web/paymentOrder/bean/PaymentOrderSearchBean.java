package com.gtech.web.paymentOrder.bean;

import com.gtech.common.base.BaseBean;

public class PaymentOrderSearchBean extends BaseBean{

    private String fromDate;
    private String toDate;
    private String conditionDate;
    private String fromPaymentOrderCode;
    private String toPaymentOrderCode;
    private String conditionPaymentOrderCode;
    private String entityID;
    private String paymentOrderStatus;

    public PaymentOrderSearchBean() {
        super();
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setConditionDate(String conditionDate) {
        this.conditionDate = conditionDate;
    }

    public String getConditionDate() {
        return conditionDate;
    }

    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    public String getEntityID() {
        return entityID;
    }

    public void setPaymentOrderStatus(String paymentOrderStatus) {
        this.paymentOrderStatus = paymentOrderStatus;
    }

    public String getPaymentOrderStatus() {
        return paymentOrderStatus;
    }

    public void setFromPaymentOrderCode(String fromPaymentOrderCode) {
        this.fromPaymentOrderCode = fromPaymentOrderCode;
    }

    public String getFromPaymentOrderCode() {
        return fromPaymentOrderCode;
    }

    public void setToPaymentOrderCode(String toPaymentOrderCode) {
        this.toPaymentOrderCode = toPaymentOrderCode;
    }

    public String getToPaymentOrderCode() {
        return toPaymentOrderCode;
    }

    public void setConditionPaymentOrderCode(String conditionPaymentOrderCode) {
        this.conditionPaymentOrderCode = conditionPaymentOrderCode;
    }

    public String getConditionPaymentOrderCode() {
        return conditionPaymentOrderCode;
    }
}