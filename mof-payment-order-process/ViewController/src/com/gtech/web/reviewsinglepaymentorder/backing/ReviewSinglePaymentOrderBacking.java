package com.gtech.web.reviewsinglepaymentorder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.util.StringUtilities;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.currency.business.CurrencyBusiness;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;
import com.gtech.web.reviewPaymentOrder.backing.ReviewPaymentOrderBacking;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpSession;

@ManagedBean(name = "reviewSinglePaymentOrderBacking", eager = true)
@SessionScoped
public class ReviewSinglePaymentOrderBacking extends GTechModelBacking {
    ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean = new PaymentOrderBean();
    private String remark;
    private String statusName;
    private String payMethodName;
    private String paramSelected;
    private String beneficaryCode;
    private String beneficaryName;
    private String secBeneficaryName;
    private String amount;
    private CurrencyBusiness currencyBusiness = new CurrencyBusiness();

    public String init() throws GTechException {
        rest();
        DomainBusiness domainBusiness = new DomainBusiness();
        if(paramSelected != null && !paramSelected.isEmpty()) {
            paymentOrderBean = paymentOrderBusiness.getFullDataPaymentOrder(new Long(paramSelected));
            payMethodName = domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentMethod()).getNameAr();
            statusName = domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentOrderStatus().longValue()).getNameAr();
            
            paymentOrderBean.setAmountInWord(null);
            setBeneficaryCode("");
            setBeneficaryName("");
            setAmount("");
            setSecBeneficaryName("");
        }

        return "PO-F5";
    }


    private void rest() {
        remark = null;
        statusName = null;
        payMethodName = null;
        paymentOrderBean = new PaymentOrderBean();
    }

    public ReviewSinglePaymentOrderBacking() throws GTechException {
    }

    @Override
    public String initialize() throws GTechException {
        return null;
    }

    public void validateBeneficiaryNumber(ValueChangeEvent event) throws GTechException {
        String beneficiaryCode = (String) event.getNewValue();
        ResponseContainer responseContainer = new ResponseContainer();
        setBeneficaryCode(beneficiaryCode);
        System.out.println(paymentOrderBean.getBeneficiaryBean().getCode());
        if (beneficiaryCode == null || !beneficaryCode.equals(paymentOrderBean.getBeneficiaryBean().getCode())) {
            responseContainer.addErrorMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER");
            createMessages(responseContainer);
            return;
        }
        responseContainer = paymentOrderBusiness.validateBeneficiaryNumber(beneficiaryCode);
        if (!responseContainer.containsErrorMessages()) {
            BeneficiaryBean beneficiaryBean = (BeneficiaryBean) responseContainer.getDataBean();
            setBeneficaryName(beneficiaryBean.getNameAr());
        } else {
            setBeneficaryName("");
        }
        
        if (getBeneficaryCode() == null || !getBeneficaryCode().equals(String.valueOf(paymentOrderBean.getBeneficiaryBean().getCode()))) {
            responseContainer.addErrorMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER");
        }
        
        reloadComponent("REC_NAME_FIELD");
        createMessages(responseContainer);
    }

    public void validateSecBeneficaryName(ValueChangeEvent event) throws GTechException {
            String secBeneficaryName = (String) event.getNewValue();
        setBeneficaryName(secBeneficaryName);
        ResponseContainer container = new ResponseContainer();
        if(paymentOrderBean.getBeneficaryName() != null) {
            if (secBeneficaryName == null || !secBeneficaryName.equals(paymentOrderBean.getBeneficaryName()))  {
                container.addErrorMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER");
            }       
        }
        
        createMessages(container);
    }

    public void validateAmount(ValueChangeEvent event) throws GTechException {
        String amount = (String) event.getNewValue();
        if(amount != null && !amount.isEmpty()) {
            String textAmount = callTafkeet(String.valueOf(amount), paymentOrderBean.getCurrencyBean().getCode());
            paymentOrderBean.setAmountInWord(textAmount);   
        }
        setAmount(amount);
        ResponseContainer container = new ResponseContainer();
        if (amount == null || ! (Double.parseDouble(amount) == paymentOrderBean.getAmount())) {
            container.addErrorMessage("AMOUNT_NOT_EQUALS_PAYMENT_OREDER");
            reloadComponent("AMOUNT_TEXT_FIELD");
        }
        createMessages(container);
        reloadComponent("AMOUNT_TEXT_FIELD");
    }
    

    public String callTafkeet(String amount , String currency) throws GTechException {
        return currencyBusiness.getTafkeetCurrency(amount,currency);

    }
    public void confirmPaymentOrder(ActionEvent event) throws GTechException {
        if (getBeneficaryCode() == null ||
            !getBeneficaryCode().equals(String.valueOf(paymentOrderBean.getBeneficiaryBean().getCode()))) {
            responseContainer.addErrorMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER");
        }
        if (getAmount() == null || !getAmount().equals(String.valueOf(paymentOrderBean.getAmount()))) {
            responseContainer.addErrorMessage("AMOUNT_NOT_EQUALS_PAYMENT_OREDER");
        }
        if ("999".equals(paymentOrderBean.getBeneficiaryBean().getCode()) &&
            (getSecBeneficaryName() == null ||
             !getSecBeneficaryName().equals(String.valueOf(paymentOrderBean.getAmount())))) {
            responseContainer.addErrorMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER");
        }
        if (responseContainer.containsErrorMessages()) {
            createMessages(responseContainer);
            return;
        }
        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100502);

    }
    
    
    public String confirmPaymentOrder() throws GTechException {
        
        responseContainer = new ResponseContainer();
        if (StringUtilities.isEmpty(getBeneficaryCode()) || !getBeneficaryCode().equals(String.valueOf(paymentOrderBean.getBeneficiaryBean().getCode()))) {
            responseContainer.addErrorMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER");
        }
        
        if (StringUtilities.isEmpty(getAmount())  || ! (Double.parseDouble(getAmount()) == paymentOrderBean.getAmount() )) {
            responseContainer.addErrorMessage("AMOUNT_NOT_EQUALS_PAYMENT_OREDER");
        }
        
//        if(StringUtilities.isNotEmpty(paymentOrderBean.getBeneficaryName())) {
//            if ( ( StringUtilities.isEmpty(getSecBeneficaryName())  || !getSecBeneficaryName().equals(paymentOrderBean.getBeneficaryName() ))) {
//                responseContainer.addErrorMessage("BENEFICARY_AMOUNT_NOT_EQUALS_PAYMENT_OREDER");
//            }    
//        }
        
        
        if (responseContainer.containsErrorMessages()) {
            createMessages(responseContainer);
            return null;
        }
        if(paymentOrderBean.getPaymentEntryMethod() == 100603 || paymentOrderBean.getPaymentEntryMethod() == 100605) {
            new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100505);
        } else {
            new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100502);
        }
       

        HttpSession  httpSession= (HttpSession)  FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        ReviewPaymentOrderBacking reviewPaymentOrderBacking = (ReviewPaymentOrderBacking) httpSession.getAttribute("reviewPaymentOrderBean");
        setParamSelected(null);
        return reviewPaymentOrderBacking.backFromReviewSinglePaymentOrder();  
    }
    

    public void returnPaymentOrder(ActionEvent event) throws GTechException {
        new ActionLogBusiness()
            .insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100510);
    }

    public String returnToPaymentOrderReviewPage() throws GTechException {

        return "PO-F4";
    }

    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setPayMethodName(String payMethodName) {
        this.payMethodName = payMethodName;
    }

    public String getPayMethodName() {
        return payMethodName;
    }


    public void setParamSelected(String paramSelected) {
        this.paramSelected = paramSelected;
    }

    public String getParamSelected() {
        return paramSelected;
    }


    public void setBeneficaryCode(String beneficaryCode) {
        this.beneficaryCode = beneficaryCode;
    }

    public String getBeneficaryCode() {
        return beneficaryCode;
    }

    public void setBeneficaryName(String beneficaryName) {
        this.beneficaryName = beneficaryName;
    }

    public String getBeneficaryName() {
        return beneficaryName;
    }

    public void setSecBeneficaryName(String secBeneficaryName) {
        this.secBeneficaryName = secBeneficaryName;
    }

    public String getSecBeneficaryName() {
        return secBeneficaryName;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }
}
