package com.gtech.web.registerdPaymentOrder.backing;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.report.WriteXls;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.admin.user.UserBean;
import com.gtech.web.admin.user.UserBusiness;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.business.EntityBusiness;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderSearchBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletResponse;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;
import oracle.adf.view.rich.render.ClientEvent;


@ManagedBean(name = "registerdPaymentOrderSearchBean")
@ViewScoped
public class RegisterdPaymentOrderSearchBacking extends GTechModelBacking {

    private ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String paymentOrderCode;
    private String date;
    private List<EntityBean> entityBeanList = new ArrayList<>();
    private Long entityId;
    private EntityBean entityBean = new EntityBean();
    private PaymentOrderSearchBean paymentOrderSearchBean = new PaymentOrderSearchBean();
    private List<UserBean> userBean = new ArrayList<>();
    public void init() {
        EntityBusiness entityBusiness = new EntityBusiness();
        try {
            entityBeanList = entityBusiness.getEntityList();
        } catch (GTechException e) {
        }
    }

    public void fillReviewPaymentOrder(ActionEvent event) {
        try {
            fillReviewPaymentOrder();
        } catch (GTechException e) {
        }
    }
    
    public void fillAssignedPaymentOrder(ActionEvent event) {
        try {
            fillAssignedPaymentOrder();
        } catch (GTechException e) {
        }
    }
    /**
        * doRestLookupAction
        *
        */
       public void  doRestLookupAction(){


       }



    public void fillReviewPaymentOrder() throws GTechException {


        reviewPOBeanList.clear();

        ResponseContainer<PaymentOrderBean> responseContainer = new ResponseContainer<PaymentOrderBean>();
        RequestContainer<PaymentOrderSearchBean> requestContainer = new RequestContainer<PaymentOrderSearchBean>();

        PaymentOrderSearchBean paymentOrderSearchBean = new PaymentOrderSearchBean();
        paymentOrderSearchBean.setToDate(date);
        paymentOrderSearchBean.setEntityID(String.valueOf(entityBean.getId()));
        paymentOrderSearchBean.setPaymentOrderStatus("100404");

        paymentOrderSearchBean.setFromPaymentOrderCode(getPaymentOrderCode());

        if(entityBean.getId() == 0){
            paymentOrderSearchBean.setEntityID("");
        }

        requestContainer.setDataBean(paymentOrderSearchBean);

        responseContainer = paymentOrderBusiness.getUnassignedPaymentOrderBeanList(requestContainer);

        if(responseContainer.containsErrorMessages()){
             createMessages(responseContainer);
             return;
         }

        reviewPOBeanList = responseContainer.getDataList();
//        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatusAndEntityIdAndPyamentOrder(100405,paymentOrderCode ,  date, entityBean.getId());
        if(reviewPOBeanList.isEmpty()){
            numberOfPaymentOrder = 0 ;
        }else{
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }
    
    public void fillAssignedPaymentOrder() throws GTechException {


        reviewPOBeanList.clear();

        ResponseContainer<PaymentOrderBean> responseContainer = new ResponseContainer<PaymentOrderBean>();
        RequestContainer<PaymentOrderSearchBean> requestContainer = new RequestContainer<PaymentOrderSearchBean>();

        PaymentOrderSearchBean paymentOrderSearchBean = new PaymentOrderSearchBean();
        paymentOrderSearchBean.setToDate(date);
        paymentOrderSearchBean.setEntityID(String.valueOf(entityBean.getId()));
        paymentOrderSearchBean.setPaymentOrderStatus("100404");

        paymentOrderSearchBean.setFromPaymentOrderCode(getPaymentOrderCode());

        if(entityBean.getId() == 0){
            paymentOrderSearchBean.setEntityID("");
        }

        requestContainer.setDataBean(paymentOrderSearchBean);

        responseContainer = paymentOrderBusiness.getAssignedPaymentOrderBeanList(requestContainer);

        if(responseContainer.containsErrorMessages()){
             createMessages(responseContainer);
             return;
         }

        reviewPOBeanList = responseContainer.getDataList();
    //        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatusAndEntityIdAndPyamentOrder(100405,paymentOrderCode ,  date, entityBean.getId());
        if(reviewPOBeanList.isEmpty()){
            numberOfPaymentOrder = 0 ;
        }else{
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }
    
    public void handleAutoFillEntity(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
            LookupBean lookupBean  = lookupBusiness.getLookupValueAutoFilled(entityBean.getCode(), null,"POG_ENTITIES");
            entityBean.setCode(lookupBean.getCode());
            entityBean.setNameAr(lookupBean.getNameAr());
            entityBean.setNameEn(lookupBean.getNameEn());
            entityBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }

        reloadComponent("entityDescId");
        reloadComponent("entityCodeId");
        reloadComponent("entityd");
    }




    public String doRest() throws GTechException {
        reviewPOBeanList.clear();
        date = null;
        numberOfPaymentOrder = null;
        return null;
    }

    @Override
    public String initialize() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        numberOfPaymentOrder = 0;
        date = "";
        // TODO Implement this method
        return "PO-F8";

    }
    
    public String initUn() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        paymentOrderSearchBean = new PaymentOrderSearchBean();
        numberOfPaymentOrder = 0;
        date ="";

        return "PO-F20";
    }
    public void downloadExcel() throws FileNotFoundException, IOException {
        WriteXls writeXls = new WriteXls();
        FileInputStream fileInputStream = null;

        File temp = writeXls.writeXlsRegReport(reviewPOBeanList);
        if (temp.exists()) {
            fileInputStream = new FileInputStream(temp);

        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        response.setContentLength((int) temp.length());
        response.reset();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=registeredPaymentOrder.xls;");

        org.apache.commons.io.IOUtils.copy(fileInputStream, response.getOutputStream());
        fileInputStream.close();
        facesContext.responseComplete();
    }
    
    public void distrubteOrder(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)){

            try {
                List<UserBean> selectedUserBean = new ArrayList<>();
                boolean check = false;
                for(UserBean bean:userBean) {
                    if(bean.isSelected()) {
                        selectedUserBean.add(bean);
                        check = true;
                    }
                }
                
                if(reviewPOBeanList == null || reviewPOBeanList.isEmpty()) {
                     ResponseContainer responseContainerIn = new ResponseContainer();
                     responseContainerIn.addWarningMessage("PLEASE_SEARCH_ASSIGNED_PO");
                     createMessages(responseContainerIn);
                     return;
                }
                
                if(!check) {
                     ResponseContainer responseContainerIn = new ResponseContainer();
                     responseContainerIn.addWarningMessage("PLEASE_SELECT_ONE_USER");
                     createMessages(responseContainerIn);
                     return;
                }
                
                paymentOrderBusiness.distributPaymentOrder(selectedUserBean, reviewPOBeanList);
                fillReviewPaymentOrder();
                for(UserBean bean:userBean) {
                    bean.setSelected(false);
                }
                
                reloadComponent("reviewTableId");
                reloadComponent("numberOfPaymentOrderId");
                
            } catch (GTechException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void unDistrubteOrder(DialogEvent dialogEvent) {
        if(dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)){

            try {
                List<UserBean> selectedUserBean = new ArrayList<>();
                boolean check = false;
                for(UserBean bean:userBean) {
                    if(bean.isSelected()) {
                        selectedUserBean.add(bean);
                        check = true;
                    }
                }
                
                if(reviewPOBeanList == null || reviewPOBeanList.isEmpty()) {
                     ResponseContainer responseContainerIn = new ResponseContainer();
                     responseContainerIn.addWarningMessage("PLEASE_SEARCH_ASSIGNED_PO");
                     createMessages(responseContainerIn);
                     return;
                }
                
                if(!check) {
                     ResponseContainer responseContainerIn = new ResponseContainer();
                     responseContainerIn.addWarningMessage("PLEASE_SELECT_ONE_USER");
                     createMessages(responseContainerIn);
                     return;
                }
                
                paymentOrderBusiness.unDistributPaymentOrder(selectedUserBean);
                fillAssignedPaymentOrder();
                for(UserBean bean:userBean) {
                    bean.setSelected(false);
                }
                
                reloadComponent("reviewTableId");
                reloadComponent("numberOfPaymentOrderId");
                
            } catch (GTechException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
        this.paymentOrderBusiness = paymentOrderBusiness;
    }

    public PaymentOrderBusiness getPaymentOrderBusiness() {
        return paymentOrderBusiness;
    }

    public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
        this.reviewPOBeanList = reviewPOBeanList;
    }

    public List<PaymentOrderBean> getReviewPOBeanList() {
        return reviewPOBeanList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setEntityBeanList(List<EntityBean> entityBeanList) {
        this.entityBeanList = entityBeanList;
    }

    public List<EntityBean> getEntityBeanList() {
        return entityBeanList;
    }


    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEntityId() {
        return entityId;
    }


    public void setEntityBean(EntityBean entityBean) {
        this.entityBean = entityBean;
    }

    public EntityBean getEntityBean() {
        return entityBean;
    }


    public void setPaymentOrderCode(String paymentOrderCode) {
        this.paymentOrderCode = paymentOrderCode;
    }

    public String getPaymentOrderCode() {
        return paymentOrderCode;
    }

    public void setPaymentOrderSearchBean(PaymentOrderSearchBean paymentOrderSearchBean) {
        this.paymentOrderSearchBean = paymentOrderSearchBean;
    }

    public PaymentOrderSearchBean getPaymentOrderSearchBean() {
        return paymentOrderSearchBean;
    }

    public void getAllMOFUsers(PopupFetchEvent popupFetchEvent) {
        UserBusiness userB = new UserBusiness();
        try {
            if(userBean != null) {
                userBean.clear();
            }
            userBean = userB.getAllMOFUsers();
        } catch (GTechException e) {
            e.printStackTrace();
        }
    }


    public void setUserBean(List<UserBean> userBean) {
        this.userBean = userBean;
    }

    public List<UserBean> getUserBean() {
        return userBean;
    }
}
