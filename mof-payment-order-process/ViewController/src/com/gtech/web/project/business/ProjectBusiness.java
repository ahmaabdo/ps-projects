package com.gtech.web.project.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.project.bean.ProjectBean;
import com.gtech.web.project.dao.ProjectDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

public class ProjectBusiness extends GTechModelBusiness {

    public ProjectBean getProject(String projectCode) throws GTechException, SQLException {
        ProjectDAO projectDAO = new ProjectDAO();
        return projectDAO.getProject(projectCode);
    }

    public ProjectBean getProject(String projectCode, Connection connection) throws GTechException, SQLException {
        ProjectDAO projectDAO = new ProjectDAO();
        return projectDAO.getProject(projectCode, connection);
    }

    public List<ProjectBean> getProjectsList() throws GTechException {
        ProjectDAO projectDAO = new ProjectDAO();
        return projectDAO.getProjectsList();
    }
}
