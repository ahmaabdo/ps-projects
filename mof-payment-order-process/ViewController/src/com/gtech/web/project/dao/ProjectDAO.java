package com.gtech.web.project.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.project.bean.ProjectBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ProjectDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(ProjectDAO.class);


    public ProjectBean getProject(String projectCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ProjectBean projectBean = null;
        ResultSet rs = null;

        String query = "SELECT PPR_ID FROM POG_PROJECTS  WHERE PPR_CODE = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, projectCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                projectBean = new ProjectBean();
                projectBean.setCode(projectCode);
                projectBean.setId(rs.getInt("PPR_ID"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return projectBean;

    }

    public ProjectBean getProject(String projectCode, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        ProjectBean projectBean = new ProjectBean();
        ResultSet rs = null;

        String query = "SELECT PROJECT_SHORT_CODE FROM POG_PROJECTS  WHERE PROJECT_SHORT_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            projectBean = new ProjectBean();
            projectBean.setCode(projectCode);
            projectBean.setId(rs.getInt("PROJECT_SHORT_CODE"));
        }


        return projectBean;

    }

    public List<ProjectBean> getProjectsList() throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<ProjectBean> projectsList = new ArrayList<ProjectBean>();
        String query = "SELECT PPR_ID, PPR_CODE, PPR_DESC_AR, PPR_DESC_EN FROM POG_PROJECTS";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ProjectBean projectBean = new ProjectBean();
                projectBean.setId(rs.getLong("PPR_ID"));
                projectBean.setCode(rs.getString("PPR_CODE"));
                projectBean.setNameAr(rs.getString("PPR_DESC_AR"));
                projectBean.setNameEn(rs.getString("PPR_DESC_EN"));
                projectsList.add(projectBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return projectsList;
    }

}
