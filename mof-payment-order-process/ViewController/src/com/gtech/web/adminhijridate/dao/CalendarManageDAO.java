package com.gtech.web.adminhijridate.dao;

import com.gtech.common.exception.GTechException;
import com.gtech.common.util.DateUtilities;
import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.dao.GTechDAO;
import com.gtech.faces.util.LabelReader;

import com.gtech.web.adminhijridate.bean.CalendarManageBean;

import com.gtech.web.adminhijridate.bean.CalendarProfileBean;
import com.gtech.web.adminhijridate.bean.DayBean;
import com.gtech.web.adminhijridate.bean.HolidayBean;
import com.gtech.web.adminhijridate.bean.MonthManageBean;

import com.gtech.web.adminhijridate.bean.YearBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CalendarManageDAO extends GTechDAO {
  
  public CalendarManageDAO() throws GTechException{
   
  }
  private static Log log = LogFactory.getLog(CalendarManageDAO.class);

  public ResponseContainer<CalendarManageBean> getAllMonthsOfYear(RequestContainer<CalendarManageBean> request) throws GTechException {

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      String query = "";
      CalendarManageBean calendarManageBean = request.getDataBean();

      ResponseContainer<CalendarManageBean> responseDataContainer = new ResponseContainer<CalendarManageBean>();

      try {

          log.debug("Start Of Get Months of the year");

          connection = getConnection();

          query = "  SELECT HYEAR , " +
                    " HMONTH , " +
                    " NOOFDAYS , " +
                    " GSTARTDATE , " +
  //                     " FULLNAME_AR , " +
                    " LASTGENRATEDDATETIME " +
                    " FROM ZACALENDARPROFILE " +
  //                      ",ZAUSER " +
                    " where HYEAR = "+calendarManageBean.getYear();


          stmt = connection.createStatement();
          rs = stmt.executeQuery(query);

          List<MonthManageBean> monthsList = new ArrayList<MonthManageBean>();

          while (rs.next()) {
              MonthManageBean monthManageBean = new MonthManageBean();
              monthManageBean.setMonthNumber(rs.getString("HMONTH"));
              monthManageBean.setNumberOfDays(rs.getString("NOOFDAYS"));
              monthManageBean.setMonthName(LabelReader.getLabel("HIJRI_MONTH_" +
                      Integer.parseInt(monthManageBean.getMonthNumber())));
              monthManageBean.setStartDate(DateUtilities.formatDate(rs.getString("GSTARTDATE")));

              monthsList.add(monthManageBean);

  //                calendarManageBean.setUserName(rs.getString("FULLNAME_AR"));
              calendarManageBean.setDateTimeModified(DateUtilities.formatDateTime(rs.getString("LASTGENRATEDDATETIME")));
          }

          calendarManageBean.setMonthsList(monthsList);

          responseDataContainer.setDataBean(calendarManageBean);
      } catch (SQLException e) {
          log.error("Exception :", e);
          throw new GTechException(e);
      } finally {
          closeResources(connection, stmt, rs);
      }
      return responseDataContainer;

  }

 
  public ResponseContainer<YearBean> getAllHijriYearsProfile() throws GTechException {

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      String query = "";
      List<YearBean> yearsList = new ArrayList<YearBean>();

      ResponseContainer<YearBean> responseDataContainer = new ResponseContainer<YearBean>();

      try {

          log.debug("Start Of Get Root Unit");

          connection = getConnection();

          query = " select Distinct HYEAR from ZACALENDARPROFILE ORDER BY HYEAR";

          

          stmt = connection.createStatement();
          rs = stmt.executeQuery(query);

          while (rs.next()) {
              YearBean yearBean = new YearBean();
              yearBean.setYear(rs.getString("HYEAR"));
              yearsList.add(yearBean);
          }

          responseDataContainer.setDataList(yearsList);
      } catch (SQLException e) {
          log.error("Exception :", e);
          throw new GTechException(e);
      } finally {
          closeResources(connection, stmt, rs);
      }
      return responseDataContainer;

  } 

 
 
  public ResponseContainer<CalendarManageBean> getAllWeekEndHolidy(RequestContainer<CalendarManageBean> request) throws GTechException {

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      String query = "";
      CalendarManageBean calendarManageBean = request.getDataBean();

      ResponseContainer<CalendarManageBean> responseDataContainer = new ResponseContainer<CalendarManageBean>();

      try {

          log.debug("Start Of Get Weeks End Holidy of the year");

          connection = getConnection();

          query = " select distinct to_char(to_date(GDATE, 'dd/mm/yyyy'), 'D') as HolidyDay from ZADATECONVERSION where hdate like '%/"+calendarManageBean.getYear()+"%'" +
              "and WEEKENDFLAG = 1 order by 1";

          

         
          stmt = connection.createStatement();
          rs = stmt.executeQuery(query);

          List<String> daysWeekEndList = new ArrayList<String>();

          while (rs.next()) {
              int dayNumberOfWeek = rs.getInt("HolidyDay")+1;
              if(dayNumberOfWeek>7)dayNumberOfWeek = 1;

              daysWeekEndList.add(String.valueOf(dayNumberOfWeek));
          }

          calendarManageBean.setWeekEndDays(daysWeekEndList);

          responseDataContainer.setDataBean(calendarManageBean);
      } catch (SQLException e) {
          log.error("Exception :", e);
          throw new GTechException(e);
      } finally {
          closeResources(connection, stmt, rs);
      }
      return responseDataContainer;

  }

  public ResponseContainer<CalendarManageBean> getAllHolidaysOfYear(RequestContainer<CalendarManageBean> request) throws GTechException {

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      String query = "";
      CalendarManageBean calendarManageBean = request.getDataBean();

      ResponseContainer<CalendarManageBean> responseDataContainer = new ResponseContainer<CalendarManageBean>();

      try {

          log.debug("Start Of Get get All Holidays of the year");

          connection = getConnection();

          query = " select GDATE,HDATE , WEEKENDFLAG from ZADATECONVERSION where HDATE like '%/"+calendarManageBean.getYear()+"%' and WEEKENDFLAG = 0 ";
 
          stmt = connection.createStatement();
          rs = stmt.executeQuery(query + " order by to_date(GDATE,'dd/mm/yyyy') ");

          List<DayBean> holidaysOfYear = new ArrayList<DayBean>();

          while (rs.next()) {
              DayBean dayBean = new DayBean();
              dayBean.setHijriDate(rs.getString("HDATE"));
              dayBean.setCenturyADDate(rs.getString("GDATE"));
              dayBean.setHoliDayName(rs.getString("WEEKENDFLAG"));

              holidaysOfYear.add(dayBean);


          }

          calendarManageBean.setHolidaysDays(holidaysOfYear);

          responseDataContainer.setDataBean(calendarManageBean);
      } catch (SQLException e) {
          log.error("Exception :", e);
          throw new GTechException(e);
      } finally {
          closeResources(connection, stmt, rs);
      }
      return responseDataContainer;

  }
  
  public ResponseContainer<CalendarManageBean> getAllEventsOfYear(RequestContainer<CalendarManageBean> request) throws GTechException {

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      String query = "";
      CalendarManageBean calendarManageBean = new CalendarManageBean();

      ResponseContainer<CalendarManageBean> responseDataContainer = new ResponseContainer<CalendarManageBean>();

      try {

          log.debug("Start Of Get get All Holidays of the year");

          connection = getConnection();

          query = " SELECT DATEID ," +
                  " (SELECT HDATE FROM ZADATECONVERSION WHERE A.DATEID = ID ) AS HDATE ," +
                  " (SELECT GDATE FROM ZADATECONVERSION WHERE A.DATEID = ID ) AS GDATE ," +
                  " TITLE_AR ," +
                  " TITLE_EN ," +
                  " SPACEIID " +
                  " FROM EVENTDATES A where SPACEIID = '"+request.getDataItem()+"'";

              
          rs = stmt.executeQuery(query+ " order by to_date(GDATE,'dd/mm/yyyy') ");

          List<DayBean> holidaysOfYear = new ArrayList<DayBean>();

          while (rs.next()) {
              DayBean dayBean = new DayBean();
              dayBean.setDateID(rs.getString("DATEID"));
              dayBean.setSpaceName(rs.getString("SPACEIID"));
              dayBean.setHijriDate(rs.getString("HDATE"));
              dayBean.setCenturyADDate(rs.getString("GDATE"));
              dayBean.setHoliDayName(rs.getString("TITLE_AR"));

              holidaysOfYear.add(dayBean);


          }

          calendarManageBean.setHolidaysDays(holidaysOfYear);

          responseDataContainer.setDataBean(calendarManageBean);
      } catch (SQLException e) {
          log.error("Exception :", e);
          throw new GTechException(e);
      } finally {
          closeResources(connection, stmt, rs);
      }
      return responseDataContainer;

  }

 

   
  private List<String> getCalenderIDsBetweenToAndFromHolidays(RequestContainer<HolidayBean> request) throws GTechException {
      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      String query = "";
      HolidayBean holidayBean = request.getDataBean();
      List<String> listOfDaysID = new ArrayList<String>();

      try {

          log.debug("Start Of Get Months of the year");

          connection = getConnection();

          query = " SELECT ID FROM ZADATECONVERSION "+
                  " WHERE TO_DATE(GDATE, 'dd/mm/yyyy') "+
                  " BETWEEN TO_DATE('"+holidayBean.getFromDateHolidayM()+"', 'dd/mm/yyyy') "+
                  " AND TO_DATE('"+holidayBean.getToDateHolidayM()+"', 'dd/mm/yyyy') ";
          
            
              stmt = connection.createStatement();
              rs = stmt.executeQuery(query);

              while (rs.next()) {
                  listOfDaysID.add(rs.getString("ID"));
              }


              } catch (SQLException e) {
                  log.error("Exception :", e);
                  throw new GTechException(e);
              } finally {
                  closeResources(connection, stmt, rs);
              }
      
              return listOfDaysID;
  }
  
}
