/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtech.web.adminhijridate.dao;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.common.util.DateUtilities;
import com.gtech.common.util.StringUtilities;
import com.gtech.web.adminhijridate.bean.CalendarBean;
import com.gtech.web.adminhijridate.bean.DayBean;
import com.gtech.web.adminhijridate.bean.YearBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tqasem
 */
public class CalendarDAO extends GTechDAO {



    public CalendarDAO() throws GTechException {
    }

    public ResponseContainer<YearBean> getAllHijriYears() throws GTechException {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "";
        List<YearBean> yearsList = new ArrayList<YearBean>();

        ResponseContainer<YearBean> responseDataContainer = new ResponseContainer<YearBean>();

        try {

            //log.debug("Start Of Get Root Unit");

            connection = getConnection();

            query = " select Distinct SUBSTR(HDATE ,7) as YEAR from ZADATECONVERSION order by YEAR ";

            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                YearBean yearBean = new YearBean();
                yearBean.setYear(rs.getString("YEAR"));
                yearsList.add(yearBean);
            }

            responseDataContainer.setDataList(yearsList);
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
        return responseDataContainer;

    }

    public ResponseContainer<YearBean> getAllCenturyADYears() throws GTechException {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "";
        List<YearBean> yearsList = new ArrayList<YearBean>();

        ResponseContainer<YearBean> responseDataContainer = new ResponseContainer<YearBean>();

        try {

            //log.debug("Start Of Get Root Unit");

            connection = getConnection();

            query = " select Distinct SUBSTR(GDATE ,7) as YEAR from ZADATECONVERSION order by YEAR";

            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                YearBean yearBean = new YearBean();
                yearBean.setYear(rs.getString("YEAR"));
                yearsList.add(yearBean);
            }

            responseDataContainer.setDataList(yearsList);
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
        return responseDataContainer;

    }

    public ResponseContainer<DayBean> getHijriDays(RequestContainer<CalendarBean> request) throws GTechException {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "";

        CalendarBean calendarBean = request.getDataBean();
        ResponseContainer<DayBean> responseDataContainer = new ResponseContainer<DayBean>();

        List<DayBean> daysList = new ArrayList<DayBean>();

        try {

            //log.debug("Start Of Get Hijri Month");

            connection = getConnection();

            query = " select HDATE , GDATE , HOLIDYNAME from ZADATECONVERSION " +
                    " where HDATE like '%/" + calendarBean.getMonthNumber() + "/" + calendarBean.getYear() + "%' " +
                    " order by to_date(GDATE, 'dd/mm/yyyy') ";


            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                DayBean dayBean = new DayBean();
                dayBean.setHijriDate(rs.getString("HDATE"));
                dayBean.setCenturyADDate(rs.getString("GDATE"));
                dayBean.setHoliDayName(rs.getString("HOLIDYNAME"));
                daysList.add(dayBean);
            }

            responseDataContainer.setDataList(daysList);
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
        return responseDataContainer;

    }

    public ResponseContainer<DayBean> getCenturyADDays(RequestContainer<CalendarBean> request) throws GTechException {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "";

        CalendarBean calendarBean = request.getDataBean();
        ResponseContainer<DayBean> responseDataContainer = new ResponseContainer<DayBean>();

        List<DayBean> daysList = new ArrayList<DayBean>();

        try {

            //log.debug("Start Of Get CenturyAD Month");

            connection = getConnection();

            query = " select HDATE , GDATE ,HOLIDYNAME from ZADATECONVERSION " +
                    " where GDATE like '%/" + calendarBean.getMonthNumber() + "/" + calendarBean.getYear() + "%' " ;
                    //" order by to_date(GDATE, 'dd/mm/yyyy') ";


            query += " order by to_date(GDATE, 'dd/mm/yyyy') ";


            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                DayBean dayBean = new DayBean();
                dayBean.setHijriDate(rs.getString("HDATE"));
                dayBean.setCenturyADDate(rs.getString("GDATE"));
                dayBean.setHoliDayName(rs.getString("HOLIDYNAME"));
                daysList.add(dayBean);
            }

            responseDataContainer.setDataList(daysList);
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
        return responseDataContainer;

    }

    public ResponseContainer<DayBean> getDateByCenturyADDate(RequestContainer<DayBean> request) throws GTechException {

        Statement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        String query = "";
        ResponseContainer<DayBean> responceDataContainer = new ResponseContainer<DayBean>();

        DayBean dayBeanRequest = request.getDataBean();
        try {
            connection = getConnection();
            //log.debug("Start Of Get Day by CenturyAD");

            query = " SELECT HDATE , GDATE from ZADATECONVERSION where GDATE like '%"+dayBeanRequest.getCenturyADDate()+"%'" ;


            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);


            if(rs.next()){
             DayBean dayBean = new DayBean();
             dayBean.setHijriDate(rs.getString("HDATE"));
             dayBean.setCenturyADDate(rs.getString("GDATE"));

             responceDataContainer.setDataBean(dayBean);
            }
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
      return responceDataContainer;
    }

     public String getDateByCenturyADDate() throws GTechException {

        Statement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        String query = "";
        String currectHDate = "";

        DayBean dayBeanRequest = new DayBean();
        dayBeanRequest.setCenturyADDate(DateUtilities.getCurrentDate());

        try {
            connection = getConnection();
            //log.debug("Start Of Get Day by CenturyAD");

            query = " SELECT HDATE , GDATE from ZADATECONVERSION where GDATE like '%"+dayBeanRequest.getCenturyADDate()+"%'" ;

            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);


            if(rs.next()){
                currectHDate = rs.getString("HDATE");
            }
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
      return currectHDate;
    }


     public String getHijriDateTimeByCenturyADDate(String gDate) throws GTechException {
        String dateTimeFormated = "";
         if (StringUtilities.isNotEmpty(gDate)) {
           Statement stmt = null;
           Connection connection = null;
           ResultSet rs = null;
           String query = "";

           DayBean dayBeanRequest = new DayBean();
           dayBeanRequest.setCenturyADDate(gDate);

           String[] dateValues = gDate.split(" ");
           String dateSeperator = "/";

           String dateValue = dateValues[0];
           String[] dateParts = dateValue.split("-");
           dateTimeFormated = dateParts[2] + dateSeperator + dateParts[1] + dateSeperator + dateParts[0];

           try {
               connection = getConnection();
               //log.debug("Start Of Get Day by CenturyAD");

               query = " SELECT HDATE , GDATE from ZADATECONVERSION where GDATE like '%"+dateTimeFormated+"%'" ;


               stmt = connection.createStatement();
               rs = stmt.executeQuery(query);


               if(rs.next()){
                   dateTimeFormated = rs.getString("HDATE");

                   if(dateValues.length >1) {
                       String timeValue = dateValues[1];
                       String[] TimeParts = timeValue.split(":");
                       dateTimeFormated +="  "+ TimeParts[0] +":" +TimeParts[1];
                   }
               }
           } catch (SQLException e) {
               //log.error("Exception :", e);
               throw new GTechException(e);
           } finally {
               closeResources(connection, stmt, rs);
           }
        }
      return dateTimeFormated;
    }

     public String getHijriDateByCenturyADDate(String gDate) throws GTechException {
        String dateTimeFormated = "";
         if (StringUtilities.isNotEmpty(gDate)) {
           Statement stmt = null;
           Connection connection = null;
           ResultSet rs = null;
           String query = "";

           DayBean dayBeanRequest = new DayBean();
           dayBeanRequest.setCenturyADDate(gDate);

           String[] dateValues = gDate.split(" ");
           String dateSeperator = "/";

           String dateValue = dateValues[0];
           String[] dateParts = dateValue.split("-");
           dateTimeFormated = dateParts[2] + dateSeperator + dateParts[1] + dateSeperator + dateParts[0];

           try {
               connection = getConnection();
               //log.debug("Start Of Get Day by CenturyAD");

               query = " SELECT HDATE , GDATE from ZADATECONVERSION where GDATE like '%"+dateTimeFormated+"%'" ;

               stmt = connection.createStatement();
               rs = stmt.executeQuery(query );


               if(rs.next()){
                   dateTimeFormated = rs.getString("HDATE");
               }
           } catch (SQLException e) {
               //log.error("Exception :", e);
               throw new GTechException(e);
           } finally {
               closeResources(connection, stmt, rs);
           }
        }
      return dateTimeFormated;
    }

    public ResponseContainer<DayBean> getDateByHijriADDate(RequestContainer<DayBean> request) throws GTechException {

        Statement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        String query = "";
        ResponseContainer<DayBean> responceDataContainer = new ResponseContainer<DayBean>();

        DayBean dayBeanRequest = request.getDataBean();
        try {
            connection = getConnection();
            //log.debug("Start Of Get Day by CenturyAD");

            query = " SELECT HDATE , GDATE from ZADATECONVERSION where HDATE like '%"+dayBeanRequest.getHijriDate()+"%'" ;

            rs = stmt.executeQuery(query );


            if(rs.next()){
             DayBean dayBean = new DayBean();
             dayBean.setHijriDate(rs.getString("HDATE"));
             dayBean.setCenturyADDate(rs.getString("GDATE"));

             responceDataContainer.setDataBean(dayBean);
            }
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
      return responceDataContainer;
    }
     public String getDateByCenturyADDate(String gDate ) throws GTechException {

        Statement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        String query = "";
        String currectHDate = "";

        DayBean dayBeanRequest = new DayBean();
        dayBeanRequest.setCenturyADDate(gDate);
      //  dayBeanRequest.setCenturyADDate(DateUtilities.getCurrentDate());

        try {
            connection = getConnection();
            //log.debug("Start Of Get Day by CenturyAD");

            query = " SELECT HDATE , GDATE from ZADATECONVERSION where GDATE like '%"+dayBeanRequest.getCenturyADDate()+"%'" ;



            stmt = connection.createStatement();
            rs = stmt.executeQuery(query );


            if(rs.next()){
                currectHDate = rs.getString("HDATE");
            }
        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
      return currectHDate;
    }

     public int getNumberOfHolidays(String fromDate , String toDate) throws GTechException {

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "";
        int holidaysCount =0;

        try {


            //log.debug("Start Of get Number Of Holidays");
            connection = getConnection();

            query = " SELECT count(*) FROM ZADATECONVERSION " +
                    " WHERE to_date(GDATE,'dd/mm/yyyy') >  to_date('"+fromDate+"','dd/mm/yyyy') AND  to_date(GDATE,'dd/mm/yyyy') <= to_date('"+toDate+"','dd/mm/yyyy')" +
                    " AND HOLIDYNAME IS NOT NULL";



            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                holidaysCount+=rs.getInt(1);
            }

        } catch (SQLException e) {
            //log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, stmt, rs);
        }
        return holidaysCount;

    }


      public String getCurrentHijriDate() throws GTechException {
          String hijriDate = "";
          PreparedStatement preparedStatement = null;
          ResultSet rs = null;
          String query = "SELECT HDATE FROM ZADATECONVERSION WHERE GDATE=TO_CHAR(SYSDATE,'DD/MM/YYYY')";
          Connection connection = getConnection();
          try {
              preparedStatement = connection.prepareStatement(query);
              rs = preparedStatement.executeQuery();
              if (rs.next()) {
                  hijriDate = rs.getString("HDATE");
              }
          } catch (Exception e) {
              throw new GTechException(e);
          } finally {
              closeResources(connection, preparedStatement, rs);
          }
          return hijriDate;
      }

      public String getCurrentHijriDate(Connection connection) throws GTechException {
          String hijriDate = "";
          PreparedStatement preparedStatement = null;
          ResultSet rs = null;
          String query = "SELECT HDATE FROM ZADATECONVERSION WHERE GDATE=TO_CHAR(SYSDATE,'DD/MM/YYYY')";
          try {
              preparedStatement = connection.prepareStatement(query);
              rs = preparedStatement.executeQuery();
              if (rs.next()) {
                  hijriDate = rs.getString("HDATE");
              }
          } catch (Exception e) {
              throw new GTechException(e);
          } finally {
              closeResources(preparedStatement, rs);
          }
          return hijriDate;
      }

      public String getGrDateByHijriDate(String hDate) throws GTechException {
          Connection connection = null;
          String dateTimeFormated = null;
            try {
                connection = getConnection();
                //log.debug("Start Of Get Day by CenturyAD");

               dateTimeFormated = getGrDateByHijriDate( connection,  hDate);

            } catch (SQLException e) {
                //log.error("Exception :", e);
                throw new GTechException(e);
            } finally {
                closeResources(connection);
            }

       return dateTimeFormated;
      }

      public String getGrDateByHijriDate(Connection connection, String hDate) throws GTechException, SQLException {
             String grDate = null ;
             PreparedStatement preparedStatement = null;
             ResultSet rs = null;
             String query = "SELECT GDATE FROM ZADATECONVERSION WHERE HDATE = ? ";
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setString(1, hDate);
             rs = preparedStatement.executeQuery();
             if (rs.next()) {
                 grDate = rs.getString("GDATE");
             }

             return grDate;
         }
  }
