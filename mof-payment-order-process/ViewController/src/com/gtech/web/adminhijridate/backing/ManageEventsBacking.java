package com.gtech.web.adminhijridate.backing;

import com.gtech.common.exception.GTechException;
import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.adminhijridate.bean.CalendarManageBean;
import com.gtech.web.adminhijridate.bean.DayBean;
import com.gtech.web.adminhijridate.bean.HolidayBean;

import com.gtech.web.adminhijridate.business.CalendarManageBusiness;

import java.text.ParseException;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import org.apache.myfaces.trinidad.component.core.data.CoreTable;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.SelectionEvent;

public class ManageEventsBacking extends GTechModelBacking {
  private List<DayBean> selectedHoliday;
  private CalendarManageBean calendarBean;
  private boolean isDeleteHoliDayRender;
  private HolidayBean holidayBean = new HolidayBean();
  private DayBean selectedDayBean = new DayBean();
  private String spaceName;

  public ManageEventsBacking() throws GTechException {
      super();
  //        Map pageFlowScope = RequestContext.getCurrentInstance().getPageFlowScope();
  //        Object myObject = pageFlowScope.get("spaceNameDesc");
  //
  //        setSpaceName(String.valueOf(myObject));
  //
  //        CalendarManageBusiness calendarManageBusiness = new CalendarManageBusiness();
  //        RequestDataContainer<CalendarManageBean> requestCalendar = new RequestDataContainer<CalendarManageBean>();
  //        requestCalendar.setDataBean(calendarBean);
  //        requestCalendar.setDataItem(getSpaceName());
  //
  //        setCalendarBean(calendarManageBusiness.getAllEventsOfYear(requestCalendar).getDataBean());
  }
  
  
  public String initialize() throws GTechException {
      
      setHolidayBean(new HolidayBean());
      
      Map pageFlowScope = RequestContext.getCurrentInstance().getPageFlowScope();
      Object myObject = pageFlowScope.get("spaceNameDesc");
      
      setSpaceName(String.valueOf(myObject));    
      
      CalendarManageBusiness calendarManageBusiness = new CalendarManageBusiness();
      RequestContainer<CalendarManageBean> requestCalendar = new RequestContainer<CalendarManageBean>();
      requestCalendar.setDataBean(calendarBean);
      requestCalendar.setDataItem(getSpaceName());
      
      setCalendarBean(calendarManageBusiness.getAllEventsOfYear(requestCalendar).getDataBean());
      
      return null;
  }

 

  public String tableSelectionListener(SelectionEvent event) {
      setIsDeleteHoliDayRender(true);
      
      Iterator iter = event.getAddedSet().iterator();
      DayBean dayBean = null;
      while(iter.hasNext()){
          setSelectedDayBean(getCalendarBean().getHolidaysDays().get((Integer)iter.next()));
      }
      
      return null;
  }

  private CoreTable getHolidayTable(String tableName) {
      UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
      CoreTable myTable = (CoreTable) vr.findComponent(tableName);

      return myTable;
  }

  public CalendarManageBean getCalendarBean() {
      return calendarBean;
  }

  public void setCalendarBean(CalendarManageBean calendarBean) {
      this.calendarBean = calendarBean;
  }

  public boolean isIsDeleteHoliDayRender() {
      return isDeleteHoliDayRender;
  }

  public void setIsDeleteHoliDayRender(boolean isDeleteHoliDayRender) {
      this.isDeleteHoliDayRender = isDeleteHoliDayRender;
  }

  public HolidayBean getHolidayBean() {
      return holidayBean;
  }

  public void setHolidayBean(HolidayBean holidayBean) {
      this.holidayBean = holidayBean;
  }


  public void setSelectedDayBean(DayBean selectedDayBean) {
      this.selectedDayBean = selectedDayBean;
  }

  public DayBean getSelectedDayBean() {
      return selectedDayBean;
  }

  public void setSpaceName(String spaceName) {
      this.spaceName = spaceName;
  }

  public String getSpaceName() {
      return spaceName;
  }
}
