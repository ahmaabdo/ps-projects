package com.gtech.web.adminhijridate.backing;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.reader.CommonConfigReader;
import com.gtech.faces.util.LabelReader;
import com.gtech.web.adminhijridate.bean.CalendarBean;
import com.gtech.web.adminhijridate.bean.CurrentHijriMeladiDate;
import com.gtech.web.adminhijridate.bean.DayBean;
import com.gtech.web.adminhijridate.bean.HolidayBean;
import com.gtech.web.adminhijridate.bean.YearBean;
import com.gtech.web.adminhijridate.business.CalendarBusiness;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.event.PopupFetchEvent;

import org.apache.myfaces.trinidad.event.ReturnEvent;

public class CalenderApplicationScopeBacking extends GTechModelBacking implements ServletContextListener{
  
  private static List<SelectItem> yearsHijriList;
  private static List<SelectItem> yearsCenturyADList;
  private static CalendarBean calendarBean;
  private static DayBean dayBean;
  private static List<SelectItem> monthsHijriList;
  private static List<SelectItem> monthsCenturyADList;
  private static boolean calendarHijriShowed;
  private List<String> listOfEvents = new ArrayList<String>();

  private String inputId;
  private String rowIndex;
  private String columnIndex;
  private String hijriCurrentDate;
  private String currentDate;
  private static Map<String,CalendarBean>  calenderDataOfAllYear ;
  private static List<String> listOfHijriYear = new ArrayList<String>(); 

  private static CurrentHijriMeladiDate currentHijriMeladiDate =
      new CurrentHijriMeladiDate();

  @Override
  public String initialize() throws GTechException {
      throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public  void contextInitialized(ServletContextEvent event) {

         try {
            CalenderApplicationScopeBacking.populateCalenderApplicationScopeBacking();
         } catch (Exception ex) {
             ex.printStackTrace();
         }
     }
  
  public void contextDestroyed(ServletContextEvent servletContextEvent) {
  }

  public static void populateCalenderApplicationScopeBacking() throws GTechException {
      calenderDataOfAllYear = new HashMap<String,CalendarBean>();
      dayBean = new DayBean();
      calendarBean = new CalendarBean();

      calendarHijriShowed= true;
      DayBean dayBeanRequest = new DayBean();
      dayBeanRequest.setCenturyADDate(getCurrentsDate());

      RequestContainer<DayBean> request =
          new RequestContainer<DayBean>();
      request.setDataBean(dayBeanRequest);

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      ResponseContainer<DayBean> response =
          calendarBusiness.getHijriDate(request);
      dayBean = response.getDataBean();

      calendarBean.setYear((dayBean != null ?
                            dayBean.getHijriDate().split("/")[2] : ""));
      calendarBean.setMonthNumber((dayBean != null ?
                                   dayBean.getHijriDate().split("/")[1] :
                                   ""));


      setListOfHijriYear(new ArrayList<String>());
      populateHijriYears();
      String month = "";
      CalendarBean calendarBean = new CalendarBean();
      setCalenderDataOfAllYear(new HashMap<String,CalendarBean>());
      
      for(String year : getListOfHijriYear()){
          for(int i=1 ; i<=12 ; i++){
              month = ( i < 10 ? "0"+String.valueOf(i) : String.valueOf(i));
              calendarBean = new CalendarBean();
              calendarBean.setMonthNumber(month);    
              calendarBean.setYear(year);
              
              getCalenderDataOfAllYear().put(year+month, fllHijriCalendar(calendarBean));
          }
      }
      
      setCalendarBean(getCalenderDataOfAllYear().get(calendarBean.getYear() + calendarBean.getMonthNumber()));
  }

  public String getHijriCurrentDate() throws GTechException {
      DayBean dayBean = getHijriDateFromMeladi(getCurrentDate());

      return dayBean.getHijriDate();
  }

  

  private DayBean getHijriDateFromMeladi(String date) throws GTechException {
      DayBean dayBeanRequest = new DayBean();
      dayBeanRequest.setCenturyADDate(date);

      RequestContainer<DayBean> request =
          new RequestContainer<DayBean>();
      request.setDataBean(dayBeanRequest);

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      ResponseContainer<DayBean> response =
          calendarBusiness.getHijriDate(request);
      DayBean dayBean = response.getDataBean();


      return dayBean;
  }

  private DayBean getMeladiDateFromHijri(String date) throws GTechException {
      DayBean dayBeanRequest = new DayBean();
      dayBeanRequest.setCenturyADDate(date);

      RequestContainer<DayBean> request =
          new RequestContainer<DayBean>();
      request.setDataBean(dayBeanRequest);

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      ResponseContainer<DayBean> response =
          calendarBusiness.getHijriDate(request);
      DayBean dayBean = response.getDataBean();


      return dayBean;
  }

  public void setHijriCurrentDate(String hijriCurrentDate) {
      this.hijriCurrentDate = hijriCurrentDate;
  }

  public void setCurrentDate(String currentDate) {
      this.currentDate = currentDate;
  }

  public static String getCurrentsDate() {
      Calendar cal = Calendar.getInstance();

      String currentDate =
          (cal.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + cal.get(Calendar.DAY_OF_MONTH) :
           cal.get(Calendar.DAY_OF_MONTH)) + "/";
      currentDate +=
              ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" + (cal.get(Calendar.MONTH) +
                                                      1) :
               (cal.get(Calendar.MONTH) + 1)) + "/";
      currentDate += cal.get(Calendar.YEAR);

      return currentDate;
  }

  private void fllHijriCalendar() throws GTechException {

      //calendarBean.setMonthBean(new MonthBean());

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      RequestContainer<CalendarBean> request = new RequestContainer<CalendarBean>();
      request.setDataBean(calendarBean);

      calendarBean =
              calendarBusiness.getCalendarHijri(request).getDataBean();
  }
  
  private static CalendarBean fllHijriCalendar(CalendarBean calendarBean1) throws GTechException {

      //calendarBean.setMonthBean(new MonthBean());

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      RequestContainer<CalendarBean> request = new RequestContainer<CalendarBean>();
      request.setDataBean(calendarBean1);

      return calendarBusiness.getCalendarHijri(request).getDataBean();
  }

  private static void populateHijriYears() throws GTechException {
      yearsHijriList = new ArrayList<SelectItem>();
      setListOfHijriYear(new ArrayList<String>());
      
      CalendarBusiness calendarBusiness = new CalendarBusiness();
      ResponseContainer<YearBean> responseDataContainer =
          calendarBusiness.getAllHihriYears();
      

      for (YearBean yearBean : responseDataContainer.getDataList()) {
          SelectItem yearItem = new SelectItem();
          yearItem.setLabel(yearBean.getYear());
          yearItem.setValue(yearBean.getYear());
          
          getListOfHijriYear().add(yearBean.getYear());
          yearsHijriList.add(yearItem);
      }
  }

  private void populateCenturyADYears() throws GTechException {
      yearsCenturyADList = new ArrayList<SelectItem>();

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      ResponseContainer<YearBean> responseDataContainer =
          calendarBusiness.getAllCenturyADYears();

      for (YearBean yearBean : responseDataContainer.getDataList()) {
          SelectItem yearItem = new SelectItem();
          yearItem.setLabel(yearBean.getYear());
          yearItem.setValue(yearBean.getYear());

          yearsCenturyADList.add(yearItem);
      }
  }

  private void fllCenturyADCalendar() throws GTechException {
      //        calendarBean = new CalendarBean();
      //        calendarBean.setYear("2006");
      //        calendarBean.setMonthNumber("02");

      CalendarBusiness calendarBusiness = new CalendarBusiness();
      RequestContainer<CalendarBean> request =
          new RequestContainer<CalendarBean>();
      request.setDataBean(calendarBean);

      calendarBean =
              calendarBusiness.getCalendarCenturyAD(request).getDataBean();
  }

  private void fillCenturyADMonths() {

      monthsCenturyADList = new ArrayList<SelectItem>();
      for (int i = 1; i <= 12; i++) {

          SelectItem monthItem = new SelectItem();
          monthItem.setLabel(LabelReader.getLabel("CENTURYAD_MONTH_" + i));

          String monthValue = (i < 10 ? "0" + i : String.valueOf(i));
          monthItem.setValue(monthValue);

          monthsCenturyADList.add(monthItem);
      }
  }

  private boolean checkIfYearExist(String year) {
      boolean result = false;
      for (SelectItem si : getYearsHijriList()) {
          if (si.getLabel().equals(year)) {
              result = true;
              break;
          } else {
              continue;
          }
      }
      return result;
  }

  public void getNextMonthListener(ActionEvent event) throws GTechException {
      if (calendarBean.getYear() == null ||
          calendarBean.getYear().equals("")) {
          return;
      }

      if (calendarBean.getMonthNumber().equals("12")) {
          if (!checkIfYearExist(String.valueOf(Integer.parseInt(calendarBean.getYear()) +
                                               1))) {
              return;
          }
      }

      if (calendarBean.getMonthNumber().equals("12")) {
          calendarBean.setMonthNumber("01");
          calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) +
                                              1));
      } else {
          String newMonth =
              String.valueOf(Integer.parseInt(calendarBean.getMonthNumber()) +
                             1);
          newMonth = (newMonth.length() == 2 ? newMonth : "0" + newMonth);

          calendarBean.setMonthNumber(newMonth);
      }

      if (isCalendarHijriShowed()) {
          fllHijriCalendar();
      } else {
          fllCenturyADCalendar();
      }
  }

  public void getLastMonthListener(ActionEvent event) throws GTechException {
      if (calendarBean.getYear() == null ||
          calendarBean.getYear().equals("")) {
          return;
      }

      if (calendarBean.getMonthNumber().equals("01")) {
          if (!checkIfYearExist(String.valueOf(Integer.parseInt(calendarBean.getYear()) -
                                               1))) {
              return;
          }
      }

      if (calendarBean.getMonthNumber().equals("01")) {
          calendarBean.setMonthNumber("12");
          calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) -
                                              1));
      } else {
          String newMonth =
              String.valueOf(Integer.parseInt(calendarBean.getMonthNumber()) -
                             1);
          newMonth = (newMonth.length() == 2 ? newMonth : "0" + newMonth);

          calendarBean.setMonthNumber(newMonth);
      }

      if (isCalendarHijriShowed()) {
          fllHijriCalendar();
      } else {
          fllCenturyADCalendar();
      }
  }

  public void monthValueChangeListener(ValueChangeEvent event) throws GTechException {
      calendarBean.setMonthNumber((String)event.getNewValue());
      if (isCalendarHijriShowed()) {
          calendarBean = getCalenderDataOfAllYear().get(calendarBean.getYear() + calendarBean.getMonthNumber());
      } else {
          fllCenturyADCalendar();
      }

      // addPartialCalindarCompnent();
  }

  public void getNextYearListener(ActionEvent event) throws GTechException {
      if (calendarBean.getYear() == null ||
          calendarBean.getYear().equals("")) {
          return;
      }

      if (!checkIfYearExist(String.valueOf(Integer.parseInt(calendarBean.getYear()) +
                                           1))) {
          return;
      }

      calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) +
                                          1));

      if (isCalendarHijriShowed()) {
          fllHijriCalendar();
      } else {
          fllCenturyADCalendar();
      }
  }

  public void getLastYearListener(ActionEvent event) throws GTechException {

  }

  public String getLastYearListener() throws GTechException {
      if (calendarBean.getYear() == null ||
          calendarBean.getYear().equals("")) {
          return null;
      }

      if (!checkIfYearExist(String.valueOf(Integer.parseInt(calendarBean.getYear()) -
                                           1))) {
          return "";
      }

      calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) -
                                          1));

      if (isCalendarHijriShowed()) {
          fllHijriCalendar();
      } else {
          fllCenturyADCalendar();
      }
      return null;
  }

  public void yearValueChangeListener(ValueChangeEvent event) throws GTechException {
      calendarBean.setYear((String)event.getNewValue());
      if (isCalendarHijriShowed()) {
          calendarBean = getCalenderDataOfAllYear().get(calendarBean.getYear() + calendarBean.getMonthNumber());
      } else {
          fllCenturyADCalendar();
      }
  }

  public String closeWindow() throws GTechException {

      //        HttpSession session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true) ;
      //        VacationProsessInvokeBacking vacationProsessInvokeBacking= (VacationProsessInvokeBacking) session.getAttribute("VacationProsessInvokeBackedBean");
      //        VacationProcessInvokeBean vacationProcessInvokeBean = vacationProsessInvokeBacking.getVacationProcessInvokeBean();
      //
      //        if(dayBean == null){
      //          return null;
      //        }
      //
      //        if((columnIndex == null && rowIndex == null ) || (columnIndex.equals("") && rowIndex.equals(""))){
      //
      //          vacationProcessInvokeBean.setVacationStartDate(dayBean.getHijriDate());
      //          vacationProcessInvokeBean.setVacationStartDateM(dayBean.getCenturyADDate());
      //
      //           if(vacationProcessInvokeBean.getVacationDuration() != null){
      //               int dur;
      //               try {
      //                  dur  = Integer.parseInt(vacationProcessInvokeBean.getVacationDuration()) ;
      //               } catch(NumberFormatException nfe){
      //                 FacesContext.getCurrentInstance().addMessage(null,
      //                                                              new FacesMessage(FacesMessage.SEVERITY_ERROR,
      //                                                                               "Please Enter Number Value In Duration.",
      //                                                                               null));
      //                 return null;
      //               }
      //           SimpleDateFormat df = new SimpleDateFormat(CommonConfigReader.getValue("DEFAULT_DATE_PATTERN"));
      //           Calendar cal = Calendar.getInstance();
      //           Date date;
      //                  try {
      //                      date = df.parse(dayBean.getCenturyADDate());
      //                  } catch (ParseException e) {
      //                      throw new GTechException(e);
      //                  }
      //           cal.setTime(date);
      //
      //           RequestDataContainer<DayBean> requestDataContainer = new RequestDataContainer<DayBean>();
      //           CalendarBusiness calendarBusiness = new CalendarBusiness();
      //           for(int i=0 ; dur > i ; i++) {
      //             dayBean.setCenturyADDate(df.format(cal.getTime()));
      //             requestDataContainer.setDataBean(dayBean);
      //               if(calendarBusiness.isItHolidayDay(requestDataContainer).getDataBean() != null){
      //                 dur++;
      //               }
      //
      //             cal.add(Calendar.DAY_OF_MONTH ,1);
      //           }
      //
      //          cal.add(Calendar.DAY_OF_MONTH ,-1);
      //
      //           DayBean db = getHijriDateFromMeladi(df.format(cal.getTime()));
      //           vacationProcessInvokeBean.setVacationEndDate(db.getHijriDate());
      //
      //         vacationProsessInvokeBacking.setVacationProcessInvokeBean(vacationProcessInvokeBean);
      //         session.setAttribute("VacationProsessInvokeBackedBean", vacationProsessInvokeBacking);
      //        }
      //
      //        }else{
      //
      //          vacationProcessInvokeBean.setVacationStartDateM(dayBean.getCenturyADDate());
      //          vacationProcessInvokeBean.setVacationStartDate(dayBean.getHijriDate());
      //
      //          if(vacationProcessInvokeBean.getVacationDuration() != null){
      //            int dur;
      //            try {
      //               dur  = Integer.parseInt(vacationProcessInvokeBean.getVacationDuration()) ;
      //            } catch(NumberFormatException nfe){
      //              FacesContext.getCurrentInstance().addMessage(null,
      //                                                           new FacesMessage(FacesMessage.SEVERITY_ERROR,
      //                                                                            "Please Enter Number Value In Duration.",
      //                                                                            null));
      //              return null;
      //            }
      //            SimpleDateFormat df = new SimpleDateFormat(CommonConfigReader.getValue("DEFAULT_DATE_PATTERN"));
      //            Calendar cal = Calendar.getInstance();
      //            Date date;
      //                   try {
      //                       date = df.parse(dayBean.getCenturyADDate());
      //                   } catch (ParseException e) {
      //                       throw new GTechException(e);
      //                   }
      //            cal.setTime(date);
      //
      //
      //            RequestDataContainer<DayBean> requestDataContainer = new RequestDataContainer<DayBean>();
      //            CalendarBusiness calendarBusiness = new CalendarBusiness();
      //            for(int i=0 ; dur > i ; i++) {
      //              dayBean.setCenturyADDate(df.format(cal.getTime()));
      //              requestDataContainer.setDataBean(dayBean);
      //                if(calendarBusiness.isItHolidayDay(requestDataContainer).getDataBean() != null){
      //                  dur++;
      //                }
      //              cal.add(Calendar.DAY_OF_MONTH ,1);
      //            }
      //
      //            cal.add(Calendar.DAY_OF_MONTH ,-1);
      //
      //            DayBean db = getHijriDateFromMeladi(df.format(cal.getTime()));
      //            vacationProcessInvokeBean.setVacationEndDate(db.getHijriDate());
      //
      //          vacationProsessInvokeBacking.setVacationProcessInvokeBean(vacationProcessInvokeBean);
      //          session.setAttribute("VacationProsessInvokeBackedBean", vacationProsessInvokeBacking);
      //          }
      //
      //         vacationProsessInvokeBacking.setVacationProcessInvokeBean(vacationProcessInvokeBean);
      //         session.setAttribute("VacationProsessInvokeBackedBean", vacationProsessInvokeBacking);
      //
      //        columnIndex = null;
      //        rowIndex = null;
      //        }
      //
      return null;
  }


  public void prepareCalendarFromDate(PopupFetchEvent popupFetchEvent) throws GTechException {


      setCalendarHijriShowed(true);
      inputId = "fromDate";

      calendarBean = new CalendarBean();


      if (dayBean.getHijriDate().trim().equals("")) {

          DayBean dayBeanRequest = new DayBean();
          dayBeanRequest.setCenturyADDate(getCurrentDate());

          RequestContainer<DayBean> request =
              new RequestContainer<DayBean>();
          request.setDataBean(dayBeanRequest);

          CalendarBusiness calendarBusiness = new CalendarBusiness();
          ResponseContainer<DayBean> response =
              calendarBusiness.getHijriDate(request);
          dayBean = response.getDataBean();
      }

      calendarBean.setYear((dayBean != null ?
                            dayBean.getHijriDate().split("/")[2] : ""));
      calendarBean.setMonthNumber((dayBean != null ?
                                   dayBean.getHijriDate().split("/")[1] :
                                   ""));

      populateHijriYears();
      fllHijriCalendar();
  }

  public void prepareHolidayCalendarFromDate(PopupFetchEvent popupFetchEvent) throws GTechException {

      HttpSession session =
          (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      ManageEventsBacking calendarManageBacking =
          (ManageEventsBacking)session.getAttribute("manageEventsBackedBean");
      HolidayBean holidayBean = calendarManageBacking.getHolidayBean();

      setCalendarHijriShowed(true);
      inputId = "fromDate";

      calendarBean = new CalendarBean();
      dayBean.setHijriDate((holidayBean.getFromDateHolidayH() != null ?
                            holidayBean.getFromDateHolidayH() : ""));

      dayBean.setCenturyADDate((holidayBean.getFromDateHolidayM() != null ?
                                holidayBean.getFromDateHolidayM() : ""));


      if (dayBean.getHijriDate().trim().equals("")) {

          DayBean dayBeanRequest = new DayBean();
          dayBeanRequest.setCenturyADDate(getCurrentDate());

          RequestContainer<DayBean> request =
              new RequestContainer<DayBean>();
          request.setDataBean(dayBeanRequest);

          CalendarBusiness calendarBusiness = new CalendarBusiness();
          ResponseContainer<DayBean> response =
              calendarBusiness.getHijriDate(request);
          dayBean = response.getDataBean();
      }

      calendarBean.setYear((dayBean != null ?
                            dayBean.getHijriDate().split("/")[2] : ""));
      calendarBean.setMonthNumber((dayBean != null ?
                                   dayBean.getHijriDate().split("/")[1] :
                                   ""));


      calendarManageBacking.setHolidayBean(holidayBean);
      session.setAttribute("manageEventsBackedBean", calendarManageBacking);

      populateHijriYears();
      fllHijriCalendar();

  }

  public void prepareHolidayCalendarToDate(PopupFetchEvent popupFetchEvent) throws GTechException {

      HttpSession session =
          (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      ManageEventsBacking calendarManageBacking =
          (ManageEventsBacking)session.getAttribute("manageEventsBackedBean");
      HolidayBean holidayBean = calendarManageBacking.getHolidayBean();

      setCalendarHijriShowed(true);
      inputId = "fromDate";

      calendarBean = new CalendarBean();
      dayBean.setHijriDate((holidayBean.getToDateHolidayH() != null ?
                            holidayBean.getToDateHolidayH() : ""));

      dayBean.setCenturyADDate((holidayBean.getToDateHolidayM() != null ?
                                holidayBean.getToDateHolidayM() : ""));


      if (dayBean.getHijriDate().trim().equals("")) {

          DayBean dayBeanRequest = new DayBean();
          dayBeanRequest.setCenturyADDate(getCurrentDate());

          RequestContainer<DayBean> request =
              new RequestContainer<DayBean>();
          request.setDataBean(dayBeanRequest);

          CalendarBusiness calendarBusiness = new CalendarBusiness();
          ResponseContainer<DayBean> response =
              calendarBusiness.getHijriDate(request);
          dayBean = response.getDataBean();
      }

      calendarBean.setYear((dayBean != null ?
                            dayBean.getHijriDate().split("/")[2] : ""));
      calendarBean.setMonthNumber((dayBean != null ?
                                   dayBean.getHijriDate().split("/")[1] :
                                   ""));


      calendarManageBacking.setHolidayBean(holidayBean);
      session.setAttribute("manageEventsBackedBean", calendarManageBacking);

      populateHijriYears();
      fllHijriCalendar();

  }

  public String closeHolidayCalendarFromDateWindow() throws GTechException {

      HttpSession session =
          (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      ManageEventsBacking calendarManageBacking =
          (ManageEventsBacking)session.getAttribute("manageEventsBackedBean");
      HolidayBean holidayBean = calendarManageBacking.getHolidayBean();

      if (dayBean == null) {
          return null;
      }

      holidayBean.setFromDateHolidayH((dayBean.getHijriDate() != null ?
                                       dayBean.getHijriDate() : ""));

      holidayBean.setFromDateHolidayM((dayBean.getCenturyADDate() != null ?
                                       dayBean.getCenturyADDate() : ""));

      calendarManageBacking.setHolidayBean(holidayBean);
      session.setAttribute("manageEventsBackedBean", calendarManageBacking);


      return null;
  }

  public String closeHolidayCalendarToDateWindow() throws GTechException {

      HttpSession session =
          (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      ManageEventsBacking calendarManageBacking =
          (ManageEventsBacking)session.getAttribute("manageEventsBackedBean");
      HolidayBean holidayBean = calendarManageBacking.getHolidayBean();

      if (dayBean == null) {
          return null;
      }

      holidayBean.setToDateHolidayH((dayBean.getHijriDate() != null ?
                                     dayBean.getHijriDate() : ""));

      holidayBean.setToDateHolidayM((dayBean.getCenturyADDate() != null ?
                                     dayBean.getCenturyADDate() : ""));

      calendarManageBacking.setHolidayBean(holidayBean);
      session.setAttribute("manageEventsBackedBean", calendarManageBacking);


      return null;
  }

  public String goToHijriCalendar() throws GTechException {
      HttpSession session =
          (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);

      setCalendarHijriShowed(true);

      calendarBean = new CalendarBean();

      if (dayBean.getHijriDate().trim().equals("")) {

          DayBean dayBeanRequest = new DayBean();
          dayBeanRequest.setCenturyADDate(getCurrentDate());

          RequestContainer<DayBean> request =
              new RequestContainer<DayBean>();
          request.setDataBean(dayBeanRequest);

          CalendarBusiness calendarBusiness = new CalendarBusiness();
          ResponseContainer<DayBean> response =
              calendarBusiness.getHijriDate(request);
          dayBean = response.getDataBean();
      }

      calendarBean.setYear((dayBean != null ?
                            dayBean.getHijriDate().split("/")[2] : ""));
      calendarBean.setMonthNumber((dayBean != null ?
                                   dayBean.getHijriDate().split("/")[1] :
                                   ""));

      populateHijriYears();
      fllHijriCalendar();

      return "dialog:calendarHijriDialog";
  }

  public String goToCenturyADCalendar() throws GTechException {

      setCalendarHijriShowed(false);

      calendarBean = new CalendarBean();


      if (dayBean.getCenturyADDate().trim().equals("")) {
          dayBean = new DayBean();
          dayBean.setCenturyADDate(getCurrentDate());
      }

      calendarBean.setYear((dayBean != null ?
                            dayBean.getCenturyADDate().split("/")[2] : ""));
      calendarBean.setMonthNumber((dayBean != null ?
                                   dayBean.getCenturyADDate().split("/")[1] :
                                   ""));


      populateCenturyADYears();
      fillCenturyADMonths();
      fllCenturyADCalendar();


      return "dialog:calendarCenturyADDialog";
  }

  public void setDateCalenderReturnListner(ReturnEvent ev) {

      HttpSession session =
          (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);

      if (dayBean == null)
          return;
      if ((columnIndex == null && rowIndex == null) ||
          (columnIndex.equals("") && rowIndex.equals(""))) {


      } else {


          columnIndex = null;
          rowIndex = null;
      }


      //        dayBean = new DayBean();
      //        dayBean = (DayBean)ev.getReturnValue();
      //        outMailBean.setMailDate(dayBean.getCenturyADDate());
      //mailsReportBean.setFromMailHDate(dayBean.getHijriDate());

  }

  public static List<SelectItem> getYearsHijriList() {
      return yearsHijriList;
  }

  public void setYearsHijriList(List<SelectItem> yearsHijriList) {
      CalenderApplicationScopeBacking.yearsHijriList = yearsHijriList;
  }

  public List<SelectItem> getYearsCenturyADList() {
      return yearsCenturyADList;
  }

  public void setYearsCenturyADList(List<SelectItem> yearsCenturyADList) {
      CalenderApplicationScopeBacking.yearsCenturyADList = yearsCenturyADList;
  }



  public DayBean getDayBean() {
      return dayBean;
  }

  public void setDayBean(DayBean dayBean) {
      CalenderApplicationScopeBacking.dayBean = dayBean;
  }

  public List<SelectItem> getMonthsHijriList() {
      return monthsHijriList;
  }

  public void setMonthsHijriList(List<SelectItem> monthsHijriList) {
      CalenderApplicationScopeBacking.monthsHijriList = monthsHijriList;
  }

  public List<SelectItem> getMonthsCenturyADList() {
      return monthsCenturyADList;
  }

  public void setMonthsCenturyADList(List<SelectItem> monthsCenturyADList) {
      CalenderApplicationScopeBacking.monthsCenturyADList = monthsCenturyADList;
  }

  public boolean isCalendarHijriShowed() {
      return calendarHijriShowed;
  }

  public void setCalendarHijriShowed(boolean calendarHijriShowed) {
      CalenderApplicationScopeBacking.calendarHijriShowed = calendarHijriShowed;
  }

  public String getInputId() {
      return inputId;
  }

  public void setInputId(String inputId) {
      this.inputId = inputId;
  }

  public String getColumnIndex() {
      return columnIndex;
  }

  public void setColumnIndex(String columnIndex) {
      this.columnIndex = columnIndex;
  }

  public String getRowIndex() {
      return rowIndex;
  }

  public void setRowIndex(String rowIndex) {
      this.rowIndex = rowIndex;
  }


  public static void main(String[] args) throws GTechException {

      CalendarBacking cb = new CalendarBacking();
      SimpleDateFormat df =
          new SimpleDateFormat(CommonConfigReader.getValue("DEFAULT_DATE_PATTERN"));
      Calendar cal = Calendar.getInstance();
      Date date;
      try {
          date = df.parse("14/2/2011");
      } catch (ParseException e) {
          throw new GTechException(e);
      }
      cal.setTime(date);
      cal.add(Calendar.DAY_OF_MONTH, Integer.parseInt("5"));
      Date d2 = cal.getTime();
     
  }


  public void setListOfEvents(List<String> listOfEvents) {
      this.listOfEvents = listOfEvents;
  }

  public List<String> getListOfEvents() {
      return listOfEvents;
  }

  public String getHijriCurrentDate1() {
      return hijriCurrentDate;
  }

  public static void setCalenderDataOfAllYear(Map<String, CalendarBean> calenderDataOfAllYear) {
        CalenderApplicationScopeBacking.calenderDataOfAllYear = calenderDataOfAllYear;
  }

  public static Map<String, CalendarBean> getCalenderDataOfAllYear() {
      return calenderDataOfAllYear;
  }

  

  

  public static void setListOfHijriYear(List<String> listOfHijriYear) {
        CalenderApplicationScopeBacking.listOfHijriYear = listOfHijriYear;
  }

  public static List<String> getListOfHijriYear() {
      return listOfHijriYear;
  }

  public static void setCalendarBean(CalendarBean calendarBean) {
        CalenderApplicationScopeBacking.calendarBean = calendarBean;
  }

  public static CalendarBean getCalendarBean() {
      return calendarBean;
  }


  public static void setCurrentHijriMeladiDate(CurrentHijriMeladiDate currentHijriMeladiDate) {
        CalenderApplicationScopeBacking.currentHijriMeladiDate = currentHijriMeladiDate;
  }

  public static CurrentHijriMeladiDate getCurrentHijriMeladiDate() {
      return currentHijriMeladiDate;
  }
  }


