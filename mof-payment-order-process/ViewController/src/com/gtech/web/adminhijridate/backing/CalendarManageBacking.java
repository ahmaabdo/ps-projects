package com.gtech.web.adminhijridate.backing;

import com.gtech.common.exception.GTechException;
import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.faces.util.LabelReader;

import com.gtech.web.adminhijridate.bean.CalendarManageBean;
import com.gtech.web.adminhijridate.bean.DayBean;
import com.gtech.web.adminhijridate.bean.HolidayBean;
import com.gtech.web.adminhijridate.bean.MonthManageBean;
import com.gtech.web.adminhijridate.bean.YearBean;
import com.gtech.web.adminhijridate.business.CalendarManageBusiness;
import java.text.ParseException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.NavigationHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.FacesListener;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.event.DialogEvent;

import org.apache.myfaces.trinidad.component.core.data.CoreTable;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.RowKeySet;

public class CalendarManageBacking extends GTechModelBacking {
  private List<SelectItem> yearsHijriList;
  private List<DayBean> selectedHoliday;
  private CalendarManageBean calendarBean;
  private boolean editYear;
  private boolean isDeleteHoliDayRender;
  private boolean editYearPirmetion;
  private HolidayBean holidayBean = new HolidayBean();


  public CalendarManageBacking() throws GTechException{
      calendarBean = new CalendarManageBean();
      initialize();
  }
  
  public String initialize() throws GTechException {
     reSetData();

      return "goToManageCalendar";
  }

  public String initAddNewHijriYear() throws GTechException {
      calendarBean = new CalendarManageBean();
      List<MonthManageBean> list = new ArrayList<MonthManageBean>();
      for(int i = 1 ; i <= 12 ; i++){
       MonthManageBean monthBean = new MonthManageBean();
        monthBean.setMonthName(LabelReader.getLabel("HIJRI_MONTH_"+i));
        monthBean.setMonthNumber(String.valueOf(i));
       list.add(monthBean);
      }

      calendarBean.setMonthsList(list);
      return "addDate";
  }

  private void populateHijriYears() throws GTechException {
      yearsHijriList = new ArrayList<SelectItem>();

      CalendarManageBusiness calendarBusiness = new CalendarManageBusiness();
      ResponseContainer<YearBean> responseDataContainer = calendarBusiness.getAllHihriYears();

      for (YearBean yearBean : responseDataContainer.getDataList()) {
          SelectItem yearItem = new SelectItem();
          yearItem.setLabel(yearBean.getYear());
          yearItem.setValue(yearBean.getYear());

          yearsHijriList.add(yearItem);
      }
  }
  
  public void initAddYear(){
    calendarBean = new CalendarManageBean();
  }
  
  public String initEditYear() throws GTechException{
    
    
    setEditYear(true);
    RequestContainer<CalendarManageBean> request = new RequestContainer<CalendarManageBean>();
    request.setDataBean(calendarBean);

    CalendarManageBusiness calendarManageBusiness = new CalendarManageBusiness();
    calendarBean = calendarManageBusiness.getAllMonthsOfYear(request).getDataBean();

    calendarBean.setStartDate(calendarBean.getMonthsList().get(0).getStartDate());
    
    return "editDate";
  }

  public void yearValueChangeListener(ValueChangeEvent event) throws GTechException {
        calendarBean.setYear((String)event.getNewValue());
        setHolidayBean(null);
        RequestContainer<CalendarManageBean> request = new RequestContainer<CalendarManageBean>();
        request.setDataBean(calendarBean);

        CalendarManageBusiness calendarManageBusiness = new CalendarManageBusiness();
        calendarBean = calendarManageBusiness.getAllMonthsOfYear(request).getDataBean();

        calendarBean.setStartDate(calendarBean.getMonthsList().get(0).getStartDate());
  }

 

   public String populateEdit(){
      setEditYear(true);
      holidayBean = new HolidayBean();
      return null;
   }
   public String initEditHijriYear() throws  GTechException{
    
       if(!editYearPirmetion){
         return null;
       }
       
     editYearPirmetion = false;
     return initialize();
  
   }

  

  public String tableSelectionListener(SelectionEvent event) {
      setIsDeleteHoliDayRender(true);
      //addPartialTriggerInRegion("holidayTable");
      return null;
  }
  
   private void reSetData() throws GTechException{
      calendarBean = new CalendarManageBean();
      populateHijriYears();
      setEditYear(false);
      setIsDeleteHoliDayRender(false);
      selectedHoliday = new ArrayList<DayBean>();
      setIsDeleteHoliDayRender(false);
   }

  private CoreTable getHolidayTable(String tableName) {
      UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
      CoreTable myTable = (CoreTable) vr.findComponent(tableName);

      return myTable;
  }

  public List<SelectItem> getYearsHijriList() {
      return yearsHijriList;
  }

  public void setYearsHijriList(List<SelectItem> yearsHijriList) {
      this.yearsHijriList = yearsHijriList;
  }

  public CalendarManageBean getCalendarBean() {
      return calendarBean;
  }

  public void setCalendarBean(CalendarManageBean calendarBean) {
      this.calendarBean = calendarBean;
  }

  public boolean isEditYear() {
      return editYear;
  }

  public void setEditYear(boolean editYear) {
      this.editYear = editYear;
  }

  public boolean isIsDeleteHoliDayRender() {
      return isDeleteHoliDayRender;
  }

  public void setIsDeleteHoliDayRender(boolean isDeleteHoliDayRender) {
      this.isDeleteHoliDayRender = isDeleteHoliDayRender;
  }

  public HolidayBean getHolidayBean() {
      return holidayBean;
  }

  public void setHolidayBean(HolidayBean holidayBean) {
      this.holidayBean = holidayBean;
  }

  }

