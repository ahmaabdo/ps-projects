

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtech.web.adminhijridate.backing;


import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.faces.util.LabelReader;
import com.gtech.web.adminhijridate.bean.CalendarBean;

import com.gtech.web.adminhijridate.bean.DayBean;

import com.gtech.web.adminhijridate.bean.YearBean;
import com.gtech.web.adminhijridate.business.CalendarBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpSession;

import org.apache.myfaces.trinidad.component.core.input.CoreInputHidden;
import org.apache.myfaces.trinidad.component.core.input.CoreInputText;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.ReturnEvent;

/**
 *
 * @author tqasem
 */
public class CalendarBacking extends GTechModelBacking {

    private List<SelectItem> yearsHijriList;
    private List<SelectItem> yearsCenturyADList;
    private CalendarBean calendarBean;
    private DayBean dayBean;
    private List<SelectItem> monthsHijriList;
    private List<SelectItem> monthsCenturyADList;
    private boolean calendarHijriShowed;

    private String inputId;
    private String rowIndex;
    private String columnIndex;
    private String dateValue;

    @Override
    public String initialize() throws GTechException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public CalendarBacking() throws GTechException {
        dayBean = new DayBean();
        //        calendarBean = new CalendarBean();
        //        calendarBean.setYear("1427");
        //        calendarBean.setMonthNumber("01");
        //        setCalendarHijriShowed(true);
        //        if (isCalendarHijriShowed()) {
        //            populateHijriYears();
        //            fillHijriMonths();
        //            fllHijriCalendar();
        //        } else {
        //            populateCenturyADYears();
        //            fillCenturyADMonths();
        //            fllCenturyADCalendar();
        //        }
    }

    private void fllHijriCalendar() throws GTechException {

        //calendarBean.setMonthBean(new MonthBean());

        CalendarBusiness calendarBusiness = new CalendarBusiness();
        RequestContainer<CalendarBean> request = new RequestContainer<CalendarBean>();
        request.setDataBean(calendarBean);

        calendarBean = calendarBusiness.getCalendarHijri(request).getDataBean();
    }

    private void populateHijriYears() throws GTechException {
        yearsHijriList = new ArrayList<SelectItem>();

        CalendarBusiness calendarBusiness = new CalendarBusiness();
        ResponseContainer<YearBean> responseDataContainer = calendarBusiness.getAllHihriYears();

        for (YearBean yearBean : responseDataContainer.getDataList()) {
            SelectItem yearItem = new SelectItem();
            yearItem.setLabel(yearBean.getYear());
            yearItem.setValue(yearBean.getYear());

            yearsHijriList.add(yearItem);
        }
    }

    private void fillHijriMonths() {

        monthsHijriList = new ArrayList<SelectItem>();

        for (int i = 1; i <= 12; i++) {

            SelectItem monthItem = new SelectItem();
            monthItem.setLabel(LabelReader.getLabel("HIJRI_MONTH_" + i));

            String monthValue = (i < 10 ? "0" + i : String.valueOf(i));
            monthItem.setValue(monthValue);

            monthsHijriList.add(monthItem);
        }

        //        SelectItem monthItem1 = new SelectItem();
        //        monthItem1.setLabel("محرم");
        //        monthItem1.setValue("01");
        //
        //        monthsHijriList.add(monthItem1);
        //
        //        SelectItem monthItem2 = new SelectItem();
        //        monthItem2.setLabel("صفر");
        //        monthItem2.setValue("02");
        //
        //        monthsHijriList.add(monthItem2);
        //
        //        SelectItem monthItem3 = new SelectItem();
        //        monthItem3.setLabel("ربيع الأول");
        //        monthItem3.setValue("03");
        //
        //        monthsHijriList.add(monthItem3);
        //
        //        SelectItem monthItem4 = new SelectItem();
        //        monthItem4.setLabel("ربيع الثاني");
        //        monthItem4.setValue("04");
        //
        //        monthsHijriList.add(monthItem4);
        //
        //        SelectItem monthItem5 = new SelectItem();
        //        monthItem5.setLabel("جمادي الأولى");
        //        monthItem5.setValue("05");
        //
        //        monthsHijriList.add(monthItem5);
        //
        //        SelectItem monthItem6 = new SelectItem();
        //        monthItem6.setLabel("جمادي الأخرة");
        //        monthItem6.setValue("06");
        //
        //        monthsHijriList.add(monthItem6);
        //
        //        SelectItem monthItem7 = new SelectItem();
        //        monthItem7.setLabel("رجب");
        //        monthItem7.setValue("07");
        //
        //        monthsHijriList.add(monthItem7);
        //
        //        SelectItem monthItem8 = new SelectItem();
        //        monthItem8.setLabel("شعبان");
        //        monthItem8.setValue("08");
        //
        //        monthsHijriList.add(monthItem8);
        //
        //        SelectItem monthItem9 = new SelectItem();
        //        monthItem9.setLabel("رمضان");
        //        monthItem9.setValue("09");
        //
        //        monthsHijriList.add(monthItem9);
        //
        //        SelectItem monthItem10 = new SelectItem();
        //        monthItem10.setLabel("شوال");
        //        monthItem10.setValue("10");
        //
        //        monthsHijriList.add(monthItem10);
        //
        //        SelectItem monthItem11 = new SelectItem();
        //        monthItem11.setLabel("ذو القعدة");
        //        monthItem11.setValue("11");
        //
        //        monthsHijriList.add(monthItem11);
        //
        //        SelectItem monthItem12 = new SelectItem();
        //        monthItem12.setLabel("ذو الحجة");
        //        monthItem12.setValue("12");
        //
        //        monthsHijriList.add(monthItem12);
    }

    private void populateCenturyADYears() throws GTechException {
        yearsCenturyADList = new ArrayList<SelectItem>();

        CalendarBusiness calendarBusiness = new CalendarBusiness();
        ResponseContainer<YearBean> responseDataContainer = calendarBusiness.getAllCenturyADYears();

        for (YearBean yearBean : responseDataContainer.getDataList()) {
            SelectItem yearItem = new SelectItem();
            yearItem.setLabel(yearBean.getYear());
            yearItem.setValue(yearBean.getYear());

            yearsCenturyADList.add(yearItem);
        }
    }

    private void fllCenturyADCalendar() throws GTechException {
        //        calendarBean = new CalendarBean();
        //        calendarBean.setYear("2006");
        //        calendarBean.setMonthNumber("02");

        CalendarBusiness calendarBusiness = new CalendarBusiness();
        RequestContainer<CalendarBean> request = new RequestContainer<CalendarBean>();
        request.setDataBean(calendarBean);

        calendarBean = calendarBusiness.getCalendarCenturyAD(request).getDataBean();
    }

    private void fillCenturyADMonths() {

        monthsCenturyADList = new ArrayList<SelectItem>();
        for (int i = 1; i <= 12; i++) {

            SelectItem monthItem = new SelectItem();
            monthItem.setLabel(LabelReader.getLabel("CENTURYAD_MONTH_" + i));

            String monthValue = (i < 10 ? "0" + i : String.valueOf(i));
            monthItem.setValue(monthValue);

            monthsCenturyADList.add(monthItem);
        }

        //        SelectItem monthItem1 = new SelectItem();
        //        monthItem1.setLabel("يناير");
        //        monthItem1.setValue("01");
        //
        //        monthsCenturyADList.add(monthItem1);
        //
        //        SelectItem monthItem2 = new SelectItem();
        //        monthItem2.setLabel("فبراير");
        //        monthItem2.setValue("02");
        //
        //        monthsCenturyADList.add(monthItem2);
        //
        //        SelectItem monthItem3 = new SelectItem();
        //        monthItem3.setLabel("مارس");
        //        monthItem3.setValue("03");
        //
        //        monthsCenturyADList.add(monthItem3);
        //
        //        SelectItem monthItem4 = new SelectItem();
        //        monthItem4.setLabel("ابريل");
        //        monthItem4.setValue("04");
        //
        //        monthsCenturyADList.add(monthItem4);
        //
        //        SelectItem monthItem5 = new SelectItem();
        //        monthItem5.setLabel("مايو");
        //        monthItem5.setValue("05");
        //
        //        monthsCenturyADList.add(monthItem5);
        //
        //        SelectItem monthItem6 = new SelectItem();
        //        monthItem6.setLabel("يونيو");
        //        monthItem6.setValue("06");
        //
        //        monthsCenturyADList.add(monthItem6);
        //
        //        SelectItem monthItem7 = new SelectItem();
        //        monthItem7.setLabel("يوليو");
        //        monthItem7.setValue("07");
        //
        //        monthsCenturyADList.add(monthItem7);
        //
        //        SelectItem monthItem8 = new SelectItem();
        //        monthItem8.setLabel("اغسطس");
        //        monthItem8.setValue("08");
        //
        //        monthsCenturyADList.add(monthItem8);
        //
        //        SelectItem monthItem9 = new SelectItem();
        //        monthItem9.setLabel("سبتمبر");
        //        monthItem9.setValue("09");
        //
        //        monthsCenturyADList.add(monthItem9);
        //
        //        SelectItem monthItem10 = new SelectItem();
        //        monthItem10.setLabel("أكتوبر");
        //        monthItem10.setValue("10");
        //
        //        monthsCenturyADList.add(monthItem10);
        //
        //        SelectItem monthItem11 = new SelectItem();
        //        monthItem11.setLabel("نوفمبر");
        //        monthItem11.setValue("11");
        //
        //        monthsCenturyADList.add(monthItem11);
        //
        //        SelectItem monthItem12 = new SelectItem();
        //        monthItem12.setLabel("ديسمبر");
        //        monthItem12.setValue("12");
        //
        //        monthsCenturyADList.add(monthItem12);
    }

    public void getNextMonthListener(ActionEvent event) throws GTechException {
        if (calendarBean.getYear() == null || calendarBean.getYear().equals("")) {
            return;
        }
        if (calendarBean.getMonthNumber().equals("12")) {
            calendarBean.setMonthNumber("01");
            calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) + 1));
        } else {
            String newMonth = String.valueOf(Integer.parseInt(calendarBean.getMonthNumber()) + 1);
            newMonth = (newMonth.length() == 2 ? newMonth : "0" + newMonth);

            calendarBean.setMonthNumber(newMonth);
        }

        if (isCalendarHijriShowed()) {
            fllHijriCalendar();
        } else {
            fllCenturyADCalendar();
        }

        addPartialCalindarCompnent();
    }

    public void getLastMonthListener(ActionEvent event) throws GTechException {
        if (calendarBean.getYear() == null || calendarBean.getYear().equals("")) {
            return;
        }

        if (calendarBean.getMonthNumber().equals("01")) {
            calendarBean.setMonthNumber("12");
            calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) - 1));
        } else {
            String newMonth = String.valueOf(Integer.parseInt(calendarBean.getMonthNumber()) - 1);
            newMonth = (newMonth.length() == 2 ? newMonth : "0" + newMonth);

            calendarBean.setMonthNumber(newMonth);
        }

        if (isCalendarHijriShowed()) {
            fllHijriCalendar();
        } else {
            fllCenturyADCalendar();
        }

        addPartialCalindarCompnent();
    }

    public void monthValueChangeListener(ValueChangeEvent event) throws GTechException {
        calendarBean.setMonthNumber((String) event.getNewValue());
        if (isCalendarHijriShowed()) {
            fllHijriCalendar();
        } else {
            fllCenturyADCalendar();
        }

        addPartialCalindarCompnent();
    }

    public void getNextYearListener(ActionEvent event) throws GTechException {
        if (calendarBean.getYear() == null || calendarBean.getYear().equals("")) {
            return;
        }
        calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) + 1));

        if (isCalendarHijriShowed()) {
            fllHijriCalendar();
        } else {
            fllCenturyADCalendar();
        }

        addPartialCalindarCompnent();
    }

    public void getLastYearListener(ActionEvent event) throws GTechException {
        if (calendarBean.getYear() == null || calendarBean.getYear().equals("")) {
            return;
        }
        calendarBean.setYear(String.valueOf(Integer.parseInt(calendarBean.getYear()) - 1));

        if (isCalendarHijriShowed()) {
            fllHijriCalendar();
        } else {
            fllCenturyADCalendar();
        }

        addPartialCalindarCompnent();
    }

    public void yearValueChangeListener(ValueChangeEvent event) throws GTechException {
        calendarBean.setYear((String) event.getNewValue());
        if (isCalendarHijriShowed()) {
            fllHijriCalendar();
        } else {
            fllCenturyADCalendar();
        }

        addPartialCalindarCompnent();
    }

    private void addPartialCalindarCompnent() {
        addPartialTrigger("calendarId");
        //     addPartialTrigger("yearId");
        //     addPartialTrigger("monthId");
    }

    public String closeWindow() {

        RequestContext.getCurrentInstance().returnFromDialog(getDayBean(), null);

        return null;
    }

    public String goToHijriCalendar() throws GTechException {
        setCalendarHijriShowed(true);

        calendarBean = new CalendarBean();

        dayBean.setHijriDate((getInputText(inputId + "H").getValue() != null ?
                              getInputText(inputId + "H").getValue().toString() : ""));


        if (dayBean.getHijriDate()
                   .trim()
                   .equals("")) {

            DayBean dayBeanRequest = new DayBean();
            dayBeanRequest.setCenturyADDate(getCurrentDate());

            RequestContainer<DayBean> request = new RequestContainer<DayBean>();
            request.setDataBean(dayBeanRequest);

            CalendarBusiness calendarBusiness = new CalendarBusiness();
            ResponseContainer<DayBean> response = calendarBusiness.getHijriDate(request);
            dayBean = response.getDataBean();
        }

        calendarBean.setYear((dayBean != null ? dayBean.getHijriDate().split("/")[2] : ""));
        calendarBean.setMonthNumber((dayBean != null ? dayBean.getHijriDate().split("/")[1] : ""));


        populateHijriYears();
        fillHijriMonths();
        fllHijriCalendar();

        return "dialog:calendarHijriDialog";
    }


    public void setDateCalenderReturnListner(ReturnEvent ev) throws GTechException {
        if (dayBean == null)
            return;
        if ((columnIndex == null && rowIndex == null) || (columnIndex.equals("") && rowIndex.equals(""))) {
            if (getInputText(inputId + "M") != null) {
                getInputText(inputId + "M").setValue(dayBean.getCenturyADDate());
                addPartialTrigger(inputId + "M");
            }

            if (getInputText(inputId + "H") != null) {
                getInputText(inputId + "H").setValue(dayBean.getHijriDate());
                addPartialTrigger(inputId + "H");
            }

        }

        //        dayBean = new DayBean();
        //        dayBean = (DayBean)ev.getReturnValue();
        //        outMailBean.setMailDate(dayBean.getCenturyADDate());
        //mailsReportBean.setFromMailHDate(dayBean.getHijriDate());

    }

    public List<SelectItem> getYearsHijriList() {
        return yearsHijriList;
    }

    public void setYearsHijriList(List<SelectItem> yearsHijriList) {
        this.yearsHijriList = yearsHijriList;
    }

    public List<SelectItem> getYearsCenturyADList() {
        return yearsCenturyADList;
    }

    public void setYearsCenturyADList(List<SelectItem> yearsCenturyADList) {
        this.yearsCenturyADList = yearsCenturyADList;
    }

    public CalendarBean getCalendarBean() {
        return calendarBean;
    }

    public void setCalendarBean(CalendarBean calendarBean) {
        this.calendarBean = calendarBean;
    }

    public DayBean getDayBean() {
        return dayBean;
    }

    public void setDayBean(DayBean dayBean) {
        this.dayBean = dayBean;
    }

    public List<SelectItem> getMonthsHijriList() {
        return monthsHijriList;
    }

    public void setMonthsHijriList(List<SelectItem> monthsHijriList) {
        this.monthsHijriList = monthsHijriList;
    }

    public List<SelectItem> getMonthsCenturyADList() {
        return monthsCenturyADList;
    }

    public void setMonthsCenturyADList(List<SelectItem> monthsCenturyADList) {
        this.monthsCenturyADList = monthsCenturyADList;
    }

    public boolean isCalendarHijriShowed() {
        return calendarHijriShowed;
    }

    public void setCalendarHijriShowed(boolean calendarHijriShowed) {
        this.calendarHijriShowed = calendarHijriShowed;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(String columnIndex) {
        this.columnIndex = columnIndex;
    }

    public String getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(String rowIndex) {
        this.rowIndex = rowIndex;
    }

    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public String getDateValue() {
        return dateValue;
    }
}
