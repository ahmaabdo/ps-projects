package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

import java.util.ArrayList;
import java.util.List;

public class MonthBean extends BaseBean {

    private List<WeekBean> weeksList = new ArrayList<WeekBean>();
    private int monthNumber;

    public List<WeekBean> getWeeksList() {
        return weeksList;
    }

    public void setWeeksList(List<WeekBean> weeksList) {
        this.weeksList = weeksList;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

}


