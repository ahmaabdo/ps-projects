package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

public class WeekBean extends BaseBean{
  private DayBean saturday;
  private DayBean sunday;
  private DayBean monday;
  private DayBean tuesday;
  private DayBean wednesday;
  private DayBean thursday;
  private DayBean friday;
  

  public DayBean getFriday() {
      return friday;
  }

  public void setFriday(DayBean friday) {
      this.friday = friday;
  }

  public DayBean getMonday() {
      return monday;
  }

  public void setMonday(DayBean monday) {
      this.monday = monday;
  }

  public DayBean getSaturday() {
      return saturday;
  }

  public void setSaturday(DayBean saturday) {
      this.saturday = saturday;
  }

  public DayBean getSunday() {
      return sunday;
  }

  public void setSunday(DayBean sunday) {
      this.sunday = sunday;
  }

  public DayBean getThursday() {
      return thursday;
  }

  public void setThursday(DayBean thursday) {
      this.thursday = thursday;
  }

  public DayBean getTuesday() {
      return tuesday;
  }

  public void setTuesday(DayBean tuesday) {
      this.tuesday = tuesday;
  }

  public DayBean getWednesday() {
      return wednesday;
  }

  public void setWednesday(DayBean wednesday) {
      this.wednesday = wednesday;
  }
}
