package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

public class MonthManageBean extends BaseBean{
  private String numberOfDays;
  private String monthNumber;
  private String monthName;
  private String startDate;

  public String getMonthNumber() {
      return monthNumber;
  }

  public void setMonthNumber(String monthNumber) {
      this.monthNumber = monthNumber;
  }

  public String getNumberOfDays() {
      return numberOfDays;
  }

  public void setNumberOfDays(String monthNumber) {
      this.numberOfDays = monthNumber;
  }

  public String getMonthName() {
      return monthName;
  }

  public void setMonthName(String monthName) {
      this.monthName = monthName;
  }

  public String getStartDate() {
      return startDate;
  }

  public void setStartDate(String startDate) {
      this.startDate = startDate;
  }

  
}
