package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

import java.util.ArrayList;
import java.util.List;

public class CalendarManageBean extends BaseBean {

    private String year;
    private List<MonthManageBean> monthsList = new ArrayList<MonthManageBean>();
    private String startDate;
    private String userName;
    private String dateTimeModified;
    private List<String> weekEndDays  = new ArrayList<String>();
    private List<DayBean> holidaysDays  = new ArrayList<DayBean>();

    public List<MonthManageBean> getMonthsList() {
        return monthsList;
    }

    public void setMonthsList(List<MonthManageBean> monthsList) {
        this.monthsList = monthsList;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDateTimeModified() {
        return dateTimeModified;
    }

    public void setDateTimeModified(String dateTimeModified) {
        this.dateTimeModified = dateTimeModified;
    }

    public List<String> getWeekEndDays() {
        return weekEndDays;
    }

    public void setWeekEndDays(List<String> weekEndDays) {
        this.weekEndDays = weekEndDays;
    }

    public List<DayBean> getHolidaysDays() {
        return holidaysDays;
    }

    public void setHolidaysDays(List<DayBean> holidaysDays) {
        this.holidaysDays = holidaysDays;
    }
    
}
