package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

public class HolidayBean extends BaseBean {
  private String fromDateHolidayM;
  private String toDateHolidayM;
  private String fromDateHolidayH;
  private String toDateHolidayH;
  private String holidyName;
  private String spaceNameDesc;

  private String generalFromDateHoliday;
  private String generalToDateHoliday;

  

  public String getHolidyName() {
      return holidyName;
  }

  public void setHolidyName(String holidyName) {
      this.holidyName = holidyName;
  }

  public String getGeneralFromDateHoliday() {
      return generalFromDateHoliday;
  }

  public void setGeneralFromDateHoliday(String generalFromDateHoliday) {
      this.generalFromDateHoliday = generalFromDateHoliday;
  }

  public String getGeneralToDateHoliday() {
      return generalToDateHoliday;
  }

  public void setGeneralToDateHoliday(String generalToDateHoliday) {
      this.generalToDateHoliday = generalToDateHoliday;
  }


  public void setFromDateHolidayM(String fromDateHolidayM) {
      this.fromDateHolidayM = fromDateHolidayM;
  }

  public String getFromDateHolidayM() {
      return fromDateHolidayM;
  }

  public void setToDateHolidayM(String toDateHolidayM) {
      this.toDateHolidayM = toDateHolidayM;
  }

  public String getToDateHolidayM() {
      return toDateHolidayM;
  }

  public void setFromDateHolidayH(String fromDateHolidayH) {
      this.fromDateHolidayH = fromDateHolidayH;
  }

  public String getFromDateHolidayH() {
      return fromDateHolidayH;
  }

  public void setToDateHolidayH(String toDateHolidayH) {
      this.toDateHolidayH = toDateHolidayH;
  }

  public String getToDateHolidayH() {
      return toDateHolidayH;
  }

  public void setSpaceNameDesc(String spaceNameDesc) {
      this.spaceNameDesc = spaceNameDesc;
  }

  public String getSpaceNameDesc() {
      return spaceNameDesc;
  }
}
