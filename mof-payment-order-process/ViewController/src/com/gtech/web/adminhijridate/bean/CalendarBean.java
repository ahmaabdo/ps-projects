package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

public class CalendarBean extends BaseBean {
  private MonthBean monthBean;
  private String year;
  private String monthNumber;



  public MonthBean getMonthBean() {
      return monthBean;
  }

  public void setMonthBean(MonthBean monthBean) {
      this.monthBean = monthBean;
  }

  public String getMonthNumber() {
      return monthNumber;
  }

  public void setMonthNumber(String monthNumber) {
      this.monthNumber = monthNumber;
  }

  public String getYear() {
      return year;
  }

  public void setYear(String year) {
      this.year = year;
  }

}
