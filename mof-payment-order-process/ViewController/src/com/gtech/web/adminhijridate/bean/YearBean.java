package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

public class YearBean extends BaseBean{
  private String year;

  public String getYear() {
      return year;
  }

  public void setYear(String year) {
      this.year = year;
  }
}
