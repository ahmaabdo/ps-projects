package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

import java.util.List;

public class CalendarProfileBean extends BaseBean {
  private String hYear;
  private String hMonth;
  private String noOfDays;
  private String startDate;
  private List<DayBean>listDays;
  private Integer userId;

  public String getHMonth() {
      return hMonth;
  }

  public void setHMonth(String hMonth) {
      this.hMonth = hMonth;
  }

  public String getHYear() {
      return hYear;
  }

  public void setHYear(String hYear) {
      this.hYear = hYear;
  }

  public String getNoOfDays() {
      return noOfDays;
  }

  public void setNoOfDays(String noOfDays) {
      this.noOfDays = noOfDays;
  }

  public String getStartDate() {
      return startDate;
  }

  public void setStartDate(String startDate) {
      this.startDate = startDate;
  }

  public List<DayBean> getListDays() {
      return listDays;
  }

  public void setListDays(List<DayBean> listDays) {
      this.listDays = listDays;
  }

  public Integer getUserId() {
      return userId;
  }

  public void setUserId(Integer userId) {
      this.userId = userId;
  }

}
