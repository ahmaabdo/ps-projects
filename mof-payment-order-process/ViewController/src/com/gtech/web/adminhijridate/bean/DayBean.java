package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

import java.util.ArrayList;
import java.util.List;

public class DayBean extends BaseBean {

    private String dateID;
    private String spaceName;
    private String hijriDate;
    private String centuryADDate;
    private String numberDayOfHijriMonth;
    private String numberDayOfCenturyADMonth;
    private String dayOfWeek;
    private String holiDayName;
    private List<String> listOfEvents = new ArrayList<String>();

    public String getHijriDate() {
        return hijriDate;
    }

    public void setHijriDate(String hijriDate) {
        this.hijriDate = hijriDate;
    }

    public String getNumberDayOfHijriMonth() {
        return numberDayOfHijriMonth;
    }

    public void setNumberDayOfHijriMonth(String numberDayOfHijriMonth) {
        this.numberDayOfHijriMonth = numberDayOfHijriMonth;
    }

    public String getCenturyADDate() {
        return centuryADDate;
    }

    public void setCenturyADDate(String centuryADDate) {
        this.centuryADDate = centuryADDate;
    }

    public String getNumberDayOfCenturyADMonth() {
        return numberDayOfCenturyADMonth;
    }

    public void setNumberDayOfCenturyADMonth(String numberDayOfCenturyADMonth) {
        this.numberDayOfCenturyADMonth = numberDayOfCenturyADMonth;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getHoliDayName() {
        return holiDayName;
    }

    public void setHoliDayName(String holiDayName) {
        this.holiDayName = holiDayName;
    }


    public void setDateID(String dateID) {
        this.dateID = dateID;
    }

    public String getDateID() {
        return dateID;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setListOfEvents(List<String> listOfEvents) {
        this.listOfEvents = listOfEvents;
    }

    public List<String> getListOfEvents() {
        return listOfEvents;
    }
}
