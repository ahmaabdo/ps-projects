package com.gtech.web.adminhijridate.bean;

import com.gtech.common.base.BaseBean;

public class CurrentHijriMeladiDate extends BaseBean{
  private String hijriDay;
  private String hijriMonth;
  private String hijriYear;
  
  private String meladiDay;
  private String meladiMonth;
  private String meladiYear;
  
  private String dayName;
  
  public CurrentHijriMeladiDate() {
      super();
  }


  public void setHijriDay(String hijriDay) {
      this.hijriDay = hijriDay;
  }

  public String getHijriDay() {
      return hijriDay;
  }

  public void setHijriMonth(String hijriMonth) {
      this.hijriMonth = hijriMonth;
  }

  public String getHijriMonth() {
      return hijriMonth;
  }

  public void setHijriYear(String hijriYear) {
      this.hijriYear = hijriYear;
  }

  public String getHijriYear() {
      return hijriYear;
  }

  public void setMeladiDay(String meladiDay) {
      this.meladiDay = meladiDay;
  }

  public String getMeladiDay() {
      return meladiDay;
  }

  public void setMeladiMonth(String meladiMonth) {
      this.meladiMonth = meladiMonth;
  }

  public String getMeladiMonth() {
      return meladiMonth;
  }

  public void setMeladiYear(String meladiYear) {
      this.meladiYear = meladiYear;
  }

  public String getMeladiYear() {
      return meladiYear;
  }

  public void setDayName(String dayName) {
      this.dayName = dayName;
  }

  public String getDayName() {
      return dayName;
  }
}
