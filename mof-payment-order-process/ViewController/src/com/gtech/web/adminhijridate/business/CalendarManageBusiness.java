package com.gtech.web.adminhijridate.business;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.adminhijridate.bean.CalendarManageBean;
import com.gtech.web.adminhijridate.bean.MonthManageBean;
import com.gtech.web.adminhijridate.bean.YearBean;
import com.gtech.web.adminhijridate.dao.CalendarManageDAO;

import java.util.Calendar;
import java.util.List;

public class CalendarManageBusiness extends GTechModelBusiness{


      public ResponseContainer<YearBean> getAllHihriYears() throws GTechException{

          CalendarManageDAO calendarDAO = new CalendarManageDAO();
          ResponseContainer<YearBean> responseDataContainer = calendarDAO.getAllHijriYearsProfile();
          return responseDataContainer;
      }

      public ResponseContainer<CalendarManageBean> getAllMonthsOfYear(RequestContainer<CalendarManageBean> request) throws GTechException {
          CalendarManageDAO calendarManageDAO = new CalendarManageDAO();
          ResponseContainer<CalendarManageBean> response = calendarManageDAO.getAllMonthsOfYear(request);

          CalendarManageBean calendarManageBean = response.getDataBean();

          RequestContainer<CalendarManageBean> requestCalendarManageBean = new RequestContainer<CalendarManageBean>();
          requestCalendarManageBean.setDataBean(calendarManageBean);

          response = calendarManageDAO.getAllWeekEndHolidy(requestCalendarManageBean);

          RequestContainer<CalendarManageBean> requestAllHolidaysOfYear = new RequestContainer<CalendarManageBean>();
          requestAllHolidaysOfYear.setDataBean(response.getDataBean());
          return calendarManageDAO.getAllHolidaysOfYear(requestAllHolidaysOfYear);

          
      }

      public ResponseContainer<CalendarManageBean>getAllHolidaysOfYear(RequestContainer<CalendarManageBean> request) throws GTechException{
          CalendarManageDAO calendarManageDAO = new CalendarManageDAO();
          return calendarManageDAO.getAllHolidaysOfYear(request);
       }
      
      public ResponseContainer<CalendarManageBean>getAllEventsOfYear(RequestContainer<CalendarManageBean> request) throws GTechException{
          CalendarManageDAO calendarManageDAO = new CalendarManageDAO();
          return calendarManageDAO.getAllEventsOfYear(request);
       }

      


     
      private String checkInValedMonths(List<MonthManageBean> monthsList) {

          String invalidMonths = "";
          for (MonthManageBean monthManageBean : monthsList) {

              if (monthManageBean.getNumberOfDays() == null || monthManageBean.getNumberOfDays().isEmpty()) {
                  if (!invalidMonths.isEmpty()) {
                      invalidMonths += ",";
                  }
                  invalidMonths += monthManageBean.getMonthName();
              }
          }
          return invalidMonths;
      }

          private int getNoOfDays(List<MonthManageBean> monthsList) {

          int numberOfDays = 0;
          for (MonthManageBean monthManageBean : monthsList) {
              numberOfDays+= Integer.parseInt(monthManageBean.getNumberOfDays());
          }
          
          return numberOfDays-1;
      }

      private Calendar getDate(String date) {


          Calendar cal = Calendar.getInstance();

          cal.set(Calendar.YEAR, Integer.parseInt(date.split("/")[2]));
          cal.set(Calendar.MONTH, Integer.parseInt(date.split("/")[1]) - 1);
          cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.split("/")[0]));

          return cal;
      }

      private String converDateToString(Calendar cal) {
          String day = (cal.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + cal.get(Calendar.DAY_OF_MONTH) : String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));

          String month = ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : String.valueOf((cal.get(Calendar.MONTH) + 1)));

          return day + "/" + month + "/" + cal.get(Calendar.YEAR);
      }

      private Calendar getDateCalendar(String date) {


          Calendar cal = Calendar.getInstance();

          cal.set(Calendar.YEAR, Integer.parseInt(date.split("/")[2]));
          cal.set(Calendar.MONTH, Integer.parseInt(date.split("/")[1])-1);
          cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.split("/")[0]));

          return cal;
      }

      private int getDayOfWeek(Calendar cal){
         int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)+1;
         if(dayOfWeek>7)dayOfWeek = 1;

         return dayOfWeek;
      }
      
      public static void main(String args[]){
        CalendarManageBusiness a =new CalendarManageBusiness();
        Calendar mDate = a.getDate("06/12/2010");
        mDate.add(Calendar.DAY_OF_MONTH, 354);
        String toDate = a.converDateToString(mDate);
      }
  }


