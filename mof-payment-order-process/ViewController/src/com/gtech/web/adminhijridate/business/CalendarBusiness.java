/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gtech.web.adminhijridate.business;


import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.adminhijridate.bean.CalendarBean;
import com.gtech.web.adminhijridate.bean.DayBean;
import com.gtech.web.adminhijridate.bean.MonthBean;
import com.gtech.web.adminhijridate.bean.WeekBean;
import com.gtech.web.adminhijridate.bean.YearBean;
import com.gtech.web.adminhijridate.dao.CalendarDAO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author tqasem
 */
public class CalendarBusiness extends GTechModelBusiness{

    public ResponseContainer<YearBean> getAllHihriYears() throws GTechException{

        CalendarDAO calendarDAO = new CalendarDAO();
        ResponseContainer<YearBean> responseDataContainer = calendarDAO.getAllHijriYears();
        return responseDataContainer;
    }

    public ResponseContainer<YearBean> getAllCenturyADYears() throws GTechException{

        CalendarDAO calendarDAO = new CalendarDAO();
        ResponseContainer<YearBean> responseDataContainer = calendarDAO.getAllCenturyADYears();
        return responseDataContainer;
    }

    public ResponseContainer<CalendarBean> getCalendarHijri(RequestContainer<CalendarBean> request)  throws GTechException{
     CalendarDAO calendarDAO = new CalendarDAO();
     List<DayBean> daysBean = calendarDAO.getHijriDays(request).getDataList();

     MonthBean monthBean = filliMonth(daysBean);

     CalendarBean calendarBean = request.getDataBean();
     calendarBean.setMonthBean(monthBean);

     ResponseContainer<CalendarBean> response = new ResponseContainer<CalendarBean>();
     response.setDataBean(calendarBean);

     return response;
    }

    public ResponseContainer<CalendarBean> getCalendarCenturyAD(RequestContainer<CalendarBean> request)  throws GTechException{
     CalendarDAO calendarDAO = new CalendarDAO();
     List<DayBean> daysBean = calendarDAO.getCenturyADDays(request).getDataList();

     MonthBean monthBean = filliMonth(daysBean);

     CalendarBean calendarBean = request.getDataBean();
     calendarBean.setMonthBean(monthBean);

     ResponseContainer<CalendarBean> response = new ResponseContainer<CalendarBean>();
     response.setDataBean(calendarBean);

     return response;
    }

    public ResponseContainer<DayBean> getHijriDate(RequestContainer<DayBean> request) throws GTechException{

        CalendarDAO calendarDao = new CalendarDAO();
        ResponseContainer<DayBean> response = calendarDao.getDateByCenturyADDate(request);

        return response;
    }

    private MonthBean filliMonth(List<DayBean> daysBean){

        if(daysBean == null  || daysBean.size() == 0)return null;

        MonthBean monthBean = new MonthBean();

        int firstDateOfMonth = getDayOfWeek(getDateCalendar(daysBean.get(0).getCenturyADDate()));

        List<WeekBean> weeksList = new ArrayList<WeekBean>();

        boolean completdMonth = false;
        int weekIndex = 1;
        int indexDate = 0;

        while (!completdMonth) {

            WeekBean weekBean = new WeekBean();

            for (int i = 1; i <= 7; i++) {
                DayBean dayBaen = new DayBean();

                if (!(weekIndex == 1 && i < firstDateOfMonth)) {
                    if (indexDate < daysBean.size()) {

//                        if (indexDate > 1) {
//                            hijriDate.add(hijriDate.DAY_OF_MONTH, 1);
//                            mDate.add(hijriDate.DAY_OF_MONTH, 1);
//                        }
//                        dayBaen.setHijriDate(converDateToString(hijriDate));
//                        dayBaen.setMeladyDate(converDateToString(mDate));

                        dayBaen = daysBean.get(indexDate);
                        dayBaen.setNumberDayOfHijriMonth(dayBaen.getHijriDate().substring(0, 2) );
                        dayBaen.setNumberDayOfCenturyADMonth(dayBaen.getCenturyADDate().substring(0, 2));

                        indexDate++;
                    } else {
                        completdMonth = true;
                    }
                }

                switch (i) {
                    case 1:
                        weekBean.setSaturday(dayBaen);
                        break;
                    case 2:
                        weekBean.setSunday(dayBaen);
                        break;
                    case 3:
                        weekBean.setMonday(dayBaen);
                        break;
                    case 4:
                        weekBean.setTuesday(dayBaen);
                        break;
                    case 5:
                        weekBean.setWednesday(dayBaen);
                        break;
                    case 6:
                        weekBean.setThursday(dayBaen);
                        break;
                    case 7:
                        weekBean.setFriday(dayBaen);
                        break;
                }
            }
            weeksList.add(weekBean);
            weekIndex++;
        }

        monthBean.setWeeksList(weeksList);

        return monthBean;
    }





    private Calendar getDateCalendar(String date) {


        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, Integer.parseInt(date.split("/")[2]));
        cal.set(Calendar.MONTH, Integer.parseInt(date.split("/")[1])-1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.split("/")[0]));

        return cal;
    }

    private int getDayOfWeek(Calendar cal){
       int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)+1;
       if(dayOfWeek>7)dayOfWeek = 1;

       return dayOfWeek;
    }

    public String getCurrentHijriDate() throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        return calendarDAO.getCurrentHijriDate();
    }

    public String getGrDateByHijriDate(String hDate) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        return calendarDAO.getGrDateByHijriDate(hDate);
    }


}
