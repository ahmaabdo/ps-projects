package com.gtech.web.actionLog.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.bean.ActionLogBean;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.business.EntityBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;


@ManagedBean(name = "actionLogBacking")
@ViewScoped
public class ActionLogBacking extends GTechModelBacking {

    private ResponseContainer responseContainer = new ResponseContainer();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private List<ActionLogBean> actionLogList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String paymentOrderNumber;
    private EntityBean entityBean = new EntityBean();


    public void init() {
        EntityBusiness entityBusiness = new EntityBusiness();
        try {
            entityBean = entityBusiness.getUserEntity(getLogedInUser().getUserID());
        } catch (GTechException e) {
        }
    }

    public void fillActionLog(ActionEvent event) {
        try {
            fillActionLog();
        } catch (GTechException e) {
        }
    }


    public void fillActionLog() throws GTechException {
        actionLogList.clear();
        actionLogList = actionLogBusiness.getAllPaymentActionLog(paymentOrderNumber,getLogedInUser());
        if(actionLogList.isEmpty()){
            numberOfPaymentOrder = 0 ;
        }else{
            numberOfPaymentOrder = actionLogList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }

    @Override
    public String initialize() throws GTechException {
        actionLogList = new ArrayList<>();
        numberOfPaymentOrder = 0;
        paymentOrderNumber = null;
        // TODO Implement this method
        return "PO-F16";

    }


    public String doRest() throws GTechException {
        actionLogList.clear();
        paymentOrderNumber = null;
        numberOfPaymentOrder = null;
        return null;
    }


    public void setActionLogList(List<ActionLogBean> actionLogList) {
        this.actionLogList = actionLogList;
    }

    public List<ActionLogBean> getActionLogList() {
        return actionLogList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setPaymentOrderNumber(String paymentOrderNumber) {
        this.paymentOrderNumber = paymentOrderNumber;
    }

    public String getPaymentOrderNumber() {
        return paymentOrderNumber;
    }

    public void setEntityBean(EntityBean entityBean) {
        this.entityBean = entityBean;
    }

    public EntityBean getEntityBean() {
        return entityBean;
    }
}
