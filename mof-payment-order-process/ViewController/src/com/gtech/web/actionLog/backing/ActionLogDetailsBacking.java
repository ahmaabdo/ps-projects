package com.gtech.web.actionLog.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import oracle.adf.view.rich.render.ClientEvent;

@ManagedBean(name = "actionLogDetailsBacking")
@SessionScoped
public class ActionLogDetailsBacking extends GTechModelBacking {
    ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean;
    private String remark;
    private String statusName;
    private String payMethodName;
    private String paramSelected;

    public ActionLogDetailsBacking() throws GTechException {
        super();
    }

    @Override
    public String initialize() throws GTechException {
        return null;
    }

    public String init() throws GTechException {
        rest();
        DomainBusiness domainBusiness = new DomainBusiness();
        paymentOrderBean = paymentOrderBusiness.getFullDataPaymentOrderByPONumber(paramSelected);
        if(paymentOrderBean != null && paymentOrderBean.getId() > 0 ) {
            payMethodName = domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentMethod()).getNameAr();
            statusName =  domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentOrderStatus().longValue()).getNameAr();
            return "PO-F19";   
        }
        ResponseContainer responseContainer = new ResponseContainer();
        responseContainer.addErrorMessage("NO_DATA_FOUND");
        createMessages(responseContainer);
        return null;
    }

    private void rest() {
        remark = null;
        statusName = null;
        payMethodName = null;
        paymentOrderBean = new PaymentOrderBean();
    }

    public void handleAutoFillExpense(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {
             ExpenseBean expenseBean = getPaymentOrderBean().getExpenseBean();
            LookupBean lookupBean =
                lookupBusiness.getLookupValueAutoFilled(expenseBean.getCode(),null,"POG_EXPENSE_TYPES");
            expenseBean.setCode(lookupBean.getCode());
            expenseBean.setDescAr(lookupBean.getNameAr());
            expenseBean.setDescEn(lookupBean.getNameEn());
            expenseBean.setId(lookupBean.getId());
        } catch (GTechException e) {
        }
        reloadComponent("EXPENSE_ID_FIELD");
        reloadComponent("EXPENSE_CODE_FIELD");
        reloadComponent("EXPENSE_DESC_FIELD");
    }


    public String returnToPaymentOrderAuditingPage() throws GTechException {

        return "PO-F16";
    }

    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setPayMethodName(String payMethodName) {
        this.payMethodName = payMethodName;
    }

    public String getPayMethodName() {
        return payMethodName;
    }

    public void setParamSelected(String paramSelected) {
        this.paramSelected = paramSelected;
    }

    public String getParamSelected() {
        return paramSelected;
    }
}



