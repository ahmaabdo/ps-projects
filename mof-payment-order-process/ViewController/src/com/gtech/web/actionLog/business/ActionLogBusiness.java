package com.gtech.web.actionLog.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.actionLog.bean.ActionLogBean;
import com.gtech.web.actionLog.dao.ActionLogDAO;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;

import java.util.List;

import login.LoginBean;

public class ActionLogBusiness extends GTechModelBusiness {

    private ActionLogDAO actionLogDAO = new ActionLogDAO();

    
    public boolean insertFile(List<PaymentOrderBean> paymentOrderBeanList, Integer actionId, String userId) throws GTechException {
        return actionLogDAO.insertFile(paymentOrderBeanList, actionId, userId);
    }

    public boolean insereLogLine(Long paymentOrderId, String remark, Long userId,
                                 Integer actionId) throws GTechException {
        return actionLogDAO.insereLogLine(paymentOrderId, remark, userId, actionId);
    }
    
    public ActionLogBean getPaymentActionLog(long paymentId) throws GTechException {
        return actionLogDAO.getPaymentActionLog(paymentId);
    }
    
    public List<ActionLogBean> getAllPaymentActionLog(String paymentId,LoginBean user) throws GTechException {
        return actionLogDAO.getAllPaymentActionLog(paymentId,user);
    }
}
