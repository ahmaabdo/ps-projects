package com.gtech.web.actionLog.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.actionLog.bean.ActionLogBean;
import com.gtech.web.adminhijridate.dao.CalendarDAO;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import login.LoginBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ActionLogDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(ActionLogDAO.class);


    public boolean insertFile(List<PaymentOrderBean> paymentOrderBeanList, Integer actionId,
                              String userId) throws GTechException {
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            String query =
                "INSERT INTO POG_SGR_ACTION_LOG " + "(ROS_ID , " + " ROS_SGR_ID ," + " ROS_REMARKS , " +
                " ROS_SSC_ID , " + " ROS_ACTION_ID) VALUES " + "(ROS_PK_SEQ.NEXTVAL,?,?,?,?)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            for (PaymentOrderBean paymentOrderBean : paymentOrderBeanList) {

                if (paymentOrderBean.isCheck()) {
                    preparedStatement.setLong(1, paymentOrderBean.getId());
                    preparedStatement.setString(2, paymentOrderBean.getRemark());
                    preparedStatement.setString(3, userId);
                    preparedStatement.setInt(4, actionId);
                    boolean isInseted = preparedStatement.executeUpdate() > 0;
                    if (!isInseted) {
                        return isInseted;
                    }
                }

            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement);
        }
        return true;
    }

    public boolean insereLogLine(Long paymentOrderId, String remark, Long userId,
                                 Integer actionId) throws GTechException {
        
        Connection connection = getConnection();
        boolean isOk = false;
        try {
            isOk= insereLogLine(paymentOrderId, remark, userId, actionId, connection);
        }finally {
            closeResources(connection);
        }
       
        return isOk;
    }
    
    public boolean insertLogLine(Long paymentOrderId, String remark, Long userId, Integer actionId,
                                 Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        try {
            String query =
                "INSERT INTO POG_SGR_ACTION_LOG " + "(ROS_ID , ROS_SGR_ID , ROS_REMARKS , " +
                " ROS_SSC_ID , ROS_ACTION_ID) VALUES (ROS_PK_SEQ.NEXTVAL,?,?,?,?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, paymentOrderId);
            preparedStatement.setString(2, remark);
            preparedStatement.setLong(3, userId);
            preparedStatement.setInt(4, actionId);
            boolean isInseted = preparedStatement.executeUpdate() > 0;
            if (!isInseted) {
                return isInseted;
            }
        } catch (SQLException e) {
            throw new GTechException(e);
        } finally {
            closeResources(preparedStatement);
        }
        return true;
    }
    

    public boolean insereLogLine(Long paymentOrderId, String remark, Long userId, Integer actionId,
                                 Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        try {
            String query =
                "INSERT INTO POG_SGR_ACTION_LOG " + "(ROS_ID , ROS_SGR_ID , ROS_REMARKS , " +
                " ROS_SSC_ID , ROS_ACTION_ID) VALUES (ROS_PK_SEQ.NEXTVAL,?,?,?,?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, paymentOrderId);
            preparedStatement.setString(2, remark);
            preparedStatement.setLong(3, userId);
            preparedStatement.setInt(4, actionId);
            boolean isInseted = preparedStatement.executeUpdate() > 0;
            if (!isInseted) {
                return isInseted;
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(preparedStatement);
        }
        return true;
    }
    
    public boolean insereLogLine(String paymentOrderId, String remark, String userId,
                                 Integer actionId, Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        try {
            String query =
                "INSERT INTO POG_SGR_ACTION_LOG " + "(ROS_ID , ROS_SGR_ID , ROS_REMARKS , " +
                " ROS_SSC_ID , ROS_ACTION_ID) VALUES (ROS_PK_SEQ.NEXTVAL,?,?,?,?)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, paymentOrderId);
            preparedStatement.setString(2, remark);
            preparedStatement.setString(3, userId);
            preparedStatement.setInt(4, actionId);
            boolean isInseted = preparedStatement.executeUpdate() > 0;
            if (!isInseted) {
                return isInseted;
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(preparedStatement);
        }
        return true;
    }

    public ActionLogBean getPaymentActionLog(long paymentId) throws GTechException {
        ActionLogBean actionLogBean = new ActionLogBean();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String query =
                "SELECT LOGS.ROS_ID,  ROS_ACTION_DATE_HI,DMG.GAO_DESC_AR ,USR.SSC_NAME_AR " + "    FROM \n" +
                "        POG_SGR_ACTION_LOG logs, " + "        POG_USERS USR, " + "        POG_DMG_VALUES DMG " +
                "     WHERE USR.SSC_ID = LOGS.ROS_SSC_ID " + "        AND DMG.GAO_ID = LOGS.ROS_ACTION_ID " +
                "        AND LOGS.ROS_SGR_ID = ? " +
                "        AND LOGS.ROS_ID = (SELECT MAX(ROS_ID) FROM POG_SGR_ACTION_LOG WHERE ROS_SGR_ID = LOGS.ROS_SGR_ID)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, paymentId);

            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                actionLogBean.setLastUpdateDate(rs.getString("ROS_ACTION_DATE_HI"));
                actionLogBean.setLastUpdateUser(rs.getString("SSC_NAME_AR"));
                actionLogBean.setLastStatusDescAr(rs.getString("GAO_DESC_AR"));

                return actionLogBean;
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return actionLogBean;
    }

    public List<ActionLogBean> getAllPaymentActionLog(String paymentId,LoginBean user) throws GTechException {
        List<ActionLogBean> actionLogBeanList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String query =
                " SELECT LOGS.ROS_ID,  ROS_ACTION_DATE_HI,DMG.GAO_DESC_AR ,USR.SSC_NAME_AR,ROS_REMARKS,ROS_SEQ                      \n" +
                "    FROM " + "        POG_SGR_ACTION_LOG logs, " + "        POG_USERS USR, " +
                "        POG_DMG_VALUES DMG, " + "        POG_PAYMENT_ORDERS PYM " +
                "     WHERE USR.SSC_ID = LOGS.ROS_SSC_ID " + "        AND DMG.GAO_ID = LOGS.ROS_ACTION_ID " +
                "        AND LOGS.ROS_SGR_ID = PYM.SGR_ID " + "        AND PYM.SGR_CODE = ? ";
                
            
            if(user.getEntityBean() != null  && user.getEntityBean().getCode() != null) {
                query += " AND PYM.SGR_EEO_ID = ?" + " AND PYM.SGR_TTG_ID = ?" + " AND PYM.SGR_PTN_ID = ? AND PYM.SGR_ORG_ID = ? ";
            }
                    
            query += "        ORDER BY ROS_ID DESC ";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, paymentId);
            if(user.getEntityBean() != null  && user.getEntityBean().getCode() != null) {
                preparedStatement.setLong(2, user.getEntityBean().getId());
                preparedStatement.setLong(3, user.getDepartmentBean().getId());
                preparedStatement.setLong(4, user.getSectionBean().getId());
                preparedStatement.setLong(5, user.getOrganizationBean().getId());
            }
            rs = preparedStatement.executeQuery();
            ActionLogBean actionLogBean = null;
            while (rs.next()) {
                actionLogBean = new ActionLogBean();

                actionLogBean.setLastUpdateDate(rs.getString("ROS_ACTION_DATE_HI"));
                actionLogBean.setLastUpdateUser(rs.getString("SSC_NAME_AR"));
                actionLogBean.setLastStatusDescAr(rs.getString("GAO_DESC_AR"));
                actionLogBean.setRemark(rs.getString("ROS_REMARKS"));
                actionLogBean.setSeq(rs.getInt("ROS_SEQ"));

                actionLogBeanList.add(actionLogBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }

        return actionLogBeanList;
    }
    
    public ActionLogBean getAuditActionLog(long paymentId) throws GTechException {
        ActionLogBean actionLogBean = new ActionLogBean();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String query =
                "SELECT *\n" + 
                "  FROM POG_SGR_ACTION_LOG  LOGS " + 
                " WHERE LOGS.ROS_SGR_ID = ? " + 
                " AND LOGS.ROS_ID = (SELECT MAX(ROS_ID) FROM POG_SGR_ACTION_LOG WHERE ROS_SGR_ID = LOGS.ROS_SGR_ID)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, paymentId);

            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                actionLogBean.setRemark(rs.getString("ROS_REMARKS"));
                return actionLogBean;
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return actionLogBean;
    }
}
