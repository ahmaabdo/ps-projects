package com.gtech.web.actionLog.bean;

import com.gtech.common.base.*;

public class ActionLogBean extends BaseBean {


    private Integer paymentNumberId;
    private String remark;
    private String lastStatusDescAr;
    private Integer seq;

    public void setPaymentNumberId(Integer paymentNumberId) {
        this.paymentNumberId = paymentNumberId;
    }

    public Integer getPaymentNumberId() {
        return paymentNumberId;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setLastStatusDescAr(String lastStatusDescAr) {
        this.lastStatusDescAr = lastStatusDescAr;
    }

    public String getLastStatusDescAr() {
        return lastStatusDescAr;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Integer getSeq() {
        return seq;
    }
}
