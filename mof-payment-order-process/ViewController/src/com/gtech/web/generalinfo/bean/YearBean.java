package com.gtech.web.generalinfo.bean;

import com.gtech.common.base.BaseBean;

public class YearBean extends BaseBean {
    private int year;
    private String yearDesc;
    public YearBean() {
        super();
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setYearDesc(String yearDesc) {
        this.yearDesc = yearDesc;
    }

    public String getYearDesc() {
        return yearDesc;
    }
}
