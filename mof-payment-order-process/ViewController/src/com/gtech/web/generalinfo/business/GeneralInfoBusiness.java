package com.gtech.web.generalinfo.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.generalinfo.dao.GeneralInfoDao;

import java.util.Date;

public class GeneralInfoBusiness extends GTechModelBusiness {
    private GeneralInfoDao generalInfoDao = new GeneralInfoDao();

    public String getGeneralInfoYears(Date date) throws GTechException {
        return generalInfoDao.getGeneralInfoYears(date);
    }

    public CurrencyBean getSaudiRialCurrency() throws GTechException {
        return generalInfoDao.getSaudiRialCurrency();
    }
}
