package com.gtech.web.generalinfo.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.currency.bean.CurrencyBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import java.util.Date;

public class GeneralInfoDao extends GTechDAO {
    public GeneralInfoDao() {
        super();
    }

    public String getGeneralInfoYears(Date date) throws GTechException {
        String year = "";
        CallableStatement statement = null;
        ResultSet rs = null;
        String query ="{ ? = call pog_util.get_year_by_date(?)}";
        Connection connection = getConnection();
        try {
            statement = connection.prepareCall(query);
            statement.registerOutParameter(1, Types.VARCHAR);
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            statement.setDate(2, sqlDate);
            statement.executeUpdate();
            year = statement.getString(1);
        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, statement, rs);
        }
        return year;
    }

    public CurrencyBean getSaudiRialCurrency() throws GTechException {
        CurrencyBean currencyBean = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        String query = "SELECT * FROM POG_GENERAL_INFO, POG_CURRENCIES WHERE CRN_ID = GRO_SAR_CURRENCY_ID";
        Connection connection = getConnection();
        try {
            statement = connection.prepareStatement(query);
            rs = statement.executeQuery();
            if (rs.next()){
                currencyBean = new CurrencyBean();
                currencyBean.setCurId(rs.getString("GRO_SAR_CURRENCY_ID"));
                currencyBean.setCode(rs.getString("CRN_CODE"));
                currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
            }

        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statement, rs);
        }
        return currencyBean;
    }
}
