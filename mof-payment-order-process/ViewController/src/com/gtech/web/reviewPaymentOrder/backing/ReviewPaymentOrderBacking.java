package com.gtech.web.reviewPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;
import com.gtech.web.reviewsinglepaymentorder.backing.ReviewSinglePaymentOrderBacking;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


@ManagedBean(name = "reviewPaymentOrderBean")
@SessionScoped
public class ReviewPaymentOrderBacking extends GTechModelBacking {

    private ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String date;
    private boolean hiddenDate = false;
    private String poId;

    public ReviewPaymentOrderBacking() throws GTechException {

    }

    public void fillReviewPaymentOrder(ActionEvent event) {
        try {
            if(isValidSearch()) {
                fillReviewPaymentOrder();
            }
             
        } catch (GTechException e) {
            e.printStackTrace();
        }
    }

    public void fillReviewPaymentOrder() throws GTechException {
        reviewPOBeanList.clear();
        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatus(100401, getPoDateFrom() ,getPoDateTo(),
                                                                            getPoNumber(),
                                                                            getBenNumber(),
                                                                            getIbanAccNumber(), 
                                                                            getLogedInUser());
                                                                           
        if(reviewPOBeanList.isEmpty()){
            numberOfPaymentOrder = 0 ;
        }else{
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        
        if(reviewPOBeanList != null &&
           !reviewPOBeanList.isEmpty() &&
            reviewPOBeanList.size() == 1) {
            FacesContext context = FacesContext.getCurrentInstance();
            ReviewSinglePaymentOrderBacking bean = context.getApplication().evaluateExpressionGet(context, "#{reviewSinglePaymentOrderBacking}", ReviewSinglePaymentOrderBacking.class);
            
            System.out.println(reviewPOBeanList.get(0).getId());
            poId = reviewPOBeanList.get(0).getId() + "";
            bean.setParamSelected(poId);
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");

    }

    @Override
    public String initialize() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        numberOfPaymentOrder = 0;
        date = "";
        if (getLogedInUser().getEntityBean() == null || getLogedInUser().getEntityBean().getId() <= 0) {
            hiddenDate = true;
        } else{
            hiddenDate = false;
        }
        // TODO Implement this method
        return "PO-F4";
    }

    public String backFromReviewSinglePaymentOrder() {
        try {
            setPoId(null);
            setPoNumber(null);
            fillReviewPaymentOrder();
        } catch (GTechException e) {
        }

        return "PO-F4";
    }

    public String doSave() throws GTechException {
        responseContainer = new ResponseContainer();
        if (actionLogBusiness.insertFile(reviewPOBeanList, 100502, getLogedInUser().getUserID())) {
            fillReviewPaymentOrder();
            responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        } else {
            responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
        }
        createMessages(responseContainer);

        return null;
    }

    public String doRest() throws GTechException {
        reviewPOBeanList.clear();
        resetSearchParam();
        numberOfPaymentOrder = null;
        return null;
    }

    public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
        this.paymentOrderBusiness = paymentOrderBusiness;
    }

    public PaymentOrderBusiness getPaymentOrderBusiness() {
        return paymentOrderBusiness;
    }

    public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
        this.reviewPOBeanList = reviewPOBeanList;
    }

    public List<PaymentOrderBean> getReviewPOBeanList() {
        return reviewPOBeanList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setHiddenDate(boolean hiddenDate) {
        this.hiddenDate = hiddenDate;
    }

    public boolean isHiddenDate() {
        return hiddenDate;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getPoId() {
        return poId;
    }
}
