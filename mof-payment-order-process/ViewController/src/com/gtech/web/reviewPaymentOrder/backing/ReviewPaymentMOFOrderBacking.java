package com.gtech.web.reviewPaymentOrder.backing;


import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.util.StringUtilities;
import com.gtech.faces.util.MessageReader;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.currency.business.CurrencyBusiness;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;
import com.gtech.web.reviewPaymentOrder.backing.ReviewPaymentOrderBacking;

import com.gtech.web.reviewsinglepaymentorder.backing.ReviewSinglePaymentOrderBacking;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;

@ManagedBean(name = "reviewPaymentMOFOrderBacking", eager = true)
@SessionScoped
public class ReviewPaymentMOFOrderBacking extends GTechModelBacking {
    ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean = new PaymentOrderBean();
    private String remark;
    private String beneficaryCode;
    private String beneficaryName;
    private String secBeneficaryName;
    private String amount;
    private String poId;
    private String refNumber;
    private Integer numberOfPaymentOrder;
    private CurrencyBusiness currencyBusiness = new CurrencyBusiness();
    
    public void retrivePOByNumber(ValueChangeEvent valueChangeEvent) {
        try {
            setPoNumber(valueChangeEvent.getNewValue().toString());
            PaymentOrderBean paymentOrderBeanSearch = paymentOrderBusiness.getFullDataPaymentOrderMOF(Long.parseLong(getPoNumber()), getLogedInUser());
            
            if (paymentOrderBeanSearch != null) {
                paymentOrderBean =paymentOrderBeanSearch;    
            } else {
                setPoId(null);
                setPoNumber(null);
                setPaymentOrderBean(new PaymentOrderBean());
                getPaymentOrderBean().setBeneficiaryBean(new BeneficiaryBean());
                getPaymentOrderBean().setExpenseBean(new ExpenseBean());
                AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                FacesContext facesContext = FacesContext.getCurrentInstance();
                UIComponent component = facesContext.getViewRoot().findComponent("paymentOrderNumber");
                RichInputText richInputText = (RichInputText) component;
                richInputText.clearCachedClientIds();
                richInputText.clearInitialState();
                richInputText.resetValue();
                richInputText.setValue("");
                adfFacesContext.addPartialTarget(richInputText);
                FacesMessage msg=new FacesMessage(MessageReader.getMessage("PO_CANT_REVIEWED"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx=FacesContext.getCurrentInstance();
                fctx.addMessage("paymentOrderNumber", msg);
                runJSPoNumber();
                return;
            }
            runJS();
            reloadComponent("reviewMOFForm");
        } catch (GTechException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public String initialize() throws GTechException {
        setPoId(null);
        setPoNumber(null);
        setPaymentOrderBean(new PaymentOrderBean());
        getPaymentOrderBean().setBeneficiaryBean(new BeneficiaryBean());
        getPaymentOrderBean().setExpenseBean(new ExpenseBean());
        // TODO Implement this method
        return "PO-F26";
    }

    public void validateBeneficiaryNumber(ValueChangeEvent event) throws GTechException {
        String beneficiaryCode = (String) event.getNewValue();
        ResponseContainer responseContainer = new ResponseContainer();
        setBeneficaryCode(beneficiaryCode);
        if (beneficiaryCode == null || !beneficaryCode.equals(paymentOrderBean.getBeneficiaryBean().getCode())) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("BEN_NUMBER", msg);
            
            //runFocusJSBEN_NUMBER();
            runJSBen();
            return;
        }
        responseContainer = paymentOrderBusiness.validateBeneficiaryNumber(beneficiaryCode);
        if (!responseContainer.containsErrorMessages()) {
            BeneficiaryBean beneficiaryBean = (BeneficiaryBean) responseContainer.getDataBean();
            setBeneficaryName(beneficiaryBean.getNameAr());
            runJS();
        } else {
            setBeneficaryName("");
        }
        
        if (getBeneficaryCode() == null || !getBeneficaryCode().equals(String.valueOf(paymentOrderBean.getBeneficiaryBean().getCode()))) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("BEN_NUMBER", msg);
            runJSBen();
        }
        
        reloadComponent("BEN_NUMBER");
    }
    
    public void validateRefNumber(ValueChangeEvent event) throws GTechException {
        String refNumber = (String) event.getNewValue();
        setRefNumber(refNumber);
        
        if (refNumber == null || !refNumber.equals(paymentOrderBean.getRefNumber())) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("PO_REF_LABEL_VALUE");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("REF_NUMBER_VALIDATE"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("PO_REF_LABEL_VALUE", msg);
            
            //runFocusJSBEN_NUMBER();
            runFocusJSPO_REF_LABEL_VALUE();
            runJS();
            return;
        }
        System.out.println("HOO");
        runJS();
    }

    public void validateSecBeneficaryName(ValueChangeEvent event) throws GTechException {
        String secBeneficaryName = (String) event.getNewValue();
        setBeneficaryName(secBeneficaryName);
        if(paymentOrderBean.getBeneficaryName() != null) {
            if (secBeneficaryName == null || !secBeneficaryName.equals(paymentOrderBean.getBeneficaryName()))  {
                
                AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
                FacesContext facesContext = FacesContext.getCurrentInstance();
                UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
                RichInputText richInputText = (RichInputText) component;
                richInputText.clearCachedClientIds();
                richInputText.clearInitialState();
                richInputText.resetValue();
                richInputText.setValue("");
                adfFacesContext.addPartialTarget(richInputText);
                
                FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext fctx=FacesContext.getCurrentInstance();
                fctx.addMessage("BEN_NUMBER", msg);
                runJSBen();
            } else {
                runJS();
            }
        }
        runJS();
    }

    public void validateAmount(ValueChangeEvent event) throws GTechException {
        String amount = (String) event.getNewValue();
        if (paymentOrderBean.getCurrencyBean().getNameEn().contains("SAR")) {
            setAmount(round(paymentOrderBean.getAmount(), currencyBusiness.getPrecision("SAR")) + "");
        } else {
            setAmount(round(paymentOrderBean.getAmount(), currencyBusiness.getPrecision(paymentOrderBean.getCurrencyBean().getNameEn().substring(paymentOrderBean.getCurrencyBean().getNameEn().indexOf(',') + 1, paymentOrderBean.getCurrencyBean().getNameEn().lastIndexOf(',')).trim())) + "");
        }
        if (amount == null || ! (Double.parseDouble(amount) == paymentOrderBean.getAmount())) {
            
            setAmount(null);
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("AMOUNT_FIELD");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("AMOUNT_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("AMOUNT_FIELD", msg);
            runJSAmount();
        }
        runJS();
        reloadComponent("AMOUNT_TEXT_FIELD");
    }
    

    public void confirmPaymentOrder(ActionEvent event) throws GTechException {
        boolean check = false;
        if (getBeneficaryCode() == null ||
            !getBeneficaryCode().equals(String.valueOf(paymentOrderBean.getBeneficiaryBean().getCode()))) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("BEN_NUMBER", msg);
            check = true;
        }
        if (paymentOrderBean.getCurrencyBean().getNameEn().contains("SAR")) {
            setAmount(round(paymentOrderBean.getAmount(), currencyBusiness.getPrecision("SAR")) + "");
        } else {
            setAmount(round(paymentOrderBean.getAmount(), currencyBusiness.getPrecision(paymentOrderBean.getCurrencyBean().getNameEn().substring(paymentOrderBean.getCurrencyBean().getNameEn().indexOf(',') + 1, paymentOrderBean.getCurrencyBean().getNameEn().lastIndexOf(',')).trim())) + "");
        }
        if (getAmount() == null || !getAmount().equals(String.valueOf(paymentOrderBean.getAmount()))) {
           
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("AMOUNT_FIELD");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
           
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("AMOUNT_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("AMOUNT_FIELD", msg);
            check = true;
        }
        if ("999".equals(paymentOrderBean.getBeneficiaryBean().getCode()) &&
            (getSecBeneficaryName() == null ||
             !getSecBeneficaryName().equals(String.valueOf(paymentOrderBean.getAmount())))) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("BEN_NUMBER", msg);
            check = true;
        }
        
//        if (refNumber == null || !refNumber.equals(paymentOrderBean.getRefNumber())) {
//            
//            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
//            FacesContext facesContext = FacesContext.getCurrentInstance();
//            UIComponent component = facesContext.getViewRoot().findComponent("PO_REF_LABEL_VALUE");
//            RichInputText richInputText = (RichInputText) component;
//            richInputText.clearCachedClientIds();
//            richInputText.clearInitialState();
//            richInputText.resetValue();
//            richInputText.setValue("");
//            adfFacesContext.addPartialTarget(richInputText);
//            
//            FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
//            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//            FacesContext fctx=FacesContext.getCurrentInstance();
//            fctx.addMessage("PO_REF_LABEL_VALUE", msg);
//            
//            //runFocusJSBEN_NUMBER();
//            runFocusJSPO_REF_LABEL_VALUE();
//            check = true;
//        }
//        
        if (check) {
            runJS();
            return;
        }
        runJS();
        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100502);

    }
    
    
    public String confirmPaymentOrder() throws GTechException {
        
        boolean check = false;
        if (StringUtilities.isEmpty(getBeneficaryCode()) || !getBeneficaryCode().equals(String.valueOf(paymentOrderBean.getBeneficiaryBean().getCode()))) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("BENEFICARY_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("BEN_NUMBER", msg);
            check = true;
        }
        
        if (StringUtilities.isEmpty(getAmount())  || ! (Double.parseDouble(getAmount()) == paymentOrderBean.getAmount() )) {
            
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            UIComponent component = facesContext.getViewRoot().findComponent("AMOUNT_FIELD");
            RichInputText richInputText = (RichInputText) component;
            richInputText.clearCachedClientIds();
            richInputText.clearInitialState();
            richInputText.resetValue();
            richInputText.setValue("");
            adfFacesContext.addPartialTarget(richInputText);
            
            FacesMessage msg=new FacesMessage(MessageReader.getMessage("AMOUNT_NOT_EQUALS_PAYMENT_OREDER"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fctx=FacesContext.getCurrentInstance();
            fctx.addMessage("AMOUNT_FIELD", msg);
            check = true;
        }
        
//        if(StringUtilities.isNotEmpty(paymentOrderBean.getBeneficaryName())) {
//            if ( ( StringUtilities.isEmpty(getSecBeneficaryName())  || !getSecBeneficaryName().equals(paymentOrderBean.getBeneficaryName() ))) {
//                responseContainer.addErrorMessage("BENEFICARY_AMOUNT_NOT_EQUALS_PAYMENT_OREDER");
//            }    
//        }
        
        
        if (check) {
            runJS();
            return null;
        }
        if(paymentOrderBean.getPaymentEntryMethod() == 100603 || paymentOrderBean.getPaymentEntryMethod() == 100605) {
            new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100505);
        } else {
            new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100502);
        }
        
        setPoId(null);
        setPoNumber(null);
        setPaymentOrderBean(new PaymentOrderBean());
        getPaymentOrderBean().setBeneficiaryBean(new BeneficiaryBean());
        getPaymentOrderBean().setExpenseBean(new ExpenseBean());
        setBeneficaryCode(null);
        setAmount(null);
        
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent componentPO = facesContext.getViewRoot().findComponent("paymentOrderNumber");
        RichInputText richInputTextPO = (RichInputText) componentPO;
        richInputTextPO.clearCachedClientIds();
        richInputTextPO.clearInitialState();
        richInputTextPO.resetValue();
        richInputTextPO.setValue("");
        adfFacesContext.addPartialTarget(richInputTextPO);
        
        UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
        RichInputText richInputText = (RichInputText) component;
        richInputText.clearCachedClientIds();
        richInputText.clearInitialState();
        richInputText.resetValue();
        richInputText.setValue("");
        adfFacesContext.addPartialTarget(richInputText);
        
        UIComponent componentAmount = facesContext.getViewRoot().findComponent("AMOUNT_FIELD");
        RichInputText richInputTextAmount = (RichInputText) componentAmount;
        richInputTextAmount.clearCachedClientIds();
        richInputTextAmount.clearInitialState();
        richInputTextAmount.resetValue();
        richInputTextAmount.setValue("");
        adfFacesContext.addPartialTarget(richInputTextAmount);
        
        runJS();
        return null;  
    }
        
    public String backFromReviewSinglePaymentOrder() {
       
        setPoId(null);
        setPoNumber(null);
        setPaymentOrderBean(new PaymentOrderBean());
        getPaymentOrderBean().setBeneficiaryBean(new BeneficiaryBean());
        getPaymentOrderBean().setExpenseBean(new ExpenseBean());
        setBeneficaryCode(null);
        setAmount(null);
        
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent component = facesContext.getViewRoot().findComponent("BEN_NUMBER");
        RichInputText richInputText = (RichInputText) component;
        richInputText.clearCachedClientIds();
        richInputText.clearInitialState();
        richInputText.resetValue();
        richInputText.setValue("");
        adfFacesContext.addPartialTarget(richInputText);
        
        UIComponent componentAmount = facesContext.getViewRoot().findComponent("AMOUNT_FIELD");
        RichInputText richInputTextAmount = (RichInputText) componentAmount;
        richInputTextAmount.clearCachedClientIds();
        richInputTextAmount.clearInitialState();
        richInputTextAmount.resetValue();
        richInputTextAmount.setValue("");
        adfFacesContext.addPartialTarget(richInputTextAmount);
        runJS();
        return "PO-F26";
    }

    public String returnToPaymentOrderReviewPage() throws GTechException {

        return "PO-F26";
    }
    
    
    public void runJS(){
        callJavaScriptFunction("$('input').keydown( function(e) {\n" + 
        "                                    \n" + 
        "                                    \n" + 
        "                                    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;\n" + 
        "                                  \n" + 
        "                                    if(key == 13) {\n" + 
        "                                        e.preventDefault();\n" + 
        "                                        var filedVal = this.value;\n" + 
        "                                    if(!filedVal) {\n" + 
        "                                        return;\n" + 
        "                                    }\n" + 
        "                                     var inputs = $(this).closest('form').find(':input:enabled:visible');\n" + 
        "                                         inputs.eq( inputs.index(this)+ 1 ).focus().delay(300);\n" + 
        "                                         inputs.eq( inputs.index(this)+ 1 ).select().delay(300);\n" + 
        "                                    }\n" + 
        "                                });");
    }
    
    public void runJSBen(){
        callJavaScriptFunction("document.getElementById('BEN_NUMBER::content').select();");
    }
    
    public void runJSAmount(){
        callJavaScriptFunction("document.getElementById('AMOUNT_FIELD::content').select();");
    }
    
    public void runJSPoNumber(){
        callJavaScriptFunction("document.getElementById('paymentOrderNumber::content').select();");
    }
    
    public void runFocusJSPO_REF_LABEL_VALUE() {
        callJavaScriptFunction("document.getElementById('PO_REF_LABEL_VALUE::content').select();");
    }
    
    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setBeneficaryCode(String beneficaryCode) {
        this.beneficaryCode = beneficaryCode;
    }

    public String getBeneficaryCode() {
        return beneficaryCode;
    }

    public void setBeneficaryName(String beneficaryName) {
        this.beneficaryName = beneficaryName;
    }

    public String getBeneficaryName() {
        return beneficaryName;
    }

    public void setSecBeneficaryName(String secBeneficaryName) {
        this.secBeneficaryName = secBeneficaryName;
    }

    public String getSecBeneficaryName() {
        return secBeneficaryName;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getPoId() {
        return poId;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }
    
    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getRefNumber() {
        return refNumber;
    }

}

