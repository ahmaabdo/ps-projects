package com.gtech.web.admin.group;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.event.SelectionEvent;

@ManagedBean(name = "groupRoleBackedBean")
@SessionScoped
public class GroupBacking extends GTechModelBacking {

    private GroupBean groupBean = new GroupBean();
    private List<GroupBean> groups = new ArrayList<GroupBean>();
    private List<GroupBean> filteredGroups;
    private boolean selectedFlag;
    private boolean backAction =false;
    private List<SelectItem> listOfRoles =new ArrayList<SelectItem>();
    private final String TABLE_NAME = "panelCollection:searchResultsTable";
    
    public GroupBacking() {
        super();
    }

    @Override
    public String initialize() throws GTechException {
        groupBean = new GroupBean();
        groups = new ArrayList<GroupBean>();
        setSelectedFlag(false);

        return "goToManageGroup";
    }

    public String getAllGroupsAction() throws GTechException {
        RequestContainer<GroupBean> requestData = new RequestContainer<GroupBean>();

        GroupBusiness business = new GroupBusiness();
        ResponseContainer<GroupBean> response = business.getAllGroups(requestData);

        resetManageGroupAction();
        setGroups(response.getDataList());

        return null;
    }

    public String getGroupsAction() throws GTechException {

        RequestContainer<GroupBean> requestData = new RequestContainer<GroupBean>();
        requestData.setDataBean(getGroupBean());

        GroupBusiness business = new GroupBusiness();
        ResponseContainer<GroupBean> response = business.getGroups(requestData);

        if(!isBackAction() && response.containsErrorMessages()) {
            createMessages(response.getErrorMessagesList());
        } else {
            setGroups(response.getDataList());
        }

        return null;
    }

    public String resetManageGroupAction() {
        groupBean = new GroupBean();
        
        if(groups != null){
            groups.clear();
        }
        clearTableSelection();
        return null;
    }


    public String editGroupAction() throws GTechException {
            RequestContainer<GroupBean> requestData = new RequestContainer<GroupBean>();
            requestData.setDataBean(getGroupBean());

            GroupBusiness business = new GroupBusiness();
            ResponseContainer<GroupBean> response = business.updateGroup(requestData);

            if(response.containsErrorMessages()) {
                createMessages(response.getErrorMessagesList());
            } else {
                createMessages(response.getInfoMessagesList());
            }

            return null;
        }

    public String addGroupAction() throws GTechException {
           RequestContainer<GroupBean> requestData = new RequestContainer<GroupBean>();
           
           requestData.setDataBean(getGroupBean());

           GroupBusiness business = new GroupBusiness();
           ResponseContainer<GroupBean> response = business.addGroup(requestData);

           if(response.containsErrorMessages()) {
               createMessages(response.getErrorMessagesList());
           } else {
               createMessages(response.getInfoMessagesList());
               clearAddGroupAction();
           }

           return null;
       }

    public String clearAddGroupAction() {
           setGroupBean(null);
           setGroupBean(new GroupBean());

           return null;
       }



    public String resetEditGroupAction() throws GTechException {
            getGroupFromDB();

            return null;
        }

    public void deleteGroupDialogListener(DialogEvent event) throws GTechException {
            if(event.getOutcome() == DialogEvent.Outcome.no) {
                return;
            }

            RequestContainer<GroupBean> requestData = new RequestContainer<GroupBean>();
            requestData.setDataBean(getSelectedGroupBean());

            GroupBusiness business = new GroupBusiness();
            ResponseContainer<GroupBean> response = business.deleteGroup(requestData);

            if(response.containsErrorMessages()) {
                createMessages(response.getErrorMessagesList());
            } else {
                createMessages(response.getInfoMessagesList());
                backToManageGroup();
                addPartialTrigger("panelCollection");
                addPartialTrigger(TABLE_NAME);
            }
        }


    public void tableSelectionListener(SelectionEvent selection){

          setSelectedFlag(true);
          addPartialTrigger(TABLE_NAME);
        }



    public String goToAddGroupAction() throws GTechException {


            listOfRoles.clear();
            listOfRoles.addAll(getListOfRoleItems());


            setGroupBean(new GroupBean());

            return "goToAddGroup";
        }

    private GroupBean getSelectedGroupBean() {
         return (GroupBean)getSearchReslutsTable().getSelectedRowData();
       }

    private void getGroupFromDB() throws GTechException {
           RequestContainer<GroupBean> requestData = new RequestContainer<GroupBean>();
           requestData.setDataBean(getGroupBean());

           GroupBusiness business = new GroupBusiness();
           ResponseContainer<GroupBean> dataResponse = business.getGroup(requestData);

           setGroupBean(dataResponse.getDataBean());
       }

        public String goToEditGroupAction() throws GTechException {

            setGroupBean(getSelectedGroupBean());
            getGroupFromDB();


            listOfRoles.clear();
            listOfRoles.addAll(getListOfRoleItems());



            return "goToEditGroup";
        }

        public String backToManageGroup() throws GTechException {
            setBackAction(true);
            getGroupsAction();
            setBackAction(false);


            setSelectedFlag(false);
            clearTableSelection();

            return "backToManageGroup";
        }



    public  List<SelectItem> getListOfRoleItems() throws GTechException {
          List <SelectItem> list = new ArrayList<SelectItem>();

          GroupDao groupRoleDAO = new GroupDao();
          list = groupRoleDAO.getAllowedRole();

          return list;
        }


    private void clearTableSelection() {
      if(getSearchReslutsTable() != null && getSearchReslutsTable().getSelectedRowKeys() != null) {
        getSearchReslutsTable().getSelectedRowKeys().clear();
      }
    }
    
    
    private RichTable getSearchReslutsTable() {
      return (RichTable)FacesContext.getCurrentInstance().getViewRoot().findComponent(TABLE_NAME);
    }

    public void setGroupBean(GroupBean groupBean) {
        this.groupBean = groupBean;
    }

    public GroupBean getGroupBean() {
        return groupBean;
    }

    public void setGroups(List<GroupBean> groups) {
        this.groups = groups;
    }

    public List<GroupBean> getGroups() {
        return groups;
    }

    public void setFilteredGroups(List<GroupBean> filteredGroups) {
        this.filteredGroups = filteredGroups;
    }

    public List<GroupBean> getFilteredGroups() {
        return filteredGroups;
    }

    public void setSelectedFlag(boolean selectedFlag) {
        this.selectedFlag = selectedFlag;
    }

    public boolean isSelectedFlag() {
        return selectedFlag;
    }

    public void setListOfRoles(List<SelectItem> listOfRoles) {
        this.listOfRoles = listOfRoles;
    }

    public List<SelectItem> getListOfRoles() {
        return listOfRoles;
    }


    public void setBackAction(boolean backAction) {
        this.backAction = backAction;
    }

    public boolean isBackAction() {
        return backAction;
    }
}
