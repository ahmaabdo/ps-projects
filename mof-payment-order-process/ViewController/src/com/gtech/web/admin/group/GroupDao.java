package com.gtech.web.admin.group;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.common.keys.DataKeys;
import com.gtech.common.util.CollectionUtilities;
import com.gtech.common.util.EncryptionUtilities;
import com.gtech.common.util.StringUtilities;
import com.gtech.faces.beans.SessionLocaleBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public class GroupDao extends GTechDAO{

    private SessionLocaleBean localeBean;
    private String groupQuery;
    private String roleQuery;

    public GroupDao() {
        super();
        localeBean = new SessionLocaleBean();
        localeBean.setUserSessionLocale(DataKeys.APPLICATION_LOCALE);

        groupQuery = "SELECT GRP_ID," +
                    "  GRP_CODE," +
                    "  GRP_DESC_AR," +
                    "  GRP_DESC_EN " +
                    " FROM POG_USER_GROUPS " ;

        roleQuery = " SELECT IGM_ID, " +
                    "  IGM_CODE, " +
                    "  IGM_TITLE_AR, " +
                    "  IGM_TITLE_EN " +
                    "  FROM POG_FORMS_LIST ";
    }


    public ResponseContainer<GroupBean> getAllGroups(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        ResponseContainer<GroupBean> dataResposne = new ResponseContainer<GroupBean>();

        String query = groupQuery;
        try {

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            GroupBean groupBean = null;
            List<GroupBean> groups = new ArrayList<GroupBean>();
            while(rs.next()) {
                groupBean = new GroupBean();
                fillGroupBean(groupBean, rs);
                groups.add(groupBean);
                
                fillGroupRoles(groupBean, connection);
            }

            dataResposne.setDataList(groups);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }


    public List<SelectItem> getAllowedRole() throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<SelectItem> dataResposne = new ArrayList<SelectItem>();

        String query = " SELECT IGM_ID, IGM_CODE, IGM_TITLE_AR, IGM_TITLE_EN FROM POG_FORMS_LIST ";
        try {

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            SelectItem selectItem = new SelectItem();
            while(rs.next()) {
                selectItem = new SelectItem();
                selectItem.setValue(rs.getString("IGM_ID"));
                selectItem.setLabel(rs.getString("IGM_TITLE_AR"));
                dataResposne.add(selectItem);
            }

        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }

    public ResponseContainer<GroupBean> getGroups(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        GroupBean searchBean = RequestContainer.getDataBean();
        ResponseContainer<GroupBean> dataResposne = new ResponseContainer<GroupBean>();
        String query = groupQuery;

        try {

          

            if(StringUtilities.isNotEmpty(searchBean.getFullNameAR())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " GRP_DESC_AR LIKE '%"+ searchBean.getFullNameAR() +"%' ";
            }


            if(StringUtilities.isNotEmpty(searchBean.getFullNameEN())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " GRP_DESC_EN LIKE '%"+ searchBean.getFullNameEN() +"%' ";
            }

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            GroupBean groupBean = null;
            List<GroupBean> groups = new ArrayList<GroupBean>();
            while(rs.next()) {
                groupBean = new GroupBean();
                fillGroupBean(groupBean, rs);
                groups.add(groupBean);
            }

            dataResposne.setDataList(groups);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }


    public ResponseContainer<GroupBean> getGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        GroupBean searchBean = RequestContainer.getDataBean();
        ResponseContainer<GroupBean> dataResposne = new ResponseContainer<GroupBean>();
        String query = groupQuery;

        try {

            if(StringUtilities.isNotEmpty(searchBean.getGroupID())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " grp_id ="+ searchBean.getGroupID() +" ";
            }


            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            GroupBean groupBean = null;
            if(rs.next()) {
                groupBean = new GroupBean();
                fillGroupBean(groupBean, rs);
                fillGroupRoles(groupBean, connection);
            }
            dataResposne.setDataBean(groupBean);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }

    public ResponseContainer<GroupBean> addGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        Connection connection = null;
        GroupBean groupBean = RequestContainer.getDataBean();
        ResponseContainer<GroupBean> dataResposne = new ResponseContainer<GroupBean>();

            try {
        PreparedStatement preparedStatement = null;
        connection = getConnection(false);
        Integer poId = getSeq("GRP_PK_SEQ", connection);
        
        String query =  " INSERT" +
                        " INTO pog_user_groups" +
                        "  (" +
                        "    grp_id," +
                        "    grp_code," +
                        "    GRP_DESC_AR," +
                        "    grp_desc_en" +
                        "  )" +
                        "  VALUES" +
                        "(?,?,?,?)";

        preparedStatement = connection.prepareStatement(query);

        preparedStatement.setInt(1, poId);
        preparedStatement.setString(2, groupBean.getGroupCode());
        preparedStatement.setString(3, groupBean.getFullNameAR());
        preparedStatement.setString(4, groupBean.getFullNameEN());

        preparedStatement.executeUpdate();

        addGroupRoles(String.valueOf(poId), groupBean.getListOfRole(), connection);

            connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return dataResposne;
    }

    public ResponseContainer<GroupBean> updateGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        GroupBean groupBean = RequestContainer.getDataBean();
        ResponseContainer<GroupBean> dataResposne = new ResponseContainer<GroupBean>();

        try {
            connection = getConnection(false);
            
            String query ="UPDATE pog_user_groups" +
            " SET "+
            "   grp_code  = ?" +
            " , GRP_DESC_AR  = ?" +
            " , grp_desc_en = ?" +
            "   WHERE grp_id = ?" ;

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, groupBean.getGroupCode());
            preparedStatement.setString(2, groupBean.getFullNameAR());
            preparedStatement.setString(3, groupBean.getFullNameEN());
            

            preparedStatement.setString(4, groupBean.getGroupID());

            deleteGroupRoles(groupBean.getGroupID(), connection);
            addGroupRoles(groupBean.getGroupID(), groupBean.getListOfRole(), connection);

            preparedStatement.executeUpdate();

            connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return dataResposne;
    }

    public ResponseContainer<GroupBean> deleteGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        GroupBean groupBean = RequestContainer.getDataBean();
        ResponseContainer<GroupBean> dataResposne = new ResponseContainer<GroupBean>();

        try {
            connection = getConnection(false);
            
            String query ="DELETE FROM pog_user_groups WHERE grp_id = ?";

            deleteGroupRoles(groupBean.getGroupID(), connection);

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, groupBean.getGroupID());

            preparedStatement.executeUpdate();

            connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return dataResposne;
    }

    private void addGroupRoles(String groupId, List<String> roles, Connection connection) throws GTechException {

        if(roles == null) {
            return;
        }

        PreparedStatement preparedStatement = null;

        try {
            Integer poId = 0;

            String query =
                " INSERT" +
                " INTO POG_USER_PRIVS" +
                "  (" +
                "    RES_ID," +
                "    RES_GRP_ID," +
                "    RES_IGM_ID " +
                "  )" +
                "  VALUES" +
                "(?,?,?)";

            preparedStatement = connection.prepareStatement(query);

            for(String role : roles) {
                poId = getSeq("RES_PK_SEQ", connection);

                preparedStatement.setLong(1, poId);
                preparedStatement.setString(2, groupId);
                preparedStatement.setString(3, role);

                preparedStatement.executeUpdate();
            }

        } catch(SQLException sqle){
            throw new GTechException(sqle);
        } finally {
            closeResources(preparedStatement);
        }
    }

    private void deleteGroupRoles(String groupId, Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection(false);

            String query ="DELETE FROM POG_USER_PRIVS WHERE RES_GRP_ID = ?";

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, groupId);

            preparedStatement.executeUpdate();

        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }
    }

    private void fillGroupRoles(GroupBean group, Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<String> roleList = new ArrayList<String>();
        List<String> roleListDesc = new ArrayList<String>();
        String roleDesc = "";
    
        String query = " SELECT RES_ID, RES_GRP_ID, RES_IGM_ID , " +
                       " (SELECT IGM_TITLE_AR FROM POG_FORMS_LIST WHERE IGM_ID = POG_USER_PRIVS.RES_IGM_ID) AS RES_IGM_DESC "+
                       " FROM POG_USER_PRIVS WHERE RES_GRP_ID = ?";
        try {
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, group.getGroupID());
            rs = preparedStatement.executeQuery();

            GroupBean groupBean = null;
            List<GroupBean> groups = new ArrayList<GroupBean>();

            while(rs.next()) {
                roleList.add(rs.getString("RES_IGM_ID"));
                roleListDesc.add(rs.getString("RES_IGM_DESC"));
            }
            
            for(String role :roleListDesc){
                if(StringUtilities.isEmpty(roleDesc)){
                    roleDesc +=role;
                } else {
                    roleDesc +=" , "+role;
                }
            }
            
            group.setListOfRoleDesc(roleDesc);
            group.setListOfRole(roleList);

        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(preparedStatement, rs);
        }

    }

    private void fillGroupBean(GroupBean groupBean, ResultSet rs) throws SQLException, GTechException {

        groupBean.setFullNameAR(rs.getString("GRP_DESC_AR"));
        groupBean.setFullNameEN(rs.getString("GRP_DESC_EN"));
        groupBean.setGroupCode(rs.getString("GRP_CODE"));
        groupBean.setGroupID(rs.getString("GRP_ID"));
       
    }
}
