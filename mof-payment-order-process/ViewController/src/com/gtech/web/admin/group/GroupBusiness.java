package com.gtech.web.admin.group;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;

import com.gtech.common.model.GTechModelBusiness;

import java.util.List;

public class GroupBusiness extends GTechModelBusiness {
    public GroupBusiness() {
        super();
    }
    
    
    public ResponseContainer<GroupBean> getAllGroups(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        GroupDao groupDAO = new GroupDao();
        ResponseContainer<GroupBean> response = groupDAO.getAllGroups(RequestContainer);                
        
        return response;
    }
    
    public ResponseContainer<GroupBean> getGroups(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        GroupBean groupBean = RequestContainer.getDataBean();
        boolean success = validateOneRequiredValue(groupBean.getFullNameAR(),groupBean.getFullNameEN());
        
        if(!success) {
            addErrorMessage("ONE_VALUE_REQUIRED");
            return getResponseContainer();
        }
        
        GroupDao groupDAO = new GroupDao();
        ResponseContainer<GroupBean> response = groupDAO.getGroups(RequestContainer);                
        
        return response;
    }
    
    public ResponseContainer<GroupBean> getGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        GroupDao groupDAO = new GroupDao();
        ResponseContainer<GroupBean> response = groupDAO.getGroup(RequestContainer);                
        
        return response;
    }
    
    public ResponseContainer<GroupBean> addGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        GroupBean groupBean = RequestContainer.getDataBean();
        
        validateGroupBean(groupBean);
        if(getResponseContainer().containsErrorMessages()) {
            return getResponseContainer();
        }
        
        if(groupBean.getFullNameAR().contains("'")) {
            groupBean.setFullNameAR(groupBean.getFullNameAR().replaceAll("'", "''"));
        }  
        
        if(groupBean.getFullNameEN().contains("'")) {
            groupBean.setFullNameEN(groupBean.getFullNameEN().replaceAll("'", "''"));
        }  
        
        GroupDao groupDAO = new GroupDao();
        ResponseContainer<GroupBean> response = groupDAO.addGroup(RequestContainer);                                
        
        response.addInfoMessage("ADD_SUCCESS_MESSAGE");
        
        return response;
    }
    
    public ResponseContainer<GroupBean> updateGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        GroupBean groupBean = RequestContainer.getDataBean();
        
        validateGroupBean(groupBean);
        if(getResponseContainer().containsErrorMessages()) {
            return getResponseContainer();
        }
        
        if(groupBean.getFullNameAR().contains("'")) {
            groupBean.setFullNameAR(groupBean.getFullNameAR().replaceAll("'", "''"));
        }      
        
        if(groupBean.getFullNameEN().contains("'")) {
            groupBean.setFullNameEN(groupBean.getFullNameEN().replaceAll("'", "''"));
        } 
        
        GroupDao groupDAO = new GroupDao();
        ResponseContainer<GroupBean> response = groupDAO.updateGroup(RequestContainer);                
        
        response.addInfoMessage("UPDATE_SUCCESS_MESSAGE");
        
        return response;
    }
    
    public ResponseContainer<GroupBean> deleteGroup(RequestContainer<GroupBean> RequestContainer) throws GTechException {
        GroupBean groupBean = RequestContainer.getDataBean();
        GroupDao groupDAO = new GroupDao();
        
    
        ResponseContainer<GroupBean> response = groupDAO.deleteGroup(RequestContainer);                
        
        response.addInfoMessage("DELETE_SUCCESS_MESSAGE");
        
        return response;
    }
    
    private void validateGroupBean(GroupBean groupBean) {
        validateRequiredValue(groupBean.getFullNameAR(), "GROUPNAME_AR_REQUIRED");        
    }    
    
}
