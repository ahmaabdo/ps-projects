package com.gtech.web.admin.group;

import com.gtech.common.base.BaseBean;

import java.util.ArrayList;
import java.util.List;

public class GroupBean extends BaseBean {
    
    private String groupID;
    private String fullNameAR;
    private String fullNameEN;
    private String groupCode;
    private String listOfRoleDesc;
    private List<String> listOfRole ; 
    
    public GroupBean() {
        super();
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setFullNameAR(String fullNameAR) {
        this.fullNameAR = fullNameAR;
    }

    public String getFullNameAR() {
        return fullNameAR;
    }

    public void setFullNameEN(String fullNameEN) {
        this.fullNameEN = fullNameEN;
    }

    public String getFullNameEN() {
        return fullNameEN;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setListOfRole(List<String> listOfRole) {
        this.listOfRole = listOfRole;
    }

    public List<String> getListOfRole() {
        return listOfRole;
    }


    public void setListOfRoleDesc(String listOfRoleDesc) {
        this.listOfRoleDesc = listOfRoleDesc;
    }

    public String getListOfRoleDesc() {
        return listOfRoleDesc;
    }
}
