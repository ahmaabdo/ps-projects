package com.gtech.web.admin.user;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.common.keys.DataKeys;
import com.gtech.common.util.EncryptionUtilities;
import com.gtech.common.util.StringUtilities;
import com.gtech.faces.beans.SessionLocaleBean;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.organization.bean.OrganizationBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public class UserDao extends GTechDAO{

    private SessionLocaleBean localeBean;
    private String userQuery;
    private String roleQuery;
    private String userRoleQuery;

    public UserDao() {
        super();
        localeBean = new SessionLocaleBean();
        localeBean.setUserSessionLocale(DataKeys.APPLICATION_LOCALE);

        userQuery = "SELECT USR.SSC_ID," + 
        "       USR.SSC_USERNAME," + 
        "       USR.SSC_NAME_AR," + 
        "       USR.SSC_NAME_EN," + 
        "       USR.SSC_PASSWORD," + 
        "       USR.SSC_STATUS," + 
        "       USR.SSC_ENTITY_COMBINATION, " + 
        "       USR.SSC_GRP_ID, " + 
        "       USR.SSC_EEO_ID,   " + 
        "       USR.SSC_TTG_ID,   " + 
        "       USR.SSC_PTN_ID," + 
        "       ENT.*," + 
        "       DPT.*," + 
        "       SCT.*,   " + 
        "       ORG.*,   " + 
        "       USR.SSC_EMAIL,   " + 
        "       USR.SSC_PHONE_NO   " + 
        "     FROM POG_USERS USR," + 
        "          POG_ENTITIES ENT," + 
        "          POG_DEPARTMENTS DPT," + 
        "          POG_SECTION SCT, " + 
        "          POG_ORGANIZATION ORG" + 
        "     WHERE ENT.EEO_ID(+) = USR.SSC_EEO_ID" + 
        "           AND  DPT.TTG_ID(+) = USR.SSC_TTG_ID" + 
        "           AND SCT.PTN_ID(+) = USR.SSC_PTN_ID" +
        "           AND ORG.ORG_ID(+) = USR.SSC_ORG_ID " ;

        roleQuery = " SELECT IGM_ID, " +
                    "  IGM_CODE, " +
                    "  IGM_TITLE_AR, " +
                    "  IGM_TITLE_EN " +
                    "  FROM POG_FORMS_LIST ";
    }


    public ResponseContainer<UserBean> getAllUsers(RequestContainer<UserBean> RequestContainer) throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();

        String query = userQuery;
        try {

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            UserBean userBean = null;
            List<UserBean> users = new ArrayList<UserBean>();
            while(rs.next()) {
                userBean = new UserBean();
                fillUserBean(userBean, rs);
                users.add(userBean);
            }

            dataResposne.setDataList(users);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }


    public List<SelectItem> getAllowedRole() throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<SelectItem> dataResposne = new ArrayList<SelectItem>();

        String query = " SELECT IGM_ID, IGM_CODE, IGM_TITLE_AR, IGM_TITLE_EN FROM POG_FORMS_LIST ";
        try {

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            SelectItem selectItem = new SelectItem();
            while(rs.next()) {
                selectItem = new SelectItem();
                selectItem.setValue(rs.getString("IGM_ID"));
                selectItem.setLabel(rs.getString("IGM_TITLE_AR"));
                dataResposne.add(selectItem);
            }

        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }

    public ResponseContainer<UserBean> getUsers(RequestContainer<UserBean> RequestContainer) throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        UserBean searchBean = RequestContainer.getDataBean();
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();
        String query = userQuery;

        try {

            if(StringUtilities.isNotEmpty(searchBean.getUserName())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " SSC_USERNAME LIKE '%"+ searchBean.getUserName() +"%' ";
            }

            if(StringUtilities.isNotEmpty(searchBean.getFullNameAR())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " SSC_NAME_AR LIKE '%"+ searchBean.getFullNameAR() +"%' ";
            }


            if(StringUtilities.isNotEmpty(searchBean.getFullNameEN())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " SSC_NAME_EN LIKE '%"+ searchBean.getFullNameEN() +"%' ";
            }

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            UserBean userBean = null;
            List<UserBean> users = new ArrayList<UserBean>();
            while(rs.next()) {
                userBean = new UserBean();
                fillUserBean(userBean, rs);
                users.add(userBean);
            }

            dataResposne.setDataList(users);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }


    public ResponseContainer<UserBean> getUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        UserBean searchBean = RequestContainer.getDataBean();
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();
        String query = userQuery;

        try {

            if(StringUtilities.isNotEmpty(searchBean.getUserName())) {
                if(!query.contains("WHERE")){
                    query += " WHERE ";
                } else {
                    query += " AND ";
                }

                query += " SSC_ID ="+ searchBean.getUserIndex() +" ";
            }


            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            UserBean userBean = null;
            if(rs.next()) {
                userBean = new UserBean();
                fillUserBean(userBean, rs);
                fillUserRoles(userBean, connection);
            }
            dataResposne.setDataBean(userBean);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

        return dataResposne;
    }

    public ResponseContainer<UserBean> addUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        Connection connection = null;
        UserBean userBean = RequestContainer.getDataBean();
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();

        try {
            PreparedStatement preparedStatement = null;
            connection = getConnection(false);
            Integer poId = getSeq("SSC_PK_SEQ", connection);
            String query =
                " INSERT" +
                " INTO POG_USERS" +
                "  (" +
                "    SSC_ID," +
                "    SSC_USERNAME," +
                "    SSC_NAME_AR," +
                "    SSC_NAME_EN," +
                "    SSC_PASSWORD," +
                "    SSC_STATUS," +
                "    SSC_GRP_ID,   " +
                "    SSC_EEO_ID,   " +
                "    SSC_TTG_ID,   " +
                "    SSC_PTN_ID,   " +
                "    SSC_EMAIL,   " +
                "    SSC_PHONE_NO   " +
                "  )" +
                "  VALUES" +
                "(?,?,?,?,?,?,?,?,?,?,?,?)";
    
            preparedStatement = connection.prepareStatement(query);
    
            preparedStatement.setInt(1, poId);
            preparedStatement.setString(2, userBean.getUserName());
            preparedStatement.setString(3, userBean.getFullNameAR());
            preparedStatement.setString(4, userBean.getFullNameEN());
            preparedStatement.setString(5, EncryptionUtilities.desEncrypt(userBean.getPassword()) );
            preparedStatement.setString(6, userBean.getStatus());
            preparedStatement.setString(7, userBean.getGroupID());
            if(userBean.getEntityBean().getId() > 0) {
                preparedStatement.setLong(8, userBean.getEntityBean().getId());
            } else {
                preparedStatement.setString(8, null);
            }
            
            if(userBean.getDepartmentBean().getId() > 0) {
                preparedStatement.setLong(9, userBean.getDepartmentBean().getId());
            } else {
                preparedStatement.setString(9, null);
            }
            if(userBean.getSectionBean() != null && userBean.getSectionBean().getId() > 0) {
                preparedStatement.setLong(10, userBean.getSectionBean().getId());            
            } else {
                preparedStatement.setString(10, null);            
            }
            preparedStatement.setString(11, userBean.getEmail());
            preparedStatement.setString(12, userBean.getPhoneNo());
            
    
            preparedStatement.executeUpdate();
    
    //        addUserRoles(String.valueOf(poId), userBean.getListOfRole(), connection);
    
                connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return dataResposne;
    }

    public ResponseContainer<UserBean> updateUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        UserBean userBean = RequestContainer.getDataBean();
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();

        try {
            connection = getConnection(false);

            String query ="UPDATE POG_USERS" +
            " SET "+
            "   SSC_NAME_AR  = ?" +
            " , SSC_NAME_EN  = ?" +
            " , SSC_PASSWORD = ?" +
            " , SSC_STATUS   = ?" +
            " , SSC_ENTITY_COMBINATION   = ?" +
            " , SSC_GRP_ID   = ?" +
            " , SSC_EMAIL = ? " +
            " , SSC_PHONE_NO = ?  " +
            "   WHERE SSC_ID = ?" ;

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, userBean.getFullNameAR());
            preparedStatement.setString(2, userBean.getFullNameEN());
            preparedStatement.setString(3, EncryptionUtilities.desEncrypt(userBean.getPassword()));
            preparedStatement.setString(4, userBean.getStatus());
            preparedStatement.setString(5, userBean.getEecCode());
            preparedStatement.setString(6, userBean.getGroupID());
            
            preparedStatement.setString(7, userBean.getEmail());
            preparedStatement.setString(8, userBean.getPhoneNo());
            
            preparedStatement.setString(9, userBean.getUserIndex());

//            deleteUserRoles(userBean.getUserIndex(), connection);
//            addUserRoles(userBean.getUserIndex(), userBean.getListOfRole(), connection);

            preparedStatement.executeUpdate();

            connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return dataResposne;
    }

    public ResponseContainer<UserBean> deleteUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        UserBean userBean = RequestContainer.getDataBean();
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();

        try {
            connection = getConnection(false);

            String query ="DELETE FROM POG_USERS WHERE SSC_ID = ?";

            deleteUserRoles(userBean.getUserIndex(), connection);

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, userBean.getUserIndex());

            preparedStatement.executeUpdate();

            connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return dataResposne;
    }

    public boolean isValueUsed(UserBean userBean) throws GTechException {
        return valueUsed("ECP_USER", "USER_ID",userBean.getUserIndex(), "ECP_USER_ROLE");
    }

    private void addUserRoles(String userId, List<String> roles, Connection connection) throws GTechException {

        if(roles == null) {
            return;
        }

        PreparedStatement preparedStatement = null;

        try {
            Integer poId = 0;

            String query =
                " INSERT" +
                " INTO POG_USER_PRIVS" +
                "  (" +
                "    RES_ID," +
                "    RES_SSC_ID," +
                "    RES_IGM_ID " +
                "  )" +
                "  VALUES" +
                "(?,?,?)";

            preparedStatement = connection.prepareStatement(query);

            for(String role : roles) {
                poId = getSeq("RES_PK_SEQ", connection);

                preparedStatement.setLong(1, poId);
                preparedStatement.setString(2, userId);
                preparedStatement.setString(3, role);

                preparedStatement.executeUpdate();
            }

        } catch(SQLException sqle){
            throw new GTechException(sqle);
        } finally {
            closeResources(preparedStatement);
        }
    }

    private void deleteUserRoles(String userId, Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection(false);

            String query ="DELETE FROM POG_USER_PRIVS WHERE RES_SSC_ID = ?";

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, userId);

            preparedStatement.executeUpdate();

        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }
    }

    private void fillUserRoles(UserBean user, Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<String> roleList = new ArrayList<String>();
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();

        String query = "SELECT RES_ID, RES_SSC_ID, RES_IGM_ID FROM POG_USER_PRIVS WHERE RES_SSC_ID = ?";
        try {
            preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, user.getUserIndex());
            rs = preparedStatement.executeQuery();

            UserBean userBean = null;
            List<UserBean> users = new ArrayList<UserBean>();

            while(rs.next()) {
                roleList.add(rs.getString("RES_IGM_ID"));
            }
            user.setListOfRole(roleList);

            dataResposne.setDataList(users);
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(preparedStatement, rs);
        }

    }

    private void fillUserBean(UserBean userBean, ResultSet rs) throws SQLException, GTechException {

        userBean.setFullNameAR(rs.getString("SSC_NAME_AR"));
        userBean.setFullNameEN(rs.getString("SSC_NAME_EN"));
        userBean.setPassword(EncryptionUtilities.desDecrypt(rs.getString("SSC_PASSWORD")));
//        userBean.setPermition(rs.getString("USER_EMAIL"));
        userBean.setStatus(rs.getString("SSC_STATUS"));
        userBean.setUserIndex(rs.getString("SSC_ID"));
        userBean.setUserName(rs.getString("SSC_USERNAME"));
//        userBean.setEecCode(rs.getString("SSC_ENTITY_COMBINATION"));
        userBean.setGroupID(rs.getString("SSC_GRP_ID"));
        
        userBean.setGroupID(rs.getString("SSC_GRP_ID"));
        
        userBean.getEntityBean().setId(rs.getLong("SSC_EEO_ID"));
        userBean.getEntityBean().setCode(rs.getString("EEO_CODE"));
        userBean.getEntityBean().setNameAr(rs.getString("EEO_NAME_AR"));
        
        userBean.getDepartmentBean().setId(rs.getLong("SSC_TTG_ID"));
        userBean.getDepartmentBean().setCode(rs.getString("TTG_CODE"));
        userBean.getDepartmentBean().setNameAr(rs.getString("TTG_NAME_AR"));
        
        userBean.getSectionBean().setId(rs.getLong("SSC_PTN_ID"));
        userBean.getSectionBean().setCode(rs.getString("PTN_CODE"));
        userBean.getSectionBean().setNameAr(rs.getString("PTN_NAME_AR"));
        
        userBean.setOrganizationBean(new OrganizationBean());
        userBean.getOrganizationBean().setNameAr(rs.getString("ORG_NAME_AR"));
        userBean.getOrganizationBean().setId(rs.getLong("ORG_ID"));
        userBean.getOrganizationBean().setOrgCode(rs.getString("ORG_CODE"));
        
        userBean.setEmail(rs.getString("SSC_EMAIL"));
        userBean.setPhoneNo(rs.getString("SSC_PHONE_NO"));
//        if(rs.getString("SSC_ENTITY_COMBINATION") != null) {
//            userBean.setEecDesc(getLookupValueAutoFilled(rs.getString("SSC_ENTITY_COMBINATION"), "ENTITY_COMB").getNameAr());    
//        }
        
                                                                        
    }

   public boolean updatePassowrd(String newPassowrd,String userId)  throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getConnection(false);

            String query ="UPDATE POG_USERS" +
            " SET "+
            "  SSC_PASSWORD = ?" +
            "  WHERE SSC_ID = ?" ;

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, EncryptionUtilities.desEncrypt(newPassowrd));
            preparedStatement.setString(2, userId);

            preparedStatement.executeUpdate();

            connection.commit();
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection);
        }

        return true;
    }
   
   
    public LookupBean getLookupValueAutoFilled(String code , String lookupName) throws GTechException {
        LookupBean lookupBean = new LookupBean();
        StringBuilder query = new StringBuilder( "SELECT V_KEY_INDEX, V_CODE, V_DESC_AR, V_DESC_EN FROM POG_LOV_VALUES_V WHERE V_CODE = ?  AND V_SOURCE_TABLE = ?");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        try {
            
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, code.toUpperCase());
            statment.setString(2, lookupName);

            rs = statment.executeQuery();
            if (rs.next()) {
                lookupBean.setNameAr(rs.getString("V_DESC_AR"));
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupBean;
    }
    
    public List<UserBean> getAllMOFUsers() throws GTechException {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        ResponseContainer<UserBean> dataResposne = new ResponseContainer<UserBean>();

        String query ="SELECT SSC_ID," +
                        "  SSC_USERNAME," +
                        "  SSC_NAME_AR," +
                        "  SSC_NAME_EN," +
                        "  SSC_PASSWORD," +
                        "  SSC_STATUS," +
                        "  SSC_ENTITY_COMBINATION, " +
                        "  SSC_GRP_ID " +
                        " FROM POG_USERS  WHERE SSC_EEO_ID IS NULL AND SSC_TTG_ID IS NULL AND SSC_PTN_ID IS NULL" ;
        try {

            connection = getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            UserBean userBean = null;
            List<UserBean> users = new ArrayList<UserBean>();
            while(rs.next()) {
                userBean = new UserBean();
                userBean.setFullNameAR(rs.getString("SSC_NAME_AR"));
                userBean.setFullNameEN(rs.getString("SSC_NAME_EN"));
                userBean.setPassword(EncryptionUtilities.desDecrypt(rs.getString("SSC_PASSWORD")));
                //        userBean.setPermition(rs.getString("USER_EMAIL"));
                userBean.setStatus(rs.getString("SSC_STATUS"));
                userBean.setUserIndex(rs.getString("SSC_ID"));
                userBean.setUserName(rs.getString("SSC_USERNAME"));
                //        userBean.setEecCode(rs.getString("SSC_ENTITY_COMBINATION"));
                userBean.setGroupID(rs.getString("SSC_GRP_ID"));
                
                userBean.setGroupID(rs.getString("SSC_GRP_ID"));
                users.add(userBean);
            }

            return users;
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, stmt, rs);
        }

    }
    
    public List<UserBean> getUsersByRef(long enitytId,long deptId,long secId) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = userQuery;
        query = query + "  AND     USR.SSC_EEO_ID = ?   " + 
                "  AND     USR.SSC_TTG_ID = ?  " + 
                "  AND     USR.SSC_PTN_ID = ? ";
        try {

         

            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, enitytId);
            preparedStatement.setLong(2, deptId);
            preparedStatement.setLong(3, secId);
            
            rs = preparedStatement.executeQuery();

            UserBean userBean = null;
            List<UserBean> users = new ArrayList<UserBean>();
            while(rs.next()) {
                userBean = new UserBean();
                fillUserBean(userBean, rs);
                users.add(userBean);
            }

            return users;
        } catch(SQLException sqle) {
            throw new GTechException(sqle);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }

    }
    
    public boolean updateUserOrg(Long userId,Long orgId)  throws GTechException {
         Connection connection = null;
         PreparedStatement preparedStatement = null;

         try {
             connection = getConnection(false);

             String query ="UPDATE POG_USERS" +
             " SET "+
             "  SSC_ORG_ID = ?" +
             "  WHERE SSC_ID = ?" ;

             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, orgId);
             preparedStatement.setLong(2, userId);

             preparedStatement.executeUpdate();

             connection.commit();
         } catch(SQLException sqle) {
             throw new GTechException(sqle);
         } finally {
             closeResources(connection);
         }

         return true;
     }
    
    public boolean validateUserName(String userName) {
        Connection connection = null;
        PreparedStatement prst = null;
        ResultSet rs = null;
        String query = "SELECT 1 FROM POG_USERS WHERE UPPER(SSC_USERNAME) = ?";

        try {
        
            connection = getConnection();
            prst = connection.prepareStatement(query);
            
            prst.setString(1, userName.toUpperCase());
            rs = prst.executeQuery();

            if(rs.next()) {
                return true;
            }
        } catch(GTechException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, prst, rs);
        }

        return false;
    }
    
    public boolean updateStatus(String newStatus,String userId)  throws GTechException {
         Connection connection = null;
         PreparedStatement preparedStatement = null;

         try {
             connection = getConnection(false);

             String query ="UPDATE POG_USERS" +
             " SET "+
             "  SSC_STATUS = ?" +
             "  WHERE SSC_ID = ?" ;

             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setString(1, newStatus);
             preparedStatement.setString(2, userId);

             preparedStatement.executeUpdate();

             connection.commit();
         } catch(SQLException sqle) {
             throw new GTechException(sqle);
         } finally {
             closeResources(connection);
         }

         return true;
     }
}
