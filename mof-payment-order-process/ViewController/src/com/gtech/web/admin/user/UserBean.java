package com.gtech.web.admin.user;

import com.gtech.common.base.BaseBean;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.organization.bean.OrganizationBean;
import com.gtech.web.section.bean.SectionBean;

import java.util.List;

public class UserBean extends BaseBean {
    
    private String userName;
    private String userIndex;
    private String Password;
    private String fullNameAR;
    private String fullNameEN;
    private String eecID;
    private String eecCode;
    private String eecDesc;
    private String status;
    private String permition;
    private String groupID;
    private List<String> listOfRole ; 
    private EntityBean entityBean = new EntityBean();
    private DepartmentBean departmentBean = new DepartmentBean();
    private SectionBean sectionBean = new SectionBean();
    private OrganizationBean organizationBean = new OrganizationBean();
    private boolean selected;
    private String email;
    private String phoneNo;
    
    public UserBean() {
        super();
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserIndex(String userIndex) {
        this.userIndex = userIndex;
    }

    public String getUserIndex() {
        return userIndex;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getPassword() {
        return Password;
    }

    public void setFullNameAR(String fullNameAR) {
        this.fullNameAR = fullNameAR;
    }

    public String getFullNameAR() {
        return fullNameAR;
    }

    public void setFullNameEN(String fullNameEN) {
        this.fullNameEN = fullNameEN;
    }

    public String getFullNameEN() {
        return fullNameEN;
    }

    public void setEecID(String eecID) {
        this.eecID = eecID;
    }

    public String getEecID() {
        return eecID;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setPermition(String permition) {
        this.permition = permition;
    }

    public String getPermition() {
        return permition;
    }

    public void setListOfRole(List<String> listOfRole) {
        this.listOfRole = listOfRole;
    }

    public List<String> getListOfRole() {
        return listOfRole;
    }

    public void setEecCode(String eecCode) {
        this.eecCode = eecCode;
    }

    public String getEecCode() {
        return eecCode;
    }

    public void setEecDesc(String eecDesc) {
        this.eecDesc = eecDesc;
    }

    public String getEecDesc() {
        return eecDesc;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setEntityBean(EntityBean entityBean) {
        this.entityBean = entityBean;
    }

    public EntityBean getEntityBean() {
        return entityBean;
    }

    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }

    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }

    public void setSectionBean(SectionBean sectionBean) {
        this.sectionBean = sectionBean;
    }

    public SectionBean getSectionBean() {
        return sectionBean;
    }

    public void setOrganizationBean(OrganizationBean organizationBean) {
        this.organizationBean = organizationBean;
    }

    public OrganizationBean getOrganizationBean() {
        return organizationBean;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }
}
