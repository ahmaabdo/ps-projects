package com.gtech.web.admin.user;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;

import java.util.List;

public class UserBusiness extends GTechModelBusiness {
    public UserBusiness() {
        super();
    }
    
    
    public ResponseContainer<UserBean> getAllUsers(RequestContainer<UserBean> RequestContainer) throws GTechException {
        UserDao userDAO = new UserDao();
        ResponseContainer<UserBean> response = userDAO.getAllUsers(RequestContainer);                
        
        return response;
    }
    
    public ResponseContainer<UserBean> getUsers(RequestContainer<UserBean> RequestContainer) throws GTechException {
        UserBean userBean = RequestContainer.getDataBean();
        boolean success = validateOneRequiredValue(userBean.getUserName(),userBean.getFullNameAR(),userBean.getFullNameEN());
        
        if(!success) {
            addErrorMessage("ONE_VALUE_REQUIRED");
            return getResponseContainer();
        }
        
        UserDao userDAO = new UserDao();
        ResponseContainer<UserBean> response = userDAO.getUsers(RequestContainer);                
        
        return response;
    }
    
    public ResponseContainer<UserBean> getUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        UserDao userDAO = new UserDao();
        ResponseContainer<UserBean> response = userDAO.getUser(RequestContainer);                
        
        return response;
    }
    
    public ResponseContainer<UserBean> addUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        UserBean userBean = RequestContainer.getDataBean();
        UserDao userDAO = new UserDao();
        
        validateUserBean(userBean);
        if(getResponseContainer().containsErrorMessages()) {
            return getResponseContainer();
        }
        
        validateRequiredValue(userBean.getUserName(), "USERNAME_REQUIRED");        
        validateRequiredValue(userBean.getStatus(), "USER_STATUS_REQUIRED");
        boolean b = userDAO.validateUserName(userBean.getUserName());
        if (b) {
            getResponseContainer().addErrorMessage("USER_NAME_ALREADY_IN_USE");
            
            return getResponseContainer();
        }
        
        if(userBean.getUserName().contains("'")) {
            userBean.setUserName(userBean.getUserName().replaceAll("'", "''"));
        }            
        
       
        ResponseContainer<UserBean> response = userDAO.addUser(RequestContainer);                                
        
        response.addInfoMessage("ADD_SUCCESS_MESSAGE");
        
        return response;
    }
    
    public ResponseContainer<UserBean> updateUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        UserBean userBean = RequestContainer.getDataBean();
        
        validateUserBean(userBean);
        if(getResponseContainer().containsErrorMessages()) {
            return getResponseContainer();
        }
        
        if(userBean.getUserName().contains("'")) {
            userBean.setUserName(userBean.getUserName().replaceAll("'", "''"));
        }            
        
        UserDao userDAO = new UserDao();
        ResponseContainer<UserBean> response = userDAO.updateUser(RequestContainer);                
        
        response.addInfoMessage("UPDATE_SUCCESS_MESSAGE");
        
        return response;
    }
    
    public ResponseContainer<UserBean> deleteUser(RequestContainer<UserBean> RequestContainer) throws GTechException {
        UserBean userBean = RequestContainer.getDataBean();
        UserDao userDAO = new UserDao();
        
        if(userDAO.isValueUsed(userBean)) {
            getResponseContainer().addErrorMessage("USER_DELETE_VALIDATION", userBean.getUserName());
            return getResponseContainer();
        }
        
        ResponseContainer<UserBean> response = userDAO.deleteUser(RequestContainer);                
        
        response.addInfoMessage("DELETE_SUCCESS_MESSAGE");
        
        return response;
    }
    
    public boolean updatePassowrd(String newPassowrd,String userId) throws GTechException {
        UserDao userDAO = new UserDao();
        userDAO.updatePassowrd(newPassowrd, userId);
        return true;
    }
    
    private void validateUserBean(UserBean userBean) {
        validateRequiredValue(userBean.getUserName(), "USERNAME_REQUIRED");        
        validateRequiredValue(userBean.getStatus(), "USER_STATUS_REQUIRED");
    }    
    public List<UserBean> getAllMOFUsers() throws GTechException {
        UserDao userDAO = new UserDao();
        return userDAO.getAllMOFUsers();
    }
    
    public List<UserBean> getUsersByRef(long enitytId,long deptId,long secId) throws GTechException {
        UserDao userDAO = new UserDao();
        return userDAO.getUsersByRef(enitytId, deptId, secId);
    }
    
    public boolean updateUserOrg(List<UserBean> userList)  throws GTechException {
        UserDao userDAO = new UserDao();
        for(UserBean bean : userList) {
            userDAO.updateUserOrg(Long.parseLong(bean.getUserIndex()) , bean.getOrganizationBean().getId());
        }
        return true;
    }
    
    public boolean updateStatus(String newStatus,String userId) throws GTechException {
        UserDao userDAO = new UserDao();
        userDAO.updateStatus(newStatus, userId);
        return true;
    }
}   
