package com.gtech.web.admin.user;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.util.StringUtilities;
import com.gtech.web.admin.group.GroupBean;
import com.gtech.web.admin.group.GroupBusiness;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;
import com.gtech.web.section.bean.SectionBean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.event.SelectionEvent;

@ManagedBean(name = "manageUsersBacking")
@SessionScoped
public class UserBacking extends GTechModelBacking {

    private UserBean userBean = new UserBean();
    private List<UserBean> users = new ArrayList<UserBean>();
    private List<GroupBean> groups = new ArrayList<GroupBean>();
    private List<UserBean> filteredUsers;
    private boolean selectedFlag;
    private boolean backAction = false;
    private List<SelectItem> listOfRoles = new ArrayList<SelectItem>();
    private final String TABLE_NAME = "panelCollection:searchResultsTable";
    private String oldPassoword;
    private String newPassoword;
    private String confirmedPassoword;

    public UserBacking() {
        super();
    }

    @Override
    public String initialize() throws GTechException {
        userBean = new UserBean();
        users = new ArrayList<UserBean>();
        setSelectedFlag(false);

        return "goToManageUser";
    }

    public String getAllUsersAction() throws GTechException {
        RequestContainer<UserBean> requestData = new RequestContainer<UserBean>();

        UserBusiness business = new UserBusiness();
        ResponseContainer<UserBean> response = business.getAllUsers(requestData);

        resetManageUserAction();
        setUsers(response.getDataList());

        return null;
    }

    public String getUsersAction() throws GTechException {

        RequestContainer<UserBean> requestData = new RequestContainer<UserBean>();
        requestData.setDataBean(getUserBean());

        UserBusiness business = new UserBusiness();
        ResponseContainer<UserBean> response = business.getUsers(requestData);

        if (!isBackAction() && response.containsErrorMessages()) {
            createMessages(response.getErrorMessagesList());
        } else {
            setUsers(response.getDataList());
        }

        return null;
    }

    public String resetManageUserAction() {
        userBean = new UserBean();
        if (users != null) {
            users.clear();
        }
        clearTableSelection();
        return null;
    }


    public String editUserAction() throws GTechException {
        RequestContainer<UserBean> requestData = new RequestContainer<UserBean>();
        requestData.setDataBean(getUserBean());

        UserBusiness business = new UserBusiness();
        ResponseContainer<UserBean> response = business.updateUser(requestData);

        if (response.containsErrorMessages()) {
            createMessages(response.getErrorMessagesList());
        } else {
            createMessages(response.getInfoMessagesList());
        }

        return null;
    }

    public String addUserAction() throws GTechException {
        RequestContainer<UserBean> requestData = new RequestContainer<UserBean>();
        getUserBean().setStatus("100001");
        requestData.setDataBean(getUserBean());

        UserBusiness business = new UserBusiness();
        ResponseContainer<UserBean> response = business.addUser(requestData);

        if (response.containsErrorMessages()) {
            createMessages(response.getErrorMessagesList());
        } else {
            createMessages(response.getInfoMessagesList());
            clearAddUserAction();
        }

        return null;
    }

    public String clearAddUserAction() {
        setUserBean(null);
        setUserBean(new UserBean());

        return null;
    }


    public String resetEditUserAction() throws GTechException {
        getUserFromDB();

        return null;
    }

    public void deleteUserDialogListener(DialogEvent event) throws GTechException {
        if (event.getOutcome() == DialogEvent.Outcome.no) {
            return;
        }

        RequestContainer<UserBean> requestData = new RequestContainer<UserBean>();
        requestData.setDataBean(getSelectedUserBean());

        UserBusiness business = new UserBusiness();
        ResponseContainer<UserBean> response = business.deleteUser(requestData);

        if (response.containsErrorMessages()) {
            createMessages(response.getErrorMessagesList());
        } else {
            createMessages(response.getInfoMessagesList());
            backToManageUser();
            addPartialTrigger("panelCollection");
            addPartialTrigger(TABLE_NAME);
        }
    }


    public void tableSelectionListener(SelectionEvent selection) {

        setSelectedFlag(true);
        addPartialTrigger(TABLE_NAME);
    }


    public String goToAddUserAction() throws GTechException {

        RequestContainer<GroupBean> requestContainer = new RequestContainer<GroupBean>();
        requestContainer.setDataBean(new GroupBean());
        GroupBusiness groupBusiness = new GroupBusiness();
        setGroups(groupBusiness.getAllGroups(requestContainer).getDataList());

        listOfRoles.clear();
        listOfRoles.addAll(getListOfRoleItems());


        setUserBean(new UserBean());

        return "goToAddUser";
    }

    private UserBean getSelectedUserBean() {
        return (UserBean) getSearchReslutsTable().getSelectedRowData();
    }

    private void getUserFromDB() throws GTechException {
        RequestContainer<UserBean> requestData = new RequestContainer<UserBean>();
        requestData.setDataBean(getUserBean());

        UserBusiness business = new UserBusiness();
        ResponseContainer<UserBean> dataResponse = business.getUser(requestData);

        setUserBean(dataResponse.getDataBean());
    }

    public String goToEditUserAction() throws GTechException {

        RequestContainer<GroupBean> requestContainer = new RequestContainer<GroupBean>();
        requestContainer.setDataBean(new GroupBean());
        GroupBusiness groupBusiness = new GroupBusiness();
        setGroups(groupBusiness.getAllGroups(requestContainer).getDataList());

        setUserBean(getSelectedUserBean());
        getUserFromDB();


        listOfRoles.clear();
        listOfRoles.addAll(getListOfRoleItems());


        return "goToEditUser";
    }

    public String backToManageUser() throws GTechException {
        setBackAction(true);
        getUsersAction();
        setBackAction(false);


        setSelectedFlag(false);
        clearTableSelection();

        return "backToManageUser";
    }


    public List<SelectItem> getListOfRoleItems() throws GTechException {
        List<SelectItem> list = new ArrayList<SelectItem>();

        UserDao userRoleDAO = new UserDao();
        list = userRoleDAO.getAllowedRole();

        return list;
    }


    private void clearTableSelection() {
        if (getSearchReslutsTable() != null && getSearchReslutsTable().getSelectedRowKeys() != null) {
            getSearchReslutsTable().getSelectedRowKeys().clear();
        }
    }
    
    public void handleBranch(ClientEvent ce) {
        getUserBean().setSectionBean(new SectionBean());
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }
    
    public void handleAutoFillBranch(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();


        try {
            DepartmentBean departmentBean = getUserBean().getDepartmentBean();

            if(StringUtilities.isEmpty(departmentBean.getCode())){

                getUserBean().setDepartmentBean(new DepartmentBean());

                reloadComponent("BRANCH_CODE_FIELD");
                reloadComponent("BRANCH_ID_FIELD");
                reloadComponent("BRANCH_DESC_FIELD");

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                return;
            }

            LookupBean lookupBean = lookupBusiness.getLookupValueAutoFilled(departmentBean.getCode(),
                                                        String.valueOf(getUserBean().getEntityBean().getId()),
                                                        "POG_DEPARTMENTS");
            departmentBean.setCode(lookupBean.getCode());
            departmentBean.setNameAr(lookupBean.getNameAr());
            departmentBean.setNameEn(lookupBean.getNameEn());
            departmentBean.setId(lookupBean.getId());

            getUserBean().setSectionBean(new SectionBean());

        } catch (GTechException e) {
            e.printStackTrace();
        }
        reloadComponent("BRANCH_CODE_FIELD");
        reloadComponent("BRANCH_ID_FIELD");
        reloadComponent("BRANCH_DESC_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }
    
    public void handleAutoFillEntity(ValueChangeEvent  ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        
        getUserBean().getEntityBean().setCode(ce.getNewValue() + "");

        try {
            EntityBean entityBean = getUserBean().getEntityBean();

            if(StringUtilities.isEmpty(entityBean.getCode())){

                getUserBean().setEntityBean(new EntityBean());

                reloadComponent("BRANCH_CODE_FIELD");
                reloadComponent("BRANCH_ID_FIELD");
                reloadComponent("BRANCH_DESC_FIELD");

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                
                reloadComponent("ENTITY_DESC_LIST");
                reloadComponent("ENTITY_DESC_LABEL");
                reloadComponent("ENTITY_CODE_LIST");
                reloadComponent("ENTITY_CODE_LABEL");
                return;
            }

            LookupBean lookupBean = lookupBusiness.getLookupValueAutoFilled(entityBean.getCode(),
                                                        null,
                                                        "POG_ENTITIES");
            entityBean.setCode(lookupBean.getCode());
            entityBean.setNameAr(lookupBean.getNameAr());
            entityBean.setNameEn(lookupBean.getNameEn());
            entityBean.setId(lookupBean.getId());
            
            getUserBean().setDepartmentBean(new DepartmentBean());
            getUserBean().setSectionBean(new SectionBean());

        } catch (GTechException e) {
            e.printStackTrace();
        }
        reloadComponent("BRANCH_CODE_FIELD");
        reloadComponent("BRANCH_ID_FIELD");
        reloadComponent("BRANCH_DESC_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
        
        reloadComponent("ENTITY_DESC_LIST");
        reloadComponent("ENTITY_DESC_LABEL");
        reloadComponent("ENTITY_CODE_LIST");
        reloadComponent("ENTITY_CODE_LABEL");
    }
    
//    public void handleAutoFillEntity(ClientEvent ce) {
//        LookupBusiness lookupBusiness = new LookupBusiness();
//        try {
//            LookupBean lookupBean =
//                lookupBusiness.getLookupValueAutoFilled(userBean.getEecCode(), null, "POG_ENTITIES");
//            userBean.setEecCode(lookupBean.getCode());
//            userBean.setEecDesc(lookupBean.getNameAr());
//            userBean.setEecID("" + lookupBean.getId());
//        } catch (GTechException e) {
//
//        }
//        reloadComponent("entityDescId");
//        reloadComponent("entityCodeId");
//        reloadComponent("entityd");
//    }
    
    public void handleAutoFillSection(ClientEvent ce) {
        LookupBusiness lookupBusiness = new LookupBusiness();
        try {

            SectionBean sectionBean = getUserBean().getSectionBean();

            if(StringUtilities.isEmpty(sectionBean.getCode())){
                getUserBean().setSectionBean(new SectionBean());

                reloadComponent("SECTION_CODE_FIELD");
                reloadComponent("SECTION_ID_FIELD");
                reloadComponent("SECTION_DESC_FIELD");
                return;
            }

            LookupBean lookupBean = lookupBusiness.getLookupValueAutoFilled(sectionBean.getCode(), String.valueOf(getUserBean().getDepartmentBean().getId()), "POG_SECTION");
            sectionBean.setCode(lookupBean.getCode());
            sectionBean.setNameAr(lookupBean.getNameAr());
            sectionBean.setNameEn(lookupBean.getNameEn());
            sectionBean.setId(lookupBean.getId());
        } catch (GTechException e) {
            e.printStackTrace();
        }
        reloadComponent("SECTION_CODE_FIELD");
        reloadComponent("SECTION_ID_FIELD");
        reloadComponent("SECTION_DESC_FIELD");
    }


    public void resetPasswordListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            if (!oldPassoword.equals(getLogedInUser().getPassword())) {
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addErrorMessage("OLD_PASSWORD_VALIDATION");
                createMessages(responseContainer);
            } else if (newPassoword != null && confirmedPassoword != null && !newPassoword.equals(confirmedPassoword)) {
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addErrorMessage("CONFIRM_PASSWORD_VALIDATION");
                createMessages(responseContainer);
            } else {
                UserBusiness business = new UserBusiness();
                try {
                    boolean check = business.updatePassowrd(newPassoword, getLogedInUser().getUserID());
                } catch (GTechException e) {
                    e.printStackTrace();
                    ResponseContainer responseContainer = new ResponseContainer();
                    responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
                    createMessages(responseContainer);
                    return;
                }
                setOldPassoword(null);
                setNewPassoword(null);
                setConfirmedPassoword(null);
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
                createMessages(responseContainer);
            }

        }
    }

    private RichTable getSearchReslutsTable() {
        return (RichTable) FacesContext.getCurrentInstance()
                                       .getViewRoot()
                                       .findComponent(TABLE_NAME);
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUsers(List<UserBean> users) {
        this.users = users;
    }

    public List<UserBean> getUsers() {
        return users;
    }

    public void setFilteredUsers(List<UserBean> filteredUsers) {
        this.filteredUsers = filteredUsers;
    }

    public List<UserBean> getFilteredUsers() {
        return filteredUsers;
    }

    public void setSelectedFlag(boolean selectedFlag) {
        this.selectedFlag = selectedFlag;
    }

    public boolean isSelectedFlag() {
        return selectedFlag;
    }

    public void setListOfRoles(List<SelectItem> listOfRoles) {
        this.listOfRoles = listOfRoles;
    }

    public List<SelectItem> getListOfRoles() {
        return listOfRoles;
    }


    public void setBackAction(boolean backAction) {
        this.backAction = backAction;
    }

    public boolean isBackAction() {
        return backAction;
    }

    public void setOldPassoword(String oldPassoword) {
        this.oldPassoword = oldPassoword;
    }

    public String getOldPassoword() {
        return oldPassoword;
    }

    public void setNewPassoword(String newPassoword) {
        this.newPassoword = newPassoword;
    }

    public String getNewPassoword() {
        return newPassoword;
    }

    public void setConfirmedPassoword(String confirmedPassoword) {
        this.confirmedPassoword = confirmedPassoword;
    }

    public String getConfirmedPassoword() {
        return confirmedPassoword;
    }

    public void setGroups(List<GroupBean> groups) {
        this.groups = groups;
    }

    public List<GroupBean> getGroups() {
        return groups;
    }
}
