package com.gtech.web.auditingPaymentOrder.bean;

import com.gtech.common.base.*;

public class DateBean extends BaseBean {

    private String fromDate;
    private String toDate;


    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }
}
