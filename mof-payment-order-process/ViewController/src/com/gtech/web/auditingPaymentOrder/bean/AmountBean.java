package com.gtech.web.auditingPaymentOrder.bean;

import com.gtech.common.base.*;

public class AmountBean extends BaseBean {

    private Long fromAmount;
    private Long toAmount;


    public void setFromAmount(Long fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Long getFromAmount() {
        return fromAmount;
    }

    public void setToAmount(Long toAmount) {
        this.toAmount = toAmount;
    }

    public Long getToAmount() {
        return toAmount;
    }
}
