package com.gtech.web.auditingPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.auditingPaymentOrder.bean.AmountBean;
import com.gtech.web.auditingPaymentOrder.bean.DateBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.business.EntityBusiness;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.expense.business.ExpenseBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.render.ClientEvent;


@ManagedBean(name = "auditingSearchBean")
@ViewScoped
public class AuditingPaymentOrderSearchBacking extends GTechModelBacking {

    private ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private List<ExpenseBean> expenseBeanList = new ArrayList<>();
    private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<>();
    private List<AmountBean> amountBeanList = new ArrayList<>();
    private List<DateBean> dateBeanList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String date;
    private List<EntityBean> entityBeanList = new ArrayList<>();
    private List<EntityBean> entityBeanTable = new ArrayList<>();
    private Long entityId;
    private Double totalAmount;
    private Integer totalPaymentOrderConut;
    private boolean disabledAddAmount;
    private boolean disabledAddDate;
    private boolean disabledEntity;
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();


    @PostConstruct
    public void init() {
        EntityBusiness entityBusiness = new EntityBusiness();
        ExpenseBusiness expenseBusiness = new ExpenseBusiness();
        try {
            entityBeanList = entityBusiness.getEntityList();
            expenseBeanList = expenseBusiness.getExpenseList();
            amountBeanList.add(new AmountBean());
            dateBeanList.add(new DateBean());
            entityBeanTable.add(new EntityBean());
        } catch (GTechException e) {
        }
    }


    public void addAmount(ActionEvent event) {
        if (amountBeanList.size() >= 5) {
            disabledAddAmount = true;
            return;
        }
        amountBeanList.add(new AmountBean());
        reloadComponent("amountTableId");
    }

    public void deleteAmount(ActionEvent event) {
        disabledAddAmount = false;
        AmountBean amountBean = (AmountBean) event.getComponent().getAttributes().get("amountBean");
        amountBeanList.remove(amountBean);
        reloadComponent("amountTableId");
    }

    public void handleServerEvent(ClientEvent ce) {

    }


    public void addEntity(ActionEvent event) {
        if (entityBeanTable.size() >= 5) {
            disabledEntity = true;
            return;
        }
        entityBeanTable.add(new EntityBean());
        reloadComponent("entityTableId");
    }

    public void deleteEntity(ActionEvent event) {
        disabledEntity = false;
        EntityBean entityBean = (EntityBean) event.getComponent().getAttributes().get("entityBean");
        entityBeanTable.remove(entityBean);
        reloadComponent("entityTableId");
    }

    public void addDate(ActionEvent event) {
        if (dateBeanList.size() >= 5) {
            disabledAddDate = true;
            return;
        }
        dateBeanList.add(new DateBean());
        reloadComponent("dateTableId");
    }

    public void deleteDate(ActionEvent event) {
        disabledAddDate = false;
        DateBean dateBean = (DateBean) event.getComponent().getAttributes().get("dateBean");
        dateBeanList.remove(dateBean);
        reloadComponent("dateTableId");
    }

    public void fillPaymentOrder(ActionEvent event) {
        try {
            fillPaymentOrder();
        } catch (GTechException e) {
        }
    }

    public void fillPaymentOrder() throws GTechException {
        reviewPOBeanList.clear();
        reviewPOBeanList =
            paymentOrderBusiness.getPaymentOrderBeanForAuditing(expenseBeanList, amountBeanList, dateBeanList,
                                                                entityBeanTable);
        if (reviewPOBeanList.isEmpty()) {
            totalPaymentOrderConut = 0;
        } else {
            totalPaymentOrderConut = reviewPOBeanList.size();
        }
        totalAmount = 0.0;
        for (PaymentOrderBean paymentOrderBean : reviewPOBeanList) {
            totalAmount = totalAmount + paymentOrderBean.getAmount();
        }
        reloadComponent("reviewTableId");
        reloadComponent("totalAmountId");
        reloadComponent("totalPaymentOrderConutId");

    }

    public String doSaveAll(ActionEvent event) throws GTechException {

        for (PaymentOrderBean paymentOrderBean : reviewPOBeanList) {
            paymentOrderBean.setCheck(true);
        }

        if (actionLogBusiness.insertFile(reviewPOBeanList, 100504, getLogedInUser().getUserID())) {
            responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        } else {
            responseContainer.addErrorMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        }
        createMessages(responseContainer);
        reloadComponent("reviewTableId");
        reloadComponent("totalAmountId");
        reloadComponent("totalPaymentOrderConutId");
        return null;
    }


    public void doSave(ActionEvent event) throws GTechException {
        if (actionLogBusiness.insertFile(reviewPOBeanList, 100504, getLogedInUser().getUserID())) {
            fillPaymentOrder();
            reloadComponent("reviewTableId");
            responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        } else {
            responseContainer.addErrorMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        }
        createMessages(responseContainer);

    }

    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }

    public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
        this.paymentOrderBusiness = paymentOrderBusiness;
    }

    public PaymentOrderBusiness getPaymentOrderBusiness() {
        return paymentOrderBusiness;
    }

    public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
        this.reviewPOBeanList = reviewPOBeanList;
    }

    public List<PaymentOrderBean> getReviewPOBeanList() {
        return reviewPOBeanList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setEntityBeanList(List<EntityBean> entityBeanList) {
        this.entityBeanList = entityBeanList;
    }

    public List<EntityBean> getEntityBeanList() {
        return entityBeanList;
    }


    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEntityId() {
        return entityId;
    }


    public void setExpenseBeanList(List<ExpenseBean> expenseBeanList) {
        this.expenseBeanList = expenseBeanList;
    }

    public List<ExpenseBean> getExpenseBeanList() {
        return expenseBeanList;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalPaymentOrderConut(Integer totalPaymentOrderConut) {
        this.totalPaymentOrderConut = totalPaymentOrderConut;
    }

    public Integer getTotalPaymentOrderConut() {
        return totalPaymentOrderConut;
    }

    public void setAmountBeanList(List<AmountBean> amountBeanList) {
        this.amountBeanList = amountBeanList;
    }

    public List<AmountBean> getAmountBeanList() {
        return amountBeanList;
    }


    public void setDisabledAddAmount(boolean disabledAddAmount) {
        this.disabledAddAmount = disabledAddAmount;
    }

    public boolean isDisabledAddAmount() {
        return disabledAddAmount;
    }


    public void setDateBeanList(List<DateBean> dateBeanList) {
        this.dateBeanList = dateBeanList;
    }

    public List<DateBean> getDateBeanList() {
        return dateBeanList;
    }


    public void setEntityBeanTable(List<EntityBean> entityBeanTable) {
        this.entityBeanTable = entityBeanTable;
    }

    public List<EntityBean> getEntityBeanTable() {
        return entityBeanTable;
    }

    public void setDisabledAddDate(boolean disabledAddDate) {
        this.disabledAddDate = disabledAddDate;
    }

    public boolean isDisabledAddDate() {
        return disabledAddDate;
    }

    public void setDisabledEntity(boolean disabledEntity) {
        this.disabledEntity = disabledEntity;
    }

    public boolean isDisabledEntity() {
        return disabledEntity;
    }
}
