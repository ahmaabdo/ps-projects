package com.gtech.web.auditingPaymentOrder.backing;

import com.gtech.common.beans.container.RequestContainer;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.report.WriteXls;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderSearchBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletResponse;


@ManagedBean(name = "auditPaymentOrderForMOFBackedBean")
    @SessionScoped
public class AuditPaymentOrderForMOFBacking extends GTechModelBacking{

        private ResponseContainer responseContainer = new ResponseContainer();
        private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
        private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
        private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        private Integer numberOfPaymentOrder;
        private String date;
        private PaymentOrderSearchBean paymentOrderSearchBean = new PaymentOrderSearchBean();

        private PaymentOrderBean paymentOrderBean;
        private String remark;
        private String statusName;
        private String payMethodName;
        private String paramSelected;
        private String poId;



        public void fillAuditingPaymentOrder(ActionEvent event) {
            try {
                fillAuditingPaymentOrder();
                if(paymentOrderSearchBean != null &&
                   paymentOrderSearchBean.getFromPaymentOrderCode() != null &&
                   reviewPOBeanList != null &&
                   !reviewPOBeanList.isEmpty()) {
                    
                    poId = reviewPOBeanList.get(0).getId() + "";
                } else {
                    poId = null;
                }

                    
            } catch (GTechException e) {
                e.printStackTrace();
            }
        }

        public void fillAuditingPaymentOrder() throws GTechException {
            if(reviewPOBeanList != null) {
                reviewPOBeanList.clear();    
            }
            

            ResponseContainer<PaymentOrderBean> responseContainer = new ResponseContainer<PaymentOrderBean>();
            RequestContainer<PaymentOrderSearchBean> requestContainer = new RequestContainer<PaymentOrderSearchBean>();
            paymentOrderSearchBean.setPaymentOrderStatus("100404");

            requestContainer.setDataBean(paymentOrderSearchBean);

            responseContainer = paymentOrderBusiness.getPaymentOrderBeanForAuditedMofScreen(requestContainer,getLogedInUser().getUserID(),
                                                                                            getPoDateFrom(), getPoDateTo(), getPoNumber(), 
                                                                                            getBenNumber(), getIbanAccNumber());

            reviewPOBeanList = responseContainer.getDataList();
            if(reviewPOBeanList == null || reviewPOBeanList.isEmpty()){
                numberOfPaymentOrder = 0 ;
            }else{
                numberOfPaymentOrder = reviewPOBeanList.size();
            }
            reloadComponent("reviewTableId");
            reloadComponent("numberOfPaymentOrderId");
        }



        @Override
        public String initialize() throws GTechException {
            reviewPOBeanList = new ArrayList<PaymentOrderBean>();
            paymentOrderSearchBean = new PaymentOrderSearchBean();
            numberOfPaymentOrder = 0;
            date ="";

            return "PO-F15";
        }


        public String backFromAuditSinglePaymentOrder() {
            try {
                fillAuditingPaymentOrder();
            } catch (GTechException e) {
            }

            return "PO-F15";
        }




        private void rest(){
            remark = null;
            statusName = null ;
            payMethodName = null ;
            paymentOrderBean = new PaymentOrderBean();
            resetSearchParam();

        }
    public String doRest() throws GTechException {
        reviewPOBeanList.clear();
        resetSearchParam();
        numberOfPaymentOrder = null;
        return null;
    }

        public void returnPaymentOrder(ActionEvent event) throws GTechException {
            new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100510);
        }
       
        public void downloadExcel() throws FileNotFoundException, IOException {
            if(reviewPOBeanList != null && !reviewPOBeanList.isEmpty()) {
                WriteXls writeXls = new WriteXls();
                FileInputStream fileInputStream = null;

                File temp = writeXls.writeXlsAuditReport(reviewPOBeanList);
                if (temp.exists()) {
                    fileInputStream = new FileInputStream(temp);

                }
                FacesContext facesContext = FacesContext.getCurrentInstance();
                ExternalContext externalContext = facesContext.getExternalContext();
                HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
                response.setContentLength((int) temp.length());
                response.reset();

                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment; filename=auditPaymentOrder.xls;");

                org.apache.commons.io.IOUtils.copy(fileInputStream, response.getOutputStream());
                fileInputStream.close();
                facesContext.responseComplete();
            }
            
        }
        
        public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
            this.paymentOrderBusiness = paymentOrderBusiness;
        }

        public PaymentOrderBusiness getPaymentOrderBusiness() {
            return paymentOrderBusiness;
        }

        public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
            this.reviewPOBeanList = reviewPOBeanList;
        }

        public List<PaymentOrderBean> getReviewPOBeanList() {
            return reviewPOBeanList;
        }

        public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
            this.numberOfPaymentOrder = numberOfPaymentOrder;
        }

        public Integer getNumberOfPaymentOrder() {
            return numberOfPaymentOrder;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDate() {
            return date;
        }


    public void setPaymentOrderSearchBean(PaymentOrderSearchBean paymentOrderSearchBean) {
        this.paymentOrderSearchBean = paymentOrderSearchBean;
    }

    public PaymentOrderSearchBean getPaymentOrderSearchBean() {
        return paymentOrderSearchBean;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getPoId() {
        return poId;
    }
}


