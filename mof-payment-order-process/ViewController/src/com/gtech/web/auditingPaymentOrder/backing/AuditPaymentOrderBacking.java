package com.gtech.web.auditingPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.faces.util.MessageUtilities;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;


@ManagedBean(name = "auditPaymentOrderBackedBean")
@SessionScoped
public class AuditPaymentOrderBacking extends GTechModelBacking {

    private ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String date;

    private PaymentOrderBean paymentOrderBean;
    private String remark;
    private String statusName;
    private String payMethodName;
    private String paramSelected;


    private String checkFunds;

    public void fillAuditingPaymentOrder(ActionEvent event) {
        try {
            if(isValidSearch()) {
                fillAuditingPaymentOrder();
            }
        } catch (GTechException e) {
        }
    }

    public void fillAuditingPaymentOrder() throws GTechException {
        reviewPOBeanList.clear();
        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatus(100403, getPoDateFrom() ,getPoDateTo(),
                                                                            getPoNumber(),
                                                                            getBenNumber(),
                                                                            getIbanAccNumber(), 
                                                                            getLogedInUser());
        if (reviewPOBeanList.isEmpty()) {
            numberOfPaymentOrder = 0;
        } else {
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }

    public void fillAuditedPaymentOrder(ActionEvent event) {
        try {
            fillAuditedPaymentOrder();
        } catch (GTechException e) {
        }
    }

    public void fillAuditedPaymentOrder() throws GTechException {
        reviewPOBeanList.clear();
        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatus(100404, getPoDateFrom() ,getPoDateTo(),
                                                                            getPoNumber(),
                                                                            getBenNumber(),
                                                                            getIbanAccNumber(), 
                                                                            getLogedInUser());
        if (reviewPOBeanList.isEmpty()) {
            numberOfPaymentOrder = 0;
        } else {
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }

    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }

    public String doSave() throws GTechException {
        ResponseContainer responseContainer1 = new ResponseContainer();
        setCheckFunds(null);
        boolean check = false;
        for (PaymentOrderBean paymentOrderBean : reviewPOBeanList) {

            if (paymentOrderBean.isCheck()) {
                check = true;
                break;
            }

        }
        if (check) {
            if (actionLogBusiness.insertFile(reviewPOBeanList, 100504, getLogedInUser().getUserID())) {
                for (PaymentOrderBean paymentOrderBean : reviewPOBeanList) {
                    String checkFun = paymentOrderBusiness.isHaveCheckFunds(paymentOrderBean.getPaymentOrderNumber());
                    if(checkFun != null) {
                        MessageUtilities.createInfoMessage(paymentOrderBean.getPaymentOrderNumber() + " : " + checkFun);
                    }
                    
                }
            
                fillAuditingPaymentOrder();
                responseContainer1.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
            } else {
                responseContainer1.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
            }

        } else {
            responseContainer1.addInfoMessage("PLEASE_SELECT_ONE_RECORED");
        }
        createMessages(responseContainer1);
        return null;
    }

    public String backFromAuditSinglePaymentOrder() {
        try {
            fillAuditingPaymentOrder();
        } catch (GTechException e) {
        }

        return "PO-F11";
    }

    public String init() throws GTechException {
        rest();
        DomainBusiness domainBusiness = new DomainBusiness();
        paymentOrderBean = paymentOrderBusiness.getFullDataPaymentOrder(new Long(paramSelected));
        payMethodName = domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentMethod()).getNameAr();
        statusName =
            domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentOrderStatus().longValue()).getNameAr();
        return "PO-F13";
    }

    public String initAuditingScreen() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        numberOfPaymentOrder = 0;
        date = "";
        // TODO Implement this method
        return "PO-F11";
    }

    public String initAuditedScreen() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        numberOfPaymentOrder = 0;
        date = "";
        // TODO Implement this method
        return "PO-F12";
    }

    private void rest() {
        remark = null;
        statusName = null;
        payMethodName = null;
        paymentOrderBean = new PaymentOrderBean();
    }

    public void confirmPaymentOrder(ActionEvent event) throws GTechException {
        new ActionLogBusiness()
            .insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100603);
    }

    public void returnPaymentOrder(ActionEvent event) throws GTechException {
        new ActionLogBusiness()
            .insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100510);
    }

    public String returnToPaymentOrderConfirmPage() throws GTechException {

        return "PO-F6";
    }

    public String doRest() throws GTechException {
        reviewPOBeanList.clear();
        resetSearchParam();
        return null;
    }

    public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
        this.paymentOrderBusiness = paymentOrderBusiness;
    }

    public PaymentOrderBusiness getPaymentOrderBusiness() {
        return paymentOrderBusiness;
    }

    public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
        this.reviewPOBeanList = reviewPOBeanList;
    }

    public List<PaymentOrderBean> getReviewPOBeanList() {
        return reviewPOBeanList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setCheckFunds(String checkFunds) {
        this.checkFunds = checkFunds;
    }

    public String getCheckFunds() {
        return checkFunds;
    }
}


