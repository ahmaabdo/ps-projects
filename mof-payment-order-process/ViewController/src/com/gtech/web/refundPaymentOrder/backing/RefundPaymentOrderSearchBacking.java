package com.gtech.web.refundPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.business.EntityBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.event.DialogEvent;


@ManagedBean(name = "refundPaymentOrderSearchBean")
@SessionScoped
public class RefundPaymentOrderSearchBacking extends GTechModelBacking {

    private ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private List<PaymentOrderBean> reviewPOBeanList = new ArrayList<>();
    private Integer numberOfPaymentOrder;
    private String date;
    private EntityBean entityBean = new EntityBean();
    private String poId;
    private String poCode;
    public void init() {
        EntityBusiness entityBusiness = new EntityBusiness();
        try {
            entityBean = entityBusiness.getUserEntity(getLogedInUser().getUserID());
        } catch (GTechException e) {
        }
    }

    public void fillReviewPaymentOrder(ActionEvent event) {
        try {
            if(isValidSearch()) {
                fillReviewPaymentOrder();    
            }
            
        } catch (GTechException e) {
        }
    }

    public String backFromPaymentOrderScreen() {
        try {
            fillReviewPaymentOrder();
        } catch (GTechException e) {
        }

        return "PO-F10";
    }

    public void fillReviewPaymentOrder() throws GTechException {
        reviewPOBeanList.clear();
        reviewPOBeanList = paymentOrderBusiness.getPaymentOrderBeanByStatus(100410, getPoDateFrom() ,getPoDateTo(),
                                                                            getPoNumber(),
                                                                            getBenNumber(),
                                                                            getIbanAccNumber(), 
                                                                            getLogedInUser());
        if(reviewPOBeanList.isEmpty()){
            numberOfPaymentOrder = 0 ;
        }else{
            numberOfPaymentOrder = reviewPOBeanList.size();
        }
        reloadComponent("reviewTableId");
        reloadComponent("numberOfPaymentOrderId");
    }

    @Override
    public String initialize() throws GTechException {
        reviewPOBeanList = new ArrayList<PaymentOrderBean>();
        numberOfPaymentOrder = 0;
        date = "";
        // TODO Implement this method
        return "PO-F10";

    }

    public String doSave() throws GTechException {
        if (actionLogBusiness.insertFile(reviewPOBeanList, 100502, getLogedInUser().getUserID())) {
            fillReviewPaymentOrder();
            responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
        } else {
            responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
        }
        createMessages(responseContainer);

        return null;
    }

    public String doRest() throws GTechException {
        reviewPOBeanList.clear();
        resetSearchParam();
        numberOfPaymentOrder = null;
        return null;
    }
    
    public void ShowPopup(ActionEvent actionEvent) {
             RichPopup.PopupHints hints = new RichPopup.PopupHints();
             this.getPopup("p1RemovePo").show(hints);
    
    }
    
    public void removePo(DialogEvent dialogEvent) throws GTechException {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            paymentOrderBusiness.deletePO(poId,poCode);
            reviewPOBeanList.clear();
            
            fillReviewPaymentOrder();
            
            setPoId(null);
            setPoCode(null);
            reloadComponent("hg");
        }
    }


    public void setPaymentOrderBusiness(PaymentOrderBusiness paymentOrderBusiness) {
        this.paymentOrderBusiness = paymentOrderBusiness;
    }

    public PaymentOrderBusiness getPaymentOrderBusiness() {
        return paymentOrderBusiness;
    }

    public void setReviewPOBeanList(List<PaymentOrderBean> reviewPOBeanList) {
        this.reviewPOBeanList = reviewPOBeanList;
    }

    public List<PaymentOrderBean> getReviewPOBeanList() {
        return reviewPOBeanList;
    }

    public void setNumberOfPaymentOrder(Integer numberOfPaymentOrder) {
        this.numberOfPaymentOrder = numberOfPaymentOrder;
    }

    public Integer getNumberOfPaymentOrder() {
        return numberOfPaymentOrder;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setEntityBean(EntityBean entityBean) {
        this.entityBean = entityBean;
    }

    public EntityBean getEntityBean() {
        return entityBean;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getPoId() {
        return poId;
    }

    public void setPoCode(String poCode) {
        this.poCode = poCode;
    }

    public String getPoCode() {
        return poCode;
    }
}
