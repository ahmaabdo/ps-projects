package com.gtech.web.singlepoconfirmation.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.domain.business.DomainBusiness;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "singlePoConfirmationBacking",eager= true)
@SessionScoped
public class SinglePoConfirmationBacking extends GTechModelBacking {
    ResponseContainer responseContainer = new ResponseContainer();
    private PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
    private PaymentOrderBean paymentOrderBean;
    private String remark;
    private String statusName;
    private String payMethodName;
    private String paramSelected;

    public String init() throws GTechException{
        rest();
        DomainBusiness domainBusiness = new DomainBusiness();
        paymentOrderBean = paymentOrderBusiness.getFullDataPaymentOrder(new Long(paramSelected));
        payMethodName = domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentMethod()).getNameAr();
        statusName =
            domainBusiness.getDomainValuesByValueId(paymentOrderBean.getPaymentOrderStatus().longValue()).getNameAr();
        return "PO-F7";
    }

    private void rest(){
        remark = null;
        statusName = null ;
        payMethodName = null ;
        paymentOrderBean = new PaymentOrderBean();
    }
    public SinglePoConfirmationBacking() throws GTechException {
    }

    @Override
    public String initialize() throws GTechException {
        return null;
    }

    public void confirmPaymentOrder(ActionEvent event) throws GTechException {
        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100503);
    }

    public void returnPaymentOrder(ActionEvent event) throws GTechException {
        new ActionLogBusiness().insereLogLine(paymentOrderBean.getId(), remark, Long.parseLong(getLogedInUser().getUserID()), 100510);
    }

    public String returnToPaymentOrderConfirmPage() throws GTechException {

        return "PO-F6";
    }

    public void setPaymentOrderBean(PaymentOrderBean paymentOrderBean) {
        this.paymentOrderBean = paymentOrderBean;
    }

    public PaymentOrderBean getPaymentOrderBean() {
        return paymentOrderBean;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setPayMethodName(String payMethodName) {
        this.payMethodName = payMethodName;
    }

    public String getPayMethodName() {
        return payMethodName;
    }

    public void setParamSelected(String paramSelected) {
        this.paramSelected = paramSelected;
    }

    public String getParamSelected() {
        return paramSelected;
    }
}
