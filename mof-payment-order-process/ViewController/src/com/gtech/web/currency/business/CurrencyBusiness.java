package com.gtech.web.currency.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.currency.dao.CurrencyDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

public class CurrencyBusiness extends GTechModelBusiness {

    public CurrencyBean getCurrency(String currencyCode) throws GTechException {
        CurrencyDAO currencyDAO = new CurrencyDAO();
        return currencyDAO.getCurrency(currencyCode);
    }

    public CurrencyBean getCurrency(String currencyCode, Connection connection) throws GTechException, SQLException {
        CurrencyDAO currencyDAO = new CurrencyDAO();
        return currencyDAO.getCurrency(currencyCode, connection);
    }

    public List<CurrencyBean> getCurrenciesList() throws GTechException {
        CurrencyDAO currencyDAO = new CurrencyDAO();
        return currencyDAO.getCurrenciesList();
    }
    
    public String getTafkeetCurrency(String amount,String currencyCode) throws GTechException {
        CurrencyDAO currencyDAO = new CurrencyDAO();
        return currencyDAO.getTafkeetCurrency(amount,currencyCode);
    }
    
    public int getPrecision(String currencyCode) throws GTechException {
        CurrencyDAO currencyDAO = new CurrencyDAO();
        return currencyDAO.getPrecision(currencyCode);
    }

}
