package com.gtech.web.currency.bean;

import com.gtech.common.base.BaseBean;

public class CurrencyBean extends BaseBean {
    
    private String curId;
    private String code;
    private String nameAr;
    private String nameEn;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setCurId(String curId) {
        this.curId = curId;
    }

    public String getCurId() {
        return curId;
    }
}
