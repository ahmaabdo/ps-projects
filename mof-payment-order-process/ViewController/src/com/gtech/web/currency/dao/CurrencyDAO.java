package com.gtech.web.currency.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.currency.bean.CurrencyBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CurrencyDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(CurrencyDAO.class);

    public CurrencyBean getCurrency(String currencyCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        CurrencyBean currencyBean = null;
        ResultSet rs = null;
        String query = "SELECT CRN_ID FROM POG_CURRENCIES  WHERE CRN_CODE = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, currencyCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                currencyBean = new CurrencyBean();
                currencyBean.setCode(currencyCode);
                currencyBean.setCurId(rs.getString("CRN_ID"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return currencyBean;

    }

    public CurrencyBean getCurrency(String currencyCode, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        CurrencyBean currencyBean = new CurrencyBean();
        ResultSet rs = null;
        String query = "SELECT CRN_ID FROM POG_CURRENCIES  WHERE CRN_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, currencyCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            currencyBean = new CurrencyBean();
            currencyBean.setCode(currencyCode);
            currencyBean.setCurId(rs.getString("CRN_ID"));
        }
        return currencyBean;
    }

    public List<CurrencyBean> getCurrenciesList() throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<CurrencyBean> currenciesList = new ArrayList<CurrencyBean>();
        String query = "SELECT CRN_ID, CRN_CODE, CRN_DESC_AR, CRN_DESC_EN FROM POG_CURRENCIES";
        ResultSet rs = null;
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                CurrencyBean currencyBean = new CurrencyBean();
                currencyBean.setCurId(rs.getString("CRN_ID"));
                currencyBean.setCode(rs.getString("CRN_CODE"));
                currencyBean.setNameAr(rs.getString("CRN_DESC_AR"));
                currencyBean.setNameEn(rs.getString("CRN_DESC_EN"));
                currenciesList.add(currencyBean);
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return currenciesList;
    }
    
    public String getTafkeetCurrency(String amount,String currencyCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        CurrencyBean currencyBean = null;
        ResultSet rs = null;
        String query = "SELECT CONVERT_NUMBER.TO_TEXT(?,?) DESCAR FROM DUAL ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, amount);
            preparedStatement.setString(2, currencyCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getString("DESCAR");      
            }
            
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return "";

    }
    
    public int getPrecision(String currencyCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        CurrencyBean currencyBean = null;
        ResultSet rs = null;
        String query = "        SELECT PRECISION FROM apps.FND_CURRENCIES_VL WHERE CURRENCY_CODE = ?";
        try {
            connection = getConnection();
            System.out.println(currencyCode);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, currencyCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getInt("PRECISION");      
            }
            
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return 2;
    }
        
}
