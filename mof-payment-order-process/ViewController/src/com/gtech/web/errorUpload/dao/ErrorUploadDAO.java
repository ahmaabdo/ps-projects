package com.gtech.web.errorUpload.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.adminhijridate.dao.CalendarDAO;
import com.gtech.web.entity.bean.EntityBean;

import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.errorUpload.bean.FileBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.section.bean.SectionBean;

import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ErrorUploadDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(ErrorUploadDAO.class);

    
    public List<FileBean> getFileByUserId(Long userId) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        FileBean fileBean = null;
        ResultSet rs = null;
        List<FileBean> fileBeanList = new ArrayList<>();
        String query = "SELECT PDA_ID , PDA_FILE_NAME  FROM POG_LOADED_FILES  WHERE PDA_LOADING_STATUS = 100702  AND PDA_LOAD_BY_SSC_ID = ?"; 
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, userId);
            rs = preparedStatement.executeQuery();
            while(rs.next()) {
                fileBean = new FileBean();
                fileBean.setFileName(rs.getString("PDA_FILE_NAME"));
                fileBean.setId(rs.getInt("PDA_ID"));
                fileBeanList.add(fileBean);
            }
          
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return fileBeanList;

    }
    
    public List<ErrorUploadBean> getErrorUploadList(Long FileId) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ErrorUploadBean errorUploadBean = null;
        ResultSet rs = null;
        List<ErrorUploadBean> errorUploadBeanList = new ArrayList<>();
        String query = "SELECT IED_ID , IED_CODE , IED_REMARKS  FROM POG_PAYMENT_ORDERS_STAGING   WHERE IED_PDA_ID = ?"; 
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, FileId);
            rs = preparedStatement.executeQuery();
            while(rs.next()) {
                errorUploadBean = new ErrorUploadBean();
                errorUploadBean.setCode(rs.getString("IED_CODE"));
                errorUploadBean.setError(rs.getString("IED_REMARKS"));
                errorUploadBean.setId(rs.getInt("IED_ID"));
                errorUploadBeanList.add(errorUploadBean);
            }
          
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return errorUploadBeanList;

    }
    
    public List<ErrorUploadBean> getUploadList(Integer FileId,  Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        ErrorUploadBean errorUploadBean = null;
        ResultSet rs = null;
        List<ErrorUploadBean> errorUploadBeanList = new ArrayList<>();
        String query = "SELECT IED_ID , IED_CODE , IED_REMARKS  FROM POG_PAYMENT_ORDERS_STAGING   WHERE IED_PDA_ID = ? order by IED_REMARKS DESC"; 
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, FileId);
            rs = preparedStatement.executeQuery();
            while(rs.next()) {
                errorUploadBean = new ErrorUploadBean();
                errorUploadBean.setCode(rs.getString("IED_CODE"));
                errorUploadBean.setError(rs.getString("IED_REMARKS"));
                errorUploadBean.setId(rs.getInt("IED_ID"));
                if(errorUploadBean.getError() != null && ! errorUploadBean.getError().isEmpty()){
                    String[] parts = errorUploadBean.getError().split("--");
                    if(parts != null && parts.length>0){
                        String part = parts[0];
                        if(part == null && part.isEmpty()){
                            errorUploadBean.setRemarkRows(parts.length-1);
                        }else{
                            errorUploadBean.setRemarkRows(parts.length);
                        }
                    }
                }else{
                    errorUploadBean.setRemarkRows(1);
                }
                
                errorUploadBeanList.add(errorUploadBean);
            }
          
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(preparedStatement, rs);
        }
        return errorUploadBeanList;

    }
    
    public boolean deleteFile(Long fileId) throws GTechException {

        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            boolean isInserted =   deleteTempFileInfo(fileId,connection) && deleteFileInfo(fileId,connection);
            if (isInserted) {
                connection.setAutoCommit(true);
                connection.commit();
            } else {
                connection.rollback();
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return true;

    }
    
    private boolean deleteFileInfo(Long fileId, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        String query =
            "DELETE FROM POG_LOADED_FILES WHERE PDA_ID = ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, fileId);
        
        // execute DELETE SQL stetement
        return preparedStatement.executeUpdate() > 0;
    }

    private boolean deleteTempFileInfo(Long fileId, Connection connection) throws GTechException, SQLException {

        PreparedStatement preparedStatement = null;
        String query = "DELETE FROM  POG_PAYMENT_ORDERS_STAGING WHERE IED_PDA_ID = ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, fileId);
        // execute delete SQL stetement
        return preparedStatement.executeUpdate() > 0;
    }

}
