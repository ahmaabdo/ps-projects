package com.gtech.web.errorUpload.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.errorUpload.bean.FileBean;
import com.gtech.web.errorUpload.business.ErrorUploadBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import oracle.adf.view.rich.render.ClientEvent;


@ManagedBean(name = "errorUploadBean")
@ViewScoped
public class ErrorUploadBacking extends GTechModelBacking {

    private ErrorUploadBusiness errorUploadBusiness = new ErrorUploadBusiness();
    private List<FileBean> fileBeanList = new ArrayList<FileBean>();
    private List<ErrorUploadBean> errorUploadBeanList = new ArrayList<ErrorUploadBean>();
    private Long fileId;
    private ResponseContainer responseContainer = new ResponseContainer();


    @PostConstruct
    public void init() {
        fillFileList();
    }

    public void handleServerEvent(ClientEvent ce) {
        try {
            fillErrorList();
            reloadComponent(":buttonId");
            reloadComponent("buttonId");
        } catch (GTechException e) {
        }
    }

    private void fillErrorList() throws GTechException {
        errorUploadBeanList = errorUploadBusiness.getErrorUploadList(fileId);
        reloadComponent(":errorUploadTableId");
    }

    private void fillFileList() {
        try {
            fileBeanList = errorUploadBusiness.getFileByUserId(new Long("1"));
        } catch (GTechException e) {

        }
    }

    public String doSendToUploadPage() throws GTechException {
        if(fileId != null){
            if (errorUploadBusiness.deleteFile(fileId)) {
                return "/paymentOrder/upload_payment_order.jsf?faces-redirect=true";
            }
        }
        return null;
    }

    public String doRemove() throws GTechException {
        responseContainer = new ResponseContainer();
        if(fileId == null || fileId ==0){
            if (errorUploadBusiness.deleteFile(fileId)) {
                errorUploadBeanList.clear();
                fileBeanList.clear();
                fileId = null;
                fillFileList();
                responseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
            } else {
                responseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
            }
            createMessages(responseContainer);
        }

        return null;
    }

    @Override
    public String initialize() throws GTechException {
        errorUploadBeanList = new ArrayList<ErrorUploadBean>();
        // TODO Implement this method
        return "PO-F3";
    }

    public void setFileBeanList(List<FileBean> fileBeanList) {
        this.fileBeanList = fileBeanList;
    }

    public List<FileBean> getFileBeanList() {
        return fileBeanList;
    }

    public void setErrorUploadBeanList(List<ErrorUploadBean> errorUploadBeanList) {
        this.errorUploadBeanList = errorUploadBeanList;
    }

    public List<ErrorUploadBean> getErrorUploadBeanList() {
        return errorUploadBeanList;
    }


    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileId() {
        return fileId;
    }

}
