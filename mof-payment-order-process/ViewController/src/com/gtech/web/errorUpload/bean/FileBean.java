package com.gtech.web.errorUpload.bean;

import com.gtech.common.base.*;

public class FileBean extends BaseBean {

    private String fileName;

    /** Code. */
    private String code;


    /**
     * Setter Code.
     *
     * @param Code : Code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter code.
     *
     * @return code.
     */
    public String getCode() {
        return code;
    }


    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
