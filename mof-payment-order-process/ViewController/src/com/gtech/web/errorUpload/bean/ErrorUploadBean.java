package com.gtech.web.errorUpload.bean;

import com.gtech.common.base.*;

public class ErrorUploadBean extends BaseBean {


    /** Code. */
    private String code;
    private String error;
    private Integer remarkRows;


    /**
     * Setter Code.
     *
     * @param Code : Code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter code.
     *
     * @return code.
     */
    public String getCode() {
        return code;
    }


    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }


    public void setRemarkRows(Integer remarkRows) {
        this.remarkRows = remarkRows;
    }

    public Integer getRemarkRows() {
        return remarkRows;
    }
}
