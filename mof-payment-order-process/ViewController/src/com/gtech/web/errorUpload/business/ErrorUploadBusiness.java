package com.gtech.web.errorUpload.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.errorUpload.bean.FileBean;
import com.gtech.web.errorUpload.dao.ErrorUploadDAO;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.section.dao.SectionDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

public class ErrorUploadBusiness extends GTechModelBusiness {
    
    private ErrorUploadDAO errorUploadDAO = new ErrorUploadDAO();

    public List<FileBean> getFileByUserId(Long userId) throws GTechException {
        return  errorUploadDAO.getFileByUserId(userId);
    }
    
    public List<ErrorUploadBean> getErrorUploadList(Long FileId) throws GTechException {
        return errorUploadDAO.getErrorUploadList(FileId);
    }
    
    public boolean deleteFile(Long fileId) throws GTechException {
        return errorUploadDAO.deleteFile(fileId);
    }
   
}
