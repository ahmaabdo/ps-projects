package com.gtech.web.lookup.backing;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.business.LookupBusiness;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean(name = "commonLookupsBean" , eager=true)
@SessionScoped
public class LookupBacking extends GTechModelBacking{

    private  LookupBean commonLookup = new LookupBean();
    private List<LookupBean> searchList = new ArrayList<>();
    private LookupBusiness lookupBusiness = new LookupBusiness();


    public void fillParam(String ln, String ki, String di, String ci, String ma, String bi){
        clear();
        commonLookup.setComponentCodeId(ci);
        commonLookup.setComponentDescId(di);
        commonLookup.setComponentKeyId(ki);
        commonLookup.setComponentButtonId(bi);
        commonLookup.setLookupName(ln);
        commonLookup.setMaster(ma);
    }
    
    public void fillParam(String ln, String ki, String di, String ci, String ma, String bi,String entityId,String sectionId,String deptId){
        clear();
        commonLookup.setComponentCodeId(ci);
        commonLookup.setComponentDescId(di);
        commonLookup.setComponentKeyId(ki);
        commonLookup.setComponentButtonId(bi);
        commonLookup.setLookupName(ln);
        commonLookup.setEntityId(entityId);
        commonLookup.setSectionId(sectionId);
        commonLookup.setDeptId(deptId);
        commonLookup.setMaster(ma);
    }
    
    public void search() throws GTechException {
        searchList.clear();
        if(commonLookup.getLookupName() != null && 
           commonLookup.getLookupName().equals("POG_PROJECTS")) {
            
            searchList = lookupBusiness.getLookupProjectValueAutoFilled(commonLookup);
            return;
        } else if(commonLookup.getLookupName() != null && 
                  commonLookup.getLookupName().equals("POG_CURRENCIES")) {
            searchList = lookupBusiness.getLookupValueCurrency(commonLookup);
            return;
        }
        searchList =  lookupBusiness.getLookupValueAutoFilled(commonLookup);
    }
    public void  doRestLookupAction(){
        clear();
    }
    public void clear() {
       searchList.clear();
       commonLookup.setCode(null);
       commonLookup.setNameAr(null);
    }

    @Override
    public String initialize() throws GTechException {
        return null;
    }

    public void setCommonLookup(LookupBean commonLookup) {
        this.commonLookup = commonLookup;
    }

    public LookupBean getCommonLookup() {
        return commonLookup;
    }

    public void setSearchList(List<LookupBean> searchList) {
        this.searchList = searchList;
    }

    public List<LookupBean> getSearchList() {
        return searchList;
    }
}
