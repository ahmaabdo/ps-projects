package com.gtech.web.lookup.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.lookup.bean.LookupBean;
import com.gtech.web.lookup.dao.LookupDao;

import java.util.List;

public class LookupBusiness extends GTechModelBusiness {
    private LookupDao lookupDao = new LookupDao();

    public LookupBean getLookupValueAutoFilled(String searchText ,String masterkey, String lookupName ) throws GTechException {
        return lookupDao.getLookupValueAutoFilled(searchText, masterkey,lookupName);
    }

    public List<LookupBean> getLookupValueAutoFilled(LookupBean lookupBean) throws GTechException {
        return lookupDao.getLookupValueAutoFilled(lookupBean);
    }
    
    public List<LookupBean> getLookupProjectValueAutoFilled(LookupBean lookupBean) throws GTechException {
        return lookupDao.getLookupProjectValueAutoFilled(lookupBean);
    }
    
    public LookupBean getLookupProjectValueAutoFilled(String projCode,String entityId,String sectionId,String deptId) throws GTechException {
        return lookupDao.getLookupProjectValueAutoFilled(projCode,entityId,sectionId,deptId);
    }
    
    public LookupBean getbyClassFicationId(String classificationId) throws GTechException {
        return lookupDao.getbyClassFicationId(classificationId);

    }
    
    public List<LookupBean> getLookupValueCurrency(LookupBean lookupBean) throws GTechException {
        return lookupDao.getLookupValueCurrency(lookupBean);
    }
    
    public LookupBean getLookupValueAutoFilledCur(String searchText ,String masterkey, String lookupName ) throws GTechException {
        return lookupDao.getLookupValueAutoFilledCur(searchText, masterkey,lookupName);
    }
    
    public LookupBean getLookupProjectUnionClass(String projCode,String entityId,String sectionId,String deptId) throws GTechException {
        return lookupDao.getLookupProjectUnionClass(projCode,entityId,sectionId,deptId);
    }
}
