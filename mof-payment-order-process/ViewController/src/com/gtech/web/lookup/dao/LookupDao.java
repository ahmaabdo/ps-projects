package com.gtech.web.lookup.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.lookup.bean.LookupBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class LookupDao extends GTechDAO {
    public LookupDao() {
        super();
    }

    public LookupBean getLookupValueAutoFilled(String code ,String masterkey, String lookupName) throws GTechException {
        LookupBean lookupBean = new LookupBean();
        StringBuilder query = new StringBuilder( "SELECT V_KEY_INDEX, V_CODE, V_DESC_AR, V_DESC_EN FROM POG_LOV_VALUES_V WHERE V_CODE = ?  AND V_SOURCE_TABLE = ?");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        try {
            if(masterkey!=null && !masterkey.isEmpty()){
                    query.append(" AND V_MASTER_KEY = "+masterkey);
                }
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, code.toUpperCase());
            statment.setString(2, lookupName);

            rs = statment.executeQuery();
            if (rs.next()) {
                lookupBean = new LookupBean();
                lookupBean.setId(rs.getLong("V_KEY_INDEX"));
                lookupBean.setCode(rs.getString("V_CODE"));
                lookupBean.setNameAr(rs.getString("V_DESC_AR"));
                lookupBean.setNameEn(rs.getString("V_DESC_EN"));
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupBean;
    }
    
    public List<LookupBean> getLookupValueAutoFilled(LookupBean lookupBean) throws GTechException {
        List<LookupBean> lookupValues = new ArrayList<LookupBean>();
        StringBuilder query = new StringBuilder(
            "SELECT V_KEY_INDEX, V_CODE, V_DESC_AR, V_DESC_EN FROM POG_LOV_VALUES_V WHERE ");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        boolean isAdd = false;
        boolean isAddDesc = false;
        try {


            if (lookupBean.getCode() != null && !lookupBean.getCode().isEmpty() && !lookupBean.getCode().equalsIgnoreCase("null")) {
                query.append("(");
                query.append(" UPPER(V_CODE) LIKE UPPER('%"+lookupBean.getCode()+"%') ");
                isAdd = true;
            }
            if (lookupBean.getNameAr() != null && !lookupBean.getNameAr().isEmpty() && !lookupBean.getNameAr().equalsIgnoreCase("null")) {
                if(isAdd){
                    query.append(" OR ");
                }
                query.append(" UPPER(V_DESC_AR) LIKE UPPER('%"+lookupBean.getNameAr()+"%') ");
                isAddDesc = true;
            }
            if (lookupBean.getNameEn() != null && !lookupBean.getNameEn().isEmpty() && !lookupBean.getNameEn().equalsIgnoreCase("null")) {
                if(isAdd){
                    query.append(" OR ");
                }
                query.append(" UPPER(V_DESC_AR) LIKE UPPER('%"+lookupBean.getNameEn()+"%') ");
                isAddDesc = true;

            }

            if(isAdd){
                query.append(" ) ");
            }
            if(isAdd || isAddDesc){
                query.append(" AND ");
            }
            query.append(" V_SOURCE_TABLE = '"+lookupBean.getLookupName()+"'");

            if(lookupBean.getMaster()!=null&&!lookupBean.getMaster().isEmpty()){
                    query.append(" AND V_MASTER_KEY = "+lookupBean.getMaster());
            }
            statment = connection.prepareStatement(query.toString());
            rs = statment.executeQuery();
            while (rs.next()) {
                LookupBean valueBean = new LookupBean();
                valueBean.setId(rs.getLong("V_KEY_INDEX"));
                valueBean.setCode(rs.getString("V_CODE"));
                valueBean.setNameAr(rs.getString("V_DESC_AR"));
                valueBean.setNameEn(rs.getString("V_DESC_EN"));
                
                lookupValues.add(valueBean);
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupValues;
    }
    
    public List<LookupBean> getLookupValueCurrency(LookupBean lookupBean) throws GTechException {
        List<LookupBean> lookupValues = new ArrayList<LookupBean>();
        StringBuilder query = new StringBuilder(
            "SELECT CRN_ID, CRN_CODE, CRN_DESC_AR, CRN_DESC_EN FROM POG_CURRENCIES WHERE ");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        boolean isAdd = false;
        boolean isAddDesc = false;
        try {


            if (lookupBean.getCode() != null && !lookupBean.getCode().isEmpty() && !lookupBean.getCode().equalsIgnoreCase("null")) {
                query.append("(");
                query.append(" UPPER(CRN_CODE) LIKE UPPER('%"+lookupBean.getCode()+"%') ");
                isAdd = true;
            }
            if (lookupBean.getNameAr() != null && !lookupBean.getNameAr().isEmpty() && !lookupBean.getNameAr().equalsIgnoreCase("null")) {
                if(isAdd){
                    query.append(" OR ");
                }
                query.append(" UPPER(CRN_DESC_AR) LIKE UPPER('%"+lookupBean.getNameAr()+"%') ");
                isAddDesc = true;
            }
            if (lookupBean.getNameEn() != null && !lookupBean.getNameEn().isEmpty() && !lookupBean.getNameEn().equalsIgnoreCase("null")) {
                if(isAdd){
                    query.append(" OR ");
                }
                query.append(" UPPER(CRN_DESC_AR) LIKE UPPER('%"+lookupBean.getNameEn()+"%') ");
                isAddDesc = true;

            }

            if(isAdd){
                query.append(" ) ");
            }
            if(isAdd || isAddDesc){
                query.append(" AND ");
            }
            
            query.append(" 1 = 1 ");
            System.out.println(query);
            statment = connection.prepareStatement(query.toString());
            rs = statment.executeQuery();
            while (rs.next()) {
                LookupBean valueBean = new LookupBean();
                valueBean.setCurCode(rs.getString("CRN_ID"));
                valueBean.setCode(rs.getString("CRN_CODE"));
                valueBean.setNameAr(rs.getString("CRN_DESC_AR"));
                valueBean.setNameEn(rs.getString("CRN_DESC_EN"));
                
                lookupValues.add(valueBean);
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupValues;
    }
    
    public List<LookupBean> getLookupProjectValueAutoFilled(LookupBean lookupBean) throws GTechException {
        
        List<LookupBean> lookupValues = new ArrayList<LookupBean>();
        StringBuilder query = new StringBuilder(
            "Select PPR_ID,PROJECT_SHORT_CODE,PROJECT_NAME_AR,PROJECT_NAME_EN,PPR_SCO_ID " + 
            "from   pog_projects  " + 
            "WHERE  ENTITY_CODE        = ? " + 
            "  AND  DEPT_CODE          = ? " + 
            "  AND  SECTION_CODE       = ? ");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        boolean isAdd = false;
        boolean isAddDesc = false;
        try {

            
            if (lookupBean.getCode() != null && !lookupBean.getCode().isEmpty() && !lookupBean.getCode().equalsIgnoreCase("null")) {
                query.append(" AND ");
                query.append(" UPPER(PROJECT_SHORT_CODE) LIKE UPPER('%"+lookupBean.getCode()+"%') ");
                isAdd = true;
            }
           
            if (lookupBean != null && lookupBean.getNameAr() != null && !lookupBean.getNameAr().isEmpty()) {
                query.append(" AND ");
                query.append(" UPPER(PROJECT_NAME_AR) = UPPER('"+lookupBean.getNameAr()+"') ");
                isAdd = true;
            }
            
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, lookupBean.getEntityId());
            statment.setString(2, lookupBean.getDeptId());
            statment.setString(3, lookupBean.getSectionId());

            
            rs = statment.executeQuery();
            while (rs.next()) {
                LookupBean valueBean = new LookupBean();
                valueBean.setCode(rs.getString("PROJECT_SHORT_CODE"));
                valueBean.setNameAr(rs.getString("PROJECT_NAME_AR"));
                lookupValues.add(valueBean);
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupValues;
    }
    
    public LookupBean getLookupProjectValueAutoFilled(String projCode,String entityId,String sectionId,String deptId) throws GTechException {
        LookupBean lookupValues = new LookupBean();
        StringBuilder query = new StringBuilder(
            "Select PPR_ID,PROJECT_SHORT_CODE,PROJECT_NAME_AR,PROJECT_NAME_EN,PPR_SCO_ID " + 
            "from   pog_projects  " + 
            "WHERE  ENTITY_CODE        = ? " + 
            "  AND  DEPT_CODE          = ? " + 
            "  AND  SECTION_CODE       = ? ");

        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        boolean isAdd = false;
        boolean isAddDesc = false;
        try {

            
            if (projCode != null && !projCode.isEmpty() && !projCode.equalsIgnoreCase("null")) {
                query.append(" AND ");
                query.append(" UPPER(PROJECT_SHORT_CODE) = UPPER('"+projCode+"') ");
                isAdd = true;
            } else {
                return lookupValues;
            }
            
            
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, entityId);
            statment.setString(2, deptId);
            statment.setString(3, sectionId);
            
            rs = statment.executeQuery();
            if (rs.next()) {
                lookupValues.setCode(rs.getString("PROJECT_SHORT_CODE"));
                lookupValues.setNameAr(rs.getString("PROJECT_NAME_AR"));
                lookupValues.setClassificationsId(rs.getString("PPR_SCO_ID"));
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupValues;
    }
    
    
    
    public LookupBean getbyClassFicationId(String classificationId) throws GTechException {
        StringBuilder query = new StringBuilder(
            "SELECT V_KEY_INDEX, V_CODE, V_DESC_AR, V_DESC_EN FROM POG_LOV_VALUES_V WHERE V_SOURCE_TABLE  = ?  AND V_KEY_INDEX = ? ");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        LookupBean valueBean = new LookupBean();
        try {

            statment = connection.prepareStatement(query.toString());
            statment.setString(1, "POG_ECOMOMIC_CLASSIFICATIONS");
            statment.setString(2, classificationId);
            rs = statment.executeQuery();
            if (rs.next()) {
               
                valueBean.setId(rs.getLong("V_KEY_INDEX"));
                valueBean.setCode(rs.getString("V_CODE"));
                valueBean.setNameAr(rs.getString("V_DESC_AR"));
                valueBean.setNameEn(rs.getString("V_DESC_EN"));
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return valueBean;
    }
    
    public LookupBean getLookupValueAutoFilledCur(String code ,String masterkey, String lookupName) throws GTechException {
        LookupBean lookupBean = new LookupBean();
        StringBuilder query = new StringBuilder( "SELECT CRN_ID, CRN_CODE, CRN_DESC_AR, CRN_DESC_EN FROM POG_CURRENCIES WHERE CRN_CODE = ? ");
        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        try {
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, code.toUpperCase());

            rs = statment.executeQuery();
            if (rs.next()) {
                lookupBean = new LookupBean();
                lookupBean.setCurCode(rs.getString("CRN_ID"));
                lookupBean.setCode(rs.getString("CRN_CODE"));
                lookupBean.setNameAr(rs.getString("CRN_DESC_AR"));
                lookupBean.setNameEn(rs.getString("CRN_DESC_EN"));
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupBean;
    }
    
    public LookupBean getLookupProjectUnionClass(String projCode,String entityId,String sectionId,String deptId) throws GTechException {
        LookupBean lookupValues = new LookupBean();
        StringBuilder query = new StringBuilder(
            "Select PPR_ID,PROJECT_SHORT_CODE,PROJECT_NAME_AR,PROJECT_NAME_EN,PPR_SCO_ID  \n" + 
            "            from   pog_projects   \n" + 
            "            WHERE  ENTITY_CODE        = ?  \n" + 
            "              AND  DEPT_CODE          = ?  \n" + 
            "              AND  SECTION_CODE       = ?\n" + 
            "              AND PROJECT_SHORT_CODE = ?\n" + 
            "UNION ALL\n" + 
            "SELECT V_KEY_INDEX, V_CODE, V_DESC_AR, V_DESC_EN,-1 FROM POG_LOV_VALUES_V \n" + 
            "        WHERE V_CODE = ?  \n" + 
            "			  AND V_SOURCE_TABLE = 'POG_ECOMOMIC_CLASSIFICATIONS'");

        Connection connection = getConnection();
        PreparedStatement statment = null;
        ResultSet rs = null;
        try {
            
            statment = connection.prepareStatement(query.toString());
            statment.setString(1, entityId);
            statment.setString(2, deptId);
            statment.setString(3, sectionId);
            statment.setString(4, projCode);
            statment.setString(5, projCode);
            
            
            rs = statment.executeQuery();
            if (rs.next()) {
                lookupValues.setCode(rs.getString("PROJECT_SHORT_CODE"));
                lookupValues.setNameAr(rs.getString("PROJECT_NAME_AR"));
                lookupValues.setClassificationsId(rs.getString("PPR_SCO_ID"));
            }
        } catch (Exception e) {
            throw new GTechException(e.getCause());
        } finally {
            closeResources(connection, statment, rs);
        }
        return lookupValues;
    }
}
