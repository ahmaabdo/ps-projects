package com.gtech.web.lookup.bean;

import com.gtech.common.base.BaseBean;

public class LookupBean extends BaseBean {
    private String code;
    private String nameAr;
    private String nameEn;
    private String componentKeyId;
    private String componentDescId;
    private String componentCodeId;
    private String componentButtonId;
    private String master;
    private String masterKeyId;
    private String lookupName;
    private String entityId;
    private String sectionId;
    private String deptId;
    private String classificationsId;
    private String curCode;
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setComponentKeyId(String componentKeyId) {
        this.componentKeyId = componentKeyId;
    }

    public String getComponentKeyId() {
        return componentKeyId;
    }

    public void setComponentDescId(String componentDescId) {
        this.componentDescId = componentDescId;
    }

    public String getComponentDescId() {
        return componentDescId;
    }

    public void setComponentCodeId(String componentCodeId) {
        this.componentCodeId = componentCodeId;
    }

    public String getComponentCodeId() {
        return componentCodeId;
    }

    public void setComponentButtonId(String componentButtonId) {
        this.componentButtonId = componentButtonId;
    }

    public String getComponentButtonId() {
        return componentButtonId;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getMaster() {
        return master;
    }

    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    public String getLookupName() {
        return lookupName;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setMasterKeyId(String masterKeyId) {
        this.masterKeyId = masterKeyId;
    }

    public String getMasterKeyId() {
        return masterKeyId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setClassificationsId(String classificationsId) {
        this.classificationsId = classificationsId;
    }

    public String getClassificationsId() {
        return classificationsId;
    }

    public void setCurCode(String curCode) {
        this.curCode = curCode;
    }

    public String getCurCode() {
        return curCode;
    }
}
