package com.gtech.web.entity.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.faces.util.SessionUtilities;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.dao.EntityDAO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

import login.LoginBean;


public class EntityBusiness extends GTechModelBusiness {

    private EntityDAO entityDAO = new EntityDAO();

    public EntityBean getEntity(String entityCode) throws GTechException {
        return entityDAO.getEntity(entityCode);

    }

    public EntityBean getEntity(String entityCode, Connection connection) throws GTechException, SQLException {
        return entityDAO.getEntity(entityCode, connection);
    }

    public EntityBean getUserEntity(String userId) throws GTechException {
          return entityDAO.getUserEntity(userId);
          //(loginBean.getId());
      }
    
    public List<EntityBean> getEntityList() throws GTechException {
        return entityDAO.getEntityList();
    }

}
