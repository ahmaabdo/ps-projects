package com.gtech.web.entity.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.web.entity.bean.EntityBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EntityDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(EntityDAO.class);

    public EntityBean getEntity(String entityCode) throws GTechException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        EntityBean entityBean = null;
        ResultSet rs = null;
        String query = "SELECT EEO_ID,EEO_CODE ,EEO_NAME_AR FROM POG_ENTITIES  WHERE EEO_CODE = ? ";
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, entityCode);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                entityBean = new EntityBean();
                entityBean.setCode(entityCode);
                entityBean.setId(rs.getInt("EEO_ID"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return entityBean;

    }

    public EntityBean getEntity(String entityCode, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        EntityBean entityBean = new EntityBean();
        ResultSet rs = null;
        String query = "SELECT EEO_ID FROM POG_ENTITIES  WHERE EEO_CODE = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, entityCode);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            entityBean = new EntityBean();
            entityBean.setCode(entityCode);
            entityBean.setId(rs.getInt("EEO_ID"));
        }


        return entityBean;

    }

    public EntityBean getUserEntity(String userId) throws GTechException {
        PreparedStatement preparedStatement = null;
        EntityBean entityBean = null;
        ResultSet rs = null;
        String query =
            "SELECT EEO_ID, EEO_CODE, EEO_NAME_AR, EEO_NAME_EN FROM POG_ENTITIES, POG_USERS WHERE EEO_ID = SSC_EEO_ID AND SSC_ID = ? ";
        Connection connection = getConnection();
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, userId);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                entityBean = new EntityBean();
                entityBean.setId(rs.getLong("EEO_ID"));
                entityBean.setCode(rs.getString("EEO_CODE"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
            }
        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return entityBean;
    }
    
    public List<EntityBean> getEntityList() throws GTechException {
        PreparedStatement preparedStatement = null;
        List<EntityBean> entityBeanList = new ArrayList<>();
        EntityBean entityBean = null;
        ResultSet rs = null;
        String query =
            "SELECT EEO_ID, EEO_CODE, EEO_NAME_AR, EEO_NAME_EN FROM POG_ENTITIES ";
        Connection connection = getConnection();
        try {
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                entityBean = new EntityBean();
                entityBean.setId(rs.getLong("EEO_ID"));
                entityBean.setCode(rs.getString("EEO_CODE"));
                entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
                entityBean.setNameEn(rs.getString("EEO_NAME_EN"));
                entityBeanList.add(entityBean);
            }
        } catch (Exception e) {
            throw new GTechException(e);
        } finally {
            closeResources(connection, preparedStatement, rs);
        }
        return entityBeanList;
    }

}
