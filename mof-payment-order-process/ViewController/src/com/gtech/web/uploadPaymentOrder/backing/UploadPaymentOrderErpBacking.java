package com.gtech.web.uploadPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.faces.util.MessageUtilities;
import com.gtech.web.uploadPaymentOrder.bean.LoadedFileBean;
import com.gtech.web.uploadPaymentOrder.business.UploadPaymentOrderBusiness;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletResponse;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.model.UploadedFile;

import org.xml.sax.SAXException;

@ManagedBean(name = "uploadPaymentOrderErpBacking")
@ViewScoped
public class UploadPaymentOrderErpBacking extends GTechModelBacking {

    private ResponseContainer ResponseContainer = new ResponseContainer();
    private UploadPaymentOrderBusiness uploadPaymentOrderBusiness = new UploadPaymentOrderBusiness();
    private Integer fileId;
    private String fileName;
    private boolean validToSave;
    private List<LoadedFileBean> loadedFileBeanList = new ArrayList<>();
    private InputStream downloadedInputStream;
    private Integer fileIdTable;
    private Long fileIdTableDownload;
    
    @PostConstruct
    public void init() {
        try {
            loadedFileBeanList = uploadPaymentOrderBusiness.getERPFileInfo(getLogedInUser().getUserId(), null);
        } catch (GTechException e) {
            e.printStackTrace();
        }
        reloadComponent("erptbl");
    }
    
    public void fileUpdate(ValueChangeEvent valueChangeEvent) throws IOException, GTechException, Exception {
        try {
            fileId = null;
            fileName = null;
            RichInputFile inputFileComponent = (RichInputFile) valueChangeEvent.getComponent();
            UploadedFile newFile = (UploadedFile) valueChangeEvent.getNewValue();
            Source xmlFile = null;
            if (newFile.getContentType().equals("text/xml")) {
                InputStream inputStream = null;
                try {
                    fileName = newFile.getFilename();
//                    File schemaFile = new File();
                    inputStream = newFile.getInputStream();
                    xmlFile = new StreamSource(inputStream);
                    SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                    
                    Schema schema;
                    schema = schemaFactory.newSchema(getClass().getResource("uploadXMLSchema.xsd"));
                    Validator validator = schema.newValidator();
                    
                    validator.validate(xmlFile);
                } catch (SAXException e) {
                    inputFileComponent.setLocalValueSet(false);
                    inputFileComponent.resetValue();
                    inputFileComponent.setValid(false);
                    inputFileComponent.setSubmittedValue(null);
                    inputFileComponent.setValue(null);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(inputFileComponent);
                    MessageUtilities.createErrorMessage(newFile.getFilename() + " is NOT valid reason:" + e);
                    System.out.println(newFile.getFilename() + " is NOT valid reason:" + e);
                    return;
                } catch (IOException e) {
                    inputFileComponent.setLocalValueSet(false);
                    inputFileComponent.resetValue();
                    inputFileComponent.setValid(false);
                    inputFileComponent.setSubmittedValue(null);
                    inputFileComponent.setValue(null);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(inputFileComponent);
                    MessageUtilities.createErrorMessage(newFile.getFilename() + " is NOT valid reason:" + e);
                    System.out.println(newFile.getFilename() + " is NOT valid reason:" + e);
                    return;
                }
                
                fileId =
                    uploadPaymentOrderBusiness.insertERPFile(newFile.getFilename(), newFile.getInputStream(),
                                                             getLogedInUser().getUserID());
                validToSave = true;

            } else {
                validToSave = false;
                ResponseContainer.addErrorMessage("ERROR_FILE_TYPE");
                createMessages(ResponseContainer);
            }

        } catch (Exception ex) {
            validToSave = false;
            ResponseContainer.addErrorMessage("ERROR_FORMAT_TYPE");
            createMessages(ResponseContainer);
            ex.printStackTrace();
        } finally {
            reloadComponent("fileNameId");
            reloadComponent("erptbl");
            reloadComponent("saveBtnId");
            reloadComponent("erpId");
            reloadComponent("buttonId");
            
        }
    }

    public void doSave() throws GTechException {
        uploadPaymentOrderBusiness.updateXMLFileStatus(Long.parseLong(fileId+""));
        uploadPaymentOrderBusiness.parseERPFileXml(fileId);
        validToSave = false;
        reloadComponent("saveBtnId");
        loadedFileBeanList = uploadPaymentOrderBusiness.getERPFileInfo(getLogedInUser().getUserId(), null);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent component = facesContext.getViewRoot().findComponent("uploadFileId");
        RichInputFile inputFileComponent = (RichInputFile) component;
        
        inputFileComponent.setLocalValueSet(false);
        inputFileComponent.resetValue();
        inputFileComponent.setValid(false);
        inputFileComponent.setSubmittedValue(null);
        inputFileComponent.setValue(null);
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(inputFileComponent);
        reloadComponent("erptbl"); 
        ResponseContainer rc = new ResponseContainer();
        rc.addInfoMessage("ADD_SUCCESS_MESSAGE");
        createMessages(rc);

    }
    public void doSaveTable(ActionEvent actionEvent) throws GTechException {
        uploadPaymentOrderBusiness.updateXMLFileStatus(Long.parseLong(fileId+""));
        uploadPaymentOrderBusiness.parseERPFileXml(fileId);
        validToSave = false;
        loadedFileBeanList = uploadPaymentOrderBusiness.getERPFileInfo(getLogedInUser().getUserId(), null);
        ResponseContainer rc = new ResponseContainer();
        rc.addInfoMessage("ADD_SUCCESS_MESSAGE");
        reloadComponent("fileNameId");
        reloadComponent("erptbl");
        reloadComponent("saveBtnId");
        reloadComponent("erpId");
        reloadComponent("buttonId");
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent component = facesContext.getViewRoot().findComponent("uploadFileId");
        RichInputFile inputFileComponent = (RichInputFile) component;
        
        inputFileComponent.setLocalValueSet(false);
        inputFileComponent.resetValue();
        inputFileComponent.setValid(false);
        inputFileComponent.setSubmittedValue(null);
        inputFileComponent.setValue(null);
        
        createMessages(rc);
    }
    
    public void download(FacesContext context,
                             OutputStream out) throws IOException, GTechException {

        byte[] b;
        try {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.setContentType("text/csv");
            response.setCharacterEncoding("UTF-8");
            downloadedInputStream = uploadPaymentOrderBusiness.getERPFileInfoById(fileIdTableDownload);    
            int n;
            while ((n = downloadedInputStream.available()) > 0) {
                b = new byte[n];
                int result = downloadedInputStream.read(b);
                out.write(b, 0, b.length);
                if (result == -1)
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
         out.flush();
    }
    
    public void clearListener(ActionEvent event) {
        validToSave = false;
        fileId = null;
        fileName = null;
        reloadComponent("fileNameId");
        reloadComponent("uploadTableId");
        reloadComponent("downloadBtnId");
        reloadComponent("saveBtnId");

    }


    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }


    @Override
    public String initialize() throws GTechException {
        validToSave = false;
        fileId = null;
        fileName = null;
        reloadComponent("erptbl");
        return "PO-F18";
    }

    public void setValidToSave(boolean validToSave) {
        this.validToSave = validToSave;
    }

    public boolean isValidToSave() {
        return validToSave;
    }

    public void setLoadedFileBeanList(List<LoadedFileBean> loadedFileBeanList) {
        this.loadedFileBeanList = loadedFileBeanList;
    }

    public List<LoadedFileBean> getLoadedFileBeanList() {
        return loadedFileBeanList;
    }


    public void setDownloadedInputStream(InputStream downloadedInputStream) {
        this.downloadedInputStream = downloadedInputStream;
    }

    public InputStream getDownloadedInputStream() {
        return downloadedInputStream;
    }

    public void setFileIdTable(Integer fileIdTable) {
        this.fileIdTable = fileIdTable;
    }

    public Integer getFileIdTable() {
        return fileIdTable;
    }

    public void setFileIdTableDownload(Long fileIdTableDownload) {
        this.fileIdTableDownload = fileIdTableDownload;
    }

    public Long getFileIdTableDownload() {
        return fileIdTableDownload;
    }

    public void reloadTable(ActionEvent actionEvent) throws GTechException {
        loadedFileBeanList = uploadPaymentOrderBusiness.getERPFileInfo(getLogedInUser().getUserId(), null);
        reloadComponent("erptbl");
    }
}
