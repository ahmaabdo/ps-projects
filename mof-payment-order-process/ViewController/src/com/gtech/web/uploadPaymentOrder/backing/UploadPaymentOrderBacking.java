package com.gtech.web.uploadPaymentOrder.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.common.report.WriteXls;
import com.gtech.common.util.ParsePaymentOrder;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;
import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.business.UploadPaymentOrderBusiness;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletResponse;

import login.LoginBean;

import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.commons.io.IOUtils;
import org.apache.myfaces.trinidad.model.UploadedFile;

@ManagedBean(name = "uploadPaymentOrderBean")
@ViewScoped
public class UploadPaymentOrderBacking extends GTechModelBacking {

    private UploadPaymentOrderBean uploadPaymentOrderBean = new UploadPaymentOrderBean();
    private ResponseContainer ResponseContainer = new ResponseContainer();
    private UploadPaymentOrderBusiness uploadPaymentOrderBusiness = new UploadPaymentOrderBusiness();
    private ActionLogBusiness actionLogBusiness = new ActionLogBusiness();
    private boolean vaildToSave ;
    private List<ErrorUploadBean> errorUploadBeanList = new ArrayList<>();

    public void fileUpdate(ValueChangeEvent valueChangeEvent) throws IOException, GTechException, Exception {
        try{
         
        RichInputFile inputFileComponent = (RichInputFile) valueChangeEvent.getComponent();
        UploadedFile newFile = (UploadedFile) valueChangeEvent.getNewValue();
        if (newFile.getContentType().equals("text/xml")) {
            uploadPaymentOrderBean.setUserId(Integer.valueOf(getLogedInUser().getUserID()));
            ParsePaymentOrder parsePaymentOrder = new ParsePaymentOrder();
            uploadPaymentOrderBean = parsePaymentOrder.parsePaymentOrder(newFile.getInputStream());
            uploadPaymentOrderBean.setFileName(newFile.getFilename());
            fillUploadPaymentOrder();
            validation(getLogedInUser());
            List<PaymentOrderBean> paymentOrderBeanList = uploadPaymentOrderBean.getPaymentOrderBeanList();
            if(paymentOrderBeanList == null || paymentOrderBeanList.isEmpty()){
                uploadPaymentOrderBean.setTotalBadRecodrs(0);
                uploadPaymentOrderBean.setTotalRecord(0);
                uploadPaymentOrderBean.setTotalSuccessRecodrs(0);
            }else{
                uploadPaymentOrderBean.setTotalRecord(paymentOrderBeanList.size());
                for (PaymentOrderBean paymentOrderBean : paymentOrderBeanList) {
                    String remark = paymentOrderBean.getRemark();
                    if (remark == null || remark.isEmpty()) {
                        incrementSuccess();
                        vaildToSave = true;
                    }else{
                        incrementBad();
                    }
                }
            }
            uploadPaymentOrderBusiness.insertFile(uploadPaymentOrderBean,getLogedInUser().getUserID());
        } else {
            uploadPaymentOrderBean = new UploadPaymentOrderBean();
            ResponseContainer.addErrorMessage("ERROR_FILE_TYPE");
            createMessages(ResponseContainer);
        }
        inputFileComponent.setLocalValueSet(false);
        inputFileComponent.resetValue();
        inputFileComponent.setValid(false);
        inputFileComponent.setSubmittedValue(null);
        inputFileComponent.setValue(null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(inputFileComponent);
        reloadComponent("fileNameId");
        reloadComponent("uploadTableId");
        reloadComponent("saveBtnId");
        reloadComponent("downloadBtnId");
        }catch(Exception ex){
            ex.printStackTrace();
            ResponseContainer.addErrorMessage("ERROR_FORMAT_TYPE");
            uploadPaymentOrderBean = new UploadPaymentOrderBean();
            createMessages(ResponseContainer);
        }
    }
    
    
    public void fileUpdateOnDb(ValueChangeEvent valueChangeEvent) throws IOException, GTechException, Exception {
        try{
         
        RichInputFile inputFileComponent = (RichInputFile) valueChangeEvent.getComponent();
        UploadedFile newFile = (UploadedFile) valueChangeEvent.getNewValue();
        if (newFile.getContentType().equals("text/xml")) {
            uploadPaymentOrderBean.setUserId(Integer.valueOf(getLogedInUser().getUserID()));
            ParsePaymentOrder parsePaymentOrder = new ParsePaymentOrder();
            uploadPaymentOrderBean.setFileName(newFile.getFilename());
            uploadPaymentOrderBean.setFileInputStream(newFile.getInputStream());
            
            uploadPaymentOrderBusiness.uploadFromDb(uploadPaymentOrderBean,getLogedInUser().getUserID());
        } else {
            uploadPaymentOrderBean = new UploadPaymentOrderBean();
            ResponseContainer.addErrorMessage("ERROR_FILE_TYPE");
            createMessages(ResponseContainer);
        }
        inputFileComponent.setLocalValueSet(false);
        inputFileComponent.resetValue();
        inputFileComponent.setValid(false);
        inputFileComponent.setSubmittedValue(null);
        inputFileComponent.setValue(null);
        AdfFacesContext.getCurrentInstance().addPartialTarget(inputFileComponent);
        reloadComponent("fileNameId");
        reloadComponent("uploadTableId");
        reloadComponent("saveBtnId");
        reloadComponent("downloadBtnId");
        reloadComponent("totalBadRecodrsId");
        reloadComponent("totalSuccessRecodrsId");
        reloadComponent("totalRecordId");
        }catch(Exception ex){
            ex.printStackTrace();
            ResponseContainer.addErrorMessage("ERROR_UPLOAD_FILE");
            uploadPaymentOrderBean = new UploadPaymentOrderBean();
            createMessages(ResponseContainer);
        }
    }

    private void incrementSuccess(){
        if(uploadPaymentOrderBean.getTotalSuccessRecodrs() == null){
            uploadPaymentOrderBean.setTotalSuccessRecodrs(1);
        }else{
            uploadPaymentOrderBean.setTotalSuccessRecodrs(uploadPaymentOrderBean.getTotalSuccessRecodrs()+1);
        }
    }

    private void incrementBad(){
        if(uploadPaymentOrderBean.getTotalBadRecodrs() == null){
            uploadPaymentOrderBean.setTotalBadRecodrs(1);
        }else{
            uploadPaymentOrderBean.setTotalBadRecodrs(uploadPaymentOrderBean.getTotalBadRecodrs()+1);
        }
    }

    private void fillUploadPaymentOrder() throws GTechException {
        UploadPaymentOrderBusiness uploadPaymentOrderBusiness = new UploadPaymentOrderBusiness();
        uploadPaymentOrderBusiness.fillUploadPaymentOrder(uploadPaymentOrderBean);
    }

    private void validation(LoginBean loginBean) throws GTechException {
        uploadPaymentOrderBusiness.validation(uploadPaymentOrderBean, loginBean);
    }

    public void doSave() throws GTechException {
        PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
        if (paymentOrderBusiness.doSave(uploadPaymentOrderBean,getLogedInUser().getUserID())) {
            for (PaymentOrderBean paymentOrderBean : uploadPaymentOrderBean.getPaymentOrderBeanList()) {
                if(paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()){
                    continue ;
                }
                paymentOrderBean.setCheck(true);
            }
            if(actionLogBusiness.insertFile(uploadPaymentOrderBean.getPaymentOrderBeanList(), 100501, getLogedInUser().getUserID())){
                vaildToSave = false;
                paymentOrderBusiness.checkStatusAndUpdateToReview(uploadPaymentOrderBean.getPaymentOrderBeanList(),getLogedInUser().getUserID());
                ResponseContainer.addInfoMessage("RECORD_WAS_ADDED_SUCCESSFULLY");
            }else{
                ResponseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
            }
        } else {
            ResponseContainer.addErrorMessage("UN_COMPLETED_ADDED_CHANGE");
        }
        reloadComponent("saveBtnId");
        createMessages(ResponseContainer);

    }

    public void clearListener(ActionEvent event) {
        uploadPaymentOrderBean = new UploadPaymentOrderBean();
        vaildToSave = false;
        reloadComponent("fileNameId");
        reloadComponent("uploadTableId");
        reloadComponent("downloadBtnId");
        reloadComponent("saveBtnId");
        reloadComponent("totalBadRecodrsId");
        reloadComponent("totalSuccessRecodrsId");
        reloadComponent("totalRecordId");
    }

    public void downloadExcel() throws FileNotFoundException, IOException {
        WriteXls writeXls = new WriteXls();
        FileInputStream fileInputStream = null;
        List<PaymentOrderBean> listOfPaymentOrderBean = new ArrayList<>();
        for(PaymentOrderBean paymentOrderBean :uploadPaymentOrderBean.getPaymentOrderBeanList()){

            if(paymentOrderBean.getRemark() == null || paymentOrderBean.getRemark().isEmpty()){
                continue ;
            }
            listOfPaymentOrderBean.add(paymentOrderBean);
        }
        File temp = writeXls.writeXlsReport(listOfPaymentOrderBean);
        if (temp.exists()) {
            fileInputStream = new FileInputStream(temp);

        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        response.setContentLength(100);
        response.reset();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=CustomerClaims.xls;");
        
        org.apache.commons.io.IOUtils.copy(fileInputStream, response.getOutputStream());
        IOUtils.closeQuietly(fileInputStream);
        facesContext.responseComplete();
    }
    
    public void downloadExcelUploadDb() throws FileNotFoundException, IOException {
        WriteXls writeXls = new WriteXls();
        FileInputStream fileInputStream = null;
        List<ErrorUploadBean> listOfPaymentOrderBean = new ArrayList<>();
        for(ErrorUploadBean errorUploadBean :uploadPaymentOrderBean.getUploadBeanList()){

            if(errorUploadBean.getError() == null || errorUploadBean.getError().isEmpty()){
                continue ;
            }
            listOfPaymentOrderBean.add(errorUploadBean);
        }
        File temp = writeXls.writeXlsReport1(listOfPaymentOrderBean) ;
        if (temp.exists()) {
            fileInputStream = new FileInputStream(temp);

        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        
        response.reset();

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=CustomerClaims.xls;");

        IOUtils.copyLarge(fileInputStream, response.getOutputStream());
        fileInputStream.close();
        facesContext.responseComplete();
    }


    public void setUploadPaymentOrderBean(UploadPaymentOrderBean uploadPaymentOrderBean) {
        this.uploadPaymentOrderBean = uploadPaymentOrderBean;
    }

    public UploadPaymentOrderBean getUploadPaymentOrderBean() {
        return uploadPaymentOrderBean;
    }


    public void setVaildToSave(boolean vaildToSave) {
        this.vaildToSave = vaildToSave;
    }

    public boolean isVaildToSave() {
        return vaildToSave;
    }

    @Override
    public String initialize() throws GTechException {
        uploadPaymentOrderBean = new UploadPaymentOrderBean();

        return "PO-F2";
    }


    public void setErrorUploadBeanList(List<ErrorUploadBean> errorUploadBeanList) {
        this.errorUploadBeanList = errorUploadBeanList;
    }

    public List<ErrorUploadBean> getErrorUploadBeanList() {
        return errorUploadBeanList;
    }
}
