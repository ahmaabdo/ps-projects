package com.gtech.web.uploadPaymentOrder.business;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBusiness;
import com.gtech.web.uploadPaymentOrder.bean.LoadedFileBean;
import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.dao.UploadPaymentOrderDAO;

import java.io.InputStream;

import java.util.List;

import login.LoginBean;


public class UploadPaymentOrderBusiness extends GTechModelBusiness {

    private UploadPaymentOrderDAO uploadPaymentOrderDAO = new UploadPaymentOrderDAO();

    public boolean insertFile(UploadPaymentOrderBean uploadPaymentOrderBean,String userId) throws GTechException {
     return uploadPaymentOrderDAO.insertFile(uploadPaymentOrderBean,userId);   
     }

    public void fillUploadPaymentOrder (UploadPaymentOrderBean uploadPaymentOrderBean) throws GTechException {
        uploadPaymentOrderDAO.fillUploadPaymentOrder(uploadPaymentOrderBean);
    }
    
    public  void validation(UploadPaymentOrderBean uploadPaymentOrderBean, LoginBean loginBean) throws GTechException {
        uploadPaymentOrderDAO.validation(uploadPaymentOrderBean, loginBean);

    }
    
    public Integer insertERPFile(String fileName,InputStream inputStream,String userId) throws GTechException {
      return uploadPaymentOrderDAO.insertERPFileInfo(fileName,inputStream, userId);
    }
    
    public void parseERPFileXml(Integer fileId) throws GTechException {
       uploadPaymentOrderDAO.parseERPFileXmlThread(fileId);
    }
    
    public List<LoadedFileBean> getERPFileInfo(Integer userId,String date) throws GTechException {
        return uploadPaymentOrderDAO.getERPFileInfo(userId, date);
    }
    
    public InputStream getERPFileInfoById(Long id) throws GTechException {
        return uploadPaymentOrderDAO.getERPFileInfoById(id);
    }
    
    public void updateXMLFileStatus(Long id) throws GTechException {
        uploadPaymentOrderDAO.updateXMLFileStatus(id);
    }
    public  void uploadFromDb(UploadPaymentOrderBean uploadPaymentOrderBean, String userId) throws GTechException {
        uploadPaymentOrderDAO.insertFileUsingFunction(uploadPaymentOrderBean, userId);

    }

}
