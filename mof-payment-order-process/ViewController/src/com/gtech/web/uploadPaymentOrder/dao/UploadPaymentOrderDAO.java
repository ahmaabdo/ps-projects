package com.gtech.web.uploadPaymentOrder.dao;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.faces.util.MessageReader;
import com.gtech.web.adminhijridate.dao.CalendarDAO;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.beneficiary.dao.BeneficiaryDAO;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.currency.dao.CurrencyDAO;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.department.dao.DepartmentDAO;
import com.gtech.web.ecomomicClassifications.dao.EcomomicClassificationDAO;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.entity.dao.EntityDAO;
import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.errorUpload.dao.ErrorUploadDAO;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.nationality.dao.NationalityDao;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.project.dao.ProjectDAO;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.section.dao.SectionDAO;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.bean.LoadedFileBean;
import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import login.LoginBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class UploadPaymentOrderDAO extends GTechDAO {
    private static Log log = LogFactory.getLog(UploadPaymentOrderDAO.class);
    private ErrorUploadDAO errorUploadDAO = new ErrorUploadDAO();
    
    private  void uploadFuncation(Integer fileId, Connection connection) throws GTechException {
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.load_xml_file(?) }";
        try {
            callableStatement = connection.prepareCall(query);
            
            callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
            callableStatement.setInt(2,fileId);
            
            // execute getDBUSERByUserId store procedure
            callableStatement.executeUpdate();
               
            
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void validation(UploadPaymentOrderBean uploadPaymentOrderBean, LoginBean loginBean) throws GTechException {

        Connection connection = null;
        CallableStatement callableStatement = null;
        String query = " { ? = call POG_UTIL.validate_po(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        List<String> paymentOrderList = new ArrayList<>();
        try {
            connection = getConnection();

            callableStatement = connection.prepareCall(query);
            for (PaymentOrderBean paymentOrderBean : uploadPaymentOrderBean.getPaymentOrderBeanList()) {
                if(paymentOrderList != null && !paymentOrderList.isEmpty() && paymentOrderList.contains(paymentOrderBean.getPaymentOrderNumber())){
                    
                    paymentOrderBean.setRemark(MessageReader.getMessage("PAYMENT_ORDER_EXISTS"));
                    continue;
                }
                paymentOrderList.add(paymentOrderBean.getPaymentOrderNumber());
                callableStatement.registerOutParameter(1, Types.LONGNVARCHAR);
                callableStatement.setString(2,  checkEmpty(paymentOrderBean.getPaymentOrderNumber()));
                callableStatement.setString(3,  checkEmpty(paymentOrderBean.getPaymentOrderDateH()));
                callableStatement.setString(4,  checkEmpty(paymentOrderBean.getPaymentOrderYear()));
                callableStatement.setString(5,  checkEmpty(paymentOrderBean.getEntityBean().getCode()));
                callableStatement.setString(6,  checkEmpty(paymentOrderBean.getDepartmentBean().getCode()));
                callableStatement.setString(7,  checkEmpty((paymentOrderBean.getSectionBean().getCode())));
                callableStatement.setString(8,  checkEmpty(paymentOrderBean.getBeneficiaryBean().getCode()));
                callableStatement.setString(9,  checkEmpty(paymentOrderBean.getIban()));        
                callableStatement.setString(10, checkEmpty(paymentOrderBean.getPaymentMethod()));
                callableStatement.setString(11, checkEmpty(paymentOrderBean.getCurrencyBean().getCode()));
                callableStatement.setString(12, checkEmpty(paymentOrderBean.getAmount()));
                callableStatement.setString(13, checkEmpty(paymentOrderBean.getAmountInWord())); 
                callableStatement.setString(14, "");
                callableStatement.setString(16, checkEmpty(paymentOrderBean.getNationalityBean().getNationalityCode()));
                callableStatement.setString(17, checkEmpty(paymentOrderBean.getExchangeOf()));
                callableStatement.setString(18, "AR");
                callableStatement.setString(19, "1");
                callableStatement.setString(20, checkEmpty(loginBean.getUserID()));
                callableStatement.setString(21, checkEmpty(paymentOrderBean.getBeneficaryName()));
                StringBuffer distribution = new StringBuffer();
                for (DistributionsPaymentOrderBean distributionsBean : paymentOrderBean.getDistributionsPoBeanList()) {
                    distribution.append(checkEmpty(distributionsBean.getProjectBean().getCode()));
                    distribution.append("#");
                    distribution.append(checkEmpty(distributionsBean.getEcomomicClassificationBean().getCode()));
                    distribution.append("#");
                    distribution.append(checkEmpty(distributionsBean.getAmount()));
                    distribution.append("#");
                    distribution.append(checkEmpty(distributionsBean.getRefundCategoryCode()));
                    distribution.append("#");
                    distribution.append(checkEmpty(distributionsBean.getRefundAmount()));
                    distribution.append("#@");
                }
                callableStatement.setString(15, distribution.toString());

                // execute getDBUSERByUserId store procedure
                callableStatement.executeUpdate();
                paymentOrderBean.setRemark(callableStatement.getString(1));
                if(paymentOrderBean.getRemark() != null && !paymentOrderBean.getRemark().isEmpty()){
                    String[] parts = paymentOrderBean.getRemark().split("--");
                    if(parts != null && parts.length>0){
                        String part = parts[0];
                        if(part == null && part.isEmpty()){
                            paymentOrderBean.setRemarkRows(parts.length-1);
                        }else{
                            paymentOrderBean.setRemarkRows(parts.length);
                        }
                    }
                }else{
                    paymentOrderBean.setRemarkRows(1);
                }
            }
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            try {
            callableStatement.closeOnCompletion();
            } catch (Exception e) {
                e.printStackTrace();
            }
            closeResources(connection);
        }
    }
    
    public String checkEmpty(String object){
        
        if(object == null){
            
            return "";
        }
        
        return object; 
    }
    
    public String checkEmpty(Double object){
        
        if(object == null || object.doubleValue() == 0.0){
            
            return "";
        }
        
        return object.toString(); 
    }
    
    public String checkEmpty(Long object){
        
        if(object == null || object.longValue() == 0L){
            
            return "";
        }
        
        return object.toString(); 
    }

    public void fillUploadPaymentOrder(UploadPaymentOrderBean uploadPaymentOrderBean) throws GTechException {

        Connection connection = null;
        BeneficiaryDAO beneficiaryDao = new BeneficiaryDAO();
        CurrencyDAO currencyDao = new CurrencyDAO();
        DepartmentDAO departmentDao = new DepartmentDAO();
        EntityDAO entityDao = new EntityDAO();
        SectionDAO sectionDao = new SectionDAO();
        ProjectDAO projectDao = new ProjectDAO();
        NationalityDao nationalityDao = new NationalityDao();

        EcomomicClassificationDAO ecomomicClassificationDAO = new EcomomicClassificationDAO();
        try {
            connection = getConnection();
            for(PaymentOrderBean paymentOrderBean : uploadPaymentOrderBean.getPaymentOrderBeanList()){
                paymentOrderBean.setBeneficiaryBean(beneficiaryDao.getBeneficiary(paymentOrderBean.getBeneficiaryBean().getCode(),connection));
                if(paymentOrderBean.getBeneficiaryBean() == null){
                    paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
                }
                paymentOrderBean.setCurrencyBean(currencyDao.getCurrency(paymentOrderBean.getCurrencyBean().getCode(),connection));
                if(paymentOrderBean.getCurrencyBean() == null){
                    paymentOrderBean.setCurrencyBean(new CurrencyBean());
                }
                paymentOrderBean.setDepartmentBean(departmentDao.getDepartment(paymentOrderBean.getDepartmentBean().getCode(),connection));
                if(paymentOrderBean.getDepartmentBean() == null){
                    paymentOrderBean.setDepartmentBean(new DepartmentBean());
                }
                paymentOrderBean.setEntityBean(entityDao.getEntity(paymentOrderBean.getEntityBean().getCode(),connection));
                if(paymentOrderBean.getEntityBean() == null){
                    paymentOrderBean.setEntityBean(new EntityBean());
                }
                paymentOrderBean.setSectionBean(sectionDao.getSection(paymentOrderBean.getSectionBean().getCode(),connection));
                if(paymentOrderBean.getSectionBean() == null){
                    paymentOrderBean.setSectionBean(new SectionBean());
                }
                paymentOrderBean.setExpenseBean(null);
                paymentOrderBean.setNationalityBean(nationalityDao.getNationality(paymentOrderBean.getNationalityBean().getNationalityCode(),connection));
                if(paymentOrderBean.getNationalityBean() == null){
                    paymentOrderBean.setNationalityBean(new NationalityBean());
                }
                for(DistributionsPaymentOrderBean distributionsPaymentOrderBean : paymentOrderBean.getDistributionsPoBeanList()){
                    distributionsPaymentOrderBean.setProjectBean(projectDao.getProject(distributionsPaymentOrderBean.getProjectBean().getCode(),connection));
                    distributionsPaymentOrderBean.setEcomomicClassificationBean(ecomomicClassificationDAO.getEcomomicClassification(distributionsPaymentOrderBean.getEcomomicClassificationBean().getCode(),connection));
                }
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
    }

    public boolean insertFile(UploadPaymentOrderBean uploadPaymentOrderBean,String userId) throws GTechException {

        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            Integer fileId = getSeq("PDA_PK_SEQ", connection);
            boolean isInserted = fileId != null && insertFileInfo(uploadPaymentOrderBean, fileId, connection,userId);

            if (isInserted) {
                for (PaymentOrderBean paymentOrderBean : uploadPaymentOrderBean.getPaymentOrderBeanList()) {
                    isInserted = insertTempFileInfo(paymentOrderBean, fileId, connection);
                    if (!isInserted) {
                        break;
                    }
                }
            }

            if (isInserted) {
                connection.setAutoCommit(true);
                connection.commit();
            } else {
                connection.rollback();
            }

        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection);
        }
        return true;

    }
    
    public void insertFileUsingFunction(UploadPaymentOrderBean uploadPaymentOrderBean,String userId) throws GTechException {

        Connection connection = null;
        try {
            connection = getConnection();
            
            Integer fileId = getSeq("PDA_PK_SEQ", connection);
            boolean isInserted = fileId != null && insertFileInfoUsingFunction(uploadPaymentOrderBean, fileId, connection,userId);

            uploadFuncation(fileId,connection);
            findUploadInfo(uploadPaymentOrderBean , fileId , connection);
            uploadPaymentOrderBean.setUploadBeanList(errorUploadDAO.getUploadList(fileId, connection)); 
             
           
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } catch (UnsupportedEncodingException e) {
        } finally {
            closeResources(connection);
        }

    }
    
    private boolean insertFileInfoUsingFunction(UploadPaymentOrderBean uploadPaymentOrderBean, Integer fileId,
                                   Connection connection,String userId) throws GTechException, SQLException,
                                                                                             UnsupportedEncodingException {
        CalendarDAO calendarDAO = new CalendarDAO();
        PreparedStatement preparedStatement = null;
        String query =
            "INSERT INTO POG_LOADED_FILES " +
            "(PDA_ID, PDA_FILE_NAME, PDA_LOAD_BY_SSC_ID, PDA_LOAD_ON_GR, PDA_LOAD_ON_HI , " +
            " PDA_TOT_RECORDS , PDA_SUCCESS_RECORDS , PDA_BAD_RECORDS , PDA_LOADING_STATUS , PDA_FILE_CONTENT) VALUES" +
            "(?,?,?,SYSDATE,?,?,?,?,?,?)";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, fileId);
        preparedStatement.setString(2, uploadPaymentOrderBean.getFileName());
        preparedStatement.setString(3, userId);
        preparedStatement.setString(4, calendarDAO.getCurrentHijriDate(connection));
        preparedStatement.setInt(5, 0);
        preparedStatement.setInt(6,0);
        preparedStatement.setInt(7,0);
        preparedStatement.setInt(8,100700);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(uploadPaymentOrderBean.getFileInputStream(), "windows-1256"));
        preparedStatement.setClob(9,bufferedReader);
        // execute insert SQL stetement
        return preparedStatement.executeUpdate() > 0;
    }

    private boolean insertFileInfo(UploadPaymentOrderBean uploadPaymentOrderBean, Integer fileId,
                                   Connection connection,String userId) throws GTechException, SQLException {
        CalendarDAO calendarDAO = new CalendarDAO();
        PreparedStatement preparedStatement = null;
        String query =
            "INSERT INTO POG_LOADED_FILES " +
            "(PDA_ID, PDA_FILE_NAME, PDA_LOAD_BY_SSC_ID, PDA_LOAD_ON_GR, PDA_LOAD_ON_HI , " +
            " PDA_TOT_RECORDS , PDA_SUCCESS_RECORDS , PDA_BAD_RECORDS , PDA_LOADING_STATUS) VALUES" +
            "(?,?,?,SYSDATE,?,?,?,?,?)";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, fileId);
        preparedStatement.setString(2, uploadPaymentOrderBean.getFileName());
        preparedStatement.setString(3, userId);
        preparedStatement.setString(4, calendarDAO.getCurrentHijriDate(connection));
        preparedStatement.setInt(5, uploadPaymentOrderBean.getTotalRecord());
        preparedStatement.setInt(6,
                                 uploadPaymentOrderBean.getTotalSuccessRecodrs() == null ? 0 :
                                 uploadPaymentOrderBean.getTotalSuccessRecodrs());
        preparedStatement.setInt(7,
                                 uploadPaymentOrderBean.getTotalBadRecodrs() == null ? 0 :
                                 uploadPaymentOrderBean.getTotalBadRecodrs());
        preparedStatement.setInt(8, uploadPaymentOrderBean.getTotalBadRecodrs() == null ? 100701 : 100702);
        // execute insert SQL stetement
        return preparedStatement.executeUpdate() > 0;
    }
    
    public void findUploadInfo(UploadPaymentOrderBean uploadPaymentOrderBean, Integer FileId,  Connection connection) throws GTechException {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<ErrorUploadBean> errorUploadBeanList = new ArrayList<>();
        String query = "SELECT PDA_TOT_RECORDS , PDA_SUCCESS_RECORDS , PDA_BAD_RECORDS  FROM POG_LOADED_FILES   WHERE PDA_ID = ?"; 
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, FileId);
            rs = preparedStatement.executeQuery();
            while(rs.next()) {
                uploadPaymentOrderBean.setTotalBadRecodrs(rs.getInt("PDA_BAD_RECORDS"));
                uploadPaymentOrderBean.setTotalRecord(rs.getInt("PDA_TOT_RECORDS"));
                uploadPaymentOrderBean.setTotalSuccessRecodrs(rs.getInt("PDA_SUCCESS_RECORDS"));
            }
          
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            closeResources(preparedStatement, rs);
        }

    }

    private boolean insertTempFileInfo(PaymentOrderBean paymentOrderBean, Integer fileId,
                                       Connection connection) throws GTechException, SQLException {

        PreparedStatement preparedStatement = null;
        String query =
            "INSERT INTO POG_PAYMENT_ORDERS_STAGING " + "(IED_CODE, IED_REMARKS, IED_PDA_ID) VALUES" + "(?,?,?)";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, paymentOrderBean.getPaymentOrderNumber());
        preparedStatement.setString(2, paymentOrderBean.getRemark());
        preparedStatement.setInt(3, fileId);

        // execute insert SQL stetement
        return preparedStatement.executeUpdate() > 0;
    }
    
    public Integer insertERPFileInfo(String fileName,InputStream is, String userId) throws GTechException {
        Connection connection = null;
        ResultSet rs= null;
        PreparedStatement preparedStatement = null;
        Integer fileId ;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            fileId = getSeq("PDA_PK_SEQ", connection);
            CalendarDAO calendarDAO = new CalendarDAO();
            
            String query =
                "INSERT INTO POG_LOADED_FILES " +
                "(PDA_ID, PDA_FILE_NAME, PDA_LOAD_BY_SSC_ID, PDA_LOAD_ON_GR, PDA_LOAD_ON_HI , " +
                " PDA_FILE_CONTENT,PDA_FILE_STATUS ) VALUES" +
                "(?,?,?,SYSDATE,?,?,?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, fileId);
            preparedStatement.setString(2, fileName);
            preparedStatement.setString(3, userId);
            preparedStatement.setString(4, calendarDAO.getCurrentHijriDate(connection));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "windows-1256"));
            preparedStatement.setClob(5,bufferedReader);
            preparedStatement.setString(6, "UPLOADED");

            preparedStatement.executeUpdate();
            
           
            connection.commit();
        } catch (SQLException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } catch (IOException e) {
            log.error("Exception :", e);
            try {
                connection.rollback();
            } catch (SQLException f) {
                throw new GTechException(e);
            }
            throw new GTechException(e);
        } finally {
            closeResources(connection,preparedStatement,rs);
        }
        // execute insert SQL stetement
        
        return fileId;
    }
    
    public void parseERPFileXmlThread(Integer fileId){
        new Thread(new Runnable() {
             public void run() {
                try {
                    parseERPFileXml(fileId);
                } catch (GTechException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    
    public void parseERPFileXml(Integer fileId) throws GTechException {
        Connection connection = null;
        CallableStatement  callableStatement  = null;
        try {
            connection = getConnection();
//            connection.setAutoCommit(false);
//            fileId = getSeq("PDA_PK_SEQ", connection);
//            CalendarDAO calendarDAO = new CalendarDAO();
            
            String query =
                "{call POG_UTIL.load_and_parse_xml(?)}";
            callableStatement = connection.prepareCall(query);
            callableStatement.setInt(1, fileId);
            
            callableStatement.execute();
            
            
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        } finally {
            try {
                callableStatement.close();
            } catch (SQLException e) {
            }
            closeResources(connection);
        }
        // execute insert SQL stetement
        
    }
    
    public List<LoadedFileBean> getERPFileInfo(Integer userId,String date) throws GTechException {
        List<LoadedFileBean> loadedFileBeanList = new ArrayList<>();
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        ResultSet rs= null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnection();
            
            String query =
                "SELECT * FROM POG_LOADED_FILES LFILE WHERE LFILE.PDA_LOAD_BY_SSC_ID = ? " + 
                "AND LFILE.PDA_LOAD_ON_GR < TRUNC(SYSDATE+1) " + 
                "AND LFILE.PDA_LOAD_ON_GR > TRUNC(SYSDATE - 30) ";
            
            if(date != null) {
                query = query + " AND ";
                
                query = query + " TRUNC(PDA_LOAD_ON_HI) <=  TO_DATE('"+calendarDAO.getGrDateByHijriDate(connection, date)+"', 'DD/MM/YYYY')";
                
            }
            query = query + "ORDER BY PDA_LOAD_ON_GR DESC";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, userId);
            
            rs = preparedStatement.executeQuery();
            
            LoadedFileBean loadedFileBean = null;
            while(rs.next()) {
                loadedFileBean = new LoadedFileBean();
//                byte[] bytes = rs.getBytes("PDA_EXCEL_RESULT");
//                if(bytes != null && bytes.length > 0) {
//                    loadedFileBean.setExcelFile(new ByteArrayInputStream(bytes));
//                }
                
                loadedFileBean.setFileName(rs.getString("PDA_FILE_NAME") != null ? rs.getString("PDA_FILE_NAME").replace(".xml", "") : rs.getString("PDA_FILE_NAME"));
                loadedFileBean.setUserId(rs.getInt("PDA_LOAD_BY_SSC_ID"));
                loadedFileBean.setHijriDate(rs.getString("PDA_LOAD_ON_HI"));
                loadedFileBean.setStatus(rs.getString("PDA_FILE_STATUS"));
                loadedFileBean.setId(rs.getLong("PDA_ID"));
                
                loadedFileBeanList.add(loadedFileBean);
            }
            return loadedFileBeanList;    
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        }  finally {
            closeResources(connection,preparedStatement,rs);
        }
        // execute insert SQL stetement
        

    }
    
    public InputStream getERPFileInfoById(Long id) throws GTechException {
        List<LoadedFileBean> loadedFileBeanList = new ArrayList<>();
        CalendarDAO calendarDAO = new CalendarDAO();
        Connection connection = null;
        ResultSet rs= null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnection();
            
            String query =
                "SELECT * FROM POG_LOADED_FILES LFILE WHERE LFILE.PDA_ID = ? " + 
                "AND LFILE.PDA_LOAD_ON_GR < TRUNC(SYSDATE+1) " + 
                "AND LFILE.PDA_LOAD_ON_GR > TRUNC(SYSDATE - 30) ";
            
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            
            rs = preparedStatement.executeQuery();
            
            LoadedFileBean loadedFileBean = null;
            if(rs.next()) {
                loadedFileBean = new LoadedFileBean();
                byte[] bytes = rs.getBytes("PDA_EXCEL_RESULT");
                if(bytes != null && bytes.length > 0) {
                    loadedFileBean.setExcelFile(new ByteArrayInputStream(bytes));
                }
                
                return loadedFileBean.getExcelFile();
            }
            return null;    
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        }  finally {
            closeResources(connection,preparedStatement,rs);
        }
        // execute insert SQL stetement
        

    }
    
    public void updateXMLFileStatus(Long id) throws GTechException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnection();
            
            String query =
                "UPDATE POG_LOADED_FILES SET PDA_FILE_STATUS ='PROCESSING' WHERE PDA_ID = ?";
            
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            
            preparedStatement.executeUpdate();
            
               
        } catch (SQLException e) {
            log.error("Exception :", e);
            throw new GTechException(e);
        }  finally {
            closeResources(connection,preparedStatement);
        }
        // execute insert SQL stetement
        

    }
}
