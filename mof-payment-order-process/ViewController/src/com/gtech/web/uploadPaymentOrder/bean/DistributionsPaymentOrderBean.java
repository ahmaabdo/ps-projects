package com.gtech.web.uploadPaymentOrder.bean;

import com.gtech.common.base.BaseBean;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.project.bean.ProjectBean;

public class DistributionsPaymentOrderBean extends BaseBean {

    public static final String DISTRIBUTIONS_PROGRAM_NUMBER = "PROGRAM_NUMBER";
    public static final String DISTRIBUTIONS_PO_ECONOMIC_CLASSIFICATION = "ECONOMIC_CLASSIFICATION";
    public static final String DISTRIBUTIONS_PO_AMOUNT = "AMOUNT";
    public static final String DISTRIBUTIONS_PO_REFUND_CATEGORY_CODE = "REFUND_CATEGORY_CODE";
    public static final String DISTRIBUTIONS_PO_REFUND_AMOUNT = "REFUND_AMOUNT";

    private ProjectBean projectBean;
    private EcomomicClassificationBean ecomomicClassificationBean;
    private String refundCategoryCode;
    private Double refundAmount;
    private String subSubCode;
    private Double amount;
    private String progDesc;
    private String projectClassCode;
    private String sonCofogCode;

    public void setProjectBean(ProjectBean projectBean) {
        this.projectBean = projectBean;
    }

    public ProjectBean getProjectBean() {
        return projectBean;
    }

    public void setEcomomicClassificationBean(EcomomicClassificationBean ecomomicClassificationBean) {
        this.ecomomicClassificationBean = ecomomicClassificationBean;
    }

    public EcomomicClassificationBean getEcomomicClassificationBean() {
        return ecomomicClassificationBean;
    }

    public void setRefundCategoryCode(String refundCategoryCode) {
        this.refundCategoryCode = refundCategoryCode;
    }

    public String getRefundCategoryCode() {
        return refundCategoryCode;
    }

    public void setRefundAmount(Double refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Double getRefundAmount() {
        return refundAmount;
    }

    public void setSubSubCode(String subSubCode) {
        this.subSubCode = subSubCode;
    }

    public String getSubSubCode() {
        return subSubCode;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setProgDesc(String progDesc) {
        this.progDesc = progDesc;
    }

    public String getProgDesc() {
        return progDesc;
    }

    public void setProjectClassCode(String projectClassCode) {
        this.projectClassCode = projectClassCode;
    }

    public String getProjectClassCode() {
        return projectClassCode;
    }

    public void setSonCofogCode(String sonCofogCode) {
        this.sonCofogCode = sonCofogCode;
    }

    public String getSonCofogCode() {
        return sonCofogCode;
    }

}
