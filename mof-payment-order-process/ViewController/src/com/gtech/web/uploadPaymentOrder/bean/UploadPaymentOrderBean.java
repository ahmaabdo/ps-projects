package com.gtech.web.uploadPaymentOrder.bean;

import com.gtech.common.base.BaseBean;
import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;

import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;

public class UploadPaymentOrderBean extends BaseBean{

    public static final String PAYMENT_ORDER_NUMBER = "PO_NUMBER";
    public static final String PAYMENT_ORDER_DATE = "PO_DATE";
    public static final String PAYMENT_ORDER_YEARS = "PO_YEARS";
    public static final String PAYMENT_ORDER_ENTITY = "PO_ENTITY";
    public static final String PAYMENT_ORDER_DEPARTMENT = "PO_DEPARTMENT";
    public static final String PAYMENT_ORDER_SECTION = "PO_SECTION";
    public static final String PAYMENT_ORDER_BENEFICIARY_NUMBER = "PO_BENEFICIARY_NUMBER";
    public static final String PAYMENT_ORDER_IBAN = "PO_IBAN";
    public static final String PAYMENT_ORDER_PAYMENT_TYPE = "PO_PAYMENT_TYPE";
    public static final String PAYMENT_ORDER_CURRENCY = "PO_CURRENCY";
    public static final String PAYMENT_ORDER_AMOUNT = "PO_AMOUNT";
    public static final String PAYMENT_ORDER_AMOUNT_IN_WORD = "PO_AMOUNT_IN_WORD";
    public static final String PO_ONE_TIME_SUPPLIER_NAME = "PO_ONE_TIME_SUPPLIER_NAME";
    public static final String PAYMENT_ORDER_EXPENSE = "PO_EXPENSE";
    public static final String PAYMENT_ORDER_EXCHANGE_OF = "PO_EXCHANGE_OF";
    public static final String PAYMENT_ORDER_BENEFICIARY_NATIONALITY = "PO_BENEFICIARY_NATIONALITY";
    public static final String PAYMENT_ORDER = "PO";
    public static final String PAYMENT_ORDER_DISTRIBUTIONS = "DISTRIBUTIONS";


    private String fileName;
    private List<PaymentOrderBean> paymentOrderBeanList;
    private Integer totalRecord;
    private Integer totalSuccessRecodrs;
    private Integer totalBadRecodrs;
    private InputStream fileInputStream;
    private List<ErrorUploadBean> uploadBeanList = new ArrayList<>();
    private String contentFile;


    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setPaymentOrderBeanList(List<PaymentOrderBean> paymentOrderBeanList) {
        this.paymentOrderBeanList = paymentOrderBeanList;
    }

    public List<PaymentOrderBean> getPaymentOrderBeanList() {
        return paymentOrderBeanList;
    }

    public void setTotalRecord(Integer totalRecord) {
        this.totalRecord = totalRecord;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public void setTotalSuccessRecodrs(Integer totalSuccessRecodrs) {
        this.totalSuccessRecodrs = totalSuccessRecodrs;
    }

    public Integer getTotalSuccessRecodrs() {
        return totalSuccessRecodrs;
    }

    public void setTotalBadRecodrs(Integer totalBadRecodrs) {
        this.totalBadRecodrs = totalBadRecodrs;
    }

    public Integer getTotalBadRecodrs() {
        return totalBadRecodrs;
    }


    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }


    public void setUploadBeanList(List<ErrorUploadBean> uploadBeanList) {
        this.uploadBeanList = uploadBeanList;
    }

    public List<ErrorUploadBean> getUploadBeanList() {
        return uploadBeanList;
    }

    public void setContentFile(String contentFile) {
        this.contentFile = contentFile;
    }

    public String getContentFile() {
        return contentFile;
    }
}
