package com.gtech.web.uploadPaymentOrder.bean;


public class DISTRIBUTIONS {
    
    private String PROGRAM_NUMBER;
    private long ECONOMIC_CLASSIFICATION;
    private double AMOUNT;

    public void setPROGRAM_NUMBER(String PROGRAM_NUMBER) {
        this.PROGRAM_NUMBER = PROGRAM_NUMBER;
    }

    public String getPROGRAM_NUMBER() {
        return PROGRAM_NUMBER;
    }

    public void setECONOMIC_CLASSIFICATION(long ECONOMIC_CLASSIFICATION) {
        this.ECONOMIC_CLASSIFICATION = ECONOMIC_CLASSIFICATION;
    }

    public long getECONOMIC_CLASSIFICATION() {
        return ECONOMIC_CLASSIFICATION;
    }

    public void setAMOUNT(double AMOUNT) {
        this.AMOUNT = AMOUNT;
    }

    public double getAMOUNT() {
        return AMOUNT;
    }
}
