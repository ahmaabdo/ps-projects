package com.gtech.web.uploadPaymentOrder.bean;

import java.util.ArrayList;
import java.util.List;

public class ROOT {
    
    private String username;
    
    private String password;
    
    private List<PO> PO = new ArrayList<>();

    public void setPO(List<PO> PO) {
        this.PO = PO;
    }

    public List<PO> getPO() {
        return PO;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
