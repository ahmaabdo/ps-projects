package com.gtech.web.uploadPaymentOrder.bean;

import java.util.ArrayList;
import java.util.List;

public class PO {
    
    private long PO_NUMBER;
    private String PO_DATE;
    private long PO_ENTITY;
    private long PO_DEPARTMENT;
    private long PO_SECTION;
    private long PO_ORGANIZATION;
    private long PO_BENEFICIARY_NUMBER;
    private String PO_IBAN;
    private String PO_CURRENCY;
    private String PO_PAYMENT_TYPE;
    private String PO_EXCHANGE_OF;
    private String PO_ONE_TIME_SUPPLIER_NAME;
    private String PO_BENEFICIARY_NATIONALITY;
    private String PO_EXPENSE;
    private double PO_AMOUNT;
    private List<DISTRIBUTIONS> DISTRIBUTIONS = new ArrayList<>();


    public void setPO_NUMBER(long PO_NUMBER) {
        this.PO_NUMBER = PO_NUMBER;
    }

    public long getPO_NUMBER() {
        return PO_NUMBER;
    }

    public void setPO_DATE(String PO_DATE) {
        this.PO_DATE = PO_DATE;
    }

    public String getPO_DATE() {
        return PO_DATE;
    }

    public void setPO_ENTITY(long PO_ENTITY) {
        this.PO_ENTITY = PO_ENTITY;
    }

    public long getPO_ENTITY() {
        return PO_ENTITY;
    }

    public void setPO_DEPARTMENT(long PO_DEPARTMENT) {
        this.PO_DEPARTMENT = PO_DEPARTMENT;
    }

    public long getPO_DEPARTMENT() {
        return PO_DEPARTMENT;
    }

    public void setPO_SECTION(long PO_SECTION) {
        this.PO_SECTION = PO_SECTION;
    }

    public long getPO_SECTION() {
        return PO_SECTION;
    }

    public void setPO_ORGANIZATION(long PO_ORGANIZATION) {
        this.PO_ORGANIZATION = PO_ORGANIZATION;
    }

    public long getPO_ORGANIZATION() {
        return PO_ORGANIZATION;
    }

    public void setPO_BENEFICIARY_NUMBER(long PO_BENEFICIARY_NUMBER) {
        this.PO_BENEFICIARY_NUMBER = PO_BENEFICIARY_NUMBER;
    }

    public long getPO_BENEFICIARY_NUMBER() {
        return PO_BENEFICIARY_NUMBER;
    }

    public void setPO_IBAN(String PO_IBAN) {
        this.PO_IBAN = PO_IBAN;
    }

    public String getPO_IBAN() {
        return PO_IBAN;
    }

    public void setPO_CURRENCY(String PO_CURRENCY) {
        this.PO_CURRENCY = PO_CURRENCY;
    }

    public String getPO_CURRENCY() {
        return PO_CURRENCY;
    }

    public void setPO_PAYMENT_TYPE(String PO_PAYMENT_TYPE) {
        this.PO_PAYMENT_TYPE = PO_PAYMENT_TYPE;
    }

    public String getPO_PAYMENT_TYPE() {
        return PO_PAYMENT_TYPE;
    }

    public void setPO_EXCHANGE_OF(String PO_EXCHANGE_OF) {
        this.PO_EXCHANGE_OF = PO_EXCHANGE_OF;
    }

    public String getPO_EXCHANGE_OF() {
        return PO_EXCHANGE_OF;
    }

    public void setPO_ONE_TIME_SUPPLIER_NAME(String PO_ONE_TIME_SUPPLIER_NAME) {
        this.PO_ONE_TIME_SUPPLIER_NAME = PO_ONE_TIME_SUPPLIER_NAME;
    }

    public String getPO_ONE_TIME_SUPPLIER_NAME() {
        return PO_ONE_TIME_SUPPLIER_NAME;
    }

    public void setPO_BENEFICIARY_NATIONALITY(String PO_BENEFICIARY_NATIONALITY) {
        this.PO_BENEFICIARY_NATIONALITY = PO_BENEFICIARY_NATIONALITY;
    }

    public String getPO_BENEFICIARY_NATIONALITY() {
        return PO_BENEFICIARY_NATIONALITY;
    }

    public void setPO_AMOUNT(double PO_AMOUNT) {
        this.PO_AMOUNT = PO_AMOUNT;
    }

    public double getPO_AMOUNT() {
        return PO_AMOUNT;
    }

    public void setDISTRIBUTIONS(List<DISTRIBUTIONS> DISTRIBUTIONS) {
        this.DISTRIBUTIONS = DISTRIBUTIONS;
    }

    public List<DISTRIBUTIONS> getDISTRIBUTIONS() {
        return DISTRIBUTIONS;
    }

    public void setPO_EXPENSE(String PO_EXPENSE) {
        this.PO_EXPENSE = PO_EXPENSE;
    }

    public String getPO_EXPENSE() {
        return PO_EXPENSE;
    }
}
