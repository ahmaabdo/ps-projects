package com.gtech.web.uploadPaymentOrder.bean;

import com.gtech.common.base.BaseBean;

import java.io.InputStream;

public class LoadedFileBean  extends BaseBean{
    
    private String fileName;
    private String hijriDate;
    private InputStream excelFile;
    private String status;
     
    public LoadedFileBean() {
        super();
    }


    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setHijriDate(String hijriDate) {
        this.hijriDate = hijriDate;
    }

    public String getHijriDate() {
        return hijriDate;
    }

    public void setExcelFile(InputStream excelFile) {
        this.excelFile = excelFile;
    }

    public InputStream getExcelFile() {
        return excelFile;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
