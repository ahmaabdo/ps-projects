package com.gtech.web.createPassword.backing;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.admin.user.UserBusiness;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "firstTimeLoginBacking")
@RequestScoped
public class FirstTimeLoginBacking extends GTechModelBacking {
    
    private String oldPassword;
    private String newPassword;
    
    
    public FirstTimeLoginBacking() {
        super();
    }
    
    
    @PostConstruct
    public void init() {
        
    }
    
    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }
    
    public String updatePassword() {
        if (oldPassword != null && newPassword != null && !oldPassword.equals(newPassword)) {
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addErrorMessage("CONFIRM_PASSWORD_VALIDATION");
                createMessages(responseContainer);
                return null;
          }
        
        UserBusiness business = new UserBusiness();
        try {
            boolean check = business.updatePassowrd(oldPassword, getLogedInUser().getUserID());
            if(check) {
                business.updateStatus("100002", getLogedInUser().getUserID());
            }
        } catch (GTechException e) {
            e.printStackTrace();
        }
        
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedInUser");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedUserName");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedInUserFullName");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedBean");
        
        return "goToLogin";
    }
    
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }
}
