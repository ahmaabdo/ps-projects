package com.gtech.common.base;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;


public class BaseBean implements Serializable, Cloneable {
    private long id;
    private String creationUser;
    private Integer userId;
    private String creationDate;
    private String lastUpdateUser;
    private String lastUpdateDate;
    private List valuesList = new ArrayList();

    public BaseBean() {
    }

    public Object clone() {
        Object clone = null;

        try {
            clone = super.clone();
        } catch (CloneNotSupportedException cne) {
            System.err.println("Error while cloning Object " + cne.getMessage());
        }

        return clone;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setLastUpdateUser(String lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public String getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setValuesList(List valuesList) {
        this.valuesList = valuesList;
    }

    public List getValuesList() {
        return valuesList;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public Integer getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof BaseBean)) {
            return false;
        }
        final BaseBean other = (BaseBean) object;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = 1;
        result = PRIME * result + (int) (id ^ (id >>> 32));
        return result;
    }
}

