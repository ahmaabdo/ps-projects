package com.gtech.common.model;

import com.gtech.common.reader.CommonConfigReader;
import com.gtech.common.util.StringUtilities;
import com.gtech.common.beans.container.ResponseContainer;
import java.io.File;
import java.util.List;

public abstract class GTechModelBusiness {
    private ResponseContainer responseContainer = new ResponseContainer();
    public static final String TempFolderName = CommonConfigReader.getValue("TEMP_FOLDER");

    protected boolean validateRequiredValue(String value, String messageKey) {
        final boolean result = StringUtilities.isNotEmpty(value);
        if (!result) {
            responseContainer.addErrorMessage(messageKey);
        }
        return result;
    }


    protected boolean isNumericValue(String value, String messageKey) {
        if (!isNumericValue(value)) {
            responseContainer.addErrorMessage(messageKey);
            return false;
        }
        return true;
    }

    protected boolean isNumericValue(String value) {
        if (StringUtilities.isNotEmpty(value)) {
            return value.matches("[0-9]+");
        }
        return true;
    }


    protected boolean validateRequiredValue(String value, String messageKey,
                                            String... messageArguments) {
        final boolean result = StringUtilities.isNotEmpty(value);
        if (!result) {
            responseContainer.addErrorMessage(messageKey,
                                                  messageArguments);
        }
        return result;
    }

    protected boolean validateRequiredValues(String... values) {
        for (String value : values) {
            if (StringUtilities.isEmpty(value)) {
                return false;
            }
        }
        return true;
    }

    protected boolean validateOneRequiredValue(String... values) {
        for (String value : values) {
            if (StringUtilities.isNotEmpty(value)) {
                return true;
            }
        }
        return false;
    }

    protected boolean validateRequiredValue(String value) {
        final boolean result = StringUtilities.isNotEmpty(value);
        return result;
    }

    protected boolean isListEmpty(List listOfValues, String messageKey) {
        final boolean result =
            (listOfValues == null || listOfValues.size() == 0);
        if (result) {
            responseContainer.addErrorMessage(messageKey);
        }
        return result;
    }

    protected boolean isListEmpty(List listOfValues, String messageKey,
                                  String... messageArguments) {
        final boolean result =
            (listOfValues == null || listOfValues.size() == 0);
        if (result) {
            responseContainer.addErrorMessage(messageKey,
                                                  messageArguments);
        }
        return result;
    }

    protected boolean deleteFileOnDisk(String deletedFileName) {
        File file = new File(TempFolderName + deletedFileName);
        if (file.exists()) {
            return file.delete();
        } else {
            return false;
        }
    }

    protected boolean isListNotEmpty(List listOfValues) {
        return !isListEmpty(listOfValues);
    }

    protected boolean isListEmpty(List listOfValues) {
        final boolean result =
            (listOfValues == null || listOfValues.size() == 0);
        return result;
    }

    protected void addWarningMessage(String messageKey,
                                     String... messageArguments) {
        responseContainer.addWarningMessage(messageKey, messageArguments);
    }

    protected void addErrorMessage(String messageKey,
                                   String... messageArguments) {
        responseContainer.addErrorMessage(messageKey, messageArguments);
    }

    protected void addInfoMessage(String messageKey,
                                  String... messageArguments) {
        responseContainer.addInfoMessage(messageKey, messageArguments);
    }

    protected boolean isResponseContainsErrorMessages() {
        return responseContainer.containsErrorMessages();
    }

    protected boolean isResponseContainsWarningMessages() {
        return responseContainer.containsWarningMessages();
    }

    protected boolean isResponseContainsInfoMessages() {
        return responseContainer.containsInfoMessages();
    }

    public ResponseContainer getResponseContainer() {
        return responseContainer;
    }

    public static boolean isValidEmailExpression(String emailstr) {
        return emailstr.matches("[a-zA-Z0-9_.-]+@[a-zA-Z0-9_.-]+\\.[a-zA-Z]+");
    }    
}
