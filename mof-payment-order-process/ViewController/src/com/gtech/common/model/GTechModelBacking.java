package com.gtech.common.model;

import com.gtech.common.base.BaseBean;
import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.reader.CommonConfigReader;
import com.gtech.faces.backing.ModelBacking;

import java.util.Calendar;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import login.LoginBean;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.fragment.RichRegion;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.component.core.data.CoreTable;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public abstract class GTechModelBacking extends ModelBacking {
    private String paginationTabelSize = "5";
    
    private String poDateFrom;
    private String poDateTo;
    private String poNumber;
    private String benNumber;
    private String ibanAccNumber;
    
    public static final String TempFolderName = CommonConfigReader.getValue("TEMP_FOLDER");


    public GTechModelBacking() {
    }

    public void reloadComponent(String componentId){
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
         FacesContext facesContext = FacesContext.getCurrentInstance();
         UIComponent component = facesContext.getViewRoot().findComponent(componentId);
         if(component != null){
                adfFacesContext.addPartialTarget(component);
        }
    }

    public String getPaginationTabelSize() {
        return paginationTabelSize;
    }

    public void setaginationTabelSize(String paginationTabelSize) {
        this.paginationTabelSize = paginationTabelSize;
    }


    protected void createMessages(ResponseContainer responseBean) {
           createMessages(responseBean.getInfoMessagesList());
           createMessages(responseBean.getErrorMessagesList());
           createMessages(responseBean.getWarningMessagesList());
       }


    protected UIComponent getComponentInCoreTable(String componentId, int rowIndex, int columnIndex) {

            UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();

            CoreTable coreTable = (CoreTable) vr.findComponent(componentId.split(":")[0]);
            coreTable.setRowIndex(rowIndex);

            int index = 0;

            UIComponent uiComponent = null;

            for (UIComponent column : coreTable.getChildren()) {
                if (index == columnIndex) {
                    for (UIComponent component : column.getChildren()) {

                        if (component.getClientId(FacesContext.getCurrentInstance()).equals(componentId)) {
                            uiComponent = component;
                            break;
                        } else {
                            for (UIComponent componentChild : component.getChildren()) {
                                if (componentChild.getClientId(FacesContext.getCurrentInstance()).equals(componentId)) {
                                    uiComponent = componentChild;
                                    break;
                                }
                            }
                        }
                    }
                }
                index++;
            }
            return uiComponent;
        }

    protected void addPartialTrigger(String componentId) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIComponent component = facesContext.getViewRoot().findComponent(componentId);
        RequestContext.getCurrentInstance().addPartialTarget(component);
    }

    protected void addPartialTriggerInRegion(String componentId) {
      UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
      RichRegion richRegion = (RichRegion) vr.findComponent("r1");

      UIComponent component = richRegion.findComponent(componentId);
      RequestContext.getCurrentInstance().addPartialTarget(component);
    }

    protected void addPartialTrigger(UIComponent component) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        RequestContext.getCurrentInstance().addPartialTarget(component);
    }

     protected <T extends BaseBean> T copyBean(T bean) throws GTechException{
        return ((T) bean.clone());
    }

    protected RichInputText getInputText(String inputId) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIViewRoot vr = facesContext.getViewRoot();
        RichInputText inputText = (RichInputText) vr.findComponent(inputId);

        return inputText;
    }
    
    protected RichPopup getPopup(String inputId) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIViewRoot vr = facesContext.getViewRoot();
        RichPopup richPopup = (RichPopup) vr.findComponent(inputId);

        return richPopup;
    }
    
    protected RichPopup getPopupDialog(String inputId) {
      UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
      RichRegion richRegion = (RichRegion) vr.findComponent("r1");

      RichPopup richPopup = (RichPopup) richRegion.findComponent(inputId);
      return richPopup;
    }

    protected RichInputText getInputTextFromRichRegion(String inputId) {
        UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
        RichRegion richRegion = (RichRegion) vr.findComponent("r1");

        RichInputText inputText = (RichInputText) richRegion.findComponent(inputId);
        return inputText;
    }

    protected void addPartialTriggerFromRichRegion(String componentId) {
        UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
        RichRegion richRegion = (RichRegion) vr.findComponent("r1");

        UIComponent inputText = richRegion.findComponent(componentId);
        RequestContext.getCurrentInstance().addPartialTarget(inputText);
    }

    protected RichInputText getInputTextFromRichRegion(UIComponent componentId) {
        RichInputText inputText = (RichInputText) componentId;
        return inputText;
    }




    protected RichInputText getInputText(UIComponent componentId) {
        RichInputText inputText = (RichInputText) componentId;
        return inputText;
    }


    public String getCurrentDate(){
     Calendar cal = Calendar.getInstance();

     String currentDate = (cal.get(Calendar.DAY_OF_MONTH)<10 ? "0"+cal.get(Calendar.DAY_OF_MONTH):cal.get(Calendar.DAY_OF_MONTH)) + "/";
            currentDate+= ((cal.get(Calendar.MONTH)+1)<10?"0"+(cal.get(Calendar.MONTH)+1):(cal.get(Calendar.MONTH)+1)) + "/";
            currentDate+= cal.get(Calendar.YEAR);

     return currentDate;
    }

    protected void callJavaScriptFunction(String functionName){
     FacesContext facesContext = FacesContext.getCurrentInstance();
     ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
     service.addScript(facesContext, functionName);
    }

    protected LoginBean getLogedInUser(){
        return (LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loggedBean");
    }

    public boolean havaPermition(String formNo){
        LoginBean lb = (LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loggedBean");
        String listOfPermition = lb.getPermition();
        if(listOfPermition !=null &&listOfPermition.contains(formNo+",")){
            return true;
        }
        return false;
    }
    
    public boolean isValidSearch() {
//        if((poDateFrom == null && poDateTo != null) || 
//            (poDateTo == null && poDateFrom != null)) {
//            
//            ResponseContainer responseContainer = new ResponseContainer();
//            responseContainer.addErrorMessage("PO_CRAITRIA_VALIDATION");
//            createMessages(responseContainer);
//            
//            return false;
//        }
        
        if(true) {
            return true;
        }
        if(poDateTo != null ||
           poDateFrom != null ||
           poNumber != null ||
           benNumber != null ||
           ibanAccNumber != null) {
            return true;
        }
        
        ResponseContainer responseContainer = new ResponseContainer();
        responseContainer.addErrorMessage("PO_CRAITRIA_VALIDATION");
        createMessages(responseContainer);
        
        return false;
    }
    
    public void resetSearchParam() {
    
        poDateFrom = null;
        poDateTo = null;
        poNumber = null;
        benNumber = null;
        ibanAccNumber = null;
    }
    
    public void setPoDateFrom(String poDateFrom) {
        this.poDateFrom = poDateFrom;
    }

    public String getPoDateFrom() {
        return poDateFrom;
    }

    public void setPoDateTo(String poDateTo) {
        this.poDateTo = poDateTo;
    }

    public String getPoDateTo() {
        return poDateTo;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setBenNumber(String benNumber) {
        this.benNumber = benNumber;
    }

    public String getBenNumber() {
        return benNumber;
    }

    public void setIbanAccNumber(String ibanAccNumber) {
        this.ibanAccNumber = ibanAccNumber;
    }

    public String getIbanAccNumber() {
        return ibanAccNumber;
    }
}
