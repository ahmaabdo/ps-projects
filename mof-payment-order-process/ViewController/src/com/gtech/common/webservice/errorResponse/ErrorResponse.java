package com.gtech.common.webservice.errorResponse;

import java.io.Serializable;

import java.util.List;

public class ErrorResponse implements Serializable{
    @SuppressWarnings("compatibility:6150618738582201443")
    private static final long serialVersionUID = 1L;

    private String errorMsg;
    
    private String errorType;
    
    private List<String> msg;
    
    private String paymentOrderNumber;
    
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorType() {
        return errorType;
    }


    public void setMsg(List<String> msg) {
        this.msg = msg;
    }

    public List<String> getMsg() {
        return msg;
    }

    public void setPaymentOrderNumber(String paymentOrderNumber) {
        this.paymentOrderNumber = paymentOrderNumber;
    }

    public String getPaymentOrderNumber() {
        return paymentOrderNumber;
    }
}
