package com.gtech.common.keys;

public interface DataKeys {

   

    // Condition
    public static String APPLICATION_LOCALE = "AR";
    public static final String CONDITION_EQUAL = "1";
    public static final String CONDITION_LESS_THAN = "2";
    public static final String CONDITION_LESS_THAN_OR_EQUAL = "3";
    public static final String CONDITION_GREATER_THAN = "4";
    public static final String CONDITION_GREATER_THAN_OR_EQUAL = "5";
    public static final String CONDITION_BETWEEN = "6";

  
  
}
