package com.gtech.common.util;

import com.gtech.common.reader.*;

import java.text.*;

import java.util.*;

public class DateUtilities {

    private static String dateSeperator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
    private static String projectDateFormat = CommonConfigReader.getValue("DEFAULT_DATE_FORMAT");

    public static String getCurrentDate() {

        DateFormat dateFormatter = new SimpleDateFormat(projectDateFormat);
        Date currentDate = new Date();
        String formattedCurrentDate = dateFormatter.format(currentDate);
        return formattedCurrentDate;
    }

    public static String getCurrentDateTime() {
        String projectDateFormat = CommonConfigReader.getValue("DEFAULT_DATETTIME_FORMAT");
        DateFormat dateFormatter = new SimpleDateFormat(projectDateFormat);
        Date currentDate = new Date();
        String formattedCurrentDate = dateFormatter.format(currentDate);
        return formattedCurrentDate;
    }

    public static String getCurrentTime() {
        DateFormat dateFormatter = new SimpleDateFormat("HH:mm");
        Date currentDate = new Date();
        String formattedCurrentDate = dateFormatter.format(currentDate);
        return formattedCurrentDate;
    }

    public static String getCurrentTimev() {
        DateFormat dateFormatter = new SimpleDateFormat("hh:mm");
        Date currentDate = new Date();
        String formattedCurrentDate = dateFormatter.format(currentDate);
        return formattedCurrentDate;
    }


    public static String getCurrentYear() {
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        return year;
    }


    public static String formatDate(String dateString) {
        if (StringUtilities.isNotEmpty(dateString)) {
            String[] dateValues = dateString.split(" ");
            String dateValue = dateValues[0];

            String[] dateParts = dateValue.split("-");

            return dateParts[2] + dateSeperator + dateParts[1] + dateSeperator + dateParts[0];
        }

        return dateString;
    }

    public static String formatTime(Date date, String pataren) {
        SimpleDateFormat formatter = new SimpleDateFormat(pataren);
        return formatter.format(date);
    }

    public static String formatDateTime(String dateString) {
        if (StringUtilities.isNotEmpty(dateString)) {

            String[] dateValues = dateString.split(" ");
            String dateTimeFormated = "";

            String dateValue = dateValues[0];
            String[] dateParts = dateValue.split("-");
            dateTimeFormated = dateParts[2] + dateSeperator + dateParts[1] + dateSeperator + dateParts[0];

            if (dateValues.length > 1) {
                String timeValue = dateValues[1];
                String[] TimeParts = timeValue.split(":");
                dateTimeFormated += "  " + TimeParts[0] + ":" + TimeParts[1];
            }

            return dateTimeFormated;
        }

        return dateString;
    }


    public static String getAndFormatDateTime(String dateString) {
        if (StringUtilities.isNotEmpty(dateString)) {

            String[] dateValues = dateString.split(" ");
            String dateTimeFormated = "";

            String dateValue = dateValues[0];
            String[] dateParts = dateValue.split("-");
            dateTimeFormated = dateParts[2] + dateSeperator + dateParts[1] + dateSeperator + dateParts[0];

            if (dateValues.length > 1) {
                String timeValue = dateValues[1];
                String[] TimeParts = timeValue.split(":");
                dateTimeFormated += "  " + TimeParts[0] + ":" + TimeParts[1];
            }

            return dateTimeFormated;
        }

        return dateString;
    }

    public static String formatTime(String timeString) {
        if (StringUtilities.isNotEmpty(timeString)) {
            //  String dateSeperator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
            String[] dateValues = timeString.split(" ");
            String dateValue = dateValues[0];
            String timeValue = dateValues[1];

            String[] dateParts = dateValue.split("-");
            String[] TimeParts = timeValue.split(":");

            return TimeParts[0] + ":" + TimeParts[1];
        }

        return timeString;
    }

    public static String removeTimeSeparator(String time) {
        return time.replace(":", "");
    }


    public static int compareDate2(String fromDate, String toDate) throws ParseException {
        String projectDateFormat = CommonConfigReader.getValue("DEFAULT_DATE_FORMAT");
        DateFormat dateFormat = new SimpleDateFormat(projectDateFormat);
        Date fromDateObject = dateFormat.parse(fromDate);
        Date toDateObject = dateFormat.parse(toDate);
        long from = fromDateObject.getTime();
        long to = toDateObject.getTime();
        long diff = fromDateObject.getTime() - toDateObject.getTime();
        int noofdays = (int) (diff / (1000 * 24 * 60 * 60));
        return noofdays;
    }

    public static int compareDate(String fromDate, String toDate) throws ParseException {
        String projectDateFormat = CommonConfigReader.getValue("DEFAULT_DATE_FORMAT");
        DateFormat dateFormat = new SimpleDateFormat(projectDateFormat);
        Date fromDateObject = dateFormat.parse(fromDate);
        Date toDateObject = dateFormat.parse(toDate);
        long from = fromDateObject.getTime();
        long to = toDateObject.getTime();
        if (from < to) {
            return 1;
        } else if (from == to) {
            return 0;
        } else {
            return -1;
        }
    }

    public static boolean isValidDateFormat(String date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonConfigReader.getValue("DEFAULT_DATE_FORMAT"));
        Date testDate = null;
        try {
            testDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return false;
        }

        if (!simpleDateFormat.format(testDate).equals(date)) {
            return false;
        }
        return true;
    }

    public static Date parseDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonConfigReader.getValue("DEFAULT_DATE_FORMAT"));
        Date testDate = null;
        try {
            testDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
        return testDate;
    }

}
