package com.gtech.common.util;

import com.gtech.common.exception.GTechException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import java.time.LocalDate;
import java.time.chrono.HijrahChronology;
import java.time.chrono.HijrahDate;

import java.util.Calendar;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class EncryptionUtilities {

    static private Cipher ecipher;
    static private Cipher dcipher;
    static private SecretKey key;
    static private String keyStr = "GreenTechCommKey";
    static private String keyStr2 = "P@$$w0rdGT";
    static private boolean ZASitekey;

    private EncryptionUtilities() {
    }

    public static String convertCharset(String text, String charset) {
        String temp = text;
        if (StringUtilities.isNotEmpty(text)) {
            try {
                temp = new String(text.getBytes(charset));
            } catch (UnsupportedEncodingException e) {
                // TODO: handle exception
            }
        }

        return temp;
    }

    private static void generateSecretKey() throws GTechException {

        try {
            byte[] secret;
            if (ZASitekey) {
                secret = keyStr2.getBytes("UTF8");
            } else {
                secret = keyStr.getBytes("UTF8");
            }

            SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
            DESKeySpec desSpec = new DESKeySpec(secret);
            key = skf.generateSecret(desSpec);

        } catch (UnsupportedEncodingException unEx) {
            throw new GTechException("EncryptionUtilities:generateSecretKey: " + unEx.getMessage(), unEx);
        } catch (NoSuchAlgorithmException noEx) {
            throw new GTechException("EncryptionUtilities:generateSecretKey: ", noEx);
        } catch (InvalidKeyException inEx) {
            throw new GTechException("EncryptionUtilities:generateSecretKey: " + inEx.getMessage(), inEx);
        } catch (InvalidKeySpecException innEx) {
            throw new GTechException("EncryptionUtilities:generateSecretKey: " + innEx.getMessage(), innEx);
        }

    }

    public static String desEncrypt(String str) throws GTechException {
        ZASitekey = false;
        try {
            if (ecipher == null) {
                generateSecretKey();
                ecipher = Cipher.getInstance("DES");
                ecipher.init(Cipher.ENCRYPT_MODE, key);
            }
            byte[] utf8 = str.getBytes("UTF8");
            byte[] enc = ecipher.doFinal(utf8);
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (GTechException coEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + coEx.getMessage(), coEx);
        } catch (NoSuchAlgorithmException nsEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + nsEx.getMessage(), nsEx);
        } catch (NoSuchPaddingException nopEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + nopEx.getMessage(), nopEx);
        } catch (InvalidKeyException ikEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + ikEx.getMessage(), ikEx);
        } catch (UnsupportedEncodingException unsEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + unsEx.getMessage(), unsEx);
        } catch (IllegalBlockSizeException ilbsEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + ilbsEx.getMessage(), ilbsEx);
        } catch (BadPaddingException bpEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + bpEx.getMessage(), bpEx);
        }
    }

    public static String desDecrypt(String str) throws GTechException {
        ZASitekey = false;
        try {
            if (dcipher == null) {
                generateSecretKey();
                dcipher = Cipher.getInstance("DES");
                dcipher.init(Cipher.DECRYPT_MODE, key);
            }
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
            byte[] utf8 = dcipher.doFinal(dec);
            return new String(utf8, "UTF8");
        } catch (GTechException coEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + coEx.getMessage(), coEx);
        } catch (NoSuchAlgorithmException nsEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + nsEx.getMessage(), nsEx);
        } catch (NoSuchPaddingException nopEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + nopEx.getMessage(), nopEx);
        } catch (InvalidKeyException ikEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + ikEx.getMessage(), ikEx);
        } catch (UnsupportedEncodingException unsEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + unsEx.getMessage(), unsEx);
        } catch (IllegalBlockSizeException ilbsEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + ilbsEx.getMessage(), ilbsEx);
        } catch (BadPaddingException bpEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + bpEx.getMessage(), bpEx);
        } catch (IOException ioEx) {
            throw new GTechException("EncryptionUtilities:desEncrypt: " + ioEx.getMessage(), ioEx);
        }
    }

    public static void main(String[] a) throws GTechException{
       // System.out.println(desEncrypt("1062946841"));
        System.out.println(desDecrypt("aP9j8QzvAgs="));
//                    Date dateG=new Date();
//            Calendar cl=Calendar.getInstance();
//            cl.setTime(dateG);
//            
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, 2018);
//        cal.set(Calendar.DAY_OF_YEAR, 1); 
//        
//            HijrahDate islamyDate=HijrahChronology.INSTANCE.date(LocalDate.of(cl.get(Calendar.YEAR),cl.get(Calendar.MONTH)+1, cl.get(Calendar.DATE))); 
//        HijrahDate islamyDate1=HijrahChronology.INSTANCE.date(LocalDate.of(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH)+1, cal.get(Calendar.DATE))); 
//        
//        System.out.println(islamyDate);       
//        System.out.println(islamyDate1);   
        
    }
}
