package com.gtech.common.util;

import com.gtech.common.reader.CommonConfigReader;

import java.util.StringTokenizer;

public class HijriDateUtilities {
    static String dateSeparator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
    

    private HijriDateUtilities() {
    }

    public static String getCurrentHijriDateAsString() {
        return String.valueOf(getCurrentHijriDateAsNumber());
    }

    public static long getCurrentHijriDateAsNumber() {
        String hijriWithoutSep = removeDateSeperators(getCurrentHijriDate());

        long hijriDate = Long.parseLong(hijriWithoutSep);

        return hijriDate;
    }

    public static String getCurrentHijriDateTimeAsString() {
        return String.valueOf(getCurrentHijriDateTimeAsNumber());
    }

    public static long getCurrentHijriDateTimeAsNumber() {
        String hijriWithoutSep = removeDateSeperators(getCurrentHijriDate());
        String timeWithoutSep = DateUtilities.removeTimeSeparator(DateUtilities.getCurrentTime());

        long hijriDate = Long.parseLong(hijriWithoutSep + timeWithoutSep);

        return hijriDate;
    }

    public static String formatHijriDate(String dateTime) {
        if (StringUtilities.isNotEmpty(dateTime)) {
            return formatHijriDate(Long.parseLong(dateTime));
        }

        return dateTime;
    }

    public static String formatHijriDate(long dateTime) {
        String dateTimeString = String.valueOf(dateTime);
        String year="";
        String month="";
        String day="";
        if (dateTimeString.length() >= 8) {
             year = dateTimeString.substring(0, 4);
             month = dateTimeString.substring(4, 6);
             day = dateTimeString.substring(6, 8);
        }


        String formattedDate = day + dateSeparator + month + dateSeparator + year;

        return formattedDate;
    }

    public static String formatHijriDateForReports(String dateTime) {
        if (StringUtilities.isNotEmpty(dateTime)) {
            return formatHijriDateForReports(Long.parseLong(dateTime));
        }

        return dateTime;
    }

    public static String formatHijriDateForReports(long dateTime) {
        String dateTimeString = String.valueOf(dateTime);
        String dateSeparator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");

        String year = dateTimeString.substring(0, 4);
        String month = dateTimeString.substring(4, 6);
        String day = dateTimeString.substring(6, 8);

        String formattedDate = day + dateSeparator + month + dateSeparator + year;

        return formattedDate;
    }

    public static String formatHijriTime(String dateTime) {
        if (StringUtilities.isNotEmpty(dateTime)) {
            return formatHijriTime(Long.parseLong(dateTime));
        }

        return dateTime;
    }

    public static String formatHijriTime(long dateTime) {
        String dateTimeString = String.valueOf(dateTime);

        String time = dateTimeString.substring(8, 12);
        String hours = time.substring(0, 2);
        String minutes = time.substring(2, 4);

        String formattedTime = hours + ":" + minutes;

        return formattedTime;
    }

    public static String formatHijriDateTimeForReports(String dateTime) {
        if (StringUtilities.isEmpty(dateTime)) {
            return dateTime;
        }

        return formatHijriDateForReports(dateTime) + " " + formatHijriTime(dateTime);
    }

    public static String formatHijriDateTime(String dateTime) {
        if (StringUtilities.isEmpty(dateTime)) {
            return dateTime;
        }

        return formatHijriDate(dateTime) + " " + formatHijriTime(dateTime);
    }

    public static long getHijriDateTimeAfterHours(String dateTime, int hours) {
        if (StringUtilities.isNotEmpty(dateTime)) {
            return getHijriDateTimeAfterHours(Long.parseLong(dateTime), hours);
        }

        return 0;
    }

    public static long getHijriDateTimeAfterHours(long dateTime, int hours) {
        int afterDays = 0;
        int afterHours = 0;

        if (hours >= 24) {
            double temp = hours / 24.0;

            afterDays = (int) temp;
            //afterHours = (int) (Math.ceil((temp % afterDays) * 24));//here
            afterHours = hours - (afterDays * 24);
        } else {
            afterHours = hours;
        }

//        //SYSTEM.OUT.PRINTln(afterDays + " " + afterHours);

        String currentHijriTimeString = DateUtilities.removeTimeSeparator(formatHijriTime(dateTime));
        String currentHijriDateString = removeDateSeparator(formatHijriDateForHours(dateTime));

        int currentHours = Integer.parseInt(currentHijriTimeString.substring(0, 2));
        afterHours = afterHours + currentHours;

        if (afterDays > 0 || afterHours >= 24) {
            int currentAfterDays = (int) (afterHours / 24);

            if (currentAfterDays > 0) {
                int currentAfterHours = (int) (afterHours % 24);
                afterHours = currentAfterHours;
            }

            afterDays = afterDays + currentAfterDays;

            currentHijriTimeString = String.format("%02d%s", afterHours, currentHijriTimeString.substring(2, 4));
            currentHijriDateString = String.valueOf(Long.parseLong(currentHijriDateString) + afterDays);
        } else {
            currentHijriTimeString = String.format("%02d%s", afterHours, currentHijriTimeString.substring(2, 4));
        }

//        //SYSTEM.OUT.PRINTln(afterDays + " " + afterHours);

        long hijriDate = Long.parseLong(currentHijriDateString + currentHijriTimeString);

        return hijriDate;

    }

    /******************* Old Methods ************************/
    public static String removeDateSeperators(String dateString) {
        String dateSeperator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");

        if (dateString == null) {
            return null;
        }

        if (dateString.length() >= 10 && dateString.contains(dateSeperator)) {
            return dateString.substring(6) + dateString.substring(3, 5) + dateString.substring(0, 2);
        } else {
            return dateString;
        }
    }

    public static String getCurrentHijriDate() {
        String dateSeperator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
        String date = DateUtilities.getCurrentDate();
        String dayStr = date.substring(0, 2);

        String monthStr = date.substring(3, 5);
        String yearStr = date.substring(6);

        double d = Double.parseDouble(dayStr);
        double m = Double.parseDouble(monthStr);
        double y = Double.parseDouble(yearStr);

        double jd;
        double l;
        double n;
        double j;

        if ((y > 1582) || ((y == 1582) && (m > 10)) || ((y == 1582) &&
                (m == 10) && (d > 14))) {
            jd = intPart((1461 * (y + 4800 + intPart((m - 14) / 12))) / 4) + intPart((367 * (m - 2 - 12 * (intPart((m - 14) / 12)))) / 12) -
                    intPart((3 * (intPart((y + 4900 + intPart((m - 14) / 12)) / 100))) /
                    4) + d - 32075;
        } else {
            jd = 367 * y - intPart((7 * (y + 5001 + intPart((m - 9) / 7))) / 4) + intPart((275 * m) / 9) + d + 1729777;
        }

        l = jd - 1948440 + 10632;
        n = intPart((l - 1) / 10631);
        l = l - 10631 * n + 354;
        j = (intPart((10985 - l) / 5316)) * (intPart((50 * l) / 17719)) +
                (intPart(l / 5670)) * (intPart((43 * l) / 15238));
        l = l - (intPart((30 - j) / 15)) * (intPart((17719 * j) / 50)) -
                (intPart(j / 16)) * (intPart((15238 * j) / 43)) + 29;
        m = intPart((24 * l) / 709);
        d = l - intPart((709 * m) / 24);
        y = 30 * n + j - 30;

        int day = (int) d;
        int month = (int) (m);
        int year = (int) y;
        day = day + 1;
        if (day > 30) {
            day = day % 30;
            month = month + 1;

        }

        String strDay = Integer.toString(day - 1);
        String strMonth = Integer.toString(month);

        if (strDay.length() == 1) {
            strDay = "0" + strDay;
        }

        if (strMonth.length() == 1) {
            strMonth = "0" + strMonth;
        }

        return strDay + dateSeperator + strMonth + dateSeperator + year;
    }

//    public static String getCurrentHijriDateAfterYears(int years) {
//        String projectDateFormat = CommonConfigReader.getValue("DEFAULT_DATE_FORMAT");
//        DateFormat dateFormatter = new SimpleDateFormat(projectDateFormat);
//        Date currentDate = new Date();
//        int curenrDateYear = currentDate.getYear();
//        curenrDateYear += years;
//        currentDate.setYear(curenrDateYear);
//        String formattedCurrentDate = dateFormatter.format(currentDate);
//
//        String formattedCurrentHijriDate = convertToHijri(formattedCurrentDate);
//
//        return formattedCurrentHijriDate;
//    }

    public static String reverseDate(String date) {
        String dateSeparator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
        String[] splitted = date.split(CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR"));

        if (splitted.length < 3) {
            return "check date format";
        }

        return splitted[2] + dateSeparator + splitted[1] + dateSeparator + splitted[0];
    }

    public static String convertToHijri(String date) {

        String dateSeperator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
        String dayStr = date.substring(0, 2);
        String monthStr = date.substring(3, 5);
        String yearStr = date.substring(6);
        double d = Double.parseDouble(dayStr);
        double m = Double.parseDouble(monthStr);
        double y = Double.parseDouble(yearStr);
        double jd;
        double l;
        double n;
        double j;

        if ((y > 1582) || ((y == 1582) && (m > 10)) || ((y == 1582) && (m == 10) && (d > 14))) {
            jd = intPart((1461 * (y + 4800 + intPart((m - 14) / 12))) / 4) + intPart((367 * (m - 2 - 12 * (intPart((m - 14) / 12)))) / 12) -
                    intPart((3 * (intPart((y + 4900 + intPart((m - 14) / 12)) / 100))) / 4) + d - 32075;
        } else {
            jd = 367 * y - intPart((7 * (y + 5001 + intPart((m - 9) / 7))) / 4) + intPart((275 * m) / 9) + d + 1729777;
        }

        l = jd - 1948440 + 10632;
        n = intPart((l - 1) / 10631);
        l = l - 10631 * n + 354;
        j = (intPart((10985 - l) / 5316)) * (intPart((50 * l) / 17719)) + (intPart(l / 5670)) * (intPart((43 * l) / 15238));
        l = l - (intPart((30 - j) / 15)) * (intPart((17719 * j) / 50)) - (intPart(j / 16)) * (intPart((15238 * j) / 43)) + 29;
        m = intPart((24 * l) / 709);
        d = l - intPart((709 * m) / 24);
        y = 30 * n + j - 30;
        int day = 0;
        double mDate = Double.parseDouble(dayStr);
        if (mDate > 10) {
            day = (int) d + 2;
        } else {
            day = (int) d + 1;
        }

        int month = (int) (m);
        int year = (int) y;

        String strDay = Integer.toString(day);
        String strMonth = Integer.toString(month);

        if (strDay.length() == 1) {
            strDay = "0" + strDay;
        }

        if (strMonth.length() == 1) {
            strMonth = "0" + strMonth;
        }

        return strDay + dateSeperator + strMonth + dateSeperator + year;
    }

    public static String convertToGregorian(String date) {
        String dateSeperator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");
        String dayStr = date.substring(0, 2);
        String monthStr = date.substring(3, 5);
        String yearStr = date.substring(6);
        int day = Integer.parseInt(dayStr);
        int month = Integer.parseInt(monthStr);
        int year = Integer.parseInt(yearStr);

        double jd;
        int l, n, i, j, k;
        jd = ((11 * year + 3) / 30) + 354 * year + 30 * month - ((month - 1) / 2) + day + 1948440 - 386;
        if (jd > 2299160) {
            l = (int) (jd + 68569);
            n = (int) ((4 * l) / 146097);
            l = (int) (l - ((146097 * n + 3) / 4));
            i = (int) ((4000 * (l + 1)) / 1461001);
            l = (int) (l - ((1461 * i) / 4) + 31);
            j = (int) ((80 * l) / 2447);
            day = (int) (l - ((2447 * j) / 80));
            l = (int) (j / 11);
            month = (int) (j + 2 - 12 * l);
            year = (int) (100 * (n - 49) + i + l);
        } else {
            j = (int) (jd + 1402);
            k = (int) ((j - 1) / 1461);
            l = (int) (j - 1461 * k);
            n = (int) (((l - 1) / 365) - (l / 1461));
            i = (int) (l - 365 * n + 30);
            j = (int) ((80 * i) / 2447);
            day = (int) (i - ((2447 * j) / 80));
            i = (j / 11);
            month = (int) (j + 2 - 12 * i);
            year = (int) (4 * k + n + i - 4716);
        }

        double hDate = Double.parseDouble(dayStr);
        if (hDate > 10) {
            day = (int) day - 1;
        } else {
            day = (int) day - 2;
        }

        String strDay = Integer.toString(day);
        String strMonth = Integer.toString(month);

        if (strDay.length() == 1) {
            strDay = "0" + strDay;
        }

        if (strMonth.length() == 1) {
            strMonth = "0" + strMonth;
        }

        return strDay + dateSeperator + strMonth + dateSeperator + year;
    }

    private static double intPart(double floatNum) {
        if (floatNum < -0.0000001) {
            return (int) Math.ceil(floatNum - 0.0000001);
        }

        return (int) Math.floor(floatNum + 0.0000001);
    }

    private static String removeDateSeparator(String dateString) {

        if (dateString == null) {
            return null;
        }

        String dateSeparator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");

        StringTokenizer stringTokenizer = new StringTokenizer(dateString, dateSeparator);
        StringBuffer date = new StringBuffer();
        while (stringTokenizer.hasMoreElements()) {
            date.append(stringTokenizer.nextToken());
        }

        return date.toString();
    }

    private static String formatHijriDateForHours(long dateTime) {
        String dateTimeString = String.valueOf(dateTime);
        String dateSeparator = CommonConfigReader.getValue("DEFAULT_DATE_SEPERATOR");

        String year = dateTimeString.substring(0, 4);
        String month = dateTimeString.substring(4, 6);
        String day = dateTimeString.substring(6, 8);

        String formattedDate = year + dateSeparator + month + dateSeparator + day;

        return formattedDate;
    }

    public static void main(String[] args) {
        //SYSTEM.OUT.PRINTln(getCurrentHijriDate());
    }
}
