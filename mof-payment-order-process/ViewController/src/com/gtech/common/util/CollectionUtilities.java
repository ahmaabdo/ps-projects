package com.gtech.common.util ;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;


public class CollectionUtilities {
//trunck
    private CollectionUtilities() {
    }
    
    public static List getListFromArray(Object[] arr) {
        List list = new ArrayList(Arrays.asList(arr)) ;
        return list ;
    }
    
    public static boolean isEmptyList(List list){
        if(list == null || list.size() == 0){
            return true;
        }
        return false;
    }
    
    public static boolean isNotEmptyList(List list){
          return !isEmptyList(list);
    }
    
    public static void main(String args[]){
         String s[] = {"a","b","c"};
         List list = getListFromArray(s);
         //SYSTEM.OUT.PRINTln(list);
    }
    
}
