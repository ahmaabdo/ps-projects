package com.gtech.common.util;

import com.gtech.common.exception.GTechException;
import com.gtech.common.tafkeet.Tafkeet;
import com.gtech.web.adminhijridate.business.CalendarBusiness;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.generalinfo.business.GeneralInfoBusiness;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.project.bean.ProjectBean;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.bean.UploadPaymentOrderBean;

import java.io.InputStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class ParsePaymentOrder {
    private CalendarBusiness calendarBusiness = new CalendarBusiness();
    private GeneralInfoBusiness generalInfoBusiness = new GeneralInfoBusiness();

    private String getStringElementValue(XMLEventReader xmlEventReader) throws XMLStreamException {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent != null && xmlEvent.isCharacters()) {
            Characters characters = xmlEvent.asCharacters();
            if (characters != null) {
                return characters.getData();
            }

        }
        return "";
    }

    private Integer getIntElementValue(XMLEventReader xmlEventReader) throws XMLStreamException {
        String value = getStringElementValue(xmlEventReader);
        if (value != null && !value.isEmpty()) {
            return Integer.valueOf(value);
        }
        return null;
    }

    private Long getLongElementValue(XMLEventReader xmlEventReader) throws XMLStreamException {
        String value = getStringElementValue(xmlEventReader);
        if (value != null && !value.isEmpty()) {
            return Long.valueOf(value);
        }
        return null;
    }

    private Double getDoubleElementValue(XMLEventReader xmlEventReader) throws XMLStreamException,Exception {
        String value = getStringElementValue(xmlEventReader);
        if (value != null && !value.isEmpty()) {
            return Double.valueOf(value);
        }  
        return 0.0;
    }

    private Date getDateElementValue(XMLEventReader xmlEventReader) throws XMLStreamException {
        String value = getStringElementValue(xmlEventReader);
        if (value != null && !value.isEmpty()) {
            return DateUtilities.parseDate(value);
        }
        return null;
    }


    private DistributionsPaymentOrderBean parseDistributionsPo(XMLEventReader xmlEventReader) throws XMLStreamException,
                                                                                                     Exception {
        DistributionsPaymentOrderBean distributionsPOBean = new DistributionsPaymentOrderBean();
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                String startElementName = startElement.getName().getLocalPart();
                switch (startElementName) {
                case DistributionsPaymentOrderBean.DISTRIBUTIONS_PROGRAM_NUMBER:
                    ProjectBean projectBean = new ProjectBean();
                    projectBean.setCode(getStringElementValue(xmlEventReader));
                    distributionsPOBean.setProjectBean(projectBean);
                    break;
                case DistributionsPaymentOrderBean.DISTRIBUTIONS_PO_ECONOMIC_CLASSIFICATION:
                    EcomomicClassificationBean ecomomicClassificationBean = new EcomomicClassificationBean();
                    ecomomicClassificationBean.setCode(getStringElementValue(xmlEventReader));
                    distributionsPOBean.setEcomomicClassificationBean(ecomomicClassificationBean);
                    break;
                case DistributionsPaymentOrderBean.DISTRIBUTIONS_PO_REFUND_CATEGORY_CODE:
                    distributionsPOBean.setRefundCategoryCode(getStringElementValue(xmlEventReader));
                    break;
                case DistributionsPaymentOrderBean.DISTRIBUTIONS_PO_AMOUNT:
                    distributionsPOBean.setAmount(getDoubleElementValue(xmlEventReader));
                    break;
                case DistributionsPaymentOrderBean.DISTRIBUTIONS_PO_REFUND_AMOUNT:
                    distributionsPOBean.setRefundAmount(getDoubleElementValue(xmlEventReader));
                    break;
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName()
                              .getLocalPart()
                              .equals(UploadPaymentOrderBean.PAYMENT_ORDER_DISTRIBUTIONS)) {
                    return distributionsPOBean;
                }
            }

        }
        return null;
    }


    public UploadPaymentOrderBean parsePaymentOrder(InputStream InputStream) throws GTechException, Exception {
        List<PaymentOrderBean> paymentOrderBeanList = new ArrayList<PaymentOrderBean>();
        PaymentOrderBean paymentOrderBean = new PaymentOrderBean();
        UploadPaymentOrderBean uploadPaymentOrderBean = new UploadPaymentOrderBean();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(InputStream);
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String startElementName = startElement.getName().getLocalPart();
                    switch (startElementName) {
                    case UploadPaymentOrderBean.PAYMENT_ORDER_NUMBER:
                        paymentOrderBean.setPaymentOrderNumber(getStringElementValue(xmlEventReader));
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_DATE:
                        paymentOrderBean.setPaymentOrderDateH(getStringElementValue(xmlEventReader));
                        if(paymentOrderBean.getPaymentOrderDateH() != null){
                           String date = calendarBusiness.getGrDateByHijriDate(paymentOrderBean.getPaymentOrderDateH());
                           if(date != null && !date.isEmpty()){
                               Date dateG = DateUtilities.parseDate(date);
                              paymentOrderBean.setPaymentOrderYear(generalInfoBusiness.getGeneralInfoYears(dateG));
                            }
                        }
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_YEARS:
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_EXCHANGE_OF:
                        paymentOrderBean.setExchangeOf(getStringElementValue(xmlEventReader));
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_ENTITY:
                        EntityBean entityBean = new EntityBean();
                        entityBean.setCode(getStringElementValue(xmlEventReader));
                        paymentOrderBean.setEntityBean(entityBean);
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_DEPARTMENT:
                        DepartmentBean departmentBean = new DepartmentBean();
                        departmentBean.setCode(getStringElementValue(xmlEventReader));
                        paymentOrderBean.setDepartmentBean(departmentBean);
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_SECTION:
                        SectionBean sectionBean = new SectionBean();
                        sectionBean.setCode(getStringElementValue(xmlEventReader));
                        paymentOrderBean.setSectionBean(sectionBean);
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_BENEFICIARY_NUMBER:
                        BeneficiaryBean beneficiaryBean = new BeneficiaryBean();
                        beneficiaryBean.setCode(getStringElementValue(xmlEventReader));
                        paymentOrderBean.setBeneficiaryBean(beneficiaryBean);
                        break;
                    case UploadPaymentOrderBean.PO_ONE_TIME_SUPPLIER_NAME:
                         paymentOrderBean.setBeneficaryName(getStringElementValue(xmlEventReader));
                         break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_IBAN:
                        paymentOrderBean.setIban(getStringElementValue(xmlEventReader));
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_PAYMENT_TYPE:
                        String paymentMethod = getStringElementValue(xmlEventReader);
                        if(paymentMethod != null){
                            switch (paymentMethod.trim()){
                            case "CHECK" :
                                paymentOrderBean.setPaymentMethod(100101L);
                                 break;
                            case "EFT" :
                                paymentOrderBean.setPaymentMethod(100102L);
                                 break;
                            case "INSTR" :
                                paymentOrderBean.setPaymentMethod(100103L);
                                break;

                            default: paymentOrderBean.setPaymentMethod(0L);
                            }
                        }

                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_CURRENCY:
                        CurrencyBean currencyBean = new CurrencyBean();
                        currencyBean.setCode(getStringElementValue(xmlEventReader));
                        paymentOrderBean.setCurrencyBean(currencyBean);
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_AMOUNT:
                        paymentOrderBean.setAmount(getDoubleElementValue(xmlEventReader));
                        if(paymentOrderBean.getAmount() != null || paymentOrderBean.getAmount() != 0.0){
                           paymentOrderBean.setAmountInWord(new Tafkeet().numDescAR(String.valueOf(paymentOrderBean.getAmount())));
                        }
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_AMOUNT_IN_WORD:

                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_EXPENSE:
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_BENEFICIARY_NATIONALITY:
                        NationalityBean nationalityBean = new NationalityBean();
                        nationalityBean.setNationalityCode(getStringElementValue(xmlEventReader));
                        paymentOrderBean.setNationalityBean(nationalityBean);
                        break;
                    case UploadPaymentOrderBean.PAYMENT_ORDER_DISTRIBUTIONS:
                        if (paymentOrderBean.getDistributionsPoBeanList() == null ||
                            paymentOrderBean.getDistributionsPoBeanList().isEmpty()) {
                            paymentOrderBean.setDistributionsPoBeanList(new ArrayList<DistributionsPaymentOrderBean>());
                        }
                        paymentOrderBean.getDistributionsPoBeanList().add(parseDistributionsPo(xmlEventReader));
                        break;


                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName()
                                  .getLocalPart()
                                  .equals(UploadPaymentOrderBean.PAYMENT_ORDER)) {
                        paymentOrderBeanList.add(paymentOrderBean);
                        paymentOrderBean = new PaymentOrderBean();
                    }
                }

            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        uploadPaymentOrderBean.setPaymentOrderBeanList(paymentOrderBeanList);
        return uploadPaymentOrderBean;
    }

}
