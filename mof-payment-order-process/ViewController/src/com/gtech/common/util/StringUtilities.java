package com.gtech.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class StringUtilities {
	
    private StringUtilities() {
    }

    public static boolean isEmpty(String data) {
        if (data == null || data.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmpty(String data) {
        if (isEmpty(data)) {
            return false;
        }
        return true;
    }
}
