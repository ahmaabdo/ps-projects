/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gtech.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeSet;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 *
 * @author tqasem
 */
public class NetworkUtility {
    
    public String prepareNetworkUserInfo(){
        String networkInfo = null;
        networkInfo =" externalIP  : "+ externalIP() + " \n DNS         : "+ externalDNS() ;
        for(String s : getIpAddressList()){
            networkInfo += " \n "+ s;
        }
        return networkInfo;
    }
    
    public String prepareHTMLNetworkUserInfo(){
        String networkInfo = null;
        networkInfo ="<br> externalIP  : "+ externalIP() + " </br> <br> DNS         : "+ externalDNS() +"</br>";
        for(String s : getIpAddressList()){
            networkInfo += " <br> "+ s +"</br>";
        }
        return networkInfo;
    }
    
    private static String GetMacAddress(InetAddress ip){
       String address = null;
        try {
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            byte[] mac = network.getHardwareAddress();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
            }
            address = sb.toString();
        } catch (SocketException e) {
            e.printStackTrace();
        }
       return address;
   }

    private TreeSet<String> getIpAddressList() {
        TreeSet<String> ipAddrs = new TreeSet<String>();

        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                if (iface.isLoopback() || !iface.isUp())
                    continue;
                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    if(addr.isSiteLocalAddress())
                    ipAddrs.add(addr.getHostAddress()+"  ,HostName : "+addr.getHostName()+"   ,MacAddress : "+GetMacAddress(addr));
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return ipAddrs;
    }

    private String externalIP() {
        String systemipaddress = "";
        try {
            URL url_name = new URL("http://bot.whatismyipaddress.com");
            BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));
            systemipaddress = sc.readLine().trim();
            if (!(systemipaddress.length() > 0)) {
                try {
                    InetAddress localhost = InetAddress.getLocalHost();
                    System.out.println((localhost.getHostAddress()).trim());
                    systemipaddress = (localhost.getHostAddress()).trim();
                } catch (Exception e1) {
                    systemipaddress = "Cannot Execute Properly";
                }
            }
        } catch (Exception e2) {
            systemipaddress = "Cannot Execute Properly";
        }
        return systemipaddress;
    }

    private String externalDNS() {
        String dnsServers = "";
        try {
            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
            DirContext ictx = new InitialDirContext(env);
            dnsServers = (String) ictx.getEnvironment().get("java.naming.provider.url");

        } catch (Exception ex) {
            return "";
        }

        return dnsServers;
    }
    
    public boolean pingToIP(String ip) {
        boolean dnsServers = false;
        try {
            InetAddress inet = InetAddress.getByName(ip);
            dnsServers = inet.isReachable(5000) ;
            
        } catch (Exception ex) {
            return false;
        }
        return dnsServers;
    }
    
    public boolean checkInternetConnection() {
        String systemipaddress = "";
        boolean result = false;
        try {
            URL url_name = new URL("http://www.google.com");
            BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));
            systemipaddress = sc.readLine().trim();
            if(StringUtilities.isNotEmpty(systemipaddress)){
                result = true;
            }
            
        } catch (Exception e2) {
            return result;
        }
        return result;
    }

    private String getYourIp(String defaultAddress) {

        String temp = defaultAddress.substring(0, 11);
        String ipToForward = "";

        TreeSet<String> ipAddrs = getIpAddressList();
        for (Iterator<String> iterator = ipAddrs.iterator(); iterator.hasNext();) {

            String tempIp = iterator.next();
            if (tempIp.contains(temp)) {
                ipToForward = tempIp;
                break;
            }
        }

        return ipToForward;

    }// ipForPortForwarding

    // get the ipaddress list


    // get default gateway address in java
    private String getDefaultGateWayAddress() {
        String defaultAddress = "";
        try {
            Process result = Runtime.getRuntime().exec("netstat -rn");

            BufferedReader output = new BufferedReader(new InputStreamReader(
                    result.getInputStream()));

            String line = output.readLine();
            while (line != null) {
                if (line.contains("0.0.0.0")) {

                    StringTokenizer stringTokenizer = new StringTokenizer(line);
                    stringTokenizer.nextElement();// first string is 0.0.0.0
                    stringTokenizer.nextElement();// second string is 0.0.0.0
                    defaultAddress = (String) stringTokenizer.nextElement(); // this
                    // is
                    // our
                    // default
                    // address
                    break;
                }

                line = output.readLine();

            }// while
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return defaultAddress;

    }// getDefaultAddress

    private String getPublicIpAddress() {
        String res = null;
        try {
            String localhost = InetAddress.getLocalHost().getHostAddress();
            Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface) e.nextElement();
                if (ni.isLoopback()) {
                    continue;
                }
//            if(ni.isPointToPoint())
//                continue;
                Enumeration<InetAddress> addresses = ni.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress address = (InetAddress) addresses.nextElement();
                    if (address instanceof Inet4Address) {
                        String ip = address.getHostAddress();
                        System.out.println(ip);
                        if (!ip.equals(localhost)) {
                            System.out.println((res = ip));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
    
    
    private static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
        System.out.printf("Display name: %s\n", netint.getDisplayName());
        System.out.printf("Name: %s\n", netint.getName());
        List<InterfaceAddress> list = netint.getInterfaceAddresses();
        
        for(InterfaceAddress ia : list){
            System.out.println("list 1 : "+ ia.getAddress().getHostName());
        }
        

        
        Enumeration<NetworkInterface> subAddresses = netint.getSubInterfaces();
        for (NetworkInterface inetAddress : Collections.list(subAddresses)) {
            System.out.printf("InetAddress: %s\n", inetAddress);
        }
        
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
            System.out.printf("InetAddress: %s\n", inetAddress);
        }
        System.out.printf("\n");
        
     }

    
}
