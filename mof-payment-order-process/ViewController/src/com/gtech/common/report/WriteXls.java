package com.gtech.common.report;

import com.gtech.faces.util.LabelReader;
import com.gtech.web.errorUpload.bean.ErrorUploadBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class WriteXls {
    
    public File writeXlsReport(List<PaymentOrderBean> listOfPaymentOrderBean) throws FileNotFoundException, IOException {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("paymentOrder");

            HSSFPalette paletteHeader = workbook.getCustomPalette();
            HSSFCellStyle styleHeader = workbook.createCellStyle();
            styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setFillForegroundColor(paletteHeader.findSimilarColor(117, 113, 113).getIndex());

            sheet.setColumnWidth(0, 2500);
            sheet.setColumnWidth(1, 9500);
            sheet.setColumnWidth(2, 9000);
            sheet.setColumnWidth(3, 5000);
            sheet.setColumnWidth(4, 5000);
        

            HSSFRow row4 = sheet.createRow(1);
           HSSFCell cell014 = row4.createCell(1);
           cell014.setCellValue(LabelReader.getLabel("PAYMENT_ORDER_REPORT_DESC"));

            HSSFRow row = sheet.createRow(3);
            
            HSSFCell cell1 = row.createCell(0);
            cell1.setCellValue(LabelReader.getLabel("NUMBER"));
            cell1.setCellStyle(styleHeader);

            HSSFCell cell2 = row.createCell(1);
            cell2.setCellValue(LabelReader.getLabel("PAYMENT_NUMBER"));
            cell2.setCellStyle(styleHeader);

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(LabelReader.getLabel("ERROR_TYPE"));
            cell3.setCellStyle(styleHeader);

            int rowCount = 3;
            for (int rowNo = 0; rowNo < listOfPaymentOrderBean.size(); rowNo++) {
                rowCount++;
                HSSFRow row1 = sheet.createRow(rowCount);
                for (int colNo = 0; colNo < 3; colNo++) {
                    HSSFCell cell = row1.createCell(colNo);
                    PaymentOrderBean paymentOrderBean = listOfPaymentOrderBean.get(rowNo);
                
                    switch (colNo) {
                        case 0:
                            cell.setCellValue(rowNo + 1);
                            break;
                        case 1:
                            cell.setCellValue(paymentOrderBean.getPaymentOrderNumber());
                            break;
                        case 2:
                            cell.getCellStyle().setWrapText(true);
                            cell.setCellValue(paymentOrderBean.getRemark());
                            break;
                    }
                }
            }
            
            File temp = null;
            temp = File.createTempFile("PaymentOrderError", ".xls");
            temp.deleteOnExit();

            FileOutputStream fileOut = new FileOutputStream(temp);

            //write this workbook to an Outputstream.
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        
            return temp;
        }
   
    public File writeXlsReport1(List<ErrorUploadBean> listOfPaymentOrderBean) throws FileNotFoundException, IOException {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("paymentOrder");

            HSSFPalette paletteHeader = workbook.getCustomPalette();
            HSSFCellStyle styleHeader = workbook.createCellStyle();
            styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setFillForegroundColor(paletteHeader.findSimilarColor(117, 113, 113).getIndex());

            sheet.setColumnWidth(0, 2500);
            sheet.setColumnWidth(1, 9500);
            sheet.setColumnWidth(2, 9000);
            sheet.setColumnWidth(3, 5000);
            sheet.setColumnWidth(4, 5000);
        

            HSSFRow row4 = sheet.createRow(1);
           HSSFCell cell014 = row4.createCell(1);
           cell014.setCellValue(LabelReader.getLabel("PAYMENT_ORDER_REPORT_DESC"));

            HSSFRow row = sheet.createRow(3);
            
            HSSFCell cell1 = row.createCell(0);
            cell1.setCellValue(LabelReader.getLabel("NUMBER"));
            cell1.setCellStyle(styleHeader);

            HSSFCell cell2 = row.createCell(1);
            cell2.setCellValue(LabelReader.getLabel("PAYMENT_NUMBER"));
            cell2.setCellStyle(styleHeader);

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(LabelReader.getLabel("ERROR_TYPE"));
            cell3.setCellStyle(styleHeader);

            int rowCount = 3;
            for (int rowNo = 0; rowNo < listOfPaymentOrderBean.size(); rowNo++) {
                rowCount++;
                HSSFRow row1 = sheet.createRow(rowCount);
                for (int colNo = 0; colNo < 3; colNo++) {
                    HSSFCell cell = row1.createCell(colNo);
                    ErrorUploadBean paymentOrderBean = listOfPaymentOrderBean.get(rowNo);
                
                    switch (colNo) {
                        case 0:
                            cell.setCellValue(rowNo + 1);
                            break;
                        case 1:
                        
                            cell.setCellValue(paymentOrderBean.getCode());
                            break;
                        case 2:
                            cell.getCellStyle().setWrapText(true);
                            cell.setCellValue(paymentOrderBean.getError());
                            break;
                    }
                }
            }
            
            File temp = null;
            temp = File.createTempFile("PaymentOrderError", ".xls");
            temp.deleteOnExit();

            FileOutputStream fileOut = new FileOutputStream(temp);

            //write this workbook to an Outputstream.
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        
            return temp;
        } 
    public File writeXlsRegReport(List<PaymentOrderBean> listOfPaymentOrderBean) throws FileNotFoundException, IOException {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("registeredPaymentOrder");

            HSSFPalette paletteHeader = workbook.getCustomPalette();
            HSSFCellStyle styleHeader = workbook.createCellStyle();
            styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setFillForegroundColor(paletteHeader.findSimilarColor(117, 113, 113).getIndex());
            styleHeader.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            sheet.setColumnWidth(0, 2500);
            sheet.setColumnWidth(1, 3000);
            sheet.setColumnWidth(2, 10000);
            sheet.setColumnWidth(3, 5000);
            sheet.setColumnWidth(4, 5000);
        

            HSSFRow row4 = sheet.createRow(1);
            HSSFCell cell014 = row4.createCell(1);
            cell014.setCellValue(LabelReader.getLabel("PAYMENT_ORDER_LBL6"));
            cell014.setCellStyle(styleHeader);
            
            HSSFRow row = sheet.createRow(3);
            
            HSSFCell cell1 = row.createCell(0);
            cell1.setCellValue(LabelReader.getLabel("REGISTERED_EXCEL_SEQ"));
            cell1.setCellStyle(styleHeader);

            HSSFCell cell2 = row.createCell(1);
            cell2.setCellValue(LabelReader.getLabel("REVIEW_PAYMENT_ORDER"));
            cell2.setCellStyle(styleHeader);

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(LabelReader.getLabel("REVIEW_BENEFICIARY_NAME"));
            cell3.setCellStyle(styleHeader);
            
            HSSFCell cell4 = row.createCell(3);
            cell4.setCellValue(LabelReader.getLabel("ENTITY_DESC_LABEL"));
            cell4.setCellStyle(styleHeader);
            
            int rowCount = 3;
            System.out.println(listOfPaymentOrderBean.size());
            for (int rowNo = 0; rowNo < listOfPaymentOrderBean.size(); rowNo++) {
                rowCount++;
                HSSFRow row1 = sheet.createRow(rowCount);
                for (int colNo = 0; colNo < 4; colNo++) {
                    HSSFCell cell = row1.createCell(colNo);
                    PaymentOrderBean paymentOrderBean = listOfPaymentOrderBean.get(rowNo);
                    switch (colNo) {
                        case 0:
                            cell.setCellValue(rowNo + 1);
                            break;
                        case 1:
                            cell.setCellValue(paymentOrderBean.getPaymentOrderNumber());
                            break;
                        case 2:
                            cell.setCellValue(paymentOrderBean.getBeneficiaryBean().getNameAr());
                            break;
                        case 3:
                            cell.setCellValue(paymentOrderBean.getEntityBean().getNameAr());
                            break;
                    }
                }
            }
            
            File temp = null;
            temp = File.createTempFile("RegisteredPaymentOrder", ".xls");
            temp.deleteOnExit();

            FileOutputStream fileOut = new FileOutputStream(temp);

            //write this workbook to an Outputstream.
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        
            return temp;
        }
    
    public File writeXlsAuditReport(List<PaymentOrderBean> listOfPaymentOrderBean) throws FileNotFoundException, IOException {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("auditPaymentOrder");

            HSSFPalette paletteHeader = workbook.getCustomPalette();
            HSSFCellStyle styleHeader = workbook.createCellStyle();
            styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setFillForegroundColor(paletteHeader.findSimilarColor(117, 113, 113).getIndex());
            styleHeader.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            sheet.setColumnWidth(0, 2500);
            sheet.setColumnWidth(1, 10000);
            sheet.setColumnWidth(2, 3000);
            sheet.setColumnWidth(3, 5000);
            sheet.setColumnWidth(4, 5000);
        

            HSSFRow row4 = sheet.createRow(1);
            HSSFCell cell014 = row4.createCell(1);
            cell014.setCellValue(LabelReader.getLabel("PAYMENT_ORDER_LBL21"));
            cell014.setCellStyle(styleHeader);
            
            HSSFRow row = sheet.createRow(3);
            
            HSSFCell cell1 = row.createCell(0);
            cell1.setCellValue(LabelReader.getLabel("REVIEW_PAYMENT_ORDER"));
            cell1.setCellStyle(styleHeader);

            HSSFCell cell2 = row.createCell(1);
            cell2.setCellValue(LabelReader.getLabel("REVIEW_BENEFICIARY_NAME"));
            cell2.setCellStyle(styleHeader);

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(LabelReader.getLabel("CONFIRM_DATE"));
            cell3.setCellStyle(styleHeader);
            
            HSSFCell cell4 = row.createCell(3);
            cell4.setCellValue(LabelReader.getLabel("AMOUNT_LABEL"));
            cell4.setCellStyle(styleHeader);

            
            int rowCount = 3;
            System.out.println(listOfPaymentOrderBean.size());
            for (int rowNo = 0; rowNo < listOfPaymentOrderBean.size(); rowNo++) {
                rowCount++;
                HSSFRow row1 = sheet.createRow(rowCount);
                for (int colNo = 0; colNo < 4; colNo++) {
                    HSSFCell cell = row1.createCell(colNo);
                    PaymentOrderBean paymentOrderBean = listOfPaymentOrderBean.get(rowNo);
                    switch (colNo) {
                        case 0:
                            cell.setCellValue(paymentOrderBean.getPaymentOrderNumber());
                            break;
                        case 1:
                            cell.setCellValue(paymentOrderBean.getBeneficiaryBean().getNameAr());
                            break;
                        case 2:
                            cell.setCellValue(paymentOrderBean.getUpdateDate());
                            break;
                        case 3:
                            cell.setCellValue(paymentOrderBean.getAmount());
                            break;

                    }
                }
            }
            
            File temp = null;
            temp = File.createTempFile("AuditPaymentOrder", ".xls");
            temp.deleteOnExit();

            FileOutputStream fileOut = new FileOutputStream(temp);

            //write this workbook to an Outputstream.
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        
            return temp;
        }
//    private void addMarge(Sheet sheet, Integer firstRow, Integer lastRow, Integer firstCol, Integer lastCol) {
//             sheet.addMergedRegion(new CellRangeAddress(
//                     firstRow, //first row (0-based)
//                     lastRow, //last row  (0-based)
//                     firstCol, //first column (0-based)
//                     lastCol //last column  (0-based)
//                     ));
//         }
}
