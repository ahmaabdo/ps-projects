package com.gtech.common.reader;
import java.util.ResourceBundle;

public class SystemConfigReader {

    private static ResourceBundle bundle = null;
    
    public static String getValue(String key) {
        if(bundle == null) {
           bundle =  BundelReader.loadResourceBundle("system-config","en");
        }
        return bundle.getString(key);
    }
}