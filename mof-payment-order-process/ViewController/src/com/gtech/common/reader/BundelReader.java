package com.gtech.common.reader;

import java.util.Locale;
import java.util.ResourceBundle;

import com.gtech.faces.beans.SessionLocaleBean;


  public class BundelReader {
    private BundelReader() {
    }
    
    
    public static ResourceBundle loadResourceBundle(String bundleName,String localeName) {
        Locale locale = new Locale(localeName, "");
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
        return bundle;
    }
    
    public static ResourceBundle loadResourceBundle(String bundleName,SessionLocaleBean sessionLocale)
    {
        Locale locale = new Locale(sessionLocale.getUserSessionLocale(),"");
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
        return bundle;
    }
    
  }    