package com.gtech.common.reader;
import java.util.ResourceBundle;

public class CommonConfigReader {  

    private static ResourceBundle bundle = null;
    
    public static String getValue(String key) {
        if(bundle == null) {
           bundle =  BundelReader.loadResourceBundle("common-config","en");
        }
        return bundle.getString(key);
    }
}