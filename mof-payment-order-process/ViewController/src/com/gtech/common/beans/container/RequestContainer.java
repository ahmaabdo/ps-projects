package com.gtech.common.beans.container;


import com.gtech.common.base.BaseBean;

import java.io.Serializable;
import java.util.List;


public class RequestContainer<T extends BaseBean> implements Serializable {
    
    private T dataBean;
    private List dataBeanList;
    private String dataItem;
    
    public RequestContainer() {
    }

    public void setDataBean(T dataBean) {
        this.dataBean = dataBean;
    }

    public T getDataBean() {
        return dataBean;
    }


    public void setDataBeanList(List dataBeanList) {
        this.dataBeanList = dataBeanList;
    }

    public List getDataBeanList() {
        return dataBeanList;
    }

    public void setDataItem(String dataItem) {
        this.dataItem = dataItem;
    }

    public String getDataItem() {
        return dataItem;
    }

}

