package com.gtech.common.beans.container;


import com.gtech.common.base.BaseBean;
import com.gtech.faces.beans.FacesContextMessageBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ResponseContainer<T extends BaseBean> implements Serializable {
    
    private T dataBean;
    private List<T> dataList;
    private String createdRecordId;
    
    private List infoMessagesList;
    private List errorMessagesList;
    private List warningMessagesList;
        
    public void setDataBean(T dataBean) {
        this.dataBean = dataBean;
    }

    public T getDataBean() {
        return dataBean;
    }

    public void addWarningMessage(String messageKey,String...messageArguments) {
        FacesContextMessageBean messageBean = new FacesContextMessageBean();
        messageBean.setMessageKey(messageKey);
        messageBean.setMessageAttributes(messageArguments);
        messageBean.setMessageType(FacesContextMessageBean.MessageType.WARNING);
        if(warningMessagesList == null) {
            warningMessagesList = new ArrayList();
        }
        warningMessagesList.add(messageBean);
    }
    
    public void addWarningMessage(FacesContextMessageBean messageBean) {
        if(warningMessagesList == null) {
            warningMessagesList = new ArrayList();
        }
        warningMessagesList.add(messageBean);
    }
    
    public void addInfoMessage(String messageKey,String...messageArguments) {
        FacesContextMessageBean messageBean = new FacesContextMessageBean();
        messageBean.setMessageKey(messageKey);
        messageBean.setMessageAttributes(messageArguments);
        messageBean.setMessageType(FacesContextMessageBean.MessageType.INFO);
        if(infoMessagesList == null) {
            infoMessagesList = new ArrayList();
        }
        infoMessagesList.add(messageBean);
    }
    
    public void addInfoMessage(FacesContextMessageBean messageBean) {
        if(infoMessagesList == null) {
            infoMessagesList = new ArrayList();
        }
        infoMessagesList.add(messageBean);
    }
    
    public void addErrorMessage(String messageKey,String...messageArguments) {
        FacesContextMessageBean messageBean = new FacesContextMessageBean();
        messageBean.setMessageKey(messageKey);
        messageBean.setMessageAttributes(messageArguments);
        messageBean.setMessageType(FacesContextMessageBean.MessageType.ERROR);
        if(errorMessagesList == null) {
            errorMessagesList = new ArrayList();
        }
        errorMessagesList.add(messageBean);
    }
    
    public void addErrorMessage(FacesContextMessageBean messageBean) {
        if(errorMessagesList == null) {
            errorMessagesList = new ArrayList();
        }
        errorMessagesList.add(messageBean);
    }
    
    public boolean containsErrorMessages() {
        if(errorMessagesList == null || errorMessagesList.size() == 0) {
            return false;
        }
        return true;
    }
    
    public boolean containsWarningMessages() {
        if(warningMessagesList == null || warningMessagesList.size() == 0) {
            return false;
        }
        return true;
    }
    
    public boolean containsInfoMessages() {
        if(infoMessagesList == null || infoMessagesList.size() == 0) {
            return false;
        }
        return true;
    }

    public void setInfoMessagesList(List infoMessagesList) {
        this.infoMessagesList = infoMessagesList;
    }

    public List getInfoMessagesList() {
        return infoMessagesList;
    }

    public void setErrorMessagesList(List errorMessagesList) {
        this.errorMessagesList = errorMessagesList;
    }

    public List getErrorMessagesList() {
        return errorMessagesList;
    }

    public void setCreatedRecordId(String createdRecordId) {
        this.createdRecordId = createdRecordId;
    }

    public String getCreatedRecordId() {
        return createdRecordId;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public List getWarningMessagesList() {
        return warningMessagesList;
    }
}
