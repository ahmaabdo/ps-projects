package com.gtech.common.beans.container;

import com.gtech.faces.util.SessionUtilities;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.dss.dataView.ViewController;

public class ReportContainer implements Serializable{

    private Map parametersMap = new HashMap();
    private String targetReport;
    private List reportDataList;

    public ReportContainer() {
        String imagePath = SessionUtilities.getHttpSession(true).
                getServletContext().getRealPath("/Resources/images/logo-en.png");
       
        
        addParameter("IMAGE_PATH", imagePath);
    }
        

    public void addParameter(String key, Object value) {
        parametersMap.put(key, value);
    }
    
    public void addAllParameters(Map<String,String> params) {
        parametersMap.putAll(params);
    }

    public Map getParametersMap() {
        return parametersMap;
    }

    public void setTargetReport(String targetReport) {
        this.targetReport = targetReport;
    }

    public String getTargetReport() {
        return targetReport;
    }

    public void setReportDataList(List reportDataList) {
        this.reportDataList = reportDataList;
    }

    public List getReportDataList() {
        return reportDataList;
    }
}
