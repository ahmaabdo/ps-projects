package com.gtech.common.tafkeet;

import com.gtech.faces.util.LabelReader;


/**
 *
 * @author tqasem
 */
public class Tafkeet {

    public String numDescAR(String amt) {
        String ones[] = new String[20];
        String tens[] = new String[10];
        String hundred[] = new String[10];
        String amountS = "000000000";

        long decimalVal = 0;
        int i = 0;
        int j = 0;
        int tmp = 0;
        int dig1a = 0;
        int dig2a = 0;
        int dig3a = 0;
        int dig4a = 0;
        int dig5a = 0;
        int dig6a = 0;
        int dig7a = 0;
        int dig8a = 0;
        int dig9a = 0;
        int dig10a = 0;
        int dig11a = 0;
        int dig12a = 0;
        int dig1 = 0;
        int dig2 = 0;
        int dig3 = 0;
        int million = 0;
        int billion = 0;
        int thousand = 0;
        int one = 0;
        String amount = "";
        int tmpp = 0;
        String tmp_desc = "";
        String tmpc = "";
        int mrest = 0;
        String mdesc = "";

        ones[0] = " ";
        ones[1] = " "+LabelReader.getLabel("TAFKEET_ONES_1");
        ones[2] = " "+LabelReader.getLabel("TAFKEET_ONES_2");
        ones[3] = " "+LabelReader.getLabel("TAFKEET_ONES_3");
        ones[4] = " "+LabelReader.getLabel("TAFKEET_ONES_4");
        ones[5] = " "+LabelReader.getLabel("TAFKEET_ONES_5");
        ones[6] = " "+LabelReader.getLabel("TAFKEET_ONES_6");
        ones[7] = " "+LabelReader.getLabel("TAFKEET_ONES_7");
        ones[8] = " "+LabelReader.getLabel("TAFKEET_ONES_8");
        ones[9] = " "+LabelReader.getLabel("TAFKEET_ONES_9");
        ones[10] = " "+LabelReader.getLabel("TAFKEET_ONES_10");
        ones[11] = " "+LabelReader.getLabel("TAFKEET_ONES_11");
        ones[12] = " "+LabelReader.getLabel("TAFKEET_ONES_12");
        ones[13] = " "+LabelReader.getLabel("TAFKEET_ONES_13");
        ones[14] = " "+LabelReader.getLabel("TAFKEET_ONES_14");
        ones[15] = " "+LabelReader.getLabel("TAFKEET_ONES_15");
        ones[16] = " "+LabelReader.getLabel("TAFKEET_ONES_16");
        ones[17] = " "+LabelReader.getLabel("TAFKEET_ONES_17");
        ones[18] = " "+LabelReader.getLabel("TAFKEET_ONES_18");
        ones[19] = " "+LabelReader.getLabel("TAFKEET_ONES_19");

        tens[0] = " ";
        tens[1] = " "+LabelReader.getLabel("TAFKEET_TENS_1");
        tens[2] = " "+LabelReader.getLabel("TAFKEET_TENS_2");
        tens[3] = " "+LabelReader.getLabel("TAFKEET_TENS_3");
        tens[4] = " "+LabelReader.getLabel("TAFKEET_TENS_4");
        tens[5] = " "+LabelReader.getLabel("TAFKEET_TENS_5");
        tens[6] = " "+LabelReader.getLabel("TAFKEET_TENS_6");
        tens[7] = " "+LabelReader.getLabel("TAFKEET_TENS_7");
        tens[8] = " "+LabelReader.getLabel("TAFKEET_TENS_8");
        tens[9] = " "+LabelReader.getLabel("TAFKEET_TENS_9");

        hundred[0] = " ";
        hundred[1] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_1");
        hundred[2] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_2");
        hundred[3] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_3");
        hundred[4] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_4");
        hundred[5] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_5");
        hundred[6] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_6");
        hundred[7] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_7");
        hundred[8] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_8");
        hundred[9] = " "+LabelReader.getLabel("TAFKEET_HUNDRED_9");

        amount = "";
        Long longValue = Double.valueOf(amt).longValue();
        amount = longValue.toString();
        amount = amount.replace('-', '0');
        j = amount.indexOf('.');
        if (j != -1)
            amount = amount.substring(0, j);

        decimalVal = Long.parseLong(amount);
        amountS = "000000000000";
        amountS = amountS + amount;
        amount = amountS.substring(amountS.length() - 12, amountS.length());

        dig1a = Integer.parseInt(amount.substring(0, 1));
        dig2a = Integer.parseInt(amount.substring(1, 2));
        dig3a = Integer.parseInt(amount.substring(2, 3));
        dig4a = Integer.parseInt(amount.substring(3, 4));
        dig5a = Integer.parseInt(amount.substring(4, 5));
        dig6a = Integer.parseInt(amount.substring(5, 6));
        dig7a = Integer.parseInt(amount.substring(6, 7));
        dig8a = Integer.parseInt(amount.substring(7, 8));
        dig9a = Integer.parseInt(amount.substring(8, 9));
        dig10a = Integer.parseInt(amount.substring(9, 10));
        dig11a = Integer.parseInt(amount.substring(10, 11));
        dig12a = Integer.parseInt(amount.substring(11, 12));
        billion = (dig1a * 100) + (dig2a * 10) + dig3a;
        million = (dig4a * 100) + (dig5a * 10) + dig6a;
        thousand = (dig7a * 100) + (dig8a * 10) + dig9a;
        one = (dig10a * 100) + (dig11a * 10) + dig12a;
        mdesc = " ";
        
        
        i = 1;
        while (i < 5) {
            tmp_desc = " ";
            
            tmp_desc = " ";
            if (i == 1) {
                tmpp = billion;
                tmpc = " "+LabelReader.getLabel("TAFKEET_BILLION");
                dig1 = dig1a;
                dig2 = dig2a;
                dig3 = dig3a;
            } else if (i == 2) {
                tmpp = million;
                tmpc = " "+LabelReader.getLabel("TAFKEET_MILEON");
                dig1 = dig4a;
                dig2 = dig5a;
                dig3 = dig6a;
            } else if (i == 3) {
                tmpp = thousand;
                tmpc = " "+LabelReader.getLabel("TAFKEET_THOUSAND");
                dig1 = dig7a;
                dig2 = dig8a;
                dig3 = dig9a;
            } else if (i == 4) {
                tmpp = one;
                tmpc = "  ";
                dig1 = dig10a;
                dig2 = dig11a;
                dig3 = dig12a;
            }

            if (tmpp == 1) {
                tmp_desc = tmpc;
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (thousand > 0 || one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 2) && (one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                }
                i += 1;
                continue;
            }
            if (tmpp == 2) {
                switch (i) {
                case 1:
                    tmp_desc = " "+LabelReader.getLabel("TAFKEET_2_BILLION");
                    break;
                case 2:
                    tmp_desc = " "+LabelReader.getLabel("TAFKEET_2_MILEON");
                    break;
                case 3:
                    tmp_desc = " "+LabelReader.getLabel("TAFKEET_2_THOUSAND");
                    break;
                case 4:
                    tmp_desc = " "+LabelReader.getLabel("TAFKEET_2_CURRENCY_MAIN_NAME_SA");
                    break;
                default:
                    break;
                }
                {
                    mdesc = mdesc + tmp_desc;
                    if ((i == 1) && (thousand > 0 || one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 2) && (one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                }
                i += 1;
                continue;
            }

            if (tmpp > 99) {
                tmp_desc = hundred[dig1];
                if ((dig2 > 0) || (dig3 > 0))
                    tmp_desc = tmp_desc.trim() + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                else {
                    tmp_desc = tmp_desc.trim() + ' ' + tmpc.trim() + " ";
                    {
                        mdesc = mdesc + tmp_desc;
                        if ((i == 1) && (thousand > 0 || one > 0))
                            mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                        else if ((i == 2) && (one > 0))
                            mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    }
                    i += 1;
                    continue;
                }
            }

            mrest = (dig2 * 10) + dig3;
            if ((mrest >= 1) && (mrest <= 19)) {
                tmp_desc = tmp_desc.trim() + " " + ones[mrest].trim();
                if ((i == 2) && (mrest <= 10)) {
                    tmp_desc = tmp_desc.trim() + " " + LabelReader.getLabel("TAFKEET_3_MILEON");
                } else if ((i == 3) && (mrest <= 10)) {
                    tmp_desc = tmp_desc.trim() + " " + LabelReader.getLabel("TAFKEET_3_THOUSAND");
                } else {
                    tmp_desc = tmp_desc.trim() + ' ' + tmpc.trim() + ' ';
                }
                {
                    mdesc = mdesc + tmp_desc;
                    if ((i == 1) && (million > 0 || thousand > 0 || one > 0))
                        mdesc = mdesc + " " + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 2) && (thousand > 0 || one > 0))
                        mdesc = mdesc + " " + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 3) && (one > 0))
                        mdesc = mdesc + " " + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                }
                i += 1;
                continue;
            }
            if ((mrest > 19) && (dig3 == 0)) {
                tmp_desc = tmp_desc + tens[dig2] + " " + tmpc + " ";
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (million > 0 || thousand > 0 || one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 2) && (thousand > 0 || one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 3) && (one > 0))
                        mdesc = mdesc + " " + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                }
                i += 1;
                continue;
            }

            if ((mrest > 19) && (dig3 > 0)) {
                tmp_desc = tmp_desc + ones[dig3] + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR") + tens[dig2] + " " + tmpc + " ";
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (million > 0 || thousand > 0 || one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 2) && (thousand > 0 || one > 0))
                        mdesc = mdesc + " "+LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                    else if ((i == 3) && (one > 0))
                        mdesc = mdesc + " " + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR");
                }
            }
            i += 1;
        }
        
        
        if (decimalVal > 0) {
            mdesc = mdesc + ' ' ;
            Long longValue6 = Double.valueOf(amt).longValue();
            amount = longValue6.toString();
            amount = amount.replace('-', '0');
            j = amount.indexOf('.');
            if (j != -1) {
                amount = amount.substring(j + 1, amount.length());
                tmp = Integer.parseInt(amount);
                if (tmp > 0)
                    mdesc = mdesc + ' ' + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR") + "( " + amount + "  )" ;
//                    mdesc = mdesc + ' ' + LabelReader.getLabel("TAFKEET_CURRENCY_CONCATINATION_LITTAR") + "( " + amount + "  )" + LabelReader.getLabel("TAFKEET_CURRENCY_SUB_NAME_SA");
            }
        } else {
            Long longValue1 = Double.valueOf(amt).longValue();
            amount = longValue1.toString();
            amount = amount.replace('-', '0');
            j = amount.indexOf('.');
            if (j != -1) {
                amount = amount.substring(j + 1, amount.length());
                tmp = Integer.parseInt(amount);
                if (tmp > 0)
                    mdesc = "( " + amount + "  )" ;
            }

        }
//        mdesc = mdesc + " لا";
//        mdesc = mdesc + "غير";
        return (mdesc);
    }


    public String numDescEN(String amt) {
        String ones[] = new String[20];
        String tens[] = new String[10];
        String hundred[] = new String[10];
        String amountS = "000000000000";

        long decimalVal = 0;
        int i = 0;
        int j = 0;
        int tmp = 0;
        int dig1a = 0;
        int dig2a = 0;
        int dig3a = 0;
        int dig4a = 0;
        int dig5a = 0;
        int dig6a = 0;
        int dig7a = 0;
        int dig8a = 0;
        int dig9a = 0;
        int dig10a = 0;
        int dig11a = 0;
        int dig12a = 0;
        int dig1 = 0;
        int dig2 = 0;
        int dig3 = 0;
        int million = 0;
        int billion = 0;
        int thousand = 0;
        int one = 0;
        String amount = "";
        int tmpp = 0;
        String tmp_desc = "";
        String tmpc = "";
        int mrest = 0;
        String mdesc = "";

        ones[0] = " ";
        ones[1] = " One";
        ones[2] = " Two";
        ones[3] = " Three";
        ones[4] = " Four";
        ones[5] = " Five";
        ones[6] = " Six";
        ones[7] = " Seven";
        ones[8] = " Eight";
        ones[9] = " Nine";
        ones[10] = " Ten";
        ones[11] = " Eleven";
        ones[12] = " Twelve";
        ones[13] = " Thirteen";
        ones[14] = " Fourteen";
        ones[15] = " Fifteen";
        ones[16] = " Sixteen";
        ones[17] = " Seventeen";
        ones[18] = " Eighteen";
        ones[19] = " Nineteen";

        tens[0] = " ";
        tens[1] = " Ten";
        tens[2] = " Twenty";
        tens[3] = " Thirty";
        tens[4] = " Forty";
        tens[5] = " Fifty";
        tens[6] = " Sixty";
        tens[7] = " Seventy";
        tens[8] = " Eighty";
        tens[9] = " Ninety";

        hundred[0] = " ";
        hundred[1] = " One Hundred";
        hundred[2] = " Two Hundred";
        hundred[3] = " Three Hundred";
        hundred[4] = " Four Hundred";
        hundred[5] = " Five Hundred";
        hundred[6] = " Six Hundred";
        hundred[7] = " Seven Hundred";
        hundred[8] = " Eight Hundred";
        hundred[9] = " Nine Hundred";

//        Long longValue = Double.valueOf(amt).longValue();
        amount = amt;
        amount = amount.replace('-', '0');
        j = amount.indexOf('.');
        if (j != -1)
            amount = amount.substring(0, j);


        decimalVal = Long.parseLong(amount);
        amountS = "000000000000";
        amountS = amountS + amount;
        amount = amountS.substring(amountS.length() - 12, amountS.length());

        dig1a = Integer.parseInt(amount.substring(0, 1));
        dig2a = Integer.parseInt(amount.substring(1, 2));
        dig3a = Integer.parseInt(amount.substring(2, 3));
        dig4a = Integer.parseInt(amount.substring(3, 4));
        dig5a = Integer.parseInt(amount.substring(4, 5));
        dig6a = Integer.parseInt(amount.substring(5, 6));
        dig7a = Integer.parseInt(amount.substring(6, 7));
        dig8a = Integer.parseInt(amount.substring(7, 8));
        dig9a = Integer.parseInt(amount.substring(8, 9));
        dig10a = Integer.parseInt(amount.substring(9, 10));
        dig11a = Integer.parseInt(amount.substring(10, 11));
        dig12a = Integer.parseInt(amount.substring(11, 12));
        billion = (dig1a * 100) + (dig2a * 10) + dig3a;
        million = (dig4a * 100) + (dig5a * 10) + dig6a;
        thousand = (dig7a * 100) + (dig8a * 10) + dig9a;
        one = (dig10a * 100) + (dig11a * 10) + dig12a;
        mdesc = " ";
        i = 1;
        while (i < 5) {
            tmp_desc = " ";
            if (i == 1) {
                tmpp = billion;
                tmpc = "billion ";
                dig1 = dig1a;
                dig2 = dig2a;
                dig3 = dig3a;
            } else if (i == 2) {
                tmpp = million;
                tmpc = "Million ";
                dig1 = dig4a;
                dig2 = dig5a;
                dig3 = dig6a;
            } else if (i == 3) {
                tmpp = thousand;
                tmpc = "Thousand ";
                dig1 = dig7a;
                dig2 = dig8a;
                dig3 = dig9a;
            } else if (i == 4) {
                tmpp = one;
                tmpc = "  ";
                dig1 = dig10a;
                dig2 = dig11a;
                dig3 = dig12a;
            }

            if (tmpp == 1) {
                //tmp_desc = tmpc;
                switch (i) {
                case 1:
                    tmp_desc = "One billion";
                    break;
                case 2:
                    tmp_desc = "One Million";
                    break;
                case 3:
                    tmp_desc = "One Thousand";
                    break;
                case 4:
                    tmp_desc = "One ";
                    break;
                default:
                    break;
                }
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (thousand > 0 || one > 0))
                        //mdesc = mdesc + " And " ;
                        mdesc = mdesc + " ";
                    else if ((i == 2) && (one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                }
                i += 1;
                continue;
            }
            if (tmpp == 2) {
                switch (i) {
                    case 1:
                        tmp_desc = "Two billion";
                        break;
                case 2:
                    tmp_desc = "Two Million";
                    break;
                case 3:
                    tmp_desc = "Two Thousand";
                    break;
                case 4:
                    tmp_desc = "Two ";
                    break;
                default:
                    break;
                }
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (thousand > 0 || one > 0))
                        //mdesc = mdesc + " And " ;
                        mdesc = mdesc + " ";
                    else if ((i == 2) && (one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                }
                i += 1;
                continue;
            }

            if (tmpp > 99) {
                tmp_desc = hundred[dig1];
                if ((dig2 > 0) || (dig3 > 0))
                    //tmp_desc = tmp_desc.trim() + " And ";
                    tmp_desc = tmp_desc.trim() + " ";
                else {
                    tmp_desc = tmp_desc.trim() + ' ' + tmpc.trim() + " ";
                    {
                        mdesc = mdesc + tmp_desc.trim();
                        if ((i == 1) && (thousand > 0 || one > 0))
                            //mdesc = mdesc + " And ";
                            mdesc = mdesc + " ";
                        else if ((i == 2) && (one > 0))
                            //mdesc = mdesc + " And ";
                            mdesc = mdesc + " ";
                    }
                    i += 1;
                    continue;
                }
            }

            mrest = (dig2 * 10) + dig3;
            if ((mrest >= 1) && (mrest <= 19)) {
                tmp_desc = tmp_desc.trim() + " " + ones[mrest].trim();
                if ((i == 2) && (mrest <= 10)) {
                    tmp_desc = tmp_desc.trim() + " Thousands ";
                } else {
                    tmp_desc = tmp_desc.trim() + ' ' + tmpc.trim() + ' ';
                }
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (thousand > 0 || one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                    else if ((i == 2) && (one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                }
                i += 1;
                continue;
            }
            if ((mrest > 19) && (dig3 == 0)) {
                tmp_desc = tmp_desc.trim() + tens[dig2].trim() + " " + tmpc.trim() + " ";
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (thousand > 0 || one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                    else if ((i == 2) && (one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                }
                i += 1;
                continue;
            }

            if ((mrest > 19) && (dig3 > 0)) {
                tmp_desc =
                    tmp_desc.trim() + " " + tens[dig2].trim() + " " + ones[dig3].trim() + " " + tmpc.trim() + " ";
                {
                    mdesc = mdesc + tmp_desc.trim();
                    if ((i == 1) && (thousand > 0 || one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                    else if ((i == 2) && (one > 0))
                        //mdesc = mdesc + " And ";
                        mdesc = mdesc + " ";
                }
            }
            i += 1;
        }

        if (decimalVal > 0) {
            //        mdesc = mdesc + ' ' + "JD";
            Long longValue2 = Double.valueOf(amt).longValue();
            amount = longValue2.toString();
            amount = amount.replace('-', '0');
            j = amount.indexOf('.');
            if (j != -1) {
                amount = amount.substring(j + 1, amount.length());
                tmp = Integer.parseInt(amount);
                if (tmp > 0)
                    mdesc = mdesc + ' ' + "And (" + amount + "0/100) ";
            }
        } else {
            Long longValue5 = Double.valueOf(amt).longValue();
            amount = longValue5.toString();
            amount = amount.replace('-', '0');
            j = amount.indexOf('.');
            if (j != -1) {
                amount = amount.substring(j + 1, amount.length());
                tmp = Integer.parseInt(amount);
                if (tmp > 0)
                    mdesc = "(" + amount + "0/100)";
            }
        }

        //        mdesc = mdesc + " Only";
        return (mdesc);
    }

    public static void main(String[] args) {

        Tafkeet t = new Tafkeet();
        System.out.println(t.numDescEN("987999999999.25"));
    }

}
