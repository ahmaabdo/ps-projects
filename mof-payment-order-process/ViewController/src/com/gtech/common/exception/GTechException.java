package com.gtech.common.exception;



public class GTechException extends Exception {
    
    public GTechException(Throwable throwable) {
        super(throwable);
        throwable.printStackTrace();
    }
    
    public GTechException(String message,Throwable throwable) {
        super(throwable);
        throwable.printStackTrace();
    }
    
    public GTechException(String message){
            super(message);
        }
}

