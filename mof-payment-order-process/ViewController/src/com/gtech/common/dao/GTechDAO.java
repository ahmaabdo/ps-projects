package com.gtech.common.dao;

import com.gtech.common.exception.GTechException;
import com.gtech.common.reader.CommonConfigReader;
import com.gtech.common.service.ServiceLocator;
import com.gtech.common.util.EncryptionUtilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.NamingException;

import javax.sql.DataSource;


public class GTechDAO {


    protected static String driverName; // = CommonConfigReader.getValue("DB_DRIVER") ;
    protected static String serverName; // = CommonConfigReader.getValue("DB_SERVER_NAME") ;
    protected static String portNumber; // = CommonConfigReader.getValue("DB_PORT") ;
    protected static String sid; // = CommonConfigReader.getValue("DB_SID") ;
    protected static String username; // = CommonConfigReader.getValue("DB_USERNAME") ;
    protected static String password; // = CommonConfigReader.getValue("DB_PASSWORD") ;


    public static Connection getConnection() throws GTechException {
        if (CommonConfigReader.getValue("CONNECTION_TYPE").equalsIgnoreCase("JDBC")) {
            return getJDBCConnection();
        } else {
            return getDSConnection();
        }
    }

    public static Connection getConnection(boolean commit) throws GTechException {
        if (CommonConfigReader.getValue("CONNECTION_TYPE").equalsIgnoreCase("JDBC")) {
            return getJDBCConnection(commit);
        } else {
            return getDSConnection(commit);
        }
    }

    public static Connection getConnection(String schemaName) throws GTechException {
        if (CommonConfigReader.getValue("CONNECTION_TYPE").equalsIgnoreCase("JDBC")) {
            return getJDBCConnection(schemaName);
        } else {
            return getDSConnection(schemaName);
        }
    }

    private static Connection getJDBCConnection() throws GTechException {

        driverName = CommonConfigReader.getValue("DB_DRIVER");
        serverName = CommonConfigReader.getValue("DB_SERVER_NAME");
        portNumber = CommonConfigReader.getValue("DB_PORT");
        sid = CommonConfigReader.getValue("DB_SID");
        username = CommonConfigReader.getValue("DB_USERNAME");
        password = CommonConfigReader.getValue("DB_PASSWORD");

        Connection connection = null;

        try {
            Class.forName(driverName);
            connection =
                DriverManager.getConnection("jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid, username,
                                            EncryptionUtilities.desDecrypt(password));
        } catch (ClassNotFoundException e) {
            throw new GTechException("Sql Exception " + e.getMessage());
        } catch (SQLException e) {
            throw new GTechException("Sql Exception " + e.getMessage());
        }

        return connection;
    }

    private static Connection getJDBCConnection(boolean autoCommit) throws GTechException {

        driverName = CommonConfigReader.getValue("DB_DRIVER");
        serverName = CommonConfigReader.getValue("DB_SERVER_NAME");
        portNumber = CommonConfigReader.getValue("DB_PORT");
        sid = CommonConfigReader.getValue("DB_SID");
        username = CommonConfigReader.getValue("DB_USERNAME");
        password = CommonConfigReader.getValue("DB_PASSWORD");

        Connection connection = null;

        try {
            Class.forName(driverName);
            connection =
                DriverManager.getConnection("jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid, username,
                                            EncryptionUtilities.desDecrypt(password));
            connection.setAutoCommit(autoCommit);
        } catch (ClassNotFoundException e) {
            throw new GTechException("Sql Exception " + e.getMessage());
        } catch (SQLException e) {
            throw new GTechException("Sql Exception " + e.getMessage());
        }

        return connection;
    }

    private static Connection getJDBCConnection(String schemaName) throws GTechException {
        driverName = CommonConfigReader.getValue(schemaName + "_" + "DB_DRIVER");
        serverName = CommonConfigReader.getValue(schemaName + "_" + "DB_SERVER_NAME");
        portNumber = CommonConfigReader.getValue(schemaName + "_" + "DB_PORT");
        sid = CommonConfigReader.getValue(schemaName + "_" + "DB_SID");
        username = CommonConfigReader.getValue(schemaName + "_" + "DB_USERNAME");
        password = CommonConfigReader.getValue(schemaName + "_" + "DB_PASSWORD");

        String dbVendor = CommonConfigReader.getValue(schemaName + "_" + "DATABASE_VENDOR"); //dataBaseVender;

        Connection connection = null;

        try {
            Class.forName(driverName);
            connection =
                DriverManager.getConnection("jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid, username,
                                            EncryptionUtilities.desDecrypt(password));
        } catch (ClassNotFoundException e) {
            throw new GTechException("Sql Exception " + e.getMessage());
        } catch (SQLException e) {
            throw new GTechException("Sql Exception " + e.getMessage());
        }

        return connection;
    }

    private static Connection getDSConnection() throws GTechException {
        return getDSConnection(CommonConfigReader.getValue("DS_NAME"));
    }

    private static Connection getDSConnection(boolean autoCommit) throws GTechException {
        Connection connection = getDSConnection(CommonConfigReader.getValue("DS_NAME"));
        try {
            connection.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            throw new GTechException(e);
        }

        return connection;
    }

    private static Connection getDSConnection(String dsName) throws GTechException {
        DataSource dataSource = null;
        Connection conn = null;
        try {
            dataSource = ServiceLocator.getDataSource(dsName);
            conn = dataSource.getConnection();
        } catch (NamingException ne) {
            throw new GTechException("unable to find DS : " + dsName, ne);
        } catch (SQLException sqle) {
            throw new GTechException("unable to get connection from DS", sqle);
        }

        return conn;
    }


    public void closeResources(Connection connection, Statement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, PreparedStatement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, Statement stmt) {
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Connection connection, PreparedStatement stmt) {
        closeResources(stmt);
        closeResources(connection);
    }

    public void closeResources(Statement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
    }

    public void closeResources(PreparedStatement stmt, ResultSet rs) {
        closeResources(rs);
        closeResources(stmt);
    }

    public void closeResources(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public void closeResources(PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public void closeResources(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }

    public void closeResources(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO
        }
    }


    public String getGeneratedSequence(Statement stmt) throws GTechException {
        ResultSet rs = null;
        try {
            rs = stmt.getGeneratedKeys();
            rs.next();
            return rs.getString(1);
        } catch (SQLException sqlExp) {
            throw new GTechException(sqlExp);
        } finally {
            closeResources(rs);
        }
    }

    protected Integer getSeq(String seqName, Connection connection) throws SQLException {
        String query = "SELECT " + seqName + ".NEXTVAL AS SEQ_ID FROM DUAL";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.next()) {
            return rs.getInt("SEQ_ID");
        }
        return null;
    }


    public boolean valueUsed(String tableName, String primaryKeyColumnName, String primaryKeyColumnValue,
                             String... ignoredTableArray) throws GTechException {
        String constraintsQuery = "";
        String checkQuery = "";
        PreparedStatement constraintsStmt = null;
        Statement checkStmt = null;
        ResultSet constraintsRs = null;
        ResultSet checkRs = null;
        Connection connection = null;
        try {
            constraintsQuery =
                " SELECT  UCC1.TABLE_NAME FK_Table,UCC1.COLUMN_NAME FK_Column" + " FROM     USER_CONSTRAINTS UC, " +
                "          USER_CONS_COLUMNS UCC1, " + "          USER_CONS_COLUMNS UCC2 " +
                " WHERE    UC.CONSTRAINT_NAME = UCC1.CONSTRAINT_NAME " +
                " AND      UC.R_CONSTRAINT_NAME = UCC2.CONSTRAINT_NAME " + " AND      UC.CONSTRAINT_TYPE = 'R' " +
                " AND      UPPER(UCC2.TABLE_NAME) = UPPER(?) " + " AND      UPPER(UCC2.COLUMN_NAME) = UPPER(?) ";

            connection = getConnection();
            constraintsStmt = connection.prepareStatement(constraintsQuery);
            constraintsStmt.setString(1, tableName);
            constraintsStmt.setString(2, primaryKeyColumnName);
            //                  constraintsStmt.setString(3,CommonConfigReader.getValue("DB_SID"));

            constraintsRs = constraintsStmt.executeQuery();

            Foreign_Tables_Loop:
            while (constraintsRs.next()) {
                for (String ignoredTable : ignoredTableArray) {
                    if (ignoredTable.equalsIgnoreCase(constraintsRs.getString("FK_Table"))) {
                        continue Foreign_Tables_Loop;
                    }
                }
                checkStmt = connection.createStatement();
                checkQuery =
                    " SELECT 1 " + " FROM " + constraintsRs.getString("FK_Table") + " " + " WHERE " +
                    constraintsRs.getString("FK_Column") + " ='" + primaryKeyColumnValue + "'";
                checkRs = checkStmt.executeQuery(checkQuery);
                if (checkRs.next()) {
                    return true;
                } else {
                    closeResources(checkStmt, checkRs);
                }
            }

        } catch (SQLException sqlExp) {
            throw new GTechException(sqlExp);
        } finally {
            closeResources(constraintsStmt, constraintsRs);
            closeResources(checkStmt, checkRs);
            closeResources(connection);
        }
        return false;
    }

}
