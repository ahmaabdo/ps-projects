package com.gtech.common.service;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ServiceLocator {
    
    private static InitialContext context;
    
    static {
        try {
            context = new InitialContext();
        } catch(NamingException ne) {
            System.err.println("Unable to create JNDI Initial Context : " + ne.getMessage());
        }
    }

    public static DataSource getDataSource(String dsJndiName) throws NamingException {
        return (DataSource)context.lookup(dsJndiName);
    }
}
