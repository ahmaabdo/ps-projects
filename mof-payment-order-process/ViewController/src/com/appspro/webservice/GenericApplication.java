package com.appspro.webservice;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("webservice")
public class GenericApplication extends Application {
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();

        // Register root resources.
        classes.add(PaymentOrderRestServices.class);
        // Register provider classes.
        classes.add(GetPaymentOrderFullData.class);
        classes.add(PaymentOrderConfirmation.class);
        classes.add(PaymentOrderMofConfirmation.class);
        return classes;
    }
}
