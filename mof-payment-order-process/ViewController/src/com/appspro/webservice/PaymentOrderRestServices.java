package com.appspro.webservice;

import com.gtech.common.exception.GTechException;
import com.gtech.common.webservice.errorResponse.ErrorResponse;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import login.LoginBean;
import login.LoginBusiness;

@Path("rest")
@Produces("application/json")
public class PaymentOrderRestServices {
    @POST
    @Produces("application/json")
    public LoginBean logIn(@QueryParam("userName") String username,
                           @QueryParam("password") String password) throws GTechException {
        System.out.println("HIIIIIIIIIIIIII");


        LoginBean user = new LoginBean();
        ErrorResponse errorResponse = null;

        if (username == null || username.isEmpty()) {
            errorResponse = new ErrorResponse();
            errorResponse.setErrorMsg("WEB_SERVICE_NULL_USERNAME");
            user.setErrorResponse(errorResponse);
            user.setCheckLogin("0");

            return user;
        }

        if (password == null || password.isEmpty()) {
            errorResponse = new ErrorResponse();
            errorResponse.setErrorMsg("WEB_SERVICE_NULL_PASSWORD");
            user.setErrorResponse(errorResponse);
            user.setCheckLogin("0");

            return user;
        }
        
        LoginBusiness loginBusiness = new LoginBusiness();
        
        user =  loginBusiness.authenticateDBUser(username, password);
        
        if (user == null || user.getUserID() == null ||  user.getUserID().isEmpty()) {
            user = new LoginBean(); 
            errorResponse = new ErrorResponse();
            errorResponse.setErrorMsg("WEB_SERVICE_LOGIN_FAILED");
            user.setErrorResponse(errorResponse);
            user.setCheckLogin("0");

            return user;

        }
        
        user.setCheckLogin("1");

        user.setUserType("1");
        user.setValuesList(null);
        user.setOrganizationBean(null);

        return user;
    }

    @GET
    @Produces("application/json")
    public List<PaymentOrderBean> getPaymentOrderBeanByStatus(
                                 @QueryParam("password") String password,
                                 @QueryParam("userName") String userName) throws GTechException {

        System.out.println("");

        PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
        LoginBusiness loginBusiness = new LoginBusiness();
        
        LoginBean user =  loginBusiness.authenticateDBUser(userName, password);
        if(user == null) {
            return null;
        }
        List<PaymentOrderBean> reviewPOBeanList = null;
            //paymentOrderBusiness.getPaymentOrderBeanByStatus(100402, null, user);
        
        return reviewPOBeanList;
    }
    

    
    public PaymentOrderRestServices() {
        super();
    }
}
