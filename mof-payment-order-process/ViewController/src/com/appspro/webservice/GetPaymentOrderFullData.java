package com.appspro.webservice;

import com.gtech.common.exception.GTechException;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import login.LoginBean;
import login.LoginBusiness;

@Path("rest/GetPaymentOrderFullData")
@Produces("application/json")
public class GetPaymentOrderFullData {
    
    @GET
    @Produces("application/json")
    public PaymentOrderBean getFullDataPaymentOrder(
                                 @QueryParam("password") String password,
                                 @QueryParam("userName") String userName,
                                 @QueryParam("poId") String poId) {

        System.out.println("");

        PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
        LoginBusiness loginBusiness = new LoginBusiness();
        
        LoginBean user =  loginBusiness.authenticateDBUser(userName, password);
        if(user == null) {
            return null;
        }
        PaymentOrderBean paymentOrderBean = null;
        try {
            paymentOrderBean = paymentOrderBusiness.getFullDataPaymentOrder(Long.parseLong(poId));
        } catch (GTechException e) {
        }

        return paymentOrderBean;
    }
}
