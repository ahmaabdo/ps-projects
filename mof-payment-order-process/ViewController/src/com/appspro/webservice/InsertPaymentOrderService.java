package com.appspro.webservice;

import com.gtech.common.exception.GTechException;
import com.gtech.common.webservice.errorResponse.ErrorResponse;
import com.gtech.web.beneficiary.bean.BeneficiaryBean;
import com.gtech.web.currency.bean.CurrencyBean;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.ecomomicClassifications.bean.EcomomicClassificationBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.expense.bean.ExpenseBean;
import com.gtech.web.nationality.bean.NationalityBean;
import com.gtech.web.paymentOrder.bean.PaymentOrderBean;
import com.gtech.web.paymentOrder.dao.PaymentOrderDAO;
import com.gtech.web.project.bean.ProjectBean;
import com.gtech.web.section.bean.SectionBean;
import com.gtech.web.uploadPaymentOrder.bean.DISTRIBUTIONS;
import com.gtech.web.uploadPaymentOrder.bean.DistributionsPaymentOrderBean;
import com.gtech.web.uploadPaymentOrder.bean.PO;
import com.gtech.web.uploadPaymentOrder.bean.ROOT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import login.LoginBean;
import login.LoginBusiness;

@WebService(serviceName = "InsertPaymentOrderService", wsdlLocation = "WEB-INF/wsdl/InsertPaymentOrderService.wsdl")
public class InsertPaymentOrderService {

    private final Long PAYMENT_METHOD_EFT = 100102L;
    private final Long PAYMENT_METHOD_CHECK = 100101L;
    private final Long PAYMENT_METHOD_INSTR = 100103L;

    @WebMethod
    public List<ErrorResponse> insertPaymentOrder(@WebParam(name = "ROOT") ROOT root) {

        List<ErrorResponse> errorList = new ArrayList<>();

        if (root == null) {
            return new ArrayList<>();
        }
        String username = root.getUsername();
        String password = root.getPassword();

        LoginBean user = new LoginBean();
        ErrorResponse errorResponse = null;

        if (username == null || username.isEmpty()) {
            errorResponse = new ErrorResponse();
            errorResponse.setErrorMsg("WEB_SERVICE_NULL_USERNAME");
            errorList.add(errorResponse);
            return errorList;
        }

        if (password == null || password.isEmpty()) {
            errorResponse = new ErrorResponse();
            errorResponse.setErrorMsg("WEB_SERVICE_NULL_PASSWORD");
            errorList.add(errorResponse);
            return errorList;
        }


        user = authinticateUser(username, password);

        if (user == null || user.getUserID() == null || user.getUserID().isEmpty()) {
            errorResponse = new ErrorResponse();
            errorResponse.setErrorMsg("WEB_SERVICE_LOGIN_FAILED");
            errorList.add(errorResponse);
            return errorList;

        }
        List<PO> poList = root.getPO();

        for (PO bean : poList) {
            try {
                errorResponse = new ErrorResponse();
                errorResponse.setPaymentOrderNumber(bean.getPO_NUMBER() + "");

                List<String> checkList = checkParameters(bean);
                if (checkList.isEmpty()) {
                    String remarks = fillPO(bean, user);
                    if (remarks != null && !remarks.isEmpty()) {
                        errorResponse.setMsg(Arrays.asList(remarks.split("--")));
                    }
                } else {
                    errorResponse.setMsg(checkList);
                }


                errorList.add(errorResponse);
            } catch (GTechException e) {
                e.printStackTrace();
            }
        }

        return errorList;
    }


    private String fillPO(PO bean, LoginBean user) throws GTechException {
        PaymentOrderBean paymentOrderBean = new PaymentOrderBean();

        paymentOrderBean.setPaymentOrderNumber(bean.getPO_NUMBER() + "");
        paymentOrderBean.setPaymentOrderDateH(bean.getPO_DATE());
        //  paymentOrderBean.getPaymentOrderYear();
        paymentOrderBean.setEntityBean(new EntityBean());
        paymentOrderBean.getEntityBean().setCode(String.format("%03d", bean.getPO_ENTITY()));

        paymentOrderBean.setDepartmentBean(new DepartmentBean());
        paymentOrderBean.getDepartmentBean().setCode(String.format("%03d", bean.getPO_DEPARTMENT()));

        paymentOrderBean.setSectionBean(new SectionBean());
        paymentOrderBean.getSectionBean().setCode(String.format("%03d", bean.getPO_SECTION()));

        paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
        paymentOrderBean.getBeneficiaryBean().setCode(bean.getPO_BENEFICIARY_NUMBER() + "");
        paymentOrderBean.setIban(bean.getPO_IBAN());

        switch (bean.getPO_PAYMENT_TYPE().trim()) {
        case "CHECK":
            paymentOrderBean.setPaymentMethod(PAYMENT_METHOD_CHECK);
            break;
        case "EFT":
            paymentOrderBean.setPaymentMethod(PAYMENT_METHOD_EFT);
            break;
        case "INSTR":
            paymentOrderBean.setPaymentMethod(PAYMENT_METHOD_INSTR);
            break;

        default:
            paymentOrderBean.setPaymentMethod(0L);
        }

        paymentOrderBean.setCurrencyBean(new CurrencyBean());
        paymentOrderBean.getCurrencyBean().setCode(bean.getPO_CURRENCY());

        paymentOrderBean.setAmount(bean.getPO_AMOUNT());
        paymentOrderBean.setAmountInWord("h");

        paymentOrderBean.setExpenseBean(new ExpenseBean());
        paymentOrderBean.getExpenseBean().setCode("");

        paymentOrderBean.setNationalityBean(new NationalityBean());
        paymentOrderBean.getNationalityBean().setNationalityCode(bean.getPO_BENEFICIARY_NATIONALITY());
        paymentOrderBean.setExchangeOf(bean.getPO_EXCHANGE_OF());

        paymentOrderBean.setId(0);
        paymentOrderBean.setBeneficaryName(bean.getPO_ONE_TIME_SUPPLIER_NAME());
        paymentOrderBean.setOrg(String.format("%03d", bean.getPO_ORGANIZATION()));

        paymentOrderBean.setDistributionsPoBeanList(new ArrayList<>());
        DistributionsPaymentOrderBean disBean = null;

        for (DISTRIBUTIONS dis : bean.getDISTRIBUTIONS()) {
            disBean = new DistributionsPaymentOrderBean();

            disBean.setProjectBean(new ProjectBean());
            disBean.getProjectBean()
                .setCode(dis.getPROGRAM_NUMBER() != null && !dis.getPROGRAM_NUMBER().isEmpty() ?
                         dis.getPROGRAM_NUMBER() : "null");

            disBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            disBean.getEcomomicClassificationBean().setCode(dis.getECONOMIC_CLASSIFICATION() + "");

            disBean.setAmount(dis.getAMOUNT());

            paymentOrderBean.getDistributionsPoBeanList().add(disBean);
        }
        PaymentOrderDAO paymentOrderDAO = new PaymentOrderDAO();
        paymentOrderDAO.doValidatePaymentOrderService(paymentOrderBean, user);

        return paymentOrderBean.getRemark();

    }
    

    /*-------------insertDraftPaymentOrder--------------*/
        @WebMethod
        public List<ErrorResponse> insertDraftPaymentOrder(@WebParam(name = "ROOT") ROOT root) {

            List<ErrorResponse> errorList = new ArrayList<>();

            if (root == null) {
                return new ArrayList<>();
            }
            String username = root.getUsername();
            String password = root.getPassword();

            LoginBean user = new LoginBean();
            ErrorResponse errorResponse = null;

            if (username == null || username.isEmpty()) {
                errorResponse = new ErrorResponse();
                errorResponse.setErrorMsg("WEB_SERVICE_NULL_USERNAME");
                errorList.add(errorResponse);
                return errorList;
            }

            if (password == null || password.isEmpty()) {
                errorResponse = new ErrorResponse();
                errorResponse.setErrorMsg("WEB_SERVICE_NULL_PASSWORD");
                errorList.add(errorResponse);
                return errorList;
            }


            user = authinticateUser(username, password);

            if (user == null || user.getUserID() == null || user.getUserID().isEmpty()) {
                errorResponse = new ErrorResponse();
                errorResponse.setErrorMsg("WEB_SERVICE_LOGIN_FAILED");
                errorList.add(errorResponse);
                return errorList;

            }
            List<PO> poList = root.getPO();

            for (PO bean : poList) {
                try {
                    errorResponse = new ErrorResponse();
                    errorResponse.setPaymentOrderNumber(bean.getPO_NUMBER() + "");

                    List<String> checkList = checkParameters(bean);
                    if (checkList.isEmpty()) {
                        String remarks = fillDraftPO(bean, user);
                        if (remarks != null && !remarks.isEmpty()) {
                            errorResponse.setMsg(Arrays.asList(remarks.split("--")));
                        }
                    } else {
                        errorResponse.setMsg(checkList);
                    }


                    errorList.add(errorResponse);
                } catch (GTechException e) {
                    e.printStackTrace();
                }
            }

            return errorList;
        }

                        /*-------------fill Draft--------------*/
    private String fillDraftPO(PO bean, LoginBean user) throws GTechException {
        PaymentOrderBean paymentOrderBean = new PaymentOrderBean();

        paymentOrderBean.setPaymentOrderNumber(bean.getPO_NUMBER() + "");
        paymentOrderBean.setPaymentOrderDateH(bean.getPO_DATE());
        //  paymentOrderBean.getPaymentOrderYear();
        paymentOrderBean.setEntityBean(new EntityBean());
        paymentOrderBean.getEntityBean().setCode(String.format("%03d", bean.getPO_ENTITY()));

        paymentOrderBean.setDepartmentBean(new DepartmentBean());
        paymentOrderBean.getDepartmentBean().setCode(String.format("%03d", bean.getPO_DEPARTMENT()));

        paymentOrderBean.setSectionBean(new SectionBean());
        paymentOrderBean.getSectionBean().setCode(String.format("%03d", bean.getPO_SECTION()));

        paymentOrderBean.setBeneficiaryBean(new BeneficiaryBean());
        paymentOrderBean.getBeneficiaryBean().setCode(bean.getPO_BENEFICIARY_NUMBER() + "");
        paymentOrderBean.setIban(bean.getPO_IBAN());

        switch (bean.getPO_PAYMENT_TYPE().trim()) {
        case "CHECK":
            paymentOrderBean.setPaymentMethod(PAYMENT_METHOD_CHECK);
            break;
        case "EFT":
            paymentOrderBean.setPaymentMethod(PAYMENT_METHOD_EFT);
            break;
        case "INSTR":
            paymentOrderBean.setPaymentMethod(PAYMENT_METHOD_INSTR);
            break;

        default:
            paymentOrderBean.setPaymentMethod(0L);
        }

        paymentOrderBean.setCurrencyBean(new CurrencyBean());
        paymentOrderBean.getCurrencyBean().setCode(bean.getPO_CURRENCY());

        paymentOrderBean.setAmount(bean.getPO_AMOUNT());
        paymentOrderBean.setAmountInWord("h");

        paymentOrderBean.setExpenseBean(new ExpenseBean());
        paymentOrderBean.getExpenseBean().setCode(bean.getPO_EXPENSE());

        paymentOrderBean.setNationalityBean(new NationalityBean());
        paymentOrderBean.getNationalityBean().setNationalityCode(bean.getPO_BENEFICIARY_NATIONALITY());
        paymentOrderBean.setExchangeOf(bean.getPO_EXCHANGE_OF());

        paymentOrderBean.setId(0);
        paymentOrderBean.setBeneficaryName(bean.getPO_ONE_TIME_SUPPLIER_NAME());
        paymentOrderBean.setOrg(String.format("%03d", bean.getPO_ORGANIZATION()));

        paymentOrderBean.setDistributionsPoBeanList(new ArrayList<>());
        DistributionsPaymentOrderBean disBean = null;

        for (DISTRIBUTIONS dis : bean.getDISTRIBUTIONS()) {
            disBean = new DistributionsPaymentOrderBean();

            disBean.setProjectBean(new ProjectBean());
            disBean.getProjectBean()
                .setCode(dis.getPROGRAM_NUMBER() != null && !dis.getPROGRAM_NUMBER().isEmpty() ?
                         dis.getPROGRAM_NUMBER() : "null");

            disBean.setEcomomicClassificationBean(new EcomomicClassificationBean());
            disBean.getEcomomicClassificationBean().setCode(dis.getECONOMIC_CLASSIFICATION() + "");

            disBean.setAmount(dis.getAMOUNT());

            paymentOrderBean.getDistributionsPoBeanList().add(disBean);
        }
        PaymentOrderDAO paymentOrderDAO = new PaymentOrderDAO();
        paymentOrderDAO.doValidateDraftPaymentOrderService(paymentOrderBean, user);

        return paymentOrderBean.getRemark();

    }

    private LoginBean authinticateUser(String userName, String password) {
        LoginBusiness loginBusiness = new LoginBusiness();
        return loginBusiness.authenticateDBUser(userName, password);
    }

    private List<String> checkParameters(PO po) {
        List<String> errorList = new ArrayList<>();

        if (po.getPO_NUMBER() <= 0) {
            errorList.add("PO_NUMBER is manditory");
        }

        if (isBlankOrNull(po.getPO_DATE())) {
            errorList.add("PO_DATE is manditory");
        }

        //        if(po.getPO_ENTITY() <= 0 ) {
        //            errorList.add("PO_ENTITY is manditory");
        //        }
        //
        //        if(po.getPO_DEPARTMENT() <= 0 ) {
        //            errorList.add("PO_DEPARTMENT is manditory");
        //        }
        //
        //        if(po.getPO_SECTION() <= 0 ) {
        //            errorList.add("PO_SECTION is manditory");
        //        }

        //        if(po.getPO_ORGANIZATION() <= 0 ) {
        //            errorList.add("PO_ORGANIZATION is manditory");
        //        }


        if (po.getPO_BENEFICIARY_NUMBER() <= 0) {
            errorList.add("PO_BENEFICIARY_NUMBER is manditory");
        }

        if (isBlankOrNull(po.getPO_PAYMENT_TYPE())) {
            errorList.add("PO_PAYMENT_TYPE is manditory");
        }

        if (isBlankOrNull(po.getPO_CURRENCY())) {
            errorList.add("PO_CURRENCY is manditory");
        }

        if (isBlankOrNull(po.getPO_BENEFICIARY_NATIONALITY())) {
            errorList.add("PO_BENEFICIARY_NATIONALITY is manditory");
        }

        if (po.getPO_AMOUNT() <= 0) {
            errorList.add("PO_AMOUNT is manditory");
        }

        if (po.getDISTRIBUTIONS() == null) {
            errorList.add("DISTRIBUTIONS is manditory");
        } else if (po.getDISTRIBUTIONS().isEmpty()) {
            errorList.add("DISTRIBUTIONS is manditory");
        }

        return errorList;
    }

    private boolean isBlankOrNull(String check) {
        if (check == null) {
            return true;
        }

        if (check.isEmpty()) {
            return true;
        }

        return false;
    }

}
