package com.appspro.webservice;

import com.gtech.common.exception.GTechException;
import com.gtech.web.actionLog.business.ActionLogBusiness;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import login.LoginBean;
import login.LoginBusiness;

@Path("rest/PaymentOrderMofConfirmation")
@Produces("application/json")
public class PaymentOrderMofConfirmation {
    public PaymentOrderMofConfirmation() {
        super();
    }
    
    @GET
    @Produces("application/json")
    public void paymentOrderMofConfirmation(
                                 @QueryParam("password") String password,
                                 @QueryParam("userName") String userName,
                                 @QueryParam("poId") String poId,
                                 @QueryParam("remarks") String remarks,
                                 @QueryParam("actionType")String actionType,
                                 @QueryParam("expenseCode")String expenseCode) throws GTechException {
         ActionLogBusiness ab = new ActionLogBusiness();
         LoginBusiness loginBusiness = new LoginBusiness();
         
         LoginBean user =  loginBusiness.authenticateDBUser(userName, password);
         if(user == null) {
             return ;
         }
         boolean check = ab.insereLogLine(Long.parseLong(poId), remarks, Long.parseLong(user.getUserID()), 100505);
         PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness(); 
         paymentOrderBusiness.updatePaymentOrderExpence(Long.parseLong(poId), Long.parseLong("1"));

    
     }
    
}
