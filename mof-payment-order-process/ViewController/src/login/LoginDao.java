package login;

import com.gtech.common.dao.GTechDAO;
import com.gtech.common.exception.GTechException;
import com.gtech.common.util.EncryptionUtilities;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.organization.bean.OrganizationBean;
import com.gtech.web.section.bean.SectionBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginDao extends GTechDAO {


    public LoginDao() throws GTechException {
        super();
    }

    public LoginBean getCustomerID(String username, String password) throws GTechException {


        LoginBean loginBean =null;
        Connection conn = null;

        Statement stmt = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        String query = "";
        String ID = "";
        boolean swichUserName = false;

        try {
            query = "SELECT USR.SSC_ID," + 
                    "       USR.SSC_USERNAME," + 
                    "       USR.SSC_NAME_AR," + 
                    "       USR.SSC_NAME_EN," + 
                    "       USR.SSC_PASSWORD," + 
                    "       USR.SSC_STATUS," + 
                    "       USR.SSC_ENTITY_COMBINATION, " + 
                    "       USR.SSC_GRP_ID, " + 
                    "       USR.SSC_EEO_ID,   " + 
                    "       USR.SSC_TTG_ID,   " + 
                    "       USR.SSC_PTN_ID," + 
                    "       ENT.*," + 
                    "       DPT.*," + 
                    "       SCT.*,   " + 
                    "       ORG.*   " + 
                    "     FROM POG_USERS USR," + 
                    "          POG_ENTITIES ENT," + 
                    "          POG_DEPARTMENTS DPT," + 
                    "          POG_SECTION SCT," + 
                    "          POG_ORGANIZATION ORG" + 
                    "     WHERE ENT.EEO_ID(+) = USR.SSC_EEO_ID" + 
                    "           AND  DPT.TTG_ID(+) = USR.SSC_TTG_ID" + 
                    "           AND SCT.PTN_ID(+) = USR.SSC_PTN_ID " +
                    "           AND ORG.ORG_ID(+) = USR.SSC_ORG_ID " +
                    "           AND UPPER(USR.SSC_USERNAME) = UPPER('" + username + "')" +
                    "           AND USR.SSC_PASSWORD = '" + EncryptionUtilities.desEncrypt(password) + "'";


            conn = getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(query);

            if (rs.next()) {
                loginBean = new LoginBean();
                loginBean.setUserID(rs.getString("SSC_ID"));
                loginBean.setUserId(rs.getInt("SSC_ID"));
                loginBean.setUserName(rs.getString("SSC_USERNAME"));
                loginBean.setFullNameAR(rs.getString("SSC_NAME_AR"));
                loginBean.setFullNameEN(rs.getString("SSC_NAME_EN"));
                loginBean.setStatus(rs.getString("SSC_STATUS"));
                loginBean.setGroupRoleID(rs.getString("SSC_GRP_ID"));
                loginBean.setPassword(EncryptionUtilities.desDecrypt(rs.getString("SSC_PASSWORD")));
                
                loginBean.setEntityBean(new EntityBean());
                loginBean.getEntityBean().setId(rs.getLong("SSC_EEO_ID"));
                loginBean.getEntityBean().setCode(rs.getString("EEO_CODE"));
                loginBean.getEntityBean().setNameAr(rs.getString("EEO_NAME_AR"));
                
                loginBean.setDepartmentBean(new DepartmentBean());
                loginBean.getDepartmentBean().setId(rs.getLong("SSC_TTG_ID"));
                loginBean.getDepartmentBean().setCode(rs.getString("TTG_CODE"));
                loginBean.getDepartmentBean().setNameAr(rs.getString("TTG_NAME_AR"));
                
                loginBean.setSectionBean(new SectionBean());
                loginBean.getSectionBean().setId(rs.getLong("SSC_PTN_ID"));
                loginBean.getSectionBean().setCode(rs.getString("PTN_CODE"));
                loginBean.getSectionBean().setNameAr(rs.getString("PTN_NAME_AR"));
                
                loginBean.setOrganizationBean(new OrganizationBean());
                loginBean.getOrganizationBean().setNameAr(rs.getString("ORG_NAME_AR"));
                loginBean.getOrganizationBean().setId(rs.getLong("ORG_ID"));
                loginBean.getOrganizationBean().setOrgCode(rs.getString("ORG_CODE"));
                loginBean.getOrganizationBean().setActive(rs.getBoolean("ACTIVE"));
                
                try{
                    String query2 = "SELECT IGM_CODE FROM POG_FORMS_LIST forms , POG_USER_PRIVS priv WHERE forms.IGM_ID = PRIV.RES_IGM_ID AND PRIV.RES_GRP_ID ="+ rs.getString("ssc_grp_id");
                    stmt = conn.createStatement();
                    rs1 = stmt.executeQuery(query2);

                }catch(SQLException dqle){

                    String query2 = "SELECT wm_concat( " +
                    "                   (SELECT IGM_CODE FROM POG_FORMS_LIST WHERE IGM_ID =POG_USER_PRIVS.RES_IGM_ID )) AS IGM_CODE  " +
                    "                   FROM POG_USER_PRIVS " +
                    "                   WHERE RES_GRP_ID = "+ rs.getString("SSC_GRP_ID");
                    stmt = conn.createStatement();
                       rs1 = stmt.executeQuery(query2);
                }

                String permission = "";
                while (rs1.next()) {
                    permission += rs1.getString("IGM_CODE")+",";
                    
                }

                loginBean.setPermition(permission);

            }


        } catch (SQLException sqle) {
            loginBean = new LoginBean();
            throw new GTechException(sqle);
        } catch (Exception e) {
            loginBean = new LoginBean();
            throw new GTechException(e);
        } finally {
            closeResources(conn);
        }

        return loginBean;
    }

    public DepartmentBean getDepartment(String departmentID, Connection connection) throws GTechException,
                                                                                             SQLException {
        PreparedStatement preparedStatement = null;
        DepartmentBean departmentBean = null;
        ResultSet rs = null;
        String query = "SELECT * FROM POG_DEPARTMENTS  WHERE TTG_ID = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, departmentID);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            departmentBean = new DepartmentBean();
            departmentBean.setId(rs.getInt("TTG_ID"));
            departmentBean.setCode(rs.getString("TTG_CODE"));
            departmentBean.setNameAr(rs.getString("TTG_NAME_AR"));
        }


        return departmentBean;

    }

    public SectionBean getSection(String sectionID, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        SectionBean sectionBean = new SectionBean();
        ResultSet rs = null;
        String query = "SELECT * FROM POG_SECTION  WHERE PTN_ID = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sectionID);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            sectionBean = new SectionBean();
            sectionBean.setId(rs.getInt("PTN_ID"));
            sectionBean.setCode(rs.getString("PTN_CODE"));
            sectionBean.setNameAr(rs.getString("PTN_NAME_AR"));
        }
        return sectionBean;
    }

    public EntityBean getEntity(String entityID, Connection connection) throws GTechException, SQLException {
        PreparedStatement preparedStatement = null;
        EntityBean entityBean = new EntityBean();
        ResultSet rs = null;
        String query = "SELECT * FROM POG_ENTITIES  WHERE EEO_ID = ? ";

        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, entityID);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            entityBean = new EntityBean();
            entityBean.setId(rs.getInt("EEO_ID"));
            entityBean.setCode(rs.getString("EEO_CODE"));
            entityBean.setNameAr(rs.getString("EEO_NAME_AR"));
        }


        return entityBean;

    }

}
