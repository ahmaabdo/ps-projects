package login;

import com.gtech.common.beans.container.ResponseContainer;
import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.faces.util.MessageReader;

import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


public class LoginBacking extends GTechModelBacking {

    private String username;
    private String password;
    private String id;
    private LoginBean loginBean = new LoginBean();

    public LoginBacking() {
        super();
    }

    public String initialize() {

        return null;
    }

    public String performLogin() throws GTechException, SQLException, GTechException {

        LoginBusiness business = new LoginBusiness();
        setLoginBean(business.authenticateDBUser(username, password));

        if (loginBean == null) {
            FacesContext.getCurrentInstance().addMessage("msgs",new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageReader.getMessage("LOGIN_FAILED_MSG"),MessageReader.getMessage("LOGIN_FAILED_MSG")));
            return null;
        }
        
        LoginBean lb = loginBean;
        boolean check = true;
        String listOfPermition = lb.getPermition();
        if(listOfPermition !=null && (listOfPermition.contains("PO-F22"+",") || listOfPermition.contains("PO-F21"+","))){
            check =  false;
        } else {
            check = true;    
        }
        
        if(check) {
                
            
            if((loginBean.getEntityBean().getId() > 0) &&
                (loginBean.getOrganizationBean().getId() <= 0) ) {
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addErrorMessage("MSG_NO_ORG");
                createMessages(responseContainer);
                
                return null;
            }
            
            if((loginBean.getEntityBean().getId() > 0) &&
                !loginBean.getOrganizationBean().isActive()) {
                ResponseContainer responseContainer = new ResponseContainer();
                responseContainer.addErrorMessage("MSG_NO_ORG_ACTIVE");
                createMessages(responseContainer);
                
                setLoginBean(null);
                return null;
    
            }
        }
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedInUser");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedUserName");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedInUserFullName");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedBean");
        
        FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .getSessionMap()
                    .put("loggedInUser", loginBean.getUserId());
        FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .getSessionMap()
                    .put("loggedUserName", loginBean.getUserName());

        FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .getSessionMap()
                    .put("loggedInUserFullName", loginBean.getFullNameAR());
        FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .getSessionMap()
                    .put("loggedBean", getLoginBean());
         
        if(getLoginBean().getStatus().equals("100001")) {
            return "PO-F001";
        }
        return "goToCustomerInfo";
    }

    public String performLogout() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedInUser");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedUserName");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedInUserFullName");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("loggedBean");
        
        setUsername(null);
        setPassword(null);

        return "goToLogin";
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }
}
