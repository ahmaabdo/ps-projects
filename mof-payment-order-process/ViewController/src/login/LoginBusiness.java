package login;

import com.gtech.common.exception.GTechException;

public class LoginBusiness {
    
    LoginBean lb = new LoginBean();
    
    public LoginBusiness() {
        super();
    }
    
   
    public LoginBean authenticateDBUser(String username, String password) {
    
        LoginBean loginBean = null;
       
        String ID = "";

        LoginDao loginDao = null;
        try {
            if (username != null && password != null & !username.equals("") &&
                !password.equals("")) {
                loginDao = new LoginDao();
                loginBean = loginDao.getCustomerID(username, password);
            }

        } catch (GTechException ce) {
            System.out.println(ce.getMessage());
        }

        return loginBean;
    }

    public static void main(String[] args) {
        LoginBusiness lb = new LoginBusiness();
    }
}
