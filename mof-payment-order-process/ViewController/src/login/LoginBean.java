package login;

import com.gtech.common.base.BaseBean;
import com.gtech.common.webservice.errorResponse.ErrorResponse;
import com.gtech.web.department.bean.DepartmentBean;
import com.gtech.web.entity.bean.EntityBean;
import com.gtech.web.organization.bean.OrganizationBean;
import com.gtech.web.section.bean.SectionBean;

public class LoginBean extends BaseBean {
    public LoginBean() {
        super();
    }


    private String userName;
    private String userID;
    private String Password;
    private String fullNameAR;
    private String fullNameEN;
    private String groupRoleID;
    private String eecIDDepartmentSection;
    private String eecID;
    private String department;
    private String section;
    private DepartmentBean departmentBean;
    private SectionBean sectionBean;
    private EntityBean entityBean;
    private String status;
    private String permition;
    private OrganizationBean organizationBean = new OrganizationBean();
    private String checkLogin;
    private String userType;
    
    private ErrorResponse errorResponse;
    
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getPassword() {
        return Password;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setFullNameAR(String fullNameAR) {
        this.fullNameAR = fullNameAR;
    }

    public String getFullNameAR() {
        return fullNameAR;
    }

    public void setFullNameEN(String fullNameEN) {
        this.fullNameEN = fullNameEN;
    }

    public String getFullNameEN() {
        return fullNameEN;
    }

    public void setEecID(String eecID) {
        this.eecID = eecID;
    }

    public String getEecID() {
        return eecID;
    }

    public void setPermition(String permition) {
        this.permition = permition;
    }

    public String getPermition() {
        return permition;
    }

    public void setEecIDDepartmentSection(String eecIDDepartmentSection) {
        this.eecIDDepartmentSection = eecIDDepartmentSection;
    }

    public String getEecIDDepartmentSection() {
        return eecIDDepartmentSection;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSection() {
        return section;
    }

    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }

    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }

    public void setSectionBean(SectionBean sectionBean) {
        this.sectionBean = sectionBean;
    }

    public SectionBean getSectionBean() {
        return sectionBean;
    }

    public void setEntityBean(EntityBean entityBean) {
        this.entityBean = entityBean;
    }

    public EntityBean getEntityBean() {
        return entityBean;
    }

    public void setGroupRoleID(String groupRoleID) {
        this.groupRoleID = groupRoleID;
    }

    public String getGroupRoleID() {
        return groupRoleID;
    }

    public void setOrganizationBean(OrganizationBean organizationBean) {
        this.organizationBean = organizationBean;
    }

    public OrganizationBean getOrganizationBean() {
        return organizationBean;
    }

    public void setCheckLogin(String checkLogin) {
        this.checkLogin = checkLogin;
    }

    public String getCheckLogin() {
        return checkLogin;
    }


    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setErrorResponse(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }
}
