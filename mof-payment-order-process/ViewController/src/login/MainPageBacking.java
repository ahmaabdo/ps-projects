package login;

import com.gtech.common.exception.GTechException;
import com.gtech.common.model.GTechModelBacking;
import com.gtech.web.paymentOrder.business.PaymentOrderBusiness;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.event.ComponentSystemEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

@ManagedBean(name = "mainPageBacking")
@ViewScoped
public class MainPageBacking extends GTechModelBacking {
    private boolean popupLaunched = false;
    private Integer no = null;
    
    @Override
    public String initialize() throws GTechException {
        // TODO Implement this method
        return null;
    }

    public void invokePopup(ComponentSystemEvent componentSystemEvent) {
        //find popup relative to event component and
        //and avoid double invocation
        try {
            

            
            if(havaPermition("PO-F10")) {
                PaymentOrderBusiness paymentOrderBusiness = new PaymentOrderBusiness();
                no = paymentOrderBusiness.getNumberOFPOBeanByStatus(100410, getLogedInUser());
     
            }
            
            if(no != null && no != 0) {
                if (!popupLaunched) {
                    UIComponent pageFragmentRoot = componentSystemEvent.getComponent();
                    //show dialog
                    RichPopup p1 = (RichPopup) pageFragmentRoot.findComponent("p1Refund");
                    RichPopup.PopupHints hints = new RichPopup.PopupHints();
                    p1.show(hints);
                }
                popupLaunched = true;
            }

        } catch (GTechException e) {
            e.printStackTrace();
        }
    }


    public void setPopupLaunched(boolean popupLaunched) {
        this.popupLaunched = popupLaunched;
    }

    public boolean isPopupLaunched() {
        return popupLaunched;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Integer getNo() {
        return no;
    }
    
    public boolean getStop() {
        if (getLogedInUser().getEntityBean() == null || getLogedInUser().getEntityBean().getId() <= 0) {
            return true;
        } else{
            return false;
        }
    }
}
