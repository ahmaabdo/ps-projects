package com.appspro.bsbc.getSAASData;

import com.appspro.bsbc.biReport.BIReportModel;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.appspro.bsbc.biReport.BIReportModel.runReport;

public class RetrivePurchaseRequisition {
    public RetrivePurchaseRequisition() {
        super();
    }

    public JSONArray getRequisitionData(String SERVICE_CONTRACT_NUMBER,
                                        String REQUISITION_NUMBER,
                                        String SERVICE_REQUEST_NUMBER,
                                        String WORK_ORDER_NUMBER) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (SERVICE_CONTRACT_NUMBER != null) {

                paramMap.put(BIReportModel.GET_PURCHASING_REQUISITION_REPORT_PARAM.SERVICE_CONTRACT_NUMBER.getValue(),
                             SERVICE_CONTRACT_NUMBER);
            }
            if (REQUISITION_NUMBER != null) {

                paramMap.put(BIReportModel.GET_PURCHASING_REQUISITION_REPORT_PARAM.REQUISITION_NUMBER.getValue(),
                             REQUISITION_NUMBER);
            }

            if (SERVICE_REQUEST_NUMBER != null) {

                paramMap.put(BIReportModel.GET_PURCHASING_REQUISITION_REPORT_PARAM.SERVICE_REQUEST_NUMBER.getValue(),
                             SERVICE_REQUEST_NUMBER);
            }

            if (WORK_ORDER_NUMBER != null) {

                paramMap.put(BIReportModel.GET_PURCHASING_REQUISITION_REPORT_PARAM.WORK_ORDER_NUMBER.getValue(),
                             WORK_ORDER_NUMBER);
            }

            JSONObject json =
                runReport(BIReportModel.REPORT_NAME.PURCHASING_REQUISITION_REPORT.getValue(),
                          paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

            if (!dataDS.isNull("G_1")) {
                Object obj = dataDS.get("G_1");
                if (obj instanceof JSONArray) {
                    JSONArray g1 = dataDS.getJSONArray("G_1");
                    return g1;
                } else {
                    JSONObject g1 = dataDS.getJSONObject("G_1");
                    JSONArray ja = new JSONArray();

                    ja.put(g1);
                    return ja;
                }

            }
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }
}
