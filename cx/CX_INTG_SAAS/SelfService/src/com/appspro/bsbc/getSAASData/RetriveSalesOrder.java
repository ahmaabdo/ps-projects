package com.appspro.bsbc.getSAASData;

import com.appspro.bsbc.biReport.BIReportModel;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import static com.appspro.bsbc.biReport.BIReportModel.runReport;

public class RetriveSalesOrder {

    public JSONArray getSOData(String SERVICE_CONTRACT_NUMBER,
                               String ORDER_NUMBER,
                               String BSBC_UNIT_PO_REF,
                               String SERVICE_REQUEST_NUMBER,
                               String WORK_ORDER_NUMBER) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if(SERVICE_CONTRACT_NUMBER != null) {

                paramMap.put(BIReportModel.GET_SALES_ORDER_REPORT_PARAM.PARAM.getValue(),
                             SERVICE_CONTRACT_NUMBER);   
            }
            
            if(ORDER_NUMBER != null) {

                paramMap.put(BIReportModel.GET_SALES_ORDER_REPORT_PARAM.ORDER_NUMBER.getValue(),
                             ORDER_NUMBER);   
            }
            
            if(BSBC_UNIT_PO_REF != null) {

                paramMap.put(BIReportModel.GET_SALES_ORDER_REPORT_PARAM.BSBC_UNIT_PO_REF.getValue(),
                             BSBC_UNIT_PO_REF);   
            }
            
            if(SERVICE_REQUEST_NUMBER != null) {

                paramMap.put(BIReportModel.GET_SALES_ORDER_REPORT_PARAM.PARAM.getValue(),
                             SERVICE_REQUEST_NUMBER);   
            }
            
            if(WORK_ORDER_NUMBER != null) {

                paramMap.put(BIReportModel.GET_SALES_ORDER_REPORT_PARAM.PARAM.getValue(),
                             WORK_ORDER_NUMBER);   
            }

            JSONObject json =
                runReport(BIReportModel.REPORT_NAME.GET_SALES_ORDER_REPORT.getValue(),
                          paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return null;
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");

                if (!dataDS.isNull("G_1")) {
                    Object obj = dataDS.get("G_1");
                    if (obj instanceof JSONArray) {
                        JSONArray g1 = dataDS.getJSONArray("G_1");
                        return g1;
                    } else {
                        JSONObject g1 = dataDS.getJSONObject("G_1");
                        JSONArray ja = new JSONArray();

                        ja.put(g1);
                        return ja;
                    }
                    
                }
                return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }
}
