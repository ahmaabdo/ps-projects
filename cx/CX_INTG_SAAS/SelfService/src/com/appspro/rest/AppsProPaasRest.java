/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.rest;

/**
 *
 * @author Hp
 */


import com.appspro.bsbc.getSAASData.RetriveInventoryTransaction;
import com.appspro.bsbc.getSAASData.RetriveInvoices;
import com.appspro.bsbc.getSAASData.RetrivePurchaseOrder;
import com.appspro.bsbc.getSAASData.RetrivePurchaseRequisition;
import com.appspro.bsbc.getSAASData.RetriveSalesOrder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.json.JSONArray;

@Path("/appsProPaasRest")
public class AppsProPaasRest {

    @GET
    @Path("/getPurchaseOrder")
    public String getPurchaseOrder(@QueryParam("serviceContractNumber")
        String serviceContractNumber, @QueryParam("orderNumber")
        String orderNumber, @QueryParam("bsbcOrderNumber")
        String bsbcOrderNumber, @QueryParam("serviceRequestNumber")
        String serviceRequestNumber, @QueryParam("workOrderNumber")
        String workOrderNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        RetrivePurchaseOrder retrivePurchaseOrder = new RetrivePurchaseOrder();

        JSONArray resposne =
            retrivePurchaseOrder.getPOData(serviceContractNumber, orderNumber,
                                           bsbcOrderNumber,
                                           serviceRequestNumber,
                                           workOrderNumber);
        return resposne.toString();

    }

    @GET
    @Path("/getPurchaseRequistion")
    public String getPurchaseRequistion(@QueryParam("serviceContractNumber")
        String serviceContractNumber, @QueryParam("requisitionNumber")
        String requisitionNumber, @QueryParam("serviceRequestNumber")
        String serviceRequestNumber, @QueryParam("workOrderNumber")
        String workOrderNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        RetrivePurchaseRequisition retrivePurchaseRequisition =
            new RetrivePurchaseRequisition();

        JSONArray resposne =
            retrivePurchaseRequisition.getRequisitionData(serviceContractNumber,
                                                          requisitionNumber,
                                                          serviceRequestNumber,
                                                          workOrderNumber);
        return resposne.toString();

    }

    @GET
    @Path("/getSalesOrder")
    public String getSalesOrder(@QueryParam("serviceContractNumber")
        String serviceContractNumber, @QueryParam("salesOrderNumber")
        String salesOrderNumber, @QueryParam("bsbcUnitPoRef")
        String bsbcUnitPoRef, @QueryParam("serviceRequestNumber")
        String serviceRequestNumber, @QueryParam("workOrderNumber")
        String workOrderNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        RetriveSalesOrder retriveSalesOrder = new RetriveSalesOrder();

        JSONArray resposne =
            retriveSalesOrder.getSOData(serviceContractNumber, salesOrderNumber,
                                        bsbcUnitPoRef, serviceRequestNumber,
                                        workOrderNumber);
        return resposne.toString();

    }

    @GET
    @Path("/getInvoices")
    public String getInvoices(@QueryParam("serviceContractNumber")
        String serviceContractNumber, @QueryParam("invoiceNumber")
        String invoiceNumber, @QueryParam("serviceRequestNumber")
        String serviceRequestNumber, @QueryParam("workOrderNumber")
        String workOrderNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        RetriveInvoices retriveInvoices = new RetriveInvoices();

        JSONArray resposne =
            retriveInvoices.getInvoice(serviceContractNumber, invoiceNumber,
                                       serviceRequestNumber, workOrderNumber);
        return resposne.toString();

    }

    @GET
    @Path("/getInventoryTransaction")
    public String getInventoryTransaction(@QueryParam("serviceContractNumber")
        String serviceContractNumber, @QueryParam("serviceRequestNumber")
        String serviceRequestNumber, @QueryParam("workOrderNumber")
        String workOrderNumber, @Context
        HttpServletRequest request, @Context
        HttpServletResponse response, @HeaderParam("Authorization")
        String authString) {

        RetriveInventoryTransaction retriveInvoices =
            new RetriveInventoryTransaction();

        JSONArray resposne =
            retriveInvoices.getInvoice(serviceContractNumber, serviceRequestNumber,
                                       workOrderNumber);
        return resposne.toString();

    }
}
