
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessages', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
     'ojs/ojbutton', 'jquery', 'ojs/ojinputtext', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojdialog'
],
    function (oj, ko, moduleUtils, app, services, commonhelper) {
        function ControllerViewModel() {
            var self = this;
            const getTranslation = oj.Translations.getTranslatedString;
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);

            self.username = ko.observable();
            self.Usernametext = ko.observableArray();
            self.loginLabel = ko.observable();
            self.SignIn = ko.observable();
            self.forgetPassword = ko.observable();
            self.confirmLanguageLbl = ko.observable();
            self.signoLbl = ko.observable();
            self.languageSwitch_lng = ko.observable();
            self.confirmationTextLbl = ko.observable();
            self.yesLbl = ko.observable();
            self.yesActionLbl = ko.observable();
            self.noActionLbl = ko.observable();
            self.loginFailureText = ko.observable();
            self.password = ko.observable();
            self.userLogin = ko.observable();
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
            self.loginUser = ko.observable();
            self.messagesDataProvider = ko.observableArray([]);
            self.loginBtnLbl = ko.observable("LOGIN");
            self.personDetails = ko.observableArray([]);
            self.personId = ko.observable();
            self.personNumber = ko.observable();
            self.personName = ko.observable();
            self.personNumberAdmin = ko.observable('000113');
            self.personNameAdmin = ko.observable('Ahmed Al Othimin');
            self.localeLogin = ko.observable();
            self.UsernameLabel = ko.observable();
            self.passwordLabel = ko.observable();
            self.loginword = ko.observable();
            self.forgotPasswordLabel = ko.observable();
            self.localeName = ko.observable();
            self.localeJop = ko.observable();
            self.localeempNO = ko.observable();
            self.localeDep = ko.observable();
            self.localeGrade = ko.observable();
            self.agreementArr = ko.observableArray();
            self.bankAccountArr = ko.observable();
            self.supplierArr = ko.observable();
            self.notiCount = ko.observable(0);
            self.agreementArrValue = ko.observable();
            self.refreshViewForLanguage = ko.observable(false);
            self.manageBankAccountArr = ko.observable();
            self.supplierNegoArr = ko.observableArray();
            self.supplierNegoArrValue = ko.observableArray();
            self.allEmployeesArr = ko.observableArray();
            self.allMappedEmployeesArr = ko.observableArray();
            self.userValidation = ko.observable();
            self.lgActionValidation = ko.observable();
            self.lgNumber = ko.observable();
            self.notificationArr = ko.observableArray();

            self.language = ko.observable();
            self.languagelable = ko.observable("English");

            self.jwt = ko.observable('');
            self.hostUrl = ko.observable('');
            self.userName = ko.observable('');
            self.userNameJWT = ko.observable('');
            self.jwtinurl = ko.observable(false);
            self.isUserLoggedIn = ko.observable(false);
            self.loginDateFromSass = ko.observable(false);
            self.disOnLogin = ko.observable(false);
            self.notifyDisabled = ko.observable(true);

            self.contractsArr = ko.observableArray([]);
            self.loginVaild = ko.observable(false);

            self.exportHeader = ko.observable({
                documentNumber: "Document Number",
                contractAmount: "Contract Amount",
                contractNumber: "Contract Number",
                negotiationNumber: "Negotiation Number",
                negotiationTitle: "Negotiation Title",
                negotiationAmount: "Negotiation Amount",
                supplier: "Supplier",
                lgNumFromBank: "LG Num From Bank",
                lgType: "LG Type",
                description: "Description",
                lgStartDate: "LG Start Date",
                lgEndDate: "LG End Date",
                lgAmount: "LG Amount",
                bankAccount: "Bank Account",
                region: "Region",
                lgStatus: "LG Status",
                approvalStatus: "Approval Status",
                lgCloseToExp: "LG Close To Exp"
            })
            //     ----------------------------------------  loading function ------------------------ 
            self.loading = function (load) {
                if (load) {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            };

            //     ----------------------------------------  call agreement report ------------------------ 
            self.purchaseAgreement = async function () {

                var agreementFun = function (data) {
                    self.agreementArr(data);
                    self.agreementArrValue(data.map(el => ({ label: el.AGREEMENTNUMBER, value: el.AGREEMENTNUMBER })))
                }

                var agreementFunError = function () { }
                await services.getGenericReport({ "reportName": "Purchase_Agreement_Report" }).then(agreementFun, agreementFunError)
            }
            //  ---------------------------------------- bank account function ------------------------ 
            self.supplierNegotiation = async function () {

                var supplierNego = function (data) {
                    self.supplierNegoArr(data);
                    self.supplierNegoArrValue(data.map(el => ({ label: el.DOCUMENT_NUMBER, value: el.DOCUMENT_NUMBER })))
                }

                var supplierNegoError = function () { }
                await services.getGeneric(commonhelper.Negotiation).then(supplierNego, supplierNegoError)
            }
            //  ---------------------------------------- bank account function ------------------------ 
            self.bankAccount = function () {

                var bankAccountFun = function (data) {
                    self.bankAccountArr(data);
                }

                var bankAccountFunError = function () { }
                services.getGeneric(commonhelper.bankAccount).then(bankAccountFun, bankAccountFunError)
            }
            // ---------------------------------------- bank account function ------------------------ 
            self.manageBankAccount = function () {
                var bankAccountFun = function (data) {
                    self.manageBankAccountArr(data);
                }

                var bankAccountFunError = function () { }
                services.getGeneric(commonhelper.allMangaBankAccount).then(bankAccountFun, bankAccountFunError)
            }

            // ---------------------------------------- all Employess function ------------------------ 
            self.allEmployess = function () {

                var employeesFun = function (data) {
                    self.allEmployeesArr(data.Data);
                    self.allMappedEmployeesArr(data.Data.map(el => ({ value: el.DisplayName, label: el.DisplayName })));
                }

                var employeesFunError = function () { }
                services.getGeneric(commonhelper.getAllEmployees).then(employeesFun, employeesFunError)
            }

            //     ---------------------------------------- call notification ------------------------ 
            self.getALlNotification = function (personNumber) {
                services.getGeneric(commonhelper.getNotification + personNumber).then(data => {
                    if (data.Data.length > 0) {
                        // console.log(data.Data)
                        // var notifyData = data.Data.map(el => {
                        //     var obj = { ...el.lgData }
                        //     delete el.lgData
                        //     obj = { ...el.lgData , ...el }
                        //     return obj;
                        // })
                        // self.notificationArr(notifyData);
                        self.notiCount(data.Data.length);
                    }
                });
            };
            //     ---------------------------------------- message function ------------------------ 
            self.createMessage = function (type, summary) {
                return {
                    severity: type,
                    summary: summary,
                    autoTimeout: 4000
                };
            };
            //     ---------------------------------------- home button action------------------------ 
            self.homeBtnAction = function () {
                oj.Router.rootInstance.go('lgScreen');
            }

            //     ---------------------------------------- notification button action------------------------ 
            self.notifBtnAction = function () {
                oj.Router.rootInstance.go('notificationScreen');
            }

            //     ---------------------------------------- notification button action------------------------ 
            self.approvalBtnAction = function () {
                oj.Router.rootInstance.go('approvalSetup');
            }

            //     ---------------------------------------- enter to login  ------------------------ 
            $(function () {
                $('#username').on('keydown', function (ev) {
                    var mobEvent = ev;
                    var mobPressed = mobEvent.keyCode || mobEvent.which;
                    if (mobPressed == 13) {
                        $('#username').blur();
                        $('#password').focus();
                    }
                    return true;
                });
            });

            //     ---------------------------------------- login button function------------------------ 
            self.loginUserInfo = function (username) {
                self.loading(true);
                services.getGeneric(commonhelper.getEmp + username).then(data => {
                    if (data.Status == "Done") {
                        var data = data.Data;
                        self.loading(false);
                        self.notifyDisabled(false)
                        self.getALlNotification(data.PersonNumber);
                        self.personNumber(data.PersonNumber);
                        self.personId(data.personId);
                        self.loginUser(data.userName);
                        self.personName(data.DisplayName);
                        self.userValidation(data.validation)
                        self.lgActionValidation(data.Action)
                    } else {
                        self.personNumber('Invalid Username')
                    }

                }, error => { });
            };

            //---------------------------------login button--------------------------------------------
            self.validateUser = function () {
                if (!self.username() || !self.password())
                    return;

                $(".apLoginBtn").addClass("loading");
                self.loginFailureText("");

                self.loading(true);

                var loginSuccessCbFn = function () {
                    self.purchaseAgreement();
                    self.bankAccount();
                    self.manageBankAccount();
                    self.supplierNegotiation();
                    self.allEmployess();
                    self.loginUserInfo(self.username());
                    self.loading(false);
                    self.messagesDataProvider([]);
                    self.isUserLoggedIn(true);
                    self.router.go('lgScreen');
                };

                var loginFailCbFn = function () {
                    self.loading(false);
                    $(".apLoginBtn").removeClass("loading");
                    self.isUserLoggedIn(false);
                    self.loginFailureText("error");
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                };

                function authinticate(data) {
                    var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));
                    var result = parsedData.result;
                    if (result) {
                        loginSuccessCbFn();
                    } else {
                        loginFailCbFn();
                    }
                    $(".apLoginBtn").removeClass("loading");
                }

                var payload = {
                    "userName": self.username(),
                    "password": self.password()
                };

                services.authenticate(payload).then(authinticate, self.failCbFn);
                $(".apLoginBtn").removeClass("loading");
            };

            //     ---------------------------------------- log out button function------------------------ 

            self.logOutBtn = function (event) {
                self.isUserLoggedIn(false);
                location.reload();
                if (self.language() === 'english') {
                    $('html').attr({ 'dir': 'ltr' });
                } else if (self.language() === 'arabic') {
                    $('html').attr({ 'dir': 'rtl' });
                }
            };

            //     ---------------------------------------- Router setup function------------------------ 
            // Router setup
            self.router = oj.Router.rootInstance;
            self.router.dispose();

            self.router.configure({
                'lgScreen': { label: 'lgScreen', value: "lgScreen/lgScreen", id: "lgScreen", title: "TBC - LG", isDefault: true },
                'lgOperation': { label: 'lgOperation', value: "lgScreen/lgOperation", id: "lgOperation", title: "TBC - LG" },
                'lgHistory': { label: 'lgHistory', value: "lgHistory/lgHistory", id: "lgHistory", title: "TBC - LG" },
                'lgEditScreen': { label: 'lgEditScreen', value: "lgScreen/lgEditScreen", id: "lgEditScreen", title: "TBC - LG" },
                'notificationScreen': { label: 'notificationScreen', value: "notification/notificationScreen", id: "notificationScreen", title: "TBC - LG" },
                'confiscationScreen': { label: 'confiscationScreen', value: "confiscation/confiscationScreen", id: "confiscationScreen", title: "TBC - LG" },
                'approvalSetup': { label: 'approvalSetup', value: "approvalSetup/approvalSetup", id: "approvalSetup", title: "TBC - LG" }

            });

            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

            self.switchl = function () {
                document.querySelector("#switch").open();
            };
            self.switchclose = function () {
                document.querySelector("#switch").close();
            };
            self.switchLanguage = function () {
                document.querySelector("#switch").close();
                if (self.getLocale() === "ar") {
                    self.setLocale("en-US");
                    localStorage.setItem("selectedLanguage", "en-US");
                } else if (self.getLocale() === "en-US") {
                    self.setLocale("ar");
                    localStorage.setItem("selectedLanguage", "ar");
                }
            };

            self._showComponentValidationErrors = function (trackerObj) {
                if (trackerObj !== undefined) {
                    trackerObj.showMessages();
                    if (trackerObj.focusOnFirstInvalid())
                        return false;
                }
                return true;
            };

            self.moduleConfig = ko.observable({ 'view': [], 'viewModel': null });

            self.loadModule = async function () {
                ko.computed(function () {
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    var masterPromise = Promise.all([
                        moduleUtils.createView({ 'viewPath': viewPath }),
                        moduleUtils.createViewModel({ 'viewModelPath': modelPath })
                    ]);
                    masterPromise.then(
                        function (values) {
                            self.moduleConfig({ 'view': values[0], 'viewModel': values[1] });
                        },
                        function (reason) { }
                    );
                });
            };

            self.getLocale = function () {
                return oj.Config.getLocale();
            };

            self.setLocale = function (lang) {

                oj.Config.setLocale(lang,
                    function () {
                        self.refreshViewForLanguage(false);
                        if (lang === 'ar') {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "rtl");
                        } else {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "ltr");
                        }
                        initTranslations();
                        self.refreshViewForLanguage(true);
                    }
                );
            };

            var newFunction = function () {
                $.when(showEmplDetails(self)).done(function () {
                    if (self.loginVaild()) {
                        var username = localStorage.getItem("userName")
                        self.isUserLoggedIn(true)
                        self.purchaseAgreement();
                        self.bankAccount();
                        self.manageBankAccount();
                        self.supplierNegotiation();
                        self.loginUserInfo(username);
                        self.messagesDataProvider([]);
                        self.disOnLogin(false);
                        self.loading(false);
                        self.router.go('lgScreen');
                    } else {
                        self.isUserLoggedIn(false);
                    }
                });
            };

            newFunction();

            self.changeLanguage = function () {

            }

            function initTranslations() {
                self.Usernametext(getTranslation("login.userName"));
                self.passwordLabel(getTranslation("login.Password"));
                self.loginLabel(getTranslation("login.loginLabel"));
                self.SignIn(getTranslation("login.SignIn"));
                self.forgetPassword(getTranslation("login.forgetPassword"));
                self.confirmLanguageLbl(getTranslation("common.confirmLanguageLbl"));
                self.signoLbl(getTranslation("labels.signOut"));
                if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                }
                self.languageSwitch_lng(getTranslation("common.switchLang"));
                self.confirmationTextLbl(getTranslation("common.confirmationTextLbl"));
                self.yesActionLbl(getTranslation("common.yesActionLbl"));
                self.noActionLbl(getTranslation("common.noActionLbl"));
            }
            initTranslations();

        }

        return new ControllerViewModel();
    }
);