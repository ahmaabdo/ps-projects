define([], function() {

    function commonHelper() {
        var self = this;

        self.getBiReportServletPath = function() {
            var host = "report/commonbireport";
            return host;
        };

        self.getEmp = "Empolyees/";
        self.updateLgStatus = "lg/updateLgStatus"
        self.addLg = "lg/addLg";
        self.editLg = "lg/updateLg";
        self.getLg = "lg/getLg";
        self.deleteLg = "lg/deleteLg";
        self.getLastRowLg = "lg/getLastRowLg";
        self.getLgHistory = "lgHistory/getLgHistory/";
        self.addLgHistoryExtend = "lgHistory/updateLgExtend";
        self.addLgHistoryIncDec = "lgHistory/updateLgIncDec";
        self.addLgHistoryRelease = "lgHistory/updateLgRelease";
        self.insertAttachment = "attachment/uploadAttachment";
        self.deleteAttachment = "attachment/deleteAttachment/";
        self.getAttachment = "attachment/getAttachment/";
        self.cashTransaction = "externalCashTreansaction/create";
        self.bankAccount = "bankAccount/getBankAccount";
        self.lgFormat = "lgFormat/extendLgFormat";
        self.lgFormatWord = "lgFormat/extendReleaseLgFormat";
        self.allMangaBankAccount = "report/AllBankAccount";
        self.Negotiation = "report/getAllSupplierNegotiation";
        self.getLastHistory = "lgHistory/getLastHistory/";
        self.getAllEmployees = "Empolyees/AllEmployees";
        self.creationEmployee = "approvalSetup/creationEmployee";
        self.creationEmployees = "approvalSetup/creationEmployees";
        self.empApprovalSetup = "approvalSetup/empApproval" ;
        self.empsApprovalSetup = "approvalSetup/empsApproval";
        self.workFlowList = "notification/workFlowList";
        self.updateNotifyStatus = "notification/updateNotifyStatus";
        self.getNotification = "notification/getNotif/";
        self.lgActionApproval = "lgHistory/lgActionApproval";
        self.workFlowListAction = "lgHistory/workFlowListAction";


        self.getInternalRest = function() {
            if (window.location.hostname == "digital.tbc.sa") {

                var host = "https://digital.tbc.sa/TBC-LG-BACKEND-TEST/webapi/";

            } else {
                var host = "http://127.0.0.1:7101/TBC-LG-BACKEND-TEST/webapi/";
            }

            return host;
        };

    }

    return new commonHelper();
});