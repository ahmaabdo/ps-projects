function showEmplDetails(app) {


    var rootViewModel = app;
    var xx = "https://132.145.44.72:8989/TBC-FIN-RET-TEST/?username=Appspro.Fin&host=https://eoay-test.fa.em2.oraclecloud.com&lang=en&jwt=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6Imxyd1JBQlc4amIyN1lJelBxdkhQS3JQWnhMUSIsImtpZCI6InRydXN0c2VydmljZSJ9.eyJleHAiOjE1ODYzMDA2MjEsInN1YiI6IkFwcHNwcm8uRmluIiwiaXNzIjoid3d3Lm9yYWNsZS5jb20iLCJwcm4iOiJBcHBzcHJvLkZpbiIsImlhdCI6MTU4NjI4NjIyMX0.LfvMtFh25R1VgArJbkjT8QzAcZYavt-PSYLfT9VTuN0158B-_nQcaPg3u1C1BVmnbSMQrEZHRpLj34MsZijwavBYsoo1Aq8Qas2R9F0ONAHB-J9i7VepNPkfa1cSWIF6B2zkFRditlxnASm_4PFO4QfqdeCDMcBNFGg2cj7llcBzkXqVlvy0rriyrkVfe3RPSAtNZDMTs9TPvkcwPTIbKEHzUm2HiudZBUlnZLsjxE8fPNS2qWkI9GHko_edjKYEMDavbbD8ibvCElnKYaxXQxYhZILA_pZODfoNO9v6ikF41k2RNe0nOHiHtQEnwQQKDAC_yeTf_qpleRxugQm2fw"
    var url = new URL(window.location.href); //xx
    var jwt;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var deviceAndBrowserDetails = navigator.userAgent; //browser and device details 
    var LogindateTime;
    var isJWTNotValid = false;


    if (url.searchParams.get("jwt")) {
        if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
            hosturl = url.searchParams.get("host");
            rootViewModel.hostUrl(hosturl);
        }

        if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
            jwt = url.searchParams.get("jwt");
            rootViewModel.jwt(jwt);
        }



        if (rootViewModel.userNameJWT() == null || rootViewModel.userNameJWT().length == 0) {
            try {
                jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
                userNameJWT = jwtJSON.sub;

                isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;

                var isHasJwt = localStorage.jwt && localStorage.jwt == jwt;
                if (isJWTExpred) { //used before
                    isJWTNotValid = true;

                } else {
                    localStorage.setItem("userName", userNameJWT);
                    rootViewModel.userNameJWT(userNameJWT);
                }

            } catch (e) {};
        }

        if (url.searchParams.get("lang") && !isJWTNotValid) {

            var sessionLang = url.searchParams.get("lang");
            if (sessionLang == 'ar') {
                rootViewModel.setLocale('ar');
                localStorage.setItem("selectedLanguage", "ar");
            } else if (sessionLang == 'en') {
                rootViewModel.setLocale("en-US");
                localStorage.setItem("selectedLanguage", "en-US");
            }
        }

        if (!isJWTNotValid) { //its mean is JWT is vaild

            rootViewModel.loginVaild(true);
            $(".apLoginBtn").addClass("loading");
            rootViewModel.disOnLogin(true);

            LogindateTime = date + ' ' + time;
            rootViewModel.loginDateFromSass(LogindateTime);
            var payloadLoginHistory = {};
            $.getJSON("https://api.ipify.org/?format=json", function(e) {
                deviceIp = e.ip;
                payloadLoginHistory = {
                    "personNumber": userNameJWT,
                    "loginDate": LogindateTime,
                    "browserAndDeviceDetails": deviceAndBrowserDetails,
                    "deviceIp": deviceIp
                };
                sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));
                localStorage.jwt = [];
                localStorage.jwt = jwt; //save jwt into local storage to be checked next time
            });


        } else { //if jwt token not vaild 
            rootViewModel.JWTExpired(true);
        }
    } else {
        if (localStorage.getItem("jwt") != null) {
            jwtJSON = jQuery.parseJSON(atob(localStorage.getItem("jwt").split('.')[1]));
            isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;
            if (!isJWTExpred) {
                rootViewModel.loginVaild(true);
                $(".apLoginBtn").addClass("loading");
                rootViewModel.disOnLogin(true);
            }
        }
    }

}