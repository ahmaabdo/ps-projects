define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker', 'ojs/ojvalidationgroup'
], function(oj, ko, $, app, services, commonhelper) {
    function confiscationScreenContentViewModel() {
        var self = this;
        self.label = ko.observable();
        var retrieved = oj.Router.rootInstance.retrieve();

        self.customsModel = {
            bankAccountVal: ko.observable(),
            bankAccountArr: ko.observableArray(),
            letterOfGuarnAmoutVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            disableAdvance: ko.observable(true),
            lgNumber: ko.observable(),
            bankAccComb: ko.observable(),
            contractStartDate: ko.observable(),
            groupValid: ko.observable(),
            disableAccDesc: ko.observable(true),
            bankDescriptionVal: ko.observable(''),
            bankAccountName: ko.observable(),
            bankAccountNumVal: ko.observable(),
            disableAccNum: ko.observable(true),
            lgNumberFromBankVal: ko.observable(),
            getAttachmentArr: ko.observableArray(),
            selectedItemsAttachment: ko.observableArray([]),
            fileData: ko.observableArray([]),
            contentData : ko.observable(),
            attTitle: ko.observable(),
            attName: ko.observable(),
            attdocNum: ko.observable(),
            documentNumber : ko.observable(),
            payloadAttach: ko.observableArray([]),
            attachmentArr: ko.observableArray([]),
            actionSeq: ko.observable(),
        };

        self.dataProviderAttachment = ko.observable()
        self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))

        ko.computed(_ => {
            if (app.bankAccountArr()) {
                var bankAccValue = app.bankAccountArr().map(el => ({ label: el.bankAccount, value: el.bankAccount }));
                self.customsModel.bankAccountArr(bankAccValue);
            }
        });

        self.connected = function() {
            self.getLastRowHistory(retrieved.documentNumber);
            if (retrieved.confis) {
                self.customsModel.letterOfGuarnAmoutVal(numberWithCommas(retrieved.lgAmount));
                self.customsModel.lgNumberFromBankVal(retrieved.lgNumFromBank)
                self.customsModel.contractStartDate(retrieved.contractStartDate)
                self.customsModel.documentNumber(retrieved.documentNumber)
            }
        };

        self.lgStatusUpdate = function() {
            var payload = [{
                documentNumber: retrieved.documentNumber,
                lgNumFromBank: retrieved.lgNumFromBank,
                lgType: retrieved.lgType,
                lgEndDate: retrieved.lgEndDate,
                lgAmount: retrieved.lgAmount,
                actionSeq: self.customsModel.actionSeq().toString(),
                lgStatus: "Confiscation",
                approvalStatus: "APPROVED",
                operation: "Confiscation",
                lgActionStatus: "Pending", 
                lgActionType : "Confiscation",
                createBy: app.personNumber(), 
                createByName: app.personName(),
                Attachments: self.customsModel.attachmentArr(),
            }]

            services.postGeneric(commonhelper.lgActionApproval, payload).then(data => {
                if (data.Status == "OK") {
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Cash Transaction Created Successfuly '));
                    oj.Router.rootInstance.go("lgScreen")
                    app.loading(false);
                } else {
                    app.messagesDataProvider.push(app.createMessage('error', `Data has not been saved, because ${data.Data}`));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
            });
        }

        self.confiscationActionBtn = function() {
            app.loading(true);
            document.querySelector("#confirmConfis").close();
            var payload = {
                "AccountingFlag": true,
                "Amount": parseInt(self.customsModel.letterOfGuarnAmoutVal().replace(/,/g, "")),
                "CurrencyCode": "SAR",
                "Description": null,
                "ReferenceText": self.customsModel.lgNumberFromBankVal(), // lg number
                "Source": "MANUAL",
                "TransactionDate": self.customsModel.contractStartDate(),
                "TransactionType": "LG",
                "ValueDate": null,
                "AssetAccountCombination": self.customsModel.bankAccComb(),
                "OffsetAccountCombination": "01-0000-000000-000000-4310103-00000-00000",
                "BusinessUnitName": null,
                "LegalEntityName": "TBC",
                "BankAccountName": parseInt(self.customsModel.bankAccountVal()),
                "BankConversionRate": null,
                "BankConversionRateType": null,
                "TransferId": 1001
            }

            services.postGeneric(commonhelper.cashTransaction, payload).then(data => {
                if (data.status == 'done') {
                    self.lgStatusUpdate();
                } else {
                    document.querySelector("#confirmConfis").close();
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', `${data.data}`));
                }
            }, error => {
                app.messagesDataProvider.push(app.createMessage('error', `Cash Transaction not created`));
                document.querySelector("#confirmConfis").close();
            });
        };

        self.confiscationBtn = function() {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                app.messagesDataProvider.push(app.createMessage('error', 'You must fill all fields'));
                return;
            } else
                document.querySelector("#confirmConfis").open();
        };

        self.backAccountChangedValue = function(event) {
            if (event['detail'].value != null && event['detail'].value != '') {
                var x = app.bankAccountArr().find(el => el.bankAccount == event['detail'].value).bankDescription
                self.customsModel.bankAccountNumVal(app.bankAccountArr().find(el => el.bankAccount == event['detail'].value).bankAccountName)
                self.customsModel.bankAccountName(app.bankAccountArr().find(el => event['detail'].value == el.bankAccount).bankAccountName)
                self.customsModel.bankAccComb(app.bankAccountArr().find(el => el.bankAccount == event['detail'].value).cashAccountComb)
                if (x != "null")
                    self.customsModel.bankDescriptionVal(app.bankAccountArr().find(el => el.bankAccountName == event['detail'].value).bankDescription)
                else
                    self.customsModel.bankDescriptionVal('No data found')
            } else {
                self.customsModel.bankDescriptionVal('')
            }
        }

        self.getLastRowHistory = function(docNum) {
            services.getGeneric(commonhelper.getLastHistory + docNum).then(data => {
                self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1)
            }, error => {
            })
        };

        self.addFileSelectListener = function(event) {
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function(evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                self.customsModel.attdocNum(self.customsModel.documentNumber())
                var x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "docNum": self.customsModel.attdocNum(),
                    "content": self.customsModel.contentData(),
                    "attach_type": "Confiscation",
                    "id": 0
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name)
                            self.customsModel.payloadAttach().push(x);
                        x = {};
                        self.customsModel.attachmentArr(self.customsModel.payloadAttach());
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
        };

        self.removeSelectedAttachment = function() {
            if (self.customsModel.selectedItemsAttachment().length < 1)
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));

            var x = self.customsModel.attachmentArr().findIndex((el => el.name == self.customsModel.selectedItemsAttachment()[0][1]))
            self.customsModel.attachmentArr().splice(x, 1);
            self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.backBtn = function() {
            oj.Router.rootInstance.go('lgScreen');
        };

        self.cancelConfiscationbtn = function() {
            document.querySelector("#confirmConfis").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                lgEndDateLbl: getTransaltion("lgScreen.date"),
                lgTransactionDateLbl: getTransaltion("lgScreen.lgTransactionDateLbl"),
                confiscationTextLbl: getTransaltion("common.confirmConfisText"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                confirmConfis: getTransaltion("common.confirmConfis"),
                letterOfGuarnAmoutLbl: getTransaltion("lgOperation.letterOfGuarnAmoutLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                backLbl: getTransaltion("common.backLbl"),
                confiscationLbl: getTransaltion("common.confiscationLbl"),
                confisScreenLbl: getTransaltion("lgScreen.confisScreenLbl"),
                bankDescriptionLbl: getTransaltion("lgScreen.bankDescriptionLbl"),
                bankAccNoLbl: getTransaltion("lgScreen.bankAccNoLbl"),
                bankAccNumLbl: getTransaltion("lgOperation.bankAccNumLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                addFileLbl: getTransaltion("common.addFileLbl"),
            });
        }
        initTransaltion();
    }

    return confiscationScreenContentViewModel;
});