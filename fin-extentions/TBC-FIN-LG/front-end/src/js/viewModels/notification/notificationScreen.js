define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker'
], function (oj, ko, $, app, services, commonhelper) {
    function notificationScreenContentViewModel() {
        var self = this;
        var srNo = 0;
        self.label = ko.observable();
        self.customsModel = {
            notifArr: ko.observableArray([]),
            lgExpireArr: ko.observableArray([]),
            columnArray: ko.observableArray(),
            suppilerNameArr: ko.observableArray(),
            bankAccountArr: ko.observableArray(),
            lgNumberFromBankArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgStatusArr: ko.observableArray(),
            suppilerNameArr1: ko.observableArray(),
            bankAccountArr1: ko.observableArray(),
            lgNumberFromBankArr1: ko.observableArray(),
            contractNumberArr1: ko.observableArray(),
            lgStatusArr1: ko.observableArray(),
            rowSelected: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(),
            suppilerNameVal: ko.observable(),
            contractNoVal: ko.observable(),
            bankAccountVal: ko.observable(),
            lgStatusVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            documentNumber: ko.observable(),
            disableAllFields: ko.observable(true),
            resetDis: ko.observable(false),
            resetExpireDis: ko.observable(false),
            notifSelected: ko.observable(true),
            notifExpireSelected: ko.observable(true),
            data: ko.observableArray([]),
            data2: ko.observableArray([]),
            expireData: ko.observableArray([]),
            notificationArr: ko.observableArray([]),
            minDateVal: ko.observable(),
            checkNotifyStatus: ko.observable(false),
            actionSeq: ko.observable(),
            notifyData: ko.observableArray(),
        };

        var payload;
        var button;
        var date = formatDate(new Date());
        self.dataproviderNotifScreen = ko.observable();
        self.dataProviderLgCloseToExpire = ko.observable();
        self.username = ko.observable();
        self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, { idAttributes: 'documentNumber' }));
        self.dataProviderLgCloseToExpire(new oj.ArrayDataProvider(self.customsModel.lgExpireArr, { idAttributes: 'id' }));

        self.connected = function () {
            self.getExpireLg();
            self.getNotificationDataAfterAction();
        };

        self.getNotificationDataAfterAction = function () {
            services.getGeneric(commonhelper.getNotification + app.personNumber()).then(data => {
                app.notiCount(data.Data.length);
                if (data.Data.length > 0) {
                    var notifyData = data.Data.map(el => {
                        let obj = { ...el.lgData }
                        delete el.lgData
                        obj = { ...obj, ...el }
                        return obj;
                    })
                    self.fillAppSearchFields(notifyData);
                    self.customsModel.notificationArr(notifyData);
                    self.customsModel.checkNotifyStatus(true);
                    self.customsModel.notifArr(notifyData);
                    self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, { idAttributes: 'documentNumber' }));
                }
                else{
                    self.customsModel.notifArr([]);
                    self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, { idAttributes: 'documentNumber' }));
                }
            }, error => {
                app.loading(false);
            });
        };

        self.getExpireLg = function () {
            services.getGeneric(commonhelper.getLg).then(data => {
                if (data.Data) {
                    self.fillExpSearchFields(data.Data)
                    data.Data.forEach(el => { el.contractAmount ? el.contractAmount = numberWithCommas(el.contractAmount) : el.contractAmount = '-', el.negotiationAmount ? el.negotiationAmount = numberWithCommas(el.negotiationAmount) : el.negotiationAmount = '-', el.contractNumber ? el.contractNumber : el.contractNumber = '-', el.negotiationTitle ? el.negotiationTitle : el.negotiationTitle = '-', el.negotiationNumber ? el.negotiationNumber : el.negotiationNumber = '-', el.lgAmount = numberWithCommas(el.lgAmount), el.srNo = srNo++ })
                    data.Data.forEach(el => {
                        diff = date_diff_indays(date, el.lgEndDate);
                        if ((diff < 60 && (el.lgType != 'Bid bond') && (el.lgStatus == 'In Process')) || (diff < 30 && (el.lgType == 'Bid bond') && (el.lgStatus == 'In Process'))) {
                            self.customsModel.lgExpireArr.push(el);
                        }
                    });
                    self.dataProviderLgCloseToExpire(new oj.ArrayDataProvider(self.customsModel.lgExpireArr, { idAttributes: 'id' }));
                }
            }, error => {
                app.loading(false);
            })
        };

        self.fillAppSearchFields = function (notificationArr) {
            var suppArr = [...new Set(notificationArr.map(el => el.supplier))];
            var lgNumArr = [...new Set(notificationArr.map(el => el.lgNumFromBank))];
            var lgStatusArr = [...new Set(notificationArr.map(el => el.lgStatus))];
            var filteredData = notificationArr.filter(el => !el.contractNumber == '-');
            filteredData.length != 0 ? contractNumArr = [...new Set(filteredData.map(el => el.contractNumber))] : contractNumArr = [];

            self.customsModel.suppilerNameArr(suppArr.map(el => ({ label: el, value: el })));
            self.customsModel.lgNumberFromBankArr(lgNumArr.map(el => ({ label: el, value: el })));
            self.customsModel.contractNumberArr(contractNumArr.map(el => ({ label: el, value: el })));
            self.customsModel.lgStatusArr(lgStatusArr.map(el => ({ label: el, value: el })));
        };

        self.fillExpSearchFields = function (lgArr) {
            var suppArr1 = [...new Set(lgArr.map(el => el.supplier))];
            var lgNumArr1 = [...new Set(lgArr.map(el => el.lgNumFromBank))];
            var lgStatusArr1 = [...new Set(lgArr.map(el => el.lgStatus))];
            var filteredData = lgArr.filter(el => !el.contractNumber == '-');
            filteredData.length != 0 ? contractNumArr1 = [...new Set(filteredData.map(el => el.contractNumber))] : contractNumArr1 = [];

            self.customsModel.suppilerNameArr1(suppArr1.map(el => ({ label: el, value: el })));
            self.customsModel.lgNumberFromBankArr1(lgNumArr1.map(el => ({ label: el, value: el })));
            self.customsModel.contractNumberArr1(contractNumArr1.map(el => ({ label: el, value: el })));
            self.customsModel.lgStatusArr1(lgStatusArr1.map(el => ({ label: el, value: el })));
        };

        ko.computed(() => {
            if (app.loginUser()) {
                self.username(app.loginUser());
                if (app.loginUser() != 'ahmed.alothimin@tbc.sa') {
                    self.notifTableLbl = 'Rejected Requests Table';
                }
            }
        });

        self.notifselectListener = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.notificationArr()[[currentRow['rowIndex']]];
                self.customsModel.notifSelected(false);
            }
        };

        self.lgCloseToExpireSelectionListener = function (event) {
            var data = event.detail;
            if (!data)
                return;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.lgExpireArr()[[currentRow['rowIndex']]];
                if (self.customsModel.rowSelected.lgStatus == "In Process") {
                    self.customsModel.notifExpireSelected(false);
                } else {
                    self.customsModel.notifExpireSelected(true);
                }
            }
        };

        self.searchBtn = function () {
            cond = function (el) {
                var res = (
                    (self.customsModel.documentNumber() ? el.documentNumber == self.customsModel.documentNumber() : true) &&
                    (self.customsModel.lgNumberFromBankVal() ? el.lgNumFromBank == self.customsModel.lgNumberFromBankVal() : true) &&
                    (self.customsModel.suppilerNameVal() ? el.supplier == self.customsModel.suppilerNameVal() : true) &&
                    (self.customsModel.contractNoVal() ? el.contractNumber == self.customsModel.contractNoVal() : true) &&
                    (self.customsModel.lgStatusVal() ? el.lgStatus == self.customsModel.lgStatusVal() : true)
                );
                return res;
            };
            var find;
            if (app.personNumber() == app.personNumberAdmin())
                find = self.customsModel.data().filter(el => cond(el));
            else
                find = self.customsModel.data2().filter(el => cond(el));

            self.customsModel.notifArr(find);
            self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, { idAttributes: 'documentNumber' }));
        };

        self.searchExpireBtn = function () {
            cond = function (el) {
                var res = (
                    (self.customsModel.documentNumber() ? el.documentNumber == self.customsModel.documentNumber() : true) &&
                    (self.customsModel.lgNumberFromBankVal() ? el.lgNumFromBank == self.customsModel.lgNumberFromBankVal() : true) &&
                    (self.customsModel.suppilerNameVal() ? el.supplier == self.customsModel.suppilerNameVal() : true) &&
                    (self.customsModel.contractNoVal() ? el.contractNumber == self.customsModel.contractNoVal() : true) &&
                    (self.customsModel.lgStatusVal() ? el.lgStatus == self.customsModel.lgStatusVal() : true)
                );
                return res;
            };
            var findExpire = self.customsModel.expireData().filter(el => cond(el));
            self.customsModel.lgExpireArr(findExpire);
            self.dataProviderLgCloseToExpire(new oj.ArrayDataProvider(self.customsModel.lgExpireArr, { idAttributes: 'documentNumber' }));
        };

        self.resetBtn = function () {
            self.customsModel.resetDis(true);
            self.customsModel.documentNumber('');
            self.customsModel.lgNumberFromBankVal('');
            self.customsModel.suppilerNameVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgStatusVal('');
            setTimeout(function () {
                self.customsModel.resetDis(false);
                if (app.personNumber() == app.personNumberAdmin())
                    self.customsModel.notifArr(self.customsModel.data());
                else
                    self.customsModel.notifArr(self.customsModel.data2());
                self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, { idAttributes: 'documentNumber' }));
            }, 1000);
        };

        self.rejectBtn = function () {
            app.loading(true);
            var selected = self.customsModel.rowSelected;
            var x = self.customsModel.notificationArr().find(el => el.documentNumber == selected.documentNumber);
            payload = {
                "id": selected.id,
                "approvalStatus": "REJECT",
                "lgStatus": "Rejected",
                "lgType": selected.lgType,
                "documentNumber": selected.documentNumber,
                "lgNumber": selected.lgNumFromBank,
                "createdBy": app.personNumber(),
                "createdByName": app.personName(),
                "requesterName": x.requesterName,
                "requesterNumber": x.requesterNumber,
                "approvalSeq": x.approvalPersonSeq,
                "lgAction" : selected.lgAction,
                "lgAmount" : selected.lgAmount,
                "lgEndDate" : selected.lgEndDate,
            };

            if(selected.lgAction == "None")
                payload.actionSeq = ''
            else 
                payload.actionSeq =    self.customsModel.notifArr().find(el => el.documentNumber == selected.documentNumber).actionSeq


            services.postGeneric(commonhelper.updateNotifyStatus, payload).then(data => {
                if (data) {
                    self.getNotificationDataAfterAction();
                    self.getExpireLg();
                    self.customsModel.notifSelected(true);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

        self.approveBtn = function () {
            app.loading(true);
            var selected = self.customsModel.rowSelected;
            var x = self.customsModel.notificationArr().find(el => el.documentNumber == selected.documentNumber);
            payload = {
                "id": selected.id,
                "approvalStatus": "APPROVED",
                "lgStatus": "In Process",
                "lgType": selected.lgType,
                "documentNumber": selected.documentNumber,
                "lgNumber": selected.lgNumFromBank,
                "createdBy": app.personNumber(),
                "createdByName": app.personName(),
                "requesterName": x.requesterName,
                "requesterNumber": x.requesterNumber,
                "approvalSeq": x.approvalPersonSeq,
                "lgAmount" : selected.lgAmount,
                "lgEndDate" : selected.lgEndDate,
                "lgAction" : selected.lgAction,          
            };

            if(selected.lgAction == "None")
                payload.actionSeq = ''
            else 
                payload.actionSeq =    self.customsModel.notifArr().find(el => el.documentNumber == selected.documentNumber).actionSeq

            if(selected.lgAction == "Release")
                payload.lgStatus = "Released"

            else if(selected.lgAction == "Confiscation")    
                payload.lgStatus = "Confiscation"

            services.postGeneric(commonhelper.updateNotifyStatus, payload).then(data => {
                if (data) {
                    self.getNotificationDataAfterAction();
                    self.getExpireLg();
                    self.customsModel.notifSelected(true);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

        self.addHistory = function (payload) {
            app.loading(true);
            if (button == "yes") {
                services.getGeneric(commonhelper.getLastHistory + self.customsModel.rowSelected.documentNumber).then(data => {
                    self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1);
                    payload[0].actionSeq = self.customsModel.actionSeq().toString()
                    services.postGeneric(commonhelper.addLgHistoryRelease, payload).then(data => {
                        app.loading(false);
                    }, error => {
                        app.loading(false);
                        app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                    });
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                })

            } else {
                services.getGeneric(commonhelper.getLastHistory + self.customsModel.rowSelected.documentNumber).then(data => {
                    self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1);
                    payload[0].actionSeq = self.customsModel.actionSeq().toString()
                    payload[0].lgEndDate = self.customsModel.lgEndDateVal();
                    services.postGeneric(commonhelper.addLgHistoryExtend, payload).then(data => {
                        app.loading(false);
                    }, error => {
                        app.loading(false);
                        app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                    });
                })
            }
        };

        self.yes = function () {
            button = "yes";
            app.loading(true);
            document.querySelector("#confirmRelease").close();
            var selected = self.customsModel.rowSelected;
            payload = [{
                id: selected.id,
                documentNumber: selected.documentNumber,
                lgType: selected.lgType,
                lgEndDate: selected.lgEndDate,
                lgAmount: selected.lgAmount,
                contractAmount: selected.contractAmount != '-' ? selected.contractAmount : selected.negotiationAmount,
                lgStatus: "Released",
                approvalStatus: "APPROVED",
                operation: "Realease",
                Attachments: []
            }];

            services.postGeneric(commonhelper.updateLgStatus, payload).then(data => {
                if (data) {
                    self.addHistory(payload);
                    app.loading(false);
                    self.getExpireLg();
                    self.customsModel.notifExpireSelected(true);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

        self.yesExtend = function () {
            button = "yesExtend";
            var selected = self.customsModel.rowSelected;
            payload = [{
                id: selected.id,
                documentNumber: selected.documentNumber,
                contractNumber: selected.contractNumber,
                contractStartDate: selected.contractStartDate,
                contractEndDate: selected.contractEndDate,
                lgNumFromBank: selected.lgNumFromBank,
                lgType: selected.lgType,
                lgEndDate: self.customsModel.lgEndDateVal(),
                contractAmount: selected.contractAmount != '-' ? selected.contractAmount : selected.negotiationAmount,
                region: selected.region,
                lgAmount: selected.lgAmount,
                description: selected.description,
                lgStatus: "In Process",
                operation: "Extend",
                createdBy: selected.createdBy,
                Attachments: []
            }];

            self.addHistory(payload);
            document.querySelector("#confirmExtend").close();
            self.getExpireLg();
        };

        self.extendBtn = function () {
            var selected = self.customsModel.rowSelected;
            document.querySelector("#confirmExtend").open();
            self.customsModel.documentNumber(selected.documentNumber);
            self.customsModel.minDateVal(selected.lgEndDate);
            self.customsModel.lgEndDateVal(selected.lgEndDate);
        };

        self.resetExpireBtn = function () {
            self.customsModel.resetExpireDis(true);
            self.customsModel.documentNumber('');
            self.customsModel.lgNumberFromBankVal('');
            self.customsModel.suppilerNameVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgStatusVal('');
            setTimeout(function () {
                self.customsModel.resetExpireDis(false);
                self.customsModel.lgExpireArr(self.customsModel.expireData());
            }, 1000);
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function (date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            var x = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
            return x
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.no = function () {
            document.querySelector("#confirmRelease").close();
        };

        self.releaseBtn = function () {
            document.querySelector("#confirmRelease").open();
        };

        self.noExtend = function () {
            document.querySelector("#confirmExtend").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                confirmText: getTransaltion("common.confirmText"),
                confirmReleaseLbl: getTransaltion("lgScreen.confirmReleaseLbl"),
                confirmExtendLbl: getTransaltion("lgScreen.confirmExtendLbl"),
                releaseLbl: getTransaltion("common.releaseLbl"),
                extendLbl: getTransaltion("common.extendLbl"),
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                suppilerNameLbl: getTransaltion("lgScreen.suppilerNameLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgStatusLbl: getTransaltion("lgScreen.lgStatusLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                bankAmountLbl: getTransaltion("lgScreen.bankAmountLbl"),
                amountLbl: getTransaltion("lgScreen.amountLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                status: getTransaltion("lgScreen.status"),
                searchLbl: getTransaltion("common.searchLbl"),
                approveLbl: getTransaltion("common.approveLbl"),
                rejectLbl: getTransaltion("common.rejectLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                approvalStatus: getTransaltion("lgScreen.approvalStatus"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                notifScreenLbl: getTransaltion("lgScreen.notifScreenLbl"),
                notifTableLbl: getTransaltion("lgScreen.notifTableLbl"),
                expireTableLbl: getTransaltion("lgScreen.expireTableLbl"),
                lgStartDateLbl: getTransaltion("lgOperation.lgStartDateLbl"),
                negotiationNumLbl: getTransaltion("lgOperation.negotiationNumLbl"),
                negotiationTitleLbl: getTransaltion("lgOperation.negotiationTitleLbl"),
                negotiationAmountLbl: getTransaltion("lgOperation.negotiationAmountLbl"),
                requesterEmpNameLbl: getTransaltion("approvalScreen.requesterEmpNameLbl"),
                lgActionLbl: getTransaltion("approvalScreen.lgActionLbl"),
            });
            self.customsModel.columnArray([
                // {
                //     "headerText": "NO.", "field": "srNo"
                // },
                {
                    "headerText": self.label().documentNumberLbl,
                    "field": "documentNumber"
                },
                {
                    "headerText": self.label().lgNumberFromBankLbl,
                    "field": "lgNumFromBank"
                },
                {
                    "headerText": self.label().contractNoLbl,
                    "field": "contractNumber"
                },
                {
                    "headerText": self.label().bankAmountLbl,
                    "field": "contractAmount"
                },
                {
                    "headerText": self.label().negotiationTitleLbl,
                    "field": "negotiationTitle"
                },
                {
                    "headerText": self.label().negotiationNumLbl,
                    "field": "negotiationNumber"
                },
                {
                    "headerText": self.label().negotiationAmountLbl,
                    "field": "negotiationAmount"
                },
                {
                    "headerText": self.label().suppilerNameLbl,
                    "field": "supplier"
                },
                {
                    "headerText": self.label().amountLbl,
                    "field": "lgAmount"
                },
                {
                    "headerText": self.label().lgStartDateLbl,
                    "field": "lgStartDate"
                },
                {
                    "headerText": self.label().lgEndDateLbl,
                    "field": "lgEndDate"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                }, 
                {
                    "headerText": self.label().status,
                    "field": "approvalStatus"
                },
                {
                    "headerText": self.label().lgActionLbl,
                    "field": "lgAction"
                },
                {
                    "headerText": self.label().requesterEmpNameLbl,
                    "field": "requesterName"
                }
            ]);
        }
        initTransaltion();
    }

    return notificationScreenContentViewModel;
});