define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojarraytabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper) {
    function approvalScreenContentViewModel() {
        var self = this;
        var srNo = 0;
        var srNo2 = 0;
        self.label = ko.observable();

        self.customsModel = {

            typeActionVal: ko.observable('Create'),
            lgTypeVal: ko.observable(),
            approvalLevelVal: ko.observable(),
            exportDis: ko.observable(false),
            dataproviderCreateAppScreen: ko.observable(),
            dataproviderApprovalScreen: ko.observable(),
            dataproviderActionValScreen: ko.observable(),
            dataproviderActionAppScreen: ko.observable(),
            employeeNameVal: ko.observable(),
            rowSelected: ko.observable(),
            appEmployeeNameVal: ko.observable(),
            groupValid2: ko.observable(),
            disEmployee: ko.observable(false),
            disTypeAction: ko.observable(false),
            updateDis: ko.observable(true),
            deleteDis: ko.observable(true),
            addDis: ko.observable(true),
            lgActionVal: ko.observable(),
            approvalTypeVal: ko.observable('LG Creation'),

            createApprovalArr: ko.observableArray([]),
            approvalArr: ko.observableArray([]),
            actionValArr: ko.observableArray([]),
            actionAppArr: ko.observableArray([]),
            allEmpolyeesArr: ko.observableArray([]),
            columnArray: ko.observableArray([]),
            approvalColumnArray: ko.observableArray([]),
            actionValColumnArray: ko.observableArray([]),
            actionAppColumnArray: ko.observableArray([]),
            typeActionArr: ko.observableArray([{ label: "Create", value: "Create" }, { label: "Approval", value: "Approval" }]),
            lgTypeArr: ko.observableArray([{ label: "Bid bond", value: "Bid bond" }, { label: "Final", value: "Final" }, { label: "Advance", value: "Advance" }]),
            approvalLevelArr: ko.observableArray([{ label: "First Approval", value: "1" }, { label: "Second Approval", value: "2" }, { label: "Third Approval", value: "3" }, { label: "Fourth Approval", value: "4" }, { label: "Fifth Approval", value: "5" }]),
            approvalTypeArr: ko.observableArray([{ label: "LG Creation", value: "LG Creation" }, { label: "LG Action", value: "LG Action" }]),
            lgActionArr: ko.observableArray([{ label: "Increase", value: "Increase" }, { label: "Decrease", value: "Decrease" }, { label: "Extend", value: "Extend" }, { label: "Release", value: "Release" }, { label: "Confiscation", value: "Confiscation" }]),
        }
        //, { label: "All Action", value: "All Action" }
        self.customsModel.dataproviderCreateAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.createApprovalArr, { idAttribute: 'srNo' })))
        self.customsModel.dataproviderApprovalScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.approvalArr, { idAttribute: 'srNo' })))
        self.customsModel.dataproviderActionValScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.actionValArr, { idAttribute: 'srNo' })))
        self.customsModel.dataproviderActionAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.actionAppArr, { idAttribute: 'srNo' })))


        ko.computed(() => {
            if (app.allMappedEmployeesArr().length > 0) {
                self.customsModel.allEmpolyeesArr(app.allMappedEmployeesArr());
                self.customsModel.addDis(false);
            }
        });

        self.connected = function () {
            app.loading(true);
            services.getGeneric(commonhelper.creationEmployees).then(data => {
                if (data.StatusCode == "OK") {
                    app.loading(false);
                    data.Data.forEach(el => {
                        if (el.lgAction && el.lgAction == 'None') {
                            el.srNo = self.customsModel.createApprovalArr().length + 1,
                                self.customsModel.createApprovalArr.push(el)
                        }
                        else {
                            el.srNo = self.customsModel.actionValArr().length + 1,
                                self.customsModel.actionValArr.push(el)
                        }
                    })
                    self.customsModel.dataproviderCreateAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.createApprovalArr, { idAttribute: 'srNo' })))
                    self.customsModel.dataproviderActionValScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.actionValArr, { idAttribute: 'srNo' })))
                }
            }, error => { 
                app.loading(false);
            });

            services.getGeneric(commonhelper.empsApprovalSetup).then(data => {
                if (data.StatusCode == "OK") {
                    app.loading(false);
                    data.Data.forEach(el => {
                        if (el.lgAction && el.lgAction == 'None') {
                            el.srNo = self.customsModel.approvalArr().length + 1,
                                self.customsModel.approvalArr.push(el)
                        }
                        else {
                            el.srNo = self.customsModel.actionAppArr().length + 1,
                                self.customsModel.actionAppArr.push(el)
                        }
                    })
                    self.customsModel.dataproviderApprovalScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.approvalArr, { idAttribute: 'srNo' })))
                    self.customsModel.dataproviderActionAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.actionAppArr, { idAttribute: 'srNo' })))
                }
            }, error => {
                app.loading(false);
             })
        };

        self.addBtn = function () {

            if ((self.customsModel.employeeNameVal() == null || self.customsModel.employeeNameVal() == 'undefined') ) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please fill all fields '));
                return;
            }

            if ((self.customsModel.lgTypeVal() == null || self.customsModel.lgTypeVal() == 'undefined') ) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please fill all fields '));
                return;
            }

            else if (self.customsModel.approvalTypeVal() == "LG Action" && (self.customsModel.lgActionVal() == null || self.customsModel.lgActionVal() == 'undefined') ) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please fill all fields '));
                return;
            }

            if (self.customsModel.typeActionVal() == "Create") {
                self.addCreationValidation();
            }
            else {
                self.addApprovalAction();
            }
        };

        self.updateBtn = function () {

            if (self.customsModel.typeActionVal() == "Create" && self.customsModel.approvalTypeVal() == "LG Creation") {
                self.updateCreationValidation();
            }
            else {
                self.updateActionValidation();
            }
        };

        self.deleteBtn = function () {

            if (self.customsModel.typeActionVal() == "Create" && self.customsModel.approvalTypeVal() == "LG Creation") {
                self.deleteCreationVaildation();
            }
            else if (self.customsModel.typeActionVal() == "Create" && self.customsModel.approvalTypeVal() == "LG Action") {
                self.deleteActionVaildation();
            }
            else if (self.customsModel.typeActionVal() == "Approval" && self.customsModel.approvalTypeVal() == "LG Action") {
                self.deleteActionApproval();
            }
            else if (self.customsModel.typeActionVal() == "Approval" && self.customsModel.approvalTypeVal() == "LG Creation") {
                self.deleteCreationApproval();
            }
        };

        self.createAppSelectListener = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected(self.customsModel.createApprovalArr()[[currentRow['rowIndex']]]);
                self.customsModel.employeeNameVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().employeeName : '')
                self.customsModel.typeActionVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().typeAction : '')
                self.customsModel.lgTypeVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().lgType : '')
                self.customsModel.disEmployee(true);
                self.customsModel.disTypeAction(true);
                self.customsModel.deleteDis(false);
                self.customsModel.updateDis(false);
                self.customsModel.addDis(true);
            }
        };

        self.createApprovalSelectListener = function () {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected(self.customsModel.approvalArr()[[currentRow['rowIndex']]]);
                self.customsModel.employeeNameVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().requesterName : '')
                self.customsModel.typeActionVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().typeAction : '')
                self.customsModel.lgTypeVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().lgType : '')
                self.customsModel.approvalLevelVal(self.customsModel.rowSelected() ? self.customsModel.approvalLevelArr().find(el => el.label == self.customsModel.rowSelected().levelApproval).value : '')
                self.customsModel.appEmployeeNameVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().approvalPersonName : '')
                self.customsModel.disEmployee(true);
                self.customsModel.disTypeAction(true);
                self.customsModel.deleteDis(false);
                self.customsModel.addDis(true);
            }
        };

        self.actionValSelectListener = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected(self.customsModel.actionValArr()[[currentRow['rowIndex']]]);
                self.customsModel.employeeNameVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().employeeName : '')
                self.customsModel.typeActionVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().typeAction : '')
                self.customsModel.lgTypeVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().lgType : '')
                self.customsModel.lgActionVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().lgAction : '')
                self.customsModel.disEmployee(true);
                self.customsModel.disTypeAction(true);
                self.customsModel.deleteDis(false);
                self.customsModel.updateDis(false);
                self.customsModel.addDis(true);
            }
        };

        self.actionAppSelectListener = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected(self.customsModel.actionAppArr()[[currentRow['rowIndex']]]);
                self.customsModel.employeeNameVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().requesterName : '')
                self.customsModel.typeActionVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().typeAction : '')
                self.customsModel.lgTypeVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().lgType : '')
                self.customsModel.approvalLevelVal(self.customsModel.rowSelected() ? self.customsModel.approvalLevelArr().find(el => el.label == self.customsModel.rowSelected().levelApproval).value : '')
                self.customsModel.appEmployeeNameVal(self.customsModel.rowSelected() ? self.customsModel.rowSelected().approvalPersonName : '')
                self.customsModel.disEmployee(true);
                self.customsModel.disTypeAction(true);
                self.customsModel.deleteDis(false);
                self.customsModel.updateDis(false);
                self.customsModel.addDis(true);
            }
        };


        self.addCreationValidation = function () {
            // var x = self.customsModel.createApprovalArr().find(el => el.employeeName == self.customsModel.employeeNameVal() && el.lgType == self.customsModel.lgTypeVal())
            // if (x){
            //     console.log(x)
            //     return;
            // }
              
            app.loading(true);
            var empEmail = app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.employeeNameVal())
            var pushedObj = {
                "employeeName": self.customsModel.employeeNameVal(),
                "employeeNumber": app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.employeeNameVal()).PersonNumber,
                "employeeEmail": empEmail.UserName,
                "typeAction": self.customsModel.typeActionVal(),
                "lgType": self.customsModel.lgTypeVal(),
                "lgAction": self.customsModel.lgActionVal() ? self.customsModel.lgActionVal() : 'None'
            }

            services.postGeneric(commonhelper.creationEmployee, pushedObj).then(data => {
                if (data.StatusCode == "OK") {
                    if (self.customsModel.typeActionVal() == "Create" && self.customsModel.approvalTypeVal() == "LG Creation") {
                        self.pushToDataProvider(self.customsModel.createApprovalArr(), data, 'lg creation')
                    }
                    else if (self.customsModel.typeActionVal() == "Create" && self.customsModel.approvalTypeVal() == "LG Action") {
                        self.pushToDataProvider(self.customsModel.actionValArr(), data, 'lg action')
                    }
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been saved '));
                    self.reset();
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };

        self.addApprovalAction = function () {
            app.loading(true);
            var empEmail = app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.employeeNameVal())
            var pushedObj = {
                "requesterName": self.customsModel.employeeNameVal(),
                "requesterNumber": app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.employeeNameVal()).PersonNumber,
                "requesterEmail": empEmail.UserName,
                "approvalPersonName": self.customsModel.appEmployeeNameVal(),
                "approvalPersonNum": app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.appEmployeeNameVal()).PersonNumber,
                "approvalPersonSeq": self.customsModel.approvalLevelVal(),
                "typeAction": self.customsModel.typeActionVal(),
                "lgType": self.customsModel.lgTypeVal(),
                "levelApproval": self.customsModel.approvalLevelArr().find(el => el.value == self.customsModel.approvalLevelVal()).label,
                "lgAction": self.customsModel.lgActionVal() ? self.customsModel.lgActionVal() : 'None'
            }

            services.postGeneric(commonhelper.empApprovalSetup, pushedObj).then(data => {
                if (data.StatusCode == "OK") {
                    if (self.customsModel.typeActionVal() == "Approval" && self.customsModel.approvalTypeVal() == "LG Creation") {
                        self.pushToDataProvider(self.customsModel.approvalArr(), data, 'lg creation approval')
                    }
                    else if (self.customsModel.typeActionVal() == "Approval" && self.customsModel.approvalTypeVal() == "LG Action") {
                        self.pushToDataProvider(self.customsModel.actionAppArr(), data, 'lg action approval')
                    }
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been saved '));
                    self.reset();
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };

        self.updateCreationValidation = function () {
            if (!self.customsModel.rowSelected()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select row to updated '));
                return;
            }
            app.loading(true);
            var pushedObj = {
                "employeeName": self.customsModel.employeeNameVal(),
                "employeeNumber": app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.employeeNameVal()).PersonNumber,
                "employeeEmail": self.customsModel.rowSelected().employeeEmail,
                "typeAction": self.customsModel.typeActionVal(),
                "lgType": self.customsModel.lgTypeVal(),
                "lgAction": self.customsModel.lgActionVal() ? self.customsModel.lgActionVal() : 'None',
                "lgSelectedAction": "None",
                "srNo": self.customsModel.rowSelected().srNo
            }

            services.postGeneric(commonhelper.creationEmployees + "/" + self.customsModel.rowSelected().employeeEmail + "/" + self.customsModel.rowSelected().lgType, pushedObj).then(data => {
                if (data.StatusCode == "OK") {
                    self.pushToDataProviderAfterUpdate(data.Data, self.customsModel.createApprovalArr(), 'update lg creation')
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been updated '));
                    self.reset();
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };

        self.updateActionValidation = function () {
            if (!self.customsModel.rowSelected()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select row to updated '));
                return;
            }

            app.loading(true);
            var pushedObj = {
                "employeeName": self.customsModel.employeeNameVal(),
                "employeeNumber": app.allEmployeesArr().find(el => el.DisplayName == self.customsModel.employeeNameVal()).PersonNumber,
                "employeeEmail": self.customsModel.rowSelected().employeeEmail,
                "typeAction": self.customsModel.typeActionVal(),
                "lgType": self.customsModel.lgTypeVal(),
                "lgAction": self.customsModel.lgActionVal(),
                "lgSelectedAction": self.customsModel.rowSelected().lgAction,
                "srNo": self.customsModel.rowSelected().srNo
            }

            services.postGeneric(commonhelper.creationEmployees + "/" + self.customsModel.rowSelected().employeeEmail + "/" + self.customsModel.rowSelected().lgType, pushedObj).then(data => {
                if (data.StatusCode == "OK") {
                    self.pushToDataProviderAfterUpdate(data.Data, self.customsModel.actionValArr(), 'update lg action')
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been updated '));
                    app.loading(false);
                }
            });
        };

        self.deleteCreationVaildation = function () {
            if (!self.customsModel.rowSelected()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select row to delete '));
                return;
            }
            app.loading(true);
            services.getGeneric(commonhelper.creationEmployees + "/" + self.customsModel.rowSelected().employeeEmail + "/" + self.customsModel.rowSelected().lgType + "/" + "None").then(data => {
                if (data.StatusCode == "OK") {
                    self.pushToDataProviderAfterDelete("delete lg creation");
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been deleted '));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };

        self.deleteActionVaildation = function () {
            if (!self.customsModel.rowSelected()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select row to delete '));
                return;
            }
            app.loading(true);
            services.getGeneric(commonhelper.creationEmployees + "/" + self.customsModel.rowSelected().employeeEmail + "/" + self.customsModel.rowSelected().lgType + "/" + self.customsModel.rowSelected().lgAction).then(data => {
                if (data.StatusCode == "OK") {
                    self.pushToDataProviderAfterDelete("delete lg action");
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been deleted '));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };

        self.deleteActionApproval = function () {
            if (!self.customsModel.rowSelected()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select row to delete '));
                return;
            }
            app.loading(true);
            services.getGeneric(commonhelper.empApprovalSetup + "/" + self.customsModel.rowSelected().requesterName + "/" + self.customsModel.rowSelected().lgType + "/" + self.customsModel.rowSelected().approvalPersonNumber + "/" + self.customsModel.rowSelected().lgAction).then(data => {
                if (data.StatusCode == "OK") {
                    self.pushToDataProviderAfterDelete("delete action approval")
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been deleted '));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };
        self.deleteCreationApproval = function () {
            if (!self.customsModel.rowSelected()) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select row to delete '));
                return;
            }
            app.loading(true);
            services.getGeneric(commonhelper.empApprovalSetup + "/" + self.customsModel.rowSelected().requesterName + "/" + self.customsModel.rowSelected().lgType + "/" + self.customsModel.rowSelected().approvalPersonNumber + "/None").then(data => {
                if (data.StatusCode == "OK") {
                    self.pushToDataProviderAfterDelete("delete creation approval")
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been deleted '));
                    app.loading(false);
                }
            }, error => {
                app.loading(false);
            })
        };


        self.pushToDataProvider = function (returnedArr, data, addType) {
            if (returnedArr.length > 0) {
                var sr = Math.max(...returnedArr.map(el => el.srNo)) + 1;
                [data.Data].forEach(el => el.srNo = sr);
            }
            else {
                data.Data.srNo = 1;
            }

            returnedArr.push(data.Data)

            if (addType == "lg creation")
                self.customsModel.dataproviderCreateAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(returnedArr, { idAttribute: 'srNo' })))
            else if (addType == "lg action")
                self.customsModel.dataproviderActionValScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(returnedArr, { idAttribute: 'srNo' })))
            else if (addType == "lg creation approval")
                self.customsModel.dataproviderApprovalScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(returnedArr, { idAttribute: 'srNo' })))
            else if (addType == "lg action approval")
                self.customsModel.dataproviderActionAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(returnedArr, { idAttribute: 'srNo' })))
        };

        self.pushToDataProviderAfterUpdate = function (returnedObj, tableArr, updateType) {
            if (updateType == "update lg creation") {
                var index = tableArr.findIndex(el => el.employeeEmail == returnedObj.employeeEmail && el.lgType == self.customsModel.rowSelected().lgType)
                tableArr[index] = returnedObj;
                self.customsModel.dataproviderCreateAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(tableArr, { idAttribute: 'srNo' })))
                self.reset();
            }
            else {
                var index = tableArr.findIndex(el => el.employeeEmail == returnedObj.employeeEmail && el.lgType == self.customsModel.rowSelected().lgType)
                tableArr[index] = returnedObj;
                self.customsModel.dataproviderActionValScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(tableArr, { idAttribute: 'srNo' })))
                self.reset();
            }

        };

        self.pushToDataProviderAfterDelete = function (updateType) {
            if (updateType == "delete lg creation") {
                srNo = 0;
                self.customsModel.createApprovalArr(self.customsModel.createApprovalArr().filter(el => !(el.employeeEmail == self.customsModel.rowSelected().employeeEmail && el.lgType == self.customsModel.rowSelected().lgType)))
                self.customsModel.createApprovalArr().forEach(el => el.srNo = ++srNo)
                self.customsModel.dataproviderCreateAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.createApprovalArr, { idAttribute: 'srNo' })))
                self.reset();
            }
            else if(updateType == "delete lg action") {
                srNo = 0;
                self.customsModel.actionValArr(self.customsModel.actionValArr().filter(el => !(el.employeeEmail == self.customsModel.rowSelected().employeeEmail && el.lgType == self.customsModel.rowSelected().lgType && el.lgAction == self.customsModel.rowSelected().lgAction)))
                self.customsModel.actionValArr().forEach(el => el.srNo = ++srNo)
                self.customsModel.dataproviderActionValScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.actionValArr, { idAttribute: 'srNo' })))
                self.reset();
            }

            else if(updateType == "delete action approval"){
                srNo = 0;
                self.customsModel.actionAppArr(self.customsModel.actionAppArr().filter(el => !(el.employeeEmail == self.customsModel.rowSelected().employeeEmail && el.lgType == self.customsModel.rowSelected().lgType && el.approvalPersonName == self.customsModel.rowSelected().approvalPersonName)))
                self.customsModel.actionAppArr().forEach(el => el.srNo = ++srNo)
                self.customsModel.dataproviderActionAppScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.actionAppArr, { idAttribute: 'srNo' })))
                self.reset();
            }

            else if(updateType == "delete creation approval"){
                srNo = 0;
                self.customsModel.approvalArr(self.customsModel.approvalArr().filter(el => !(el.employeeEmail == self.customsModel.rowSelected().employeeEmail && el.lgType == self.customsModel.rowSelected().lgType && el.approvalPersonName == self.customsModel.rowSelected().approvalPersonName)))
                self.customsModel.approvalArr().forEach(el => el.srNo = ++srNo)
                self.customsModel.dataproviderApprovalScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.approvalArr, { idAttribute: 'srNo' })))
                self.reset();
            }
        };


        self.reset = function () {
            self.customsModel.employeeNameVal('')
            self.customsModel.typeActionVal('Create')
            self.customsModel.lgTypeVal('')
            self.customsModel.lgActionVal('')
            self.customsModel.disEmployee(false)
            self.customsModel.disTypeAction(false)
            self.customsModel.appEmployeeNameVal('')
            self.customsModel.approvalTypeVal('LG Creation')
            self.customsModel.approvalLevelVal('')
            self.customsModel.deleteDis(true);
            self.customsModel.updateDis(true);
            self.customsModel.addDis(false);
        };

        self.resetBtn = function () {
            self.reset();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                approvalScreenLbl: getTransaltion("approvalScreen.approvalScreenLbl"),
                typeActionLbl: getTransaltion("approvalScreen.typeActionLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                employeeNameLbl: getTransaltion("approvalScreen.employeeNameLbl"),
                employeeNumLbl: getTransaltion("approvalScreen.employeeNumLbl"),
                approvalLevelLbl: getTransaltion("approvalScreen.approvalLevelLbl"),
                resetLabel: getTransaltion("login.resetLabel"),
                addLbl: getTransaltion("approvalScreen.addLbl"),
                updateLbl: getTransaltion("approvalScreen.updateLbl"),
                deleteLbl: getTransaltion("approvalScreen.deleteLbl"),
                requesterEmpNameLbl: getTransaltion("approvalScreen.requesterEmpNameLbl"),
                requesterEmpNumLbl: getTransaltion("approvalScreen.requesterEmpNumLbl"),
                createApprovalTblLbl: getTransaltion("approvalScreen.createApprovalTblLbl"),
                approvalLbl: getTransaltion("approvalScreen.approvalLbl"),
                creationLbl: getTransaltion("approvalScreen.creationLbl"),
                creationValidationlLbl: getTransaltion("approvalScreen.creationValidationlLbl"),
                appEmployeeNameLbl: getTransaltion("approvalScreen.appEmployeeNameLbl"),
                employeeEmailLbl: getTransaltion("approvalScreen.employeeEmailLbl"),
                appEmployeeNumLbl: getTransaltion("approvalScreen.appEmployeeNumLbl"),
                approvalTypeLbl: getTransaltion("approvalScreen.approvalTypeLbl"),
                lgActionLbl: getTransaltion("approvalScreen.lgActionLbl"),
            })

            self.customsModel.columnArray([
                {
                    "headerText": "NO.", "field": "srNo"
                },
                {
                    "headerText": self.label().employeeNameLbl,
                    "field": "employeeName"
                },
                {
                    "headerText": self.label().employeeEmailLbl,
                    "field": "employeeEmail"
                },
                {
                    "headerText": self.label().typeActionLbl,
                    "field": "typeAction"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                }
            ])

            self.customsModel.approvalColumnArray([
                {
                    "headerText": "NO.", "field": "srNo"
                },
                {
                    "headerText": self.label().requesterEmpNameLbl,
                    "field": "requesterName"
                },
                {
                    "headerText": self.label().typeActionLbl,
                    "field": "typeAction"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().appEmployeeNameLbl,
                    "field": "approvalPersonName"
                },
                {
                    "headerText": self.label().appEmployeeNumLbl,
                    "field": "approvalPersonNumber"
                },
                {
                    "headerText": self.label().approvalLevelLbl,
                    "field": "levelApproval"
                }
            ])

            self.customsModel.actionValColumnArray([
                {
                    "headerText": "NO.", "field": "srNo"
                },
                {
                    "headerText": self.label().employeeNameLbl,
                    "field": "employeeName"
                },
                {
                    "headerText": self.label().employeeEmailLbl,
                    "field": "employeeEmail"
                },
                {
                    "headerText": self.label().typeActionLbl,
                    "field": "typeAction"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().lgActionLbl,
                    "field": "lgAction"
                }
            ])

            self.customsModel.actionAppColumnArray([
                {
                    "headerText": "NO.", "field": "srNo"
                },
                {
                    "headerText": self.label().requesterEmpNameLbl,
                    "field": "requesterName"
                },
                {
                    "headerText": self.label().typeActionLbl,
                    "field": "typeAction"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().lgActionLbl,
                    "field": "lgAction"
                },
                {
                    "headerText": self.label().appEmployeeNameLbl,
                    "field": "approvalPersonName"
                },
                {
                    "headerText": self.label().appEmployeeNumLbl,
                    "field": "approvalPersonNumber"
                },
                {
                    "headerText": self.label().approvalLevelLbl,
                    "field": "levelApproval"
                }
            ])

        }

        initTransaltion();
    }

    return approvalScreenContentViewModel;

})