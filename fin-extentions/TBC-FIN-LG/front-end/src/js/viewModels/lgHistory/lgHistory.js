define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingcontrol'
], function (oj, ko, $, app, services, commonhelper) {
    function lgHistoryContentViewModel() {
        var self = this;
        self.label = ko.observable();

        var retrieved = oj.Router.rootInstance.retrieve();
        self.datacrad = ko.observableArray([]);

        self.customsModel = {
            lgArr: ko.observableArray([]),
            columnArray: ko.observableArray(),
            attachmentColumns: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(retrieved.lgNumFromBank),
            suppilerNameVal: ko.observable(retrieved.supplier),
            contractNoVal: ko.observable(retrieved.contractNumber),
            negotiationNumVal: ko.observable(retrieved.negotiationNumber),
            lgTypeVal: ko.observable(retrieved.lgType),
            lgStatusVal: ko.observable(retrieved.lgStatus),
            dateVal: ko.observable(),
            amountVal: ko.observable(),
            documentNumber: ko.observable(retrieved.documentNumber),
            disableAllFields: ko.observable(true),
            attachmentArr: ko.observableArray(),
            rowSelected: ko.observable(),
            attachtArr: ko.observableArray(),
            rowAttachSelected: ko.observableArray(),
            rowHistorySelected: ko.observableArray(),
            contractNoVis: ko.observable(true),
            negotiationNumVis: ko.observable(true),
        };

        self.dataProviderAttachment = ko.observable()
        self.dataProviderAttachment(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.attachmentArr, { idAttribute: 'id' })));
        self.dataproviderLgScreen = new oj.ArrayDataProvider(self.customsModel.lgArr, { idAttributes: 'id' });

        self.connected = function () {
            app.loading(true);
            self.getAttachment();
            if (retrieved.negotiationNumber != '-') {
                self.customsModel.contractNoVis(false);
                self.customsModel.negotiationNumVis(true);
            }
            else {
                self.customsModel.contractNoVis(true);
                self.customsModel.negotiationNumVis(false);
            }
            services.getGeneric(commonhelper.getLgHistory + retrieved.documentNumber + "/" + retrieved.lgType).then(data => {
                app.loading(false);
                data.Data.forEach(el => { el.lgAmount = numberWithCommas(el.lgAmount), el.actionSeq = parseInt(el.actionSeq)})
                if (data.Data.length > 0) {
                    for (var i in data.Data) {
                        if (data.Data[i].history == 'original') {
                            self.customsModel.dateVal(data.Data[i].lgEndDate);
                            self.customsModel.amountVal(data.Data[i].lgAmount);
                        }
                    }
                    data.Data.sort((a, b) => a.actionSeq - b.actionSeq);
                    self.customsModel.lgArr(data.Data);
                } else {
                    self.customsModel.dateVal(retrieved.lgEndDate);
                    self.customsModel.amountVal(retrieved.lgAmount);
                }
            }, error => {
                app.loading(false);
            });
        };

        self.getAttachment = function () {
            services.getGeneric(commonhelper.getAttachment + retrieved.documentNumber + "/" + retrieved.lgType).then(data => {
                if (data.Data.length > 0) {
                    var srNo = 1;
                    data.Data.forEach( el =>  el.srNo = srNo++);
                    self.customsModel.attachtArr(data.Data);

                }
            }, error => { });
        };

        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            if (e.target.type == 'button') {
                var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
                self.customsModel.rowSelected(self.customsModel.lgArr()[index])
                if (self.customsModel.attachtArr() && self.customsModel.rowSelected()) {
                    var x = self.customsModel.attachtArr().filter(e => e.attachtNum === self.customsModel.rowSelected().attachtNum)
                    self.customsModel.attachmentArr(x);
                    self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr, { idAttributes: 'id' }))
                    document.querySelector("#viewAttachment").open();
                }
            }
        });

        self.lgHistorySelectListener = function (event){
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowHistorySelected(self.customsModel.lgArr()[[currentRow['rowIndex']]]);
            }
        };

        self.lgAttachmentselectListener = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelected(self.customsModel.attachmentArr()[[currentRow['rowIndex']]]);
            }
        };

        self.DownloadFileFn = function (event) {
            download(self.customsModel.rowAttachSelected().title, self.customsModel.rowAttachSelected().content);
        };

        self.approvalListBtn = function () {
            if(self.customsModel.rowHistorySelected().operation == 'None'){
                app.messagesDataProvider.push(app.createMessage('error', 'No Approval list for None Action'));
                return;
            }

            if(self.customsModel.rowHistorySelected().length == 0){
                app.messagesDataProvider.push(app.createMessage('error', 'Please select table item'));
                return;
            }

            app.loading(true);
            var payload = {
                "documentNumber": self.customsModel.rowHistorySelected().documentNumber.toString(),
                "lgType": self.customsModel.rowHistorySelected().lgType,
                "lgActionSeq": self.customsModel.rowHistorySelected().actionSeq,
                "lgAction": self.customsModel.rowHistorySelected().operation,
            }
            self.datacrad([]);
            services.postGeneric(commonhelper.workFlowListAction, payload).then(data => {
                data.Data.sort(function (a, b){ return parseInt(a.approvalPersonSeq) - parseInt(b.approvalPersonSeq) })
                data.Data.forEach(el => {
                    if (el.approvalStatus == "APPROVED") {
                        cardStatus = 'badge-success'
                        bodyCardStatus = 'app-crd-bdy-border-suc'
                    }
                    else if (el.approvalStatus == "REJECT") {
                        cardStatus = 'badge-danger';
                        bodyCardStatus = 'app-crd-bdy-border-fail';
                    }
                    else if (el.approvalStatus == "Pending Approval") {
                        cardStatus = 'badge-warning';
                        bodyCardStatus = 'app-crd-bdy-border-pen';
                    }
                    self.datacrad.push({
                        personName: el.approvalPersonName,
                        FYA: "",
                        Approved: el.approvalStatus,
                        approvalLevel: el.approvalLevel,
                        date: "2020-05-21",
                        status: "",
                        bodyStatus: bodyCardStatus,
                        appCardStatus: cardStatus,
                        rejectReason: ""
                    })
                })
                app.loading(false);
                document.querySelector("#approvalDialog").open();
            }, error => {
                app.loading(false);
            })
        };

        function download(filename, text) {
            var link = document.createElement("a");
            link.download = filename;
            link.href = text;
            link.click();
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };

        self.backBtn = function () {
            oj.Router.rootInstance.go('lgScreen');
        };

        self.cancelviewAttDial = function () {
            document.querySelector("#viewAttachment").close();
        };

        self.approvalDialogCloseAction = function () {
            document.querySelector("#approvalDialog").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                suppilerNameLbl: getTransaltion("lgScreen.suppilerNameLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgStatusLbl: getTransaltion("lgScreen.lgStatusLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                bankAmountLbl: getTransaltion("lgScreen.bankAmountLbl"),
                amountLbl: getTransaltion("lgScreen.amountLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                status: getTransaltion("lgScreen.status"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                modifiedDateLbl: getTransaltion("lgScreen.modifiedDateLbl"),
                dateLbl: getTransaltion("lgScreen.dateLbl"),
                backLbl: getTransaltion("common.backLbl"),
                operationLbl: getTransaltion("lgScreen.operationLbl"),
                historyScreenLbl: getTransaltion("lgScreen.historyScreenLbl"),
                viewAttachLbl: getTransaltion("lgScreen.viewAttachLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                viewAttachmentLbl: getTransaltion("common.viewAttachmentLbl"),
                titleLbl: getTransaltion("lgOperation.titleLbl"),
                nameLbl: getTransaltion("lgOperation.nameLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                downloadLbl2: getTransaltion("common.downloadLbl"),
                negotiationNumLbl: getTransaltion("lgOperation.negotiationNumLbl"),
                lgActionStatusLbl: getTransaltion("approvalScreen.lgActionStatusLbl"),
                lgActionAppListLbl: getTransaltion("approvalScreen.lgActionAppListLbl"),
                approvalListLbl: getTransaltion("approvalScreen.approvalListLbl"),
                okLbl: getTransaltion("common.okLbl"),
            });

            self.customsModel.columnArray([
                {
                    "headerText": 'NO.',
                    "template": 'seqTemplate'
                },
                {
                    "headerText": self.label().documentNumberLbl,
                    "field": "documentNumber"
                },
                {
                    "headerText": self.label().operationLbl,
                    "field": "operation"
                },
                {
                    "headerText": self.label().amountLbl,
                    "field": "lgAmount"
                },
                {
                    "headerText": self.label().lgEndDateLbl,
                    "field": "lgEndDate"
                },
                {
                    "headerText": self.label().status,
                    "field": "lgStatus"
                },
                {
                    "headerText": self.label().modifiedDateLbl,
                    "field": "modDate"
                },
                {
                    "headerText": self.label().lgActionStatusLbl,
                    "field": "lgActionStatus"
                },
                {
                    "headerText": self.label().attachmentLbl,
                    "template": 'attachBtn'
                }
            ]);

            self.customsModel.attachmentColumns([
                {
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                },
                {
                    "headerText": self.label().downloadLbl2,
                    "template": 'downTemplate'
                },
            ])
        }
        initTransaltion();
    }

    return lgHistoryContentViewModel;
});