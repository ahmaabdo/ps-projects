define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker'
], function (oj, ko, $, app, services, commonhelper) {
    function lgEditScreenContentViewModel() {
        var self = this;

        self.label = ko.observable();
        var date = formatDate(new Date());
        var retrieved = oj.Router.rootInstance.retrieve();
        var payload;
        var diff;

        self.customsModel = {
            retrieved: ko.observable(retrieved),
            lgArr: ko.observableArray([]),
            attachmentArr: ko.observableArray([]),
            attachmentColumns: ko.observableArray(),
            columnArray: ko.observableArray(),
            description: ko.observable(""),
            bankAccountArr: ko.observableArray(),
            contractNumberArr: ko.observableArray([]),
            lgNumberFromBankVal: ko.observable(),
            supplierVal: ko.observable(),
            contractNoVal: ko.observable(),
            lgTypeVal: ko.observable(),
            lgType: ko.observable(),
            contractAmountVal: ko.observable(),
            contractStartDateVal: ko.observable(),
            contractEndDateVal: ko.observable(),
            letterOfGuarnAmoutVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            regionVal: ko.observable(),
            documentNumber: ko.observable(),
            bankAccountVal: ko.observable(),
            closeToExp: ko.observable(),
            disableAllFields: ko.observable(true),
            addFileDis: ko.observable(false),
            submitDisabled: ko.observable(true),
            disableIncDec: ko.observable(true),
            disableExtend: ko.observable(true),
            disableField: ko.observable(true),
            editVisible: ko.observable(false),
            attachVisible: ko.observable(false),
            viewSelect: ko.observable(true),
            selected_files: ko.observableArray(),
            fileData: ko.observableArray(),
            contentData: ko.observable(),
            data: ko.observableArray([]),
            viewVisible: ko.observable(true),
            viewExpVisible: ko.observable(false),
            contractsVis: ko.observable(false),
            negotiationAmountVis: ko.observable(false),
            contractAmountVis: ko.observable(false),
            negotiationNumVis: ko.observable(false),
            negotiationVis: ko.observable(false),
            contractNumUsed: ko.observableArray([]),
            letterIncVal: ko.observable(),
            letterDecVal: ko.observable(),
            minDateVal: ko.observable(),
            fileTrue: ko.observable(false),
            attTitle: ko.observable(),
            attName: ko.observable(),
            attdocNum: ko.observable(),
            payloadAttach: ko.observableArray([]),
            updateVisible: ko.observable(true),
            updateDisabled: ko.observable(false),
            contractNoDisable: ko.observable(false),
            bankAccountArr: ko.observable(),
            selectedItemsAttachment: ko.observableArray([]),
            getAttachmentArr: ko.observableArray([]),
            negotiationNumVal: ko.observable(retrieved.negotiationNumber),
            negotiationArr: ko.observableArray([]),
            negotiationTitleVal: ko.observable(),
            negotiationAmountLbl: ko.observable("Negotiation Amount"),
            lgAmountPercentageVal: ko.observable(),
            lgStartDateVal: ko.observable(),
            maxAmountPerc: ko.observable(),
            minAmountPerc: ko.observable(),
            actionSeq: ko.observable(),
            viewAttachmentDis: ko.observable(true),
        };

        self.dataproviderLgScreen = new oj.ArrayDataProvider(self.customsModel.lgArr, { idAttributes: 'id' });
        self.dataProviderAttachment = new oj.ArrayTableDataSource(self.customsModel.getAttachmentArr, { idAttributes: 'id' });

        self.dataProviderAttachment2 = ko.observable()
        self.dataProviderAttachment2(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))

        ko.computed(_ => {
            if (app.manageBankAccountArr()) {
                var bankAccValue = app.manageBankAccountArr().map(el => ({ label: el.BANK_NAME, value: el.BANK_NAME }));
                self.customsModel.bankAccountArr(bankAccValue);
            }
            if (app.agreementArr().length > 0) {
                self.customsModel.contractNoVal(retrieved.contractNumber);
            }
        });

        self.connected = function () {
            self.getAttachment();
            self.getLastRowHistory(retrieved.documentNumber);
            self.customsModel.documentNumber(retrieved.documentNumber);
            self.customsModel.lgNumberFromBankVal(retrieved.lgNumFromBank);
            self.customsModel.supplierVal(retrieved.supplier);
            self.customsModel.contractNoVal(retrieved.contractNumber);
            self.customsModel.lgTypeVal(retrieved.lgType);
            self.customsModel.contractAmountVal(retrieved.contractAmount != '-' ? retrieved.contractAmount : retrieved.negotiationAmount);
            self.customsModel.contractStartDateVal(retrieved.contractStartDate);
            self.customsModel.contractEndDateVal(retrieved.contractEndDate);
            self.customsModel.letterOfGuarnAmoutVal(parseInt(retrieved.lgAmount.toString().replace(/,/g, "")));
            self.customsModel.lgStartDateVal(retrieved.lgStartDate)
            self.customsModel.lgEndDateVal(retrieved.lgEndDate);
            self.customsModel.regionVal(retrieved.region);
            self.customsModel.bankAccountVal(retrieved.bankAccount);
            self.customsModel.description(retrieved.description);
            self.customsModel.negotiationTitleVal(retrieved.negotiationTitle);
            self.customsModel.negotiationNumVal(retrieved.negotiationNumber);
            self.customsModel.lgAmountPercentageVal(parseInt(retrieved.lgAmountPercnatge));
            diff = date_diff_indays(date, self.customsModel.lgEndDateVal());

            self.customsModel.closeToExp(diff);
            if (retrieved.negotiationNumber != '-') {
                self.customsModel.negotiationVis(true);
                self.customsModel.contractsVis(false);
                self.customsModel.negotiationAmountVis(true);
                self.customsModel.negotiationNumVis(true);
            }
            else {
                self.customsModel.negotiationVis(false);
                self.customsModel.contractsVis(true);
                self.customsModel.negotiationAmountVis(false);
                self.customsModel.negotiationNumVis(false);
                self.customsModel.negotiationAmountLbl("Contract Amount");
            }
            if (retrieved.type == "edit") {
                self.getContractNumber();
            } else if (retrieved.type == "Increase") {
                self.customsModel.minAmountPerc(parseInt(retrieved.lgAmountPercnatge))
                self.customsModel.letterIncVal(parseInt(retrieved.lgAmount.toString().replace(/,/g, "")));
            } else if (retrieved.type == "Decrease") {
                self.customsModel.maxAmountPerc(parseInt(retrieved.lgAmountPercnatge))
                self.customsModel.letterDecVal(parseInt(retrieved.lgAmount.toString().replace(/,/g, "")));
            } else if (retrieved.type == "Extend") {
                self.customsModel.minDateVal(self.customsModel.lgEndDateVal())
            } else if (retrieved.view) {
                self.lgEditScreenLbl = 'View LG Screen';
                self.customsModel.updateVisible(false);
                self.customsModel.contractNoDisable(true);
                self.customsModel.attachVisible(true);
                self.customsModel.viewExpVisible(true);
            }
        };

        self.getContractNumber = function () {
            app.loading(true);
            services.getGeneric(commonhelper.getLg).then(data => {
                app.loading(false);
                if (data) {
                    var contractNumArr = [...new Set(data.Data.map(el => el.contractNumber))];
                    self.customsModel.contractNumUsed(contractNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.contractNumUsed(self.customsModel.contractNumUsed().filter(el => el.value != retrieved.contractNumber))
                    self.customsModel.contractNumUsed().forEach(e => {
                        self.customsModel.contractNumberArr(self.customsModel.contractNumberArr().filter(el => el.value != e.value));
                    });
                }
            }, error => {
                app.loading(false);
            });
        };

        self.negotiationListener = function (event) {
            self.customsModel.negotiationNumVal(event['detail'].value);
            self.customsModel.letterOfGuarnAmoutVal(0);
            if (self.customsModel.negotiationNumVal()) {
                if (app.supplierNegoArr()) {
                    var negotiationObj = app.supplierNegoArr().find(el => el.DOCUMENT_NUMBER == self.customsModel.negotiationNumVal())
                    if (negotiationObj) {
                        negotiationObj.AUCTION_TITLE ? self.customsModel.negotiationTitleVal(negotiationObj.AUCTION_TITLE) : self.customsModel.negotiationTitleVal("No data found");
                        negotiationObj.Items.PARTY_NAME ? self.customsModel.supplierVal(negotiationObj.Items.PARTY_NAME) : self.customsModel.supplierVal("No data found");
                        negotiationObj.Items.BUYER_BID_TOTAL ? self.customsModel.contractAmountVal(negotiationObj.Items.BUYER_BID_TOTAL) : self.customsModel.contractAmountVal("No data found");
                        self.customsModel.letterIncVal(self.customsModel.contractAmountVal().toString().replace(/,/g, "") * 1 / 100);
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal().toString().replace(/,/g, "") * 1 / 100);
                    }
                }
            }
        };

        self.updateBtn = function () {
            self.customsModel.contractNoVal(retrieved.contractNumber);
            var operation;
            var lgStatus;
            if (lgStatus == "Rejected")
                operation = "Undo";
        
            self.customsModel.submitDisabled(false);
            payload = [{
                documentNumber: self.customsModel.documentNumber(),
                lgNumFromBank: self.customsModel.lgNumberFromBankVal(),
                lgType: self.customsModel.lgTypeVal(),
                lgEndDate: self.customsModel.lgEndDateVal(),
                lgAmount: self.customsModel.letterOfGuarnAmoutVal().toString(),
                lgStatus: retrieved.lgStatus,
                createBy: app.personNumber(),
                createByName: app.personName(),
                lgActionType : retrieved.type,
                lgActionStatus: "Pending", 
                operation: retrieved.type,
                actionSeq: self.customsModel.actionSeq().toString(),
                Attachments: self.customsModel.attachmentArr(),
                closeToExp: self.customsModel.closeToExp()
            }];
            if (self.customsModel.negotiationTitleVal() != "-" && self.customsModel.lgTypeVal() == 'Bid bond') {
                payload[0].negotiationAmount = self.customsModel.contractAmountVal().toString().replace(/,/g, "")
                payload[0].contractAmount = ""
            }
            else if (self.customsModel.contractNoVal() != "-" && self.customsModel.lgTypeVal() == 'Bid bond') {
                payload[0].contractAmount = self.customsModel.contractAmountVal().toString().replace(/,/g, "")
                payload[0].negotiationAmount = ""
            }

            self.customsModel.lgArr(payload);
            if (lgStatus == "Rejected")
                self.addHistory();

            self.customsModel.updateDisabled(true)
        };

        self.submitBtn = function () {
            app.loading(true);
            if (retrieved.type == "edit") {
                services.postGeneric(commonhelper.editLg, payload).then(data => {
                    app.loading(false);
                    if (data) {
                        // services.postGeneric(commonhelper.updateStatus, payload[0]);
                        if (!self.customsModel.fileTrue()) {
                        } else {
                            for (var i in self.customsModel.selected_files()) {
                                services.getGeneric(commonhelper.deleteAttachment + self.customsModel.selected_files()[i].id).then(data => {
                                }, error => { });
                            }
                        }
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been updated '));
                        oj.Router.rootInstance.go('lgScreen');
                    }
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been updated '));
                });
            } else {
                self.lgActionApproval()
            }
        };

        self.lgActionApproval = function () {
            services.postGeneric(commonhelper.lgActionApproval , payload).then (data => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been saved'));
                oj.Router.rootInstance.go('lgScreen');
            } , error => {
                app.loading(false);
            })
        };

        self.cancelActionBtn = function () {

        };

        self.getLastRowHistory = function (docNum) {
            services.getGeneric(commonhelper.getLastHistory + docNum).then(data => {
                self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1)
            }, error => {
            })
        };

        self.lgTypeListener = function (event) {
            self.customsModel.lgType(event['detail'].value);
            if (self.customsModel.contractNoVal()) {
                self.customsModel.letterOfGuarnAmoutVal(0);

                if (self.customsModel.contractAmountVal() != "No data found") {
                    if (self.customsModel.lgType() == "Bid bond")
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal() * 1 / 100);

                    if (self.customsModel.lgType() == "Final")
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal() * 5 / 100);
                }
            }
        };

        self.lgAmountPercantgeListener = function (event) {
            self.customsModel.lgAmountPercentageVal(event['detail'].value)
            self.customsModel.letterOfGuarnAmoutVal((self.customsModel.lgAmountPercentageVal() / 100) * self.customsModel.contractAmountVal().toString().replace(/,/g, ""))
        }


        function download(filename, text) {
            var link = document.createElement("a");
            link.download = filename;
            link.href = text;
            link.click();
        }

        self.getAttachment = function () {
            services.getGeneric(commonhelper.getAttachment + retrieved.documentNumber + "/" + retrieved.lgType).then(data => {
                if (data.Data.length > 0) {
                    self.customsModel.viewAttachmentDis(false);
                    self.customsModel.fileTrue(true);
                    self.customsModel.getAttachmentArr(data.Data);
                    self.customsModel.selected_files(data.Data);
                }
            }, error => { });
        }

        self.lgAttachmentselectListener = function (event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelected = self.customsModel.getAttachmentArr()[[currentRow['rowIndex']]];
            }
            // if (retrieved.view)
            // download(self.customsModel.rowAttachSelected.title, self.customsModel.rowAttachSelected.content);
        };

        self.DownloadFileFn = function (event) {
            download(self.customsModel.rowAttachSelected.title, self.customsModel.rowAttachSelected.content);
        };

        self.addFileSelectListener = function (event) {
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                self.customsModel.attdocNum(self.customsModel.documentNumber())
                var x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "docNum": self.customsModel.attdocNum(),
                    "content": self.customsModel.contentData(),
                    "attach_type": retrieved.type,
                    "id": 0
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name)
                            self.customsModel.payloadAttach().push(x);
                        x = {};
                        self.customsModel.attachmentArr(self.customsModel.payloadAttach());
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
        };

        self.removePickerBtnAction = function (event) {
            if (document.querySelector("oj-file-picker input"))
                document.querySelector("oj-file-picker input").value = null;

            self.customsModel.selected_files([]);
            self.customsModel.addFileDis(false);
        };

        self.removeSelectedAttachment = function () {
            if (self.customsModel.selectedItemsAttachment().length < 1)
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));

            var x = self.customsModel.attachmentArr().findIndex((el => el.name == self.customsModel.selectedItemsAttachment()[0][1]))
            self.customsModel.attachmentArr().splice(x, 1);
            self.dataProviderAttachment2(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))
        };

        self.deleteAttach = function () {
            var selected = self.customsModel.rowAttachSelected;
            app.loading(true);
            services.getGeneric(commonhelper.deleteAttachment + selected.id).then(data => {
                self.customsModel.attachmentArr([])
                self.getAttachment();
                app.loading(false);
                document.querySelector("#viewAttachment").close();
            }, error => {
                app.loading(false);
            });
        };

        function addDays(date, days) {
            const date2 = new Date(date);
            const copy = new Date(Number(date2))
            copy.setDate(date2.getDate() + days)
            return copy
        }

        self.endDateValueChanged = function (event) {

        }

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function (date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        };

        self.viewContarctAttachment = function (event) {
            document.querySelector("#viewAttachment").open();
        };

        self.noCancelActionBtn = function () {
            document.querySelector("#confirmCancel").close();
        }

        self.backBtn = function () {
            oj.Router.rootInstance.go('lgScreen');
        };

        self.cancelviewAttDial = function () {
            document.querySelector("#viewAttachment").close();
        };

        self.cancelStatusBtn = function () {
            document.querySelector("#confirmCancel").open();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                contractStartDateLbl: getTransaltion("lgOperation.contractStartDateLbl"),
                contractEndDateLbl: getTransaltion("lgOperation.contractEndDateLbl"),
                letterOfGuarnAmoutLbl: getTransaltion("lgOperation.letterOfGuarnAmoutLbl"),
                contractAmountLbl: getTransaltion("lgOperation.contractAmountLbl"),
                supplierLbl: getTransaltion("lgOperation.supplierLbl"),
                regionLbl: getTransaltion("lgOperation.regionLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                uploadLbl: getTransaltion("common.uploadLbl"),
                descriptionLbl: getTransaltion("lgOperation.descriptionLbl"),
                closeToExpLbl: getTransaltion("common.closeToExpLbl"),
                submitLbl: getTransaltion("common.submitLbl"),
                backLbl: getTransaltion("common.backLbl"),
                addFileLbl: getTransaltion("common.addFileLbl"),
                viewAttachmentLbl: getTransaltion("common.viewAttachmentLbl"),
                titleLbl: getTransaltion("lgOperation.titleLbl"),
                nameLbl: getTransaltion("lgOperation.nameLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                editLbl: getTransaltion("common.editLbl"),
                downloadLbl: getTransaltion("common.downloadLbl"),
                deleteLbl: getTransaltion("common.deleteLbl"),
                formatLbl: getTransaltion("lgOperation.formatLbl"),
                confirmCancelLbl: getTransaltion("lgOperation.confirmCancelLbl"),
                confirmCancelTextLbl: getTransaltion("lgOperation.confirmCancelTextLbl"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                lgEditScreenLbl: getTransaltion("lgOperation.lgEditScreenLbl"),
                negotiationNumLbl: getTransaltion("lgOperation.negotiationNumLbl"),
                negotiationTitleLbl: getTransaltion("lgOperation.negotiationTitleLbl"),
                negotiationAmountLbl: getTransaltion("lgOperation.negotiationAmountLbl"),
                lgAmountPercantgeLbl: getTransaltion("lgOperation.lgAmountPercantgeLbl"),
                lgStartDateLbl: getTransaltion("lgOperation.lgStartDateLbl"),
            });
            self.customsModel.columnArray([
                {
                    "headerText": self.label().documentNumberLbl,
                    "field": "documentNumber"
                },
                {
                    "headerText": self.label().lgNumberFromBankLbl,
                    "field": "lgNumFromBank"
                },
                {
                    "headerText": self.label().contractNoLbl,
                    "field": "contractNumber"
                },
                {
                    "headerText": self.label().contractAmountLbl,
                    "field": "contractAmount"
                },
                {
                    "headerText": self.label().negotiationTitleLbl,
                    "field": "negotiationTitle"
                },
                {
                    "headerText": self.label().negotiationNumLbl,
                    "field": "negotiationNumber"
                },
                {
                    "headerText": self.label().negotiationAmountLbl,
                    "field": "negotiationAmount"
                },
                {
                    "headerText": self.label().supplierLbl,
                    "field": "supplier"
                },
                {
                    "headerText": self.label().letterOfGuarnAmoutLbl,
                    "field": "lgAmount"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().lgEndDateLbl,
                    "field": "lgEndDate"
                },
                {
                    "headerText": self.label().bankAccountLbl,
                    "field": "bankAccount"
                },
                {
                    "headerText": self.label().closeToExpLbl,
                    "field": "closeToExp"
                }
            ]);
            self.customsModel.attachmentColumns([{
                "headerText": self.label().titleLbl,
                "field": "title"
            },
            {
                "headerText": self.label().nameLbl,
                "field": "name"
            },
            {
                "headerText": self.label().downloadLbl,
                "template": 'downTemplate'
            },
            {
                "headerText": self.label().deleteLbl,
                "template": 'cellTemplate'
            }
            ]);
        }
        initTransaltion();
    }

    return lgEditScreenContentViewModel;
});