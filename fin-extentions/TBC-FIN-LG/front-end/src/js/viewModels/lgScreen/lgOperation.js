define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper) {
    function lgOperationContentViewModel() {
        var self = this;
        self.label = ko.observable();

        self.customsModel = {
            lgArr: ko.observableArray([]),
            attachmentArr: ko.observableArray([]),
            attachmentColumns: ko.observableArray(),
            columnArray: ko.observableArray(),
            description: ko.observable(""),
            bankAmountArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(),
            supplierVal: ko.observable(),
            contractNoVal: ko.observable(),
            lgTypeVal: ko.observable(),
            contractAmountVal: ko.observable(),
            contractStartDateVal: ko.observable(),
            contractEndDateVal: ko.observable(),
            letterOfGuarnAmoutVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            regionVal: ko.observable('Headquarter'),
            documentNumber: ko.observable(),
            bankAccountVal: ko.observable(),
            closeToExp: ko.observable(),
            disableAllFields: ko.observable(false),
            addFileDis: ko.observable(false),
            createLg: ko.observable(false),
            sumbitLg: ko.observable(true),
            disableAdvance: ko.observable(true),
            negotiationVis: ko.observable(false),
            contractsVis: ko.observable(false),
            negotiationAmountVis: ko.observable(false),
            contractAmountVis: ko.observable(false),
            negotiationNumVis: ko.observable(false),
            selected_files: ko.observableArray(),
            fileData: ko.observable(),
            contentData: ko.observable(),
            data: ko.observableArray([]),
            contractNum: ko.observable(),
            letterIncVal: ko.observable(0),
            lgType: ko.observable(),
            bankAccountArr: ko.observable(),
            contractNumUsed: ko.observableArray([]),
            attTitle: ko.observable(),
            attName: ko.observable(),
            attdocNum: ko.observable(),
            selectedItemsAttachment: ko.observableArray([]),
            payloadAttach: ko.observableArray([]),
            groupValid: ko.observable(),
            lgStartDateVal: ko.observable(),
            lgTypeArr: ko.observableArray([]),
            fromStartDate: ko.observable(),
            negotiationNumVal: ko.observable(),
            negotiationArr: ko.observableArray([]),
            negoSupplierArr: ko.observableArray([]),
            negotiationTitleVal: ko.observable(),
            negotiationAmountLbl: ko.observable("Negotiation Amount"),
            lgAmountPercentageVal: ko.observable(),
            getAllLgArr: ko.observable(),
            negoObject: ko.observable(),
            negoSupplierVal: ko.observable(),
            disableDocumentNum: ko.observable(true),
            supplierContarctDis: ko.observable(true),
            contractAmountTemp: ko.observable(),
        };

        var payload;
        var personNumber;
        var personName;
        var bidArr = [];
        var finalArr = [];
        var advanceArr = [];

        self.dataproviderLgScreen = new oj.ArrayDataProvider(self.customsModel.lgArr, { idAttributes: 'id' });
        self.dataProviderAttachment = ko.observable()
        self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))

        self.connected = function () {
            var retrieved = oj.Router.rootInstance.retrieve();
            self.customsModel.lgTypeArr(retrieved.map(el => ({label : el.lgType , value : el.lgType})))

            self.getContractNumber();

            if (app.supplierNegoArr() && self.customsModel.lgTypeVal() == 'Bid bond') {
                self.customsModel.negotiationVis(true);
                self.customsModel.contractsVis(false);
                self.customsModel.negotiationAmountVis(true);
                self.customsModel.negotiationNumVis(true);
                self.customsModel.lgAmountPercentageVal(1);
            }
        };

        ko.computed(_ => {
            if (app.personNumber() && app.personName()) {
                personNumber = app.personNumberAdmin();
                personName = app.personNameAdmin();
            };
            if (app.manageBankAccountArr()) {
                var bankAccValue = app.manageBankAccountArr().map(el => ({ label: el.BANK_NAME, value: el.BANK_NAME }));
                self.customsModel.bankAccountArr(bankAccValue);
            } 
        });

        self.getContractNumber = function () { // call when to validate to contract or negotiation
            app.loading(true);
            self.customsModel.getAllLgArr([])
            services.getGeneric(commonhelper.getLg).then(data => {
                if (data) {
                    if (app.agreementArrValue()) {
                        // self.customsModel.contractNumberArr(app.agreementArrValue().filter(el => !data.Data.find(e => el.value == e.contractNumber)))
                        // self.customsModel.negotiationArr(app.supplierNegoArrValue().filter(el => !data.Data.find(e => el.value == e.negotiationNumber)))
                        self.customsModel.contractNumberArr(app.agreementArrValue())
                        self.customsModel.negotiationArr(app.supplierNegoArrValue())

                        self.customsModel.getAllLgArr(data.Data)
                        setTimeout(function () {
                            bidArr = self.customsModel.getAllLgArr().filter(el => el.lgType == "Bid bond").map(e => e.documentNumber)
                            finalArr = self.customsModel.getAllLgArr().filter(el => el.lgType == "Final").map(e => e.documentNumber)
                            advanceArr = self.customsModel.getAllLgArr().filter(el => el.lgType == "Advance").map(e => e.documentNumber)

                            if (bidArr.length != 0 && self.customsModel.lgTypeVal() == "Bid bond") {
                                bidSeq = Math.max(...bidArr) + 1;
                                self.customsModel.documentNumber(bidSeq);
                            }

                            else if (finalArr.length != 0 && self.customsModel.lgTypeVal() == "Final") {
                                FinalSeq = Math.max(...finalArr) + 1;
                                self.customsModel.documentNumber(FinalSeq);
                            }

                            else if (advanceArr.length != 0 && self.customsModel.lgTypeVal() == "Advance") {
                                AdvSeq = Math.max(...advanceArr) + 1;
                                self.customsModel.documentNumber(AdvSeq);
                            }

                            else {
                                self.customsModel.documentNumber(1);
                            }

                            app.loading(false);
                        }, 1000);
                    }
                }
            }, error => {
                app.loading(false);
            });
        };

        self.contactNumberListener = function (event) {
            self.customsModel.contractNum(event['detail'].value);
            self.customsModel.letterOfGuarnAmoutVal(0);
            if (self.customsModel.contractNum()) {
                if (app.agreementArr()) {
                    var agreementObj = app.agreementArr().find(el => el.AGREEMENTNUMBER == self.customsModel.contractNum())
                    if (agreementObj) {
                        agreementObj.AGREEMENTAMOUNT ? self.customsModel.contractAmountVal(numberWithCommas(agreementObj.AGREEMENTAMOUNT)) : self.customsModel.contractAmountVal(0);
                        agreementObj.AGREEMENTSTARTDATE ? self.customsModel.contractStartDateVal(agreementObj.AGREEMENTSTARTDATE.split("T")[0]) : self.customsModel.contractStartDateVal("No data found");
                        agreementObj.AGREEMENTENDDATE ? self.customsModel.contractEndDateVal(agreementObj.AGREEMENTENDDATE.split("T")[0]) : self.customsModel.contractEndDateVal("No data found");
                        agreementObj.SUPPLIER_NAME ? self.customsModel.supplierVal(agreementObj.SUPPLIER_NAME) : self.customsModel.supplierVal("No data found");
                        if (self.customsModel.lgTypeVal() == "Final") {
                            self.customsModel.letterIncVal(self.customsModel.contractAmountVal().toString().replace(/,/g, "") * self.customsModel.lgAmountPercentageVal() / 100);
                            self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal().toString().replace(/,/g, "") * self.customsModel.lgAmountPercentageVal() / 100);
                        }
                    }
                }
            }
        };

        self.negotiationListener = function (event) {
            self.customsModel.negotiationNumVal(event['detail'].value);
            self.customsModel.letterOfGuarnAmoutVal(0);
            if (self.customsModel.negotiationNumVal()) {
                if (app.supplierNegoArr()) {
                    var negotiationObj = app.supplierNegoArr().find(el => el.DOCUMENT_NUMBER == self.customsModel.negotiationNumVal())
                    self.customsModel.negoObject(negotiationObj);
                    if (negotiationObj) {
                        negotiationObj.AUCTION_TITLE ? self.customsModel.negotiationTitleVal(negotiationObj.AUCTION_TITLE) : self.customsModel.negotiationTitleVal("No data found");
                        Array.isArray(negotiationObj.Items) ? self.customsModel.negoSupplierArr(negotiationObj.Items.map(el => ({ label: el.PARTY_NAME, value: el.PARTY_NAME }))) : self.customsModel.negoSupplierArr([negotiationObj.Items].map(el => ({ label: el.PARTY_NAME, value: el.PARTY_NAME })));
                        self.customsModel.letterIncVal(0);
                        // self.customsModel.letterIncVal(self.customsModel.contractAmountVal().toString().replace(/,/g, "") * self.customsModel.lgAmountPercentageVal() / 100);
                    }
                }
            }
        };

        self.lgTypeListener = function (event) {
            self.customsModel.lgType(event['detail'].value);
            self.customsModel.negotiationTitleVal('');
            self.customsModel.contractAmountVal('');
            self.customsModel.supplierVal('');
            self.customsModel.negotiationNumVal('');
            self.customsModel.letterOfGuarnAmoutVal(0);
            self.customsModel.contractNoVal('');

            if (self.customsModel.lgType() == "Bid bond") {
                self.customsModel.negotiationVis(true);
                self.customsModel.contractsVis(false);
                self.customsModel.negotiationAmountVis(true);
                self.customsModel.contractAmountVis(false);
                self.customsModel.negotiationNumVis(true);
                self.customsModel.negotiationAmountLbl("Negotiation Amount")
                self.customsModel.lgAmountPercentageVal(1);
                self.customsModel.documentNumber(bidArr != 0 ? Math.max(...bidArr) + 1 : 1);
            }
            if (self.customsModel.lgType() == "Final") {
                self.customsModel.negotiationVis(false);
                self.customsModel.contractsVis(true);
                self.customsModel.negotiationAmountVis(false);
                self.customsModel.contractAmountVis(true);
                self.customsModel.negotiationNumVis(false);
                self.customsModel.negotiationAmountLbl("Contract Amount")
                self.customsModel.lgAmountPercentageVal(5);
                self.customsModel.documentNumber(finalArr.length != 0 ? Math.max(...finalArr) + 1 : 1);
            }

            if (self.customsModel.lgType() == "Advance") {
                self.customsModel.negotiationAmountLbl("Contract Amount")
                self.customsModel.disableAdvance(false);
                self.customsModel.negotiationVis(false);
                self.customsModel.contractsVis(true);
                self.customsModel.negotiationAmountVis(false);
                self.customsModel.contractAmountVis(true);
                self.customsModel.negotiationNumVis(false);
                self.customsModel.lgAmountPercentageVal(0);
                self.customsModel.documentNumber(advanceArr.length != 0 ? Math.max(...advanceArr) + 1 : 1);
            }
            else
                self.customsModel.disableAdvance(true);
        };

        self.lgAmountPercantgeListener = function (event) {
            self.customsModel.lgAmountPercentageVal(event['detail'].value)
            self.customsModel.letterOfGuarnAmoutVal((self.customsModel.lgAmountPercentageVal() / 100) * self.customsModel.contractAmountVal().toString().replace(/,/g, ""))
        };

        self.negoSupplierListener = function (event) {
            if (event['detail'].value != '' && event['detail'].value != null) {
                Array.isArray(self.customsModel.negoObject().Items) ? self.customsModel.contractAmountTemp(self.customsModel.negoObject().Items.find(el => event['detail'].value == el.PARTY_NAME) != undefined ? self.customsModel.negoObject().Items.find(el => event['detail'].value == el.PARTY_NAME) : 'No data found') : self.customsModel.contractAmountTemp([self.customsModel.negoObject().Items].find(el => event['detail'].value == el.PARTY_NAME) != undefined ? [self.customsModel.negoObject().Items].find(el => event['detail'].value == el.PARTY_NAME) : 'No data found');
                if (self.customsModel.contractAmountTemp().BUYER_BID_TOTAL) {
                    self.customsModel.letterOfGuarnAmoutVal(parseInt(self.customsModel.contractAmountTemp().BUYER_BID_TOTAL.toString().replace(/,/g, "")) * parseInt(self.customsModel.lgAmountPercentageVal()) / 100);
                    self.customsModel.contractAmountVal(numberWithCommas(self.customsModel.contractAmountTemp().BUYER_BID_TOTAL))
                }

                else
                    self.customsModel.contractAmountVal('No data found')
            }
            else {
                self.customsModel.contractAmountVal('No data found')
            }
        };

        self.getLastContractRow = function () {
            services.getGeneric(commonhelper.getLastRowLg).then(data => {
                if (data.Data.length > 0) {
                    var lastDocNum = data.Data[0].documentNumber;
                    self.customsModel.documentNumber(lastDocNum + 1);
                } else {
                    self.customsModel.documentNumber(1);
                }
            }, error => { });
        };

        self.createBtn = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }

            if (self.customsModel.supplierVal() == "No data found" || self.customsModel.contractAmountVal() == "No data found" || self.customsModel.contractAmountVal() == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'Data Is Missing'));
                return;
            }
            else if (self.customsModel.letterOfGuarnAmoutVal() == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'Letter of Guarantee Amount Must be larger than 0'));
                return;
            }
            else if (self.customsModel.lgNumberFromBankVal() == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'Letter of Guarantee number Must be larger than 0'));
                return;
            }
            else {
                self.customsModel.sumbitLg(false);
                var approveStatus = "Pending";
                var lgStatus = "Pending Approval";
                if (app.personNumber() == app.personNumberAdmin()) {
                    approveStatus = "APPROVED";
                    lgStatus = "In Process";
                }
                payload = [{
                    documentNumber: self.customsModel.documentNumber(),
                    lgNumFromBank: self.customsModel.lgNumberFromBankVal(),
                    lgType: self.customsModel.lgTypeVal(),
                    lgAmount: self.customsModel.letterOfGuarnAmoutVal().toString().replace(/,/g, ""),
                    lgEndDate: self.customsModel.lgEndDateVal(),
                    lgStartDate: self.customsModel.lgStartDateVal(),
                    region: self.customsModel.regionVal(),
                    bankAccount: self.customsModel.bankAccountVal(),
                    description: self.customsModel.description(),
                    lgStatus: lgStatus,
                    lgAmountPerc: self.customsModel.lgAmountPercentageVal().toString(),
                    approvalStatus: approveStatus,
                    createdBy: app.personNumber(),
                    createdByName: app.personName(),
                    lgAction: "None",
                    Attachments: self.customsModel.attachmentArr(),
                }]
                if (self.customsModel.negotiationTitleVal() != "-" && self.customsModel.lgTypeVal() == 'Bid bond') {
                    payload[0].negotiationTitle = self.customsModel.negotiationTitleVal().toString()
                    payload[0].negotiationNumber = self.customsModel.negotiationNumVal().toString()
                    payload[0].negotiationAmount = self.customsModel.contractAmountVal().toString().replace(/,/g, "")
                    payload[0].supplier = self.customsModel.negoSupplierVal(),
                    payload[0].contractNumber = ""
                    payload[0].contractAmount = ""
                }
                else  {
                    payload[0].contractNumber = self.customsModel.contractNoVal().toString()
                    payload[0].contractAmount = self.customsModel.contractAmountVal().toString().replace(/,/g, "")
                    payload[0].supplier = self.customsModel.supplierVal(),
                    payload[0].negotiationTitle = ""
                    payload[0].negotiationNumber = ""
                    payload[0].negotiationAmount = ""
                }

                var date = formatDate(new Date());
                self.customsModel.closeToExp(date_diff_indays(date, self.customsModel.lgEndDateVal()));
                payload[0].closeToExp = self.customsModel.closeToExp();
                self.customsModel.lgArr(payload);
                self.customsModel.createLg(true);
                self.customsModel.disableAllFields(true);
            }
        };

        self.submitBtn = function () {
            app.loading(true);
            var payload2 = [];
            services.postGeneric(commonhelper.addLg, payload).then(data => {
                app.loading(false);
                if (data) {
                    payload[0].cancel = "false";
                    payload2.push({ empId: personNumber, empName: personName, notification: payload[0] });
                    self.clear();
                    self.customsModel.disableAllFields(false);
                    self.getContractNumber();
                    // services.postGeneric(commonhelper.addNotification, payload2);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been saved '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
            });
            self.customsModel.sumbitLg(true);
            self.customsModel.createLg(false);
        };

        self.addFileSelectListener = function (event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                self.customsModel.attdocNum(self.customsModel.documentNumber())
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "docNum": self.customsModel.attdocNum(),
                    "content": self.customsModel.contentData(),
                    "attach_type": "Creation",
                    "lg_type": self.customsModel.lgTypeVal(),
                }

                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name) {
                            self.customsModel.payloadAttach().push(x);
                            x = {};
                            self.customsModel.attachmentArr(self.customsModel.payloadAttach());
                            self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))
                        }
                    }
                })
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
            self.customsModel.selected_files(self.customsModel.fileData());
            self.customsModel.addFileDis(true);
        };

        self.uploadContarctAttachment = function () {
            app.loading(true);
            services.postGeneric(commonhelper.insertAttachment, self.customsModel.payloadAttach()).then(data => {
                app.loading(false);
                self.customsModel.attachmentArr([]);
                self.customsModel.payloadAttach([]);
            }, error => { app.loading(false); });
        };

        self.viewContarctAttachment = function () {
            self.customsModel.attachmentArr([]);
            self.customsModel.attachmentArr(self.customsModel.payloadAttach());
            document.querySelector("#viewAttachment").open();
        };

        self.removePickerBtnAction = function (event) {
            if (document.querySelector("oj-file-picker input"))
                document.querySelector("oj-file-picker input").value = null;

            self.customsModel.selected_files([]);
            self.customsModel.addFileDis(false);
        };

        self.removeSelectedAttachment = function () {
            if (self.customsModel.selectedItemsAttachment().length < 1) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));
                return;
            }
            var x = self.customsModel.attachmentArr().findIndex((el => el.name == self.customsModel.selectedItemsAttachment()[0][1]))
            self.customsModel.attachmentArr().splice(x, 1);
            $('#filePicker input').val(null)
            self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))
        };

        self.clear = function () {
            self.customsModel.documentNumber(self.customsModel.documentNumber() + 1);
            self.customsModel.lgNumberFromBankVal(0);
            self.customsModel.supplierVal('');
            self.customsModel.contractNoVal('');
            // self.customsModel.lgTypeVal('');
            self.customsModel.contractAmountVal('');
            self.customsModel.letterOfGuarnAmoutVal(0);
            self.customsModel.lgEndDateVal('');
            self.customsModel.lgStartDateVal('');
            self.customsModel.regionVal();
            self.customsModel.bankAccountVal('');
            self.customsModel.description('');
            self.customsModel.closeToExp('');
            self.customsModel.selected_files([]);
            self.customsModel.lgArr([]);
            self.customsModel.negotiationNumVal('');
            self.customsModel.negotiationTitleVal('');
            self.customsModel.documentNumber('');
            self.customsModel.lgAmountPercentageVal(1);
            self.customsModel.attachmentArr([]);
            self.customsModel.payloadAttach();
            self.customsModel.negoSupplierArr([])
            self.customsModel.negoSupplierVal('');
        };

        self.endDateValueChanged = function (event) {
            self.customsModel.fromStartDate(event['detail'].value);
        }

        self.backBtn = function () {
            if (self.customsModel.lgArr().length == 0) {
                oj.Router.rootInstance.go('lgScreen');
            } else {
                document.querySelector("#confirmDiscard").open();
            }
        };

        self.getLastRowHistory = function (docNum) {
            services.getGeneric(commonhelper.getLastHistory + docNum).then(data => {
                self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1)
            }, error => {
            })
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function (date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.yes = function () {
            oj.Router.rootInstance.go('lgScreen');
        };

        self.no = function () {
            document.querySelector("#confirmDiscard").close();
        };

        self.cancelviewAttDial = function () {
            document.querySelector("#viewAttachment").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                contractStartDateLbl: getTransaltion("lgOperation.contractStartDateLbl"),
                contractEndDateLbl: getTransaltion("lgOperation.contractEndDateLbl"),
                letterOfGuarnAmoutLbl: getTransaltion("lgOperation.letterOfGuarnAmoutLbl"),
                contractAmountLbl: getTransaltion("lgOperation.contractAmountLbl"),
                supplierLbl: getTransaltion("lgOperation.supplierLbl"),
                regionLbl: getTransaltion("lgOperation.regionLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                uploadLbl: getTransaltion("common.uploadLbl"),
                descriptionLbl: getTransaltion("lgOperation.descriptionLbl"),
                closeToExpLbl: getTransaltion("common.closeToExpLbl"),
                submitLbl: getTransaltion("common.submitLbl"),
                backLbl: getTransaltion("common.backLbl"),
                addFileLbl: getTransaltion("common.addFileLbl"), 
                viewAttachmentLbl: getTransaltion("common.viewAttachmentLbl"),
                titleLbl: getTransaltion("lgOperation.titleLbl"),
                nameLbl: getTransaltion("lgOperation.nameLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                createLbl: getTransaltion("common.createLbl"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                confirmText: getTransaltion("common.confirmDiscardText"),
                confirmDiscard: getTransaltion("common.confirmDiscard"),
                lgCreateScreenLbl: getTransaltion("lgOperation.lgCreateScreenLbl"),
                lgStartDateLbl: getTransaltion("lgOperation.lgStartDateLbl"),
                negotiationNumLbl: getTransaltion("lgOperation.negotiationNumLbl"),
                negotiationTitleLbl: getTransaltion("lgOperation.negotiationTitleLbl"),
                negotiationAmountLbl: getTransaltion("lgOperation.negotiationAmountLbl"),
                validateLbl: getTransaltion("lgOperation.validateLbl"),
                lgAmountPercantgeLbl: getTransaltion("lgOperation.lgAmountPercantgeLbl"),
                negotaitionSupplierLbl: getTransaltion("lgOperation.negotaitionSupplierLbl"),
            });

            self.customsModel.columnArray([{
                "headerText": self.label().documentNumberLbl,
                "field": "documentNumber"
            },
            {
                "headerText": self.label().lgNumberFromBankLbl,
                "field": "lgNumFromBank"
            },
            {
                "headerText": self.label().contractNoLbl,
                "field": "contractNumber"
            },
            {
                "headerText": self.label().contractAmountLbl,
                "field": "contractAmount"
            },
            {
                "headerText": self.label().negotiationTitleLbl,
                "field": "negotiationTitle"
            },
            {
                "headerText": self.label().negotiationNumLbl,
                "field": "negotiationNumber"
            },
            {
                "headerText": self.label().negotiationAmountLbl,
                "field": "negotiationAmount"
            },
            {
                "headerText": self.label().supplierLbl,
                "field": "supplier"
            },
            {
                "headerText": self.label().letterOfGuarnAmoutLbl,
                "field": "lgAmount"
            },
            {
                "headerText": self.label().lgTypeLbl,
                "field": "lgType"
            },
            {
                "headerText": self.label().lgStartDateLbl,
                "field": "lgStartDate"
            },
            {
                "headerText": self.label().lgEndDateLbl,
                "field": "lgEndDate"
            },
            {
                "headerText": self.label().bankAccountLbl,
                "field": "bankAccount"
            },
            {
                "headerText": self.label().regionLbl,
                "field": "region"
            },
            {
                "headerText": self.label().closeToExpLbl,
                "field": "closeToExp"
            }
            ]);

            self.customsModel.attachmentColumns([
                {
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                }
            ]);
        }
        initTransaltion();
    }

    return lgOperationContentViewModel;
});