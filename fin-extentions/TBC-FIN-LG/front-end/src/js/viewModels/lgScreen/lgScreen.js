define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'xlsx', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingcontrol'
], function (oj, ko, $, app, services, commonhelper) {
    function lgScreenContentViewModel() {
        var self = this;
        self.label = ko.observable();
        var payload;
        self.datacrad = ko.observableArray([]);
        self.customsModel = {
            lgArr: ko.observableArray([]),
            lgExpireArr: ko.observableArray([]),
            lgFilterExpireArr: ko.observableArray([]),
            lgNoCancelArr: ko.observableArray([]),
            columnArray: ko.observableArray(),
            suppilerNameArr: ko.observableArray(),
            lgNumberFromBankArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgStatusArr: ko.observableArray(),
            rowSelected: ko.observable(),
            lgNumberFromBankVal: ko.observable(),
            suppilerNameVal: ko.observable(),
            contractNoVal: ko.observable(),
            lgStatusVal: ko.observable(),
            documentNumber: ko.observable(),
            searchClicked: ko.observable(true),
            disableAllFields: ko.observable(true),
            resetDis: ko.observable(false),
            lgEditSelected: ko.observable(true),
            lgSelected: ko.observable(true),
            lgViewSelected: ko.observable(true),
            data: ko.observableArray([]),
            createDis: ko.observable(true),
            groupValid: ko.observable(true),
            exportDis: ko.observable(true),
            approvalListDis: ko.observable(true),
            extendDis: ko.observable(true),
            increaseDis: ko.observable(true),
            decreaseDis: ko.observable(true),
            releaseDis: ko.observable(true),
            confisactionDis: ko.observable(true),
            selectedItemsAttachment: ko.observableArray([]),
            fileData: ko.observableArray([]),
            contentData: ko.observable(),
            attTitle: ko.observable(),
            attName: ko.observable(),
            attdocNum: ko.observable(),
            documentNumber: ko.observable(),
            payloadAttach: ko.observableArray([]),
            attachmentArr: ko.observableArray([]),
            disReleasePopAction: ko.observable(true),
            lgTypeExpireVal: ko.observable(),
            actionSeq: ko.observable(),
            lgType: ko.observable(),
        };

        self.dataProviderLgCloseToExpire = ko.observable();
        self.dataProviderLgCloseToExpire(new oj.ArrayTableDataSource(self.customsModel.lgExpireArr, { idAttribute: 'srNo' }));

        self.dataproviderLgScreen = ko.observable();
        self.dataproviderLgScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.lgArr, { idAttribute: 'srNo' })));

        self.dataProviderAttachment = ko.observable()
        self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))

        ko.computed(() => {
            if (app.userValidation()) {
                var x = app.userValidation().find(el => el.typeAction == "Create")
                self.customsModel.lgType(app.userValidation().map(el => ({ lgType: el.lgType })))
                if (x)
                    self.customsModel.createDis(false)
            }
            if (app.lgActionValidation()) {
            }

        });

        self.connected = function () {
            self.getData();
        };

        self.getData = function () {
            app.loading(true);
            services.getGeneric(commonhelper.getLg).then(data => {
                app.loading(false);
                var date = formatDate(new Date());
                var diff;
                if (data.Data) {
                    var srNo = 1;
                    self.customsModel.exportDis(false)
                    self.customsModel.lgArr([]);
                    data.Data.forEach(el => { el.contractAmount ? el.contractAmount = numberWithCommas(el.contractAmount) : el.contractAmount = '-', el.negotiationAmount ? el.negotiationAmount = numberWithCommas(el.negotiationAmount) : el.negotiationAmount = '-', el.contractNumber ? el.contractNumber : el.contractNumber = '-', el.negotiationTitle ? el.negotiationTitle : el.negotiationTitle = '-', el.negotiationNumber ? el.negotiationNumber : el.negotiationNumber = '-', el.lgAmount = numberWithCommas(el.lgAmount), el.srNo = srNo++ })
                    for (var i in data.Data) {
                        diff = date_diff_indays(date, data.Data[i].lgEndDate);
                        data.Data[i].lgCloseToExp = diff;
                        if (diff < 60 && (data.Data[i].lgType != 'Bid bond') && (data.Data[i].lgStatus == 'In Process' || data.Data[i].lgStatus == 'Pending Approval')) {
                            self.customsModel.lgFilterExpireArr.push(data.Data[i]);
                            self.dataProviderLgCloseToExpire(new oj.ArrayTableDataSource(self.customsModel.lgExpireArr, { idAttribute: 'srNo' }));
                        }
                        else if (diff < 30 && (data.Data[i].lgType == 'Bid bond') && (data.Data[i].lgStatus == 'In Process' || data.Data[i].lgStatus == 'Pending Approval')) {
                            self.customsModel.lgFilterExpireArr.push(data.Data[i]);
                            self.dataProviderLgCloseToExpire(new oj.ArrayTableDataSource(self.customsModel.lgExpireArr, { idAttribute: 'srNo' }));
                        }
                        if (data.Data[i].lgStatus == 'In Process' || data.Data[i].lgStatus == 'Rejected') {
                            self.customsModel.lgArr.push(data.Data[i]);
                            self.dataproviderLgScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.lgArr, { idAttribute: 'srNo' })));
                        }
                    }
                    self.customsModel.data(data.Data);
                    self.customsModel.lgNoCancelArr(self.customsModel.lgArr());
                    var suppArr = [...new Set(data.Data.map(el => el.supplier))];
                    var lgNumArr = [...new Set(data.Data.map(el => el.lgNumFromBank))];
                    var contractNumArr = [...new Set(data.Data.filter(e => e.contractNumber != '-').map(el => el.contractNumber))];
                    var lgStatusArr = [...new Set(data.Data.map(el => el.lgStatus))];
                    lgStatusArr.push("All");
                    self.customsModel.suppilerNameArr(suppArr.map(el => ({ label: el, value: el })));
                    self.customsModel.lgNumberFromBankArr(lgNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.contractNumberArr(contractNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.lgStatusArr(lgStatusArr.map(el => ({ label: el, value: el })));
                }
            }, error => {
                app.loading(false);
            });
        };

        self.lgselectListener = function (event) {
            self.customsModel.extendDis(true)
            self.customsModel.increaseDis(true)
            self.customsModel.decreaseDis(true)
            self.customsModel.releaseDis(true)
            self.customsModel.confisactionDis(true)

            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.lgViewSelected(false);
                self.customsModel.approvalListDis(false);
                self.customsModel.rowSelected(self.customsModel.lgArr()[[currentRow['rowIndex']]]);
                if ((self.customsModel.rowSelected().lgStatus == 'In Process')) {
                    self.customsModel.lgEditSelected(true);

                    app.lgActionValidation().map(e => ({ lgAction: e.lgAction, lgType: e.lgType })).forEach(el => {
                        if ((el.lgAction == 'Extend') && (self.customsModel.rowSelected().lgType == el.lgType))
                            self.customsModel.extendDis(false)

                        if ((el.lgAction == 'Increase') && (self.customsModel.rowSelected().lgType == el.lgType))
                            self.customsModel.increaseDis(false)

                        if ((el.lgAction == 'Decrease') && (self.customsModel.rowSelected().lgType == el.lgType))
                            self.customsModel.decreaseDis(false)

                        if ((el.lgAction == 'Release') && (self.customsModel.rowSelected().lgType == el.lgType))
                            self.customsModel.releaseDis(false)

                        if ((el.lgAction == 'Confiscation') && (self.customsModel.rowSelected().lgType == el.lgType))
                            self.customsModel.confisactionDis(false)

                        if ((el.lgAction == 'All Action') && (self.customsModel.rowSelected().lgType == el.lgType)) {
                            self.customsModel.extendDis(false)
                            self.customsModel.increaseDis(false)
                            self.customsModel.decreaseDis(false)
                            self.customsModel.releaseDis(false)
                            self.customsModel.confisactionDis(false)
                        }
                    })
                }
                else if ((self.customsModel.rowSelected().lgStatus == 'Rejected')) {
                    self.customsModel.lgEditSelected(false);
                }
                else {
                    self.customsModel.lgEditSelected(true);
                }
            }
        };

        self.viewBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                data.view = "view";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.searchBtn = function () {
            cond = function (el) {
                var res = (
                    (self.customsModel.documentNumber() ? el.documentNumber == self.customsModel.documentNumber() : true) &&
                    (self.customsModel.lgNumberFromBankVal() ? el.lgNumFromBank == self.customsModel.lgNumberFromBankVal() : true) &&
                    (self.customsModel.suppilerNameVal() ? el.supplier == self.customsModel.suppilerNameVal() : true) &&
                    (self.customsModel.contractNoVal() ? el.contractNumber == self.customsModel.contractNoVal() : true) &&
                    (self.customsModel.lgStatusVal() ? el.lgStatus == self.customsModel.lgStatusVal() : true)
                );
                return res;
            };
            var find = self.customsModel.data().filter(el => cond(el));

            self.customsModel.lgArr(find);
            self.dataproviderLgScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.lgArr, { idAttribute: 'srNo' })));

            if (self.customsModel.lgStatusVal() == "All") {
                self.customsModel.lgArr(self.customsModel.data());
                self.customsModel.exportDis(false)
                self.dataproviderLgScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.lgArr, { idAttribute: 'srNo' })));
            }
        };

        self.resetBtn = function () {
            self.customsModel.resetDis(true);
            self.customsModel.documentNumber('');
            self.customsModel.lgNumberFromBankVal('');
            self.customsModel.suppilerNameVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgStatusVal('');
            self.customsModel.lgViewSelected(true);
            self.customsModel.lgEditSelected(true);
            self.customsModel.approvalListDis(true);

            setTimeout(function () {
                self.customsModel.resetDis(false);
                self.customsModel.lgArr(self.customsModel.lgNoCancelArr());
                self.dataproviderLgScreen(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.lgArr, { idAttribute: 'srNo' })));
            }, 1000);
        };

        self.historyBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgHistory');
            }
        };

        self.addHistory = function () {
            services.getGeneric(commonhelper.getLastHistory + self.customsModel.rowSelected().documentNumber).then(data => {
                self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1);
                payload[0].actionSeq = self.customsModel.actionSeq().toString(),
                    services.postGeneric(commonhelper.addLgHistoryRelease, payload).then(data => {
                        app.loading(false);
                    }, error => {
                        app.loading(false);
                        app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                    });
            }, error => {
                app.loading(false);
            })
        };

        self.releaseAction = function () {
            var selected = self.customsModel.rowSelected();
            document.querySelector("#confirmRelease").close();
            app.loading(true);
            payload = [{
                id: selected.id,
                documentNumber: selected.documentNumber,
                lgNumFromBank: selected.lgNumFromBank,
                lgType: selected.lgType,
                lgStatus: "Released",
                operation: "Release",
                createBy: app.personNumber(),
                createByName: app.personName(),
                lgAmount: selected.lgAmount,
                lgType: selected.lgType,
                lgActionStatus: "Pending",
                lgActionType: "Release",
                lgEndDate: selected.lgEndDate,
                Attachments: self.customsModel.attachmentArr(),
            }];
            services.getGeneric(commonhelper.getLastHistory + self.customsModel.rowSelected().documentNumber).then(data => {
                self.customsModel.actionSeq(parseInt(data.Data.Action_Seq) + 1);
                payload[0].actionSeq = self.customsModel.actionSeq().toString()
                services.postGeneric(commonhelper.lgActionApproval, payload).then(data => {
                    if (data) {
                        self.resetBtn();
                        app.loading(false);
                        self.getData();
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                    }
                }, error => {
                    document.querySelector("#confirmRelease").close();
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
                });
            }, error => {
                document.querySelector("#confirmRelease").close();
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            })
        };

        self.extendBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                data.type = "Extend";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.increaseBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                data.type = "Increase";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.decreaseBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                data.type = "Decrease";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.confiscationBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                data.confis = "Confiscation";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go("confiscationScreen");
            }
        };

        self.editBtn = function () {
            if (self.customsModel.rowSelected()) {
                var data = self.customsModel.rowSelected();
                data.type = "edit";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.createBtn = function () {
            oj.Router.rootInstance.store(self.customsModel.lgType());
            oj.Router.rootInstance.go('lgOperation');
        };

        self.exportBtn = function () {
            self.customsModel.lgArr().forEach(el => { delete el.createdBy, delete el.id, delete el.srNo })
            self.customsModel.lgArr().forEach(obj => {
                Object.keys(app.exportHeader()).forEach(key => {
                    _key = app.exportHeader()[key];//new key
                    _val = obj[key];//value
                    delete obj[key];
                    obj[_key] = _val;//create new element with new key
                })
                return obj;
            })
            self.exportFile(self.customsModel.lgArr());
        };

        self.approvalListBtn = function () {
            app.loading(true);
            var payload = {
                "documentNumber": self.customsModel.rowSelected().documentNumber.toString(),
                "lgType": self.customsModel.rowSelected().lgType
            }
            self.datacrad([]);
            services.postGeneric(commonhelper.workFlowList, payload).then(data => {
                data.Data.sort(function (a, b) { return parseInt(a.approvalPersonSeq) - parseInt(b.approvalPersonSeq) })
                data.Data.forEach(el => {
                    if (el.approvalStatus == "APPROVED") {
                        cardStatus = 'badge-success'
                        bodyCardStatus = 'app-crd-bdy-border-suc'
                    }
                    else if (el.approvalStatus == "REJECT") {
                        cardStatus = 'badge-danger';
                        bodyCardStatus = 'app-crd-bdy-border-fail';
                    }
                    else if (el.approvalStatus == "Pending Approval") {
                        cardStatus = 'badge-warning';
                        bodyCardStatus = 'app-crd-bdy-border-pen';
                    }
                    self.datacrad.push({
                        personName: el.approvalPersonName,
                        FYA: "",
                        Approved: el.approvalStatus,
                        approvalLevel: el.approvalLevel,
                        date: "2020-05-21",
                        status: "",
                        bodyStatus: bodyCardStatus,
                        appCardStatus: cardStatus,
                        rejectReason: ""
                    })
                })
                app.loading(false);
                document.querySelector("#approvalDialog").open();
            }, error => {
                app.loading(false);
            })
        };

        self.addFileSelectListener = function (event) {
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function (evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                self.customsModel.attdocNum(self.customsModel.rowSelected().documentNumber)
                var x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "docNum": self.customsModel.attdocNum(),
                    "content": self.customsModel.contentData(),
                    "attach_type": "Release",
                    "id": 0
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name)
                            self.customsModel.payloadAttach().push(x);

                        x = {};
                        self.customsModel.attachmentArr(self.customsModel.payloadAttach());
                        self.customsModel.disReleasePopAction(false)
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
        };

        self.removeSelectedAttachment = function () {
            if (self.customsModel.selectedItemsAttachment().length < 1) {
                app.messagesDataProvider.push(app.createMessage('error', 'Please select attached to remove'));
                return;
            }

            var x = self.customsModel.attachmentArr().findIndex((el => el.name == self.customsModel.selectedItemsAttachment()[0][1]))
            self.customsModel.attachmentArr().splice(x, 1);
            self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))
        };

        self.attachExportBtn = function () {
            self.customsModel.lgExpireArr().forEach(el => { delete el.createdBy, delete el.id, delete el.srNo })
            self.customsModel.lgExpireArr().forEach(obj => {
                Object.keys(app.exportHeader()).forEach(key => {
                    _key = app.exportHeader()[key];//new key
                    _val = obj[key];//value
                    delete obj[key];
                    obj[_key] = _val;//create new element with new key
                })
                return obj;
            })
            self.exportFile(self.customsModel.lgExpireArr());
        };

        self.exportFile = function (arr) {
            if (arr.length == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'No LG Data Found'));
            } else {
                var wb = XLSX.utils.book_new();
                var ws = XLSX.utils.json_to_sheet(arr, { cellStyle: true });
                XLSX.utils.book_append_sheet(wb, ws, "LG");
                wb["Sheets"]['LG']["!cols"] = [{ wpx: 130 }, { wpx: 130 }, { wpx: 130 }, { wpx: 130 }, { wpx: 130 }, { wpx: 130 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }, { wpx: 135 }];
                wb["Sheets"]["LG"]["!rows"] = [{ hpx: 25 }];
                XLSX.writeFile(wb, "LG.xlsx");
            }
        };

        self.filterBtn = function () {
            self.customsModel.lgExpireArr([]);
            var cloneArr = self.customsModel.lgFilterExpireArr();
            if (self.customsModel.lgTypeExpireVal() != '') {
                var filtered = cloneArr.filter(el => el.lgType == self.customsModel.lgTypeExpireVal());
                self.customsModel.lgExpireArr(filtered);
                self.dataProviderLgCloseToExpire(new oj.ArrayTableDataSource(self.customsModel.lgExpireArr, { idAttribute: 'srNo' }));
            }
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function (date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            var x = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
            return x
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.releaseBtn = function () {
            document.querySelector("#confirmRelease").open();
        };

        self.closeToExpBtn = function () {
            document.querySelector("#viewExpire").open();
        };

        self.cancelReleaseAction = function () {
            document.querySelector("#confirmRelease").close();
        };

        self.cancelBtn = function () {
            document.querySelector("#viewExpire").close();
        };

        self.approvalDialogCloseAction = function () {
            document.querySelector("#approvalDialog").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                suppilerNameLbl: getTransaltion("lgScreen.suppilerNameLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgStatusLbl: getTransaltion("lgScreen.lgStatusLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                bankAmountLbl: getTransaltion("lgScreen.bankAmountLbl"),
                amountLbl: getTransaltion("lgScreen.amountLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                status: getTransaltion("lgScreen.status"),
                searchLbl: getTransaltion("common.searchLbl"),
                viewLbl: getTransaltion("common.viewLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                createLbl: getTransaltion("common.createLbl"),
                closeToExpLbl: getTransaltion("common.closeToExpLbl"),
                extendLbl: getTransaltion("common.extendLbl"),
                increaseLbl: getTransaltion("common.increaseLbl"),
                decreaseLbl: getTransaltion("common.decreaseLbl"),
                releaseLbl: getTransaltion("common.releaseLbl"),
                confiscationLbl: getTransaltion("common.confiscationLbl"),
                editLbl: getTransaltion("common.editLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                historyLbl: getTransaltion("lgScreen.historyLbl"),
                approvalStatus: getTransaltion("lgScreen.approvalStatus"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                confirmText: getTransaltion("common.confirmText"),
                confirmReleaseLbl: getTransaltion("lgScreen.confirmReleaseLbl"),
                contractAmountLbl: getTransaltion("lgScreen.contractAmountLbl"),
                exportLbl: getTransaltion("common.exportLbl"),
                descriptionLbl: getTransaltion("lgOperation.descriptionLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                lgCloseToExp: getTransaltion("common.closeToExpLbl"),
                lgScreenLbl: getTransaltion("lgScreen.lgScreenLbl"),
                lgStartDateLbl: getTransaltion("lgOperation.lgStartDateLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                addFileLbl: getTransaltion("common.addFileLbl"),
                negotiationNumLbl: getTransaltion("lgOperation.negotiationNumLbl"),
                negotiationTitleLbl: getTransaltion("lgOperation.negotiationTitleLbl"),
                negotiationAmountLbl: getTransaltion("lgOperation.negotiationAmountLbl"),
                filterLbl: getTransaltion("lgScreen.filterLbl"),
                lgAmountPercLbl: getTransaltion("lgScreen.lgAmountPercLbl"),
                approvalListLbl: getTransaltion("approvalScreen.approvalListLbl"),
                okLbl: getTransaltion("common.okLbl"),

            });
            self.customsModel.columnArray([{
                "headerText": self.label().documentNumberLbl,
                "field": "documentNumber"
            },
            {
                "headerText": self.label().lgNumberFromBankLbl,
                "field": "lgNumFromBank"
            },
            {
                "headerText": self.label().contractNoLbl,
                "field": "contractNumber"
            },
            {
                "headerText": self.label().contractAmountLbl,
                "field": "contractAmount"
            },
            {
                "headerText": self.label().negotiationTitleLbl,
                "field": "negotiationTitle"
            },
            {
                "headerText": self.label().negotiationNumLbl,
                "field": "negotiationNumber"
            },
            {
                "headerText": self.label().negotiationAmountLbl,
                "field": "negotiationAmount"
            },
            {
                "headerText": self.label().suppilerNameLbl,
                "field": "supplier"
            },
            {
                "headerText": self.label().amountLbl,
                "field": "lgAmount"
            },
            {
                "headerText": self.label().lgAmountPercLbl,
                "field": "lgAmountPercnatge"
            },
            {
                "headerText": self.label().lgStartDateLbl,
                "field": "lgStartDate"
            },
            {
                "headerText": self.label().lgEndDateLbl,
                "field": "lgEndDate"
            },
            {
                "headerText": self.label().lgTypeLbl,
                "field": "lgType"
            },
            {
                "headerText": self.label().status,
                "field": "lgStatus"
            },
            {
                "headerText": self.label().bankAccountLbl,
                "field": "bankAccount"
            },
            {
                "headerText": self.label().lgCloseToExp,
                "field": "lgCloseToExp"
            },
            {
                "headerText": self.label().descriptionLbl,
                "field": "description"
            },
            {
                "headerText": self.label().approvalStatus,
                "field": "approvalStatus"
            }
            ]);
        }
        initTransaltion();
    }

    return lgScreenContentViewModel;
});