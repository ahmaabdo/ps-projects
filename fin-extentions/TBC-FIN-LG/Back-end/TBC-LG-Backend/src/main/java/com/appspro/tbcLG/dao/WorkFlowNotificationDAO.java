package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.tbcLG.bean.ApprovalSetupBean;
import com.appspro.tbcLG.bean.LetterGuaranteeBean;
import com.appspro.tbcLG.bean.WorkFlowNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class WorkFlowNotificationDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps2;
    CallableStatement cs;
    ResultSet rs;
    ResultSet rs2;
    RestHelper rh = new RestHelper();

    public String updateNotifyStatus(WorkFlowNotificationBean bean,
                                     String approvalStatus) throws SQLException {
        try {
            connection = AppsproConnection.getConnection();

            String query1 = "update XXX_LG_WORKFLOW_APPROVAL set APPROVAL_STATUS = ?  where APPROVAL_PERSON_SEQ=? AND LG_DOCUMENT_NUM = ? AND LG_TYPE = ? ";
            ps = connection.prepareStatement(query1);
            ps.setString(1, bean.getApprovalStatus());
            ps.setString(2, bean.getApprovalPersonSeq());
            ps.setString(3, Integer.toString(bean.getDocumentNumber()));
            ps.setString(4, bean.getLgType());
            ps.executeUpdate();
            
            String query = "update XXX_LG_APPROVAL_LIST set APPROVAL_STATUS = ?  where REQUESTER_NAME=? AND LG_DOCUMENT_NUM = ? AND LG_TYPE = ? AND APRROVAL_NAME_SEQ = ? AND LG_ACTION = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getApprovalStatus());
            ps.setString(2, bean.getRequesterName());
            ps.setString(3, Integer.toString(bean.getDocumentNumber()));
            ps.setString(4, bean.getLgType());
            ps.setString(5, bean.getApprovalPersonSeq());
            ps.setString(6, bean.getLgAction());

            ps.executeUpdate();

            int seq = Integer.parseInt(bean.getApprovalPersonSeq()) + 1;

            if (approvalStatus.equalsIgnoreCase("APPROVED")) {
                String query2 = "SELECT * FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ? AND APPROVAL_PERSON_SEQ = ?  AND LG_TYPE =? AND LG_ACTION = ?";
                ps = connection.prepareStatement(query2);
                ps.setString(1, bean.getRequesterName());
                ps.setString(2, Integer.toString(seq));
                ps.setString(3, bean.getLgType());
                ps.setString(4, bean.getLgAction());
                rs = ps.executeQuery();

                if (rs.isBeforeFirst()) {
                    while (rs.next()) {
                        String query3 = "INSERT INTO XXX_LG_WORKFLOW_APPROVAL(REQUESTER_PERSON_NUM,REQUESTER_PERSON_NAME,APPROVAL_PERSON_NAME,APPROVAL_PERSON_NUM,APPROVAL_PERSON_SEQ,APPROVAL_STATUS,LG_DOCUMENT_NUM,LG_TYPE,LG_ACTION,ACTION_SEQ) VALUES(?,?,?,?,?,?,?,?,?,?)";
                        ps = connection.prepareStatement(query3);
                        ps.setString(1, bean.getRequesterNumber());
                        ps.setString(2, bean.getRequesterName());
                        ps.setString(3, rs.getString("APPROVAL_PERSON_NAME"));
                        ps.setString(4, rs.getString("APPROVAL_PERSON_NUM"));
                        ps.setString(5, rs.getString("APPROVAL_PERSON_SEQ"));
                        ps.setString(6, "Pending Approval");
                        ps.setString(7, Integer.toString(bean.getDocumentNumber()));
                        ps.setString(8, bean.getLgType());
                        ps.setString(9, rs.getString("LG_ACTION"));
                        ps.setString(10, bean.getActionSeq());

                        ps.executeUpdate();
                    }
                } else {
                    if (bean.getLgAction().equals("None")) {
                        String query4 = "update  XXX_LG_NUMBER set APPROVAL_STATUS=? ,STATUS=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
                        ps = connection.prepareStatement(query4);
                        ps.setString(1, bean.getApprovalStatus());
                        ps.setString(2, bean.getLgStatus());
                        ps.setInt(3, bean.getDocumentNumber());
                        ps.setString(4, bean.getLgType());
                        ps.executeUpdate();
                    } else {
                        String query6 = "update  XXX_LG_NUMBER set LG_END_DATE =? ,LG_AMOUNT =? , STATUS=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
                        ps = connection.prepareStatement(query6);
                        ps.setString(1, bean.getLgEndDate());
                        ps.setString(2, bean.getAmount());
                        ps.setString(3, bean.getLgStatus());
                        ps.setInt(4, bean.getDocumentNumber());
                        ps.setString(5, bean.getLgType());
                        ps.executeUpdate();

                        String query7 = "update  XXX_LG_HISTORY set LG_ACTION_STATUS =? where DOCUMENT_NUMBER=? and LG_TYPE =? and ACTION_SEQ = ?";
                        ps = connection.prepareStatement(query7);
                        ps.setString(1, "Approved");
                        ps.setInt(2, bean.getDocumentNumber());
                        ps.setString(3, bean.getLgType());
                        ps.setString(4, bean.getActionSeq());

                        ps.executeUpdate();
                    }
                }
            } else {
                if (bean.getLgAction().equals("None")) {
                    String query4 =  "update  XXX_LG_NUMBER set APPROVAL_STATUS=? ,STATUS=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
                    ps = connection.prepareStatement(query4);
                    ps.setString(1, bean.getApprovalStatus());
                    ps.setString(2, bean.getLgStatus());
                    ps.setInt(3, bean.getDocumentNumber());
                    ps.setString(4, bean.getLgType());
                    ps.executeUpdate();
                }else{
                    String query7 = "update  XXX_LG_HISTORY set LG_ACTION_STATUS =? where DOCUMENT_NUMBER=? and LG_TYPE =? and ACTION_SEQ = ?";
                    ps = connection.prepareStatement(query7);
                    ps.setString(1, "Rejected");
                    ps.setInt(2, bean.getDocumentNumber());
                    ps.setString(3, bean.getLgType());
                    ps.setString(4, bean.getActionSeq());

                    ps.executeUpdate();
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Notification updated Successfully";
    }

    public JSONArray getNotificationByEmpId(String empId) throws SQLException {

        JSONArray arr = new JSONArray();
        String query = "";
        try {
            connection = AppsproConnection.getConnection();

            String query2 =
                "SELECT * FROM XXX_LG_WORKFLOW_APPROVAL WHERE APPROVAL_PERSON_NUM = ? AND APPROVAL_STATUS = 'Pending Approval' ";
            ps = connection.prepareStatement(query2);
            ps.setString(1, empId);
            rs = ps.executeQuery();

            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put("requesterName",  rs.getString("REQUESTER_PERSON_NAME"));
                obj.put("requesterNumber", rs.getString("REQUESTER_PERSON_NUM"));
                obj.put("approvalPersonName", rs.getString("APPROVAL_PERSON_NAME"));
                obj.put("approvalPersonNumber", rs.getString("APPROVAL_PERSON_NUM"));
                obj.put("approvalPersonSeq", rs.getString("APPROVAL_PERSON_SEQ"));
                obj.put("approvalStatus", rs.getString("APPROVAL_STATUS"));
                obj.put("documentNumber", rs.getString("LG_DOCUMENT_NUM"));
                obj.put("lgType", rs.getString("LG_TYPE"));
                obj.put("lgAction", rs.getString("LG_ACTION"));
                obj.put("actionSeq", rs.getString("ACTION_SEQ"));

                if (rs.getString("LG_ACTION").equals("None")) {
                    query = "SELECT * FROM XXX_LG_NUMBER WHERE DOCUMENT_NUMBER = ? AND LG_TYPE =?";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, rs.getString("LG_DOCUMENT_NUM"));
                    ps.setString(2, rs.getString("LG_TYPE"));
                    rs2 = ps.executeQuery();
                } else {
                    query = "SELECT \n" +
                            "XXX_LG_NUMBER.DOCUMENT_NUMBER,\n" +
                            "XXX_LG_NUMBER.CONTRACT_NUMBER,\n" +
                            "XXX_LG_NUMBER.CONTRACT_AMOUNT,\n" +
                            "XXX_LG_NUMBER.SUPPLIER,\n" +
                            "XXX_LG_NUMBER.LG_NUM_FROM_BANK,\n" +
                            "XXX_LG_NUMBER.LG_TYPE,\n" +
                            "XXX_LG_HISTORY.AMOUNT,\n" +
                            "XXX_LG_HISTORY.LG_END_DATE,\n" +
                            "XXX_LG_HISTORY.ACTION_SEQ,\n" +
                            "XXX_LG_NUMBER.REGION,\n" +
                            "XXX_LG_NUMBER.BANK_ACCOUNT,\n" +
                            "XXX_LG_NUMBER.DESCRIPTION,\n" +
                            "XXX_LG_NUMBER.STATUS,\n" +
                            "XXX_LG_NUMBER.APPROVAL_STATUS,\n" +
                            "XXX_LG_NUMBER.LG_START_DATE,\n" +
                            "XXX_LG_NUMBER.NEGOTIATION_NUM,\n" +
                            "XXX_LG_NUMBER.NEGOTIATION_TITLE,\n" +
                            "XXX_LG_NUMBER.NEGOTIATION_AMOUNT,\n" +
                            "XXX_LG_NUMBER.LG_AMOUNT_PERC\n" +
                            "FROM \n" +
                            "XXX_LG_NUMBER,\n" +
                            "XXX_LG_HISTORY\n" +
                            "\n" +
                            "WHERE \n" +
                            "XXX_LG_HISTORY.DOCUMENT_NUMBER = XXX_LG_NUMBER.DOCUMENT_NUMBER AND\n" +
                            "XXX_LG_HISTORY.LG_TYPE = XXX_LG_NUMBER.LG_TYPE and\n" +
                            "XXX_LG_HISTORY.DOCUMENT_NUMBER = ? AND\n" +
                            "XXX_LG_HISTORY.LG_TYPE = ? and\n" +
                            "XXX_LG_HISTORY.ACTION_SEQ = ? and\n" +
                            "XXX_LG_HISTORY.LG_ACTION_STATUS = 'Pending' \n";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, rs.getString("LG_DOCUMENT_NUM"));
                    ps.setString(2, rs.getString("LG_TYPE"));
                    ps.setString(3, rs.getString("ACTION_SEQ"));
                    rs2 = ps.executeQuery();
                }
                
                while (rs2.next()) {
                    JSONObject lgObject = new JSONObject();
                    lgObject.put("documentNumber",  rs2.getInt("DOCUMENT_NUMBER"));
                    lgObject.put("contractNumber", rs2.getString("CONTRACT_NUMBER"));
                    lgObject.put("contractAmount", rs2.getString("CONTRACT_AMOUNT"));
                    lgObject.put("supplier", rs2.getString("SUPPLIER"));
                    lgObject.put("lgNumFromBank", rs2.getString("LG_NUM_FROM_BANK"));
                    lgObject.put("lgType", rs2.getString("LG_TYPE"));
                    lgObject.put("lgEndDate", rs2.getString("LG_END_DATE"));
                    lgObject.put("region", rs2.getString("REGION"));
                    lgObject.put("bankAccount", rs2.getString("BANK_ACCOUNT"));
                    lgObject.put("description", rs2.getString("DESCRIPTION"));
                    lgObject.put("lgStatus", rs2.getString("STATUS"));
                    lgObject.put("approvalStatus", rs2.getString("APPROVAL_STATUS"));
                    lgObject.put("lgStartDate", rs2.getString("LG_START_DATE"));
                    lgObject.put("negotiationNumber", rs2.getString("NEGOTIATION_NUM"));
                    lgObject.put("negotiationTitle", rs2.getString("NEGOTIATION_TITLE"));
                    lgObject.put("negotiationAmount", rs2.getString("NEGOTIATION_AMOUNT"));
                    lgObject.put("lgAmountPercnatge", rs2.getString("LG_AMOUNT_PERC"));
                    if (rs.getString("LG_ACTION").equals("None")) {
                        lgObject.put("lgAmount", rs2.getString("LG_AMOUNT"));
                    } else {
                        lgObject.put("lgAmount", rs2.getString("AMOUNT"));
                        lgObject.put("actionSeq", rs2.getString("ACTION_SEQ"));
                    }
                    obj.put("lgData", lgObject);
                }
                arr.put(obj);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
            closeResources(rs2);
        }

        return arr;
    }


    public JSONArray allEmpsApprovalList(String documentNumber,String lgType) throws SQLException {

        JSONArray responseArr = new JSONArray();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_APPROVAL_LIST WHERE LG_DOCUMENT_NUM = ? AND LG_TYPE = ? AND LG_ACTION = 'None'";
            ps = connection.prepareStatement(query);
            ps.setString(1, documentNumber);
            ps.setString(2, lgType);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                JSONObject workFlowObj = new JSONObject();
                workFlowObj.put("approvalPersonName", rs.getString("APPROVAL_NAME"));
                workFlowObj.put("approvalStatus",  rs.getString("APPROVAL_STATUS"));
                workFlowObj.put("requesterName", rs.getString("REQUESTER_NAME"));
                workFlowObj.put("approvalLevel", rs.getString("APPROVAL_LEVEL"));
                workFlowObj.put("approvalPersonSeq", rs.getString("APRROVAL_NAME_SEQ"));    
            
                responseArr.put(workFlowObj);
         }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(rs2);
        }
        return responseArr;
    }

}
