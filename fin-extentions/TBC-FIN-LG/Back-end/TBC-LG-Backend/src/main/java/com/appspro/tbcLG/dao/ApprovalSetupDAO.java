package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.tbcLG.bean.ApprovalSetupBean;
import com.appspro.tbcLG.bean.ApprovalValidationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApprovalSetupDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    ResultSet rs2;
    RestHelper rh = new RestHelper();
    
    public String addEmployeeApproval(ApprovalSetupBean bean) throws SQLException {
        try {
            connection = AppsproConnection.getConnection();

            String query2 =  "INSERT INTO XXX_LG_APPROVAL_SETUP(REQUESTER_PERSON,APPROVAL_PERSON_NUM,APPROVAL_PERSON_NAME,APPROVAL_PERSON_SEQ,TYPE_ACTION,LG_TYPE,LEVEL_APPROVAL,LG_ACTION) VALUES(?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query2);
            ps.setString(1, bean.getRequesterName());
            ps.setString(2, bean.getApprovalPersonNumber());
            ps.setString(3, bean.getApprovalPersonName());
            ps.setString(4, bean.getApprovalPersonSeq());
            ps.setString(5, bean.getTypeAction());
            ps.setString(6, bean.getLgType());
            ps.setString(7, bean.getLevelApproval());
            ps.setString(8, bean.getLgAction());
            
            ps.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Employee Approval added Successfully";
    }
    
    public JSONArray allEmployeeApproval(String requesterName) throws SQLException {

        ArrayList<ApprovalSetupBean> approvalEmpList =  new ArrayList<ApprovalSetupBean>();
        JSONArray arr = new JSONArray();
        try {

            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, requesterName);
            rs = ps.executeQuery();
            while (rs.next()) {
                ApprovalSetupBean empBean = new ApprovalSetupBean();
                empBean.setRequesterName(rs.getString("REQUESTER_PERSON"));
                empBean.setApprovalPersonName(rs.getString("APPROVAL_PERSON_NUM"));
                empBean.setApprovalPersonNumber(rs.getString("APPROVAL_PERSON_NAME"));
                empBean.setApprovalPersonSeq(rs.getString("APPROVAL_PERSON_SEQ"));
                empBean.setLgType(rs.getString("TYPE_ACTION"));
                empBean.setTypeAction(rs.getString("LG_TYPE"));
                empBean.setLevelApproval(rs.getString("LEVEL_APPROVAL"));
                empBean.setLgAction(rs.getString("LG_ACTION"));

                approvalEmpList.add(empBean);
            }

            arr = new JSONArray(approvalEmpList);
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    } 
           
    public JSONArray allEmpsApproval() throws SQLException {

        ArrayList<ApprovalSetupBean> approvalEmpList =  new ArrayList<ApprovalSetupBean>();
        JSONArray arr = new JSONArray();
        try {

            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_APPROVAL_SETUP";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ApprovalSetupBean empBean = new ApprovalSetupBean();
                empBean.setRequesterName(rs.getString("REQUESTER_PERSON"));
                empBean.setApprovalPersonNumber(rs.getString("APPROVAL_PERSON_NUM"));
                empBean.setApprovalPersonName(rs.getString("APPROVAL_PERSON_NAME"));
                empBean.setApprovalPersonSeq(rs.getString("APPROVAL_PERSON_SEQ"));
                empBean.setTypeAction(rs.getString("TYPE_ACTION"));
                empBean.setLgType(rs.getString("LG_TYPE"));
                empBean.setLevelApproval(rs.getString("LEVEL_APPROVAL"));
                empBean.setLgAction(rs.getString("LG_ACTION"));
    
                approvalEmpList.add(empBean);
            }

            arr = new JSONArray(approvalEmpList);
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }    
    
    
    public String updateApprovalEmployee(String userName,String lgType , JSONObject obj) throws SQLException {
        
    try {
            connection = AppsproConnection.getConnection();
            String query = "UPDATE XXX_LG_APPROVAL_SETUP SET LEVEL_APPROVAL = ? , LG_TYPE = ? ,APPROVAL_PERSON_NAME = ? WHERE EMPLOYEE_EMAIL = ? AND LG_TYPE = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, obj.get("levelApproval").toString());
            ps.setString(2, obj.get("lgType").toString()); 
            ps.setString(3, obj.get("approvalPersonName").toString());
            ps.setString(4, userName);
            ps.setString(5, lgType);

            ps.executeUpdate();
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
    
        return "Employee approval deleted Successfully";
    }

    
    public String deleteApprovalEmployee(String requester , String lgType , String appEmp , String lgAction) throws SQLException {
        
    try {
            connection = AppsproConnection.getConnection();
            String query = "DELETE FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ? AND LG_TYPE = ? AND APPROVAL_PERSON_NUM = ? AND LG_ACTION = ?";
            int checkResult = 0;
            ps = connection.prepareStatement(query);
            ps.setString(1, requester);
            ps.setString(2, lgType);
            ps.setString(3, appEmp);
            ps.setString(4, lgAction);

            checkResult = ps.executeUpdate();
        
            //            String query2 = "DELETE FROM XXX_LG_WORKFLOW_APPROVAL WHERE REQUESTER_PERSON_NAME = ? AND APPROVAL_PERSON_NAME= ? AND LG_TYPE = ? AND APPROVAL_STATUS = 'Pending Approval'";
            //            ps = connection.prepareStatement(query2);
            //            ps.setString(1, userName);
            //            ps.setString(2, approvalPer);
            //            ps.setString(3, lgType);
            //            ps.executeUpdate();
        
        
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
    
        return "Creation approval deleted Successfully";
    }
    
}
