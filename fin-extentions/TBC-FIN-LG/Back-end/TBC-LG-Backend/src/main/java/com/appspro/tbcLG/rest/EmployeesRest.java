package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.EmployeeBean;
import com.appspro.tbcLG.dao.EmployeesDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/Empolyees")

public class EmployeesRest {
    
    @GET
    @Path("/AllEmployees")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public String getEmployees()  {

        EmployeesDAO emp = new EmployeesDAO();
        JSONObject resObj = new JSONObject();
        JSONArray allEmployees;
        
        try {
            allEmployees = emp.getAllEmployees();
            resObj.put("Status", " OK");
            resObj.put("Data", allEmployees);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
            resObj.put("Status", " ERROR");
            resObj.put("Data", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            resObj.put("Status", " ERROR");
            resObj.put("Data", e.getMessage());
        }
        return resObj.toString();

    }
    
    @GET
    @Path("/{username}")
    @Produces( { "application/json" })
    public String getDataFromLookup(@PathParam("username")
        String userName) {
        EmployeeBean obj = new EmployeeBean();
        EmployeesDAO det = new EmployeesDAO();
        if (userName != null && !userName.isEmpty()) {
            String details = det.getEmployeeDetails(userName);
            return details;
        }else{
                return "Null Username";
        }
    }
}
