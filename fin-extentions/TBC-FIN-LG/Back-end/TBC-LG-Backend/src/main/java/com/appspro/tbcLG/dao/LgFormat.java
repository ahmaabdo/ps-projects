package com.appspro.mail;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.InputStream;

import java.math.BigInteger;

import java.nio.charset.Charset;

import java.nio.file.Files;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import org.json.JSONObject;

import org.openxmlformats.schemas.drawingml.x2006.chart.CTPageMargins;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSpacing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STLineSpacingRule;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.apache.commons.codec.binary.Base64;


public class LgFormat {

    public enum TextOrientation {
        LTR,
        RTL;
    }

    private static void setOrientation(XWPFParagraph par,TextOrientation orientation) {
        if (par.getCTP().getPPr() == null) {
            par.getCTP().addNewPPr();
        }
        if (par.getCTP().getPPr().getBidi() == null) {
            par.getCTP().getPPr().addNewBidi();
        }
        par.getCTP().getPPr().getBidi().setVal(orientation ==
                                               TextOrientation.RTL ?
                                               STOnOff.ON : STOnOff.OFF);
    }

    private static void setOrientationForCell(XWPFTableRow tableRowfour,XWPFParagraph par) {
        par = tableRowfour.getCell(1).getParagraphs().get(0);
        CTP ctp = par.getCTP();
        CTPPr ctppr = ctp.getPPr();
        if (ctppr == null)
            ctppr = ctp.addNewPPr();
        ctppr.addNewBidi().setVal(STOnOff.ON);
    }
    
    private static void setOrientationForSecCell(XWPFTableRow tableRowfour,XWPFParagraph par) {
        par = tableRowfour.getCell(0).getParagraphs().get(0);
        CTP ctp = par.getCTP();
        CTPPr ctppr = ctp.getPPr();
        if (ctppr == null)
            ctppr = ctp.addNewPPr();
        ctppr.addNewBidi().setVal(STOnOff.ON);
    }


    public static String generateWord (JSONObject obj) throws FileNotFoundException, IOException {
        
        String lgNumber = obj.getString("lgNumFromBank");
        String lgAmount = obj.getString("lgAmount");
        String endDate = obj.getString("actualEndDate");
        String lgPlusDate = obj.getString("lgPlusDate");
        String bankName = obj.getString("bankAccount");
        System.out.println(lgPlusDate);
        String blanketString = "Blanket";

        //Blank Document
        XWPFDocument document = new XWPFDocument();
        String fileName =  System.getProperty("user.home") + "\\Downloads\\test3.docx";

        //Write the Document in file system
//        FileOutputStream out = new FileOutputStream(new File(fileName));
        
        XWPFParagraph paragraphTitle = document.createParagraph();
        XWPFRun runTilel = paragraphTitle.createRun();
        String titleLine = "موضوع :  طلب تجديد الضمان البنكي النهائي";
        runTilel.setText(titleLine);
        runTilel.setBold(true);
        runTilel.setFontFamily("AL-Mohanad Bold");

        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        
        run.setBold(true);
        run.setFontSize(13);
        run.setFontFamily("AL-Mohanad Bold");
        String firstLine = "                المحترمين                   "+ bankName+  " الساده مصرف "    ;
        String seocndLine = "السلام عليكم ورحمه الله وبركاته";
        String thirdLine = blanketString+" مرفق لكم الضمان البنكي النهائي الموضح بياناته أدناه، والذي تم إصداره في تاريخ  " ;
        
        run.setText(firstLine);
        run.addBreak();
        run.setText(seocndLine);
        run.addBreak();
        run.setText(thirdLine);
        run.addBreak();
        
        setOrientation(paragraph, TextOrientation.RTL);
        paragraph.setSpacingAfter(230);
        paragraph.setSpacingAfterLines(230);
        paragraph.setSpacingBefore(230);
        paragraph.setSpacingBeforeLines(230);

        //create table
        XWPFTable table = document.createTable();
        table.setWidth(2000);
        table.getCTTbl().addNewTblPr().addNewTblW().setW(BigInteger.valueOf(10000));
        table.setCellMargins(0, 200, 0, 200);

        //create first row
        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText(lgNumber);
        tableRowOne.addNewTableCell().setText("رقم الضمان");
        setOrientationForCell(tableRowOne, paragraph);
        setOrientationForSecCell(tableRowOne, paragraph);

        //create second row
        XWPFTableRow tableRowTwo = table.createRow();
        tableRowTwo.getCell(0).setText(lgAmount);
        tableRowTwo.getCell(1).setText("مبلغ الضمان رقما");
        table.getRow(1).getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
        setOrientationForCell(tableRowTwo, paragraph);   
        setOrientationForSecCell(tableRowTwo, paragraph);

        //create four row
        XWPFTableRow tableRowfour = table.createRow();
        tableRowfour.getCell(0).setText("Blanket");
        tableRowfour.getCell(1).setText("تاريخ اصدار الضمان");
        setOrientationForCell(tableRowfour, paragraph);
        setOrientationForSecCell(tableRowfour, paragraph);

        //create five row
        XWPFTableRow tableRowfive = table.createRow();
        String x = "sherif";
        tableRowfive.getCell(0).setText(endDate);
        tableRowfive.getCell(1).setText("تاريخ فى نهايه الضمان");
        setOrientationForCell(tableRowfive, paragraph);
        setOrientationForSecCell(tableRowfive, paragraph);

        XWPFParagraph newParagraph = document.createParagraph();
        XWPFRun run2 = newParagraph.createRun();
        setOrientation(newParagraph, TextOrientation.RTL);
        
        newParagraph.setSpacingAfter(300);
        newParagraph.setSpacingAfterLines(300);
        newParagraph.setSpacingBefore(300);
        newParagraph.setSpacingBeforeLines(300);

        run2.setBold(true);
        run2.setFontSize(13);
        run2.setFontFamily("AL-Mohanad Bold");
        String fourLine = " : الرجاء تمديد الضمان حسب الآتي";
        run2.setText(fourLine);
        run2.addBreak();

        String fiveLine = lgPlusDate+ "  فتره التمديد المطلوبه:  365 يوما تبدا بتاريخ  "+endDate+ " وتنتهى بتاريخ -  " ;
        run2.setText(fiveLine);
        run2.addBreak();

        String sixLine =  " قيمه الضمان   :  "+lgAmount+"    ريال سعودى  -  ";
        run2.setText(sixLine);
        run2.addBreak();

        String sevenLine = ".وعلى ما سبق فنرجو تزويدنا بخطاب الضمان البنكي النهائي مجدداً خلال أسبوع من تاريخ خطابنا هذا";
        run2.setText(sevenLine);
        run2.addBreak();

        String eightLine = "،،،وتقبلوا وافر التحية والتقدير";
        run2.setText(eightLine);
        
        XWPFParagraph footerPara = document.createParagraph();
        XWPFRun run3 = footerPara.createRun();
        footerPara.setAlignment(ParagraphAlignment.CENTER);
        
        footerPara.setSpacingAfter(350);
        footerPara.setSpacingAfterLines(350);
        footerPara.setSpacingBefore(350);
        footerPara.setSpacingBeforeLines(350);
        
        run3.setBold(true);
        run3.setFontSize(14);
        run3.setFontFamily("AL-Mohanad Bold");
        
        String nineLine = "مدير عام قطاع المالية";
        String tenLine = "محمد بن عبد الله الرشيد";
        
        run3.setText(nineLine);
        run3.addBreak();
        run3.setText(tenLine);

        XWPFParagraph footerPara2 = document.createParagraph();
        XWPFRun run4 = footerPara2.createRun();
        footerPara2.setAlignment(ParagraphAlignment.CENTER);
        
        footerPara2.setSpacingAfter(230);
        footerPara2.setSpacingAfterLines(230);
        footerPara2.setSpacingBefore(230);
        footerPara2.setSpacingBeforeLines(230);
        
        run4.setBold(true);
        run4.setFontSize(14);
        run4.setFontFamily("AL-Mohanad Bold");
        
        String elevenLine = "الرئيس التنفيذي";
        String twelevLine = "شركة تطوير لخدمات النقل التعليمي";
        String threteenLine = "فهد بن فوزان الشايع";

        run4.setText(elevenLine);
        run4.addBreak();
        run4.setText(twelevLine);
        run4.addBreak();
        run4.setText(threteenLine);
          

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        document.write(bos);
//      document.write(out);
        String base64 = Base64.encodeBase64String(bos.toByteArray());               
//      System.out.println(base64);

//        out.close();
        System.out.println("createdocument.docx written successully");
        
        return base64;
    }
    
    public  String releaseLgFormat (JSONObject obj) throws FileNotFoundException, IOException {
        
            String lgNumber = obj.getString("lgNumFromBank");
            String lgAmount = obj.getString("lgAmount");
            String bankName = obj.getString("bankAccount");
            String blanketString = "Blanket";
                    
            XWPFDocument document = new XWPFDocument();
            String fileName =  System.getProperty("user.home") + "\\Downloads\\test4.docx";

            FileOutputStream out = new FileOutputStream(new File(fileName));
            
            XWPFParagraph paragraphTitle = document.createParagraph();
            XWPFRun runTilel = paragraphTitle.createRun();
            String titleLine = "موضوع  : إفراج ضمان بنكى";
            runTilel.setText(titleLine);
            runTilel.setBold(true);
            runTilel.setFontFamily("AL-Mohanad Bold");

            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();
            
            run.setBold(true);
            run.setFontSize(13);
            run.setFontFamily("AL-Mohanad Bold");
            String firstLine = "  الساده مصرف   "+bankName+"  المحترمين قسم خدمات الضمانات البنكيه  ";
            String seocndLine = "السلام عليكم ورحمه الله وبركاته";
            String threeLine = "إشارة إلى خطاب الضمان البنكي النهائي الموضح تفاصيله أدناه والصادر من قبلكم والمقدم من قبل\n" + 
            "مؤسسة أبو وليد للنقل المدرسي.\n";
            String fourLine = "نعيد اليكم أصل الضمان البنكي النهائي بموجب هذا الخطاب للعقد رقم 57-2018 والموضح أدناه ." ;
            
            run.setText(firstLine);
            run.addBreak();
            run.setText(seocndLine);
            run.addBreak();
            run.setText(threeLine);
            run.addBreak();
            run.setText(fourLine);
            run.addBreak();
            
            setOrientation(paragraph, TextOrientation.RTL);
            paragraph.setSpacingAfter(230);
            paragraph.setSpacingAfterLines(230);
            paragraph.setSpacingBefore(230);
            paragraph.setSpacingBeforeLines(230);
            
            //create table
            XWPFTable table = document.createTable();
            table.setWidth(2000);
            table.getCTTbl().addNewTblPr().addNewTblW().setW(BigInteger.valueOf(10000));
            table.setCellMargins(0, 200, 0, 200);

            //create first row
            XWPFTableRow tableRowOne = table.getRow(0);
            tableRowOne.getCell(0).setText(lgNumber);
            tableRowOne.addNewTableCell().setText("نوع الضمان البنكي");
            setOrientationForCell(tableRowOne, paragraph);
            setOrientationForSecCell(tableRowOne, paragraph);  

            //create second row
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(lgAmount);
            tableRowTwo.getCell(1).setText("رقم الضمان البنكي");
            table.getRow(1).getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
            setOrientationForCell(tableRowTwo, paragraph);   
            setOrientationForSecCell(tableRowTwo, paragraph);
            
            //create four row
            XWPFTableRow tableRowthree = table.createRow();
            tableRowthree.getCell(0).setText("Blanket");
            tableRowthree.getCell(1).setText("قيمه الضمان البنكى");
            setOrientationForCell(tableRowthree, paragraph);
            setOrientationForSecCell(tableRowthree, paragraph);
            
            //create four row
            XWPFTableRow tableRowfour = table.createRow();
            tableRowfour.getCell(0).setText("Blanket");
            tableRowfour.getCell(1).setText("تاريخ الإنتهاء");
            setOrientationForCell(tableRowfour, paragraph);
            setOrientationForSecCell(tableRowfour, paragraph);
            
            XWPFParagraph newParagraph = document.createParagraph();
            XWPFRun run2 = newParagraph.createRun();
//            setOrientation(newParagraph, TextOrientation.CENTER);
            newParagraph.setAlignment(ParagraphAlignment.CENTER);

            
            newParagraph.setSpacingAfter(300);
            newParagraph.setSpacingAfterLines(300);
            newParagraph.setSpacingBefore(300);
            newParagraph.setSpacingBeforeLines(300);

            run2.setBold(true);
            run2.setFontSize(13);
            run2.setFontFamily("AL-Mohanad Bold");
            String fiveLine = "وتفضلوا بقبول تحياتي وتقديري ،،،";
            run2.setText(fiveLine);
            run2.addBreak();
            
            XWPFParagraph footerPara = document.createParagraph();
            XWPFRun run3 = footerPara.createRun();
            footerPara.setAlignment(ParagraphAlignment.CENTER);
            
            footerPara.setSpacingAfter(350);
            footerPara.setSpacingAfterLines(350);
            footerPara.setSpacingBefore(350);
            footerPara.setSpacingBeforeLines(350);
            
            run3.setBold(true);
            run3.setFontSize(14);
            run3.setFontFamily("AL-Mohanad Bold");
            
            String nineLine = "مدير عام قطاع المالية";
            String tenLine = "محمد بن عبد الله الرشيد";
            
            run3.setText(nineLine);
            run3.addBreak();
            run3.setText(tenLine);
            
            XWPFParagraph footerPara2 = document.createParagraph();
            XWPFRun run4 = footerPara2.createRun();
            footerPara2.setAlignment(ParagraphAlignment.CENTER);
            
            footerPara2.setSpacingAfter(230);
            footerPara2.setSpacingAfterLines(230);
            footerPara2.setSpacingBefore(230);
            footerPara2.setSpacingBeforeLines(230);
            
            run4.setBold(true);
            run4.setFontSize(14);
            run4.setFontFamily("AL-Mohanad Bold");
            
            String elevenLine = "الرئيس التنفيذي";
            String twelevLine = "شركة تطوير لخدمات النقل التعليمي";
            String threteenLine = "فهد بن فوزان الشايع";

            run4.setText(elevenLine);
            run4.addBreak();
            run4.setText(twelevLine);
            run4.addBreak();
            run4.setText(threteenLine);
              
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            
          document.write(bos);
//            document.write(out);
           String base64 = Base64.encodeBase64String(bos.toByteArray());               
           System.out.println(base64);

//                    out.close();
            System.out.println("createdocument.docx written successully");
        
            return base64;
        }
    
    

    public static void main(String[] args)  {
        
        JSONObject obj = new JSONObject();
        obj.put("lgNumFromBank","1234566");
        obj.put("lgAmount","1200000");
        obj.put("contractStartDate","2019-12-03");
        obj.put("lgEndDate","2020-12-03");
        obj.put("lgPlusDate","03-12-2020");
        obj.put("bankAccount","SABB");

        LgFormat lg  = new LgFormat();
        try {
            lg.releaseLgFormat(obj);
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }
}
