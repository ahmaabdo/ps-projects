package com.appspro.tbcLG.bean;

public class LgAttachmentBean {
    private int id;
    private String title;
    private String name;
    private String content;
    private String attachtType;
    private int docNum;
    private String attachtNum;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setDocNum(int docNum) {
        this.docNum = docNum;
    }

    public int getDocNum() {
        return docNum;
    }

    public void setAttachtType(String attachtType) {
        this.attachtType = attachtType;
    }

    public String getAttachtType() {
        return attachtType;
    }

    public void setAttachtNum(String attachtNum) {
        this.attachtNum = attachtNum;
    }

    public String getAttachtNum() {
        return attachtNum;
    }
}
