package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.dao.CreateExternalCashDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/externalCashTreansaction")

public class CreateExternalCashRest {
   
    @POST
    @Path("/create")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response creditcashExternal (String body){
        
        JSONObject obj = new JSONObject();
        JSONObject obj2 = new JSONObject();
        JSONObject bodyObj = new JSONObject(body);
        CreateExternalCashDAO cashExternalobj = new CreateExternalCashDAO();
        
        obj = cashExternalobj.cashTransaction(bodyObj.toString());
       
        return  Response.ok(obj.toString()).build();
    }
}
