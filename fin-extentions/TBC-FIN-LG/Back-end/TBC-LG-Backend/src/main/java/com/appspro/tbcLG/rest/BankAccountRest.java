package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.BankAccountBean;
import com.appspro.tbcLG.dao.BankAccountDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/bankAccount")

public class BankAccountRest {
   
    @GET
    @Path("/getBankAccount")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response bankAccount () throws MalformedURLException,IOException {
        
        ArrayList<BankAccountBean> bankList = new ArrayList<BankAccountBean>();
        BankAccountDAO bankDao = new BankAccountDAO();
        
        bankList = bankDao.getAllBankAccount(); 
        JSONArray array = new JSONArray(bankList);
        return  Response.ok(array.toString()).build();
    }
}
