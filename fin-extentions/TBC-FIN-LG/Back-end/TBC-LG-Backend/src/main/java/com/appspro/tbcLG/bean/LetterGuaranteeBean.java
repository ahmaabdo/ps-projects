package com.appspro.tbcLG.bean;

public class LetterGuaranteeBean {
    private int id;
    private int documentNumber;
    private String contractNumber;
    private String contractAmount;
    private String supplier;
    private String contractStartDate;
    private String contractEndDate;
    private String lgNumFromBank;
    private String lgType;
    private String lgAmount;
    private String lgEndDate;
    private String region;
    private String bankAccount;
    private String description;
    private String lgStatus;
    private String approvalStatus;
    private String createdBy;
    private String lgStartDate;
    private String negotiationNumber;
    private String negotiationTitle;
    private String negotiationAmount;
    private String lgAmountPercnatge;
    private String createdByName;
    private String lgAction;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDocumentNumber(int documentNumber) {
        this.documentNumber = documentNumber;
    }

    public int getDocumentNumber() {
        return documentNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractAmount(String contractAmount) {
        this.contractAmount = contractAmount;
    }

    public String getContractAmount() {
        return contractAmount;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractStartDate() {
        return contractStartDate;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getContractEndDate() {
        return contractEndDate;
    }

    public void setLgNumFromBank(String lgNumFromBank) {
        this.lgNumFromBank = lgNumFromBank;
    }

    public String getLgNumFromBank() {
        return lgNumFromBank;
    }

    public void setLgType(String lgType) {
        this.lgType = lgType;
    }

    public String getLgType() {
        return lgType;
    }

    public void setLgAmount(String lgAmount) {
        this.lgAmount = lgAmount;
    }

    public String getLgAmount() {
        return lgAmount;
    }

    public void setLgEndDate(String lgEndDate) {
        this.lgEndDate = lgEndDate;
    }

    public String getLgEndDate() {
        return lgEndDate;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setLgStatus(String lgStatus) {
        this.lgStatus = lgStatus;
    }

    public String getLgStatus() {
        return lgStatus;
    }

    public void setLgStartDate(String lgStartDate) {
        this.lgStartDate = lgStartDate;
    }

    public String getLgStartDate() {
        return lgStartDate;
    }

    public void setNegotiationNumber(String negotiationNumber) {
        this.negotiationNumber = negotiationNumber;
    }

    public String getNegotiationNumber() {
        return negotiationNumber;
    }

    public void setNegotiationTitle(String negotiationTitle) {
        this.negotiationTitle = negotiationTitle;
    }

    public String getNegotiationTitle() {
        return negotiationTitle;
    }

    public void setNegotiationAmount(String negotiationAmount) {
        this.negotiationAmount = negotiationAmount;
    }

    public String getNegotiationAmount() {
        return negotiationAmount;
    }


    public void setLgAmountPercnatge(String lgAmountPercnatge) {
        this.lgAmountPercnatge = lgAmountPercnatge;
    }

    public String getLgAmountPercnatge() {
        return lgAmountPercnatge;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setLgAction(String lgAction) {
        this.lgAction = lgAction;
    }

    public String getLgAction() {
        return lgAction;
    }
}
