package com.appspro.tbcLG.bean;

public class ApprovalValidationBean {
    
    private String employeeName;
    private String employeeNumber;
    private String employeeEmail;
    private String typeAction;
    private String lgType;
    private String lgAction;


    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setTypeAction(String typeAction) {
        this.typeAction = typeAction;
    }

    public String getTypeAction() {
        return typeAction;
    }

    public void setLgType(String lgType) {
        this.lgType = lgType;
    }

    public String getLgType() {
        return lgType;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setLgAction(String lgAction) {
        this.lgAction = lgAction;
    }

    public String getLgAction() {
        return lgAction;
    }
}
