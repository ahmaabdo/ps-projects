package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.LetterGuaranteeBean;
import com.appspro.tbcLG.bean.LgHistoryBean;
import com.appspro.tbcLG.dao.LetterGuaranteeDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/lg")
public class LetterGuaranteeRest {
    LetterGuaranteeDAO serivces = new LetterGuaranteeDAO();
    LetterGuaranteeBean bean = new LetterGuaranteeBean();
    LgHistoryBean history = new LgHistoryBean();
    JSONObject resposne = new JSONObject();
    JSONArray arr = new JSONArray();
    String status;

    @GET
    @Path("getLg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllLgDetails() {

        try {
            arr = serivces.getAllLgDetails();
            resposne.put("Status", "OK");
            resposne.put("Data", arr);

        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    @GET
    @Path("/getLastRowLg")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLastRowLg() {

        try {
            arr = serivces.getLastRowLg();
            resposne.put("Status", "OK");
            resposne.put("Data", arr);

        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @GET
    @Path("getLg/{docNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getLg(@PathParam("docNum")
        String docNum) {

        try {
            arr = serivces.getLg(docNum);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }

    @POST
    @Path("addLg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertLgDetails(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                history.setDocumentNumber(obj2.getInt("documentNumber"));
                history.setLgType(obj2.getString("lgType"));
                history.setLgAmount(obj2.getString("lgAmount"));
                history.setLgEndDate(obj2.getString("lgEndDate"));
                history.setBankAmount(obj2.getString("contractAmount"));
                history.setLgStatus(obj2.getString("lgStatus"));
                bean.setContractNumber(obj2.getString("contractNumber"));
                bean.setContractAmount(obj2.getString("contractAmount"));
                bean.setSupplier(obj2.getString("supplier"));
                bean.setLgNumFromBank(obj2.getString("lgNumFromBank"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgAmountPercnatge(obj2.getString("lgAmountPerc"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setLgStartDate(obj2.getString("lgStartDate"));
                bean.setRegion(obj2.getString("region"));
                bean.setBankAccount(obj2.getString("bankAccount"));
                bean.setDescription(obj2.getString("description"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setApprovalStatus(obj2.getString("approvalStatus"));
                bean.setCreatedBy(obj2.getString("createdBy"));
                bean.setCreatedByName(obj2.getString("createdByName"));
                bean.setNegotiationTitle(obj2.getString("negotiationTitle"));
                bean.setNegotiationNumber(obj2.getString("negotiationNumber"));
                bean.setNegotiationAmount(obj2.getString("negotiationAmount"));
                bean.setLgAmountPercnatge(obj2.getString("lgAmountPerc"));
                bean.setLgAction(obj2.getString("lgAction"));
                JSONArray attachmentArr = obj2.getJSONArray("Attachments");
                status = serivces.addLgDetails(bean,history,attachmentArr);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @POST
    @Path("updateLg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateContractDeitails(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setId(obj2.getInt("id"));
                bean.setContractNumber(obj2.getString("contractNumber"));
                bean.setContractAmount(obj2.getString("contractAmount"));
                bean.setSupplier(obj2.getString("supplier"));
                bean.setLgNumFromBank(obj2.getString("lgNumFromBank"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgStartDate(obj2.getString("lgStartDate"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setRegion(obj2.getString("region"));
                bean.setBankAccount(obj2.getString("bankAccount"));
                bean.setDescription(obj2.getString("description"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setApprovalStatus(obj2.getString("approvalStatus"));
                bean.setCreatedBy(obj2.getString("createdBy"));
                bean.setLgAmountPercnatge(obj2.getString("lgAmountPerc"));
                bean.setNegotiationNumber(obj2.getString("negotiationNumber"));
                bean.setNegotiationTitle(obj2.getString("negotiationTitle"));
                bean.setNegotiationAmount(obj2.getString("negotiationAmount"));

                status = serivces.updateLgDetails(bean);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    @POST
    @Path("updateLgStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateContractStatus(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setApprovalStatus(obj2.getString("approvalStatus"));

                status = serivces.updateContractStatus(bean);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    

    @POST
    @Path("deleteLg")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteLgDetailsById(String body) {
        String status = "";
        JSONArray arr = new JSONArray(body);
        JSONObject obj = new JSONObject();

        try {
            for (int i = 0; i < arr.length(); i++) {
                obj = arr.getJSONObject(i);
                bean.setDocumentNumber(obj.getInt("documentNumber"));
                bean.setLgType(obj.getString("lgType"));
                status = serivces.deleteLgDetails(bean);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
}
