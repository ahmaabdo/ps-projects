package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.tbcLG.bean.LetterGuaranteeBean;
import com.appspro.tbcLG.bean.LgAttachmentBean;
import com.appspro.tbcLG.bean.LgHistoryBean;

import com.appspro.tbcLG.bean.WorkFlowNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class LgHistoryDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps1;
    PreparedStatement ps2;

    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public JSONArray getLgHistory(String docNum,String lgType) throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LgHistoryBean> lgDetailsList = new ArrayList<LgHistoryBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_HISTORY WHERE DOCUMENT_NUMBER = ? and LG_TYPE =? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, docNum);
            ps.setString(2, lgType);

            rs = ps.executeQuery();
            while (rs.next()) {
                LgHistoryBean lgBean = new LgHistoryBean();

                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setBankAmount(rs.getString("BANK_AMOUNT"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setHistory(rs.getString("HISTORY"));
                lgBean.setModDate(rs.getString("MOD_DATE"));
                lgBean.setOperation(rs.getString("OPERATION"));
                lgBean.setAttachtNum(rs.getString("ATTACHMENT_NUM"));
                lgBean.setActionSeq(rs.getString("ACTION_SEQ"));
                lgBean.setLgActionStatus(rs.getString("LG_ACTION_STATUS"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }
    public String updateLgExtend(LgHistoryBean lgDetails,LetterGuaranteeBean lgBean,WorkFlowNotificationBean notifBean ,JSONArray arr) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();
            String id = "";

            String query = "update  XXX_LG_NUMBER set  LG_END_DATE=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgBean.getLgEndDate());
            ps.setInt(2, lgBean.getDocumentNumber());
            ps.setString(3, lgBean.getLgType());

            ps.executeUpdate();
            
            String query2 = "SELECT XXX_LG_ATTACH.NEXTVAL AS NEXT_ID FROM dual";
            ps = connection.prepareStatement(query2);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getString("NEXT_ID");
            }
            
            String query3 =  "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation,ATTACHMENT_NUM,ACTION_SEQ) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?,?,?)";
            ps = connection.prepareStatement(query3);
            ps.setString(1, lgDetails.getLgType());
            ps.setString(2, lgDetails.getLgAmount());
            ps.setString(3, lgDetails.getLgEndDate());
            ps.setString(4, lgDetails.getBankAmount());
            ps.setString(5, lgDetails.getLgStatus());
            ps.setInt(6, lgDetails.getDocumentNumber());
            ps.setString(7, lgDetails.getOperation());
            ps.setString(8, id);
            ps.setString(9, lgDetails.getActionSeq());

            ps.executeUpdate();
            
            String query4 = "INSERT INTO XXX_LG_ATTACHMENT(DOC_NUM,TITLE,NAME,CONTENT,ATTACHMENT_TYPE,ATTACHMENT_NUM,LG_TYPE) VALUES(?,?,?,?,?,?,?)";
            
            for (int i = 0 ; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                ps2 = connection.prepareStatement(query4);
                ps2.setInt(1, obj.getInt("docNum"));
                ps2.setString(2, obj.get("title").toString());
                ps2.setString(3, obj.get("name").toString());
                ps2.setString(4, obj.get("content").toString());
                ps2.setString(5, obj.get("attach_type").toString());
                ps2.setString(6, id);
                ps2.setString(7,lgBean.getLgType());

                ps2.executeUpdate();
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }
        return "lg updated successfully";
    }
    public String updateLgIncDec(LgHistoryBean lgDetails,LetterGuaranteeBean lgBean, JSONArray arr ) throws SQLException {
        try {
            connection = AppsproConnection.getConnection();
            String id = "";

            String query = "update  XXX_LG_NUMBER set  LG_AMOUNT=? , LG_AMOUNT_PERC=? where DOCUMENT_NUMBER=?  and LG_TYPE =?";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgBean.getLgAmount());
            ps.setString(2, lgBean.getLgAmountPercnatge());
            ps.setInt(3, lgBean.getDocumentNumber());
            ps.setString(4, lgBean.getLgType());

            ps.executeUpdate();
            
            String query2 = "SELECT XXX_LG_ATTACH.NEXTVAL AS NEXT_ID FROM dual";
            ps = connection.prepareStatement(query2);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getString("NEXT_ID");
            }
            
            String query3 = "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation,ATTACHMENT_NUM,ACTION_SEQ) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?,?,?)";

            ps = connection.prepareStatement(query3);
            ps.setString(1, lgDetails.getLgType());
            ps.setString(2, lgDetails.getLgAmount());
            ps.setString(3, lgDetails.getLgEndDate());
            ps.setString(4, lgDetails.getBankAmount());
            ps.setString(5, lgDetails.getLgStatus());
            ps.setInt(6, lgDetails.getDocumentNumber());
            ps.setString(7, lgDetails.getOperation());
            ps.setString(8, id);
            ps.setString(9, lgDetails.getActionSeq());

            ps.executeUpdate();

            String query4 = "INSERT INTO XXX_LG_ATTACHMENT(DOC_NUM,TITLE,NAME,CONTENT,ATTACHMENT_TYPE,ATTACHMENT_NUM,LG_TYPE) VALUES(?,?,?,?,?,?,?)";
            for (int i = 0 ; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                ps = connection.prepareStatement(query4);
                ps.setInt(1, obj.getInt("docNum"));
                ps.setString(2, obj.get("title").toString());
                ps.setString(3, obj.get("name").toString());
                ps.setString(4, obj.get("content").toString());
                ps.setString(5, obj.get("attach_type").toString());
                ps.setString(6, id);
                ps.setString(7,lgBean.getLgType());
                ps.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }
        return "lg updated successfully";
    }
    public String updateLgRelease(LgHistoryBean lgDetails,LetterGuaranteeBean lgBean,JSONArray arr) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();
            String id = "";
            
            String query2 = "SELECT XXX_LG_ATTACH.NEXTVAL AS NEXT_ID FROM dual";
            ps = connection.prepareStatement(query2);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getString("NEXT_ID");
            }
            
            String query3 = "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation,ATTACHMENT_NUM,ACTION_SEQ) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?,?,?)";

            ps = connection.prepareStatement(query3);
            ps.setString(1, lgDetails.getLgType());
            ps.setString(2, lgDetails.getLgAmount());
            ps.setString(3, lgDetails.getLgEndDate());
            ps.setString(4, lgDetails.getBankAmount());
            ps.setString(5, lgDetails.getLgStatus());
            ps.setInt(6, lgDetails.getDocumentNumber());
            ps.setString(7, lgDetails.getOperation());
            ps.setString(8, id);
            ps.setString(9, lgDetails.getActionSeq());
            ps.executeUpdate();
            
            String query4 = "INSERT INTO XXX_LG_ATTACHMENT(DOC_NUM,TITLE,NAME,CONTENT,ATTACHMENT_TYPE,ATTACHMENT_NUM,LG_TYPE) VALUES(?,?,?,?,?,?,?)";
            for (int i = 0 ; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                ps = connection.prepareStatement(query4);
                ps.setInt(1, obj.getInt("docNum"));
                ps.setString(2, obj.get("title").toString());
                ps.setString(3, obj.get("name").toString());
                ps.setString(4, obj.get("content").toString());
                ps.setString(5, obj.get("attach_type").toString());
                ps.setString(6, id);
                ps.setString(7,lgBean.getLgType());
                ps.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return "lg updated successfully";
    }
    
    public JSONObject getLatRowAction (String docNum) throws SQLException {
        JSONObject obj = new JSONObject();
        System.out.println(docNum);
        try {
            connection = AppsproConnection.getConnection();
    
            String query = "SELECT MAX (ACTION_SEQ) AS ACTION_SEQ FROM XXX_LG_HISTORY WHERE DOCUMENT_NUMBER = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, docNum);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.put("Action_Seq",rs.getString("ACTION_SEQ"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return obj;
    }
    
    public String lgActionApproval (LgHistoryBean lgDetails,JSONArray arr) throws SQLException {
        String id = "";
        
        connection = AppsproConnection.getConnection();
        String query2 = "SELECT XXX_LG_ATTACH.NEXTVAL AS NEXT_ID FROM dual";
        ps = connection.prepareStatement(query2);
        rs = ps.executeQuery();
        while (rs.next()) {
            id = rs.getString("NEXT_ID");
        }
        
        String query = "INSERT INTO XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation,ATTACHMENT_NUM,ACTION_SEQ,LG_ACTION_STATUS) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?,?,?,?)";
        connection = AppsproConnection.getConnection();
        ps = connection.prepareStatement(query);
        ps.setString(1, lgDetails.getLgType());
        ps.setString(2, lgDetails.getLgAmount());
        ps.setString(3, lgDetails.getLgEndDate());
        ps.setString(4, lgDetails.getBankAmount());
        ps.setString(5, lgDetails.getLgStatus());
        ps.setInt(6, lgDetails.getDocumentNumber());
        ps.setString(7, lgDetails.getOperation());
        ps.setString(8, id);
        ps.setString(9, lgDetails.getActionSeq());
        ps.setString(10, lgDetails.getLgActionStatus());
        ps.executeUpdate();

        String query3 = "INSERT INTO XXX_LG_ATTACHMENT(DOC_NUM,TITLE,NAME,CONTENT,ATTACHMENT_TYPE,ATTACHMENT_NUM,LG_TYPE) VALUES(?,?,?,?,?,?,?)";
        for (int i = 0 ; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            ps = connection.prepareStatement(query3);
            ps.setInt(1, obj.getInt("docNum"));
            ps.setString(2, obj.get("title").toString());
            ps.setString(3, obj.get("name").toString());
            ps.setString(4, obj.get("content").toString());
            ps.setString(5, obj.get("attach_type").toString());
            ps.setString(6, id);
            ps.setString(7,lgDetails.getLgType());
            ps.executeUpdate();
        }
        
        String query4 = "SELECT * FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ? AND LG_TYPE =? AND LG_ACTION = ? AND APPROVAL_PERSON_SEQ = (SELECT TO_NUMBER(MIN(APPROVAL_PERSON_SEQ)) FROM XXX_LG_APPROVAL_SETUP)";
        ps = connection.prepareStatement(query4);
        ps.setString(1, lgDetails.getCreateByName());
        ps.setString(2, lgDetails.getLgType());
        ps.setString(3, lgDetails.getLgAction());
        rs = ps.executeQuery();
        while (rs.next()) {
            
            String query5 = "INSERT INTO XXX_LG_WORKFLOW_APPROVAL(REQUESTER_PERSON_NUM,REQUESTER_PERSON_NAME,APPROVAL_PERSON_NAME,APPROVAL_PERSON_NUM,APPROVAL_PERSON_SEQ,APPROVAL_STATUS,LG_DOCUMENT_NUM,LG_TYPE,LG_ACTION,ACTION_SEQ) VALUES(?,?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(query5);
            ps.setString(1, lgDetails.getCreateBy());
            ps.setString(2, lgDetails.getCreateByName());
            ps.setString(3, rs.getString("APPROVAL_PERSON_NAME"));
            ps.setString(4, rs.getString("APPROVAL_PERSON_NUM"));
            ps.setString(5, rs.getString("APPROVAL_PERSON_SEQ"));
            ps.setString(6, "Pending Approval");
            ps.setString(7, Integer.toString(lgDetails.getDocumentNumber()));
            ps.setString(8, lgDetails.getLgType());
            ps.setString(9, lgDetails.getLgAction());
            ps.setString(10, lgDetails.getActionSeq());

            ps.executeUpdate();
        }
        
        String query7 = "SELECT * FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ? and LG_TYPE = ? and LG_ACTION = ?";
        ps = connection.prepareStatement(query7);
        ps.setString(1, lgDetails.getCreateByName());
        ps.setString(2, lgDetails.getLgType());
        ps.setString(3, lgDetails.getLgAction());
        rs = ps.executeQuery();

        while (rs.next()) {
            String query8 = "INSERT INTO XXX_LG_APPROVAL_LIST(REQUESTER_NAME,APPROVAL_NAME,APRROVAL_NAME_SEQ,APPROVAL_LEVEL,LG_TYPE,LG_ACTION,LG_DOCUMENT_NUM,LG_ACTION_SEQ,APPROVAL_STATUS) VALUES(?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(query8);
            ps.setString(1, rs.getString("REQUESTER_PERSON"));
            ps.setString(2, rs.getString("APPROVAL_PERSON_NAME"));
            ps.setString(3, rs.getString("APPROVAL_PERSON_SEQ"));
            ps.setString(4, rs.getString("LEVEL_APPROVAL"));
            ps.setString(5, rs.getString("LG_TYPE"));
            ps.setString(6, rs.getString("LG_ACTION"));
            ps.setString(7, Integer.toString(lgDetails.getDocumentNumber()));
            ps.setString(8, lgDetails.getActionSeq());
            ps.setString(9, "Pending Approval");
            ps.executeUpdate();
        }
        
        return "lg action approval successfully";
    }
    
    public JSONArray allActionApprovalList(String documentNumber,String lgType,String lgActionSeq,String lgAction) throws SQLException {

        JSONArray responseArr = new JSONArray();

        try {
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_APPROVAL_LIST WHERE LG_DOCUMENT_NUM = ? AND LG_TYPE = ? AND LG_ACTION = ? AND LG_ACTION_SEQ = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, documentNumber);
            ps.setString(2, lgType);
            ps.setString(3, lgAction);
            ps.setString(4, lgActionSeq);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                JSONObject workFlowObj = new JSONObject();
                workFlowObj.put("approvalPersonName", rs.getString("APPROVAL_NAME"));
                workFlowObj.put("approvalStatus",  rs.getString("APPROVAL_STATUS"));
                workFlowObj.put("requesterName", rs.getString("REQUESTER_NAME"));
                workFlowObj.put("approvalLevel", rs.getString("APPROVAL_LEVEL"));
                workFlowObj.put("approvalPersonSeq", rs.getString("APRROVAL_NAME_SEQ"));    
            
                responseArr.put(workFlowObj);
         }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return responseArr;
    }
}
