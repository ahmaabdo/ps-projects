package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.tbcLG.bean.ApprovalValidationBean;
import com.appspro.tbcLG.bean.EmployeeBean;

import java.io.IOException;

import java.net.MalformedURLException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EmployeesDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public JSONArray getAllEmployees() throws MalformedURLException,
                                              IOException {

        JSONArray responseArr = new JSONArray();
        JSONObject returnresponse = new JSONObject();

        String serverUrl = rh.getInstanceUrl() + rh.getAllEmployeesUrl();

        returnresponse = rh.httpGet(serverUrl);

        JSONArray arr = returnresponse.getJSONArray("items");
        for (int i = 0; i < arr.length(); i++) {

            JSONObject reponseObj = new JSONObject();
            JSONObject projectObj = arr.getJSONObject(i);
            reponseObj.put("DisplayName", projectObj.getString("DisplayName"));
            reponseObj.put("PersonNumber",
                           projectObj.getString("PersonNumber"));
            reponseObj.put("PersonId", projectObj.getLong("PersonId"));
            reponseObj.put("UserName", projectObj.get("UserName").toString());

            responseArr.put(reponseObj);
        }

        return responseArr;
    }

    public String getEmployeeDetails(String username) {

        JSONObject obj = new JSONObject();
        JSONObject errorObj = new JSONObject();
        JSONArray empArr = new JSONArray();
        JSONArray actionArr = new JSONArray();

        try {
            String payload =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:findUserDetailsByUsername>\n" +
                "         <typ:Username>" + username + "</typ:Username>\n" +
                "      </typ:findUserDetailsByUsername>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";

            Document doc = new RestHelper().httpPost(new RestHelper().getInstanceUrl() + "/hcmService/UserDetailsServiceV2?invoke=", payload);

            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();

            if (root.getElementsByTagName("ns1:Value").getLength() != 0) {
                obj.put("PersonId", String.valueOf(root.getElementsByTagName("ns1:PersonId").item(0).getTextContent()));
                obj.put("Username", root.getElementsByTagName("ns1:Username").item(0).getTextContent());
                obj.put("DisplayName", root.getElementsByTagName("ns1:DisplayName").item(0).getTextContent());
                obj.put("PersonNumber", root.getElementsByTagName("ns1:PersonNumber").item(0).getTextContent());

                try {
                    connection = AppsproConnection.getConnection();
                    String query =  "SELECT TYPE_ACTION,LG_TYPE,LG_ACTION FROM XXX_LG_APPROVAL_VALIDATION WHERE EMPLOYEE_EMAIL = ? ";
                    ps = connection.prepareStatement(query);
                    ps.setString(1, username);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        JSONObject empObj = new JSONObject();
                        JSONObject actionObj = new JSONObject();
                        empObj.put("typeAction", rs.getString("TYPE_ACTION"));
                        if (!(rs.getString("LG_ACTION").equals("None"))) {
                            actionObj.put("typeAction",  rs.getString("TYPE_ACTION"));
                            actionObj.put("lgType",  rs.getString("LG_TYPE"));
                            actionObj.put("lgAction", rs.getString("LG_ACTION"));
                            actionArr.put(actionObj);
                        }
                        else{
                            empObj.put("lgType", rs.getString("LG_TYPE"));
                            empArr.put(empObj);
                        }
                    }
                    obj.put("validation", empArr);
                    obj.put("Action", actionArr);
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw e;
                } finally {
                    closeResources(connection, ps, rs);
                }
                errorObj.put("Status", "Done");
                errorObj.put("Data", obj);
            } else {
                errorObj.put("Status", "Error");
                errorObj.put("Data", "Invalid Username");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return errorObj.toString();
    }
}
