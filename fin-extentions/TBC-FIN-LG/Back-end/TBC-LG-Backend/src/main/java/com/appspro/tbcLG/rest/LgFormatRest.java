package com.appspro.tbcLG.rest;

import com.appspro.mail.LgFormat;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

@Path("lgFormat")

public class LgFormatRest {
   
   @POST
   @Path("/extendLgFormat")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public static String lgFormat (String body) {
       JSONObject obj = new JSONObject();
       JSONObject bodyObj = new JSONObject(body);
       LgFormat lg = new LgFormat();
       String response;
       
       try {
            response = lg.generateWord(bodyObj);
            obj.put("Status", "OK");
            obj.put("Data", response);
        } catch (FileNotFoundException e) {
            obj.put("Status", "Error");
            obj.put("Data", e.getMessage());
        } catch (IOException e) {
            obj.put("Status", "Error");
            obj.put("Data", e.getMessage());
        }
        return obj.toString();
   }
   
    @POST
    @Path("/extendReleaseLgFormat")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static String extendReleaseLgFormat (String body) {
        JSONObject obj = new JSONObject();
        JSONObject bodyObj = new JSONObject(body);
        LgFormat lg = new LgFormat();
        String response;
        
        try {
             response = lg.releaseLgFormat(bodyObj);
             obj.put("Status", "OK");
             obj.put("Data", response);
         } catch (FileNotFoundException e) {
             obj.put("Status", "Error");
             obj.put("Data", e.getMessage());
         } catch (IOException e) {
             obj.put("Status", "Error");
             obj.put("Data", e.getMessage());
         }
         return obj.toString();
    }
}
