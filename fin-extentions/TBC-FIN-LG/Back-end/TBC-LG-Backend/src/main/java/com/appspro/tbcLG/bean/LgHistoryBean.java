package com.appspro.tbcLG.bean;

public class LgHistoryBean {
    private int id;
    private int documentNumber;
    private String lgEndDate;
    private String lgType;
    private String bankAmount;
    private String lgAmount;
    private String lgStatus;
    private String modDate;
    private String history;
    private String operation;
    private String attachtNum;
    private String actionSeq;
    private String createBy;
    private String createByName;
    private String lgActionStatus;
    private String lgAction;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDocumentNumber(int documentNumber) {
        this.documentNumber = documentNumber;
    }

    public int getDocumentNumber() {
        return documentNumber;
    }

    public void setLgEndDate(String lgEndDate) {
        this.lgEndDate = lgEndDate;
    }

    public String getLgEndDate() {
        return lgEndDate;
    }

    public void setLgType(String lgType) {
        this.lgType = lgType;
    }

    public String getLgType() {
        return lgType;
    }

    public void setBankAmount(String bankAmount) {
        this.bankAmount = bankAmount;
    }

    public String getBankAmount() {
        return bankAmount;
    }

    public void setLgAmount(String lgAmount) {
        this.lgAmount = lgAmount;
    }

    public String getLgAmount() {
        return lgAmount;
    }

    public void setModDate(String modDate) {
        this.modDate = modDate;
    }

    public String getModDate() {
        return modDate;
    }

    public void setLgStatus(String lgStatus) {
        this.lgStatus = lgStatus;
    }

    public String getLgStatus() {
        return lgStatus;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getHistory() {
        return history;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setAttachtNum(String attachtNum) {
        this.attachtNum = attachtNum;
    }

    public String getAttachtNum() {
        return attachtNum;
    }

    public void setActionSeq(String actionSeq) {
        this.actionSeq = actionSeq;
    }

    public String getActionSeq() {
        return actionSeq;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateByName(String createByName) {
        this.createByName = createByName;
    }

    public String getCreateByName() {
        return createByName;
    }

    public void setLgActionStatus(String lgActionStatus) {
        this.lgActionStatus = lgActionStatus;
    }

    public String getLgActionStatus() {
        return lgActionStatus;
    }

    public void setLgAction(String lgAction) {
        this.lgAction = lgAction;
    }

    public String getLgAction() {
        return lgAction;
    }
}
