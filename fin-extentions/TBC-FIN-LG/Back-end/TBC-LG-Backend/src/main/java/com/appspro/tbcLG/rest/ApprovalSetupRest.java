package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.ApprovalSetupBean;
import com.appspro.tbcLG.dao.ApprovalSetupDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

@Path("approvalSetup")

public class ApprovalSetupRest {
   
    JSONObject returnedResponse = new JSONObject();
    ApprovalSetupDAO approvalDao = new ApprovalSetupDAO();
    
    @GET
    @Path("empsApproval")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getCreationEmployees (){
        
        try {
            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Data", approvalDao.allEmpsApproval());
        } catch (SQLException e) {
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }

        return returnedResponse.toString();
    }
    
    @GET
    @Path("empApproval/{requesterPerson}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getCreationEmployee (@PathParam("requesterPerson") String requesterPerson){
        
        try {
            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Data", approvalDao.allEmployeeApproval(requesterPerson));
        } catch (SQLException e) {
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }

        return returnedResponse.toString();
    }
    
    @POST
    @Path("empApproval")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addEmployeeApproval (String body){
        
        JSONObject bodyObj = new JSONObject(body);
        JSONObject returnedResponse = new JSONObject();
        ApprovalSetupBean bean  = new ApprovalSetupBean();

        try {
            bean.setRequesterName(bodyObj.get("requesterName").toString());
            bean.setRequesterNumber(bodyObj.get("requesterNumber").toString());
            bean.setRequesterEmail(bodyObj.get("requesterEmail").toString()); 
            bean.setApprovalPersonName(bodyObj.get("approvalPersonName").toString());
            bean.setApprovalPersonNumber(bodyObj.get("approvalPersonNum").toString());
            bean.setApprovalPersonSeq(bodyObj.get("approvalPersonSeq").toString());
            bean.setTypeAction(bodyObj.get("typeAction").toString());
            bean.setLgType(bodyObj.get("lgType").toString());
            bean.setLevelApproval(bodyObj.get("levelApproval").toString());
            bean.setLgAction(bodyObj.get("lgAction").toString());

            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Status", approvalDao.addEmployeeApproval(bean));
            returnedResponse.put("Data", bodyObj);
            
        } catch (SQLException e) {
            e.printStackTrace();
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }
        return returnedResponse.toString();
    }
    
    
    @GET
    @Path("empApproval/{requesterPerson}/{lgType}/{approvalPer}/{lgAction}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteApprovalEmployee (@PathParam("requesterPerson") String userName ,@PathParam("lgType") String lgType ,@PathParam("approvalPer") String approvalPer,@PathParam("lgAction") String lgAction){
        
        try {
            System.out.println(lgType);
            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Status", approvalDao.deleteApprovalEmployee(userName,lgType,approvalPer,lgAction));
        } catch (SQLException e) {
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }
        
        return returnedResponse.toString();
    }
   
}
