package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.tbcLG.bean.ApprovalValidationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApprovalValidationDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public String addCreationEmployee(ApprovalValidationBean bean) throws SQLException {
        try {
            connection = AppsproConnection.getConnection();

            String query2 =  "INSERT INTO XXX_LG_APPROVAL_VALIDATION(EMPLOYEE_NAME,EMPLOYEE_NUMBER,EMPLOYEE_EMAIL,TYPE_ACTION,LG_TYPE,LG_ACTION) VALUES(?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query2);
            ps.setString(1, bean.getEmployeeName());
            ps.setString(2, bean.getEmployeeNumber());
            ps.setString(3, bean.getEmployeeEmail());
            ps.setString(4, bean.getTypeAction());
            ps.setString(5, bean.getLgType());
            ps.setString(6, bean.getLgAction());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Creation Approval added Successfully";
    }

    public JSONArray allCreationEmployee() throws SQLException {

        ArrayList<ApprovalValidationBean> creationEmpList =  new ArrayList<ApprovalValidationBean>();
        JSONArray arr = new JSONArray();
        try {

            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_APPROVAL_VALIDATION";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                ApprovalValidationBean empBean = new ApprovalValidationBean();
                empBean.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
                empBean.setEmployeeNumber(rs.getString("EMPLOYEE_NUMBER"));
                empBean.setEmployeeEmail(rs.getString("EMPLOYEE_EMAIL"));
                empBean.setTypeAction(rs.getString("TYPE_ACTION"));
                empBean.setLgType(rs.getString("LG_TYPE"));
                empBean.setLgAction(rs.getString("LG_ACTION"));

                creationEmpList.add(empBean);
            }

            arr = new JSONArray(creationEmpList);
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }
    
    public String updateCreationEmployee(String userName,String lgType , JSONObject obj) throws SQLException {
        
    try {
            connection = AppsproConnection.getConnection();
            String query = "UPDATE XXX_LG_APPROVAL_VALIDATION SET TYPE_ACTION = ? , LG_TYPE = ? , LG_ACTION = ? WHERE EMPLOYEE_EMAIL = ? AND LG_TYPE = ? AND LG_ACTION = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, obj.get("typeAction").toString());
            ps.setString(2, obj.get("lgType").toString());
            ps.setString(3, obj.get("lgAction").toString());
            ps.setString(4, userName);
            ps.setString(5, lgType);
            ps.setString(6, obj.get("lgSelectedAction").toString());

            ps.executeUpdate();
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
    
        return "Creation approval deleted Successfully";
    }
    
    public String deleteCreationEmployee(String userName , String lgType , String lgAction) throws SQLException {
        
    try {
            connection = AppsproConnection.getConnection();
            String query = "DELETE FROM XXX_LG_APPROVAL_VALIDATION WHERE EMPLOYEE_EMAIL = ? AND LG_TYPE = ? AND LG_ACTION = ?";
            int checkResult = 0;
            ps = connection.prepareStatement(query);
            ps.setString(1, userName);
            ps.setString(2, lgType);
            ps.setString(3, lgAction);
            checkResult = ps.executeUpdate();
            
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
    
        return "Creation approval deleted Successfully";
    }
    
}
