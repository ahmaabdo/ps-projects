package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.tbcLG.bean.ApprovalSetupBean;
import com.appspro.tbcLG.bean.LetterGuaranteeBean;

import com.appspro.tbcLG.bean.LgHistoryBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.ws.rs.HEAD;

import org.json.JSONArray;
import org.json.JSONObject;

public class LetterGuaranteeDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps1;
    PreparedStatement ps2;

    CallableStatement cs;
    ResultSet rs;
    ResultSet rs2;
    RestHelper rh = new RestHelper();

    public JSONArray getAllLgDetails() throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LetterGuaranteeBean> lgDetailsList = new ArrayList<LetterGuaranteeBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_NUMBER";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                LetterGuaranteeBean lgBean = new LetterGuaranteeBean();

                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setContractNumber(rs.getString("CONTRACT_NUMBER"));
                lgBean.setContractAmount(rs.getString("CONTRACT_AMOUNT"));
                lgBean.setSupplier(rs.getString("SUPPLIER"));
                lgBean.setContractStartDate(rs.getString("CONTRACT_START_DATE"));
                lgBean.setContractEndDate(rs.getString("CONTRACT_END_DATE"));
                lgBean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("LG_AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setRegion(rs.getString("REGION"));
                lgBean.setBankAccount(rs.getString("BANK_ACCOUNT"));
                lgBean.setDescription(rs.getString("DESCRIPTION"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                lgBean.setCreatedBy(rs.getString("CREATED_BY"));
                lgBean.setLgStartDate(rs.getString("LG_START_DATE"));
                lgBean.setNegotiationNumber(rs.getString("NEGOTIATION_NUM"));
                lgBean.setNegotiationTitle(rs.getString("NEGOTIATION_TITLE"));
                lgBean.setNegotiationAmount(rs.getString("NEGOTIATION_AMOUNT"));
                lgBean.setLgAmountPercnatge(rs.getString("LG_AMOUNT_PERC"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }

    public JSONArray getLg(String docNum) throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LetterGuaranteeBean> lgDetailsList = new ArrayList<LetterGuaranteeBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_NUMBER WHERE DOCUMENT_NUMBER = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, docNum);

            rs = ps.executeQuery();
            while (rs.next()) {
                LetterGuaranteeBean lgBean = new LetterGuaranteeBean();

                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setContractNumber(rs.getString("CONTRACT_NUMBER"));
                lgBean.setContractAmount(rs.getString("CONTRACT_AMOUNT"));
                lgBean.setSupplier(rs.getString("SUPPLIER"));
                lgBean.setContractStartDate(rs.getString("CONTRACT_START_DATE"));
                lgBean.setContractEndDate(rs.getString("CONTRACT_END_DATE"));
                lgBean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("LG_AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setRegion(rs.getString("REGION"));
                lgBean.setBankAccount(rs.getString("BANK_ACCOUNT"));
                lgBean.setDescription(rs.getString("DESCRIPTION"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                lgBean.setCreatedBy(rs.getString("CREATED_BY"));
                lgBean.setLgStartDate(rs.getString("LG_START_DATE"));
                lgBean.setNegotiationNumber(rs.getString("NEGOTIATION_NUM"));
                lgBean.setNegotiationTitle(rs.getString("NEGOTIATION_TITLE"));
                lgBean.setNegotiationAmount(rs.getString("NEGOTIATION_AMOUNT"));
                lgBean.setLgAmountPercnatge(rs.getString("LG_AMOUNT_PERC"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }

    public String addLgDetails(LetterGuaranteeBean lgDetails,  LgHistoryBean history,  JSONArray arr) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();
            String id = "";
            String query = "insert into XXX_LG_NUMBER (CONTRACT_NUMBER,CONTRACT_AMOUNT,SUPPLIER,LG_NUM_FROM_BANK,LG_TYPE,LG_AMOUNT,LG_END_DATE,REGION,BANK_ACCOUNT,DESCRIPTION,STATUS,DOCUMENT_NUMBER,APPROVAL_STATUS,CREATED_BY,LG_START_DATE,NEGOTIATION_NUM,NEGOTIATION_TITLE,NEGOTIATION_AMOUNT,LG_AMOUNT_PERC) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, lgDetails.getContractNumber());
            ps.setString(2, lgDetails.getContractAmount());
            ps.setString(3, lgDetails.getSupplier());
            ps.setString(4, lgDetails.getLgNumFromBank());
            ps.setString(5, lgDetails.getLgType());
            ps.setString(6, lgDetails.getLgAmount());
            ps.setString(7, lgDetails.getLgEndDate());
            ps.setString(8, lgDetails.getRegion());
            ps.setString(9, lgDetails.getBankAccount());
            ps.setString(10, lgDetails.getDescription());
            ps.setString(11, lgDetails.getLgStatus());
            ps.setInt(12, lgDetails.getDocumentNumber());
            ps.setString(13, lgDetails.getApprovalStatus());
            ps.setString(14, lgDetails.getCreatedBy());
            ps.setString(15, lgDetails.getLgStartDate());
            ps.setString(16, lgDetails.getNegotiationNumber());
            ps.setString(17, lgDetails.getNegotiationTitle());
            ps.setString(18, lgDetails.getNegotiationAmount());
            ps.setString(19, lgDetails.getLgAmountPercnatge());

            ps.executeUpdate();

            String query2 = "SELECT XXX_LG_ATTACH.NEXTVAL AS NEXT_ID FROM dual";
            ps = connection.prepareStatement(query2);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getString("NEXT_ID");
            }

            String query3 =  "INSERT INTO XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,HISTORY,MOD_DATE,OPERATION,ATTACHMENT_NUM,ACTION_SEQ,LG_ACTION_STATUS) VALUES(?,?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'None',?,?,?)";
            ps = connection.prepareStatement(query3);
            ps.setString(1, history.getLgType());
            ps.setString(2, history.getLgAmount());
            ps.setString(3, history.getLgEndDate());
            ps.setString(4, history.getBankAmount());
            ps.setString(5, history.getLgStatus());
            ps.setInt(6, history.getDocumentNumber());
            ps.setString(7, "original");
            ps.setString(8, id);
            ps.setString(9, "1");
            ps.setString(10, "None");
            
            ps.executeUpdate();

            String query4 = "INSERT INTO XXX_LG_ATTACHMENT(DOC_NUM,TITLE,NAME,CONTENT,ATTACHMENT_TYPE,ATTACHMENT_NUM,LG_TYPE) VALUES(?,?,?,?,?,?,?)";
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                ps = connection.prepareStatement(query4);
                ps.setInt(1, obj.getInt("docNum"));
                ps.setString(2, obj.get("title").toString());
                ps.setString(3, obj.get("name").toString());
                ps.setString(4, obj.get("content").toString());
                ps.setString(5, obj.get("attach_type").toString());
                ps.setString(6, id);
                ps.setString(7, obj.get("lg_type").toString());

                ps.executeUpdate();
            }
            
            String query5 = "SELECT * FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ? AND LG_TYPE =? AND LG_ACTION = ? AND APPROVAL_PERSON_SEQ = (SELECT TO_NUMBER(MIN(APPROVAL_PERSON_SEQ)) FROM XXX_LG_APPROVAL_SETUP)";
            ps = connection.prepareStatement(query5);
            ps.setString(1, lgDetails.getCreatedByName());
            ps.setString(2, lgDetails.getLgType());
            ps.setString(3, lgDetails.getLgAction());
            rs = ps.executeQuery();
            while (rs.next()) {
                JSONObject appObj = new JSONObject();
                appObj.put("APPROVAL_PERSON_NUM", rs.getString("APPROVAL_PERSON_NUM"));
                appObj.put("APPROVAL_PERSON_NAME", rs.getString("APPROVAL_PERSON_NAME"));
                appObj.put("APPROVAL_PERSON_SEQ", rs.getString("APPROVAL_PERSON_SEQ"));
                appObj.put("LEVEL_APPROVAL", rs.getString("LEVEL_APPROVAL"));
                
                String query6 = "INSERT INTO XXX_LG_WORKFLOW_APPROVAL(REQUESTER_PERSON_NUM,REQUESTER_PERSON_NAME,APPROVAL_PERSON_NAME,APPROVAL_PERSON_NUM,APPROVAL_PERSON_SEQ,APPROVAL_STATUS,LG_DOCUMENT_NUM,LG_TYPE,LG_ACTION) VALUES(?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query6);
                ps.setString(1, lgDetails.getCreatedBy());
                ps.setString(2, lgDetails.getCreatedByName());
                ps.setString(3, appObj.get("APPROVAL_PERSON_NAME").toString());
                ps.setString(4, appObj.get("APPROVAL_PERSON_NUM").toString());
                ps.setString(5, appObj.get("APPROVAL_PERSON_SEQ").toString());
                ps.setString(6, "Pending Approval");
                ps.setString(7, Integer.toString(lgDetails.getDocumentNumber()));
                ps.setString(8, lgDetails.getLgType());
                ps.setString(9, lgDetails.getLgAction());

                ps.executeUpdate();
            }
            
            String query7 = "SELECT * FROM XXX_LG_APPROVAL_SETUP WHERE REQUESTER_PERSON = ? and LG_TYPE = ? and LG_ACTION = ?";
            ps = connection.prepareStatement(query7);
            ps.setString(1, lgDetails.getCreatedByName());
            ps.setString(2, lgDetails.getLgType());
            ps.setString(3, lgDetails.getLgAction());
            rs2 = ps.executeQuery();

            while (rs2.next()) {
                String query8 = "INSERT INTO XXX_LG_APPROVAL_LIST(REQUESTER_NAME,APPROVAL_NAME,APRROVAL_NAME_SEQ,APPROVAL_LEVEL,LG_TYPE,LG_ACTION,LG_DOCUMENT_NUM,LG_ACTION_SEQ,APPROVAL_STATUS) VALUES(?,?,?,?,?,?,?,?,?)";
                ps = connection.prepareStatement(query8);
                ps.setString(1, rs2.getString("REQUESTER_PERSON"));
                ps.setString(2, rs2.getString("APPROVAL_PERSON_NAME"));
                ps.setString(3, rs2.getString("APPROVAL_PERSON_SEQ"));
                ps.setString(4, rs2.getString("LEVEL_APPROVAL"));
                ps.setString(5, rs2.getString("LG_TYPE"));
                ps.setString(6, rs2.getString("LG_ACTION"));
                ps.setString(7, Integer.toString(lgDetails.getDocumentNumber()));
                ps.setString(8, "1");
                ps.setString(9, "Pending Approval");
                ps.executeUpdate();
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(rs2);
        }
        return "lg added successfully";
    }

    public String updateLgDetails(LetterGuaranteeBean lgDetails) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query = "update  XXX_LG_NUMBER set  CONTRACT_NUMBER=?,CONTRACT_AMOUNT=?,SUPPLIER=?,LG_NUM_FROM_BANK=?,LG_TYPE=?,LG_AMOUNT=?,LG_END_DATE=?,REGION=?,BANK_ACCOUNT=?,DESCRIPTION=? ,STATUS=?,APPROVAL_STATUS=?,NEGOTIATION_NUM=?,NEGOTIATION_TITLE=?,NEGOTIATION_AMOUNT=?, LG_START_DATE=?, LG_AMOUNT_PERC=? where DOCUMENT_NUMBER=? and LG_TYPE =? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgDetails.getContractNumber());
            ps.setString(2, lgDetails.getContractAmount());
            ps.setString(3, lgDetails.getSupplier());
            ps.setString(4, lgDetails.getLgNumFromBank());
            ps.setString(5, lgDetails.getLgType());
            ps.setString(6, lgDetails.getLgAmount());
            ps.setString(7, lgDetails.getLgEndDate());
            ps.setString(8, lgDetails.getRegion());
            ps.setString(9, lgDetails.getBankAccount());
            ps.setString(10, lgDetails.getDescription());
            ps.setString(11, lgDetails.getLgStatus());
            ps.setString(12, lgDetails.getApprovalStatus());
            ps.setString(13, lgDetails.getNegotiationTitle());
            ps.setString(14, lgDetails.getNegotiationNumber());
            ps.setString(15, lgDetails.getNegotiationAmount());
            ps.setString(16, lgDetails.getLgStartDate());
            ps.setString(17, lgDetails.getLgAmountPercnatge());
            ps.setInt(18, lgDetails.getDocumentNumber());
            ps.setString(19, lgDetails.getLgType());
            ps.executeUpdate();

            String query2 = "update  XXX_LG_HISTORY set  LG_END_DATE=? , AMOUNT=? where DOCUMENT_NUMBER=? and HISTORY = ?";
            ps = connection.prepareStatement(query2);
            ps.setString(1, lgDetails.getLgEndDate());
            ps.setString(2, lgDetails.getLgAmount());
            ps.setInt(3, lgDetails.getDocumentNumber());
            ps.setString(4, "original");

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return "lg updated successfully";
    }
    
    public String updateContractStatus(LetterGuaranteeBean lgDetails) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query2 = " update  XXX_LG_NUMBER set APPROVAL_STATUS=? ,STATUS=? where DOCUMENT_NUMBER=? and LG_TYPE =?";
            ps = connection.prepareStatement(query2);
            ps.setString(1, lgDetails.getApprovalStatus());
            ps.setString(2, lgDetails.getLgStatus());
            ps.setInt(3, lgDetails.getDocumentNumber());
            ps.setString(4, lgDetails.getLgType());
            ps.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return "lg updated successfully";
    }

    public String deleteLgDetails(LetterGuaranteeBean bean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();
            String query;
            int checkResult = 0;
            query = "DELETE from XXX_LG_NUMBER where DOCUMENT_NUMBER = ? and  LG_TYPE =?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, bean.getDocumentNumber());
            ps.setString(1, bean.getLgType());

            checkResult = ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            ps.close();
        }
        return "Lg Deleted Successfully";
    }

    public JSONArray getLastRowLg() throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LetterGuaranteeBean> lgDetailsList = new ArrayList<LetterGuaranteeBean>();
            connection = AppsproConnection.getConnection();
            String query = "select * from XXX_LG_NUMBER where DOCUMENT_NUMBER=(SELECT MAX(DOCUMENT_NUMBER)from XXX_LG_NUMBER) ";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                LetterGuaranteeBean lgBean = new LetterGuaranteeBean();
                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));

                lgBean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }
        return arr;
    }
}
