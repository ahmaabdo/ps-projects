package com.appspro.tbcLG.bean;

public class BankAccountBean {
    
   private String bankAccount;
   private String cashAccountComb;
   private String bankAccountName;
   private String bankDescription;
   

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setCashAccountComb(String cashAccountComb) {
        this.cashAccountComb = cashAccountComb;
    }

    public String getCashAccountComb() {
        return cashAccountComb;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankDescription(String bankDescription) {
        this.bankDescription = bankDescription;
    }

    public String getBankDescription() {
        return bankDescription;
    }
}
