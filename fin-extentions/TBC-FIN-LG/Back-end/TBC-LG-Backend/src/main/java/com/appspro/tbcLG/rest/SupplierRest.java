package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.SuppilerBean;

import com.appspro.tbcLG.dao.SupplierDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;


@Path("/supplier")
public class SupplierRest {
   
    @GET
    @Path("/getSuppliers")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response suppliers () throws MalformedURLException,IOException {
        
        ArrayList<SuppilerBean> supplierList = new ArrayList<SuppilerBean>();
        SupplierDAO supplierDao = new SupplierDAO();
        
        supplierList = supplierDao.getAllSupplier(); 
        JSONArray array = new JSONArray(supplierList);
        return  Response.ok(array.toString()).build();
    }
   
}
