package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.WorkFlowNotificationBean;
import com.appspro.tbcLG.dao.WorkFlowNotificationDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("notification")
public class WorkFlowNotificationRest {
    WorkFlowNotificationDAO serivces = new WorkFlowNotificationDAO();
    WorkFlowNotificationBean bean = new WorkFlowNotificationBean();
    JSONObject resposne = new JSONObject();
    JSONArray arr = new JSONArray();
    String status;

    @GET
    @Path("/getNotif/{empId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNotification(@PathParam("empId")
        String empId) {

        try {
            arr = serivces.getNotificationByEmpId(empId);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }

    @POST
    @Path("/updateNotifyStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateNotifyStatus(String body) {

        try {
            JSONObject obj2 = new JSONObject(body);
            String approvalStatus = obj2.getString("approvalStatus");
            bean.setDocumentNumber(obj2.getInt("documentNumber"));
            bean.setApprovalStatus(obj2.getString("approvalStatus"));
            bean.setLgNumFromBank(obj2.getString("lgNumber"));
            bean.setCreatedBy(obj2.getString("createdBy"));
            bean.setRequesterNumber(obj2.getString("requesterNumber"));
            bean.setApprovalPersonSeq(obj2.getString("approvalSeq"));
            bean.setRequesterName(obj2.getString("requesterName"));
            bean.setLgStatus(obj2.getString("lgStatus"));
            bean.setLgType(obj2.getString("lgType"));
            bean.setLgAction(obj2.getString("lgAction"));
            bean.setAmount(obj2.getString("lgAmount"));
            bean.setLgEndDate(obj2.getString("lgEndDate"));
            bean.setActionSeq(obj2.getString("actionSeq"));

            status = serivces.updateNotifyStatus(bean, approvalStatus);
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @POST
    @Path("/workFlowList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allEmpsApprovalList(String body) {

        JSONObject obj = new JSONObject(body);
        JSONArray responseObj = new JSONArray();
        String lgType = obj.get("lgType").toString();
        String lgDocumentNumber = obj.get("documentNumber").toString();

        try {
            responseObj =  serivces.allEmpsApprovalList(lgDocumentNumber, lgType);
            resposne.put("Status", "OK");
            resposne.put("Data", responseObj);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
}
