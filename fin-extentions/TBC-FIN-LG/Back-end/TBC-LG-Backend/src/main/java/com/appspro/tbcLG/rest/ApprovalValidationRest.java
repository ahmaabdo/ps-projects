package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.ApprovalValidationBean;

import com.appspro.tbcLG.dao.ApprovalValidationDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

@Path("approvalSetup")
public class ApprovalValidationRest {
    
    JSONObject returnedResponse = new JSONObject();
    ApprovalValidationDAO approvalDao = new ApprovalValidationDAO();
   
    @POST
    @Path("creationEmployee")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addCreationEmployee (String body){
        
        JSONObject bodyObj = new JSONObject(body);
        JSONObject returnedResponse = new JSONObject();
        ApprovalValidationBean bean  = new ApprovalValidationBean();

        try {
            bean.setEmployeeName(bodyObj.get("employeeName").toString());
            bean.setEmployeeNumber(bodyObj.get("employeeNumber").toString());
            bean.setEmployeeEmail(bodyObj.get("employeeEmail").toString()); 
            bean.setTypeAction(bodyObj.get("typeAction").toString());
            bean.setLgType(bodyObj.get("lgType").toString());
            bean.setLgAction(bodyObj.get("lgAction").toString());

            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Status", approvalDao.addCreationEmployee(bean));
            returnedResponse.put("Data", bodyObj);
            
        } catch (SQLException e) {
            e.printStackTrace();
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }
        return returnedResponse.toString();
    }
    
    @GET
    @Path("creationEmployees")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getCreationEmployee (){
        
        try {
            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Data", approvalDao.allCreationEmployee());
        } catch (SQLException e) {
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }

        return returnedResponse.toString();
    }
    
    @POST
    @Path("creationEmployees/{userName}/{lgType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateCreationEmployee (@PathParam("userName") String userName ,@PathParam("lgType") String lgType,String body){
        
        try {
            JSONObject obj = new JSONObject(body);
            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Status", approvalDao.updateCreationEmployee(userName,lgType,obj));
            returnedResponse.put("Data", obj);
        } catch (SQLException e) {
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }
        
        return returnedResponse.toString();
    }
    
    @GET
    @Path("creationEmployees/{userName}/{lgType}/{lgAction}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteCreationEmployee (@PathParam("userName") String userName , @PathParam("lgType") String lgType, @PathParam("lgAction") String lgAction){
        
        try {
            returnedResponse.put("StatusCode", "OK");
            returnedResponse.put("Data", approvalDao.deleteCreationEmployee(userName,lgType,lgAction));
        } catch (SQLException e) {
            returnedResponse.put("Status", "ERROR");
            returnedResponse.put("Data", e.getMessage());
        }
        
        return returnedResponse.toString();
    }
    
}
