package com.appspro.tbcLG.dao;

import biPReports.RestHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

public class CreateExternalCashDAO  extends RestHelper {
  
    public JSONObject cashTransaction(String body)  {
        
        System.out.println(body);
        
        JSONObject res = new JSONObject();
        String restApi;
    try {
        
        restApi = getInstanceUrl()+getCashExternalUrl();
            
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        URL url = new URL(null,restApi,new sun.net.www.protocol.https.Handler());

        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null;
        connection.setRequestMethod("POST");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + "YXBwc3Byby5maW46b3JhY2xlQDEyMw==");
        
        if (body != null && body.length() > 0) {
            connection.setDoOutput(true);
            OutputStream os = connection.getOutputStream();
            byte[] input = body.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        
        int code = connection.getResponseCode();
        InputStream is;
        if (code >= 400) {
            is = connection.getErrorStream();
            res.put("status", "error");
        } else {
            res.put("status", "done");
            is = connection.getInputStream();
        }
        
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        
        try {
            res.put("data", new JSONObject(response.toString()));
         }catch (Exception e) {
            res.put("data", response.toString());
        }
        
        System.out.print("code:" + code);
        if (code >= 400)
            System.out.println(",Error:" + res.get("data").toString());
        else
            System.out.println();
        } 
        catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", "Internal server error");
        }

         return res;
    }
}
