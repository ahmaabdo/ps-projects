package com.appspro.tbcLG.rest;

import com.appspro.tbcLG.bean.LetterGuaranteeBean;
import com.appspro.tbcLG.bean.LgHistoryBean;

import com.appspro.tbcLG.bean.WorkFlowNotificationBean;
import com.appspro.tbcLG.dao.LgHistoryDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/lgHistory")

public class LgHistoryRest {
    LgHistoryDAO serivces = new LgHistoryDAO();
    LgHistoryBean bean = new LgHistoryBean();
    LetterGuaranteeBean lgBean = new LetterGuaranteeBean();
    WorkFlowNotificationBean notifBean = new WorkFlowNotificationBean();
    JSONObject resposne = new JSONObject();
    JSONArray arr = new JSONArray();
    String status;

    @GET
    @Path("getLgHistory/{docNum}/{lgType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getLg(@PathParam("docNum")
        String docNum, @PathParam("lgType")
        String lgType) {
        try {
            arr = serivces.getLgHistory(docNum, lgType);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @POST
    @Path("updateLgExtend")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLgExtend(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                lgBean.setLgEndDate(obj2.getString("lgEndDate"));
                lgBean.setDocumentNumber(obj2.getInt("documentNumber"));
                lgBean.setLgType(obj2.getString("lgType"));
                notifBean.setLgEndDate(obj2.getString("lgEndDate"));
                notifBean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setId(obj2.getInt("id"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setBankAmount(obj2.getString("contractAmount"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                bean.setActionSeq(obj2.getString("actionSeq"));
                JSONArray attachmentArr = obj2.getJSONArray("Attachments");
                status =
                        serivces.updateLgExtend(bean, lgBean, notifBean, attachmentArr);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @POST
    @Path("updateLgIncDec")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLgIncDec(String body) {
        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                lgBean.setLgAmount(obj2.getString("lgAmount"));
                lgBean.setDocumentNumber(obj2.getInt("documentNumber"));
                lgBean.setLgType(obj2.getString("lgType"));
                lgBean.setLgAmountPercnatge(obj2.getString("lgAmountPerc"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setBankAmount(obj2.getString("contractAmount"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                bean.setActionSeq(obj2.getString("actionSeq"));
                JSONArray attachmentArr = obj2.getJSONArray("Attachments");
                status = serivces.updateLgIncDec(bean, lgBean, attachmentArr);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @POST
    @Path("updateLgRelease")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLgRelease(String body) {
        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                lgBean.setLgStatus(obj2.getString("lgStatus"));
                lgBean.setApprovalStatus(obj2.getString("approvalStatus"));
                lgBean.setDocumentNumber(obj2.getInt("documentNumber"));
                lgBean.setLgType(obj2.getString("lgType"));
                bean.setId(obj2.getInt("id"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setBankAmount(obj2.getString("contractAmount"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                bean.setActionSeq(obj2.getString("actionSeq"));
                JSONArray attachmentArr = obj2.getJSONArray("Attachments");
                status = serivces.updateLgRelease(bean, lgBean, attachmentArr);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }

    @GET
    @Path("getLastHistory/{docNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getLastRow(@PathParam("docNum")
        String docNum) {
        JSONObject obj = new JSONObject();

        try {
            obj = serivces.getLatRowAction(docNum);
            resposne.put("Status", "OK");
            resposne.put("Data", obj);

        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }

    @POST
    @Path("lgActionApproval")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String lgActionApproval(String body) {
        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                bean.setActionSeq(obj2.getString("actionSeq"));
                bean.setCreateBy(obj2.getString("createBy"));
                bean.setCreateByName(obj2.getString("createByName"));
                bean.setLgActionStatus(obj2.getString("lgActionStatus"));
                bean.setLgAction(obj2.getString("lgActionType"));
                JSONArray attachmentArr = obj2.getJSONArray("Attachments");
                resposne.put("Status", "OK");
                resposne.put("Data", serivces.lgActionApproval(bean, attachmentArr));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }
    
    @POST
    @Path("/workFlowListAction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String allActionApprovalList(String body) {

        JSONObject obj = new JSONObject(body);
        JSONArray responseObj = new JSONArray();
        String lgType = obj.get("lgType").toString();
        String lgDocumentNumber = obj.get("documentNumber").toString();
        String lgActionSeq = obj.get("lgActionSeq").toString();
        String lgAction = obj.get("lgAction").toString();


        try {
            responseObj =  serivces.allActionApprovalList(lgDocumentNumber,lgType,lgActionSeq,lgAction);
            resposne.put("Status", "OK");
            resposne.put("Data", responseObj);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
}
