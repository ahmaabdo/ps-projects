package com.appspro.tbcLG.bean;

public class ApprovalSetupBean {

    private String requesterName;
    private String requesterNumber;
    private String requesterEmail;
    private String approvalPersonNumber;
    private String approvalPersonName;
    private String approvalPersonSeq;
    private String lgNumber;
    private String approvalStatus;
    private String typeAction;
    private String lgType;
    private String levelApproval;
    private String lgAction;

    
    public void setApprovalPersonNumber(String approvalPersonNumber) {
        this.approvalPersonNumber = approvalPersonNumber;
    }

    public String getApprovalPersonNumber() {
        return approvalPersonNumber;
    }

    public void setApprovalPersonName(String approvalPersonName) {
        this.approvalPersonName = approvalPersonName;
    }

    public String getApprovalPersonName() {
        return approvalPersonName;
    }

    public void setApprovalPersonSeq(String approvalPersonSeq) {
        this.approvalPersonSeq = approvalPersonSeq;
    }

    public String getApprovalPersonSeq() {
        return approvalPersonSeq;
    }

    public void setRequesterNumber(String requesterNumber) {
        this.requesterNumber = requesterNumber;
    }

    public String getRequesterNumber() {
        return requesterNumber;
    }

    public void setRequesterEmail(String requesterEmail) {
        this.requesterEmail = requesterEmail;
    }

    public String getRequesterEmail() {
        return requesterEmail;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setLgNumber(String lgNumber) {
        this.lgNumber = lgNumber;
    }

    public String getLgNumber() {
        return lgNumber;
    }

    public void setTypeAction(String typeAction) {
        this.typeAction = typeAction;
    }

    public String getTypeAction() {
        return typeAction;
    }

    public void setLgType(String lgType) {
        this.lgType = lgType;
    }

    public String getLgType() {
        return lgType;
    }

    public void setLevelApproval(String levelApproval) {
        this.levelApproval = levelApproval;
    }

    public String getLevelApproval() {
        return levelApproval;
    }

    public void setLgAction(String lgAction) {
        this.lgAction = lgAction;
    }

    public String getLgAction() {
        return lgAction;
    }
}
