
package biPReports.rest;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder( { "ImageName", "PrimaryFlag", "ImageId", "PersonId",
                      "ObjectVersionNumber", "links" })
public class Photo {

    @JsonProperty("ImageName")
    private String imageName;
    @JsonProperty("PrimaryFlag")
    private Boolean primaryFlag;
    @JsonProperty("ImageId")
    private Long imageId;
    @JsonProperty("PersonId")
    private Long personId;
    @JsonProperty("ObjectVersionNumber")
    private Long objectVersionNumber;
    @JsonProperty("links")
    private List<Link_> links = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties =
        new HashMap<String, Object>();

    @JsonProperty("ImageName")
    public String getImageName() {
        return imageName;
    }

    @JsonProperty("ImageName")
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @JsonProperty("PrimaryFlag")
    public Boolean getPrimaryFlag() {
        return primaryFlag;
    }

    @JsonProperty("PrimaryFlag")
    public void setPrimaryFlag(Boolean primaryFlag) {
        this.primaryFlag = primaryFlag;
    }

    @JsonProperty("ImageId")
    public Long getImageId() {
        return imageId;
    }

    @JsonProperty("ImageId")
    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    @JsonProperty("PersonId")
    public Long getPersonId() {
        return personId;
    }

    @JsonProperty("PersonId")
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @JsonProperty("ObjectVersionNumber")
    public Long getObjectVersionNumber() {
        return objectVersionNumber;
    }

    @JsonProperty("ObjectVersionNumber")
    public void setObjectVersionNumber(Long objectVersionNumber) {
        this.objectVersionNumber = objectVersionNumber;
    }

    @JsonProperty("links")
    public List<Link_> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link_> links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
