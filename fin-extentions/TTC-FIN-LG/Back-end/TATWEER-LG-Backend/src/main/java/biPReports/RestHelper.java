package biPReports;


import com.appspro.db.CommonConfigReader;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.ws.rs.core.MediaType;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONObject;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


public class RestHelper {
    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    private String InstanceUrl =
        CommonConfigReader.getValue("InstanceUrl"); //;"https://ejcp-test.fa.em2.oraclecloud.com:443";
    private String orgnizationUrl ="//hcmCoreSetupApi/resources/latest/organizations";
    private String biReportUrl = CommonConfigReader.getValue("biReportUrl"); //"https://ejcp-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService";
    private String SecurityService = CommonConfigReader.getValue("SecurityService");
    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private String instanceName = CommonConfigReader.getValue("instanceName"); //"ejcp-test.fa.em2.oraclecloud.com";
    private String gradeUrl = "/hcmCoreSetupApi/resources/latest/grades";
    private String locationUrl = "/hcmCoreSetupApi/resources/latest/locations";
    private String positionUrl = "/hcmCoreSetupApi/resources/latest/positions";
    private String jobsUrl = "/hcmCoreSetupApi/resources/latest/jobs";
    private String absenceUrl = "/hcmRestApi/resources/11.13.17.11/absences/";
    private String AllEmployeesUrl = "/hcmRestApi/resources/11.13.18.05/emps?expand=assignments";
    private String departmentUrl =  "/hcmRestApi/resources/11.13.18.05/departmentsLov";
    private String supplierUrl = "/fscmRestApi/resources/11.13.18.05/suppliers?limit=500";
    private String contractUrl = "/fscmRestApi/resources/11.13.18.05/contracts";
    private String purchaseOrderUrl = "/fscmRestApi/resources/11.13.18.05/purchaseOrders";
    private String purchaseRequisitionsUrl = "/fscmRestApi/resources/11.13.18.05/purchaseRequisitions";
    private String receiptUrl = "/fscmRestApi/resources/11.13.18.05/purchaseRequisitions";
    private String cashExternalUrl = "/fscmRestApi/resources/11.13.18.05/cashExternalTransactions";
    private String bankAccount = "/fscmRestApi/resources/11.13.18.05/cashBankAccounts?limit=500";



    public static String USER_NAME = CommonConfigReader.getValue("USER_NAME"); //"mahmoud.essam@appspro-me.com";
    public static String PASSWORD = CommonConfigReader.getValue("PASSWORD");
    public static String Schema_Name = CommonConfigReader.getValue("Schema_Name");
    public final static String SAAS_URL = "https://" + CommonConfigReader.getValue("instanceName");
    public final static String STATIC_INSTANCE_NAME ="hdrp-test.fa.em2.oraclecloud.com";


    public static String getSchema_Name() {
        return Schema_Name;
    }


    public String getAbsenceUrl() {
        return absenceUrl;
    }

    public void setAbsenceUrl(String absenceUrl) {
        this.absenceUrl = absenceUrl;
    }

    public String getJobUrl() {
        return this.jobsUrl;
    }

    public String getPositionUrl() {
        return positionUrl;
    }

    public String getLocationUrl() {
        return this.locationUrl;
    }

    public String getGradeUrl() {
        return this.gradeUrl;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String getEmployeeUrl() {
        return InstanceUrl + employeeServiceUrl;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }


    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

//    public String callSaaS(String serverURL, String restFrameWorkVersion) {
//        SSLContext ctx = null;
//        System.setProperty("DUseSunHttpHandler", "true");
//        TrustManager[] trustAllCerts =
//            new X509TrustManager[] { new X509TrustManager() {
//                public X509Certificate[] getAcceptedIssuers() {
//                    return null;
//                }
//
//                public void checkClientTrusted(X509Certificate[] certs,
//                                               String authType) {
//                }
//
//                public void checkServerTrusted(X509Certificate[] certs,
//                                               String authType) {
//                }
//            } };
//        try {
//            ctx = SSLContext.getInstance("TLSv1.2");
//            ctx.init(null, trustAllCerts, null);
//        } catch (NoSuchAlgorithmException e) {
//            System.out.println("Error loading ssl context {}" +
//                               e.getMessage());
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//
//        SSLContext.setDefault(ctx);
//
//        ClientConfig def = new DefaultClientConfig();
//
//        def.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
//                                new HTTPSProperties(null, ctx));
//        Client client = Client.create(def);
//
//        client.addFilter(new HTTPBasicAuthFilter(RestHelper.USER_NAME,
//                                                 RestHelper.PASSWORD));
//
//        serverURL = serverURL.replaceAll("\\s", "%20");
//        WebResource resource = client.resource(serverURL);
//        resource.header("REST-Framework-Version", "2");
//
//        return resource.accept(MediaType.APPLICATION_JSON_TYPE,
//                               MediaType.APPLICATION_XML_TYPE).header("Accept-Charset",
//                                                                      "utf-8").get(String.class);
//    }

    public Document httpPost(String destUrl,
                             String postData) throws Exception {
        System.out.println(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());

        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return doc;
        } else {
            System.out.println("Failed");
        }


        return null;
    }
    
    public JSONObject httpGet (String restUrl) throws MalformedURLException, IOException {
       
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        URL url = new URL(null,restUrl,new sun.net.www.protocol.https.Handler());

        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null ;
        connection.setRequestMethod("GET");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth());
       
        BufferedReader in = new BufferedReader( new InputStreamReader (connection.getInputStream(),"utf-8"));
        String input = null;
        StringBuffer response = new StringBuffer();
        
        while((input = in.readLine()) != null) {
            response.append(input);
        }
        
        JSONObject obj = new JSONObject(response.toString());
       
       
        return obj;
        
    }

    public static String getAuth() {
        byte[] message = (USER_NAME + ":" + PASSWORD).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
    //    public static void main (String [] args ){
    //        System.out.println(getAuth());
    //    }

    public void setSecurityService(String SecurityService) {
        this.SecurityService = SecurityService;
    }

    public String getSecurityService() {
        return SecurityService;
    }

    public String getEmailBody(String reqType, String approvalName,
                               String reqName, String BeneficiaryName) {
        String body = "";
        if (reqType.equals("FYI")) {
            body =
"Dear " + approvalName + ",\\n\\r" + "Kindly review for your information the below details: " +
 "Request Name " + reqName + "\\n\\r  Beneficiary : " + BeneficiaryName;

        } else if (reqType.equals("FYA")) {
            body =
"Dear " + approvalName + ", " + "Your Action required for the \\n\\r following:-  " +
 "Request Name :  " + reqName + "\\n\\\r  Beneficiary : " + BeneficiaryName +
 "  Please connect to HCM.HHA.com.sa";
        }
        return body;
    }

    public String getEmailSubject(String reqType, String reqName,
                                  String BeneficiaryName) {
        String subject = "";
        if (reqType.equals("FYI")) {
            subject =
                    "FYI  Approval of  " + reqName + "for  " + BeneficiaryName;
        } else if (reqType.equals("FYA")) {
            subject =
                    "Action Required   Approval of  " + reqName + "for  " + BeneficiaryName;
        }
        return subject;
    }

    public void setDepartmentUrl(String departmentUrl) {
        this.departmentUrl = departmentUrl;
    }

    public String getDepartmentUrl() {
        return departmentUrl;
    }



    public void setSupplierUrl(String supplierUrl) {
        this.supplierUrl = supplierUrl;
    }

    public String getSupplierUrl() {
        return supplierUrl;
    }

    public void setContractUrl(String contractUrl) {
        this.contractUrl = contractUrl;
    }

    public String getContractUrl() {
        return contractUrl;
    }

    public void setAllEmployeesUrl(String AllEmployeesUrl) {
        this.AllEmployeesUrl = AllEmployeesUrl;
    }

    public String getAllEmployeesUrl() {
        return AllEmployeesUrl;
    }

    public void setPurchaseOrderUrl(String purchaseOrderUrl) {
        this.purchaseOrderUrl = purchaseOrderUrl;
    }

    public String getPurchaseOrderUrl() {
        return purchaseOrderUrl;
    }

    public void setPurchaseRequisitionsUrl(String purchaseRequisitionsUrl) {
        this.purchaseRequisitionsUrl = purchaseRequisitionsUrl;
    }

    public String getPurchaseRequisitionsUrl() {
        return purchaseRequisitionsUrl;
    }

    public void setReceiptUrl(String receiptUrl) {
        this.receiptUrl = receiptUrl;
    }

    public String getReceiptUrl() {
        return receiptUrl;
    }

    public void setCashExternalUrl(String cashExternalUrl) {
        this.cashExternalUrl = cashExternalUrl;
    }

    public String getCashExternalUrl() {
        return cashExternalUrl;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }
}
