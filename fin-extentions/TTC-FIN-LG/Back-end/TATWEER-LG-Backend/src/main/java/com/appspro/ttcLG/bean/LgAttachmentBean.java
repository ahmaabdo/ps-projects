package com.appspro.ttcLG.bean;

public class LgAttachmentBean {
    private int id;
    private String title;
    private String name;
    private String content;
    private int docNum;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setDocNum(int docNum) {
        this.docNum = docNum;
    }

    public int getDocNum() {
        return docNum;
    }
}
