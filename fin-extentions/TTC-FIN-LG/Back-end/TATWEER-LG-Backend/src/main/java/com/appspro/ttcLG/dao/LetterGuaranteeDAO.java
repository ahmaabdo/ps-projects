package com.appspro.ttcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.ttcLG.bean.LetterGuaranteeBean;

import com.appspro.ttcLG.bean.LgHistoryBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;

public class LetterGuaranteeDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps1;
    PreparedStatement ps2;

    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public JSONArray getAllLgDetails() throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LetterGuaranteeBean> lgDetailsList =
                new ArrayList<LetterGuaranteeBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_LG_NUMBER";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                LetterGuaranteeBean lgBean = new LetterGuaranteeBean();

                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setContractNumber(rs.getString("CONTRACT_NUMBER"));
                lgBean.setContractAmount(rs.getString("CONTRACT_AMOUNT"));
                lgBean.setSupplier(rs.getString("SUPPLIER"));
                lgBean.setContractStartDate(rs.getString("CONTRACT_START_DATE"));
                lgBean.setContractEndDate(rs.getString("CONTRACT_END_DATE"));
                lgBean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("LG_AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setRegion(rs.getString("REGION"));
                lgBean.setBankAccount(rs.getString("BANK_ACCOUNT"));
                lgBean.setDescription(rs.getString("DESCRIPTION"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                lgBean.setCreatedBy(rs.getString("CREATED_BY"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }

    public JSONArray getLg(String docNum) throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LetterGuaranteeBean> lgDetailsList =
                new ArrayList<LetterGuaranteeBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_LG_NUMBER WHERE DOCUMENT_NUMBER = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, docNum);

            rs = ps.executeQuery();
            while (rs.next()) {
                LetterGuaranteeBean lgBean = new LetterGuaranteeBean();

                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setContractNumber(rs.getString("CONTRACT_NUMBER"));
                lgBean.setContractAmount(rs.getString("CONTRACT_AMOUNT"));
                lgBean.setSupplier(rs.getString("SUPPLIER"));
                lgBean.setContractStartDate(rs.getString("CONTRACT_START_DATE"));
                lgBean.setContractEndDate(rs.getString("CONTRACT_END_DATE"));
                lgBean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("LG_AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setRegion(rs.getString("REGION"));
                lgBean.setBankAccount(rs.getString("BANK_ACCOUNT"));
                lgBean.setDescription(rs.getString("DESCRIPTION"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                lgBean.setCreatedBy(rs.getString("CREATED_BY"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }

    public String addLgDetails(LetterGuaranteeBean lgDetails,LgHistoryBean history) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query =
               "insert into XXX_LG_NUMBER (CONTRACT_NUMBER,CONTRACT_AMOUNT,SUPPLIER,CONTRACT_START_DATE,CONTRACT_END_DATE,LG_NUM_FROM_BANK,LG_TYPE,LG_AMOUNT,LG_END_DATE,REGION,BANK_ACCOUNT,DESCRIPTION,STATUS,DOCUMENT_NUMBER,APPROVAL_STATUS,CREATED_BY) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, lgDetails.getContractNumber());
            ps.setString(2, lgDetails.getContractAmount());
            ps.setString(3, lgDetails.getSupplier());
            ps.setString(4, lgDetails.getContractStartDate());
            ps.setString(5, lgDetails.getContractEndDate());
            ps.setString(6, lgDetails.getLgNumFromBank());
            ps.setString(7, lgDetails.getLgType());
            ps.setString(8, lgDetails.getLgAmount());
            ps.setString(9, lgDetails.getLgEndDate());
            ps.setString(10, lgDetails.getRegion());
            ps.setString(11, lgDetails.getBankAccount());
            ps.setString(12, lgDetails.getDescription());
            ps.setString(13, lgDetails.getLgStatus());
            ps.setInt(14, lgDetails.getDocumentNumber());
            ps.setString(15, lgDetails.getApprovalStatus());
            ps.setString(16, lgDetails.getCreatedBy());
            ps.executeUpdate();
            
            String query2 =
               "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,history,MOD_DATE,operation) VALUES(?,?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'None')";

            ps2 = connection.prepareStatement(query2);
            ps2.setString(1, history.getLgType());
            ps2.setString(2, history.getLgAmount());
            ps2.setString(3, history.getLgEndDate());
            ps2.setString(4, history.getBankAmount());
            ps2.setString(5, history.getLgStatus());
            ps2.setInt(6, history.getDocumentNumber());
            ps2.setString(7, "original");
            ps2.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "lg added successfully";
    }

    public String updateLgDetails(LetterGuaranteeBean lgDetails) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "update  XXX_LG_NUMBER set  CONTRACT_NUMBER=?,CONTRACT_AMOUNT=?,SUPPLIER=?,CONTRACT_START_DATE=?,CONTRACT_END_DATE=?,LG_NUM_FROM_BANK=?,LG_TYPE=?,LG_AMOUNT=?,LG_END_DATE=?,REGION=?,BANK_ACCOUNT=?,DESCRIPTION=? ,STATUS=?,APPROVAL_STATUS=? where DOCUMENT_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgDetails.getContractNumber());
            ps.setString(2, lgDetails.getContractAmount());
            ps.setString(3, lgDetails.getSupplier());
            ps.setString(4, lgDetails.getContractStartDate());
            ps.setString(5, lgDetails.getContractEndDate());
            ps.setString(6, lgDetails.getLgNumFromBank());
            ps.setString(7, lgDetails.getLgType());
            ps.setString(8, lgDetails.getLgAmount());
            ps.setString(9, lgDetails.getLgEndDate());
            ps.setString(10, lgDetails.getRegion());
            ps.setString(11, lgDetails.getBankAccount());
            ps.setString(12, lgDetails.getDescription());
            ps.setString(13, lgDetails.getLgStatus());
            ps.setString(14, lgDetails.getApprovalStatus());
            ps.setInt(15, lgDetails.getDocumentNumber());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "lg updated successfully";
    }

    public String deleteLgDetails(LetterGuaranteeBean bean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();
            String query;
            int checkResult = 0;
            query = "DELETE from XXX_LG_NUMBER where DOCUMENT_NUMBER = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, bean.getDocumentNumber());
            checkResult = ps.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            ps.close();
        }

        return "Lg Deleted Successfully";
    }
    public JSONArray getLastRowLg() throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LetterGuaranteeBean> lgDetailsList =
                new ArrayList<LetterGuaranteeBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "select * from XXX_LG_NUMBER where DOCUMENT_NUMBER=(SELECT MAX(DOCUMENT_NUMBER)from XXX_LG_NUMBER) ";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                LetterGuaranteeBean lgBean = new LetterGuaranteeBean();
                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setContractNumber(rs.getString("CONTRACT_NUMBER"));
                lgBean.setContractAmount(rs.getString("CONTRACT_AMOUNT"));
                lgBean.setSupplier(rs.getString("SUPPLIER"));
                lgBean.setContractStartDate(rs.getString("CONTRACT_START_DATE"));
                lgBean.setContractEndDate(rs.getString("CONTRACT_END_DATE"));
                lgBean.setLgNumFromBank(rs.getString("LG_NUM_FROM_BANK"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("LG_AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setRegion(rs.getString("REGION"));
                lgBean.setBankAccount(rs.getString("BANK_ACCOUNT"));
                lgBean.setDescription(rs.getString("DESCRIPTION"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setApprovalStatus(rs.getString("APPROVAL_STATUS"));
                lgBean.setCreatedBy(rs.getString("CREATED_BY"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }

}
