package com.appspro.ttcLG.bean;

public class BankAccountBean {
    
   private String bankAccount;
   private String cashAccountComb;
   private String bankAccountName;
   

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setCashAccountComb(String cashAccountComb) {
        this.cashAccountComb = cashAccountComb;
    }

    public String getCashAccountComb() {
        return cashAccountComb;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }
}
