/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appspro.ttcLG.dao;


import biPReports.RestHelper;

import com.appspro.ttcLG.bean.EmployeeBean;

import biPReports.rest.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.image.BufferedImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.URL;

import java.security.NoSuchAlgorithmException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import biPReports.RestHelper;

public class EmployeeDetails extends RestHelper {

    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();

    public EmployeeDetails() {
        super();
    }


    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public EmployeeBean getEmpDetails(String username, String jwt, String host,
                                      boolean mainData) {

        EmployeeBean emp = new EmployeeBean();


        String serverUrl =
            this.getEmployeeUrl() + "?q=UserName=" + username + "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield&onlyData=" +
            !mainData;

        ObjectMapper jmapper = new ObjectMapper();
        Example opty = null;
        
//            String response = new RestHelper().callSaaS(serverUrl, "1");
//            opty = jmapper.readValue(response, Example.class);


        
        emp = getEmpAssignment(null, host, emp, mainData, opty);
      
        return emp;
    }


    public EmployeeBean getEmpDetailsByPersonId(String personId, String jwt,
                                                String host) {


        String serverUrl =
            this.getEmployeeUrl() + "?q=PersonId=" + personId + "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield";

        EmployeeBean emp = new EmployeeBean();
        ObjectMapper jmapper = new ObjectMapper();
        Example opty = null;
       
//            String response = new RestHelper().callSaaS(serverUrl, "2");
//            opty = jmapper.readValue(response, Example.class);


       
        emp = getEmpAssignment(null, host, emp, true, opty);


        return emp;
    }


    public EmployeeBean getEmpAssignment(String jwttoken, String host,
                                         EmployeeBean emp, boolean mainData,
                                         Example example) {

        try {
            Map<String, String> map =
                getEmployeeDetails(example.getItems().get(0).getUserName());
            List<Assignment> assignmets = new ArrayList<Assignment>();

            emp.setDisplayName(example.getItems().get(0).getDisplayName());

            emp.setHireDate(example.getItems().get(0).getHireDate());
            emp.setDateOfBirth(example.getItems().get(0).getDateOfBirth());


            emp.setCitizenshipLegislationCode(example.getItems().get(0).getCitizenshipLegislationCode() + "");
            emp.setPersonId(example.getItems().get(0).getPersonId() + "");
            emp.setPersonNumber(example.getItems().get(0).getPersonNumber());
            emp.setCountry(example.getItems().get(0).getCountry());
            emp.setCity(example.getItems().get(0).getCity());
            emp.setRegion(example.getItems().get(0).getRegion() + "");
            emp.setNationalId(example.getItems().get(0).getNationalId());
            List<Photo> photo = example.getItems().get(0).getPhoto();
            emp.setMaritalStatus(example.getItems().get(0).getMaritalStatus() +
                                 "");
            
            emp.setEmail(example.getItems().get(0).getWorkEmail() + "");

            emp.setWorkPhone(example.getItems().get(0).getWorkPhoneNumber() +
                             "");
            emp.setWorkPhoneExt(example.getItems().get(0).getWorkPhoneExtension() +
                                "");
            emp.setMobileNumber(example.getItems().get(0).getWorkMobilePhoneNumber() +
                                "");
            emp.setFax(example.getItems().get(0).getWorkFaxNumber() + "");
            emp.setFaxExt(example.getItems().get(0).getWorkFaxExtension() +
                          "");
            emp.setLegalEntityId(example.getItems().get(0).getLegalEntityId() +
                                 "");
            emp.setOrganizationName(map.get("LegalEntityName"));
            emp.setBusinessUnitName(map.get("BussinessUnitName"));

            List<Link> links =
                example.getItems().get(0).getPersonExtraInformation().get(0).getLinks();

            assignmets = example.getItems().get(0).getAssignments();
            if (mainData) {
                getEFFUrl(links, emp);
                try {
                    for (int i = 0; i < assignmets.size(); i++) {

                        emp.setLegalEntityId(assignmets.get(i).getAssignmentCategory());
                        emp.setJobId(assignmets.get(i).getJobId() + "");
                        emp.setSalaryAmount(assignmets.get(i).getSalaryAmount());
                        emp.setManagerType(assignmets.get(i).getManagerType() +
                                           "");
                        emp.setGradeId(assignmets.get(i).getGradeId() + "");
                        emp.setAssignmentName(assignmets.get(i).getAssignmentName() +
                                              "");

                        emp.setAssignmentStatusTypeId(assignmets.get(i).getAssignmentStatusTypeId() +
                                                      "");
                        emp.setAssignmentStatus(assignmets.get(i).getAssignmentStatus() +
                                                "");

                        emp.setProbationPeriodEndDate(assignmets.get(i).getProbationPeriodEndDate() +
                                                      "");
                        emp.setDepartment(assignmets.get(i).getDepartmentId() +
                                          "");
                        emp.setPositionId(assignmets.get(i).getPositionId() +
                                          "");


                        emp.setGrade(map.get("GradeName"));
                        emp.setJobName(map.get("JobName"));
                        emp.setDepartmentName(map.get("DepartmentName"));
                        emp.setPositionName(map.get("PositionName"));
                        emp.setAssignmentName(map.get("PositionName"));
                        emp.setManagerId(map.get("ManagerId"));
                        emp.setManagerName(map.get("ManagerName"));

                        String managerName = "";

                        if (emp.getManagerId() != null &&
                            !emp.getManagerId().isEmpty() &&
                            !emp.getManagerId().equals("null")) {
                            Map<String, String> mapManager =
                                getEmployeeDetailsByPersonId(emp.getManagerId());
                            emp.setManagerOfManager(mapManager.get("ManagerId"));

                            emp.setManagerOfMnagerName(mapManager.get("ManagerName"));


                        }


                        emp.setEmployeeLocation(map.get("LocationName"));


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                HttpsURLConnection https = null;
                HttpURLConnection connection = null;
                if (photo != null && photo.size() > 0) {
                    List<Link_> linksPhotoe = photo.get(0).getLinks();
                    String serverUrl =
                        linksPhotoe.get(0).getHref() + "/enclosure/Image";
                    URL url =
                        new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
                    if (url.getProtocol().toLowerCase().equals("https")) {
                        trustAllHosts();
                        https = (HttpsURLConnection)url.openConnection();
                        https.setHostnameVerifier(DO_NOT_VERIFY);
                        connection = https;
                    } else {
                        connection = (HttpURLConnection)url.openConnection();
                    }
                    String SOAPAction = this.getInstanceUrl();
                    //connection = (HttpsURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestProperty("SOAPAction", SOAPAction);
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Content-Type",
                                                  "application/json; Charset=UTF-8");
                    //  connection.addRequestProperty("Authorization","Bearer " + jwttoken);
                    connection.addRequestProperty("Authorization",
                                                  "Basic " + getAuth());
                    BufferedImage bi =
                        ImageIO.read(connection.getInputStream());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(bi, "jpg", baos);
                    baos.flush();
                    byte[] imageInByte = baos.toByteArray();
                    emp.setPicBase64(javax.xml.bind.DatatypeConverter.printBase64Binary(imageInByte));
                    baos.close();
                    connection.disconnect();
                }
            }


            return emp;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emp;

    }

    public EmployeeBean searchPersonDetails(String name, String nationalId,
                                            String personNumber,
                                            String managerId,
                                            String effectiveAsof, String jwt,
                                            String host) {

        String queryParam = this.getEmployeeUrl() + "?q=";


        if (personNumber != null && !personNumber.isEmpty() &&
            !personNumber.equals("undefined")) {
            queryParam = queryParam + "PersonNumber='" + personNumber + "'";
        }

        if (name != null && !name.isEmpty() && !name.equals("undefined")) {
            if (personNumber != null && !personNumber.isEmpty()) {
                queryParam = queryParam + " and ";
            }
            queryParam =
                    queryParam + "DisplayName='" + name + "'";
        }

        if (nationalId != null && !nationalId.isEmpty() &&
            !nationalId.equals("undefined")) {
            if ((personNumber != null && !personNumber.isEmpty()) ||
                (name != null && !name.isEmpty())) {
                queryParam = queryParam + " and ";
            }
            queryParam =
                    queryParam + "NationalId='" + nationalId + "'";
        }


        if (effectiveAsof != null && !effectiveAsof.isEmpty() &&
            !effectiveAsof.equals("undefined")) {
            if ((personNumber != null && !personNumber.isEmpty()) ||
                (name != null && !name.isEmpty()) ||
                (nationalId != null && !nationalId.isEmpty())) {
                queryParam = queryParam + " and ";
            }
            queryParam =
                    queryParam + "EffectiveStartDate  < = '" + effectiveAsof +
                    "'";
        }

        if (managerId != null && !managerId.isEmpty()) {
            if ((personNumber != null && !personNumber.isEmpty()) ||
                (name != null && !name.isEmpty()) ||
                (nationalId != null && !nationalId.isEmpty()) ||
                (effectiveAsof != null && !effectiveAsof.isEmpty())) {
                queryParam = queryParam + " and ";
            }
            queryParam = queryParam + "assignments.ManagerId =" + managerId;
        }
        queryParam =
                queryParam + "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield";
        EmployeeBean emp = new EmployeeBean();
        ObjectMapper jmapper = new ObjectMapper();
        Example opty = null;
      
            System.out.println(queryParam);
//            String response = new RestHelper().callSaaS(queryParam, "2");
//            opty = jmapper.readValue(response, Example.class);


       

        if (opty.getItems() == null || opty.getItems().size() == 0 ||
            opty.getItems().get(0).getPersonId() == null) {
            return null;
        }
        //            List<EmployeeBean> employees = new ArrayList<EmployeeBean>();
        emp = getEmpAssignment(null, host, emp, true, opty);
        //            for (int i = 0; i < arr.length(); i++) {
        //                EmployeeBean emp = new EmployeeBean();
        //                JSONArray jsonArray = new JSONArray();
        //                jsonArray.put(arr.get(i));
        //                employees.add(getEmpAssignment(null, host, jsonArray, emp));
        //            }
        //            EmployeeBean[] array = new EmployeeBean[employees.size()];
        //            employees.toArray(array);

        return emp;

    }


    public EmployeeBean getEmpDetailsByPersonNum(String personId, String jwt,
                                                 String host) {


        String serverUrl =
            this.getEmployeeUrl() + "?q=PersonNumber=" + personId +
            "&expand=personExtraInformation,assignments,photo,assignments.peopleGroupKeyFlexfield";
        EmployeeBean emp = new EmployeeBean();
        ObjectMapper jmapper = new ObjectMapper();
        Example opty = null;
       
//            String response = new RestHelper().callSaaS(serverUrl, "2");
//            opty = jmapper.readValue(response, Example.class);


         

        if (opty.getItems() == null || opty.getItems().size() == 0 ||
            opty.getItems().get(0).getPersonId() == null) {
            return null;
        }


        return emp;
    }


    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.out.println(new Date());
        new EmployeeDetails().getEmpDetails("6415", null, null, true);
        System.out.println(new Date());

    }


    public void getEFFUrl(List<Link> arr, EmployeeBean emp) {
        Dictionary URLDictionary = new Hashtable();

        //(EFFLinks.toString());

        //  for (int counter = 0; counter < arrURL.length(); counter++) {
        for (int i = 0; i < arr.size(); i++) {

            if (arr.get(i).getName().contains("PersonExtraInformationContextXXX")) {

                String name = arr.get(i).getName().replace("privateVO", "");
                name = name.replaceAll("5F", "");
                name = name.replace("PersonExtraInformationContext", "");
                //(name);
                URLDictionary.put(name, arr.get(i).getHref());
            }
        }
        emp.setEmployeeURL(URLDictionary);

    }

    public static Map<String, String> getEmployeeDetails(String username) {
        try {
            System.out.println("Invoke service using direct HTTP call with Basic Auth");
            String payload =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:findUserDetailsByUsername>\n" +
                "         <typ:Username>" + username + "</typ:Username>\n" +
                "      </typ:findUserDetailsByUsername>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost(new RestHelper().getInstanceUrl() +
                                          "/hcmService/UserDetailsServiceV2?invoke=",
                                          payload);

            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            Map<String, String> map = new HashMap<String, String>();

            map.put("LegalEntityName",
                    root.getElementsByTagName("ns1:LegalEntityName").item(0).getTextContent());
            map.put("GradeName",
                    root.getElementsByTagName("ns1:GradeName").item(0).getTextContent());
            map.put("JobName",
                    root.getElementsByTagName("ns1:JobName").item(0).getTextContent());
            map.put("DepartmentName",
                    root.getElementsByTagName("ns1:DepartmentName").item(0).getTextContent());
            map.put("PositionName",
                    root.getElementsByTagName("ns1:PositionName").item(0).getTextContent());
            map.put("ManagerId",
                    root.getElementsByTagName("ns1:ManagerId").item(0).getTextContent());
            map.put("ManagerName",
                    root.getElementsByTagName("ns1:ManagerName").item(0).getTextContent());
            map.put("LocationName",
                    root.getElementsByTagName("ns1:LocationName").item(0).getTextContent());
            map.put("BussinessUnitName",
                    doc.getElementsByTagName("ns1:BusinessUnitName").item(0).getTextContent());
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Map<String, String> getEmployeeDetailsByPersonId(String personID) {
        try {
            System.out.println("Invoke service using direct HTTP call with Basic Auth");
            String payload =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:findUserDetailsByPersonId>\n" +
                "         <typ:PersonId>" + personID + "</typ:PersonId>\n" +
                "      </typ:findUserDetailsByPersonId>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost(new RestHelper().getInstanceUrl() +
                                          "/hcmService/UserDetailsServiceV2?invoke=",
                                          payload);

            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            Map<String, String> map = new HashMap<String, String>();

            map.put("LegalEntityName",
                    root.getElementsByTagName("ns1:LegalEntityName").item(0).getTextContent());
            map.put("GradeName",
                    root.getElementsByTagName("ns1:GradeName").item(0).getTextContent());
            map.put("JobName",
                    root.getElementsByTagName("ns1:JobName").item(0).getTextContent());
            map.put("DepartmentName",
                    root.getElementsByTagName("ns1:DepartmentName").item(0).getTextContent());
            map.put("PositionName",
                    root.getElementsByTagName("ns1:PositionName").item(0).getTextContent());
            map.put("ManagerId",
                    root.getElementsByTagName("ns1:ManagerId").item(0).getTextContent());
            map.put("ManagerName",
                    root.getElementsByTagName("ns1:ManagerName").item(0).getTextContent());
            map.put("LocationName",
                    root.getElementsByTagName("ns1:LocationName").item(0).getTextContent());

            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
