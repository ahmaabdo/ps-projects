package com.appspro.ttcLG.bean;
/**
 *
 * @author lenovo
 */
public class SummaryReportBean {

    private String id;
    private String report_Name;
    private String parameter1;
    private String parameter2;
    private String parameter3;
    private String parameter4;
    private String parameter5;
    private String reportTypeVal;
    
    
    

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setReport_Name(String report_Name) {
        this.report_Name = report_Name;
    }

    public String getReport_Name() {
        return report_Name;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter3(String parameter3) {
        this.parameter3 = parameter3;
    }

    public String getParameter3() {
        return parameter3;
    }

    public void setParameter4(String parameter4) {
        this.parameter4 = parameter4;
    }

    public String getParameter4() {
        return parameter4;
    }

    public void setParameter5(String parameter5) {
        this.parameter5 = parameter5;
    }

    public String getParameter5() {
        return parameter5;
    }

    public void setReportTypeVal(String reportTypeVal) {
        this.reportTypeVal = reportTypeVal;
    }

    public String getReportTypeVal() {
        return reportTypeVal;
    }
    
    @Override
    public String toString() {
        return "SummaryReportBean{" + "id=" + id + ", report_Name=" + report_Name + ", parameter1=" + parameter1 + ", parameter2=" + parameter2 + ", parameter3=" + parameter3 + ", parameter4=" + parameter4 + ", parameter5=" + parameter5 + ",reportTypeVal="+reportTypeVal+'}';
    }
}

