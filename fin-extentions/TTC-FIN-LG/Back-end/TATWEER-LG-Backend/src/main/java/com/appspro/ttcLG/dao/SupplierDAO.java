package com.appspro.ttcLG.dao;

import biPReports.RestHelper;

import com.appspro.ttcLG.bean.SuppilerBean;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;

import java.net.HttpURLConnection;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import javax.ws.rs.GET;

import org.json.JSONArray;
import org.json.JSONObject;

public class SupplierDAO extends RestHelper {
  
    RestHelper rs = new RestHelper();        
    public ArrayList<SuppilerBean> getAllSupplier() throws MalformedURLException,IOException {
        
        ArrayList<SuppilerBean> supplierList = new ArrayList<SuppilerBean>();
        String restApi = getInstanceUrl()+getSupplierUrl();
        
        JSONObject obj = rs.httpGet(restApi);
        JSONArray arr = obj.getJSONArray("items");
        
        for (int i = 0; i < arr.length(); i++) {
            SuppilerBean bean = new SuppilerBean();
            JSONObject supplierObj = arr.getJSONObject(i);

            bean.setSupplierName(supplierObj.getString("Supplier"));
            bean.setSupplierId(supplierObj.getLong("SupplierId"));
            bean.setSupplierNumber(supplierObj.getString("SupplierNumber"));

            supplierList.add(bean);
            
            }
       
        return supplierList;
        
    }
}
