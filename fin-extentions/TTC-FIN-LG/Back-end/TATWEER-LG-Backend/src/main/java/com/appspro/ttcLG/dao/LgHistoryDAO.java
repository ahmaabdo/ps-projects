package com.appspro.ttcLG.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.ttcLG.bean.LetterGuaranteeBean;
import com.appspro.ttcLG.bean.LgHistoryBean;

import com.appspro.ttcLG.bean.WorkFlowNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;

public class LgHistoryDAO extends AppsproConnection{
    Connection connection;
    PreparedStatement ps;
    PreparedStatement ps1;
    PreparedStatement ps2;

    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public JSONArray getLgHistory(String docNum) throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<LgHistoryBean> lgDetailsList =
                new ArrayList<LgHistoryBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_LG_HISTORY WHERE DOCUMENT_NUMBER = ? ";
            ps = connection.prepareStatement(query);
            ps.setString(1, docNum);

            rs = ps.executeQuery();
            while (rs.next()) {
                LgHistoryBean lgBean = new LgHistoryBean();

                lgBean.setId(rs.getInt("ID"));
                lgBean.setDocumentNumber(rs.getInt("DOCUMENT_NUMBER"));
                lgBean.setLgType(rs.getString("LG_TYPE"));
                lgBean.setLgAmount(rs.getString("AMOUNT"));
                lgBean.setLgEndDate(rs.getString("LG_END_DATE"));
                lgBean.setBankAmount(rs.getString("BANK_AMOUNT"));
                lgBean.setLgStatus(rs.getString("STATUS"));
                lgBean.setHistory(rs.getString("HISTORY"));
                lgBean.setModDate(rs.getString("MOD_DATE"));
                lgBean.setOperation(rs.getString("OPERATION"));

                lgDetailsList.add(lgBean);
            }
            arr = new JSONArray(lgDetailsList);

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }
    public String updateLgExtend(LgHistoryBean lgDetails,LetterGuaranteeBean lgBean,WorkFlowNotificationBean notifBean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "update  XXX_LG_NUMBER set  LG_END_DATE=? where DOCUMENT_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgBean.getLgEndDate());
            ps.setInt(2, lgBean.getDocumentNumber());
            ps.executeUpdate();
            
            String query1 =
                "update  XXX_LG_NOTIFICATION_DETAILS set  LG_END_DATE=? where DOC_NUM=?";
            ps = connection.prepareStatement(query1);
            ps.setString(1, notifBean.getLgEndDate());
            ps.setInt(2, notifBean.getDocumentNumber());
            ps.executeUpdate();
            
            String query2 =
               "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?)";

            ps2 = connection.prepareStatement(query2);
            ps2.setString(1, lgDetails.getLgType());
            ps2.setString(2, lgDetails.getLgAmount());
            ps2.setString(3, lgDetails.getLgEndDate());
            ps2.setString(4, lgDetails.getBankAmount());
            ps2.setString(5, lgDetails.getLgStatus());
            ps2.setInt(6, lgDetails.getDocumentNumber());
            ps2.setString(7, lgDetails.getOperation());
            ps2.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "lg updated successfully";
    }
    public String updateLgIncDec(LgHistoryBean lgDetails,LetterGuaranteeBean lgBean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "update  XXX_LG_NUMBER set  LG_AMOUNT=? where DOCUMENT_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgBean.getLgAmount());
            ps.setInt(2, lgBean.getDocumentNumber());
            ps.executeUpdate();
            
            String query2 =
               "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?)";

            ps2 = connection.prepareStatement(query2);
            ps2.setString(1, lgDetails.getLgType());
            ps2.setString(2, lgDetails.getLgAmount());
            ps2.setString(3, lgDetails.getLgEndDate());
            ps2.setString(4, lgDetails.getBankAmount());
            ps2.setString(5, lgDetails.getLgStatus());
            ps2.setInt(6, lgDetails.getDocumentNumber());
            ps2.setString(7, lgDetails.getOperation());
            ps2.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "lg updated successfully";
    }
    public String updateLgRelease(LgHistoryBean lgDetails,LetterGuaranteeBean lgBean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "update  XXX_LG_NUMBER set  STATUS=?,APPROVAL_STATUS=? where DOCUMENT_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, lgBean.getLgStatus());
            ps.setString(2, lgBean.getApprovalStatus());
            ps.setInt(3, lgBean.getDocumentNumber());
            ps.executeUpdate();
            
            String query2 =
               "insert into XXX_LG_HISTORY(LG_TYPE,AMOUNT,LG_END_DATE,BANK_AMOUNT,STATUS,DOCUMENT_NUMBER,MOD_DATE,history,operation) VALUES(?,?,?,?,?,?,TO_CHAR(sysdate, 'yyyy-mm-dd'),'updated',?)";

            ps2 = connection.prepareStatement(query2);
            ps2.setString(1, lgDetails.getLgType());
            ps2.setString(2, lgDetails.getLgAmount());
            ps2.setString(3, lgDetails.getLgEndDate());
            ps2.setString(4, lgDetails.getBankAmount());
            ps2.setString(5, lgDetails.getLgStatus());
            ps2.setInt(6, lgDetails.getDocumentNumber());
            ps2.setString(7, lgDetails.getOperation());
            ps2.executeUpdate();

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResources(connection, ps, rs);
            closeResources(ps2);
        }

        return "lg updated successfully";
    }
}
