package com.appspro.ttcLG.rest;

import com.appspro.ttcLG.bean.EmployeeBean;
import com.appspro.ttcLG.dao.EmployeeDetails;
import com.appspro.ttcLG.dao.EmployeesDAO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/Empolyees")

public class EmployeesRest {
  
    @Path("/AllEmployees")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployees(@Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) throws JsonProcessingException, UnsupportedEncodingException {

        ObjectMapper mapper = new ObjectMapper();
        EmployeeBean obj = new EmployeeBean();
        EmployeesDAO emp = new EmployeesDAO();

        ArrayList<EmployeeBean> empList = emp.getAllEmployees();
        String jsonInString = null;
        jsonInString = mapper.writeValueAsString(empList);
        return Response.ok(new String(jsonInString.getBytes(), "UTF-8"), MediaType.APPLICATION_JSON).build();

    }
    @GET
    @Path("/{username}")
    @Produces( { "application/json" })
    public String getDataFromLookup(@PathParam("username")
        String userName) {
        EmployeeBean obj = new EmployeeBean();
        EmployeesDAO det = new EmployeesDAO();
        if (userName != null && !userName.isEmpty()) {
            String details = det.getEmployeeDetails(userName);
            return details;
        }else{
                return "Null Username";
            }
}
}
