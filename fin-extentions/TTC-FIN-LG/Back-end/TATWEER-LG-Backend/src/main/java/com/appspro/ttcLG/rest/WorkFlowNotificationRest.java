package com.appspro.ttcLG.rest;

import com.appspro.ttcLG.bean.WorkFlowNotificationBean;
import com.appspro.ttcLG.dao.WorkFlowNotificationDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("notification")
public class WorkFlowNotificationRest {
    WorkFlowNotificationDAO serivces = new WorkFlowNotificationDAO();
    WorkFlowNotificationBean bean = new WorkFlowNotificationBean();
    JSONObject resposne = new JSONObject();
    JSONArray arr = new JSONArray();
    String status;
    
    @GET
    @Path("/getNotif/{empId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getNotification(@PathParam("empId")
        String empId) {

        try {
            arr = serivces.getNotificationByEmpId(empId);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }
    @POST
    @Path("addNotif")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertNoyifDetails(String body) {
            
        try {
            JSONArray arr = new JSONArray(body);
            JSONObject obj2 = arr.getJSONObject(0);
            JSONObject obj = arr.getJSONObject(0).getJSONObject("notification");
            bean.setEmpId(obj2.getString("empId"));
            bean.setEmpName(obj2.getString("empName"));
            bean.setApprovalStatus(obj.getString("approvalStatus"));
            bean.setCancel(obj.getString("cancel"));
            bean.setDocumentNumber(obj.getInt("documentNumber"));
            bean.setLgNumFromBank(obj.getString("lgNumFromBank"));
            bean.setContractNumber(obj.getString("contractNumber"));
            bean.setContractAmount(obj.getString("contractAmount"));
            bean.setLgStatus(obj.getString("lgStatus"));
            bean.setAmount(obj.getString("lgAmount"));
            bean.setSupplier(obj.getString("supplier"));
            bean.setLgEndDate(obj.getString("lgEndDate"));
            bean.setLgType(obj.getString("lgType"));
            bean.setContractStartDate(obj.getString("contractStartDate"));
            bean.setContractEndDate(obj.getString("contractEndDate"));
            bean.setRegion(obj.getString("region"));
            bean.setDescription(obj.getString("description"));
            bean.setCreatedBy(obj.getString("createdBy"));

            status = serivces.insertIntoWorkflow(bean);
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } 
        catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString() ;
    }
    @POST
    @Path("/updateStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateNotification(String body) {

        try {
            JSONObject obj2 = new JSONObject(body);
            bean.setDocumentNumber(obj2.getInt("documentNumber"));
            bean.setApprovalStatus(obj2.getString("approvalStatus"));
            bean.setLgStatus(obj2.getString("lgStatus"));
            bean.setDocumentNumber(obj2.getInt("documentNumber"));
            status = serivces.updateWorkflow(bean);
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
}
