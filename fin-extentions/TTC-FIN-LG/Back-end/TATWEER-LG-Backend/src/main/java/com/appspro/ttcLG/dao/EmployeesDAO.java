package com.appspro.ttcLG.dao;

import biPReports.RestHelper;

import com.appspro.ttcLG.bean.EmployeeBean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
public class EmployeesDAO extends RestHelper {
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public ArrayList<EmployeeBean> getAllEmployees() {
        ArrayList<EmployeeBean> employeesList = new ArrayList<EmployeeBean>();

        boolean hasMore = false;
        int offset = 0;
        do{
            
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;

        String serverUrl = getInstanceUrl() + getAllEmployeesUrl()+"&limit=500&offset="+offset;
            System.out.println(serverUrl);
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
                
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization", "Basic " + getAuth());
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
            JSONObject obj = new JSONObject(response.toString());
            JSONArray arr = obj.getJSONArray("items");
            for (int i = 0; i < arr.length(); i++) {
                EmployeeBean bean = new EmployeeBean();
                JSONObject projectObj = arr.getJSONObject(i);
                bean.setDisplayName(projectObj.getString("DisplayName"));
                bean.setPersonNumber(projectObj.getString("PersonNumber"));
                bean.setPersonID(projectObj.getLong("PersonId"));
                bean.setUserName(projectObj.getString("UserName"));

                employeesList.add(bean);
            }
            
              hasMore = obj.getBoolean("hasMore");
             offset += 500;
            

          } catch (Exception e) {
          }
        }while (hasMore);
        return employeesList;
    }
    public static  String getEmployeeDetails(String username) {
        EmployeeBean obj = new EmployeeBean();
        String resp="";
        try {
            System.out.println("Invoke service using direct HTTP call with Basic Auth");
            String payload =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <typ:findUserDetailsByUsername>\n" +
                "         <typ:Username>" + username + "</typ:Username>\n" +
                "      </typ:findUserDetailsByUsername>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
            Document doc =
                new RestHelper().httpPost(new RestHelper().getInstanceUrl() +
                                          "/hcmService/UserDetailsServiceV2?invoke=",
                                          payload);

            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            if(root.getElementsByTagName("ns1:Value").getLength()!=0){
                obj.setPersonId(String.valueOf(root.getElementsByTagName("ns1:PersonId").item(0).getTextContent()));
                obj.setUserName(root.getElementsByTagName("ns1:Username").item(0).getTextContent());
                obj.setDisplayName(root.getElementsByTagName("ns1:DisplayName").item(0).getTextContent());
                obj.setPersonNumber(root.getElementsByTagName("ns1:PersonNumber").item(0).getTextContent());
                resp =new JSONObject(obj).toString();
            }else{
                    resp = "Invalid Username";
                }            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }
}
