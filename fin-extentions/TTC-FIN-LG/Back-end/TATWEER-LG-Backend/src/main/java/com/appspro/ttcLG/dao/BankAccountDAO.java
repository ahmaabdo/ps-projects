package com.appspro.ttcLG.dao;

import biPReports.RestHelper;

import com.appspro.ttcLG.bean.BankAccountBean;
import com.appspro.ttcLG.bean.SuppilerBean;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class BankAccountDAO extends RestHelper {

    RestHelper rs = new RestHelper();

    public ArrayList<BankAccountBean> getAllBankAccount() throws MalformedURLException,
                                                              IOException {

        ArrayList<BankAccountBean> backList = new ArrayList<BankAccountBean>();
        String restApi = getInstanceUrl() + getBankAccount();

        JSONObject obj = rs.httpGet(restApi);
        JSONArray arr = obj.getJSONArray("items");

        for (int i = 0; i < arr.length(); i++) {
            BankAccountBean bean = new BankAccountBean();
            JSONObject bankObj = arr.getJSONObject(i);

            bean.setBankAccount(bankObj.getString("BankAccountNumber"));
            bean.setCashAccountComb(bankObj.getString("CashAccountCombination"));  
            bean.setBankAccountName(bankObj.getString("BankAccountName"));

            backList.add(bean);

        }

        return backList;
    }
}
