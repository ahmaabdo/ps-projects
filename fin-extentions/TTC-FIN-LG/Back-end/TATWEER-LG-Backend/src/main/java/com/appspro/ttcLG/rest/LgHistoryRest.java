package com.appspro.ttcLG.rest;

import com.appspro.ttcLG.bean.LetterGuaranteeBean;
import com.appspro.ttcLG.bean.LgHistoryBean;

import com.appspro.ttcLG.bean.WorkFlowNotificationBean;
import com.appspro.ttcLG.dao.LgHistoryDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;
@Path("/lgHistory")
public class LgHistoryRest {
    LgHistoryDAO serivces = new LgHistoryDAO();
    LgHistoryBean bean = new LgHistoryBean();
    LetterGuaranteeBean lgBean = new LetterGuaranteeBean();
    WorkFlowNotificationBean notifBean = new WorkFlowNotificationBean();
    JSONObject resposne = new JSONObject();
    JSONArray arr = new JSONArray();
    String status;
    @GET
    @Path("getLgHistory/{docNum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getLg(@PathParam("docNum")
        String docNum) {

        try {
            arr = serivces.getLgHistory(docNum);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }

        return resposne.toString();
    }
    @POST
    @Path("updateLgExtend")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLgExtend(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                lgBean.setLgEndDate(obj2.getString("lgEndDate"));
                lgBean.setDocumentNumber(obj2.getInt("documentNumber"));
                notifBean.setLgEndDate(obj2.getString("lgEndDate"));
                notifBean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setId(obj2.getInt("id"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setBankAmount(obj2.getString("contractAmount"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                status = serivces.updateLgExtend(bean,lgBean,notifBean);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    @POST
    @Path("updateLgIncDec")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLgIncDec(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                lgBean.setLgAmount(obj2.getString("lgAmount"));
                lgBean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setId(obj2.getInt("id"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setBankAmount(obj2.getString("contractAmount"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                status = serivces.updateLgIncDec(bean,lgBean);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    @POST
    @Path("updateLgRelease")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateLgRelease(String body) {

        try {
            JSONArray arr = new JSONArray(body);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj2 = arr.getJSONObject(i);
                lgBean.setLgStatus(obj2.getString("lgStatus"));
                lgBean.setApprovalStatus(obj2.getString("approvalStatus"));
                lgBean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setId(obj2.getInt("id"));
                bean.setDocumentNumber(obj2.getInt("documentNumber"));
                bean.setLgType(obj2.getString("lgType"));
                bean.setLgAmount(obj2.getString("lgAmount"));
                bean.setLgEndDate(obj2.getString("lgEndDate"));
                bean.setBankAmount(obj2.getString("contractAmount"));
                bean.setLgStatus(obj2.getString("lgStatus"));
                bean.setOperation(obj2.getString("operation"));
                status = serivces.updateLgRelease(bean,lgBean);
            }
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
}
