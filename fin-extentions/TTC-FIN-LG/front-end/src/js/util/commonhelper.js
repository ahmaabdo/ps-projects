define([], function() {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function() {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function() {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getBiReportServletPath = function() {
            var host = "report/commonbireport";
            return host;
        };


        self.getEmployees = "Empolyees/AllEmployees";
        self.getEmp = "Empolyees/";
        self.getNotification = "notification/getNotif/";
        self.addNotification = "notification/addNotif";
        self.updateStatus = "notification/updateStatus";
        self.addLg = "lg/addLg";
        self.getLgHistory = "lgHistory/getLgHistory/";
        self.addLgHistoryExtend = "lgHistory/updateLgExtend";
        self.addLgHistoryIncDec = "lgHistory/updateLgIncDec";
        self.addLgHistoryRelease = "lgHistory/updateLgRelease";
        self.getLgByDocNum = "lg/getLastRowLg";
        self.editLg = "lg/updateLg";
        self.getLg = "lg/getLg";
        self.deleteLg = "lg/deleteLg";
        self.getLastRowLg = "lg/getLastRowLg";
        self.insertAttachment = "attachment/uploadAttachment";
        self.deleteAttachment = "attachment/deleteAttachment/";
        self.getAttachment = "attachment/getAttachment/";
        self.cashTransaction = "externalCashTreansaction/create";
        self.bankAccount = "bankAccount/getBankAccount";
        self.supplier = "supplier/getSuppliers";
        self.lgFormat = "lgFormat/extendLgFormat";
        self.lgFormatWord = "lgFormat/extendReleaseLgFormat";


        self.getInternalRest = function() {

            var host = "http://127.0.0.1:7101/TATWEER-LG-context-root/webapi/";

            if (window.location.hostname == "130.61.24.142") {
                var host = "https://130.61.24.142:7002/TATWEER-LG-context-root/webapi/";
            } else {
                var host = "http://127.0.0.1:7101/TATWEER-LG-context-root/webapi/";
            }

            return host;
        };

    }

    return new commonHelper();
});