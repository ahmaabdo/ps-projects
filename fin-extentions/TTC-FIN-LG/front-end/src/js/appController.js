/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessages', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojdialog'],
    function (oj, ko, moduleUtils, app, services, commonhelper) {
        function ControllerViewModel() {
            var self = this;
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);

            self.username = ko.observable();
            self.password = ko.observable();
            self.userLogin = ko.observable();
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
            self.loginUser = ko.observable();
            self.messagesDataProvider = ko.observableArray([]);
            self.loginBtnLbl = ko.observable("LOGIN");
            self.personDetails = ko.observableArray([]);
            self.personId=ko.observable();
            self.personNumber = ko.observable();
            self.personName = ko.observable();
            self.personNumberAdmin = ko.observable('P_002');
            self.personNameAdmin = ko.observable('Abduallah Oweidi');
            self.localeLogin = ko.observable();
            self.UsernameLabel = ko.observable();
            self.passwordLabel = ko.observable();
            self.loginword = ko.observable();
            self.forgotPasswordLabel = ko.observable();
            self.localeName = ko.observable();
            self.localeJop = ko.observable();
            self.localeempNO = ko.observable();
            self.localeDep = ko.observable();
            self.localeGrade = ko.observable();
            self.agreementArr = ko.observable();
            self.bankAccountArr = ko.observable();
            self.supplierArr = ko.observable();
            self.notiCount = ko.observable(0);


            self.language = ko.observable();
            self.languagelable = ko.observable("English");
            var storage = window.localStorage;
            var isTrue, preferedLanguage;

            self.jwt = ko.observable('');
            self.hostUrl = ko.observable('');
            self.userName = ko.observable('');
            self.jwtinurl = ko.observable(false);
            self.isUserLoggedIn = ko.observable(false);

            // self.navDir = ko.observableArray([{ page: "Penalty Home >", path: "" }]);
            // self.currentPage = ko.observableArray([{page: "" , path : ""}]);
            self.pathPages = ko.observableArray([{title:'Home',path:'lgScreen'}]);
            // self.navDir = 


            // sessionStorage.clear();


            // -----------------------------------navigation---------------------------------------------------
           

            self.lgScreen = function () { 
                oj.Router.rootInstance.go('lgScreen');
            }
            self.penaltySetup = function () {
//                oj.Router.rootInstance.go('penaltySetupSummary')
            }
                 
            self.currentPagePath = function(path){
                oj.Router.rootInstance.go(path);
            }
            //     ----------------------------------------  loading function ------------------------ 

            self.loading = function (load) {
                if (load) {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            };

            //     ----------------------------------------  call agreement report ------------------------ 

            self.purchaseAgreement = async function (){ 

                var agreementFun = function(data){
                    self.agreementArr(data);
                }

                var agreementFunError = function(){
                }
               await services.getGenericReport({"reportName": "Purchase_Agreement_Report"}).then(agreementFun,agreementFunError)
            }
            //     ---------------------------------------- bank account function ------------------------ 
            
            self.bankAccount = function (){ 

                var bankAccountFun = function(data){
                    self.bankAccountArr(data);
                }

                var bankAccountFunError = function(){
                }
                services.getGeneric(commonhelper.bankAccount).then(bankAccountFun,bankAccountFunError)
            }

            //     ---------------------------------------- supplier function ------------------------ 

            self.supplier = function (){ 

                var supplierFun = function(data){
                    self.supplierArr(data);
                }

                var supplierFunError = function(){
                }
                services.getGeneric(commonhelper.supplier).then(supplierFun,supplierFunError)
            }
            
            //     ---------------------------------------- message function ------------------------ 

            self.createMessage = function (type, summary) {
                return {
                    severity: type,
                    summary: summary,
                    autoTimeout: 4000
                };
            };

            //     ---------------------------------------- enter to login  ------------------------ 

            $(function () {
                $('#username').on('keydown', function (ev) {
                    var mobEvent = ev;
                    var mobPressed = mobEvent.keyCode || mobEvent.which;
                    if (mobPressed == 13) {
                        $('#username').blur();
                        $('#password').focus();
                    }
                    return true;
                });
            });


            //     ---------------------------------------- call jwt from helper ------------------------ 

            self.refresh = function () {
                $.when(showEmplDetails(oj, ko, services, self, false, commonhelper)).done(function () {
                    if (self.jwtinurl()) {
                        self.isUserLoggedIn(true)
                    }
                    else {
                        self.isUserLoggedIn(false);
                        self.userLogin(sessionStorage.Username);
                    }
                });
            };

            self.refresh();
            
            self.getALlNotification = function () {
                    services.getGeneric(commonhelper.getNotification+self.personNumberAdmin()).then(data=>{
                        var notifData=[];
                        if(self.personNumber()==self.personNumberAdmin()){
                        for (var i in data.Data) {
                            
                            if (data.Data[i].approvalStatus == 'Pending') {
                                notifData.push(data.Data[i]);
                            }
                        }
                        }else{
                            for (var i in data.Data) {
                            if (data.Data[i].approvalStatus == 'REJECT') {
                                notifData.push(data.Data[i]);
                            }
                        }
                        }
                        self.notiCount(notifData.length);
                    });
                };

            //     ---------------------------------------- login button function------------------------ 
                self.login = function (username) {
                    self.loading(true);
                    services.getGeneric(commonhelper.getEmp+username).then(data => {
                        self.loading(false);
                        self.personNumber(data.personNumber);
                        self.personId(data.personId);
                        self.personName(data.displayName);
                        self.getALlNotification();
                    }, error => {
                    });
                };

                self.validateUser = function () {
                    self.loading(true);
//                $(".apLoginBtn button").addClass("loading");
                    var username = self.username();
                    var password = self.password();
                    // Here we check null value and show error to fill
                    if (!username && !password) {
                        self.loading(false);
                        self.messagesDataProvider.push(self.createMessage("error", "Please enter user name and Password"));

                    } else if (!username) {
                        self.loading(false);
                        self.messagesDataProvider.push(self.createMessage("error", "Please enter user name"));

                    } else if (!password) {
                        self.loading(false);
                        self.messagesDataProvider.push(self.createMessage("error", "Please enter Password"));

                    } else {
//                    $('#loader-overlay').show();

                        var loginCbFn = function (data) {
                            self.loading(false);
//                        $(".apLoginBtn button").removeClass("loading");
                            self.messagesDataProvider([]);

                            var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));

                            if (parsedData.result == true) {
                                self.purchaseAgreement();
                                self.bankAccount();
                                self.supplier();
                                self.login(username);
                                self.isUserLoggedIn(true);
                                sessionStorage.setItem('username', self.isUserLoggedIn());

                                var userNamelabel = self.username();
                                sessionStorage.setItem('isUserLoggedIn', self.isUserLoggedIn());
                                sessionStorage.setItem('Username', userNamelabel);

//                                $('#loader-overlay').hide();

                            } else {
//                                $(".apLoginBtn button").removeClass("loading");
                                // If authentication failed
                                // alert(response.Error);
                                self.messagesDataProvider.push(self.createMessage("error", "Wrong user name or passowrd"));
                                self.loginUser(username);

                                return;
                            }
                        };

                        var loginFailCbFn = function () {
                            self.loading(false);
                            self.messagesDataProvider([]);
                            self.messagesDataProvider.push(self.createMessage("error", "Cannot validate UserName"));
//                            $(".apLoginBtn button").removeClass("loading");
                            return;
                        };

                        var payload = {
                            "userName": username,
                            "password": password
                        };
                        services.authenticate(payload).then(loginCbFn, loginFailCbFn);
                        return;
                    }
                };

            //     ---------------------------------------- home icon function------------------------ 

            $(document).on('click', '.home-icon', function () {
                oj.Router.rootInstance.go('lgScreen');
                // self.navDir.push({ page: "Penalty Screen", path: "lgScreen" })
                self.pathPages([{title:'Home',path:'lgScreen'}]);
            });

            //     ---------------------------------------- notification icon function ------------------------ 

            $(document).on('click', '.notifiIcon', function () {
                oj.Router.rootInstance.go('notificationScreen');
                // self.navDir.push({ page: "Penalty Summary", path: "penaltySetupSummary" })
                self.pathPages([{title:'Notification Screen',path:'notificationScreen'}]);
            });

            //     ---------------------------------------- log out button function------------------------ 

            self.logOutBtn = function (event) {
                // if (event.target.value === 'out') {
                location.reload();
                self.isUserLoggedIn(false);
                sessionStorage.setItem('username', '');
                sessionStorage.setItem('isUserLoggedIn', '');


                if (self.language() === 'english') {
                    $('html').attr({ 'dir': 'ltr' });
                } else if (self.language() === 'arabic') {
                    $('html').attr({ 'dir': 'rtl' });
                }

            };

            //     ---------------------------------------- Router setup function------------------------ 
            // Router setup
            self.router = oj.Router.rootInstance;
            self.router.dispose();

            self.router.configure({
                'lgScreen': { label: 'lgScreen', value: "lgScreen/lgScreen", id: "lgScreen", title: "TTC - LG", isDefault: true },
                'lgOperation': { label: 'lgOperation', value: "lgScreen/lgOperation", id: "lgOperation", title: "TTC - LG"},
                'lgHistory': { label: 'lgHistory', value: "lgHistory/lgHistory", id: "lgHistory", title: "TTC - LG"},
                'lgEditScreen': { label: 'lgEditScreen', value: "lgScreen/lgEditScreen", id: "lgEditScreen", title: "TTC - LG"},
                'notificationScreen': { label: 'notificationScreen', value: "notification/notificationScreen", id: "notificationScreen", title: "TTC - LG"},
                'confiscationScreen': { label: 'confiscationScreen', value: "confiscation/confiscationScreen", id: "confiscationScreen", title: "TTC - LG"}
            });
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();


            self.langSelected = function (event) {
                var valueObj = buildValueChange(event['detail']);

                self.setPreferedLanguage(valueObj.value);
            };

            function buildValueChange(valueParam) {
                var valueObj = {};
                //valueObj.previousValue = valueParam.previousValue;
                if (valueParam.previousValue) {
                    valueObj.value = valueParam.value;
                } else {
                    valueObj.value = valueParam.previousValue;
                }
                //
                return valueObj;
            }

            $(function () {
                storage.setItem('setLang', 'english');
                //                    storage.setItem('setLang', 'arabic');
                preferedLanguage = storage.getItem('setLang');
                self.language(preferedLanguage);
                changeToArabic(preferedLanguage);
                //document.addEventListener("deviceready", selectLanguage, false);
            });

            // Choose prefered language
            function selectLanguage() {
                oj.Config.setLocale('english',
                    function () {
                        $('html').attr({ 'lang': 'en-us', 'dir': 'ltr' });
                        self.localeLogin(oj.Translations.getTranslatedString('Login'));
                        self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                        self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                        self.loginword(oj.Translations.getTranslatedString('login'));
                        self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                        self.localeName(oj.Translations.getTranslatedString('Name'));
                        self.localeJop(oj.Translations.getTranslatedString('JOP'));
                        self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                        self.localeDep(oj.Translations.getTranslatedString('Dep'));
                        self.localeGrade(oj.Translations.getTranslatedString('Grade'));

                    }
                );
            }

            // Set Prefered Language
            self.setPreferedLanguage = function (lang) {
                var selecedLanguage = lang;
                storage.setItem('setLang', selecedLanguage);
                storage.setItem('setTrue', true);
                preferedLanguage = storage.getItem('setLang');

                // Call function to convert arabic
                changeToArabic(preferedLanguage);
            };

            function changeToArabic(preferedLanguage) {
                if (preferedLanguage == 'arabic') {

                    oj.Config.setLocale('ar-sa',
                        function () {
                            $('html').attr({ 'lang': 'ar-sa', 'dir': 'rtl' });
                            self.localeLogin(oj.Translations.getTranslatedString('Login'));
                            self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                            self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                            self.loginword(oj.Translations.getTranslatedString('login'));
                            self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                            self.localeName(oj.Translations.getTranslatedString('Name'));
                            self.localeJop(oj.Translations.getTranslatedString('JOP'));
                            self.localeempNO(oj.Translations.getTranslatedString('EmpNO'));
                            self.localeDep(oj.Translations.getTranslatedString('Dep'));
                            self.localeGrade(oj.Translations.getTranslatedString('Grade'));
                        }
                    );

                } else if (preferedLanguage == 'english') {
                    selectLanguage();
                }
            }
            $('.login-label').toggleClass('user-label-right');
            $('.input100').toggleClass('box-input-text');
            $('.wrap-input100').toggleClass('box-input-text');
            self.changeLanguage = function () {
                //storage.setItem('setLang', 'arabic');
                if (self.language() === 'english') {
                    self.language("arabic");
                    self.loginBtnLbl("تسجيل دخول")
                    self.setPreferedLanguage(self.language());
                    self.languagelable("English");
                    $('.login-label').toggleClass('user-label-right');
                    $('.login-label').removeClass('user-label-left');
                    $('.input100').toggleClass('box-input-text');
                    $('.wrap-input100').toggleClass('box-input-text');
                } else if (self.language() === 'arabic') {
                    self.language("english");
                    self.loginBtnLbl("LOGIN")
                    self.setPreferedLanguage(self.language());
                    self.languagelable("عربي");
                    $('.login-label').toggleClass('user-label-left');
                    $('.login-label').removeClass('user-label-right');
                    $('.input100').removeClass('box-input-text');
                    $('.wrap-input100').removeClass('box-input-text');


                }
            };


            self.moduleConfig = ko.observable({ 'view': [], 'viewModel': null });

            // Search Start
            self.keyword = ko.observableArray();
            //Business Unit Input search Start 

            self.searchWord = ko.observable();
            self.tags = [
                { value: "Value 1", label: "Unit 1" },
                { value: "Value 2", label: "Unit 2" },
                { value: "Value 3", label: "Unit 3" },
                { value: "Value 4", label: "Unit 4" },
                { value: "Value 5", label: "Unit 5" },
                { value: "Value 6", label: "Unit 6" },
                { value: "Value 7", label: "Unit 7" }
            ];

            self.tagsDataProvider = new oj.ArrayDataProvider(self.tags, { keyAttributes: 'value' });

            self.search = function (event) {
                var trigger = event.type;
                var term;

                if (trigger === "ojValueUpdated") {
                    // search triggered from input field
                    // getting the search term from the ojValueUpdated event
                    term = event['detail']['value'];
                    trigger += " event";
                } else {
                    // search triggered from end slot
                    // getting the value from the element to use as the search term.
                    term = document.getElementById("search").value;
                    trigger = "click on search button";
                }

            };

            self.loadModule = async function () {
                //    oj.Router.rootInstance.go('lgScreen');
                ko.computed(function () {
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    var masterPromise = Promise.all([
                        moduleUtils.createView({ 'viewPath': viewPath }),
                        moduleUtils.createViewModel({ 'viewModelPath': modelPath })
                    ]);
                    masterPromise.then(
                        function (values) {
                            self.moduleConfig({ 'view': values[0], 'viewModel': values[1] });
                        },
                        function (reason) { }
                    );
                });
            };


            // Drawer
            // Close offcanvas on medium and larger screens
            // self.mdScreen.subscribe(function () {
            //     oj.OffcanvasUtils.close(self.drawerParams);
            // });
            self.drawerParams = {
                displayMode: 'push',
                selector: '#navDrawer',
                content: '#pageContent'
            };
            // Called by navigation drawer toggle button and after selection of nav drawer item
            self.toggleDrawer = function () {
                return oj.OffcanvasUtils.toggle(self.drawerParams);
            };
            // Add a close listener so we can move focus back to the toggle button when the drawer closes
            $("#navDrawer").on("ojclose", function () {
                $('#drawerToggleButton').focus();
            });

            // Header
            // Application Name used in Branding Area
            self.appName = ko.observable("Nexus");
            // User Info used in Global Navigation area


            // Footer
            function footerLink(name, id, linkTarget) {
                this.name = name;
                this.linkId = id;
                this.linkTarget = linkTarget;
            }
            self.footerLinks = ko.observableArray([
                new footerLink('About Oracle', 'aboutOracle', 'http://www.oracle.com/us/corporate/index.html#menu-about'),
                new footerLink('Contact Us', 'contactUs', 'http://www.oracle.com/us/corporate/contact/index.html'),
                new footerLink('Legal Notices', 'legalNotices', 'http://www.oracle.com/us/legal/index.html'),
                new footerLink('Terms Of Use', 'termsOfUse', 'http://www.oracle.com/us/legal/terms/index.html'),
                new footerLink('Your Privacy Rights', 'yourPrivacyRights', 'http://www.oracle.com/us/legal/privacy/index.html')
            ]);
        }

        return new ControllerViewModel();
    }
);
