define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper','ojs/ojselectcombobox','ojs/ojarraydataprovider',
'ojs/ojknockout', 'ojs/ojlabelvalue','ojs/ojtable', 'ojs/ojdatetimepicker'
], function (oj, ko, $, app, services, commonhelper) {
    function lgHistoryContentViewModel() {
        var self = this;
        self.historyScreenLbl = 'LG History Screen';
        var retrieved=oj.Router.rootInstance.retrieve();
        self.label = ko.observable();
        self.customsModel = {
            lgArr: ko.observableArray([]),
            columnArray: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(retrieved.lgNumFromBank),
            suppilerNameVal: ko.observable(retrieved.supplier),
            contractNoVal: ko.observable(retrieved.contractNumber),
            lgTypeVal: ko.observable(retrieved.lgType),
            lgStatusVal: ko.observable(retrieved.lgStatus),
            dateVal: ko.observable(),
            amountVal: ko.observable(),
            documentNumber: ko.observable(retrieved.documentNumber),
            disableAllFields: ko.observable(true)
        };
        self.operation="Operation";
        self.dataproviderLgScreen = new oj.ArrayDataProvider(self.customsModel.lgArr, {idAttributes: 'id'});
        self.connected = function ()
        {
            app.loading(true);
            services.getGeneric(commonhelper.getLgHistory+retrieved.documentNumber).then(data => {
                app.loading(false);
                if (data) {
                    for(var i in data.Data){
                    if(data.Data[i].history=='original'){
                        self.customsModel.dateVal(data.Data[i].lgEndDate);
                        self.customsModel.amountVal(data.Data[i].lgAmount);
                    }
                }
                    self.customsModel.lgArr(data.Data);
                }
            }, error => {
                app.loading(false);
            });
        };

        self.lgselectListener = function (event)
        {

        };

        self.backBtn = function ()
        {
            oj.Router.rootInstance.go('lgScreen');
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                suppilerNameLbl: getTransaltion("lgScreen.suppilerNameLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgStatusLbl: getTransaltion("lgScreen.lgStatusLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                bankAmountLbl: getTransaltion("lgScreen.bankAmountLbl"),
                amountLbl: getTransaltion("lgScreen.amountLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                status: getTransaltion("lgScreen.status"),
                documentNumberLbl:getTransaltion("lgScreen.documentNumberLbl"),
                modifiedDateLbl:getTransaltion("lgScreen.modifiedDateLbl"),
                dateLbl:getTransaltion("lgScreen.dateLbl"),
                backLbl: getTransaltion("common.backLbl")
            });
            self.customsModel.columnArray([
                {
                    "headerText": self.label().documentNumberLbl, "field": "documentNumber"
                },
                {
                    "headerText": self.operation, "field": "operation"
                },
                {
                    "headerText": self.label().amountLbl, "field": "lgAmount"
                },
                {
                    "headerText": self.label().lgEndDateLbl, "field": "lgEndDate"
                },
                {
                    "headerText": self.label().status, "field": "lgStatus"
                },
                {
                    "headerText": self.label().modifiedDateLbl, "field": "modDate"
                }
            ]);
        }
        initTransaltion();
    }
    
    return lgHistoryContentViewModel;
});
