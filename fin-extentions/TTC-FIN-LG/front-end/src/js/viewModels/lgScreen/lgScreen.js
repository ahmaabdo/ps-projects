define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'xlsx', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingcontrol'
], function(oj, ko, $, app, services, commonhelper) {
    function lgScreenContentViewModel() {
        var self = this;
        self.lgScreenLbl = 'LG Screen';
        self.label = ko.observable();

        self.customsModel = {
            lgArr: ko.observableArray([]),
            lgExpireArr: ko.observableArray([]),
            lgNoCancelArr: ko.observableArray([]),
            columnArray: ko.observableArray(),
            suppilerNameArr: ko.observableArray(),
            lgNumberFromBankArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgStatusArr: ko.observableArray(),
            rowSelected: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(),
            suppilerNameVal: ko.observable(),
            contractNoVal: ko.observable(),
            lgStatusVal: ko.observable(),
            documentNumber: ko.observable(),
            searchClicked: ko.observable(false),
            disableAllFields: ko.observable(true),
            resetDis: ko.observable(false),
            lgEditSelected: ko.observable(true),
            lgSelected: ko.observable(true),
            lgViewSelected: ko.observable(true),
            data: ko.observableArray([])
        };

        self.dataproviderLgScreen = ko.observable();
        self.dataProviderLgCloseToExpire = ko.observable();
        self.filterExportData = ko.observableArray([]);
        self.dataproviderLgScreen(new oj.ArrayDataProvider(self.customsModel.lgArr, { keyAttributes: 'id' }));
        self.dataProviderLgCloseToExpire(new oj.ArrayDataProvider(self.customsModel.lgExpireArr, { keyAttributes: 'id' }));
        //        self.pagingDataProvider = new PagingDataProviderView(new oj.ArrayTableDataSource(self.customsModel.lgArr));
        // self.customsModel.dataproviderLgScreen = new oj.PagingDataProviderView(new oj.ArrayDataProvider(self.customsModel.lgArr, {idAttribute: 'documentNumber'}));


        self.connected = function() {
            //            self.getData();
        };

        self.getData = function() {
            app.loading(true);
            //            self.customsModel.lgExpireArr([]);
            services.getGeneric(commonhelper.getLg).then(data => {
                app.loading(false);
                var date = formatDate(new Date());
                var diff;
                if (data.Data) {
                    self.customsModel.lgArr([]);
                    for (var i in data.Data) {
                        diff = date_diff_indays(date, data.Data[i].lgEndDate);
                        data.Data[i].lgCloseToExp = diff;
                        if (diff < 30 && (data.Data[i].lgStatus == 'In Process' || data.Data[i].lgStatus == 'Pending Approval')) {
                            self.customsModel.lgExpireArr.push(data.Data[i]);
                            self.dataProviderLgCloseToExpire(new oj.ArrayDataProvider(self.customsModel.lgExpireArr, { idAttributes: 'id' }));
                        }
                        if (data.Data[i].lgStatus == 'In Process' || data.Data[i].lgStatus == 'Rejected') {
                            self.customsModel.lgArr.push(data.Data[i]);
                            self.dataproviderLgScreen(new oj.ArrayDataProvider(self.customsModel.lgArr, { keyAttributes: 'id' }));
                        }
                    }
                    self.customsModel.data(data.Data);
                    self.customsModel.lgNoCancelArr(self.customsModel.lgArr());
                    var suppArr = [...new Set(data.Data.map(el => el.supplier))];
                    var lgNumArr = [...new Set(data.Data.map(el => el.lgNumFromBank))];
                    var contractNumArr = [...new Set(data.Data.map(el => el.contractNumber))];
                    var lgStatusArr = [...new Set(data.Data.map(el => el.lgStatus))];
                    lgStatusArr.push("All");
                    self.customsModel.suppilerNameArr(suppArr.map(el => ({ label: el, value: el })));
                    self.customsModel.lgNumberFromBankArr(lgNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.contractNumberArr(contractNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.lgStatusArr(lgStatusArr.map(el => ({ label: el, value: el })));
                }

            }, error => {
                app.loading(false);
            });
        };

        ko.computed(() => {
            if (app.personNumber()) {
                self.getData();
            }
        });

        self.lgselectListener = function(event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.lgViewSelected(false);
                self.customsModel.rowSelected = self.customsModel.lgArr()[[currentRow['rowIndex']]];
                if (self.customsModel.rowSelected.lgStatus == 'In Process') {
                    self.customsModel.lgSelected(false);
                    if (app.personNumber() == app.personNumberAdmin())
                        self.customsModel.searchClicked(true);
                    else
                        self.customsModel.searchClicked(false);
                } else {
                    self.customsModel.lgSelected(true);
                    self.customsModel.searchClicked(false);
                }
                if (app.personNumber() == app.personNumberAdmin() || self.customsModel.rowSelected.lgStatus == 'Rejected') {
                    if (self.customsModel.rowSelected.lgStatus !== 'Cancelled') {
                        self.customsModel.lgEditSelected(false);
                    } else {
                        self.customsModel.lgEditSelected(true);
                    }
                }
            }
        };

        self.lgCloseToExpireSelectionListener = function(event) {
            var data = event.detail;
            if (!data)
                return;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.lgSelected(false);
                self.customsModel.searchClicked(true);
                self.customsModel.rowSelected = self.customsModel.lgExpireArr()[[currentRow['rowIndex']]];
            }
        };

        self.viewBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                data.view = "view";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.searchBtn = function() {
            cond = function(el) {
                var res = (
                    (self.customsModel.documentNumber() ? el.documentNumber == self.customsModel.documentNumber() : true) &&
                    (self.customsModel.lgNumberFromBankVal() ? el.lgNumFromBank == self.customsModel.lgNumberFromBankVal() : true) &&
                    (self.customsModel.suppilerNameVal() ? el.supplier == self.customsModel.suppilerNameVal() : true) &&
                    (self.customsModel.contractNoVal() ? el.contractNumber == self.customsModel.contractNoVal() : true) &&
                    (self.customsModel.lgStatusVal() ? el.lgStatus == self.customsModel.lgStatusVal() : true)
                );
                return res;
            };
            var find = self.customsModel.data().filter(el => cond(el));
            if (find.length == self.customsModel.data().length) {
                self.customsModel.lgArr(self.customsModel.lgNoCancelArr());
                self.dataproviderLgScreen(new oj.ArrayDataProvider(self.customsModel.lgArr, { keyAttributes: 'id' }));
            } else {
                self.customsModel.lgArr(find);
                self.dataproviderLgScreen(new oj.ArrayDataProvider(self.customsModel.lgArr, { keyAttributes: 'id' }));
            }
            if (self.customsModel.lgStatusVal() == "All") {
                self.customsModel.lgArr(self.customsModel.data());
                self.dataproviderLgScreen(new oj.ArrayDataProvider(self.customsModel.lgArr, { keyAttributes: 'id' }));
            }

        };

        self.resetBtn = function() {
            self.customsModel.resetDis(true);
            setTimeout(function() {
                self.customsModel.resetDis(false);
                //            self.customsModel.lgArr([]);
                self.customsModel.lgArr(self.customsModel.lgNoCancelArr());
                self.dataproviderLgScreen(new oj.ArrayDataProvider(self.customsModel.lgArr, { keyAttributes: 'id' }));
            }, 1000);
            self.customsModel.searchClicked(false);
            self.customsModel.documentNumber('');
            self.customsModel.lgNumberFromBankVal('');
            self.customsModel.suppilerNameVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgStatusVal('');
        };

        self.createBtn = function() {
            oj.Router.rootInstance.go('lgOperation');
        };

        self.historyBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgHistory');
            }
        };

        self.addHistory = function() {
            services.postGeneric(commonhelper.addLgHistoryRelease, payload).then(data => {
                app.loading(false);
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
            });
        };

        function download(filename, text) {
            var link = document.createElement("a");
            link.download = filename;
            link.href = text;
            link.click();
        }

        self.yes = function() {
            document.querySelector("#confirmRelease").close();
            var selected = self.customsModel.rowSelected;
            app.loading(true);
            payload = [{
                id: selected.id,
                documentNumber: selected.documentNumber,
                contractNumber: selected.contractNumber,
                contractAmount: selected.contractAmount,
                supplier: selected.supplier,
                contractStartDate: selected.contractStartDate,
                contractEndDate: selected.contractEndDate,
                lgNumFromBank: selected.lgNumFromBank,
                lgType: selected.lgType,
                lgEndDate: selected.lgEndDate,
                region: selected.region,
                lgAmount: selected.lgAmount,
                bankAccount: selected.bankAccount,
                description: selected.description,
                lgStatus: "Released",
                operation: "Release",
                approvalStatus: selected.approvalStatus,
                createdBy: selected.createdBy
            }];
            services.postGeneric(commonhelper.updateStatus, payload[0]).then(data => {
                if (data) {
                    var selected = self.customsModel.rowSelected;
                    services.postGeneric(commonhelper.lgFormatWord, selected).then(data => {
                        var x = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,";
                        var xx = x + data.Data;
                        download("نموذج افراج عن ضمان.docx", xx)
                        document.querySelector("#confirmRelease").close();
                        app.loading(false);
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'LG Extend Format Download Successfully'));
                    }, error => {

                    })
                    self.addHistory();
                    self.getData();
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

        self.extendBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                data.extend = "extend";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.increaseBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                data.incLg = "incLg";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.decreaseBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                data.decLg = "decLg";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        var payload;
        self.releaseBtn = function() {
            document.querySelector("#confirmRelease").open();
        };

        self.confiscationBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                data.confis = "confis";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go("confiscationScreen");
            }
        };

        self.editBtn = function() {
            if (self.customsModel.rowSelected) {
                var data = self.customsModel.rowSelected;
                data.edit = "edit";
                oj.Router.rootInstance.store(data);
                oj.Router.rootInstance.go('lgEditScreen');
            }
        };

        self.exportFile = function(arr) {
            if (arr.length == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'No LG Data Found'));
            } else {
                /* make the worksheet */
                var ws = XLSX.utils.json_to_sheet(arr);
                //            /* add to workbook */
                var wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, "LG");
                //            /* generate an XLSX file */
                XLSX.writeFile(wb, "LG.xlsx");
                app.messagesDataProvider.push(app.createMessage('confirmation', 'Data Exported '));
            }
        };

        self.exportBtn = function() {
            self.filterExportData(self.customsModel.lgArr());
            self.exportFile(self.filterExportData());
        };

        self.closeToExpBtn = function() {
            document.querySelector("#viewExpire").open();
        };

        self.no = function() {
            document.querySelector("#confirmRelease").close();
        };

        self.cancelBtn = function() {
            document.querySelector("#viewExpire").close();
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function(date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            var x = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
            return x
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                suppilerNameLbl: getTransaltion("lgScreen.suppilerNameLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgStatusLbl: getTransaltion("lgScreen.lgStatusLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                bankAmountLbl: getTransaltion("lgScreen.bankAmountLbl"),
                amountLbl: getTransaltion("lgScreen.amountLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                status: getTransaltion("lgScreen.status"),
                searchLbl: getTransaltion("common.searchLbl"),
                viewLbl: getTransaltion("common.viewLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                createLbl: getTransaltion("common.createLbl"),
                closeToExpLbl: getTransaltion("common.closeToExpLbl"),
                extendLbl: getTransaltion("common.extendLbl"),
                increaseLbl: getTransaltion("common.increaseLbl"),
                decreaseLbl: getTransaltion("common.decreaseLbl"),
                releaseLbl: getTransaltion("common.releaseLbl"),
                confiscationLbl: getTransaltion("common.confiscationLbl"),
                editLbl: getTransaltion("common.editLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                historyLbl: getTransaltion("lgScreen.historyLbl"),
                approvalStatus: getTransaltion("lgScreen.approvalStatus"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                confirmText: getTransaltion("common.confirmText"),
                confirmReleaseLbl: getTransaltion("lgScreen.confirmReleaseLbl"),
                contractAmountLbl: getTransaltion("lgScreen.contractAmountLbl"),
                exportLbl: getTransaltion("common.exportLbl"),
                descriptionLbl: getTransaltion("lgOperation.descriptionLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                lgCloseToExp: getTransaltion("common.closeToExpLbl")
            });
            self.customsModel.columnArray([{
                    "headerText": self.label().documentNumberLbl,
                    "field": "documentNumber"
                },
                {
                    "headerText": self.label().lgNumberFromBankLbl,
                    "field": "lgNumFromBank"
                },
                {
                    "headerText": self.label().contractNoLbl,
                    "field": "contractNumber"
                },
                {
                    "headerText": self.label().status,
                    "field": "lgStatus"
                },
                {
                    "headerText": self.label().suppilerNameLbl,
                    "field": "supplier"
                },
                {
                    "headerText": self.label().bankAccountLbl,
                    "field": "bankAccount"
                },
                {
                    "headerText": self.label().lgCloseToExp,
                    "field": "lgCloseToExp"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().contractAmountLbl,
                    "field": "contractAmount"
                },
                {
                    "headerText": self.label().amountLbl,
                    "field": "lgAmount"
                },
                {
                    "headerText": self.label().lgEndDateLbl,
                    "field": "lgEndDate"
                },
                {
                    "headerText": self.label().descriptionLbl,
                    "field": "description"
                },
                {
                    "headerText": self.label().approvalStatus,
                    "field": "approvalStatus"
                }
            ]);
        }
        initTransaltion();
    }

    return lgScreenContentViewModel;
});