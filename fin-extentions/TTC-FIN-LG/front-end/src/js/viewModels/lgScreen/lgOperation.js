define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker', 'ojs/ojvalidationgroup'
], function(oj, ko, $, app, services, commonhelper) {
    function lgOperationContentViewModel() {
        var self = this;
        self.lgCreateScreenLbl = 'Create LG Screen';
        self.label = ko.observable();
        self.groupValid = ko.observable();
        self.customsModel = {
            lgArr: ko.observableArray([]),
            attachmentArr: ko.observableArray(),
            attachmentColumns: ko.observableArray(),
            columnArray: ko.observableArray(),
            description: ko.observable(""),
            bankAmountArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgTypeArr: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(),
            supplierVal: ko.observable(),
            contractNoVal: ko.observable(),
            lgTypeVal: ko.observable(),
            contractAmountVal: ko.observable(),
            contractStartDateVal: ko.observable(),
            contractEndDateVal: ko.observable(),
            letterOfGuarnAmoutVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            regionVal: ko.observable('Headquarter'),
            documentNumber: ko.observable(),
            bankAccountVal: ko.observable(),
            closeToExp: ko.observable(),
            disableAllFields: ko.observable(true),
            addFileDis: ko.observable(false),
            createLg: ko.observable(true),
            disableAdvance: ko.observable(true),
            selected_files: ko.observableArray(),
            fileData: ko.observable(),
            contentData: ko.observable(),
            data: ko.observableArray([]),
            contractNum: ko.observable(),
            letterIncVal: ko.observable(),
            lgType: ko.observable(),
            bankAccountArr: ko.observable(),
            contractNumUsed: ko.observableArray([]),
            attTitle: ko.observable(),
            attName: ko.observable(),
            attdocNum: ko.observable(),
        };
        self.dataProviderAttachment = ko.observable()

        self.payloadAttach = ko.observableArray([]);
        var payload;
        var personNumber;
        var personName;
        self.dataproviderLgScreen = new oj.ArrayDataProvider(self.customsModel.lgArr, { idAttributes: 'id' });
        // self.dataProviderAttachment = new oj.ArrayTableDataSource(self.customsModel.attachmentArr, { idAttributes: 'title' });
        self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))

        self.removedArray = ko.observableArray([]);
        self.selectedItemsAttachment = ko.observableArray([]);
        self.connected = function() {
            self.customsModel.lgTypeArr.push({ label: 'Bid bond', value: 'Bid bond' }, { label: 'Final', value: 'Final' }, { label: 'Advance', value: 'Advance' });
        };

        self.clear = function() {
            self.customsModel.documentNumber(self.customsModel.documentNumber() + 1);
            self.customsModel.lgNumberFromBankVal(0);
            self.customsModel.supplierVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgTypeVal('');
            self.customsModel.contractAmountVal(0);
            self.customsModel.contractStartDateVal('');
            self.customsModel.contractEndDateVal('');
            self.customsModel.letterOfGuarnAmoutVal(0);
            self.customsModel.lgEndDateVal('');
            self.customsModel.regionVal();
            self.customsModel.bankAccountVal('');
            self.customsModel.description('');
        };

        self.getContractNumber = function() {
            app.loading(true);
            services.getGeneric(commonhelper.getLg).then(data => {
                app.loading(false);
                if (data) {
                    var contractNumArr = [...new Set(data.Data.map(el => el.contractNumber))];
                    self.customsModel.contractNumUsed(contractNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.contractNumUsed().forEach(e => {
                        self.customsModel.contractNumberArr(self.customsModel.contractNumberArr().filter(el => el.value != e.value));
                    });
                }

            }, error => {
                app.loading(false);
            });
        };

        ko.computed(_ => {
            if (app.agreementArr()) {
                var agreementValue = app.agreementArr().filter(el => el.AGREEMENTSTSTUS == "OPEN")
                agreementValue = agreementValue.map(el => ({ label: el.AGREEMENTNUMBER, value: el.AGREEMENTNUMBER }))
                self.customsModel.contractNumberArr(agreementValue);
                self.getContractNumber();
            }
            if (app.bankAccountArr()) {
                var bankAccValue = app.bankAccountArr().map(el => ({ label: el.bankAccount, value: el.bankAccount }))
                self.customsModel.bankAccountArr(bankAccValue);
            }
        });

        ko.computed(() => {
            if (app.personNumber() && app.personName()) {
                personNumber = app.personNumberAdmin();
                personName = app.personNameAdmin();
            };
            services.getGeneric(commonhelper.getLgByDocNum).then(data => {
                if (data.Data.length > 0) {
                    var lastDocNum = data.Data[0].documentNumber;
                    self.customsModel.documentNumber(lastDocNum + 1);
                } else {
                    self.customsModel.documentNumber(1);
                }
            }, error => {});

        });

        self.lgselectListener = function(event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.lgArr()[[currentRow['rowIndex']]];
            }
        };

        self.addFileSelectListener = function(event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function(evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                self.customsModel.attdocNum(self.customsModel.documentNumber())
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "docNum": self.customsModel.attdocNum(),
                    "content": self.customsModel.contentData(),
                }

                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name)
                            self.payloadAttach().push(x);
                        x = {};
                        self.customsModel.attachmentArr(self.payloadAttach());
                    }
                })
            };

            reader.readAsDataURL(self.customsModel.fileData()[0]);
            self.customsModel.selected_files(self.customsModel.fileData());
            self.customsModel.addFileDis(true);
        };

        self.uploadContarctAttachment = function(event) {
            app.loading(true);
            services.postGeneric(commonhelper.insertAttachment, self.payloadAttach()).then(data => {
                app.loading(false);
            }, error => { app.loading(false); });
        };

        self.viewContarctAttachment = function() {
            self.customsModel.attachmentArr([]);
            self.customsModel.attachmentArr(self.payloadAttach());
            document.querySelector("#viewAttachment").open();
        };

        self.removePickerBtnAction = function(event) {
            if (document.querySelector("oj-file-picker input"))
                document.querySelector("oj-file-picker input").value = null;

            self.customsModel.selected_files([]);
            self.customsModel.addFileDis(false);
        };

        self.removeSelectedAttachment = function() {

            if (self.selectedItemsAttachment().length < 1)
                app.messagesDataProvider.push(app.createMessage('error', 'Please select an item'));

            var x = self.customsModel.attachmentArr().findIndex((el => el.name == self.selectedItemsAttachment()[0][1]))
            self.customsModel.attachmentArr().splice(x, 1);
            self.dataProviderAttachment(new oj.ArrayTableDataSource(self.customsModel.attachmentArr))
        };

        var submit;
        submit = false;
        self.submitBtn = function() {
            submit = true;
            app.loading(true);
            var payload2 = [];
            services.postGeneric(commonhelper.addLg, payload).then(data => {
                app.loading(false);
                if (data) {
                    payload[0].cancel = "false";
                    payload2.push({ empId: personNumber, empName: personName, notification: payload[0] });
                    services.postGeneric(commonhelper.addNotification, payload2);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been saved '));
                    if (self.customsModel.attachmentArr().length > 0)
                        self.uploadContarctAttachment();
                    self.customsModel.attachmentArr([]);
                    self.payloadAttach([]);
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
            });
            self.clear();
            self.getContractNumber();
            self.customsModel.lgArr([]);
            self.customsModel.createLg(true);
            self.customsModel.selected_files([]);
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };
        var date_diff_indays = function(date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        };
        var create;
        create = false;
        self.createBtn = function() {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid") {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }
            create = true;
            if (self.customsModel.supplierVal() == "No data found" || self.customsModel.contractStartDateVal() == "No data found" ||
                self.customsModel.contractEndDateVal() == "No data found" || self.customsModel.contractAmountVal() == 0) {
                app.messagesDataProvider.push(app.createMessage('error', 'Data Is Missing'));
            } else {
                var approveStatus = "Pending";
                var lgStatus = "Pending Approval";
                if (app.personNumber() == app.personNumberAdmin()) {
                    approveStatus = "APPROVED";
                    lgStatus = "In Process";
                }
                self.customsModel.createLg(false);
                payload = [{
                    documentNumber: self.customsModel.documentNumber(),
                    contractNumber: self.customsModel.contractNoVal(),
                    contractAmount: self.customsModel.contractAmountVal().toString(),
                    supplier: self.customsModel.supplierVal(),
                    contractStartDate: self.customsModel.contractStartDateVal(),
                    contractEndDate: self.customsModel.contractEndDateVal(),
                    lgNumFromBank: self.customsModel.lgNumberFromBankVal(),
                    lgType: self.customsModel.lgTypeVal(),
                    lgAmount: self.customsModel.letterOfGuarnAmoutVal().toString(),
                    lgEndDate: self.customsModel.lgEndDateVal(),
                    region: self.customsModel.regionVal(),
                    bankAccount: self.customsModel.bankAccountVal(),
                    description: self.customsModel.description(),
                    lgStatus: lgStatus,
                    approvalStatus: approveStatus,
                    createdBy: app.personNumber()
                }];
                var date = formatDate(new Date());
                var diff;
                diff = date_diff_indays(date, self.customsModel.lgEndDateVal());
                self.customsModel.closeToExp(diff);
                payload[0].closeToExp = self.customsModel.closeToExp();
                self.customsModel.lgArr(payload);
            }
        }
        self.backBtn = function() {
            if (!create) {
                oj.Router.rootInstance.go('lgScreen');
            } else {
                if (!submit)
                    document.querySelector("#confirmDiscard").open();
                create = false;
            }
        };
        self.yes = function() {
            oj.Router.rootInstance.go('lgScreen');
        };
        self.no = function() {
            document.querySelector("#confirmDiscard").close();
        };
        self.cancelviewAttDial = function() {
            document.querySelector("#viewAttachment").close();
        };

        self.contactNumberListener = function(event) {
            self.customsModel.contractNum(event['detail'].value);
            self.customsModel.letterOfGuarnAmoutVal(0);
            if (self.customsModel.contractNum()) {
                if (app.agreementArr() && app.supplierArr()) {
                    var agreementObj = app.agreementArr().find(el => el.AGREEMENTNUMBER == self.customsModel.contractNum())
                    if (agreementObj) {
                        var x = app.supplierArr().find(el => el.supplierId == agreementObj.SUPPLIER);
                        agreementObj.AGREEMENTAMOUNT ? self.customsModel.contractAmountVal(agreementObj.AGREEMENTAMOUNT) : self.customsModel.contractAmountVal(0);
                        agreementObj.AGREEMENTSTARTDATE ? self.customsModel.contractStartDateVal(agreementObj.AGREEMENTSTARTDATE.split("T")[0]) : self.customsModel.contractStartDateVal("No data found");
                        agreementObj.AGREEMENTENDDATE ? self.customsModel.contractEndDateVal(agreementObj.AGREEMENTENDDATE.split("T")[0]) : self.customsModel.contractEndDateVal("No data found");
                        agreementObj.SUPPLIER ? self.customsModel.supplierVal(app.supplierArr().find(el => el.supplierId == agreementObj.SUPPLIER).supplierName) : self.customsModel.supplierVal("No data found");
                    }
                }
            }
        };

        self.lgTypeListener = function(event) {
            self.customsModel.lgType(event['detail'].value);
            if (self.customsModel.contractNoVal()) {
                self.customsModel.letterOfGuarnAmoutVal(0);

                if (self.customsModel.contractAmountVal() != "No data found") {
                    if (self.customsModel.lgType() == "Bid bond") {
                        self.customsModel.letterIncVal(self.customsModel.contractAmountVal() * 1 / 100);
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal() * 1 / 100);
                    }

                    if (self.customsModel.lgType() == "Final") {
                        self.customsModel.letterIncVal(self.customsModel.contractAmountVal() * 5 / 100);
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal() * 5 / 100);
                    }
                    if (self.customsModel.lgType() == "Advance")
                        self.customsModel.disableAdvance(false);
                    else
                        self.customsModel.disableAdvance(true);

                }
            }
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                contractStartDateLbl: getTransaltion("lgOperation.contractStartDateLbl"),
                contractEndDateLbl: getTransaltion("lgOperation.contractEndDateLbl"),
                letterOfGuarnAmoutLbl: getTransaltion("lgOperation.letterOfGuarnAmoutLbl"),
                contractAmountLbl: getTransaltion("lgOperation.contractAmountLbl"),
                supplierLbl: getTransaltion("lgOperation.supplierLbl"),
                regionLbl: getTransaltion("lgOperation.regionLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                uploadLbl: getTransaltion("common.uploadLbl"),
                descriptionLbl: getTransaltion("lgOperation.descriptionLbl"),
                closeToExpLbl: getTransaltion("common.closeToExpLbl"),
                submitLbl: getTransaltion("common.submitLbl"),
                backLbl: getTransaltion("common.backLbl"),
                addFileLbl: getTransaltion("common.addFileLbl"),
                viewAttachmentLbl: getTransaltion("common.viewAttachmentLbl"),
                titleLbl: getTransaltion("lgOperation.titleLbl"),
                nameLbl: getTransaltion("lgOperation.nameLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                createLbl: getTransaltion("common.createLbl"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),
                confirmText: getTransaltion("common.confirmDiscardText"),
                confirmDiscard: getTransaltion("common.confirmText")
            });

            self.customsModel.columnArray([{
                    "headerText": self.label().supplierLbl,
                    "field": "supplier"
                },
                {
                    "headerText": self.label().documentNumberLbl,
                    "field": "documentNumber"
                },
                {
                    "headerText": self.label().bankAccountLbl,
                    "field": "bankAccount"
                },
                {
                    "headerText": self.label().regionLbl,
                    "field": "region"
                },
                {
                    "headerText": self.label().contractNoLbl,
                    "field": "contractNumber"
                },
                {
                    "headerText": self.label().lgNumberFromBankLbl,
                    "field": "lgNumFromBank"
                },
                {
                    "headerText": self.label().letterOfGuarnAmoutLbl,
                    "field": "lgAmount"
                },
                {
                    "headerText": self.label().contractAmountLbl,
                    "field": "contractAmount"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().contractStartDateLbl,
                    "field": "contractStartDate"
                },
                {
                    "headerText": self.label().contractEndDateLbl,
                    "field": "contractEndDate"
                },
                {
                    "headerText": self.label().lgEndDateLbl,
                    "field": "lgEndDate"
                },
                {
                    "headerText": self.label().closeToExpLbl,
                    "field": "closeToExp"
                }
            ]);

            self.customsModel.attachmentColumns([{
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                }
            ]);
        }
        initTransaltion();
    }

    return lgOperationContentViewModel;
});