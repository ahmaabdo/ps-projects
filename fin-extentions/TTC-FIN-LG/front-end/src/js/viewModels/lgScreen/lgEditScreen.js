define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker'
], function(oj, ko, $, app, services, commonhelper) {
    function lgEditScreenContentViewModel() {
        var self = this;
        self.lgEditScreenLbl = 'Edit LG Screen';
        self.label = ko.observable();
        self.customsModel = {
            lgArr: ko.observableArray([]),
            attachmentArr: ko.observableArray([]),
            attachmentColumns: ko.observableArray(),
            columnArray: ko.observableArray(),
            description: ko.observable(""),
            bankAccountArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgTypeArr: ko.observableArray(),
            lgNumberFromBankVal: ko.observable(),
            supplierVal: ko.observable(),
            contractNoVal: ko.observable(),
            lgTypeVal: ko.observable(),
            lgType: ko.observable(),
            contractAmountVal: ko.observable(),
            contractStartDateVal: ko.observable(),
            contractEndDateVal: ko.observable(),
            letterOfGuarnAmoutVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            regionVal: ko.observable(),
            documentNumber: ko.observable(),
            bankAccountVal: ko.observable(),
            closeToExp: ko.observable(),
            disableAllFields: ko.observable(true),
            addFileDis: ko.observable(false),
            createLg: ko.observable(true),
            disableIncDec: ko.observable(true),
            disableExtend: ko.observable(true),
            disableField: ko.observable(true),
            editVisible: ko.observable(false),
            viewCancelVisible: ko.observable(false),
            attachVisible: ko.observable(false),
            viewSelect: ko.observable(true),
            selected_files: ko.observableArray(),
            fileData: ko.observable(),
            contentData: ko.observable(),
            data: ko.observableArray([]),
            viewVisible: ko.observable(true),
            viewFormatVisible: ko.observable(false),
            viewExpVisible: ko.observable(false),
            contractNumUsed: ko.observableArray([]),
            letterIncVal: ko.observable(),
            letterDecVal: ko.observable(),
            minDateVal: ko.observable(),
        };


        var retrieved = oj.Router.rootInstance.retrieve();
        var payloadAttach = [];
        var payload;
        var date = formatDate(new Date());
        var diff;
        self.dataproviderLgScreen = new oj.ArrayDataProvider(self.customsModel.lgArr, { idAttributes: 'id' });
        self.dataProviderAttachment = new oj.ArrayTableDataSource(self.customsModel.attachmentArr, { idAttributes: 'id' });

        self.getContractNumber = function() {
            app.loading(true);
            services.getGeneric(commonhelper.getLg).then(data => {
                app.loading(false);
                if (data) {
                    var contractNumArr = [...new Set(data.Data.map(el => el.contractNumber))];
                    self.customsModel.contractNumUsed(contractNumArr.map(el => ({ label: el, value: el })));
                    self.customsModel.contractNumUsed(self.customsModel.contractNumUsed().filter(el => el.value != retrieved.contractNumber))
                    self.customsModel.contractNumUsed().forEach(e => {
                        self.customsModel.contractNumberArr(self.customsModel.contractNumberArr().filter(el => el.value != e.value));
                    });
                }

            }, error => {
                app.loading(false);
            });
        };

        self.fileTrue = ko.observable(false);
        self.connected = function() {
            self.getAttachment();
            self.customsModel.documentNumber(retrieved.documentNumber);
            self.customsModel.lgNumberFromBankVal(retrieved.lgNumFromBank);
            self.customsModel.supplierVal(retrieved.supplier);
            self.customsModel.contractNoVal(retrieved.contractNumber);
            self.customsModel.lgTypeVal(retrieved.lgType);
            self.customsModel.contractAmountVal(retrieved.contractAmount);
            self.customsModel.contractStartDateVal(retrieved.contractStartDate);
            self.customsModel.contractEndDateVal(retrieved.contractEndDate);
            self.customsModel.letterOfGuarnAmoutVal(parseInt(retrieved.lgAmount));
            self.customsModel.lgEndDateVal(retrieved.lgEndDate);
            self.customsModel.regionVal(retrieved.region);
            self.customsModel.bankAccountVal(retrieved.bankAccount);
            self.customsModel.description(retrieved.description);
            diff = date_diff_indays(date, self.customsModel.lgEndDateVal());
            self.customsModel.closeToExp(diff);
            if (retrieved.edit) {
                self.customsModel.disableIncDec(false);
                self.customsModel.disableExtend(false);
                self.customsModel.disableField(false);
                self.customsModel.attachVisible(true);
                self.customsModel.viewSelect(false);
                self.customsModel.editVisible(true);
                self.customsModel.viewCancelVisible(true);
            } else if (retrieved.incLg) {
                self.customsModel.disableIncDec(false);
                self.customsModel.disableExtend(true);
                self.customsModel.disableField(true);
                self.customsModel.viewCancelVisible(false);
                self.customsModel.letterIncVal(self.customsModel.letterOfGuarnAmoutVal());
            } else if (retrieved.decLg) {
                self.customsModel.disableIncDec(false);
                self.customsModel.disableExtend(true);
                self.customsModel.disableField(true);
                self.customsModel.viewCancelVisible(false);
                self.customsModel.letterDecVal(self.customsModel.letterOfGuarnAmoutVal());
            } else if (retrieved.extend) {
                self.customsModel.disableIncDec(true);
                self.customsModel.disableExtend(false);
                self.customsModel.disableField(true);
                self.customsModel.minDateVal(self.customsModel.lgEndDateVal())
                self.customsModel.viewFormatVisible(true);
            } else if (retrieved.view) {
                self.lgEditScreenLbl = 'View LG Screen';
                self.customsModel.disableIncDec(true);
                self.customsModel.disableExtend(true);
                self.customsModel.disableField(true);
                self.customsModel.viewVisible(false);
                self.customsModel.attachVisible(true);
                self.customsModel.viewExpVisible(true);
                self.customsModel.viewCancelVisible(false);
            }



            self.customsModel.lgTypeArr.push({ label: 'Bid bond', value: 'Bid bond' }, { label: 'Final', value: 'Final' }, { label: 'Advance', value: 'Advance' });
        };

        self.getAttachment = function() {
            services.getGeneric(commonhelper.getAttachment + retrieved.documentNumber).then(data => {
                if (data.Data.length > 0) {
                    self.fileTrue(true);
                    self.customsModel.attachmentArr(data.Data);
                    self.customsModel.selected_files(data.Data);
                }
            }, error => {});
        }

        ko.computed(_ => {
            if (app.agreementArr()) {
                var agreementValue = app.agreementArr().map(el => ({ label: el.AGREEMENTNUMBER, value: el.AGREEMENTNUMBER }))
                self.customsModel.contractNumberArr(agreementValue);
                self.getContractNumber();
            }
            if (app.bankAccountArr()) {
                var bankAccValue = app.bankAccountArr().map(el => ({ label: el.bankAccount, value: el.bankAccount }))
                self.customsModel.bankAccountArr(bankAccValue);
            }
        });

        self.lgselectListener = function(event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.lgArr()[[currentRow['rowIndex']]];
            }
        };

        function download(filename, text) {
            var link = document.createElement("a");
            link.download = filename;
            link.href = text;
            link.click();
        }
        self.lgAttachmentselectListener = function(event) {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowAttachSelected = self.customsModel.attachmentArr()[[currentRow['rowIndex']]];
            }
            if (retrieved.view)
                download(self.customsModel.rowAttachSelected.title, self.customsModel.rowAttachSelected.content);
        };
        self.DownloadFileFn = function(event) {
            self.downLink(true);
            if (self.downLink()) {
                download(self.customsModel.rowAttachSelected.title, self.customsModel.rowAttachSelected.content);
                self.downLink(false);
            }
        };
        self.addFileSelectListener = function(event) {
            var x = {};
            self.customsModel.fileData(event.detail.files);
            var reader = new FileReader();
            reader.onload = function(evt) {
                self.customsModel.contentData(evt.target.result);
                self.customsModel.attTitle(self.customsModel.fileData()[0].name)
                self.customsModel.attName(self.customsModel.fileData()[0].name.split('.')[0])
                self.customsModel.attdocNum(self.customsModel.documentNumber())
                x = {
                    "title": self.customsModel.attTitle(),
                    "name": self.customsModel.attName(),
                    "docNum": self.customsModel.attdocNum(),
                    "content": self.customsModel.contentData(),
                    "id": 0
                };
                ko.computed(_ => {
                    if (self.customsModel.contentData()) {
                        if (x.name)
                            self.payloadAttach().push(x);
                        x = {};
                        self.customsModel.attachmentArr(self.payloadAttach());
                    }
                });
            };
            reader.readAsDataURL(self.customsModel.fileData()[0]);
            //            self.customsModel.addFileDis(true);
        };

        self.uploadContarctAttachment = function(event) {
            app.loading(true);
            services.postGeneric(commonhelper.insertAttachment, self.payloadAttach()).then(data => {
                app.loading(false);
            }, error => {
                app.loading(false);
            });
        };
        self.viewContarctAttachment = function(event) {
            document.querySelector("#viewAttachment").open();
        };

        self.removePickerBtnAction = function(event) {
            if (document.querySelector("oj-file-picker input"))
                document.querySelector("oj-file-picker input").value = null;

            self.customsModel.selected_files([]);
            self.customsModel.addFileDis(false);
        };

        self.deleteAttach = function() {
            var selected = self.customsModel.rowAttachSelected;
            app.loading(true);
            services.getGeneric(commonhelper.deleteAttachment + selected.id).then(data => {
                app.loading(false);
                document.querySelector("#viewAttachment").close();
            }, error => {
                app.loading(false);
            });
        };

        self.lgTypeListener = function(event) {
            self.customsModel.lgType(event['detail'].value);
            if (self.customsModel.contractNoVal()) {
                self.customsModel.letterOfGuarnAmoutVal(0);

                if (self.customsModel.contractAmountVal() != "No data found") {
                    if (self.customsModel.lgType() == "Bid bond")
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal() * 1 / 100);

                    if (self.customsModel.lgType() == "Final")
                        self.customsModel.letterOfGuarnAmoutVal(self.customsModel.contractAmountVal() * 5 / 100);
                    //                    if (self.customsModel.lgType() == "Advance")
                    //                        self.customsModel.disableIncDec(false);
                    //                    else
                    //                        self.customsModel.disableIncDec(true);

                }
            }
        };

        self.addHistory = function() {
            if (retrieved.incLg || retrieved.decLg) {
                services.postGeneric(commonhelper.addLgHistoryIncDec, payload).then(data => {
                    app.loading(false);
                    if (data) {
                        oj.Router.rootInstance.go('lgScreen');
                    }
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                });
            } else if (retrieved.extend) {
                services.postGeneric(commonhelper.addLgHistoryExtend, payload).then(data => {
                    app.loading(false);
                    if (data) {
                        oj.Router.rootInstance.go('lgScreen');
                    }
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                });
            } else {
                services.postGeneric(commonhelper.addLgHistoryRelease, payload).then(data => {
                    app.loading(false);
                    if (data) {
                        oj.Router.rootInstance.go('lgScreen');
                    }
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                });
            }

        };
        //        self.notifChang=ko.observable(false);
        //        ko.computed(()=>{
        //            if(self.notifChang()){
        //                app.getALlNotification();
        //                self.notifChang(false);
        //            }
        //        })
        self.submitBtn = function() {
            app.loading(true);
            if (retrieved.edit) {
                services.postGeneric(commonhelper.editLg, payload).then(data => {
                    app.loading(false);
                    if (data) {
                        services.postGeneric(commonhelper.updateStatus, payload[0]);
                        if (!self.fileTrue()) {
                            self.uploadContarctAttachment();
                        } else {
                            for (var i in self.customsModel.selected_files()) {
                                services.getGeneric(commonhelper.deleteAttachment + self.customsModel.selected_files()[i].id);
                            }
                            self.uploadContarctAttachment();
                        }
                        app.messagesDataProvider.push(app.createMessage('confirmation', 'Data has been updated '));
                        oj.Router.rootInstance.go('lgScreen');
                    }
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been updated '));
                });
            } else {
                self.addHistory();
            }
        };

        self.updateBtn = function() {
            self.customsModel.contractNoVal(retrieved.contractNumber);
            var lgStatus = retrieved.lgStatus;
            var approvalStatus = "Pending";
            var operation;
            operation = "None";
            if (lgStatus == "Rejected")
                operation = "Undo";
            if (retrieved.edit) {
                if (app.personNumber() !== app.personNumberAdmin()) {
                    //                    self.notifChang(true);
                    lgStatus = "Pending Approval";
                    approvalStatus = "Pending";
                } else {
                    lgStatus = "In Process";
                    approvalStatus = "APPROVED";
                }
            }
            if (retrieved.incLg)
                operation = "Increase";
            if (retrieved.decLg)
                operation = "Decrease";
            if (retrieved.extend)
                operation = "Extend";
            self.customsModel.createLg(false);
            payload = [{
                id: retrieved.id,
                documentNumber: self.customsModel.documentNumber(),
                contractNumber: self.customsModel.contractNoVal(),
                contractAmount: self.customsModel.contractAmountVal(),
                supplier: self.customsModel.supplierVal(),
                contractStartDate: self.customsModel.contractStartDateVal(),
                contractEndDate: self.customsModel.contractEndDateVal(),
                lgNumFromBank: self.customsModel.lgNumberFromBankVal(),
                lgType: self.customsModel.lgTypeVal(),
                lgEndDate: self.customsModel.lgEndDateVal(),
                region: self.customsModel.regionVal(),
                lgAmount: self.customsModel.letterOfGuarnAmoutVal().toString(),
                bankAccount: self.customsModel.bankAccountVal(),
                description: self.customsModel.description(),
                lgStatus: lgStatus,
                createdBy: retrieved.createdBy,
                operation: operation,
                approvalStatus: approvalStatus
            }];
            payload[0].closeToExp = self.customsModel.closeToExp();
            self.customsModel.lgArr(payload);
            if (lgStatus == "Rejected")
                self.addHistory();
        };

        self.backBtn = function() {
            oj.Router.rootInstance.go('lgScreen');
        };

        self.cancelviewAttDial = function() {
            document.querySelector("#viewAttachment").close();
        };

        self.cancelStatusBtn = function() {
            document.querySelector("#confirmCancel").open();
        };

        self.yes = function() {
            app.loading(true);
            var payload = {
                "id": retrieved.id,
                "approvalStatus": "CANCEL",
                "documentNumber": self.customsModel.documentNumber(),
                "lgStatus": "Cancelled"
            };
            services.postGeneric(commonhelper.updateStatus, payload).then(data => {
                if (data) {
                    oj.Router.rootInstance.go('lgScreen');
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'LG has been Canceled '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'LG has not been Canceled '));
            });
        }

        self.no = function() {
            document.querySelector("#confirmCancel").close();
        }

        self.LgFormatBtn = function() {
            retrieved.actualEndDate = self.customsModel.lgEndDateVal()
            var splitDate = addDays(self.customsModel.lgEndDateVal(), 365)
            retrieved.lgPlusDate = formatDate(splitDate)
            services.postGeneric(commonhelper.lgFormat, retrieved).then(data => {
                var x = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,";
                var xx = x + data.Data;
                download("نموذج تجديد ضمان.docx", xx)
                app.messagesDataProvider.push(app.createMessage('confirmation', 'LG Extend Format Download Successfully'));
            }, error => {

            })
        }

        function download(filename, text) {
            var link = document.createElement("a");
            link.download = filename;
            link.href = text;
            link.click();
        }

        function addDays(date, days) {
            const date2 = new Date(date);
            const copy = new Date(Number(date2))
            copy.setDate(date2.getDate() + days)
            return copy
        }

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function(date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                contractStartDateLbl: getTransaltion("lgOperation.contractStartDateLbl"),
                contractEndDateLbl: getTransaltion("lgOperation.contractEndDateLbl"),
                letterOfGuarnAmoutLbl: getTransaltion("lgOperation.letterOfGuarnAmoutLbl"),
                contractAmountLbl: getTransaltion("lgOperation.contractAmountLbl"),
                supplierLbl: getTransaltion("lgOperation.supplierLbl"),
                regionLbl: getTransaltion("lgOperation.regionLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                documentNumberLbl: getTransaltion("lgScreen.documentNumberLbl"),
                attachmentLbl: getTransaltion("lgOperation.attachmentLbl"),
                uploadLbl: getTransaltion("common.uploadLbl"),
                descriptionLbl: getTransaltion("lgOperation.descriptionLbl"),
                closeToExpLbl: getTransaltion("common.closeToExpLbl"),
                submitLbl: getTransaltion("common.submitLbl"),
                backLbl: getTransaltion("common.backLbl"),
                addFileLbl: getTransaltion("common.addFileLbl"),
                viewAttachmentLbl: getTransaltion("common.viewAttachmentLbl"),
                titleLbl: getTransaltion("lgOperation.titleLbl"),
                nameLbl: getTransaltion("lgOperation.nameLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                editLbl: getTransaltion("common.editLbl"),
                downloadLbl: getTransaltion("common.downloadLbl"),
                deleteLbl: getTransaltion("common.deleteLbl"),
                formatLbl: getTransaltion("lgOperation.formatLbl"),
                confirmCancelLbl: getTransaltion("lgOperation.confirmCancelLbl"),
                confirmCancelTextLbl: getTransaltion("lgOperation.confirmCancelTextLbl"),
                yesLbl: getTransaltion("common.yesLbl"),
                noLbl: getTransaltion("common.noLbl"),


            });
            self.customsModel.columnArray([{
                    "headerText": self.label().supplierLbl,
                    "field": "supplier"
                },
                {
                    "headerText": self.label().documentNumberLbl,
                    "field": "documentNumber"
                },
                {
                    "headerText": self.label().bankAccountLbl,
                    "field": "bankAccount"
                },
                {
                    "headerText": self.label().regionLbl,
                    "field": "region"
                },
                {
                    "headerText": self.label().contractNoLbl,
                    "field": "contractNumber"
                },
                {
                    "headerText": self.label().lgNumberFromBankLbl,
                    "field": "lgNumFromBank"
                },
                {
                    "headerText": self.label().letterOfGuarnAmoutLbl,
                    "field": "lgAmount"
                },
                {
                    "headerText": self.label().contractAmountLbl,
                    "field": "contractAmount"
                },
                {
                    "headerText": self.label().lgTypeLbl,
                    "field": "lgType"
                },
                {
                    "headerText": self.label().contractStartDateLbl,
                    "field": "contractStartDate"
                },
                {
                    "headerText": self.label().contractEndDateLbl,
                    "field": "contractEndDate"
                },
                {
                    "headerText": self.label().lgEndDateLbl,
                    "field": "lgEndDate"
                },
                {
                    "headerText": self.label().closeToExpLbl,
                    "field": "closeToExp"
                }
            ]);
            self.customsModel.attachmentColumns([{
                    "headerText": self.label().titleLbl,
                    "field": "title"
                },
                {
                    "headerText": self.label().nameLbl,
                    "field": "name"
                },
                {
                    headerText: self.label().download,
                    template: 'downTemplate'
                },
                {
                    headerText: self.label().deleteLbl,
                    template: 'cellTemplate'
                }
            ]);
        }
        initTransaltion();
    }

    return lgEditScreenContentViewModel;
});