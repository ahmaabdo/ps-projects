define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker'
], function (oj, ko, $, app, services, commonhelper) {
    function confiscationScreenContentViewModel() {
        var self = this;
        self.confisScreenLbl = 'Confiscation Screen';

        self.customsModel = {
            bankAccountVal: ko.observable(),
            bankAccountArr: ko.observableArray(),
            letterOfGuarnAmoutVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            disableAdvance: ko.observable(true),
            lgNumber: ko.observable(),
            bankAccComb: ko.observable(),
            contractStartDate: ko.observable(),
        };

        self.label = ko.observable();
        var retrieved = oj.Router.rootInstance.retrieve();

        ko.computed(_ => {
            if (app.bankAccountArr()) {
                var bankAccValue = app.bankAccountArr().map(el => ({ label: el.bankAccountName, value: el.bankAccountName }));
                self.customsModel.bankAccountArr(bankAccValue);
            }
        });

        self.connected = function () {
            if (retrieved.confis) {
                self.customsModel.letterOfGuarnAmoutVal(parseInt(retrieved.lgAmount));
                self.customsModel.lgNumber(retrieved.lgNumFromBank)
                self.customsModel.contractStartDate(retrieved.contractStartDate)
                // self.customsModel.lgEndDateVal(retrieved.lgEndDate);
            }
        };

        self.bankAccvalueChange = function()
        {
            self.customsModel.bankAccComb(app.bankAccountArr().find(el => el.bankAccountName == self.customsModel.bankAccountVal()).cashAccountComb)
        }

        // "ExternalTransactionId": 100000038260021,
        // "TransactionId": 102502,
        self.lgStatusUpdate = function()
        {
            var payload = [{
                id: retrieved.id,
                documentNumber: retrieved.documentNumber,
                contractNumber: retrieved.contractNumber,
                contractAmount: retrieved.contractAmount,
                supplier: retrieved.supplier,
                contractStartDate: retrieved.contractStartDate,
                contractEndDate: retrieved.contractEndDate,
                lgNumFromBank: retrieved.lgNumFromBank,
                lgType: retrieved.lgType,
                lgEndDate: retrieved.lgEndDate,
                region: retrieved.region,
                lgAmount: retrieved.lgAmount,
                bankAccount: retrieved.bankAccount,
                description: retrieved.description,
                lgStatus: "Confiscated",
                approvalStatus:"APPROVED",
                operation:"Confiscated",
                createdBy: retrieved.createdBy
            }]
    
            services.postGeneric(commonhelper.addLgHistoryRelease,payload).then(data => {
                app.loading(false);
                },error =>{
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
            });
        }
            
        self.confiscationBtn = function () {
            document.querySelector("#confirmConfis").open();
        };

        self.backBtn = function () {
            oj.Router.rootInstance.go('lgScreen');
        };
        self.yes = function () {
            var payload = {
                "AccountingFlag": true,
                "Amount": self.customsModel.letterOfGuarnAmoutVal(),
                "CurrencyCode": "SAR",
                "Description": null, 
                "ReferenceText": self.customsModel.lgNumber(), // lg number
                "Source": "ORA_BAT",
                "TransactionDate": self.customsModel.contractStartDate(),
                "TransactionType": "LG",
                "ValueDate": null,
                "AssetAccountCombination": self.customsModel.bankAccComb(), 
                "OffsetAccountCombination": "01-7110102-00000-0000-000-0000",
                "BusinessUnitName": "TTC BU",
                "LegalEntityName": "TTC BU",
                "BankAccountName": self.customsModel.bankAccountVal(),
                "BankConversionRate": null,
                "BankConversionRateType": null,
                "TransferId": 300016704710
            }

            services.postGeneric(commonhelper.cashTransaction, payload).then(data => {
                self.lgStatusUpdate()
                app.messagesDataProvider.push(app.createMessage('confirmation', 'Cash Transaction Created Successfuly '));
                oj.Router.rootInstance.go("lgScreen")
            }, error => {

            });
        };
        self.no = function () {
            document.querySelector("#confirmConfis").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                lgEndDateLbl: getTransaltion("lgScreen.date"), 
                lgTransactionDateLbl: getTransaltion("lgScreen.lgTransactionDateLbl"),
                confiscationTextLbl: getTransaltion("common.confirmConfisText"),
                yesLbl:getTransaltion("common.yesLbl"),
                noLbl:getTransaltion("common.noLbl"),
                confirmConfis:getTransaltion("common.confirmConfis"),
                letterOfGuarnAmoutLbl: getTransaltion("lgOperation.letterOfGuarnAmoutLbl"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl"),
                backLbl: getTransaltion("common.backLbl"),
                confiscationLbl: getTransaltion("common.confiscationLbl")
            });

        }
        initTransaltion();
    }

    return confiscationScreenContentViewModel;
});
