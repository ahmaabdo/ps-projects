define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper','ojs/ojselectcombobox','ojs/ojarraydataprovider',
'ojs/ojknockout', 'ojs/ojlabelvalue','ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojfilepicker'
], function (oj, ko, $, app, services, commonhelper) {
    function notificationScreenContentViewModel() {
        var self = this;
        self.notifScreenLbl = 'Notification Screen';
        self.notifTableLbl = 'Pending Requests Table';
        self.expireTableLbl = 'LG Close To Expire Date';
        self.label = ko.observable();
        self.customsModel = {
            notifArr: ko.observableArray([]),
            lgExpireArr:ko.observableArray([]),
            columnArray: ko.observableArray(),
            suppilerNameArr: ko.observableArray(),
            bankAccountArr: ko.observableArray(),
            lgNumberFromBankArr: ko.observableArray(),
            contractNumberArr: ko.observableArray(),
            lgStatusArr: ko.observableArray(),
            suppilerNameArr1: ko.observableArray(),
            bankAccountArr1: ko.observableArray(),
            lgNumberFromBankArr1: ko.observableArray(),
            contractNumberArr1: ko.observableArray(),
            lgStatusArr1: ko.observableArray(),
            rowSelected:ko.observableArray(),
            lgNumberFromBankVal: ko.observable(),
            suppilerNameVal: ko.observable(),
            contractNoVal: ko.observable(),
            bankAccountVal: ko.observable(),
            lgStatusVal: ko.observable(),
            lgEndDateVal: ko.observable(),
            documentNumber: ko.observable(),
            disableAllFields: ko.observable(true),
            resetDis:ko.observable(false),
            resetExpireDis:ko.observable(false),
            notifSelected:ko.observable(true),
            notifExpireSelected:ko.observable(true),
            data:ko.observableArray([]),
            data2:ko.observableArray([]),
            expireData:ko.observableArray([]),
            minDateVal:ko.observable()
        };

        var payload;
        var button;
        self.dataproviderNotifScreen = ko.observable();
        self.dataProviderLgCloseToExpire = ko.observable();
        self.username=ko.observable();
        self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, {idAttributes: 'documentNumber'}));
        self.dataProviderLgCloseToExpire (new oj.ArrayDataProvider(self.customsModel.lgExpireArr, {idAttributes: 'id'}));
       
        self.connected = function (){

        };

        self.getData=function(){
            app.loading(true);
            services.getGeneric(commonhelper.getNotification+app.personNumberAdmin()).then(data =>{
                app.loading(false);
                self.customsModel.notifArr([]);
                self.customsModel.lgExpireArr([]);
                var srNo = 0 ;
                data.Data.forEach(el => el.srNo = ++srNo)
                var date=formatDate(new Date());
                var diff;
                if(data.Data){
                    for (var i in data.Data) {
                        diff = date_diff_indays(date, data.Data[i].lgEndDate);
                        if (app.personNumber() != app.personNumberAdmin()) {
                            if(data.Data[i].approvalStatus == 'REJECT' &&data.Data[i].createdBy ==app.personNumber()){
                                self.customsModel.notifArr.push(data.Data[i]);
                                self.customsModel.data2.push(data.Data[i]);
                            self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, {idAttributes: 'documentNumber'}));
                            }
                        } else {
                            if (diff <= 30 && data.Data[i].approvalStatus !=='REJECT' &&( data.Data[i].lgStatus =='In Process'||data.Data[i].lgStatus =='Pending Approval')){
                                self.customsModel.lgExpireArr.push(data.Data[i]);
                                self.customsModel.expireData.push(data.Data[i]);
                                self.dataProviderLgCloseToExpire (new oj.ArrayDataProvider(self.customsModel.lgExpireArr, {idAttributes: 'id'}));
                            }
                            if(data.Data[i].approvalStatus =='Pending') {
                                self.customsModel.data.push(data.Data[i]);
                                self.customsModel.notifArr.push(data.Data[i]);
                                self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, {idAttributes: 'documentNumber'}));
                            }
                        }
                    }
                 
                    var suppArr =[...new Set(self.customsModel.notifArr().map(el => el.supplier))];
                    var lgNumArr =[...new Set(self.customsModel.notifArr().map(el => el.lgNumFromBank))];
                    var contractNumArr =[...new Set(self.customsModel.notifArr().map(el => el.contractNumber))];
                    var lgStatusArr =[...new Set(self.customsModel.notifArr().map(el => el.lgStatus))];
                    var suppArr1 =[...new Set(self.customsModel.lgExpireArr().map(el => el.supplier))];
                    var lgNumArr1 =[...new Set(self.customsModel.lgExpireArr().map(el => el.lgNumFromBank))];
                    var contractNumArr1 =[...new Set(self.customsModel.lgExpireArr().map(el => el.contractNumber))];
                    var lgStatusArr1 =[...new Set(self.customsModel.lgExpireArr().map(el => el.lgStatus))];
                    self.customsModel.suppilerNameArr(suppArr.map(el => ({label: el, value: el})));
                    self.customsModel.lgNumberFromBankArr(lgNumArr.map(el => ({label: el, value: el})));
                    self.customsModel.contractNumberArr(contractNumArr.map(el => ({label: el, value: el})));
                    self.customsModel.lgStatusArr(lgStatusArr.map(el => ({label: el, value: el})));
                    self.customsModel.suppilerNameArr1(suppArr1.map(el => ({label: el, value: el})));
                    self.customsModel.lgNumberFromBankArr1(lgNumArr1.map(el => ({label: el, value: el})));
                    self.customsModel.contractNumberArr1(contractNumArr1.map(el => ({label: el, value: el})));
                    self.customsModel.lgStatusArr1(lgStatusArr1.map(el => ({label: el, value: el})));
                }   
                    
            },error=> {
                app.loading(false);
            });
        };

        ko.computed(()=>{
            if(app.username()){
                self.username(app.username());
                if(app.username()!='abdullah.oweidi@appspro-me.com'){
                    self.notifTableLbl = 'Rejected Requests Table';
                }
            self.getData();
            }
        });

        ko.computed(_ =>{
            if(app.bankAccountArr())
            {
                var bankAccValue = app.bankAccountArr().map(el => ({label:el.bankAccount,value:el.bankAccount}));
                self.customsModel.bankAccountArr(bankAccValue);
            }
        });

        self.notifselectListener = function (event)
        {
            var data = event.detail;
            if (!data)
                return;

            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.notifArr()[[currentRow['rowIndex']]];
                self.customsModel.notifSelected(false);
            }
        };

        self.lgCloseToExpireSelectionListener = function (event)
        {
            var data = event.detail;
            if (!data)
                return;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.lgExpireArr()[[currentRow['rowIndex']]];
                if(self.customsModel.rowSelected.lgStatus=="In Process" ){
                    self.customsModel.notifExpireSelected(false);
                }else{
                    self.customsModel.notifExpireSelected(true);
                }
            }
        };

        self.searchBtn = function ()
        {
            cond = function (el) {
                var res = (
                        (self.customsModel.documentNumber() ? el.documentNumber == self.customsModel.documentNumber() : true) &&
                        (self.customsModel.lgNumberFromBankVal() ? el.lgNumFromBank == self.customsModel.lgNumberFromBankVal() : true) &&
                        (self.customsModel.suppilerNameVal() ? el.supplier == self.customsModel.suppilerNameVal() : true) &&
                        (self.customsModel.contractNoVal() ? el.contractNumber == self.customsModel.contractNoVal() : true) &&
                        (self.customsModel.lgStatusVal() ? el.lgStatus == self.customsModel.lgStatusVal() : true)
                        );
                return res;
            };
            var find;
            if(app.personNumber()==app.personNumberAdmin())
                find = self.customsModel.data().filter(el => cond(el));
            else
                find = self.customsModel.data2().filter(el => cond(el));
            self.customsModel.notifArr(find);
            self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, {idAttributes: 'documentNumber'}));
        };

        self.searchExpireBtn = function ()
        {
            cond = function (el) {
                var res = (
                        (self.customsModel.documentNumber() ? el.documentNumber == self.customsModel.documentNumber() : true) &&
                        (self.customsModel.lgNumberFromBankVal() ? el.lgNumFromBank == self.customsModel.lgNumberFromBankVal() : true) &&
                        (self.customsModel.suppilerNameVal() ? el.supplier == self.customsModel.suppilerNameVal() : true) &&
                        (self.customsModel.contractNoVal() ? el.contractNumber == self.customsModel.contractNoVal() : true) &&
                        (self.customsModel.lgStatusVal() ? el.lgStatus == self.customsModel.lgStatusVal() : true)
                        );
                return res;
            };
            var findExpire = self.customsModel.expireData().filter(el => cond(el));
            self.customsModel.lgExpireArr(findExpire);
            self.dataProviderLgCloseToExpire(new oj.ArrayDataProvider(self.customsModel.lgExpireArr, {idAttributes: 'documentNumber'}));
        };

        self.resetBtn = function ()
        {
            self.customsModel.resetDis(true);
            setTimeout(function(){ 
            self.customsModel.resetDis(false);
            if(app.personNumber()==app.personNumberAdmin())
                self.customsModel.notifArr(self.customsModel.data());
            else
                self.customsModel.notifArr(self.customsModel.data2());
            self.dataproviderNotifScreen(new oj.ArrayDataProvider(self.customsModel.notifArr, {idAttributes: 'documentNumber'}));
            }, 1000);
            self.customsModel.documentNumber('');
            self.customsModel.lgNumberFromBankVal('');
            self.customsModel.suppilerNameVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgStatusVal('');
        };

        self.resetExpireBtn = function ()
        {
            self.customsModel.resetExpireDis(true);
            setTimeout(function(){ 
            self.customsModel.resetExpireDis(false);
            self.customsModel.lgExpireArr(self.customsModel.expireData());
            }, 1000);
            self.customsModel.documentNumber('');
            self.customsModel.lgNumberFromBankVal('');
            self.customsModel.suppilerNameVal('');
            self.customsModel.contractNoVal('');
            self.customsModel.lgStatusVal('');
        };

        
        self.approveRej=ko.observable(false);
        ko.computed(()=>{
            if (self.approveRej()){
                app.getALlNotification();
                self.approveRej(false);
            }
        });
        self.rejectBtn = function ()
        {
            var selected = self.customsModel.rowSelected;
            payload = {
                "id":selected.id,
                "approvalStatus":"REJECT",
                "documentNumber":selected.documentNumber,
                "lgStatus":"Rejected"
            };
            services.postGeneric(commonhelper.updateStatus, payload).then(data => {
                if (data) {
                    app.loading(false);
                    self.getData();
                    self.approveRej(true);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

    
        self.approveBtn = function () 
        {
            var selected = self.customsModel.rowSelected;
            payload = {
                "id":selected.id,
                "approvalStatus":"APPROVED",
                "documentNumber":selected.documentNumber,
                "lgStatus":"In Process"
            };
            services.postGeneric(commonhelper.updateStatus, payload).then(data => {
                if (data) {
                    app.loading(false);
                    self.getData();
                    self.approveRej(true);
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

        self.addHistory = function (payload)
        {
            app.loading(true);
            if (button == "yes") {
                services.postGeneric(commonhelper.addLgHistoryRelease, payload).then(data => {
                    app.loading(false);
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                });
            } else {
                payload[0].lgEndDate = self.customsModel.lgEndDateVal();
                services.postGeneric(commonhelper.addLgHistoryExtend, payload).then(data => {
                    app.loading(false);
                    self.getData();
                }, error => {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', 'Data has not been saved '));
                });
            }
        };
        
        self.yes = function ()
        {
            button="yes";
            document.querySelector("#confirmRelease").close();
            var selected = self.customsModel.rowSelected;
            app.loading(true);
            payload = 
                [{
                    id: selected.id,
                    documentNumber: selected.documentNumber,
                    contractNumber: selected.contractNumber,
                    contractAmount: selected.contractAmount,
                    supplier: selected.supplier,
                    contractStartDate: selected.contractStartDate,
                    contractEndDate: selected.contractEndDate,
                    lgNumFromBank: selected.lgNumFromBank,
                    lgType: selected.lgType,
                    lgEndDate: selected.lgEndDate,
                    region: selected.region,
                    lgAmount: selected.amount,
                    bankAccount: selected.bankAccount,
                    description: selected.description,
                    lgStatus: "Released",
                    approvalStatus:"APPROVED",
                    operation:"Realease",
                    createdBy: selected.createdBy
                }];

            services.postGeneric(commonhelper.updateStatus, payload[0]).then(data => {
                if (data) {
                    self.addHistory(payload);
                    app.loading(false);
                    self.getData();
                    app.messagesDataProvider.push(app.createMessage('confirmation', 'Status has been updated '));
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', 'Status has not been updated '));
            });
        };

      
        self.extendBtn = function () {
            var selected=self.customsModel.rowSelected;
            document.querySelector("#confirmExtend").open();
            self.customsModel.documentNumber(selected.documentNumber);
            self.customsModel.minDateVal(selected.lgEndDate);
            self.customsModel.lgEndDateVal(selected.lgEndDate);
        };

        self.yesExtend = function () {
            button = "yesExtend";
            var selected=self.customsModel.rowSelected;
            payload = 
            [{
                id:selected.id,
                documentNumber:selected.documentNumber,
                contractNumber:selected.contractNumber,
                contractAmount:selected.contractAmount,
                supplier:selected.supplier,
                contractStartDate:selected.contractStartDate,
                contractEndDate:selected.contractEndDate,
                lgNumFromBank:selected.lgNumFromBank,
                lgType:selected.lgType,
                lgEndDate:self.customsModel.lgEndDateVal(),
                region:selected.region,
                lgAmount:selected.amount,
                description:selected.description,
                lgStatus:"In Process",
                operation:"Extend",
                createdBy:selected.createdBy
            }];

            self.addHistory(payload);
            document.querySelector("#confirmExtend").close();
            self.getData();
        };

        function formatDate(date) {
            var month = '' + (date.getMonth() + 1), day = '' + date.getDate(), year = date.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;
            return [year, month, day].join('-');
        };

        var date_diff_indays = function (date1, date2) {
            var dt1 = new Date(date1);
            var dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        };

        self.no = function ()
        {
            document.querySelector("#confirmRelease").close();
        };

        self.releaseBtn = function () {
            document.querySelector("#confirmRelease").open();
        };

        self.noExtend = function () {
            document.querySelector("#confirmExtend").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                yesLbl:getTransaltion("common.yesLbl"),
                noLbl:getTransaltion("common.noLbl"),
                confirmText:getTransaltion("common.confirmText"),
                confirmReleaseLbl:getTransaltion("lgScreen.confirmReleaseLbl"),
                confirmExtendLbl:getTransaltion("lgScreen.confirmExtendLbl"),
                releaseLbl: getTransaltion("common.releaseLbl"),
                extendLbl: getTransaltion("common.extendLbl"),
                contractNoLbl: getTransaltion("lgScreen.contractNoLbl"),
                suppilerNameLbl: getTransaltion("lgScreen.suppilerNameLbl"),
                lgNumberFromBankLbl: getTransaltion("lgScreen.lgNumberFromBankLbl"),
                lgStatusLbl: getTransaltion("lgScreen.lgStatusLbl"),
                lgTypeLbl: getTransaltion("lgScreen.lgTypeLbl"),
                bankAmountLbl: getTransaltion("lgScreen.bankAmountLbl"),
                amountLbl: getTransaltion("lgScreen.amountLbl"),
                lgEndDateLbl: getTransaltion("lgScreen.lgEndDateLbl"),
                documentNumberLbl:getTransaltion("lgScreen.documentNumberLbl"),
                status: getTransaltion("lgScreen.status"),
                searchLbl: getTransaltion("common.searchLbl"),
                approveLbl: getTransaltion("common.approveLbl"),
                rejectLbl: getTransaltion("common.rejectLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                approvalStatus:getTransaltion("lgScreen.approvalStatus"),
                bankAccountLbl: getTransaltion("lgOperation.bankAccountLbl")
            });
            self.customsModel.columnArray([
                // {
                //     "headerText": "NO.", "field": "srNo"
                // },
                {
                    "headerText": self.label().documentNumberLbl, "field": "documentNumber"
                },
                {
                    "headerText": self.label().lgNumberFromBankLbl, "field": "lgNumFromBank"
                },
                {
                    "headerText": self.label().contractNoLbl, "field": "contractNumber"
                },
                {
                    "headerText": self.label().status, "field": "lgStatus"
                },
                {
                    "headerText": self.label().suppilerNameLbl, "field": "supplier"
                },
                {
                    "headerText": self.label().lgTypeLbl, "field": "lgType"
                },
                {
                    "headerText": self.label().bankAmountLbl, "field": "contractAmount"
                },
                {
                    "headerText": self.label().amountLbl, "field": "amount"
                },
                {
                    "headerText": self.label().lgEndDateLbl, "field": "lgEndDate"
                },
                {
                    "headerText": self.label().status, "field": "lgStatus"
                }
            ]);
        }
        initTransaltion();
    }
    
    return notificationScreenContentViewModel;
});
