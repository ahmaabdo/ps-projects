function showEmplDetails(oj,ko, services, app, mainData, commonhelper) {


    var rootViewModel = app;
    var xx = "https://hop1.hha.com.sa/Fusion-SSHR-PROD/?username=PAASUser&host=https://ejfz-test.fa.em2.oraclecloud.com&lang=en&jwt=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6IjdjR0ZsSEMzVEtBZUZoNllLM1l4WWVPUHQ4QSIsImtpZCI6InRydXN0c2VydmljZSJ9.eyJleHAiOjE1NzcxOTE2NzIsInN1YiI6IlBBQVNVc2VyIiwiaXNzIjoid3d3Lm9yYWNsZS5jb20iLCJwcm4iOiJQQUFTVXNlciIsImlhdCI6MTU3NzE5MTA3Mn0.IYHb6GfappmuiDdaUnxcxOOKbnwd4spMl6av-T4LFDI6KWYKNoCktDYf7DYbrxNY40h864YjhHQM8Qv805XFiGBV9D7yyXZpYjVX3pIZycjpCq4i6qCDx6am-SjnbUtnQ29z5OzyurrUOA6l79lEblvwgbPKdT6jZsA-QOs8zZ0H0cyfqGKvMPQXrVZ2lNvMIIL8tfteaN4NxDBP2WhypMJkOjAWLIFigdhk-DwsMOv9lZhs4hn9THcrF9DrX81zhQBOrF3AZrhQRU0_eaz7VSREREMEZ-NuXBTeXMaql86o_cnL4waJ7jJFs1JfhS-G8XZMvpeTi5MLp1ZnqRMcxA"
    var url = new URL(window.location.href); //xx
    var username;
    var hosturl;
    var jwt;
    var jwtinurl;
    var deviceIp;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var deviceAndBrowserDetails = navigator.userAgent; //browser and device details 
    var LogindateTime;
    var isJWTNotValid = false;


    if (url.searchParams.get("jwt")) {
        if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
            hosturl = url.searchParams.get("host");
            rootViewModel.hostUrl(hosturl);
        }

        if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
            jwt = url.searchParams.get("jwt");

            rootViewModel.jwt(jwt);
        }   

    

    if (rootViewModel.userName() == null || rootViewModel.userName().length == 0) {
        try {
            jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
            username = jwtJSON.sub;
        
            isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;

            var isHasJwt = localStorage.jwt && localStorage.jwt == jwt;
            if ( isJWTExpred) {//used before
                isJWTNotValid = true;
                
            } else {
                rootViewModel.userName(username);

            }

        } catch (e) { };
    }

    if (url.searchParams.get("lang") && !isJWTNotValid) {

        var sessionLang = url.searchParams.get("lang");
        if (sessionLang == 'ar') {
            rootViewModel.setLocale('ar');
            localStorage.setItem("selectedLanguage", "ar");
        } else if (sessionLang == 'en') {
            // rootViewModel.setLocale("en-US");
            localStorage.setItem("selectedLanguage", "en-US");
        }
    }

    if (!isJWTNotValid) {   //its mean is JWT is vaild
        $(".apLoginBtn").addClass("loading");
        // rootViewModel.disOnLogin(true);
        LogindateTime = date + ' ' + time;
        // rootViewModel.loginDateFromSass(LogindateTime);
        var payloadLoginHistory = {};
        $.getJSON("https://api.ipify.org/?format=json", function (e) {
            deviceIp = e.ip;
            payloadLoginHistory = {
                "personNumber": username,
                "loginDate": LogindateTime,
                "browserAndDeviceDetails": deviceAndBrowserDetails,
                "deviceIp": deviceIp
            };
            sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));
            localStorage.jwt = jwt;//save jwt into local storage to be checked next time
         });
        rootViewModel.jwtinurl(true)

    }

    else { //if jwt token not vaild 
         }

    }
    else { //if url not have jwt
        
    }

}