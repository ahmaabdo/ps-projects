package com.appspro.tbcFIN.dao;

import biPReports.RestHelper;

import com.appspro.tbcFIN.bean.InvoicesBean;


import com.sun.org.glassfish.gmbal.Description;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


public class InvoicesDAO  extends RestHelper{
  
   RestHelper rs = new RestHelper();
   
   public JSONArray allInvoices () throws MalformedURLException,IOException {
       
       boolean hasMore = false;
       JSONArray responseArray = new JSONArray();
       int offset = 0;
        do {
           JSONObject obj = new JSONObject();
           String restApi = getInstanceUrl()+getGetAllInvoices()+"?limit=400&offset="+offset+"&expand=invoiceLines.invoiceLineDff";           
           obj = rs.httpGet(restApi);
           
           JSONArray arr2 = obj.getJSONArray("items");
           
           for (int i = 0; i < arr2.length(); i++) {
               JSONObject dff = new JSONObject();
               JSONObject responseObject = new JSONObject();
               JSONObject invoiceObj = arr2.getJSONObject(i);
               JSONArray linesArr = invoiceObj.getJSONArray("invoiceLines");
               if(invoiceObj.getString("ValidationStatus").equals("Validated")) {
                   dff =  linesArr.getJSONObject(0).getJSONArray("invoiceLineDff").getJSONObject(0);
                   responseObject.put("InvoiceNumber",invoiceObj.getString("InvoiceNumber"));
                   responseObject.put("PurchaseOrderNumber",invoiceObj.has("PurchaseOrderNumber") ? invoiceObj.get("PurchaseOrderNumber").toString():null);
                   responseObject.put("InvoiceAmount",invoiceObj.getInt("InvoiceAmount"));
                   responseObject.put("InvoiceDate",invoiceObj.getString("InvoiceDate"));
                   responseObject.put("Description",invoiceObj.has("Description") ? invoiceObj.get("Description").toString():null);
                   responseObject.put("InvoiceType",invoiceObj.getString("InvoiceType"));
                   responseObject.put("Supplier",invoiceObj.getString("Supplier"));   
                   responseObject.put("SupplierSite",invoiceObj.has("SupplierSite") ? invoiceObj.get("SupplierSite").toString():null);
                   responseObject.put("BusinessUnit",invoiceObj.has("BusinessUnit") ? invoiceObj.get("BusinessUnit").toString():null);
                   responseObject.put("invoiceLines",invoiceObj.getJSONArray("invoiceLines"));
                   responseObject.put("projectNumber",dff.has("projectNumber") ? dff.get("projectNumber").toString():null);

                   responseArray.put(responseObject);
                  }
             
               }
           
           hasMore = obj.getBoolean("hasMore");
           offset += 400;
       
       }
        while(hasMore);
       
       return responseArray;
   }
}
