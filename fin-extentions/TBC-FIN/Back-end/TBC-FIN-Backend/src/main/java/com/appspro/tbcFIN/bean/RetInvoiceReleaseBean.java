package com.appspro.tbcFIN.bean;

public class RetInvoiceReleaseBean {
   
   private String id;
   private String invoiveNumber;
   private String invoiceRelease;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setInvoiveNumber(String invoiveNumber) {
        this.invoiveNumber = invoiveNumber;
    }

    public String getInvoiveNumber() {
        return invoiveNumber;
    }

    public void setInvoiceRelease(String invoiceRelease) {
        this.invoiceRelease = invoiceRelease;
    }

    public String getInvoiceRelease() {
        return invoiceRelease;
    }
}
