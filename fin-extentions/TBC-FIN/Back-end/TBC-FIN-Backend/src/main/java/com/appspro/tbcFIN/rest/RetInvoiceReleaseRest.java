package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.bean.RetInvoiceReleaseBean;
import com.appspro.tbcFIN.bean.RetentionInvoiceBean;
import com.appspro.tbcFIN.dao.RetInvoiceReleaseDAO;
import com.appspro.tbcFIN.dao.RetentionInvoiceDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/retention")

public class RetInvoiceReleaseRest {
   
    RetInvoiceReleaseDAO serivces = new RetInvoiceReleaseDAO();
    JSONArray arr = new JSONArray();
    JSONObject resposne = new JSONObject();
    
    @GET
    @Path("/InvRelease/{invNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getInvoiceRelease(@PathParam("invNumber")String invNumber)  {
       
        try {
            arr = serivces.invoiceRelease(invNumber);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    @GET
    @Path("/getAllInvRelease")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllInvoiceRelease()  {
       
        try {
            arr = serivces.getAllInvoiceRelease();
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    @POST
    @Path("insertInvRelease")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertInvRelease(String body) {
            
        RetInvoiceReleaseBean bean = new RetInvoiceReleaseBean();
        String status = "";
        try {
            JSONObject obj = new JSONObject(body);            
            bean.setInvoiveNumber(obj.getString("invoiceNumber"));
            bean.setInvoiceRelease(obj.getString("invoiceRelease"));

            status = serivces.insertInvoiceRelease(bean);
                
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } 
        catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString() ;
    }
    
    @POST
    @Path("updateInvRelease")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateInvRelease(String body) {
            
        RetInvoiceReleaseBean bean = new RetInvoiceReleaseBean();
        String status = "";
        try {
            JSONObject obj = new JSONObject(body);
            System.out.println(obj);
            
            bean.setInvoiveNumber(obj.getString("invoiceNumber"));
            bean.setInvoiceRelease(obj.getString("invoiceRelease"));

            status = serivces.updateInvoiceRelease(bean);
                
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } 
        catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString() ;
    }
    
}
