package com.appspro.tbcFIN.dao;

import biPReports.RestHelper;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class PurchaseOrderDAO extends RestHelper {
    
    public JSONObject purchaseOrder (String POHeaderId) throws MalformedURLException,
                                                               IOException {
        
        String restUrl = getInstanceUrl()+getPurchaseOrderUrl() + "?q=OrderNumber="+POHeaderId+"&onlyData=true";
        RestHelper rs = new RestHelper();
        
        JSONObject obj = rs.httpGet(restUrl);
        JSONArray arr = obj.getJSONArray("items");
        JSONObject res = arr.getJSONObject(0);
        
        JSONObject responseObj = new JSONObject();
        
        responseObj.put("Ordered",res.has("Ordered") ?res.optString("Ordered") : null);
                   
        return responseObj;
    }
}
