package com.appspro.tbcFIN.dao;

import biPReports.RestHelper;

import com.appspro.tbcFIN.bean.BusinessUnitBean;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class BusinessUnitDAO extends RestHelper {
 
    RestHelper rs = new RestHelper();   
    
    public ArrayList<BusinessUnitBean> getAllBU() throws MalformedURLException,IOException {
        
        ArrayList<BusinessUnitBean> BuList = new ArrayList<BusinessUnitBean>();
        
        String restApi = getInstanceUrl() + getBussinesUnitUrl();
        
        JSONObject obj = rs.httpGet(restApi);
        JSONArray arr = obj.getJSONArray("items");
        
        for (int i = 0; i < arr.length(); i++) {
            BusinessUnitBean bean = new BusinessUnitBean();
            JSONObject BuObj = arr.getJSONObject(i);

            bean.setBusinessUnitId(BuObj.getLong("BusinessUnitId"));
            bean.setBusinessUnitName(BuObj.getString("BusinessUnitName"));

            BuList.add(bean);
        }
       
        return BuList;
    }
}
