package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.dao.PurchaseOrderDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/purchaseOrder")

public class PurchaseOrderRest {
   
    @GET
    @Path("/getPurchaseOrder/{POHeaderId}")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response purchaseOrder (@PathParam ("POHeaderId") String POHeaderId) throws MalformedURLException,IOException {
       
       JSONObject obj2 = new JSONObject();
       PurchaseOrderDAO obj = new PurchaseOrderDAO();
       obj2 = obj.purchaseOrder(POHeaderId);
       
       return  Response.ok(obj2.toString()).build();   
    } 
}
