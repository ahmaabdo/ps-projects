package com.appspro.tbcFIN.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;
import com.appspro.tbcFIN.bean.RetInvoiceReleaseBean;
import com.appspro.tbcFIN.bean.RetentionInvoiceBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;

public class RetInvoiceReleaseDAO extends AppsproConnection {

    Connection connection;
    PreparedStatement ps;

    ResultSet rs;
    RestHelper rh = new RestHelper();

    public JSONArray invoiceRelease(String invNumber) throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<RetInvoiceReleaseBean> retentionList =
                new ArrayList<RetInvoiceReleaseBean>();
            connection = AppsproConnection.getConnection();
            String query =
                "SELECT * FROM XXX_RETENTION_RELEASE WHERE INVOICE_NUMBER = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, invNumber);
            rs = ps.executeQuery();

            while (rs.next()) {
                RetInvoiceReleaseBean bean = new RetInvoiceReleaseBean();

                bean.setId(rs.getString("ID"));
                bean.setInvoiveNumber(rs.getString("INVOICE_NUMBER"));
                bean.setInvoiceRelease(rs.getString("INVOICE_RELEASE"));

                retentionList.add(bean);
            }
            arr = new JSONArray(retentionList);

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }
    
    public JSONArray getAllInvoiceRelease() throws SQLException {

        JSONArray arr = new JSONArray();

        try {
            ArrayList<RetInvoiceReleaseBean> retentionList = new ArrayList<RetInvoiceReleaseBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_RETENTION_RELEASE";
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                RetInvoiceReleaseBean bean = new RetInvoiceReleaseBean();

                bean.setId(rs.getString("ID"));
                bean.setInvoiveNumber(rs.getString("INVOICE_NUMBER"));
                bean.setInvoiceRelease(rs.getString("INVOICE_RELEASE"));

                retentionList.add(bean);
            }
            arr = new JSONArray(retentionList);

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return arr;
    }

    public String updateInvoiceRelease(RetInvoiceReleaseBean bean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();

            String query =
                "UPDATE  XXX_RETENTION_RELEASE SET  INVOICE_RELEASE=? WHERE INVOICE_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getInvoiceRelease());
            ps.setString(2, bean.getInvoiveNumber());
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Invoice Release updated successfully";
    }

    public String insertInvoiceRelease(RetInvoiceReleaseBean bean) throws SQLException {

        try {
            connection = AppsproConnection.getConnection();
            String query =
                "INSERT INTO XXX_RETENTION_RELEASE (INVOICE_NUMBER,INVOICE_RELEASE) VALUES (?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getInvoiveNumber());
            ps.setString(2, bean.getInvoiceRelease());
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            closeResources(connection, ps, rs);
        }

        return "Invoice Release added successfully";
    }
}
