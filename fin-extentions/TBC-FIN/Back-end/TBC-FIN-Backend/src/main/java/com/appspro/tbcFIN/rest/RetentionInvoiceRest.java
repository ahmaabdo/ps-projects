package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.bean.RetentionInvoiceBean;
import com.appspro.tbcFIN.dao.RetentionInvoiceDAO;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/retention")

public class RetentionInvoiceRest {
    
    RetentionInvoiceDAO serivces = new RetentionInvoiceDAO();
    JSONArray arr = new JSONArray();
    JSONObject resposne = new JSONObject();
    
    @GET
    @Path("/retentionInv/{poNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllContractDetails(@PathParam("poNumber")String poNumber)  {
       
        try {
            arr = serivces.retentionInvoice(poNumber);
            resposne.put("Status", "OK");
            resposne.put("Data", arr);
            
        } catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString();
    }
    
    @POST
    @Path("insertRetentionInv")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertRetentionInvoice(String body) {
            
        RetentionInvoiceBean bean = new RetentionInvoiceBean();
        String status = "";
        try {
            JSONObject obj = new JSONObject(body);
            System.out.println(obj);
            
            bean.setPoNumber(obj.getString("poNumber"));
            bean.setPoAmount(obj.getString("poAmount"));
            bean.setReamingAmount(obj.getString("reamingAmount"));

            status = serivces.insertRetentionInvoice(bean);
                
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } 
        catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString() ;
    }
    
    @POST
    @Path("updateRetentionInv")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateRetentionInvoice(String body) {
            
        RetentionInvoiceBean bean = new RetentionInvoiceBean();
        String status = "";
        try {
            JSONObject obj = new JSONObject(body);
            System.out.println(obj);
            
            bean.setPoNumber(obj.getString("poNumber"));
            bean.setReamingAmount(obj.getString("reamingAmount"));

            status = serivces.updateRetentionInvoice(bean);
                
            resposne.put("Status", "OK");
            resposne.put("Data", status);
        } 
        catch (SQLException e) {
            resposne.put("Status", "ERROR");
            resposne.put("Data", e.getMessage());
        }
        return resposne.toString() ;
    }
    
}
