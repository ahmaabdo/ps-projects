package com.appspro.tbcFIN.bean;

public class BusinessUnitBean {
 
    private long businessUnitId;  
    private String BusinessUnitName;

    public void setBusinessUnitName(String BusinessUnitName) {
        this.BusinessUnitName = BusinessUnitName;
    }

    public String getBusinessUnitName() {
        return BusinessUnitName;
    }

    public void setBusinessUnitId(long businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public long getBusinessUnitId() {
        return businessUnitId;
    }
}
