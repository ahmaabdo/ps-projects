package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.dao.EmployeeDetails;
import com.appspro.tbcFIN.bean.EmployeeBean;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/getemployee")
public class GetEmployee {

    @GET
    @Path("/{username}")
    @Produces( { "application/json" })
    public EmployeeBean getDataFromLookup(@PathParam("username")
        String userName) {


        EmployeeBean obj = new EmployeeBean();
        EmployeeDetails det = new EmployeeDetails();
        if (userName != null && !userName.isEmpty()) {
            obj = det.getEmpDetails(userName, null, null,true);
        }

        return obj;
    }

}
