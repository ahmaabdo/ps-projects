package com.appspro.tbcFIN.dao;

import biPReports.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.tbcFIN.bean.RetentionInvoiceBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONArray;


public class RetentionInvoiceDAO extends AppsproConnection {
    
    Connection connection;
    PreparedStatement ps;

    ResultSet rs;
    RestHelper rh = new RestHelper();
    
    public JSONArray retentionInvoice(String poNumber) throws SQLException {

        JSONArray arr  = new JSONArray();

        try{
            ArrayList<RetentionInvoiceBean> retentionList = new ArrayList<RetentionInvoiceBean>();
            connection = AppsproConnection.getConnection();
            String query = "SELECT * FROM XXX_RETENTION_INVOICE WHERE PO_NUMBER = ?";
            ps = connection.prepareStatement(query);
            ps.setString(1, poNumber);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                RetentionInvoiceBean bean = new RetentionInvoiceBean();

                bean.setId(rs.getString("ID"));
                bean.setPoNumber(rs.getString("PO_NUMBER"));
                bean.setPoAmount(rs.getString("PO_TOTAL_AMOUNT"));
                bean.setReamingAmount(rs.getString("REAMING_AMOUNT"));
             
                retentionList.add(bean);
            }
            arr = new JSONArray(retentionList);
            
        } catch (SQLException e){
            e.printStackTrace();
           throw e;
        }
        finally {
           closeResources(connection, ps, rs);
        }

        return arr;
    }
    
    public String updateRetentionInvoice(RetentionInvoiceBean bean) throws SQLException {

        try{
            connection = AppsproConnection.getConnection();
            
            String query = "UPDATE  XXX_RETENTION_INVOICE set  REAMING_AMOUNT=? where PO_NUMBER=?";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getReamingAmount());
            ps.setString(2, bean.getPoNumber());
            ps.executeUpdate();
            
        } catch (SQLException e){
            e.printStackTrace();
            throw e;
        }
        finally {
            closeResources(connection, ps, rs);
        }

        return "PO Remaing Amount updated successfully";
    }
    
    public String insertRetentionInvoice(RetentionInvoiceBean bean) throws SQLException {

        try{
            connection = AppsproConnection.getConnection();
            String query = "INSERT INTO XXX_RETENTION_INVOICE (PO_NUMBER,PO_TOTAL_AMOUNT,REAMING_AMOUNT) VALUES (?,?,?)";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getPoNumber());
            ps.setString(2, bean.getPoAmount());
            ps.setString(3, bean.getReamingAmount());
            ps.executeUpdate();
            
           }catch (SQLException e){
                e.printStackTrace();
                throw e;
           }
            finally {
                 closeResources(connection, ps, rs);
            }

             return "PO Inserted successfully";
    } 
}
