package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.dao.CreditInvoiceDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;


@Path("/creditInvoice")
public class creditInvoiceRest {
   
    @POST
    @Path("/create/{method}/{invNumber}")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response creditInvoice (String body,@PathParam("method")String method,@PathParam("invNumber")long invNumber ){
        
        JSONObject obj2 = new JSONObject();
        JSONObject bodyObj = new JSONObject(body);
        System.out.println(bodyObj);
        CreditInvoiceDAO creditInv = new CreditInvoiceDAO();
        obj2 = creditInv.createInvoice(bodyObj.toString(),method,invNumber);
        
        return  Response.ok(obj2.toString()).build();
    }
}

