package com.appspro.tbcFIN.bean;

public class RetentionInvoiceBean {
  
  private String id;
  private String poNumber;
  private String poAmount;
  private String reamingAmount;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoAmount(String poAmount) {
        this.poAmount = poAmount;
    }

    public String getPoAmount() {
        return poAmount;
    }

    public void setReamingAmount(String reamingAmount) {
        this.reamingAmount = reamingAmount;
    }

    public String getReamingAmount() {
        return reamingAmount;
    }
}
