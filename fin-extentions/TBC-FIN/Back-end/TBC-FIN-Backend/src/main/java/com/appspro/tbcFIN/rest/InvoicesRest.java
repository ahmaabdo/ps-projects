package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.dao.InvoicesDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;

@Path("/Invoices")
public class InvoicesRest {
    
    @GET
    @Path("/getAllInvoices")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response allInvoices () throws MalformedURLException, IOException {
        
        JSONArray arr = new JSONArray();
        InvoicesDAO  invoice  = new InvoicesDAO();

        arr = invoice.allInvoices();
            
        return  Response.ok(arr.toString()).build();
    }
}
