package com.appspro.tbcFIN.rest;

import com.appspro.tbcFIN.bean.BusinessUnitBean;
import com.appspro.tbcFIN.dao.BusinessUnitDAO;

import java.io.IOException;

import java.net.MalformedURLException;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;


@Path("/businessUnit")
public class BusinessUnitRest {
  
    @GET
    @Path("/allbusinessUnit")
    @Consumes("application/json")
    @Produces("application/json")
    
    public Response business () throws MalformedURLException,IOException {
        
        ArrayList<BusinessUnitBean> supplierList = new ArrayList<BusinessUnitBean>();
        BusinessUnitDAO businessDao = new BusinessUnitDAO();
        
        supplierList = businessDao.getAllBU(); 
        
        JSONArray array = new JSONArray(supplierList);
        return  Response.ok(array.toString()).build();
    }
}
