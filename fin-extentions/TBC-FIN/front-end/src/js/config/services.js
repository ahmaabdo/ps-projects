define(['config/serviceconfig', 'util/commonhelper'], function(serviceConfig, commonHelper) {

    function services() {

        var self = this;
        var biReportServletPath = "report/commonbireport";
        var restPath = commonHelper.getInternalRest();
        var headers = {};

        self.authenticate = function(payload) {
            var serviceURL = restPath + "login";
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };
        self.postGeneric = function(serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.getGeneric = function(serviceName) {
            var serviceURL = restPath + serviceName;

            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, true);
        };

        self.getAllEmployees = function(serviceName) {
            var serviceURL = restPath + serviceName;
            var headers = {
                //   "Authorization": "Basic QXBwc1Byby1IQ006QXBwc3Byb0AxMjM="
            };
            return serviceConfig.callGetService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

        self.getGenericReport = function(payload) {
            var serviceURL = restPath + biReportServletPath;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.searcheGeneric = function(serviceName, payload) {
            var serviceURL = restPath + serviceName;
            var headers = {
                "content-type": "application/x-www-form-urlencoded"
            };
            return serviceConfig.callPostServiceUncodeed(serviceURL, payload, serviceConfig.contentTypeApplicationXWWw, true, headers);
        };

        self.editGeneric = function(serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPutService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.postSaasTsGeneric = function(serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.addGeneric = function(serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, true, headers);
        };

        self.addGenericSaas = function(serviceName, payload) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callPostService(serviceURL, payload, serviceConfig.contentTypeApplicationJSON, false, headers);
        };

        self.deleteGeneric = function(serviceName, headers) {
            var serviceURL = restPath + serviceName;
            return serviceConfig.callDeleteService(serviceURL, serviceConfig.contentTypeApplicationJSON, headers, true);
        };

    }

    return new services();
});