define([], function() {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function() {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function() {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getBiReportServletPath = function() {
            var host = "report/commonbireport";
            return host;
        };

        self.supplier = "supplier/getSuppliers";
        self.businessUnit = "businessUnit/allbusinessUnit";
        self.supplierSite = "report/getAllSupplierSite";
        self.allInvoices = "Invoices/getAllInvoices";
        self.allInvoicesreport = "report/getAllInvoicelines";
        self.createcreditInvoice = "creditInvoice/create/";
        self.retentionInv = "retention/retentionInv/";
        self.insertRetentionInv = "retention/insertRetentionInv";
        self.updateRetentionInv = "retention/updateRetentionInv";
        self.poAmountUrl = "purchaseOrder/getPurchaseOrder";
        self.insetInvRelease = "retention/insertInvRelease";
        self.updateInvRelease = "retention/updateInvRelease";
        self.getInvRelease = "retention/InvRelease/";
        self.getAllInvRelease = "retention/getAllInvRelease"

        self.getInternalRest = function() {

            if (window.location.hostname == "digital.tbc.sa") {

                var host = "https://digital.tbc.sa/TBC-FIN-BACKEND-TEST/rest/";

            } else {

                var host = " http://127.0.0.1:7101/TBC-FIN-BACKEND-TEST/rest/";
            }

            return host;
        };

    }

    return new commonHelper();
});