define({
    "root": {

        "login": {
            "userName": "User Name: ",
            "Password": "Password: ",
            "loginLabel": "Login",
            "resetLabel": "Reset",
            "loginFailureText": "Invalid User Name or Password",
            "SignOut": "Sign Out",
            "Name": "Employee Name: "
        },

        "retentionScreen": {
            "bussinesUnitLbl": "Bussines Unit",
            "supplierLbl": "Supplier",
            "supplierSiteLbl": "Supplier Site",
            "purchaseOrderLbl": "Purchase Order",
            "projectNumberLbl": "Project Number",
            "invoiceDateLbl": "Invoice Date",
            "calculateLbl": "Calculate & Create Debit Memo",
            "supplierNameLbl": "Supplier Name",
            "invoiceType": "Invoice Type",
            "invoiceNumberLbl": "Invoice Number",
            "PONumberLbl": "PO Number",
            "projectDescriptionLbl": "Project Description",
            "invoiceAmountLbl": "Invoice Amount",
            "invoiceDescriptionLbl": "Invoice Description",
            "retentionPercLbl": "Retention Percentag",
            "calculateRetentionLbl": "Create Retention Invoice",
            "amountReleaseLbl": "Amount Release",
            "calRetScreenLbl": "Calculate Retention Screen",
            "creRetScreenLbl": "Create Retention Invoice Screen",
            "paymentTermLbl": "Payment Term"
        },

        "common": {
            "switchLang": "العربية",
            "inputPlaceholder": "Please enter value",
            "selectPlaceholder": "Please Select value",
            "addLbl": "Add",
            "editLbl": "Edit",
            "backLbl": " Back",
            "noActionLbl": "لا",
            "yesLbl": "نعم",
            "viewLbl": "View",
            "searchLbl": "Search",
            "deleteLbl": "Delete",
            "saveLbl": "Save and Close",
            "clearLbl": "Clear",
            "confirmationMessageLbl": "Confirm Discard Changes",
            "cancelLbl": "Cancel",
            "contractTypeLbl": "Contract Type :",
            "submitLbl": "submit",
            "showMoreLbl": "View More",
            "okLbl": "OK",
            "percentSignLbl": "%",
            "srNoLbl": "NO.",
            "sureMessageLbl": " Are you sure?",
            "confirmationTextLbl": "Confirm Dialog",
            "resetLbl": "Reset",
            "changeLbl": "Change Percentag",
            "cancelLbl": "Cancel",
            "changeAmountLbl": "Change Amount",
            "confirmLanguageLbl": "هل انت متاكد من تغيير اللغه ؟"

        }
    },
    "ar": true,
    "cs": true,
    "id": true
});