define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function(oj, ko, $, app, services, commonhelper, DataCollectionEditUtils) {
    function rententionContentViewModel() {
        var self = this;
        var filteredData;

        self.customsModel = {
            bussinesUnitVal: ko.observable(),
            supplierVal: ko.observable(),
            supplierSiteVal: ko.observable(),
            purchaseOrderVal: ko.observable(),
            projectNumberVal: ko.observable(),
            invoiceDateVal: ko.observable(),
            retentionNo: ko.observable(),
            rowSelected: ko.observable(),

            calculateDis: ko.observable(true),
            searchDis: ko.observable(true),

            businessUnitArr: ko.observableArray([]),
            supplierArr: ko.observableArray([]),
            supplierSiteArr: ko.observableArray([]),
            invoiceArr: ko.observableArray([]),
            columnArray: ko.observableArray([]),
            linesArr: ko.observable([]),
            standardInvoiceArr: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
            sumInvAmountArr: ko.observableArray([]),
            uncreatedDebitInvoices: ko.observableArray([]),
        };

        self.selectedItems = ko.observableArray([]);
        self.headerCheckStatus = ko.observable();
        self.label = ko.observable();

        self.dataproviderRetention = ko.observable();
        self.dataproviderRetention(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.invoiceArr, { idAttribute: 'srN' })));

        ko.computed(function() {
            if (app.businessUnitArr()) {
                var xx = app.businessUnitArr().map(el => ({ label: el.businessUnitName, value: el.businessUnitName }))
                self.customsModel.businessUnitArr(xx)
            }
            if (app.supplierSiteArr()) {
                var x = app.supplierSiteArr().map(el => ({ label: el.VENDOR_NAME, value: el.VENDOR_NAME }))
                self.customsModel.supplierArr(x)
                var suppSiteArr = [...new Set(app.supplierSiteArr().map(el => el.VENDOER_SITE))];
            }
            if (app.invoicesReportArr().length > 0) {
                self.customsModel.standardInvoiceArr(app.invoicesReportArr())
                self.customsModel.invoiceArr([]);
                self.customsModel.searchDis(false);
            }
        })

        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
            self.customsModel.rowSelected = filteredData[index];
            if (e.target.type == 'button') {
                if (self.customsModel.rowSelected) {
                    if (self.customsModel.rowSelected.RETENTION_PERCANTGE) {
                        self.customsModel.retentionNo(parseInt(self.customsModel.rowSelected.RETENTION_PERCANTGE))
                        document.querySelector("#retentionDialog").open();
                    } else {
                        document.querySelector("#retentionDialog").open();
                        self.customsModel.retentionNo(0)
                    }
                }
            } else if (e.target.type == 'checkbox') {
                if (self.customsModel.rowSelected) {
                    if (e.target.checked) {
                        self.customsModel.rowSelectedArr().push(self.customsModel.rowSelected)
                        self.customsModel.calculateDis(false);
                    } else {
                        self.customsModel.rowSelectedArr(self.customsModel.rowSelectedArr().filter(el => el.INVOICE_NUMBER != self.customsModel.rowSelected.INVOICE_NUMBER))
                        if (self.customsModel.rowSelectedArr().length == 0)
                            self.customsModel.calculateDis(true);
                    }
                }
            }
            self.customsModel.sumInvAmountArr(self.customsModel.rowSelectedArr())
            console.log(self.customsModel.rowSelectedArr())
        })

        self.searchBtn = function() {
            cond = function(el) {
                var res = (self.customsModel.supplierVal() ? el.SUPPLIER_NAME === self.customsModel.supplierVal() : true) &&
                    (self.customsModel.bussinesUnitVal() ? el.BUSINESSUNITNAME === self.customsModel.bussinesUnitVal() : true) &&
                    (self.customsModel.supplierSiteVal() ? el.SUPPLIER_SITE === self.customsModel.supplierSiteVal() : true) &&
                    (self.customsModel.purchaseOrderVal() ? el.PURCHASE_ORDER_NUMBER === self.customsModel.purchaseOrderVal() : true) &&
                    (self.customsModel.projectNumberVal() ? el.PROJECT_NUMBER == self.customsModel.projectNumberVal() : true) &&
                    (self.customsModel.invoiceDateVal() ? el.INVOICE_DATE === self.customsModel.invoiceDateVal() : true)
                return res;
            }

            var x = self.customsModel.standardInvoiceArr().filter(el => el.INVOICE_NUMBER.toString().split('-')[0] != "RET " && el.INVOICE_NUMBER.toString().split('-')[1] != " DUE ")
            self.customsModel.standardInvoiceArr(x)
            filteredData = self.customsModel.standardInvoiceArr().filter(el => cond(el))
            self.customsModel.invoiceArr(filteredData);
            self.dataproviderRetention(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.invoiceArr, { idAttribute: 'srN' })));
        }

        self.resetBtn = function() {
            self.customsModel.supplierVal('')
            self.customsModel.bussinesUnitVal('')
            self.customsModel.supplierSiteVal('')
            self.customsModel.purchaseOrderVal('')
            self.customsModel.projectNumberVal('')
            self.customsModel.invoiceDateVal('')
            self.customsModel.invoiceArr([]);
            self.customsModel.calculateDis(true);
            self.customsModel.rowSelectedArr([])
        }

        var reamingAmount;
        var empty = true;
        var noPOFound = true;
        var sumInv;
        var poamount;

        self.calculateBtn = function() {
            app.loading(true);
            if (self.customsModel.rowSelectedArr().length == 0) {
                self.customsModel.uncreatedDebitInvoices().forEach(e => {
                    if (e.res == "PO") {
                        app.messagesDataProvider.push(app.createMessage('error', `Can't create debit invoice for (${e.number}),Invoice amount larger than PO (${e.po}) Amount.`))
                    } else if (e.res == "PONotFound") {
                        app.messagesDataProvider.push(app.createMessage('error', `Can't create debit invoice without Purchase Order.`))
                    } else if (e.res == "Percantage") {
                        app.messagesDataProvider.push(app.createMessage('error', `Retention Percentage must large than zero for invoice number ${e.number},To able create debit invoice.`))
                    } else if (e.res == "created") {
                        app.messagesDataProvider.push(app.createMessage('confirmation', `Debit invoice created successfuly with number ${e.number}`))
                    } else if (e.res == "error") {
                        app.messagesDataProvider.push(app.createMessage('error', `Can't create debit invoice for (${e.number}) with the following error${e.errorResp}`))
                    } else if (e.res == "project") {
                        app.messagesDataProvider.push(app.createMessage('error', `Can't create debit invoice without project number`))
                    } else if (e.res == "retention") {
                        app.messagesDataProvider.push(app.createMessage('error', `Can't create debit invoice without retention percentage`))
                    }
                })
                self.customsModel.uncreatedDebitInvoices([])
                self.customsModel.calculateDis(true);
                app.allInvoicesreport();
                app.loading(false);
            } else {
                var element = self.customsModel.rowSelectedArr().shift();
                if (element.PURCHASE_ORDER_NUMBER) {
                    services.getGeneric(commonhelper.poAmountUrl + "/" + element.PURCHASE_ORDER_NUMBER).then(data => {
                        poamount = data.Ordered
                        var poAmt = poamount * 0.05;
                        if (poAmt >= (element.INVOICE_AMOUNT_WITHOUT_TAX.replace(/,/g, "") * (element.RETENTION_PERCANTGE / 100))) {
                            if (element.PROJECT_NUMBER) {
                                if (element.RETENTION_PERCANTGE != 0 && element.RETENTION_PERCANTGE) {
                                    services.getGeneric(commonhelper.retentionInv + element.PURCHASE_ORDER_NUMBER).then(data => {
                                        if (data.Data.length == 0) {
                                            self.createInvoice(element);
                                        } else {
                                            reamingAmount = data.Data[0].reamingAmount;
                                            if (reamingAmount >= (element.INVOICE_AMOUNT_WITHOUT_TAX.replace(/,/g, "") * (element.RETENTION_PERCANTGE / 100))) {
                                                self.createInvoice(element);
                                                empty = false;
                                            } else {
                                                var obj = { number: element.INVOICE_NUMBER, res: "PO", po: element.PURCHASE_ORDER_NUMBER }
                                                self.customsModel.uncreatedDebitInvoices().push(obj);
                                                self.calculateBtn();
                                            }
                                        }
                                    }, error => {
                                        app.loading(false);
                                    })
                                } else {
                                    var obj = { number: element.INVOICE_NUMBER, res: "retention", po: element.PURCHASE_ORDER_NUMBER }
                                    self.customsModel.uncreatedDebitInvoices().push(obj)
                                    self.calculateBtn();
                                }
                            } else {
                                var obj = { number: element.INVOICE_NUMBER, res: "project", po: element.PURCHASE_ORDER_NUMBER }
                                self.customsModel.uncreatedDebitInvoices().push(obj)
                                self.calculateBtn();
                            }
                        } else {
                            var obj = { number: element.INVOICE_NUMBER, res: "PO", po: element.PURCHASE_ORDER_NUMBER }
                            self.customsModel.uncreatedDebitInvoices().push(obj)
                            self.calculateBtn();
                        }
                    }, error => {})
                } else {
                    var obj = { number: element.INVOICE_NUMBER, res: "PONotFound" }
                    self.customsModel.uncreatedDebitInvoices().push(obj)
                    self.calculateBtn();
                }
            }

        }

        self.createInvoice = function(el) {
            app.loading(true);
            var payload = {
                "InvoiceNumber": "RET-" + el.INVOICE_NUMBER,
                "BusinessUnit": el.BUSINESSUNITNAME,
                "InvoiceAmount": (el.INVOICE_AMOUNT_WITHOUT_TAX.replace(/,/g, "") * (el.RETENTION_PERCANTGE / 100)) * -1,
                "InvoiceDate": el.INVOICE_DATE,
                "Supplier": el.SUPPLIER_NAME,
                "SupplierSite": el.SUPPLIER_SITE,
                "AccountingDate": el.INVOICE_DATE,
                "InvoiceReceivedDate": el.INVOICE_DATE,
                "InvoiceType": "Debit memo",
                "Description": "Retention " + el.INVOICE_DESCRIPTION,
                "InvoiceSource": "Manual invoice entry",
                "InvoiceCurrency": "SAR",
                "PaymentCurrency": "SAR",
                "PaymentTerms": el.PAYMENTTERM,
                "TaxationCountry": "Saudi Arabia",
            };

            var lines = [{
                "LineNumber": 1,
                "LineType": "Item",
                "LineAmount": (el.INVOICE_AMOUNT_WITHOUT_TAX.replace(/,/g, "") * (el.RETENTION_PERCANTGE / 100) * -1), //el.INVOICE_RELEASE,
                "Description": "Retention " + el.INVOICE_DESCRIPTION,
                "AccountingDate": el.INVOICE_DATE,
                "TaxClassification": "SAUDI_VAT_0%",
                "DistributionCombination": "01-0000-000000-000000-2110201-00000-00000",
                "TransactionBusinessCategory": "Purchase Transaction",
                "TransactionBusinessCategoryCodePath": "PURCHASE_TRANSACTION",
                "invoiceLineDff": [{
                    "projectNumber": self.customsModel.rowSelected.PROJECT_NUMBER
                }]
            }]

            payload.invoiceLines = lines

            var patchPayload = {
                "invoiceDff": [{
                    "retentionVisibality": "Debit"
                }]
            }

            var payload2 = {
                poNumber: el.PURCHASE_ORDER_NUMBER,
            }

            services.postGeneric(commonhelper.createcreditInvoice + "POST/" + 1, payload).then(data => {
                sumInv = el.INVOICE_AMOUNT_WITHOUT_TAX.replace(/,/g, "") * (el.RETENTION_PERCANTGE / 100);
                if (data.status == "done") {
                    var obj = { number: data.data.InvoiceNumber, res: "created" }
                    self.customsModel.uncreatedDebitInvoices().push(obj)
                    services.postGeneric(commonhelper.createcreditInvoice + "PATCH/" + el.INVOICE_ID, patchPayload).then(data => {
                        self.calculateBtn();
                    }, error => { app.loading(false); })
                    if (empty) {
                        payload2.poAmount = poamount;
                        payload2.reamingAmount = ((poamount * (el.RETENTION_PERCANTGE / 100)) - sumInv).toString();
                        services.postGeneric(commonhelper.insertRetentionInv, payload2).then(data => {}, error => { app.loading(false); })
                    } else if (!empty) {
                        payload2.reamingAmount = (reamingAmount - sumInv).toString();
                        services.postGeneric(commonhelper.updateRetentionInv, payload2).then(data => {}, error => { app.loading(false); })
                    }
                } else {
                    var obj = { number: el.INVOICE_NUMBER, res: "error", errorResp: data.data }
                    self.customsModel.uncreatedDebitInvoices().push(obj);
                    self.calculateBtn();
                }
            }, error => {
                app.loading(false);
                app.messagesDataProvider.push(app.createMessage('error', `Error in creation invoice, try again.`))
                self.customsModel.rowSelectedArr([])
            })

        }

        self.changeDialBtn = function() {
            self.customsModel.rowSelected.RETENTION_PERCANTGE = self.customsModel.retentionNo()
            filteredData[filteredData.findIndex(el => el.INVOICE_ID == self.customsModel.rowSelected.INVOICE_ID)] = self.customsModel.rowSelected
            self.customsModel.invoiceArr(filteredData)
            document.querySelector("#retentionDialog").close();
            self.dataproviderRetention(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.invoiceArr, { idAttribute: 'INVOICE_ID' })));
        }

        self.supplierValueChanged = function(event) {
            if (app.supplierSiteArr()) {
                if (event['detail'].value == '' || event['detail'].value == null) {
                    self.customsModel.supplierSiteVal('')
                    self.customsModel.supplierSiteArr([])
                } else {
                    if (self.customsModel.bussinesUnitVal()) {
                        var bussinesId = app.businessUnitArr().find(e => self.customsModel.bussinesUnitVal() == e.businessUnitName).businessUnitId
                        var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                        self.supplierFun(suppSite, bussinesId);
                    } else {
                        var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                        if (Array.isArray(suppSite)) {
                            self.customsModel.supplierSiteArr(suppSite.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        } else {
                            self.customsModel.supplierSiteArr([suppSite].map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        }
                        self.customsModel.supplierSiteVal(app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).VENDOER_SITE)
                    }
                }
            }
        }

        self.businessUnitChanged = function(event) {
            self.customsModel.supplierSiteVal('')
            if (app.supplierSiteArr() && self.customsModel.supplierVal() && app.businessUnitArr()) {
                if (event['detail'].value == '' || event['detail'].value == null) {
                    self.customsModel.supplierSiteVal('')
                    if (self.customsModel.supplierVal()) {
                        var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                        if (Array.isArray(suppSite)) {
                            self.customsModel.supplierSiteArr(suppSite.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        } else {
                            self.customsModel.supplierSiteArr([suppSite].map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        }
                        self.customsModel.supplierSiteVal(app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).VENDOER_SITE)
                    }
                } else {
                    var bussinesId = app.businessUnitArr().find(e => event['detail'].value == e.businessUnitName).businessUnitId
                    var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                    self.supplierFun(suppSite, bussinesId);
                }
            }
        }

        self.supplierFun = function(suppSite, bussinesId) {
            var suppArr = [];
            if (Array.isArray(suppSite)) {
                suppSite.forEach(e => {
                    if (e.BUSINESSUNIT == bussinesId)
                        suppArr.push({ VENDOER_SITE: e.VENDOER_SITE })
                })
                if (suppArr.length == 0)
                    self.customsModel.supplierSiteArr([])
                else
                    self.customsModel.supplierSiteArr(suppArr.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));

            } else {
                [suppSite].forEach(e => {
                    if (e.BUSINESSUNIT == bussinesId)
                        suppArr.push({ VENDOER_SITE: e.VENDOER_SITE })
                })
                if (suppArr.length == 0)
                    self.customsModel.supplierSiteArr([])

                else
                    self.customsModel.supplierSiteArr(suppArr.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
            }
        }

        self.getIndexInSelectedItems = function(id) {
            for (var i = 0; i < self.selectedItems().length; i++) {
                var range = self.selectedItems()[i];
                var startIndex = range.startIndex.row;
                var endIndex = range.endIndex.row;
                if (id >= startIndex && id <= endIndex) {
                    return i;
                }
            }
            return -1;
        };

        self.handleCheckbox = function(id, srN) {
            var isChecked = self.getIndexInSelectedItems(id) != -1 || (srN ? self.customsModel.rowSelectedArr().find(e => e.srN == srN) : false);
            return isChecked ? ['checked'] : [];
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.cancelDialBtn = function() {
            document.querySelector("#retentionDialog").close();
        }

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                bussinesUnitLbl: getTransaltion("retentionScreen.bussinesUnitLbl"),
                supplierLbl: getTransaltion("retentionScreen.supplierLbl"),
                supplierSiteLbl: getTransaltion("retentionScreen.supplierSiteLbl"),
                purchaseOrderLbl: getTransaltion("retentionScreen.purchaseOrderLbl"),
                projectNumberLbl: getTransaltion("retentionScreen.projectNumberLbl"),
                invoiceDateLbl: getTransaltion("retentionScreen.invoiceDateLbl"),
                calculateLbl: getTransaltion("retentionScreen.calculateLbl"),
                searchLbl: getTransaltion("common.searchLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                supplierNameLbl: getTransaltion("retentionScreen.supplierNameLbl"),
                invoiceType: getTransaltion("retentionScreen.invoiceType"),
                PONumberLbl: getTransaltion("retentionScreen.PONumberLbl"),
                projectDescriptionLbl: getTransaltion("retentionScreen.projectDescriptionLbl"),
                invoiceAmountLbl: getTransaltion("retentionScreen.invoiceAmountLbl"),
                invoiceNumberLbl: getTransaltion("retentionScreen.invoiceNumberLbl"),
                invoiceDescriptionLbl: getTransaltion("retentionScreen.invoiceDescriptionLbl"),
                retentionPercLbl: getTransaltion("retentionScreen.retentionPercLbl"),
                calRetScreenLbl: getTransaltion("retentionScreen.calRetScreenLbl"),
                changeLbl: getTransaltion("common.changeLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                paymentTermLbl: getTransaltion("retentionScreen.paymentTermLbl"),
            });

            self.customsModel.columnArray([{
                    "headerText": "Select",
                    "template": 'checkTemplate'
                }, {
                    "headerText": "NO.",
                    "template": 'seqTemplate'
                },
                {
                    "headerText": self.label().invoiceNumberLbl,
                    "field": "INVOICE_NUMBER"
                },
                {
                    "headerText": self.label().invoiceType,
                    "field": "INVOICE_TYPE"
                },
                {
                    "headerText": self.label().invoiceAmountLbl,
                    "field": "INVOICE_AMOUNT_WITHOUT_TAX"
                },
                {
                    "headerText": self.label().invoiceDateLbl,
                    "field": "INVOICE_DATE"
                },
                {
                    "headerText": self.label().invoiceDescriptionLbl,
                    "field": "INVOICE_DESCRIPTION"
                },
                {
                    "headerText": self.label().bussinesUnitLbl,
                    "field": "BUSINESSUNITNAME"
                },
                {
                    "headerText": self.label().supplierNameLbl,
                    "field": "SUPPLIER_NAME"
                },
                {
                    "headerText": self.label().supplierSiteLbl,
                    "field": "SUPPLIER_SITE"
                },
                {
                    "headerText": self.label().PONumberLbl,
                    "field": "PURCHASE_ORDER_NUMBER"
                },
                {
                    "headerText": self.label().projectNumberLbl,
                    "field": "PROJECT_NUMBER"
                },
                {
                    "headerText": self.label().projectDescriptionLbl,
                    "field": "PROJECT_DESCRIPTION"
                },
                {
                    "headerText": self.label().paymentTermLbl,
                    "field": "PAYMENTTERM"
                },
                {
                    "headerText": self.label().retentionPercLbl,
                    "field": "RETENTION_PERCANTGE"
                },
                {
                    "headerText": "Action",
                    "field": "RETENTION_PERCANTGE",
                    "template": 'buttonTemplate'
                }
            ]);
        }
        initTransaltion();
    }


    return rententionContentViewModel;
});