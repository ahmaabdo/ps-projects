define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function(oj, ko, $, app, services, commonhelper) {
    function createRetentionContentViewModel() {
        var self = this;
        self.label = ko.observable();

        self.customsModel = {
            bussinesUnitVal: ko.observable(),
            supplierVal: ko.observable(),
            supplierSiteVal: ko.observable(),
            purchaseOrderVal: ko.observable(),
            projectNumberVal: ko.observable(),
            invoiceDateVal: ko.observable(),
            rowSelected: ko.observable(),
            retentionNo: ko.observable(),
            getIndexInSelectedItems: ko.observable(),
            selectedItems: ko.observable(),

            calculateDis: ko.observable(true),
            searchDis: ko.observable(true),

            businessUnitArr: ko.observableArray([]),
            supplierArr: ko.observableArray([]),
            supplierSiteArr: ko.observableArray([]),
            invoiceArr: ko.observableArray([]),
            columnArray: ko.observableArray([]),
            linesArr: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
        };

        self.getIndexInSelectedItems = ko.observableArray();
        self.selectedItems = ko.observableArray();

        self.dataproviderRetention2 = ko.observable();
        self.dataproviderRetention2(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.invoiceArr, { idAttribute: 'srN' })));

        ko.computed(function() {
            if (app.businessUnitArr()) {
                var xx = app.businessUnitArr().map(el => ({ label: el.businessUnitName, value: el.businessUnitName }))
                self.customsModel.businessUnitArr(xx)
            }
            if (app.supplierSiteArr()) {
                var x = app.supplierSiteArr().map(el => ({ label: el.VENDOR_NAME, value: el.VENDOR_NAME }))
                self.customsModel.supplierArr(x)
                var suppSiteArr = [...new Set(app.supplierSiteArr().map(el => el.VENDOER_SITE))];
            }
            if (app.invoicesReportArr().length > 0) {
                self.customsModel.invoiceArr([])
                self.customsModel.searchDis(false);
            }
        })

        $("body").delegate("#table2 .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder2").val();
            self.customsModel.rowSelected = self.customsModel.invoiceArr()[index];
            if (e.target.type == 'checkbox') {
                if (self.customsModel.rowSelected) {
                    if (e.target.checked) {
                        self.customsModel.rowSelectedArr().push(self.customsModel.rowSelected)
                        self.customsModel.calculateDis(false);
                    } else {
                        self.customsModel.rowSelectedArr(self.customsModel.rowSelectedArr().filter(el => el.INVOICE_NUMBER != self.customsModel.rowSelected.INVOICE_NUMBER))
                        if (self.customsModel.rowSelectedArr().length == 0)
                            self.customsModel.calculateDis(true);
                    }
                }
            } else {
                self.customsModel.retentionNo(parseInt(self.customsModel.rowSelected.INVOICE_RELEASE.replace(/,/g, "")))
                document.querySelector("#releaseDialog").open();
            }
        })

        self.searchBtn = function() {
            app.loading(true);
            cond = function(el) {
                var res = (self.customsModel.supplierVal() ? el.SUPPLIER_NAME === self.customsModel.supplierVal() : true) &&
                    (self.customsModel.bussinesUnitVal() ? el.BUSINESSUNITNAME === self.customsModel.bussinesUnitVal() : true) &&
                    (self.customsModel.supplierSiteVal() ? el.SUPPLIER_SITE === self.customsModel.supplierSiteVal() : true) &&
                    (self.customsModel.purchaseOrderVal() ? el.PURCHASE_ORDER_NUMBER === self.customsModel.purchaseOrderVal() : true) &&
                    (self.customsModel.projectNumberVal() ? el.PROJECT_NUMBER == self.customsModel.projectNumberVal() : true) &&
                    (self.customsModel.invoiceDateVal() ? el.INVOICE_DATE === self.customsModel.invoiceDateVal() : true)
                return res;
            }
            services.getGeneric(commonhelper.getAllInvRelease).then(data => {
                app.loading(false);
                if (data.Data.length != 0) {
                    data.Data.forEach(el => {
                        var obj = app.debitInvoices().find(e => e.INVOICE_NUMBER == el.invoiveNumber)
                        if (obj) {
                            obj.INVOICE_RELEASE = parseInt(el.invoiceRelease);
                            obj.INVOICE_AMOUNT = parseInt(el.invoiceRelease) * -1;
                        }
                    })
                }
                app.debitInvoices().forEach(el => { el.INVOICE_AMOUNT = numberWithCommas(el.INVOICE_AMOUNT), el.INVOICE_RELEASE = numberWithCommas(el.INVOICE_RELEASE) })
                filteredData = app.debitInvoices().filter(el => cond(el));
                self.customsModel.invoiceArr(app.debitInvoices().filter(el => cond(el)));
                self.dataproviderRetention2(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.invoiceArr, { idAttribute: 'srN' })));
            }, error => {
                app.loading(false);
            })
        }

        self.resetBtn = function() {
            self.customsModel.supplierVal('')
            self.customsModel.bussinesUnitVal('')
            self.customsModel.supplierSiteVal('')
            self.customsModel.purchaseOrderVal('')
            self.customsModel.projectNumberVal('')
            self.customsModel.invoiceDateVal('')
            self.customsModel.invoiceArr([]);
            self.customsModel.rowSelectedArr([])
        }

        var releaseReamning = false;
        var releaseAmount = 0;
        var totalAmount = 0;
        var lineNumber = 1;
        var seq;
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        self.calculateBtn = function() {
            app.loading(true);
            var arr = [];

            var filterProjNum = app.notApprovalInvoice().filter(e => e.INVOICE_NUMBER.toString().split('-')[2] == self.customsModel.rowSelected.PROJECT_NUMBER)
            if (self.customsModel.rowSelectedArr().every(el => el.PROJECT_NUMBER)) {
                if (self.customsModel.rowSelected.PROJECT_NUMBER) {
                    app.notApprovalInvoice().filter(e => e.INVOICE_NUMBER.toString().split('-')[2] == self.customsModel.rowSelected.PROJECT_NUMBER).map(e => e.INVOICE_NUMBER).forEach(x => {
                        var prevInvoice = x.split('-')
                        arr.push(parseInt(prevInvoice[prevInvoice.length - 1]))
                    })
                    var val = arr.filter(el => isNaN(el) != true && el != undefined && el != "Infinity")
                    if (val.length == 0) {
                        seq = 1;
                    } else if (val.length != 0) {
                        var number = Math.max(...val);
                        seq = ++number
                    }
                    self.customsModel.rowSelectedArr().forEach(el => {
                        el.INVOICE_RELEASE = parseInt(el.INVOICE_RELEASE.toString().replace(/,/g, "")),
                            el.INVOICE_AMOUNT = parseInt(el.INVOICE_AMOUNT.toString().replace(/,/g, ""))
                        if (el.INVOICE_RELEASE == (el.INVOICE_AMOUNT * -1)) {
                            el.releaseReamning = false;
                        } else if (el.INVOICE_RELEASE != (el.INVOICE_AMOUNT * -1)) {
                            el.releaseReamning = true;
                            releaseAmount = parseInt((self.customsModel.rowSelected.INVOICE_AMOUNT.toString().replace(/,/g, "")) * -1) - parseInt(self.customsModel.rowSelected.INVOICE_RELEASE.toString().replace(/,/g, ""))
                        }
                    })
                    self.createRetentionInvoice();
                }
            } else {
                app.messagesDataProvider.push(app.createMessage('error', `Can't create retention invoice, Project Number not found.`))
                app.loading(false);
                return;
            }
        }

        self.createRetentionInvoice = function() {
            self.customsModel.rowSelectedArr().forEach(el => {
                totalAmount += parseInt(el.INVOICE_RELEASE.toString().replace(/,/g, ""));
                var lines = {
                    "LineNumber": lineNumber++,
                    "LineType": "Item",
                    "LineAmount": parseInt(el.INVOICE_RELEASE.toString().replace(/,/g, "")),
                    "Description": el.INVOICE_NUMBER,
                    "AccountingDate": el.INVOICE_DATE,
                    "TaxClassification": "SAUDI_VAT_0%",
                    "DistributionCombination": "01-0000-000000-000000-2110201-00000-00000",
                    "TransactionBusinessCategory": "Purchase Transaction",
                    "TransactionBusinessCategoryCodePath": "PURCHASE_TRANSACTION",
                    "invoiceLineDff": [{
                        "projectNumber": self.customsModel.rowSelected.PROJECT_NUMBER
                    }]
                }
                self.customsModel.linesArr().push(lines);
            })
            var payload = {
                "InvoiceNumber": "RET - DUE -" + "" + self.customsModel.rowSelected.PROJECT_NUMBER + "-" + seq,
                "BusinessUnit": self.customsModel.rowSelectedArr()[0].BUSINESSUNITNAME,
                "InvoiceAmount": parseInt(totalAmount),
                "InvoiceDate": date.toString,
                "Supplier": self.customsModel.rowSelectedArr()[0].SUPPLIER_NAME,
                "SupplierSite": self.customsModel.rowSelectedArr()[0].SUPPLIER_SITE,
                "AccountingDate": date.toString,
                "InvoiceReceivedDate": date.toString,
                "InvoiceType": "Standard",
                "Description": "Standard",
                "InvoiceSource": "Manual invoice entry",
                "InvoiceCurrency": "SAR",
                "PaymentCurrency": "SAR",
                "PaymentTerms": self.customsModel.rowSelectedArr()[0].PAYMENTTERM,
                "TaxationCountry": "Saudi Arabia",
                "invoiceLines": self.customsModel.linesArr()
            };
            services.postGeneric(commonhelper.createcreditInvoice + "POST/" + 1, payload).then(data => {
                if (data.status == "done") {
                    var responseInvoiceNumber = data.data.InvoiceNumber;
                    services.getGeneric(commonhelper.getAllInvRelease).then(data => {
                        self.customsModel.rowSelectedArr().forEach(el => {
                            var elementReleaseAmount = (el.INVOICE_AMOUNT.toString().replace(/,/g, "") * -1) - el.INVOICE_RELEASE.toString().replace(/,/g, "")
                            patchPayload = {
                                "invoiceDff": [{
                                    "retentionVisibality": "Debit"
                                }]
                            }
                            releasePayload = {
                                "invoiceNumber": el.INVOICE_NUMBER,
                                "invoiceRelease": elementReleaseAmount.toString().replace(/,/g, "")
                            }
                            if (el.releaseReamning) {
                                if (data.Data.length != 0) {
                                    var obj = data.Data.find(e => e.invoiveNumber == el.INVOICE_NUMBER)
                                    if (obj) {
                                        services.postGeneric(commonhelper.updateInvRelease, releasePayload).then(data => {}, error => {})
                                        if ((obj.invoiceRelease - elementReleaseAmount.toString().replace(/,/g, "")) == 0) {
                                            services.postGeneric(commonhelper.createcreditInvoice + "PATCH/" + el.INVOICE_ID, patchPayload).then(data => {}, error => {})
                                        }
                                    } else {
                                        services.postGeneric(commonhelper.insetInvRelease, releasePayload).then(data => {}, error => {})
                                    }
                                } else {
                                    services.postGeneric(commonhelper.insetInvRelease, releasePayload).then(data => {}, error => {})
                                }
                            } else {
                                services.postGeneric(commonhelper.createcreditInvoice + "PATCH/" + el.INVOICE_ID, patchPayload).then(data => {}, error => {})
                            }
                            app.debitInvoices([])
                            self.customsModel.rowSelectedArr([])
                            self.customsModel.linesArr([])
                            lines = {};
                            totalAmount = 0;
                            lineNumber = 1;
                            app.allInvoicesreport();
                        })
                        app.messagesDataProvider.push(app.createMessage('confirmation', `Invoice created successfuly with number ${responseInvoiceNumber}`))
                    }, error => {})
                } else {
                    app.messagesDataProvider.push(app.createMessage('error', `Error in : ${data.data}`))
                    self.customsModel.calculateDis(true);
                    self.customsModel.rowSelectedArr([])
                    self.customsModel.linesArr([])
                    lines = {};
                    totalAmount = 0;
                    lineNumber = 1;
                    app.allInvoicesreport();
                }
            }, error => {
                app.loading(false);
            })
        }

        self.supplierValueChanged = function(event) {
            if (app.supplierSiteArr()) {
                if (event['detail'].value == '' || event['detail'].value == null) {
                    self.customsModel.supplierSiteVal('')
                    self.customsModel.supplierSiteArr([])
                } else {
                    if (self.customsModel.bussinesUnitVal()) {
                        var bussinesId = app.businessUnitArr().find(e => self.customsModel.bussinesUnitVal() == e.businessUnitName).businessUnitId
                        var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                        self.supplierFun(suppSite, bussinesId);
                    } else {
                        var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                        if (Array.isArray(suppSite)) {
                            self.customsModel.supplierSiteArr(suppSite.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        } else {
                            self.customsModel.supplierSiteArr([suppSite].map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        }
                        self.customsModel.supplierSiteVal(app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).VENDOER_SITE)
                    }
                }
            }
        }

        self.businessUnitChanged = function(event) {
            self.customsModel.supplierSiteVal('')
            if (app.supplierSiteArr() && self.customsModel.supplierVal() && app.businessUnitArr()) {
                if (event['detail'].value == '' || event['detail'].value == null) {
                    self.customsModel.supplierSiteVal('')
                    if (self.customsModel.supplierVal()) {
                        var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                        if (Array.isArray(suppSite)) {
                            self.customsModel.supplierSiteArr(suppSite.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        } else {
                            self.customsModel.supplierSiteArr([suppSite].map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
                        }
                        self.customsModel.supplierSiteVal(app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).VENDOER_SITE)
                    }
                } else {
                    var bussinesId = app.businessUnitArr().find(e => event['detail'].value == e.businessUnitName).businessUnitId
                    var suppSite = app.supplierSiteArr().find(el => el.VENDOR_NAME == self.customsModel.supplierVal()).Items
                    self.supplierFun(suppSite, bussinesId);
                }
            }
        }

        self.supplierFun = function(suppSite, bussinesId) {
            var suppArr = [];
            if (Array.isArray(suppSite)) {
                suppSite.forEach(e => {
                    if (e.BUSINESSUNIT == bussinesId)
                        suppArr.push({ VENDOER_SITE: e.VENDOER_SITE })
                })
                if (suppArr.length == 0)
                    self.customsModel.supplierSiteArr([])
                else
                    self.customsModel.supplierSiteArr(suppArr.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));

            } else {
                [suppSite].forEach(e => {
                    if (e.BUSINESSUNIT == bussinesId)
                        suppArr.push({ VENDOER_SITE: e.VENDOER_SITE })
                })
                if (suppArr.length == 0)
                    self.customsModel.supplierSiteArr([])

                else
                    self.customsModel.supplierSiteArr(suppArr.map(el => ({ label: el.VENDOER_SITE, value: el.VENDOER_SITE })));
            }
        }

        self.changeDialBtn = function() {
            self.customsModel.rowSelected.INVOICE_RELEASE = self.customsModel.retentionNo()
            filteredData[filteredData.findIndex(el => el.INVOICE_ID == self.customsModel.rowSelected.INVOICE_ID)] = self.customsModel.rowSelected
            self.customsModel.invoiceArr(filteredData)
            document.querySelector("#releaseDialog").close();
            self.dataproviderRetention2(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.invoiceArr, { idAttribute: 'INVOICE_ID' })));
        }

        self.cancelDialBtn = function() {
            document.querySelector("#releaseDialog").close();
        }

        //------------------------------ handle check box in table 

        self.getIndexInSelectedItems = function(id) {

            for (var i = 0; i < self.selectedItems().length; i++) {
                var range = self.selectedItems()[i];
                var startIndex = range.startIndex.row;
                var endIndex = range.endIndex.row;
                if (id >= startIndex && id <= endIndex) {
                    return i;
                }
            }
            return -1;
        };

        self.handleCheckbox = function(id, srN) {
            var isChecked = self.getIndexInSelectedItems(id) != -1 || (srN ? self.customsModel.rowSelectedArr().find(e => e.srN == srN) : false);
            return isChecked ? ['checked'] : [];
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                bussinesUnitLbl: getTransaltion("retentionScreen.bussinesUnitLbl"),
                supplierLbl: getTransaltion("retentionScreen.supplierLbl"),
                supplierSiteLbl: getTransaltion("retentionScreen.supplierSiteLbl"),
                purchaseOrderLbl: getTransaltion("retentionScreen.purchaseOrderLbl"),
                projectNumberLbl: getTransaltion("retentionScreen.projectNumberLbl"),
                invoiceDateLbl: getTransaltion("retentionScreen.invoiceDateLbl"),
                calculateLbl: getTransaltion("retentionScreen.calculateLbl"),
                searchLbl: getTransaltion("common.searchLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                supplierNameLbl: getTransaltion("retentionScreen.supplierNameLbl"),
                invoiceType: getTransaltion("retentionScreen.invoiceType"),
                PONumberLbl: getTransaltion("retentionScreen.PONumberLbl"),
                projectDescriptionLbl: getTransaltion("retentionScreen.projectDescriptionLbl"),
                invoiceAmountLbl: getTransaltion("retentionScreen.invoiceAmountLbl"),
                invoiceNumberLbl: getTransaltion("retentionScreen.invoiceNumberLbl"),
                invoiceDescriptionLbl: getTransaltion("retentionScreen.invoiceDescriptionLbl"),
                retentionPercLbl: getTransaltion("retentionScreen.retentionPercLbl"),
                calculateRetentionLbl: getTransaltion("retentionScreen.calculateRetentionLbl"),
                amountReleaseLbl: getTransaltion("retentionScreen.amountReleaseLbl"),
                creRetScreenLbl: getTransaltion("retentionScreen.creRetScreenLbl"),
                changeAmountLbl: getTransaltion("common.changeAmountLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                paymentTermLbl: getTransaltion("retentionScreen.paymentTermLbl"),

            });

            self.customsModel.columnArray([{
                    headerTemplate: 'headerCheckTemplate',
                    headerText: 'Select',
                    template: 'checkTemplate',
                    sortable: 'disabled'
                },
                {
                    "headerText": "NO.",
                    "template": 'seqTemplate'
                },
                {
                    "headerText": self.label().invoiceNumberLbl,
                    "field": "INVOICE_NUMBER"
                },
                {
                    "headerText": self.label().invoiceType,
                    "field": "INVOICE_TYPE"
                },
                {
                    "headerText": self.label().invoiceAmountLbl,
                    "field": "INVOICE_AMOUNT"
                },
                {
                    "headerText": self.label().invoiceDateLbl,
                    "field": "INVOICE_DATE"
                },
                {
                    "headerText": self.label().invoiceDescriptionLbl,
                    "field": "INVOICE_DESCRIPTION"
                },
                {
                    "headerText": self.label().bussinesUnitLbl,
                    "field": "BUSINESSUNITNAME"
                },
                {
                    "headerText": self.label().supplierNameLbl,
                    "field": "SUPPLIER_NAME"
                },
                {
                    "headerText": self.label().supplierSiteLbl,
                    "field": "SUPPLIER_SITE"
                },
                {
                    "headerText": self.label().PONumberLbl,
                    "field": "PURCHASE_ORDER_NUMBER"
                },
                {
                    "headerText": self.label().projectNumberLbl,
                    "field": "PROJECT_NUMBER"
                },
                {
                    "headerText": self.label().paymentTermLbl,
                    "field": "PAYMENTTERM"
                },
                {
                    "headerText": self.label().projectDescriptionLbl,
                    "field": "PROJECT_DESCRIPTION"
                },
                {
                    "headerText": self.label().amountReleaseLbl,
                    "field": "INVOICE_RELEASE"
                }
            ]);
        }
        initTransaltion();
    }
    return createRetentionContentViewModel;
});