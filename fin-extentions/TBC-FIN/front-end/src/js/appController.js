/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojmodule-element-utils', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessages', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
        'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojdialog'
    ],
    function(oj, ko, $, moduleUtils, app, services, commonhelper) {
        function ControllerViewModel() {
            var self = this;
            const getTranslation = oj.Translations.getTranslatedString;
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);


            self.passwordLabel = ko.observable();


            self.messagesDataProvider = ko.observableArray([]);
            self.supplierSiteArr = ko.observable();
            self.businessUnitArr = ko.observable();
            self.invoicesReportArr = ko.observableArray([]);
            self.debitInvoices = ko.observable();

            self.message = ko.observable();
            self.isUserLoggedIn = ko.observable(false);
            self.Usernametext = ko.observableArray();
            self.loginLabel = ko.observable();
            self.SignIn = ko.observable();
            self.tracker = ko.observable();
            self.forgetPassword = ko.observable();
            self.signoLbl = ko.observable();
            self.passwordLabel = ko.observable();
            self.userName = ko.observable('');
            self.password = ko.observable('');
            self.loginFailureText = ko.observable();
            self.loginLabel = ko.observable();
            self.disOnLogin = ko.observable(false);
            self.languageSwitch_lng = ko.observable();
            self.confirmationTextLbl = ko.observable();
            self.yesLbl = ko.observable();
            self.noActionLbl = ko.observable();
            self.loading = ko.observable(false);
            self.refreshViewForLanguage = ko.observable(false);
            self.language = ko.observable();
            self.confirmLanguageLbl = ko.observable();
            self.notApprovalInvoice = ko.observableArray([]);

            self.jwt = ko.observable('');
            self.JWTExpired = ko.observable(false);
            self.hostUrl = ko.observable('');
            self.loginDateFromSass = ko.observable('');


            self.loginVaild = ko.observable(false);
            //     ----------------------------------------  loading function ------------------------ 
            self.loading = function(load) {
                if (load) {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            };

            //     ---------------------------------------- message function ------------------------ 
            self.createMessage = function(type, summary) {
                return {
                    severity: type,
                    summary: summary,
                    autoTimeout: 9000
                };
            };

            //     ---------------------------------------- supplier function ------------------------ 
            self.supplierSite = function() {

                var supplierSiteFun = function(data) {
                    self.supplierSiteArr(data);
                }

                var supplierSiteFunError = function() {}
                services.getGeneric(commonhelper.supplierSite).then(supplierSiteFun, supplierSiteFunError)
            }

            //     ---------------------------------------- business unit function ------------------------ 
            self.businessUnit = function() {

                var BUFun = function(data) {
                    self.businessUnitArr(data);
                }

                var BUFunError = function() {}
                services.getGeneric(commonhelper.businessUnit).then(BUFun, BUFunError)
            }

            //     ---------------------------------------- get all invoice function ------------------------ 
            self.allInvoicesreport = function() {
                var invoicesFun = function(data) {
                    self.notApprovalInvoice(data);
                    data = data.filter(e => e.INVOICE_APPROVAL_STATUS == "MANUALLY APPROVED" || e.INVOICE_APPROVAL_STATUS == "WFAPPROVED")
                    data.forEach((el) => {
                        el.INVOICE_DATE = el.INVOICE_DATE.split('T')[0]
                        el.INVOICE_NUMBER = el.INVOICE_NUMBER.split('_')[0]
                        if (el.RETENTION_PERCANTGE)
                            el.RETENTION_PERCANTGE = el.RETENTION_PERCANTGE.split('%')[0]

                        if (el.lines && Array.isArray(el.lines)) {
                            el.lines.forEach((e, index) => {
                                el.INVOICE_AMOUNT_WITHOUT_TAX = numberWithCommas(el.lines.map(x => x.AMOUNT).reduce((y, z) => y + z))
                                if (el.INVOICE_TYPE == "STANDARD") {
                                    if (e.PROJECT_NUMBER) {
                                        el.PROJECT_NUMBER = e.PROJECT_NUMBER.split('_')[0],
                                            el.PROJECT_DESCRIPTION = e.PROJECT_DESCRIPTION
                                    }
                                } else if (el.INVOICE_TYPE == "DEBIT") {
                                    if (e.PROJECT_NUMBER_DFF) {
                                        el.PROJECT_NUMBER = e.PROJECT_NUMBER_DFF.split('_')[0]
                                        el.PROJECT_DESCRIPTION = e.PROJECT_DESCRIPTION_DFF
                                    }
                                }
                            })
                        } else if (el.lines && !Array.isArray(el.lines)) {
                            [el.lines].forEach((e, index) => {
                                el.INVOICE_AMOUNT_WITHOUT_TAX = numberWithCommas([el.lines].map(x => x.AMOUNT).reduce((y, z) => y + z))
                                if (el.INVOICE_TYPE == "STANDARD") {
                                    if (e.PROJECT_NUMBER) {
                                        el.PROJECT_NUMBER = e.PROJECT_NUMBER.split('_')[0],
                                            el.PROJECT_DESCRIPTION = e.PROJECT_DESCRIPTION
                                    }
                                } else if (el.INVOICE_TYPE == "DEBIT") {
                                    if (e.PROJECT_NUMBER_DFF) {
                                        el.PROJECT_NUMBER = e.PROJECT_NUMBER_DFF.split('_')[0]
                                        el.PROJECT_DESCRIPTION = e.PROJECT_DESCRIPTION_DFF
                                    }
                                }
                            })
                        }
                    })
                    var srN = 0;
                    data.forEach(el => el.srN = ++srN)
                    var y = data.filter(el => el.RETENTION_STATUS != "Debit" && el.INVOICE_TYPE != "DEBIT")
                    self.invoicesReportArr(y);
                    var x = data.filter(el => el.INVOICE_TYPE == "DEBIT" && el.INVOICE_NUMBER.toString().split('-')[0] == "RET" && el.RETENTION_STATUS != "Debit");
                    self.debitInvoices(x)
                    self.debitInvoices().forEach(el => el.INVOICE_RELEASE = el.INVOICE_AMOUNT * -1)
                    self.loading(false);
                }
                var invoicesFunError = function() {
                    self.loading(false);
                }
                services.getGeneric(commonhelper.allInvoicesreport).then(invoicesFun, invoicesFunError)
            }

            self.homeBtnAction = function() {
                oj.Router.rootInstance.go('retentionScreen');
            }

            self.retBtnAction = function() {
                oj.Router.rootInstance.go('createRetentionScreen');
            }

            //     ---------------------------------------- enter to login  ------------------------ 
            $(function() {
                $('#username').on('keydown', function(ev) {
                    var mobEvent = ev;
                    var mobPressed = mobEvent.keyCode || mobEvent.which;
                    if (mobPressed == 13) {
                        $('#username').blur();
                        $('#password').focus();
                    }
                    return true;
                });
            });

            //     ---------------------------------------- number format  ------------------------ 
            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            //     ---------------------------------------- login button function------------------------ 
            self.onLogin = function() {
                if (!self.userName() || !self.password())
                    return;

                self.disOnLogin(true);
                $(".apLoginBtn").addClass("loading");
                self.loginFailureText("");

                var loginSuccessCbFn = function() {
                    self.supplierSite();
                    self.businessUnit();
                    self.allInvoicesreport();
                    self.loading(false);
                    self.messagesDataProvider([]);
                    self.isUserLoggedIn(true);
                };

                var loginFailCbFn = function() {
                    self.loading(false);
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                    self.isUserLoggedIn(false);
                    self.loginFailureText("error");
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                };

                function authinticate(data) {
                    var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));
                    var result = parsedData.result;
                    if (result) {
                        loginSuccessCbFn();
                    } else {
                        loginFailCbFn();
                    }
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                }

                var payload = {
                    "userName": self.userName(),
                    "password": self.password()
                };

                services.authenticate(payload).then(authinticate, self.failCbFn);
                $(".apLoginBtn").removeClass("loading");
            };

            //     ---------------------------------------- log out button function------------------------ 
            self.logOutBtn = function(event) {
                self.isUserLoggedIn(false);
                if (self.language() === 'english') {
                    $('html').attr({ 'dir': 'ltr' });
                } else if (self.language() === 'arabic') {
                    $('html').attr({ 'dir': 'rtl' });
                }
            };

            //     ---------------------------------------- Router setup function------------------------ 
            // Router setup
            self.router = oj.Router.rootInstance;

            self.router.configure({
                'retentionScreen': { label: 'retentionScreen', value: "retentionScreen/retentionScreen", id: "retentionScreen", title: "TBC - Invoices", isDefault: true },
                'createRetentionScreen': { label: 'createRetentionScreen', value: "retentionScreen/createRetentionScreen", id: "createRetentionScreen", title: "TBC - Retention" },
            });
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

            self.switchl = function() {
                document.querySelector("#switch").open();
            };
            self.switchclose = function() {
                document.querySelector("#switch").close();
            };
            self.switchLanguage = function() {
                document.querySelector("#switch").close();
                if (self.getLocale() === "ar") {
                    self.setLocale("en-US");
                    localStorage.setItem("selectedLanguage", "en-US");
                } else if (self.getLocale() === "en-US") {
                    self.setLocale("ar");
                    localStorage.setItem("selectedLanguage", "ar");
                }
            };

            self._showComponentValidationErrors = function(trackerObj) {
                if (trackerObj !== undefined) {
                    trackerObj.showMessages();
                    if (trackerObj.focusOnFirstInvalid())
                        return false;
                }
                return true;
            };

            self.moduleConfig = ko.observable({ 'view': [], 'viewModel': null });

            self.loadModule = async function() {
                ko.computed(function() {
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    var masterPromise = Promise.all([
                        moduleUtils.createView({ 'viewPath': viewPath }),
                        moduleUtils.createViewModel({ 'viewModelPath': modelPath })
                    ]);
                    masterPromise.then(
                        function(values) {
                            self.moduleConfig({ 'view': values[0], 'viewModel': values[1] });
                        },
                        function(reason) {}
                    );
                });
            };

            self.getLocale = function() {
                return oj.Config.getLocale();

            };
            self.setLocale = function(lang) {
                oj.Config.setLocale(lang,
                    function() {
                        self.refreshViewForLanguage(false);
                        if (lang === 'ar') {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "rtl");
                        } else {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "ltr");
                        }
                        initTranslations();
                        self.refreshViewForLanguage(true);
                    }
                );
            };

            var newFunction = function() {
                if (localStorage.getItem("jwt") != null) {
                    jwtJSON = jQuery.parseJSON(atob(localStorage.getItem("jwt").split('.')[1]));
                    isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;
                    if (!isJWTExpred) {
                        self.isUserLoggedIn(true)
                        self.supplierSite();
                        self.businessUnit();
                        self.allInvoicesreport();
                        self.disOnLogin(false);
                        self.router.go('retentionScreen');
                    } else {
                        $.when(showEmplDetails(self)).done(function() {
                            if (self.loginVaild()) {
                                self.isUserLoggedIn(true)
                                self.supplierSite();
                                self.businessUnit();
                                self.allInvoicesreport();
                                self.disOnLogin(false);
                                self.router.go('retentionScreen');
                            } else {
                                self.disOnLogin(false);
                                self.isUserLoggedIn(false);
                            }
                        });
                    }

                } else {
                    $.when(showEmplDetails(self)).done(function() {
                        if (self.loginVaild()) {
                            self.isUserLoggedIn(true)
                            self.supplierSite();
                            self.businessUnit();
                            self.allInvoicesreport();
                            self.disOnLogin(false);
                            self.router.go('retentionScreen');
                        } else {
                            self.disOnLogin(false);
                            self.isUserLoggedIn(false);
                        }
                    });
                }
            };

            newFunction();

            function initTranslations() {
                self.Usernametext(getTranslation("login.userName"));
                self.passwordLabel(getTranslation("login.Password"));
                self.loginLabel(getTranslation("login.loginLabel"));
                self.SignIn(getTranslation("login.SignIn"));
                self.forgetPassword(getTranslation("login.forgetPassword"));
                self.confirmLanguageLbl(getTranslation("common.confirmLanguageLbl"));
                self.signoLbl(getTranslation("labels.signOut"));
                if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                }
                self.languageSwitch_lng(getTranslation("common.switchLang"));
                self.confirmationTextLbl(getTranslation("common.confirmationTextLbl"));
                self.yesLbl(getTranslation("common.yesLbl"));
                self.noActionLbl(getTranslation("common.noActionLbl"));
            }
            initTranslations();
        }
        return new ControllerViewModel();
    }
);