package com.appspro.tbcRevenue.dao;

import biPReports.RestHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;

public class CreateRevenueDao extends RestHelper {

    RestHelper rs = new RestHelper();

    public JSONObject createProjectBillingEventDu(JSONArray eventDetails) {
        JSONObject resObj = new JSONObject();
        JSONObject res = new JSONObject();
        try {
            for (int i = 0; i < eventDetails.length(); i++) {
                JSONObject event = eventDetails.getJSONObject(i);

                JSONObject data = event.getJSONObject("data");
                String destUrl = getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/projectBillingEvents";                  
                res  = rs.callRest(destUrl, "POST", data.toString());
                String EventId = res.getJSONObject("data").get("EventId").toString();                 
                String receiptNum = event.getString("receiptNumber");
                String receiptPercentage = event.getString("receiptPercentage");
             
                
                JSONObject DFFObj = new JSONObject();
                DFFObj.put("receiptNumber", receiptNum);
                DFFObj.put("eventStatus", "Created");
//                DFFObj.put("receiptPercentage", receiptPercentage);
    
                String DffUrl = destUrl + "/" + EventId + "/child/billingEventDFF";
                JSONObject dffResp = rs.callRest(DffUrl, "POST", DFFObj.toString());

            }
            resObj.put("Status", "OK");
            resObj.put("Data", res);
      
        } catch (Exception e) {
            e.printStackTrace();
            resObj.put("Status", "ERROR");
            resObj.put("Data",res);
        }
        return resObj;
    }
    public JSONObject createProjectBillingEvent(JSONObject eventDetails) {
        JSONObject resObj = new JSONObject();

        try {
            String destUrl = getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/projectBillingEvents";
            JSONObject res = rs.callRest(destUrl, "POST", eventDetails.toString());
          
            resObj.put("Status", "OK");
            resObj.put("Data", res);

        } catch (Exception e) {
            e.printStackTrace();
            resObj.put("Status", "ERROR");
            resObj.put("Data", e.getMessage());
        }

        return resObj;
    }
    
    public JSONObject updateEventDff(JSONObject eventDetailsDff) {
        JSONObject DFFObj = new JSONObject();
        JSONObject resObj = new JSONObject();
//        String ob = eventDetails.get("data").toString();

        try {
            String EventId = eventDetailsDff.get("eventId").toString();
            String destUrl = getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/projectBillingEvents";


            DFFObj.put("receiptNumber", eventDetailsDff.get("receiptNumber").toString());
            DFFObj.putOpt("eventStatus", "Created");

            String DffUrl = destUrl + "/" + EventId + "/child/billingEventDFF";

            JSONObject dffres = rs.callRest(DffUrl, "POST", DFFObj.toString());

            resObj.put("Status", "OK");
            resObj.put("Data", dffres);

        } catch (Exception e) {
            e.printStackTrace();
            resObj.put("Status", "ERROR");
            resObj.put("Data", e.getMessage());
        }

        return resObj;
    }
    
    

}
