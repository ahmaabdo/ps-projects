package biPReports;


import com.appspro.db.CommonConfigReader;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.ws.rs.core.MediaType;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;


public class RestHelper {
    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private String employeeServiceUrl = "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private String AllEmployeesUrl = "/hcmRestApi/resources/11.13.18.05/emps?expand=assignments";
    private String bussinesUnitUrl = "/fscmRestApi/resources/11.13.18.05/finBusinessUnitsLOV";

    public static String Schema_Name = CommonConfigReader.getValue("Schema_Name");


    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public Document httpPost(String destUrl, String postData) throws Exception {
        
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
     
        java.net.URL url = new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String SOAPAction = "";
        connection.setRequestProperty("Content-Length", String.valueOf(b.length));
        connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        connection.setRequestProperty("SOAPAction", SOAPAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth());
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        OutputStream out = connection.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + connection.getResponseCode() + "; connection response: " + connection.getResponseMessage());

        if (connection.getResponseCode() == 200) {
            InputStream in = connection.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in,"UTF-8");
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));
            
            XML.toJSONObject(response).toString();
            
            iReader.close();
            bReader.close();
            in.close();
            connection.disconnect();
            return doc;
        } 
        else {
            System.out.println("error"+connection.getResponseMessage());
            InputStream in = connection.getErrorStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }
            System.out.println("error2"+connection.getErrorStream());
            System.out.println("Failed"+response);
            if (response.indexOf("<?xml") > 0)
                response =response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +15);
            
            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("UTF-8"))));
            
            XML.toJSONObject(response).toString();
            
            iReader.close();
            bReader.close();
            in.close();
            connection.disconnect();
            
            return doc;
            
        }
    }
    
    public JSONObject httpGet (String restUrl) throws MalformedURLException, IOException {
       
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        
        URL url = new URL(null,restUrl,new sun.net.www.protocol.https.Handler());

        if(url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            https = (HttpsURLConnection)url.openConnection();
            connection = https;
        }
        else{
            connection = (HttpsURLConnection)url.openConnection();
        }
        
        String soapAction = null ;
        connection.setRequestMethod("GET");
        connection.setReadTimeout(6000000);
        connection.setRequestProperty("content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("SoapAction", soapAction);
        connection.setRequestProperty("Authorization", "Basic " + getAuth());
       
        BufferedReader in = new BufferedReader( new InputStreamReader (connection.getInputStream(),"utf-8"));
        String input = null;
        StringBuffer response = new StringBuffer();
        
        while((input = in.readLine()) != null) {
            response.append(input);
        }
        JSONObject obj = new JSONObject(response.toString());
       
        return obj; 
    }

    
    public static String getAuth() {
        byte[] message = (getUserName() + ":" + getPassword()).getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }
    
    public JSONObject callRest(String serverUrl, String method, String body) {
        JSONObject res = new JSONObject();
        try {
            System.out.println("calling ::" + serverUrl);
            System.out.println("method ::" + method);
            System.out.println("payload ::" + body);

            HttpsURLConnection https = null;
            HttpURLConnection connection = null;

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);


            if (method.equalsIgnoreCase("PATCH")) {
                connection.setRequestProperty("X-HTTP-Method-Override",
                                              "PATCH");
                connection.setRequestMethod("POST");
            } else {
                connection.setRequestMethod(method);
            }


            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());

            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            
            }
            int code = connection.getResponseCode();


            InputStream is;
            if (code >= 400) {
                is = connection.getErrorStream();
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
                
            }
            System.out.println(code);
            System.out.println(is);
            StringBuffer response = new StringBuffer();

            if(is != null){
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            System.out.println(response.toString());
            in.close();
            }
            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                e.printStackTrace();
                res.put("data", response.toString());
            }

            System.out.print("code:" + code);
            if (code >= 400)
                System.out.println(",Error:" + res.get("data").toString());
        
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", "Internal server error");
        }

        return res;
    }

    public String getInstanceUrl() {
        String url="";
        if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            url= CommonConfigReader.getValue("DEV_InstanceUrl");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            url= CommonConfigReader.getValue("PROD_InstanceUrl");
        }
        return url;
    }
    
    public String getInstanceName() {
        String name="";
        if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            name= CommonConfigReader.getValue("DEV_instanceName");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            name= CommonConfigReader.getValue("PROD_instanceName");
        }
        return name;
    }
    
    public static String getUserName() {
        String userName="";
        if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            userName= CommonConfigReader.getValue("DEV_USER_NAME");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            userName= CommonConfigReader.getValue("PROD_USER_NAME");
        }
        return userName;
    }
    
    public static String getPassword() {
        String password="";
        if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            password= CommonConfigReader.getValue("DEV_PASSWORD");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            password= CommonConfigReader.getValue("PROD_PASSWORD");
        }
        return password;
    }
    
    public String getBiReportUrl() {
        String biReportUrl="";
        if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            biReportUrl = CommonConfigReader.getValue("DEV_biReportUrl");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            biReportUrl = CommonConfigReader.getValue("PROD_biReportUrl");
        }
        return biReportUrl;
    }
    
    public String getSecurityService() {
        String securityService="";
        if ("dev".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            securityService = CommonConfigReader.getValue("DEV_SecurityService");
        } else if ("prod".equalsIgnoreCase(CommonConfigReader.getValue("DEP-TYPE"))) {
            securityService = CommonConfigReader.getValue("PROD_SecurityService");
        }
        return securityService;
    }
    
    public static String getSchema_Name() {
        return Schema_Name;
    }

    public String getEmployeeRestEndPointUrl() {
        return employeeServiceUrl;
    }

    public void setAllEmployeesUrl(String AllEmployeesUrl) {
        this.AllEmployeesUrl = AllEmployeesUrl;
    }

    public String getAllEmployeesUrl() {
        return AllEmployeesUrl;
    }

    public void setBussinesUnitUrl(String bussinesUnitUrl) {
        this.bussinesUnitUrl = bussinesUnitUrl;
    }

    public String getBussinesUnitUrl() {
        return bussinesUnitUrl;
    }
}
