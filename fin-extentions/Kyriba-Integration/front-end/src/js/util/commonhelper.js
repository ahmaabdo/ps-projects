define([], function() {

    function commonHelper() {
        var self = this;

        self.getBiReportServletPath = function() {
            var host = "report/commonbireport";
            return host;
        };

        self.getAllContarcts = "report/getAllContarcts";
        self.createEvent = "projectBillingEvent/createEvent";
        self.updateEventDff = "projectBillingEvent/update";


        self.getInternalRest = function() {
             //TBC-REV-BACK-PROD 
            if (window.location.hostname == "digital.tbc.sa") {

                var host = "https://digital.tbc.sa/TBC-FIN-REV-BACKEND-TEST/webapi/";

            } else {

                var host = " http://127.0.0.1:7101/TBC-FIN-REV-BACKEND-TEST/webapi/";
            }

            return host;
        };

    }

    return new commonHelper();
});