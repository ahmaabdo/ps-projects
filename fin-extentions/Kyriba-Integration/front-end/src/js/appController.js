/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'appController', 'jquery','ojs/ojrouter', 'config/services', 'util/commonhelper', 'jquery', 'ojs/ojmessages', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas', 'ojs/ojbutton', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojdialog'
],
    function (oj, ko, moduleUtils, app, $, Router, services, commonhelper, $) {
        function ControllerViewModel() {
            var self = this;
            var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
            var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
            const getTranslation = oj.Translations.getTranslatedString;

            self.username = ko.observable();
            self.password = ko.observable();
            self.userLogin = ko.observable();
            self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
            self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
            self.loginUser = ko.observable();
            self.messagesDataProvider = ko.observableArray([]);
            self.loginBtnLbl = ko.observable("LOGIN");
            self.personDetails = ko.observableArray([]);
            self.localeLogin = ko.observable();
            self.UsernameLabel = ko.observable();
            self.passwordLabel = ko.observable();
            self.loginword = ko.observable();
            self.forgotPasswordLabel = ko.observable();
            self.supplierArr = ko.observable();
            self.supplierSiteArr = ko.observable();
            self.businessUnitArr = ko.observable();
            self.invoicesArr = ko.observable();
            self.invoicesReportArr = ko.observable();
            self.message = ko.observable();
            self.isUserLoggedIn = ko.observable(false);
            self.Usernametext = ko.observableArray();
            self.SignIn = ko.observable();
            self.tracker = ko.observable();
            self.forgetPassword = ko.observable();
            self.signoLbl = ko.observable();
            self.changeLbl = ko.observable();
            self.passwordLabel = ko.observable();
            self.userName = ko.observable('');
            self.password = ko.observable('');
            self.loginFailureText = ko.observable();
            self.loginLabel = ko.observable();
            self.disOnLogin = ko.observable(false);
            self.languageSwitch_lng = ko.observable();
            self.confirmMessage = ko.observable();
            self.yes = ko.observable();
            self.no = ko.observable();
            self.loading = ko.observable(false);
            self.refreshViewForLanguage = ko.observable(false);
            self.showDashboard = ko.observable();
            self.language = ko.observable();
            self.languagelable = ko.observable("English");
            self.yesLbl = ko.observable();
            self.noActionLbl = ko.observable();
            self.confirmLanguageLbl = ko.observable();
            self.confirmationTextLbl = ko.observable();

            self.jwt = ko.observable('');
            self.hostUrl = ko.observable('');
            self.userName = ko.observable('');
            self.jwtinurl = ko.observable(false);
            self.isUserLoggedIn = ko.observable(false);
            self.contractsArr = ko.observableArray([]);
            self.loginVaild = ko.observable(false);
            self.loginDateFromSass = ko.observable('');
            //     ----------------------------------------  loading function ------------------------ 

            self.loading = function (load) {
                if (load) {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            };

            //     ---------------------------------------- message function ------------------------ 

            self.createMessage = function (type, summary) {
                return {
                    severity: type,
                    summary: summary,
                    autoTimeout: 9000
                };
            };
            //     ---------------------------------------- get all invoice function ------------------------ 
            self.getAllContarcts = function () {
                self.loading(true);
                var contractsFun = function (data) {
                    data.G_1.forEach(el => el.date = new Date(el.REVENUE_DATE))
                    data.G_1.sort((a, b) => b.date - a.date) //sort events with date
                    var srNo = 0;
                    data.G_1.forEach(el => {
                        el.srNo = ++srNo,
                            el.REVENUE_DATE = el.REVENUE_DATE.toString().split('T')[0],
                            el.PROJECT_NUMBER = el.PROJECT_NUMBER.split('_')[0],
                            el.TASK_NUMBER = el.TASK_NUMBER.split('_')[0]
                        if (Array.isArray(el.G_2)) { 
                            el.CONTRACT_NUMBER = el.G_2[0].CONTRACT_NUMBER.split('_')[0]
                            el.EVENT_PERCENTAGE = el.G_2[0].RECEIPT_PERCENTAGE
                            var x = el.G_2.find(el => el.EVENT_STATUS == 'Created');
                            if (x) {
                                el.REVENUE_DATE = x.EVENT_DATE
                                el.EVENT_STATUS = x.EVENT_STATUS
                                el.LINE_NUMBER = x.LINE_NUMBER
                            }
                            else {
                                el.EVENT_STATUS = 'Not Created'
                            }
                        } else {
                            el.CONTRACT_NUMBER = [el.G_2][0].CONTRACT_NUMBER.split('_')[0]
                            el.EVENT_PERCENTAGE = [el.G_2][0].RECEIPT_PERCENTAGE
                            el.EVENT_STATUS = [el.G_2][0].EVENT_STATUS
                            if ([el.G_2][0].EVENT_STATUS == "Created")
                                el.REVENUE_DATE = [el.G_2][0].EVENT_DATE
                        }
                    })
                    self.contractsArr(data.G_1);
                    self.loading(false);
                };

                var contractsFunError = function () {
                    self.loading(false);
                };
                services.getGeneric(commonhelper.getAllContarcts).then(contractsFun, contractsFunError);
            };
            //     ---------------------------------------- home icon function------------------------ 

            $(document).on('click', '.home-icon', function () {
                oj.Router.rootInstance.go('');
            });

            //     ---------------------------------------- notification icon function ------------------------ 

            $(document).on('click', '.notifiIcon', function () {
                oj.Router.rootInstance.go('');
            });

            //     ---------------------------------------- enter to login  ------------------------ 

            $(function () {
                $('#username').on('keydown', function (ev) {
                    var mobEvent = ev;
                    var mobPressed = mobEvent.keyCode || mobEvent.which;
                    if (mobPressed == 13) {
                        $('#username').blur();
                        $('#password').focus();
                    }
                    return true;
                });
            });

            //     ---------------------------------------- login button function------------------------ 

            self.onLogin = function () {
                if (!self.userName() || !self.password())
                    return;

                self.disOnLogin(true);
                $(".apLoginBtn").addClass("loading");
                self.loginFailureText("");

                var loginSuccessCbFn = function () {
                    self.loading(false);
                    self.messagesDataProvider([]);
                    self.isUserLoggedIn(true);
                    self.getAllContarcts();
                };

                var loginFailCbFn = function () {
                    self.loading(false);
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                    self.isUserLoggedIn(false);
                    self.loginFailureText("error");
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                };

                function authinticate(data) {
                    var parsedData = jQuery.parseJSON(data.replace('\'', '"').replace('\'', '"'));
                    var result = parsedData.result;
                    if (result) {
                        loginSuccessCbFn();
                    } else {
                        loginFailCbFn();
                    }
                    $(".apLoginBtn").removeClass("loading");
                    self.disOnLogin(false);
                }

                var payload = {
                    "userName": self.userName(),
                    "password": self.password()
                };

                services.authenticate(payload).then(authinticate, self.failCbFn);
                $(".apLoginBtn").removeClass("loading");
            };

            //     ---------------------------------------- log out button function------------------------ 

            self.logOutBtn = function (event) {
                // location.reload();
                self.isUserLoggedIn(false);
                sessionStorage.setItem('username', '');
                sessionStorage.setItem('isUserLoggedIn', '');

                if (self.language() === 'english') {
                    $('html').attr({ 'dir': 'ltr' });
                } else if (self.language() === 'arabic') {
                    $('html').attr({ 'dir': 'rtl' });
                }
            };

            //     ---------------------------------------- Router setup function------------------------ 
            // Router setup
            self.router = oj.Router.rootInstance;
            self.router.dispose();

            self.router.configure({
                'Home': { label: 'Home', value: "Home/homeScreen", id: "Home", title: "Home" , isDefault: true},
                'KyribaInterface': { label: 'KyribaInterface', value: "KyribaScreen/KyribaInterface", id: "KyribaInterface", title: "Kyriba"},
                'Audit': { label: 'Audit', value: "KyribaScreen/Audit", id: "Audit", title: "Audit"},
                'createRetentionScreen': { label: 'createRevenueScreen', value: "RevenueScreen/createRevenueScreen", id: "createRevenueScreen", title: "TBC - Revenue" }
            });
            oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();


            self.langSelected = function (event) {
                var valueObj = buildValueChange(event['detail']);
                self.setPreferedLanguage(valueObj.value);
            };

            function buildValueChange(valueParam) {
                var valueObj = {};
                if (valueParam.previousValue) {
                    valueObj.value = valueParam.value;
                } else {
                    valueObj.value = valueParam.previousValue;
                }
                return valueObj;
            }

            self.switchl = function () {
                document.querySelector("#switch").open();
            };
            self.switchclose = function () {
                document.querySelector("#switch").close();
            };

            self.getLocale = function () {
                return oj.Config.getLocale();
            };

            self.setLocale = function (lang) {
                oj.Config.setLocale(lang,
                    function () {
                        self.refreshViewForLanguage(false);
                        if (lang === 'ar') {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "rtl");
                        } else {
                            $("html").attr("lang", lang);
                            $("html").attr("dir", "ltr");
                        }
                        initTranslations();
                        self.refreshViewForLanguage(true);
                    }
                );
            };

            self.switchLanguage = function () {
                document.querySelector("#switch").close();
                if (self.getLocale() === "ar") {
                    self.setLocale("en-US");
                    localStorage.setItem("selectedLanguage", "en-US");
                } else if (self.getLocale() === "en-US") {
                    self.setLocale("ar");
                    localStorage.setItem("selectedLanguage", "ar");
                }
            };

            self.moduleConfig = ko.observable({ 'view': [], 'viewModel': null });

            self.loadModule = async function () {
                ko.computed(function () {
                    var name = self.router.moduleConfig.name();
                    var viewPath = 'views/' + name + '.html';
                    var modelPath = 'viewModels/' + name;
                    var masterPromise = Promise.all([
                        moduleUtils.createView({ 'viewPath': viewPath }),
                        moduleUtils.createViewModel({ 'viewModelPath': modelPath })
                    ]);
                    masterPromise.then(
                        function (values) {
                            self.moduleConfig({ 'view': values[0], 'viewModel': values[1] });
                        },
                        function (reason) { }
                    );
                });
            };

            var newFunction = function () {
                $.when(showEmplDetails(self)).done(function () {
                    if (self.loginVaild()) {
                        self.getAllContarcts();
                        self.isUserLoggedIn(true)
                        self.disOnLogin(false);
                    } else {
                        self.disOnLogin(false);
                        self.isUserLoggedIn(false);
                    }
                });
            };

            newFunction();

            function initTranslations() {
                self.Usernametext(getTranslation("login.userName"));
                self.passwordLabel(getTranslation("login.Password"));
                self.loginLabel(getTranslation("login.loginLabel"));
                self.SignIn(getTranslation("login.SignIn"));
                self.forgetPassword(getTranslation("login.forgetPassword"));
                self.changeLbl(getTranslation("labels.changeLang"));
                self.signoLbl(getTranslation("labels.signOut"));
                if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                    self.loginFailureText(getTranslation("login.loginFailureText"));
                }
                self.languageSwitch_lng(getTranslation("common.switchLang"));
                self.confirmMessage(getTranslation("labels.confirmMessage"));
                self.yesLbl(getTranslation("common.yesLbl"));
                self.noActionLbl(getTranslation("common.noActionLbl"));
                self.confirmationTextLbl(getTranslation("common.confirmationTextLbl"));
                self.confirmLanguageLbl(getTranslation("common.confirmLanguageLbl"));
            }
            initTranslations();
        }

        return new ControllerViewModel();
    }
);