define(['ojs/ojcore', 'knockout', 'jquery','ojs/ojrouter', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function (oj, ko, $, Router, app, services, commonhelper) {
    function AuditContentViewModel() {
        var self = this;
        self.label = ko.observable();
        self.KyribaInterfaceLbl = ko.observable()
        self.agreement = ko.observableArray();
        self.customsModel = {
          
        };
        backBtn = function(){
            app.router.go('Home')
        }
        /********************************************** */


        ko.computed(function () {

        });

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                audit: getTransaltion("audit.audit"),
                back: getTransaltion("audit.back"),
                save: getTransaltion("audit.save"),
                payment: getTransaltion("audit.payment"),
                editPayment: getTransaltion("audit.editPayment"),
                paymentStatus: getTransaltion("audit.paymentStatus"),
                comments: getTransaltion("audit.comments"),
                createdBy: getTransaltion("audit.createdBy"),
                creationDate: getTransaltion("audit.creationDate"),
                lastUpdatedBy: getTransaltion("audit.lastUpdatedBy"),
                paymentInformation: getTransaltion("audit.paymentInformation"),
                transactionCode: getTransaltion("audit.transactionCode"),
                benificaryBankAccountNumber: getTransaltion("audit.benificaryBankAccountNumber"),
                reference: getTransaltion("audit.reference"),
                benificaryBankAccountCurrency: getTransaltion("audit.benificaryBankAccountCurrency"),
                debitAccount: getTransaltion("audit.debitAccount"),
                transactionCurrency: getTransaltion("audit.transactionCurrency"),
                benificaryName: getTransaltion("audit.benificaryName"),
                transactionAmount: getTransaltion("audit.transactionAmount"),
                benificaryAddress_1: getTransaltion("audit.benificaryAddress_1"),
                transactionDate: getTransaltion("audit.transactionDate"),
                benificaryAddress_2: getTransaltion("audit.benificaryAddress_2"),
                reason_1: getTransaltion("audit.reason_1"),
                reason_2: getTransaltion("audit.reason_2"),
                benificaryCity: getTransaltion("audit.benificaryCity"),
                benificaryState_Province: getTransaltion("audit.benificaryState_Province"),
                onBehalfTo: getTransaltion("audit.onBehalfTo"),
                benificaryZipCode: getTransaltion("audit.benificaryZipCode"),
                budgetCode: getTransaltion("audit.budgetCode"),
                benificaryCountryCode: getTransaltion("audit.benificaryCountryCode"),
                correspondentCountryCode: getTransaltion("audit.correspondentCountryCode"),
                benificaryBankName: getTransaltion("audit.benificaryBankName"),
                ERPApprover: getTransaltion("audit.ERPApprover"),
                benificaryBankAddress_1: getTransaltion("audit.benificaryBankAddress_1"),
                currencyBuyFlag: getTransaltion("audit.currencyBuyFlag"),
                benificaryBankAddress_2: getTransaltion("audit.benificaryBankAddress_2"),
                currencyBuyReference: getTransaltion("audit.currencyBuyReference"),
                benificaryBankCity: getTransaltion("audit.benificaryBankCity"),
                currencyBuyDate: getTransaltion("audit.currencyBuyDate"),
                benificaryBankState_Province: getTransaltion("audit.benificaryBankState_Province"),
                purpose: getTransaltion("audit.purpose"),
                benificaryBankZipCode: getTransaltion("audit.benificaryBankZipCode"),
                benificaryBankACHCode: getTransaltion("audit.benificaryBankACHCode"),
                benificaryBankCountryCode: getTransaltion("audit.benificaryBankCountryCode"),
                benificaryCorporateIdCode: getTransaltion("audit.benificaryCorporateIdCode"),
                benificaryBankBIC: getTransaltion("audit.benificaryBankBIC"),
                benificaryTpIdentifierType: getTransaltion("audit.benificaryTpIdentifierType"),
                correspondentBankName: getTransaltion("audit.correspondentBankName"),
                benificaryTpIdentifierValue: getTransaltion("audit.benificaryTpIdentifierValue"),
                correspondentBankBIC: getTransaltion("audit.correspondentBankBIC")
            });

        }
        initTransaltion();
    }

    return AuditContentViewModel;
});