define(['ojs/ojcore', 'knockout', 'jquery','ojs/ojrouter', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function (oj, ko, $, Router, app, services, commonhelper) {
    function KyribaInterfaceContentViewModel() {
        var self = this;
        self.label = ko.observable();
        self.KyribaInterfaceLbl = ko.observable()
        self.customsModel = {
          
        };
        backBtn = function(){
            app.router.go('Home')
        }
        /********************************************** */


        ko.computed(function () {

        });

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                KyribaInterfaceLbl: getTransaltion("KyribaScreen.KyribaInterfaceLbl"),
                back: getTransaltion("KyribaScreen.back"),
                save: getTransaltion("KyribaScreen.save"),
                transactionCode: getTransaltion("KyribaScreen.transactionCode"),
                budgetCode: getTransaltion("KyribaScreen.budgetCode"),
                purpose: getTransaltion("KyribaScreen.purpose"),
                currencyBuyFlag: getTransaltion("KyribaScreen.currencyBuyFlag"),
                currencyBuyReference: getTransaltion("KyribaScreen.currencyBuyReference"),
                currencyBuyDate: getTransaltion("KyribaScreen.currencyBuyDate"),
                beneficaryBankACHCode: getTransaltion("KyribaScreen.beneficaryBankACHCode"),
                beneficaryCorporateIDCode: getTransaltion("KyribaScreen.beneficaryCorporateIDCode"),
                beneficaryTPIdentifierType: getTransaltion("KyribaScreen.beneficaryTPIdentifierType"),
                beneficaryTPIdentifierValue: getTransaltion("KyribaScreen.beneficaryTPIdentifierValue"),
                reason1: getTransaltion("KyribaScreen.reason1"),
                reason2: getTransaltion("KyribaScreen.reason2"),
                tpAccountIdentifierType: getTransaltion("KyribaScreen.tpAccountIdentifierType"),
                paymentDetails: getTransaltion("KyribaScreen.paymentDetails"),
                debitAccount: getTransaltion("KyribaScreen.debitAccount"),
                amount: getTransaltion("KyribaScreen.amount"),
                currency: getTransaltion("KyribaScreen.currency"),
                date: getTransaltion("KyribaScreen.date"),
                bankingDetails: getTransaltion("KyribaScreen.bankingDetails"),
                bankCode: getTransaltion("KyribaScreen.bankCode"),
                details: getTransaltion("KyribaScreen.details"),
                sambaPOR: getTransaltion("KyribaScreen.sambaPOR"),
                saabCodes: getTransaltion("KyribaScreen.saabCodes"),
                centralBankPurposes: getTransaltion("KyribaScreen.centralBankPurposes"),
                ftsFABCode: getTransaltion("KyribaScreen.ftsFABCode"),
                freeText_1: getTransaltion("KyribaScreen.freeText_1"),
                freeText_2: getTransaltion("KyribaScreen.freeText_2"),
                OnBehalfTo: getTransaltion("KyribaScreen.OnBehalfTo"),
                POApproval: getTransaltion("KyribaScreen.POApproval"),
                beneficaryDetails: getTransaltion("KyribaScreen.beneficaryDetails"),
                name: getTransaltion("KyribaScreen.name"),
                city: getTransaltion("KyribaScreen.city"),
                zipCode: getTransaltion("KyribaScreen.zipCode"),
                address_1: getTransaltion("KyribaScreen.address_1"),
                address_2: getTransaltion("KyribaScreen.address_2"),
                state_Prov: getTransaltion("KyribaScreen.state_Prov"),
                beneficaryBankDetails: getTransaltion("KyribaScreen.beneficaryBankDetails"),
                bank: getTransaltion("KyribaScreen.bank"),
                streetAddress_1: getTransaltion("KyribaScreen.streetAddress_1"),
                streetAddress_2: getTransaltion("KyribaScreen.streetAddress_2"),
                accountNumber: getTransaltion("KyribaScreen.accountNumber"),
                accountCurrency: getTransaltion("KyribaScreen.accountCurrency"),
                bankBIC: getTransaltion("KyribaScreen.bankBIC"),
                coresspndentBankName: getTransaltion("KyribaScreen.coresspndentBankName"),
                coresspndentBankBIC: getTransaltion("KyribaScreen.coresspndentBankBIC"),
                sendtoPayment: getTransaltion("KyribaScreen.sendtoPayment"),
            });

        }
        initTransaltion();
    }

    return KyribaInterfaceContentViewModel;
});