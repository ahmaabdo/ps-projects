define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function (oj, ko, $, app, services, commonhelper) {
    function createRevenueContentViewModel() {
        var self = this;
        self.label = ko.observable();

        self.customsModel = {
            bussinesUnitVal: ko.observable(),
            projectNameVal: ko.observable(),
            contractNumberVal: ko.observable(),
            taskNameVal: ko.observable(),
            contractLineVal: ko.observable(),
            amountVal: ko.observable(),
            projectNumber: ko.observable(),
            taskNumber: ko.observable(),
            completionDateVal: ko.observable(),
            receiptNumber: ko.observable(),
            rowSelected: ko.observable(),
            lgScreenLbl: ko.observable(),
            contractNumberVal: ko.observable(),
            revenueDateVal: ko.observable(),
            bussinesUnitVal: ko.observable(),
            receiptNumVal: ko.observable(),
            projectNumVal: ko.observable(),
            contractNumberVal2: ko.observable(),
            taskNumberVal: ko.observable(),
            rowSelected: ko.observable(),
            eventPercentageVal: ko.observable(12),
            categoryNameVal: ko.observable(),
            expendituretypeVal: ko.observable(),
            vendorNameVal: ko.observable(),
            poDescriptionVal: ko.observable(),
            expenditureTypeVal: ko.observable(),
            categoryNameVal: ko.observable(),
            receiptDateFromVal: ko.observable(),
            receiptDateToVal: ko.observable(),
            eventStatusVal: ko.observable(),
            projectTypeVal: ko.observable(),

            contractsArr: ko.observableArray([]),
            columnArray: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
            bussinesUnitArr: ko.observableArray([]),
            contractNumberArr: ko.observableArray([]),
            receiptNumArr: ko.observableArray([]),
            projectNumArr: ko.observableArray([]),
            contractLineArr: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
            expenditureTypeArr: ko.observableArray([]),
            categoryNameArr: ko.observableArray([]),
            projectTypeArr: ko.observableArray([]),

            resetDis: ko.observable(false),
            isDisabled: ko.observable(true),
            createEventDis: ko.observable(true),
            searchDis: ko.observable(true),
            createEventDailDis: ko.observable(false),
            contractDis: ko.observable(false),
            modifyEventDailDis: ko.observable(false),
            eventPercentageDis: ko.observable(true),
            completionDis: ko.observable(false)
        };

        //check box difinetion
        self.getIndexInSelectedItems = ko.observableArray();
        self.selectedItems = ko.observableArray();
        //revenue data provider
        self.dataproviderRevenue = ko.observable();
        self.dataproviderRevenue(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.contractsArr, { idAttribute: 'srNo' })));

        ko.computed(function () {
            if (app.contractsArr().length > 0) {
                self.customsModel.searchDis(false);
                var BU_NAME = [...new Set(app.contractsArr().map(el => el.BU_NAME))];
                var CONTRACT_NUMBER = [...new Set(app.contractsArr().map(el => el.CONTRACT_NUMBER))];
                var RECEIPT_NUM = [...new Set(app.contractsArr().map(el => el.RECEIPT_NUM))];
                var PROJECT_NUMBER = [...new Set(app.contractsArr().map(el => el.PROJECT_NUMBER))];
                var EXPENDITURE_TYPE = [...new Set(app.contractsArr().map(el => el.EXPENDITURE_TYPE))];
                var CATEGORY_NAME = [...new Set(app.contractsArr().map(el => el.CATEGORY_NAME))];
                var PROJECT_TYPE = [...new Set(app.contractsArr().map(el => el.PROJECT_TYPE))];

                self.customsModel.bussinesUnitArr(BU_NAME.map(el => ({ label: el, value: el })));
                self.customsModel.contractNumberArr(CONTRACT_NUMBER.map(el => ({ label: el, value: el })));
                self.customsModel.receiptNumArr(RECEIPT_NUM.map(el => ({ label: el, value: el })));
                self.customsModel.projectNumArr(PROJECT_NUMBER.map(el => ({ label: el, value: el })));
                self.customsModel.expenditureTypeArr(EXPENDITURE_TYPE.map(el => ({ label: el, value: el })));
                self.customsModel.categoryNameArr(CATEGORY_NAME.map(el => ({ label: el, value: el })));
                self.customsModel.projectTypeArr(PROJECT_TYPE.map(el => ({ label: el, value: el })));
            }
        });

        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder").val();

            self.customsModel.rowSelected = self.customsModel.contractsArr()[index];
            self.customsModel.bussinesUnitVal(self.customsModel.rowSelected.BU_NAME)
            self.customsModel.projectNameVal(self.customsModel.rowSelected.PROJECT_NAME)
            self.customsModel.contractNumberVal(self.customsModel.rowSelected.CONTRACT_NUMBER)
            self.customsModel.contractNumberVal2(self.customsModel.rowSelected.CONTRACT_NUMBER)
            self.customsModel.taskNumberVal(self.customsModel.rowSelected.TASK_NUMBER)
            self.customsModel.amountVal(self.customsModel.rowSelected.REVENUE_AMOUNT)
            self.customsModel.completionDateVal(self.customsModel.rowSelected.REVENUE_DATE);
            self.customsModel.projectNumber(self.customsModel.rowSelected.PROJECT_NUMBER);
            self.customsModel.taskNameVal(self.customsModel.rowSelected.TASK_NAME);
            self.customsModel.receiptNumber(self.customsModel.rowSelected.RECEIPT_NUM);
            self.customsModel.projectNumVal(self.customsModel.rowSelected.PROJECT_NUMBER);
            self.customsModel.receiptNumVal(self.customsModel.rowSelected.RECEIPT_NUM);

            self.customsModel.expendituretypeVal(self.customsModel.rowSelected.EXPENDITURE_TYPE);
            self.customsModel.vendorNameVal(self.customsModel.rowSelected.VENDOR_NAME);
            self.customsModel.poDescriptionVal(self.customsModel.rowSelected.ITEM_DESCRIPTION);
            self.customsModel.categoryNameVal(self.customsModel.rowSelected.DESCRIPTION);

            if (e.target.type == 'button') {
                if (Array.isArray(self.customsModel.rowSelected.G_2)) {
                    self.customsModel.contractDis(false);
                    var linsArr = self.customsModel.rowSelected.G_2.map(el => ({ label: el.LINE_NUMBER, value: el.LINE_NUMBER }))
                    self.customsModel.contractLineArr(linsArr)
                    if (self.customsModel.rowSelected.SELECTED_LINENUM) {
                        self.customsModel.contractLineVal(self.customsModel.rowSelected.SELECTED_LINENUM)
                    } 
                } else {
                    self.customsModel.contractDis(true);
                    var x = [self.customsModel.rowSelected.G_2].map(el => ({ label: el.LINE_NUMBER, value: el.LINE_NUMBER }))
                    self.customsModel.contractLineArr(x)
                    self.customsModel.contractLineVal(parseInt(self.customsModel.rowSelected.G_2.LINE_NUMBER))
                }
                document.querySelector("#revenueDialog").open();
                if (self.customsModel.rowSelected.EVENT_STATUS == "Created") {
                    self.customsModel.createEventDailDis(true);
                    self.customsModel.completionDis(true);
                    self.customsModel.modifyEventDailDis(true);
                    self.customsModel.contractDis(true);
                    self.customsModel.contractLineVal(self.customsModel.rowSelected.LINE_NUMBER)
                } else {
                    self.customsModel.createEventDailDis(false);
                    self.customsModel.completionDis(false);
                    self.customsModel.modifyEventDailDis(false);
                }
            } else if (e.target.type == 'checkbox') {
                if (self.customsModel.rowSelected) {
                    if (e.target.checked) {
                        if(!self.customsModel.rowSelected.SELECTED_LINENUM)
                        {   
                            if(Array.isArray(self.customsModel.rowSelected.G_2))
                                self.customsModel.rowSelected.SELECTED_LINENUM = self.customsModel.rowSelected.G_2[0].LINE_NUMBER
                            
                            else
                                self.customsModel.rowSelected.SELECTED_LINENUM = self.customsModel.rowSelected.G_2.LINE_NUMBER     
                        }
                        self.customsModel.rowSelectedArr().push(self.customsModel.rowSelected)
                        self.customsModel.createEventDis(false);
                    } else {
                        self.customsModel.rowSelectedArr(self.customsModel.rowSelectedArr().filter(el => el.srNo != self.customsModel.rowSelected.srNo))
                        if (self.customsModel.rowSelectedArr().length == 0) {
                            self.customsModel.createEventDis(true);
                        }
                    }
                }
            }
        });

        self.searchBtn = function () {
            cond = function (el) {
                var res = (
                    (self.customsModel.bussinesUnitVal() ? el.BU_NAME == self.customsModel.bussinesUnitVal() : true) &&
                    (self.customsModel.contractNumberVal() ? el.CONTRACT_NUMBER == self.customsModel.contractNumberVal() : true) &&
                    (self.customsModel.receiptNumVal() ? el.RECEIPT_NUM == self.customsModel.receiptNumVal() : true) &&
                    (self.customsModel.projectNumVal() ? el.PROJECT_NUMBER == self.customsModel.projectNumVal() : true) &&
                    (self.customsModel.expenditureTypeVal() ? el.EXPENDITURE_TYPE == self.customsModel.expenditureTypeVal() : true) &&
                    (self.customsModel.eventStatusVal() ? el.EVENT_STATUS == self.customsModel.eventStatusVal() : true) &&
                    (self.customsModel.receiptDateToVal() && self.customsModel.receiptDateFromVal() ? (el.REVENUE_DATE >= self.customsModel.receiptDateFromVal() && el.REVENUE_DATE <= self.customsModel.receiptDateToVal()) : (self.customsModel.receiptDateToVal() ? el.REVENUE_DATE <= self.customsModel.receiptDateToVal() : self.customsModel.receiptDateFromVal() ? el.REVENUE_DATE >= self.customsModel.receiptDateFromVal() : true)) &&
                    (self.customsModel.categoryNameVal() ? el.CATEGORY_NAME == self.customsModel.categoryNameVal() : true) &&
                    (self.customsModel.projectTypeVal() ? el.PROJECT_TYPE == self.customsModel.projectTypeVal() : true)
                );
                return res;
            };
            app.contractsArr().forEach(el => { el.REVENUE_AMOUNT = numberWithCommas(el.REVENUE_AMOUNT) })
            self.customsModel.contractsArr(app.contractsArr().filter(el => cond(el)));
            self.dataproviderRevenue(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.contractsArr, { idAttribute: 'srNo' })));
        };

        self.resetBtnAction = function () {
            self.customsModel.contractNumberVal('')
            self.customsModel.bussinesUnitVal('')
            self.customsModel.revenueDateVal('')
            self.customsModel.projectNumVal('')
            self.customsModel.receiptNumVal('')
            self.customsModel.expenditureTypeVal('')
            self.customsModel.expenditureTypeVal('')
            self.customsModel.categoryNameVal('')
            self.customsModel.receiptDateToVal('')
            self.customsModel.receiptDateFromVal('')
            self.customsModel.eventStatusVal('')
            self.customsModel.projectTypeVal('')
            self.customsModel.contractsArr([]);
            self.customsModel.rowSelectedArr([]);
        }

        self.createEventBtnAction = function () {
            app.loading(true);
            self.customsModel.createEventDis(true);
            document.querySelector("#revenueDialog").close();
            var payload = [];
            for (var i in self.customsModel.rowSelectedArr()) {
                payload.push({
                    data: {
                        "ContractTypeName": "Revenue Contract",
                        "ContractNumber": self.customsModel.rowSelectedArr()[i].CONTRACT_NUMBER,
                        "ContractLineNumber": self.customsModel.rowSelectedArr()[i].SELECTED_LINENUM, //self.customsModel.contractLineVal(), 
                        "EventTypeName": "Revenue Milestone",
                        "EventDescription": "Event to recognize revenue for a specified amount based on progress made",
                        "ProjectNumber": self.customsModel.rowSelectedArr()[i].PROJECT_NUMBER,
                        "CompletionDate": self.customsModel.rowSelectedArr()[i].REVENUE_DATE,
                        "TaskNumber": self.customsModel.rowSelectedArr()[i].TASK_NUMBER,
                        "BillTrnsAmount": self.customsModel.rowSelectedArr()[i].REVENUE_AMOUNT.replace(/,/g, ""),
                        "BillTrnsCurrencyCode": "SAR",
                        "BusinessUnitName": self.customsModel.rowSelectedArr()[i].BU_NAME,
                    },
                    receiptNumber: self.customsModel.rowSelectedArr()[i].RECEIPT_NUM.toString(),
                    receiptPercentage: self.customsModel.rowSelectedArr()[i].EVENT_PERCENTAGE.toString(),
                });
            }

            var createEventsucess = function (res) {
                if (res.Status == "OK") {
                    if (res.Data.status == "done") {
                        app.loading(false)
                        app.messagesDataProvider.push(app.createMessage('confirmation', `Event Created Successfully`))
                        app.getAllContarcts();
                        self.customsModel.searchDis(false);
                        self.customsModel.contractsArr([])
                        self.resetBtnAction();
                    } else {
                        app.messagesDataProvider.push(app.createMessage('error', `Error in create event,${res.Data.data}`))
                        app.loading(false);
                        self.resetBtnAction();
                    }
                } else {
                    app.loading(false)
                    self.resetBtnAction();
                    app.messagesDataProvider.push(app.createMessage('error', `Error in create event ,${res.Data.data}`))
                }
            }
            var createEventfail = function () {
                document.querySelector("#revenueDialog").close();
                self.customsModel.contractsArr([])
                app.loading(false)
                app.messagesDataProvider.push(app.createMessage('error', `Event Not Created`))
            }
            services.postGeneric(commonhelper.createEvent, payload).then(createEventsucess, createEventfail)
        };

        self.modifyEventBtn = function () {
            self.customsModel.rowSelected.REVENUE_DATE = self.customsModel.completionDateVal();
            self.customsModel.rowSelected.REVENUE_AMOUNT = numberWithCommas((parseInt(self.customsModel.rowSelected.REVENUE_AMOUNT.replace(/,/g, "") * ((self.customsModel.eventPercentageVal() + 100) / 100))).toFixed(2));
            self.customsModel.rowSelected.SELECTED_LINENUM = self.customsModel.contractLineVal();

            self.dataproviderRevenue(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.contractsArr, { idAttribute: 'srNo' })));
            document.querySelector("#revenueDialog").close();
        }

        self.getIndexInSelectedItems = function (id) {
            for (var i = 0; i < self.selectedItems().length; i++) {
                var range = self.selectedItems()[i];
                var startIndex = range.startIndex.row;
                var endIndex = range.endIndex.row;
                if (id >= startIndex && id <= endIndex) {
                    return i;
                }
            }
            return -1;
        };

        self.handleCheckbox = function (id, srN) {
            var isChecked = self.getIndexInSelectedItems(id) != -1 || (srN ? self.customsModel.rowSelectedArr().find(e => e.srN == srN) : false);
            return isChecked ? ['checked'] : [];
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.cancelDialBtn = function () {
            document.querySelector("#revenueDialog").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                bussinesUnitLbl: getTransaltion("revenueScreen.bussinesUnitLbl"),
                projectNameLbl: getTransaltion("revenueScreen.projectNameLbl"),
                contractNumberLbl: getTransaltion("revenueScreen.contractNumberLbl"),
                taskNameLbl: getTransaltion("revenueScreen.taskNameLbl"),
                contractLineLbl: getTransaltion("revenueScreen.contractLineLbl"),
                completionDateLbl: getTransaltion("revenueScreen.completionDateLbl"),
                amountLbl: getTransaltion("revenueScreen.amountLbl"),
                calculateLbl: getTransaltion("revenueScreen.calculateLbl"),
                searchLbl: getTransaltion("common.searchLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                supplierNameLbl: getTransaltion("revenueScreen.supplierNameLbl"),
                revenueAmount: getTransaltion("revenueScreen.revenueAmount"),
                PONumberLbl: getTransaltion("revenueScreen.PONumberLbl"),
                projectDescriptionLbl: getTransaltion("revenueScreen.projectDescriptionLbl"),
                revenueDate: getTransaltion("revenueScreen.revenueDate"),
                receiptNumberLbl: getTransaltion("revenueScreen.receiptNumberLbl"),
                contractNumber: getTransaltion("revenueScreen.contractNumber"),
                retentionPercLbl: getTransaltion("revenueScreen.retentionPercLbl"),
                createRevenueLbl: getTransaltion("revenueScreen.createRevenueLbl"),
                amountReleaseLbl: getTransaltion("revenueScreen.amountReleaseLbl"),
                changeLbl: getTransaltion("common.changeLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                creteEventBtnLbl: getTransaltion("revenueScreen.creteEventBtnLbl"),
                status: getTransaltion("revenueScreen.status"),
                revenueDateLbl: getTransaltion("revenueScreen.revenueDateLbl"),
                contractNumberLbl: getTransaltion("revenueScreen.contractNumberLbl"),
                receiptNumberLbl: getTransaltion("revenueScreen.receiptNumberLbl"),
                bussinesUnitLbl: getTransaltion("revenueScreen.bussinesUnitLbl"),
                resetBtnLbl: getTransaltion("revenueScreen.resetBtnLbl"),
                receiptNumLbl: getTransaltion("revenueScreen.receiptNumLbl"),
                projectNumLbl: getTransaltion("revenueScreen.projectNumLbl"),
                createViewBtnLbl: getTransaltion("revenueScreen.createViewBtnLbl"),
                revenueScreenLbl: getTransaltion("revenueScreen.revenueScreenLbl"),
                taskNumberLbl: getTransaltion("revenueScreen.taskNumberLbl"),
                modifyEventBtnLbl: getTransaltion("revenueScreen.modifyEventBtnLbl"),
                eventPercatgeLbl: getTransaltion("revenueScreen.eventPercatgeLbl"),
                profitMarginLbl: getTransaltion("revenueScreen.profitMarginLbl"),
                poNumberLbl: getTransaltion("revenueScreen.poNumberLbl"),
                receiptDateLbl: getTransaltion("revenueScreen.receiptDateLbl"),
                categoryNameLbl: getTransaltion("revenueScreen.categoryNameLbl"),
                expenditureTypeLbl: getTransaltion("revenueScreen.expenditureTypeLbl"),
                vendorNameLbl: getTransaltion("revenueScreen.vendorNameLbl"),
                poDescriptionLbl: getTransaltion("revenueScreen.poDescriptionLbl"),
                receiptDateToLbl: getTransaltion("revenueScreen.receiptDateToLbl"),
                receiptDateFromLbl: getTransaltion("revenueScreen.receiptDateFromLbl"), 
                eventStatusLbl: getTransaltion("revenueScreen.eventStatusLbl"),
                projectTypeLbl: getTransaltion("revenueScreen.projectTypeLbl"),
                eventDetailsLbl: getTransaltion("revenueScreen.eventDetailsLbl"),

            });

            self.customsModel.columnArray([{
                "headerText": "Select",
                "template": 'checkTemplate'
            },
            {
                "headerText": "NO.",
                "template": 'seqTemplate'
            },
            {
                "headerText": "Business Unit",
                "field": "BU_NAME",
            },
            {
                "headerText": self.label().receiptNumberLbl,
                "field": "RECEIPT_NUM"
            },
            {
                "headerText": self.label().revenueAmount,
                "field": "REVENUE_AMOUNT"
            },
            {
                "headerText": self.label().receiptDateLbl,
                "field": "REVENUE_DATE"
            },
            {
                "headerText": self.label().categoryNameLbl,
                "field": "CATEGORY_NAME"
            },
            {
                "headerText": self.label().expenditureTypeLbl,
                "field": "EXPENDITURE_TYPE"
            },
            {
                "headerText": self.label().projectNumLbl,
                "field": "PROJECT_NUMBER"
            },
            {
                "headerText": self.label().taskNumberLbl,
                "field": "TASK_NUMBER"
            },
            {
                "headerText": self.label().projectTypeLbl,
                "field": "PROJECT_TYPE"
            },
            {
                "headerText": self.label().contractNumber,
                "field": "CONTRACT_NUMBER"
            },
            {
                "headerText": self.label().poNumberLbl,
                "field": "PO_NUMBER"
            },
            {
                "headerText": self.label().profitMarginLbl,
                "field": "EVENT_PERCENTAGE"
            },
            {
                "headerText": self.label().status,
                "field": "EVENT_STATUS"
            },
            {
                "headerText": "Action",
                "template": 'buttonTemplate'
            }
            ]);
        }
        initTransaltion();
    }

    return createRevenueContentViewModel;
});