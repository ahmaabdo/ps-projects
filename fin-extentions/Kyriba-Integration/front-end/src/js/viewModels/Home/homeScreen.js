define(['ojs/ojcore', 'knockout', 'jquery','ojs/ojrouter', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function (oj, ko, $,Router, app, services, commonhelper) {
    function HomeContentViewModel() {
        var self = this;
        self.label = ko.observable();
        self.KyribaInterfaceLbl = ko.observable()
        self.customsModel = {
          
        };

        KyribaInterfaceBtn = function(){
            app.router.go('KyribaInterface');
        }
        AuditHomeBtn = function(){
            app.router.go('Audit')
        }
        
        ko.computed(function () {

        });

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                
            });

        }
        initTransaltion();
    }

    return HomeContentViewModel;
});