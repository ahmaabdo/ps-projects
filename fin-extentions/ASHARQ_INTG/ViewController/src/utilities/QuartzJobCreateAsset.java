package utilities;


import com.appspro.finance2iot.CreateIotAsset;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


public class QuartzJobCreateAsset implements Job{
    private Logger log = Logger.getLogger(QuartzJobCreateAsset.class);

    @Override
    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
        CreateIotAsset createIotAsset = new CreateIotAsset();

        System.out.println("QuartzJob!!!!");
        createIotAsset.checkAndBuildJson();
        System.out.println("QuartzJob!!!!");
    }
}
