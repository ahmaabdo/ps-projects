package utilities;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class ServletListenerUpdateAsset implements ServletContextListener {
    private ServletContext context = null;

    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        System.out.println("----- listeners initiated");
        try {
            JobDetail job =
                JobBuilder.newJob(QuartzJobUpdateAsset.class).build();
            Trigger trigger =
                TriggerBuilder.newTrigger().withIdentity("jobUpdate").withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(9,
                                                                                                                            45)).forJob(job).build();
            Scheduler sch = StdSchedulerFactory.getDefaultScheduler();
            sch.start();
            sch.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            System.out.println("Error" + e);
            e.printStackTrace();
        }
    }

    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
    }
}
