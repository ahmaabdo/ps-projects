package utilities;

import com.appspro.finance2iot.CreateIotAsset;

import com.appspro.finance2iot.DeleteAsset;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class QuartzJobDeleteAsset implements Job{
    private Logger log = Logger.getLogger(QuartzJobDeleteAsset.class);

    @Override
    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
        DeleteAsset deleteAsset = new DeleteAsset();
        System.out.println("QuartzJob!!!!");
        deleteAsset.deleteAssetFromIOT();
        System.out.println("QuartzJob!!!!");
    }
}
