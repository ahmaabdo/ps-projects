package utilities;


import com.appspro.iot2finance.UpdateTagNumber;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class QuartzJobUpdateAsset implements Job {
    private Logger log = Logger.getLogger(QuartzJobDeleteAsset.class);

    @Override
    public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
        UpdateTagNumber updateTagNumber = new UpdateTagNumber();
        System.out.println("QuartzJob!!!!");
        updateTagNumber.postFileToUCMAndAssetTable();
        System.out.println("QuartzJob!!!!");
    }
}
