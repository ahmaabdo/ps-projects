package utilities;

import com.appspro.Helper.RestHelper;

import com.appspro.bean.AssetBean;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.net.URL;

import java.nio.channels.FileChannel;

import java.text.SimpleDateFormat;

import java.util.Base64;

import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONException;
import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

public class GeneralHelper {
    static String path = System.getProperty("user.dir");
    static String tDir = System.getProperty("java.io.tmpdir");
    static String encodedValue = null;

    public String decodeString(String value) {
        Base64.Decoder decoder = Base64.getDecoder();
        String decoded = new String(decoder.decode(value));
        //        System.out.println("Decoded Data: " + decoded);
        return decoded;
    }

    public static String GetDataAndTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String dateTime =
            formatter.format(date).replaceAll("/", "").replaceAll(":",
                                                                  "").replaceAll(" ",
                                                                                 "");
        System.out.println(dateTime);
        return dateTime;
    }


    public String getAssetFromFinance(String dateParam, String type) {
        try {
            String result =
                RestHelper.httpPostSoapReq("https://emrb-test.fa.em3.oraclecloud.com/xmlpserver/services/v2/ReportService",
                                           RestHelper.getRequest(dateParam,
                                                                 type));
            DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            ByteArrayInputStream bis =
                new ByteArrayInputStream(result.getBytes());
            Document document = builder.parse(bis);
            document.getDocumentElement().normalize();
            DocumentTraversal trav = (DocumentTraversal)document;

            NodeIterator it =
                trav.createNodeIterator(document.getDocumentElement(),
                                        NodeFilter.SHOW_ELEMENT, null, true);
            int c = 1;
            for (Node node = it.nextNode(); node != null;
                 node = it.nextNode()) {
                String name = node.getNodeName();
                if (name.equalsIgnoreCase("ns1:reportBytes")) { // ishould here to check if its null
                    encodedValue = node.getTextContent();
                }
                c++;
            }
            String resultAfterDecoded = decodeString(encodedValue);
            return resultAfterDecoded;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String uploadFileToUcm(String dataParam, String type) {
        try {
            String result =
                RestHelper.httpPostSoapReq("https://emrb-test.fa.em3.oraclecloud.com:443/fscmService/FinancialUtilService",
                                           RestHelper.getRequest(dataParam,
                                                                 type));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String uploadFileToAssetTable(String dataParam, String type) {
        try {
            String result =
                RestHelper.httpPostSoapReq("https://emrb-test.fa.em3.oraclecloud.com:443/fscmService/FinancialUtilService",
                                           RestHelper.getRequest(dataParam,
                                                                 type));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String postFileToAssetTable(String param, String request) {
        try {

            String resXml = uploadFileToAssetTable(param, request);
            JSONObject jSONObject = XML.toJSONObject(resXml);
            System.out.println(jSONObject);
            JSONObject header = jSONObject.getJSONObject("env:Envelope");
            JSONObject body = header.getJSONObject("env:Body");
            JSONObject uploadFileToUcmResponse =
                body.getJSONObject("ns0:submitESSJobRequestResponse");
            JSONObject result =
                uploadFileToUcmResponse.getJSONObject("result");
            String content = result.optString("content");
            return content;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject getAssetAndDeviceIot(String param, String type) {
        try {
            if (type.equals("getByName")) {
                JSONObject response =
                    new JSONObject(RestHelper.callGetRest("https://amtest-asharqnewsservices.oracleiotcloud.com/assetMonitoring/clientapi/v2/assets?q={\"name\": { \"$eq\":\"" +
                                                          param + "\" } }"));
                return response;
            } else if (type.equals("getDeviceById")) {
                JSONObject response =
                    new JSONObject(RestHelper.callGetRest("https://amtest-asharqnewsservices.oracleiotcloud.com/iot/api/v2/devices/" +
                                                          param));
                return response;
            } else if (type.equals("getAssetType")) {
                JSONObject response =
                    new JSONObject(RestHelper.callGetRest("https://amtest-asharqnewsservices.oracleiotcloud.com/assetMonitoring/clientapi/v2/assetTypes?q={\"name\": { \"$eq\":\"" +
                                                          param + "\" } }"));
                return response;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public AssetBean addAndDeleteAssetIot(String body,
                                          String type) throws JSONException {
        AssetBean assetBean = new AssetBean();
        if (type.equals("createAsset")) {
            assetBean =
                    RestHelper.callPostRest("https://amtest-asharqnewsservices.oracleiotcloud.com/assetMonitoring/clientapi/v2/assets",
                                            "application/json", body, "IOT");
            return assetBean;
        } else if (type.equals("retirement")) {
            assetBean =
                    RestHelper.callDeleteRest("https://amtest-asharqnewsservices.oracleiotcloud.com/assetMonitoring/clientapi/v2/assets/" +
                                              body);
            return assetBean;
        } else if (type.equals("createAssetType")) {
            assetBean =
                    RestHelper.callPostRest("https://amtest-asharqnewsservices.oracleiotcloud.com/assetMonitoring/clientapi/v2/assetTypes",
                                            "application/json", body, "IOT");
            return assetBean;
        }
        return null;
    }

    public static void copyFaTransfersT() {

        try {
            File fi = new File(path + "\\FaTransfersT.csv");
            FileChannel sourceChannel = null;
            FileChannel destChannel = null;
            try {
                sourceChannel = new FileInputStream(fi).getChannel();
                destChannel =
                        new FileOutputStream(tDir + "/FaTransfersT.csv").getChannel();
                destChannel.transferFrom(sourceChannel, 0,
                                         sourceChannel.size());
            } finally {
                sourceChannel.close();
                destChannel.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFaTransferDistsT() {

        try {
            File fi = new File(path + "\\FaTransferDistsT.csv");
            FileChannel sourceChannel = null;
            FileChannel destChannel = null;
            try {
                sourceChannel = new FileInputStream(fi).getChannel();
                destChannel =
                        new FileOutputStream(tDir + "/FaTransferDistsT.csv").getChannel();
                destChannel.transferFrom(sourceChannel, 0,
                                         sourceChannel.size());
            } finally {
                sourceChannel.close();
                destChannel.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String encode(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int)file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return base64File;
    }

}
