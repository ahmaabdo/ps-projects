package com.appspro.biReport;

import java.util.HashMap;
import java.util.Map;
import static com.appspro.biReport.BIReportFinanceModel.runReportWithParam;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RetriveAssetWithNullTag {
    public String getAssetWithNullTagData(String CREATION_DATE) {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            if (CREATION_DATE != null) {

                paramMap.put(BIReportFinanceModel.FIXED_ASSET_NO_TAG_Report.CREATION_DATE.getValue(),
                             CREATION_DATE);
            }
            JSONObject json =
                runReportWithParam(BIReportFinanceModel.REPORT_NAME.FIXED_ASSET_NO_TAG_Report.getValue(),
                                   paramMap);
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.getJSONObject("DATA_DS");
            if (!dataDS.has("G_1")) {
                return "null";
            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.getJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.getJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
