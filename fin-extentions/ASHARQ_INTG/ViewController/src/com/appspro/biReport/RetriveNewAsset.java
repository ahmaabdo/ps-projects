package com.appspro.biReport;

import static com.appspro.biReport.BIReportFinanceModel.runReport;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class RetriveNewAsset {

    public String getAssetData() {
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            JSONObject json =
                runReport(BIReportFinanceModel.REPORT_NAME.FIXED_ASSET_Report.getValue());
            System.out.println(json);
            if (json.length() < 1) {
                return "null";
            }
            JSONObject dataDS = json.optJSONObject("DATA_DS");

            if (dataDS == null) {
                return "null";
            }
            //            if (!dataDS.has("G_1")) {
            //                return "null";
            //            }
            Object jsonTokner =
                new JSONTokener(dataDS.get("G_1").toString()).nextValue();
            if (jsonTokner instanceof JSONObject) {
                JSONObject g1 = dataDS.optJSONObject("G_1");
                JSONArray j1 = new JSONArray();
                j1.put(g1);
                return j1.toString();
            } else {
                JSONArray g1 = dataDS.optJSONArray("G_1");
                return g1.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
