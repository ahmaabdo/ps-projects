/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.database;

import com.appspro.database.BundelReader;

import java.util.ResourceBundle;

public class CommonConfigReader {

    private static ResourceBundle bundle = null;

    public static String getValue(String key) {
        if (bundle == null) {
            bundle = BundelReader.loadResourceBundle("connNew", "en");
        }
        return bundle.getString(key);
    }
}
