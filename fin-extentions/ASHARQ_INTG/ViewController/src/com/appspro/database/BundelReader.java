/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.database;

import com.appspro.database.SessionLocaleBean;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author user
 */
public class BundelReader {

    private BundelReader() {
    }

    public static ResourceBundle loadResourceBundle(String bundleName, String localeName) {
        Locale locale = new Locale(localeName, "");
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
        return bundle;
    }

    public static ResourceBundle loadResourceBundle(String bundleName, SessionLocaleBean sessionLocale) {
        Locale locale = new Locale(sessionLocale.getUserSessionLocale(), "");
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
        return bundle;
    }
}
