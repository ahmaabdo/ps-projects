package com.appspro.database;

import com.appspro.bean.AssetBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static com.appspro.Helper.RestHelper.getSchema_Name;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class AssetController extends AppsproConnection {
    PreparedStatement stmt;
    ResultSet rset;
    String query;
    String sqlStirng;
    Connection conn;
    static int batcheNum;
    //" + " " + getSchema_Name() + ".

    public AssetBean insertAsset(AssetBean assetBean, String table) {
        try {
            conn = AppsproConnection.getConnection();
            if (table.equals("NEW_ASSET")) {
                query =
                        "INSERT INTO " + table + "(ASSET_ID,ASSET_DESCRIPTION,TAG_NUMBER,SERIAL_NUMBER,MODEL_NUMBER,BOOK_NAME,PRORATE_CONVENTION,UNIT,INTANGIBLE,IN_PHYSICAL_INVENTORY,MINOR_CATEGORY,CREATION_DATE_ASSET,CREATION_DATE_DB,LOCATION_ID,LOCATION_SEGMENT,ASSET_NUMBER,STATUS_CODE,STATUS_EXPLANATION,ASSET_ID_IOT,BATCHE_NUM,MAJOR_CATEGORY)\n" +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(SYSDATE, 'DD-MM-YYYY'),?,?,?,?,?,?,?,?)";
            } else if (table.equals("RETIRED_ASSET")) {
                query =
                        "INSERT INTO " + table + "(ASSET_ID,ASSET_DESCRIPTION,TAG_NUMBER,SERIAL_NUMBER,MODEL_NUMBER,BOOK_NAME,PRORATE_CONVENTION,UNIT,INTANGIBLE,IN_PHYSICAL_INVENTORY,MINOR_CATEGORY,CREATION_DATE_ASSET,CREATION_DATE_DB,LOCATION_ID,LOCATION_SEGMENT,ASSET_NUMBER,STATUS_CODE,STATUS_EXPLANATION,ASSET_ID_IOT,BATCHE_NUM,MAJOR_CATEGORY)\n" +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(SYSDATE, 'DD-MM-YYYY'),?,?,?,?,?,?,?,?)";
            } else if (table.equals("TAG_UPDATED_ASSET")) {
                query =
                        "INSERT INTO " + table + "(ASSET_ID,ASSET_DESCRIPTION,TAG_NUMBER,SERIAL_NUMBER,MODEL_NUMBER,BOOK_NAME,PRORATE_CONVENTION,UNIT,INTANGIBLE,IN_PHYSICAL_INVENTORY,MINOR_CATEGORY,CREATION_DATE_ASSET,CREATION_DATE_DB,LOCATION_ID,LOCATION_SEGMENT,ASSET_NUMBER,STATUS_CODE,STATUS_EXPLANATION,ASSET_ID_IOT,BATCHE_NUM,MAJOR_CATEGORY)\n" +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(SYSDATE, 'DD-MM-YYYY'),?,?,?,?,?,?,?,?)";
            }

            else if (table.equals("NEW_ASSET_TYPE")) {
                query =
                        "INSERT INTO " + table + "(ASSET_ID,ASSET_DESCRIPTION,TAG_NUMBER,SERIAL_NUMBER,MODEL_NUMBER,BOOK_NAME,PRORATE_CONVENTION,UNIT,INTANGIBLE,IN_PHYSICAL_INVENTORY,MINOR_CATEGORY,CREATION_DATE_ASSET,CREATION_DATE_DB,LOCATION_ID,LOCATION_SEGMENT,ASSET_NUMBER,STATUS_CODE,STATUS_EXPLANATION,ASSET_ID_IOT,BATCHE_NUM,MAJOR_CATEGORY)\n" +
                        "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,TO_DATE(SYSDATE, 'DD-MM-YYYY'),?,?,?,?,?,?,?,?,?)";
            }
            stmt = conn.prepareStatement(query);
            System.out.println("Execute " + query);
            stmt.setString(1, assetBean.getAssetId());
            stmt.setString(2, assetBean.getDescription());
            stmt.setString(3, assetBean.getTagNumber());
            stmt.setString(4, assetBean.getSerialNumber());
            stmt.setString(5, assetBean.getModelNumber());
            stmt.setString(6, assetBean.getBookName());
            stmt.setString(7, assetBean.getProrateConvention());
            stmt.setString(8, String.valueOf(assetBean.getUnit()));
            stmt.setString(9, assetBean.getIntangible());
            stmt.setString(10, assetBean.getInPhysicalInventory());
            stmt.setString(11, assetBean.getMinorCategory());
            stmt.setString(12, assetBean.getCreateDateSaaS());
            stmt.setString(13, assetBean.getLocationId());
            stmt.setString(14, assetBean.getLocationSegment());
            stmt.setString(15, assetBean.getAssetNumber());
            stmt.setString(16, assetBean.getStatusCode());
            stmt.setString(17, assetBean.getStatus());
            stmt.setString(18, assetBean.getAssetIdIot());
            stmt.setInt(19, assetBean.getBatcheNum());
            stmt.setString(20, assetBean.getMajorCategory());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeResources(conn, stmt, rset);
        }
        return assetBean;
    }

    public int getLastBatche() {
        try {
            conn = AppsproConnection.getConnection();
            query =
                    "SELECT (COALESCE(MAX(BATCHE_NUM),12345)) AS BATCHE_NUM FROM TAG_UPDATED_ASSET";
            stmt = conn.prepareStatement(query);
            System.out.println("Execute " + query);
            rset = stmt.executeQuery();
            while (rset.next()) {
                AssetBean assetBean = new AssetBean();
                assetBean.setBatcheNum(rset.getInt("BATCHE_NUM"));
                batcheNum = assetBean.getBatcheNum();
                return batcheNum;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeResources(conn, stmt, rset);
        }
        return batcheNum;
    }

    public List<AssetBean> getAllAsset() {
        List<AssetBean> assetBeanList = new ArrayList<AssetBean>();

        try {
            conn = AppsproConnection.getConnection();
            String query = null;
            query =
                    "SELECT * from \"C##TEST\".\"NEW_ASSET\" WHERE TAG_NUMBER IS NULL AND STATUS_EXPLANATION = 'Succsess Created'";
            System.out.println("\nExecuting query: " + query);
            stmt = conn.prepareStatement(query);
            rset = stmt.executeQuery();
            while (rset.next()) {
                AssetBean assetBean = new AssetBean();
                assetBean.setMinorCategory(rset.getString("MINOR_CATEGORY"));
                assetBean.setBookName(rset.getString("BOOK_NAME"));
                assetBean.setAssetNumber(rset.getString("ASSET_NUMBER"));
                assetBeanList.add(assetBean);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            closeResources(conn, stmt, rset);
        }
        return assetBeanList;
    }

    public List<AssetBean> updateStatusDeleted(String assetNumber) {
        List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
        try {

            conn = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE C##TEST.NEW_ASSET SET STATUS_EXPLANATION = 'Success Deleted' WHERE ASSET_NUMBER = ?";
            System.out.println("\nExecuting query: " + query);
            stmt = conn.prepareStatement(query);
            stmt.setString(1, assetNumber);
            stmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(conn, stmt, rset);
        }
        return assetBeanList;
    }

    public List<AssetBean> updateTagId(String assetNumber, String tagId) {
        List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
        try {

            conn = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE C##TEST.NEW_ASSET SET TAG_NUMBER = ? WHERE ASSET_NUMBER = ?";
            System.out.println("\nExecuting query: " + query);
            stmt = conn.prepareStatement(query);
            stmt.setString(1, tagId);
            stmt.setString(2, assetNumber);
            stmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(conn, stmt, rset);
        }
        return assetBeanList;
    }


    public static void main(String[] args) {
        AssetController assetController = new AssetController();
        assetController.getLastBatche();
    }
}
