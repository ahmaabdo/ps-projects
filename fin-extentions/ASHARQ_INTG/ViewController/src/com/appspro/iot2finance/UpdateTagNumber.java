package com.appspro.iot2finance;


import com.appspro.bean.AssetBean;
import com.appspro.biReport.RetriveAssetWithNullTag;
import com.appspro.database.AssetController;

import com.google.gson.Gson;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import utilities.GeneralHelper;


public class UpdateTagNumber extends GeneralHelper {
    static String tDir = System.getProperty("java.io.tmpdir");
    static List list = new ArrayList();
    static int batcheNum;
    AssetController assetController = new AssetController();
    RetriveAssetWithNullTag retriveAssetWithNullTag =
        new RetriveAssetWithNullTag();
    //    public void checkAndUpdateAsset() {
    //        try {
    //            JSONObject res = getAssetIotByNameOrDate("", "getByDate");
    //            int count = res.optInt("count");
    //            System.out.println(count);
    //            if (count >= 1) {
    //              //  createFile();
    //                JSONArray item = res.getJSONArray("items");
    //                System.out.println(item);
    //                for (int j = 0; j < item.length(); j++) {
    //                    AssetBean assetBean = new AssetBean();
    //                    JSONObject result = item.getJSONObject(j);
    //                    assetBean.setTagNumber(result.optString("registrationTime"));
    //                    JSONObject attribute = result.getJSONObject("attributes");
    //                    int assetNumber = attribute.optInt("Asset_number");
    //                    if (String.valueOf(assetNumber) != null) {
    //                        assetBean.setAssetNumber(String.valueOf(assetNumber));
    //                        writeToFile(assetBean);
    //                    } else {
    //                        continue;
    //                    }
    //                }
    //            }
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //    }

//    public void checkAndUpdateAsset() {
//        AssetBean assetBean = new AssetBean();
//        AssetBean assetBean2 = new AssetBean();
//        try {
//            String jsonArray =
//                retriveAssetWithNullTag.getAssetWithNullTagData("09-07-2020");
//            if (!jsonArray.equals("null")) {
//                batcheNum = assetController.getLastBatche() + 1;
//                JSONArray ja = new JSONArray(jsonArray);
//                for (int ind = 0; ind < ja.length(); ind++) {
//                    JSONObject jsonObject = ja.getJSONObject(ind);
//                    assetBean.setAssetNumber(jsonObject.optString("ASSET_NUMBER"));
//                    assetBean.setDescription(jsonObject.optString("ASSET_DESCRIPTION"));
//                    assetBean.setAssetId(jsonObject.optString("ASSET_ID"));
//                    assetBean.setTagNumber(jsonObject.optString("TAG_NUMBER"));
//                    assetBean.setModelNumber(jsonObject.optString("MODEL_NUMBER"));
//                    assetBean.setSerialNumber(jsonObject.optString("SERIAL_NUMBER"));
//                    assetBean.setLocationId(jsonObject.optString("LOCATION_ID"));
//                    assetBean.setLocationSegment(jsonObject.optString("LOC_SEG"));
//                    assetBean.setBookName(jsonObject.optString("BOOK_NAME"));
//                    assetBean.setProrateConvention(jsonObject.optString("PRORATE_CONVENTION"));
//                    assetBean.setUnit(jsonObject.optString("UNIT"));
//                    assetBean.setIntangible(jsonObject.optString("INTANGIBLE"));
//                    assetBean.setInPhysicalInventory(jsonObject.optString("IN_PHYISCAL_INVENTORY"));
//                    assetBean.setCreateDateSaaS(jsonObject.optString("CREATION_DATE"));
//                    String category = jsonObject.optString("CATEGORY");
//                    String categoryFirstPart = category.split("\\-")[0];
//                    assetBean.setMajorCategory(categoryFirstPart);
//                    JSONObject getAssetFromIot =
//                        getAssetAndDeviceIot(assetBean.getDescription() +
//                                             "--" + assetBean.getAssetNumber(),
//                                             "getByName");
//                    JSONArray item = getAssetFromIot.optJSONArray("items");
//                    if (!item.equals(null)) {
//                        JSONObject obj = item.optJSONObject(0);
//                        JSONObject attribute = obj.optJSONObject("attributes");
//                        JSONObject lat = attribute.optJSONObject("lat");
//                        if (!lat.equals("")) {
//                            String deviceId = lat.optString("deviceId");
//                            if (!deviceId.equals("")) {
//                                JSONObject getDeviceFromIot =
//                                    getAssetAndDeviceIot(deviceId,
//                                                         "getDeviceById");
//                                String error =
//                                    getDeviceFromIot.optString("detail");
//                                if (!error.equals("")) {
//                                    //Insert error TO DB
//                                    assetBean.setStatus(getDeviceFromIot.optString("detail"));
//                                    assetBean.setStatusCode(getDeviceFromIot.optString("status"));
//                                    assetController.insertAsset(assetBean,
//                                                                "TAG_UPDATED_ASSET");
//                                } else {
//                                    assetBean.setBatcheNum(batcheNum);
//                                    assetBean.setTagNumber(getDeviceFromIot.optString("hardwareId"));
//                                    assetBean.setStatus("Wrote to the file");
//                                    assetController.insertAsset(assetBean,
//                                                                "TAG_UPDATED_ASSET");
//                                    writeToFile(assetBean);
//                                }
//                            } else {
//                                assetBean.setStatus("There is no TagID in IOT");
//                                assetController.insertAsset(assetBean,
//                                                            "TAG_UPDATED_ASSET");
//                                System.out.println("Null");
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static void createFile() {
        CSVWriter csvWriter;
        try {
            Workbook wb = new HSSFWorkbook();
            csvWriter =
                    new CSVWriter(new FileWriter(tDir + "/FaDescriptiveDetailsINT.xls"));
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("FaDescriptiveDetailsINT");
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //    public static void writeToFile(int rowIndex, AssetBean assetBean) {
    //        try {
    //            XSSFWorkbook wb = new XSSFWorkbook();
    //            int i = wb.getNumberOfSheets();
    //            OutputStream os =
    //                new FileOutputStream(tDir + "/FaDescriptiveDetailsINT.xls");
    //            XSSFSheet sheet = wb.getSheetAt(0);
    //            // Specific row number
    //            Row row = sheet.createRow(rowIndex);
    //
    //            // Specific cell number
    //            Cell groupName = row.createCell(0);
    //            Cell bookType = row.createCell(1);
    //            Cell assetNumber = row.createCell(2);
    //            Cell tagNumber = row.createCell(15);
    //            Cell end =
    //                row.createCell(121); // putting value at specific position
    //            groupName.setCellValue("abcd1");
    //            bookType.setCellValue("SNS BOOK");
    //            assetNumber.setCellValue(assetBean.getAssetNumber());
    //            tagNumber.setCellValue(assetBean.getTagNumber());
    //            end.setCellValue("END");
    //
    //            // writing the content to Workbook
    //            wb.write(os);
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //    }

    public void writeToFile(AssetBean assetBean) {
        try {
            CSVWriter writer =
                new CSVWriter(new FileWriter(tDir + "/FaDescriptiveDetailsINT.csv"));
            String line[] =
            { "WSS" + String.valueOf(assetBean.getBatcheNum()), assetBean.getBookName(),
              assetBean.getAssetNumber(), "", "", "", "", "", "", "", "", "",
              "", "", "", assetBean.getTagNumber().substring(9), "", "", "", "", "", "", "",
              "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
              "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
              "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
              "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
              "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
              "", "", "", "", "", "", "", "", "", "END" };
            list.add(line);
            writer.writeAll(list);
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void checkAndUpdateAsset() {
        try {
            AssetBean assetBean = new AssetBean();
            List<AssetBean> assetBeanList = new ArrayList<>();
            assetBeanList = assetController.getAllAsset();
            batcheNum = assetController.getLastBatche() + 1;
            for (int ind = 0; ind < assetBeanList.size(); ind++) {
                String assetName =
                    assetBeanList.get(ind).getMinorCategory() + "--" +
                    assetBeanList.get(ind).getAssetNumber();
                assetBean.setAssetNumber(assetBeanList.get(ind).getAssetNumber());
                assetBean.setBookName(assetBeanList.get(ind).getBookName());
                JSONObject getAssetFromIot =
                    getAssetAndDeviceIot(assetName, "getByName");
                JSONArray item = getAssetFromIot.optJSONArray("items");
                int count = getAssetFromIot.optInt("count");
                if (count >= 1) {
                    JSONObject obj = item.optJSONObject(0);
                    JSONObject attribute = obj.optJSONObject("attributes");
                    JSONObject lat = attribute.optJSONObject("lat");
                    if (!lat.equals("")) {
                        String deviceId = lat.optString("deviceId");
                        if (!deviceId.equals("")) {
                            JSONObject getDeviceFromIot =
                                getAssetAndDeviceIot(deviceId,
                                                     "getDeviceById");
                            String error =
                                getDeviceFromIot.optString("detail");
                            if (!error.equals("")) {
                                //Insert error TO DB
                                assetBean.setStatus(getDeviceFromIot.optString("detail"));
                                assetBean.setStatusCode(getDeviceFromIot.optString("status"));
                                assetController.insertAsset(assetBean,
                                                            "TAG_UPDATED_ASSET");
                            } else {
                                assetBean.setBatcheNum(batcheNum);
                                assetBean.setTagNumber(getDeviceFromIot.optString("hardwareId"));
                                assetBean.setStatus("Wrote to the file");
                                assetController.insertAsset(assetBean,
                                                            "TAG_UPDATED_ASSET");
                                assetController.updateTagId(assetBean.getTagNumber(),assetBean.getAssetNumber());
                                writeToFile(assetBean);
                            }
                        } else {
                            //                            assetBean.setStatus("There is no TagID in IOT");
                            //                            assetController.insertAsset(assetBean,
                            //                                                        "TAG_UPDATED_ASSET");
                            System.out.println("Null");
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createTempZip() {
        try {
            checkAndUpdateAsset();
            String srcFiles = (tDir + "/FaDescriptiveDetailsINT.csv");
            FileOutputStream fos;

            fos = new FileOutputStream(tDir + "/FaDescriptiveDetailsINT.zip");

            ZipOutputStream zipOut = new ZipOutputStream(fos);
            File fileToZip = new File(srcFiles);
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
            zipOut.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postFileToUCMAndAssetTable() {
        try {
            createTempZip();
            String fileEncoded = encode(tDir + "/FaDescriptiveDetailsINT.zip");
            String resXml =
                uploadFileToUcm(fileEncoded, "uploadToUCMDescriptive");
            JSONObject jSONObject = XML.toJSONObject(resXml);
            System.out.println(jSONObject);
            JSONObject header = jSONObject.getJSONObject("env:Envelope");
            JSONObject body = header.getJSONObject("env:Body");
            JSONObject uploadFileToUcmResponse =
                body.getJSONObject("ns0:uploadFileToUcmResponse");
            JSONObject result =
                uploadFileToUcmResponse.getJSONObject("result");
            String content = result.optString("content");
            postFileToAssetTable(content, "uploadTOAssetTableDescriptive");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        UpdateTagNumber descriptiveDetailAsset = new UpdateTagNumber();
         descriptiveDetailAsset.postFileToUCMAndAssetTable();
//        descriptiveDetailAsset.checkAndUpdateAsset();
    }

}
