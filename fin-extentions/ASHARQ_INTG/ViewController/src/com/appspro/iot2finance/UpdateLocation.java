//package com.appspro.iot2finance;
//
//import com.appspro.Helper.RestHelper;
//import utilities.GeneralHelper;
//
//import com.opencsv.CSVReader;
//import com.opencsv.CSVWriter;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipOutputStream;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.json.XML;
//
//
//public class UpdateLocation extends GeneralHelper {
//
//    static String tDir = System.getProperty("java.io.tmpdir");
//    ClassLoader cl = getClass().getClassLoader();
//    static String path = System.getProperty("user.dir");
//    File csvOne = new File(path + "\\FaTransfersT.csv");
//    File csvTwo = new File(path + "\\FaTransferDistsT.csv");
//    //    public void getZip() {
//    //        OutputStream outputStream = null;
//    //        ZipFile zip_file;
//    //        try {
//    //            zip_file =
//    //                    new ZipFile("C:\\Users\\DELL\\Desktop\\FaMassTransfer1.zip");
//    //
//    //            for (Enumeration e = zip_file.entries(); e.hasMoreElements(); ) {
//    //                ZipEntry entry = (ZipEntry)e.nextElement();
//    //                System.out.println(entry.getName());
//    //                InputStream in = zip_file.getInputStream(entry);
//    //                outputStream = new FileOutputStream(entry.getName());
//    //                int read = 0;
//    //                byte[] bytes = new byte[2056];
//    //
//    //                while ((read = in.read(bytes)) != -1) {
//    //                    outputStream.write(bytes, 0, read);
//    //                }
//    //
//    //                in = new FileInputStream(entry.getName());
//    //                BufferedReader br =
//    //                    new BufferedReader(new InputStreamReader(in));
//    //                File inputFile = new File(entry.getName());
//    //                if (entry.getName().equals("FaTransfersT.csv")) {
//    //                    readFaTransfersT(br);
//    //                }
//    //                if (entry.getName().equals("FaTransferDistsT.csv")) {
//    //                    readFaTransferDistsT(br);
//    //                }
//    //
//    //            }
//    //            zipMultipleFile();
//    //        } catch (IOException e) {
//    //            e.printStackTrace();
//    //        }
//    //    }
//
//    public static void readAndUpdateFaTransfersT() {
//
//        try {
//            copyFaTransfersT();
//            File inputFile = new File(tDir + "/FaTransfersT.csv");
//            CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
//            List<String[]> csvBody = reader.readAll();
//            csvBody.get(0)[0] = "C33";
//            csvBody.get(0)[1] = "SNS BOOK";
//            csvBody.get(0)[2] = "324";
//            csvBody.get(0)[3] = "TRANSFER";
//            csvBody.get(0)[4] = "POST";
//            csvBody.get(0)[6] = "2020/06/30";
//            csvBody.get(0)[66] = "END";
//            reader.close();
//            //            List<String[]> dataLines = new ArrayList<>();
//            //            dataLines.add(new String[] { "C2", "SNS BOOK", "4",
//            //                                         "TRANSFER","POST","","2020/06/30"});
//            CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
//            writer.writeAll(csvBody);
//            System.out.println(csvBody.get(0)[0]);
//            writer.flush();
//            writer.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void readAndUpdateFaTransferDistsT() {
//
//        try {
//            copyFaTransferDistsT();
//            File inputFile = new File(tDir + "/FaTransferDistsT.csv");
//            CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
//            List<String[]> csvBody = reader.readAll();
//            csvBody.get(0)[0] = "C33";
//            csvBody.get(1)[0] = "C33";
//            csvBody.get(0)[1] = "SNS BOOK";
//            csvBody.get(1)[1] = "SNS BOOK";
//            csvBody.get(0)[2] = "324";
//            csvBody.get(1)[2] = "324";
//            csvBody.get(0)[3] = "TRANSFER";
//            csvBody.get(1)[3] = "TRANSFER";
//            csvBody.get(0)[4] = "-1";
//            csvBody.get(1)[4] = "1";
//            csvBody.get(0)[6] = "13";
//            csvBody.get(1)[6] = "11";
//            csvBody.get(0)[7] = "131";
//            csvBody.get(1)[7] = "111";
//            csvBody.get(0)[8] = "00";
//            csvBody.get(1)[8] = "00";
//            csvBody.get(0)[9] = "000";
//            csvBody.get(1)[9] = "120";
//            csvBody.get(0)[10] = "00";
//            csvBody.get(1)[10] = "00";
//            csvBody.get(0)[11] = "000";
//            csvBody.get(1)[11] = "000";
//            csvBody.get(0)[13] = "10";
//            csvBody.get(1)[13] = "10";
//            csvBody.get(0)[14] = "111";
//            csvBody.get(1)[14] = "111";
//            csvBody.get(0)[15] = "970";
//            csvBody.get(1)[15] = "120";
//            csvBody.get(0)[16] = "5104002003";
//            csvBody.get(1)[16] = "5104001002";
//            csvBody.get(0)[17] = "000";
//            csvBody.get(1)[17] = "000";
//            csvBody.get(0)[18] = "000000";
//            csvBody.get(1)[18] = "000000";
//            csvBody.get(0)[19] = "00";
//            csvBody.get(1)[19] = "00";
//            csvBody.get(0)[20] = "000";
//            csvBody.get(1)[20] = "000";
//            csvBody.get(0)[21] = "000";
//            csvBody.get(1)[21] = "000";
//            csvBody.get(0)[43] = "END";
//            csvBody.get(1)[43] = "END";
//            reader.close();
//            CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
//            writer.writeAll(csvBody);
//            System.out.println(csvBody.get(0)[0]);
//            writer.flush();
//            writer.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void createTempZip() {
//        try {
//            readAndUpdateFaTransfersT();
//            readAndUpdateFaTransferDistsT();
//
//            List<String> srcFiles =
//                Arrays.asList((tDir + "/FaTransfersT.csv"), (tDir +
//                                                             "/FaTransferDistsT.csv"));
//            FileOutputStream fos;
//
//            fos = new FileOutputStream(tDir + "/FaMassTransfer.zip");
//
//            ZipOutputStream zipOut = new ZipOutputStream(fos);
//            for (String srcFile : srcFiles) {
//                File fileToZip = new File(srcFile);
//                FileInputStream fis = new FileInputStream(fileToZip);
//                ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
//                zipOut.putNextEntry(zipEntry);
//
//                byte[] bytes = new byte[1024];
//                int length;
//                while ((length = fis.read(bytes)) >= 0) {
//                    zipOut.write(bytes, 0, length);
//                }
//                fis.close();
//            }
//            zipOut.close();
//            fos.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void postFileToUCMAndAssetTable() {
//        try {
//            createTempZip();
//            String fileEncoded = encode(tDir + "/FaMassTransfer.zip");
//            String resXml = uploadFileToUcm(fileEncoded, "uploadToUCMTransfer");
//            JSONObject jSONObject = XML.toJSONObject(resXml);
//            System.out.println(jSONObject);
//            JSONObject header = jSONObject.getJSONObject("env:Envelope");
//            JSONObject body = header.getJSONObject("env:Body");
//            JSONObject uploadFileToUcmResponse =
//                body.getJSONObject("ns0:uploadFileToUcmResponse");
//            JSONObject result =
//                uploadFileToUcmResponse.getJSONObject("result");
//            String content = result.optString("content");
//            postFileToAssetTable(content, "uploadTOAssetTableTransfer");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//   
//
//
//    public static void main(String[] args) throws IOException {
//        UpdateLocation tansferAsset = new UpdateLocation();
//         tansferAsset.postFileToUCMAndAssetTable();
//    }
//}
