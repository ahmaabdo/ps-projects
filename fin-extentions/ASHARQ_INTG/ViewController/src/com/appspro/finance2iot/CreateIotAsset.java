package com.appspro.finance2iot;


import com.appspro.bean.AssetBean;

import com.appspro.biReport.RetriveNewAsset;
import com.appspro.database.AssetController;

import utilities.GeneralHelper;

import org.json.JSONArray;
import org.json.JSONObject;


public class CreateIotAsset extends GeneralHelper {

    RetriveNewAsset retriveNewAsset = new RetriveNewAsset();
    AssetController assetController = new AssetController();

    public AssetBean checkAndBuildJson() {
        AssetBean assetBean = new AssetBean();
        AssetBean assetBean2 = new AssetBean();
        try {
            String jsonArray = retriveNewAsset.getAssetData();
            if (!jsonArray.equals("null")) {
                JSONArray ja = new JSONArray(jsonArray);
                for (int ind = 0; ind < ja.length(); ind++) {
                    JSONObject jsonObject = ja.getJSONObject(ind);
                    assetBean.setAssetNumber(jsonObject.optString("ASSET_NUMBER"));
                    assetBean.setDescription(jsonObject.optString("ASSET_DESCRIPTION"));
                    assetBean.setAssetId(jsonObject.optString("ASSET_ID"));
                    assetBean.setTagNumber(jsonObject.optString("TAG_NUMBER"));
                    assetBean.setModelNumber(jsonObject.optString("MODEL_NUMBER"));
                    assetBean.setSerialNumber(jsonObject.optString("SERIAL_NUMBER"));
                    assetBean.setLocationId(jsonObject.optString("LOCATION_ID"));
                    assetBean.setLocationSegment(jsonObject.optString("LOC_SEG"));
                    assetBean.setBookName(jsonObject.optString("BOOK_NAME"));
                    assetBean.setProrateConvention(jsonObject.optString("PRORATE_CONVENTION"));
                    assetBean.setUnit(jsonObject.optString("UNIT"));
                    assetBean.setIntangible(jsonObject.optString("INTANGIBLE"));
                    assetBean.setInPhysicalInventory(jsonObject.optString("IN_PHYISCAL_INVENTORY"));
                    assetBean.setCreateDateSaaS(jsonObject.optString("CREATION_DATE"));
                    String majorCategory = jsonObject.optString("SEGMENT1");
                    String majorCategoryWithOutSpecialChar = majorCategory.replaceAll("[^a-zA-Z0-9]", " ");
                    String minotCategory = jsonObject.optString("SEGMENT2");
                    String minordCategoryWithOutSpecialChar = minotCategory.replaceAll("[^a-zA-Z0-9]", " ");
                    assetBean.setMajorCategory(majorCategoryWithOutSpecialChar);
                    assetBean.setMinorCategory(minordCategoryWithOutSpecialChar);
                    String result = checkAssetType(majorCategoryWithOutSpecialChar);
                    if (result.equals("Failed Created") || result.equals("Already exist")) {
                        System.out.println(result);
                    }
                    
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("name",
                                 minordCategoryWithOutSpecialChar + "--" + assetBean.getAssetNumber());
                    jsonBody.put("type", assetBean.getMajorCategory());
                    jsonBody.put("description", assetBean.getDescription());
                    JSONArray attribute = new JSONArray();
                    JSONObject assetNum = new JSONObject();
                    assetNum.put("name", "Asset_number");
                    assetNum.put("type", "NUMBER");
                    assetNum.put("value",
                                 Integer.parseInt(assetBean.getAssetNumber()));
                    attribute.put(assetNum);
                    JSONObject assetId = new JSONObject();
                    assetId.put("name", "Asset_ID");
                    assetId.put("type", "NUMBER");
                    assetId.put("value",
                                Integer.parseInt(assetBean.getAssetId()));
                    attribute.put(assetId);
                    JSONObject deviceLat = new JSONObject();
                    deviceLat.put("name", "lat");
                    deviceLat.put("type", "DEVICE");
                    JSONObject deviceValueLat = new JSONObject();
                    deviceValueLat.put("device", "none");
                    deviceLat.put("value", deviceValueLat);
                    attribute.put(deviceLat);

                    JSONObject deviceLon = new JSONObject();
                    deviceLon.put("name", "lon");
                    deviceLon.put("type", "DEVICE");
                    JSONObject deviceValueLon = new JSONObject();
                    deviceValueLon.put("device", "none");
                    deviceLon.put("value", deviceValueLon);
                    attribute.put(deviceLon);

                    JSONObject deviceAlt = new JSONObject();
                    deviceAlt.put("name", "alt");
                    deviceAlt.put("type", "DEVICE");
                    JSONObject deviceValueAlt = new JSONObject();
                    deviceValueAlt.put("device", "none");
                    deviceAlt.put("value", deviceValueAlt);
                    attribute.put(deviceAlt);

                    jsonBody.put("attributes", attribute);
                    System.out.println(jsonBody.toString());
                    assetBean2 =
                            addAndDeleteAssetIot(jsonBody.toString(), "createAsset");
                    if (!assetBean2.getStatusCode().equals("200") &&
                        !assetBean2.getStatusCode().equals("201")) {
                        //Insert To DB the error with details
                        System.out.println("Insert This TO DB");
                        JSONObject errorResponse =
                            new JSONObject(assetBean2.getError().toString());
                        String errorDetails =
                            errorResponse.optString("detail");
                        assetBean.setStatusCode(errorResponse.optString("status"));
                        assetBean.setStatus(errorResponse.optString("detail"));
                        String codeStatus = errorResponse.optString("status");
                        assetController.insertAsset(assetBean, "NEW_ASSET");
                        System.out.println(errorDetails);
                        System.out.println(codeStatus);
                    } else {
                        //insert the success to DB
                        assetBean.setStatusCode(assetBean2.getStatusCode());
                        assetBean.setStatus("Succsess Created");
                        System.out.println("Success");
                        assetController.insertAsset(assetBean, "NEW_ASSET");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return assetBean;
    }

    public String checkAssetType(String category) {
        try {
            JSONObject res = getAssetAndDeviceIot(category, "getAssetType");
            int count = res.optInt("count");
            System.out.println(count);
            if (count == 0) {
                String result = creatAssetType(category);
                if (result.equals("Failed Created")) {
                    return "Failed Created";
                } else {
                    return "Success Created";
                }
            } else {
                return "Already exist";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String creatAssetType(String category) {
        try {
            AssetBean assetBean = new AssetBean();

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("name", category);
            jsonBody.put("description", category);
            JSONArray attribute = new JSONArray();

            JSONObject jsonAssetNumber = new JSONObject();
            jsonAssetNumber.put("name", "Asset_number");
            jsonAssetNumber.put("type", "NUMBER");
            jsonAssetNumber.put("required", "false");
            JSONObject propertiesAssetNum = new JSONObject();
            propertiesAssetNum.put("category", "Uncategorized");
            jsonAssetNumber.put("properties", propertiesAssetNum);
            jsonAssetNumber.put("static", "false");
            jsonAssetNumber.put("unique", "false");
            attribute.put(jsonAssetNumber);

            JSONObject jsonAssetId = new JSONObject();
            jsonAssetId.put("name", "Asset_ID");
            jsonAssetId.put("type", "NUMBER");
            jsonAssetId.put("required", "false");
            JSONObject propertiesAssetId = new JSONObject();
            propertiesAssetId.put("category", "Uncategorized");
            jsonAssetId.put("properties", propertiesAssetId);
            jsonAssetId.put("static", "false");
            jsonAssetId.put("unique", "false");
            attribute.put(jsonAssetId);

            JSONObject jsonLat = new JSONObject();
            jsonLat.put("name", "lat");
            jsonLat.put("static", "false");
            jsonLat.put("type", "SENSOR");
            jsonLat.put("required", "false");
            jsonLat.put("dataType", "NUMBER");
            attribute.put(jsonLat);

            JSONObject jsonLon = new JSONObject();
            jsonLon.put("name", "lon");
            jsonLon.put("static", "false");
            jsonLon.put("type", "SENSOR");
            jsonLon.put("required", "false");
            jsonLon.put("dataType", "NUMBER");
            attribute.put(jsonLon);

            JSONObject jsonAlt = new JSONObject();
            jsonAlt.put("name", "alt");
            jsonAlt.put("static", "false");
            jsonAlt.put("type", "SENSOR");
            jsonAlt.put("required", "false");
            jsonAlt.put("dataType", "NUMBER");
            attribute.put(jsonAlt);
            jsonBody.put("attributes", attribute);
            System.out.println(jsonBody.toString());
            assetBean =
                    addAndDeleteAssetIot(jsonBody.toString(), "createAssetType");
            if (!assetBean.getStatusCode().equals("200") &&
                !assetBean.getStatusCode().equals("201")) {
                //Insert To DB the error with details
                System.out.println("Insert This TO DB");
                JSONObject errorResponse =
                    new JSONObject(assetBean.getError().toString());
                String errorDetails = errorResponse.optString("detail");
                assetBean.setStatusCode(errorResponse.optString("status"));
                assetBean.setStatus(errorResponse.optString("detail"));
                String codeStatus = errorResponse.optString("status");
                assetController.insertAsset(assetBean, "NEW_ASSET_TYPE");
                System.out.println(errorDetails);
                System.out.println(codeStatus);
                return "Failed Created";
            } else {
                //insert the success to DB
                assetBean.setStatusCode(assetBean.getStatusCode());
                assetBean.setStatus("Succsess Created");
                System.out.println("Success");
                assetController.insertAsset(assetBean, "NEW_ASSET_TYPE");
                return "Success Created";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) {
        CreateIotAsset getAddAsset = new CreateIotAsset();
        getAddAsset.checkAndBuildJson();
    }
}
