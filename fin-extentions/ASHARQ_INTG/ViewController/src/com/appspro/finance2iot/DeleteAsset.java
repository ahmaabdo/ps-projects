package com.appspro.finance2iot;

import com.appspro.bean.AssetBean;

import com.appspro.biReport.RetriveRetirementAsset;
import com.appspro.database.AssetController;

import utilities.GeneralHelper;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.nio.channels.FileChannel;
import java.nio.file.Files;

import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;


public class DeleteAsset extends GeneralHelper {

    RetriveRetirementAsset retriveRetirementAsset =
        new RetriveRetirementAsset();
    AssetController assetController = new AssetController();

    public AssetBean deleteAssetFromIOT() {
        AssetBean assetBean = new AssetBean();
        AssetBean assetBean2 = new AssetBean();
        try {
            String jsonArray = retriveRetirementAsset.getAssetRetireData();
            System.out.println(jsonArray);
            if (!jsonArray.equals("null")) {
                JSONArray ja = new JSONArray(jsonArray);
                for (int ind = 0; ind < ja.length(); ind++) {
                    JSONObject jsonObject = ja.getJSONObject(ind);
                    assetBean.setAssetNumber(jsonObject.optString("ASSET_NUMBER"));
                    assetBean.setDescription(jsonObject.optString("ASSET_DESCRIPTION"));
                    assetBean.setAssetId(jsonObject.optString("ASSET_ID"));
                    assetBean.setTagNumber(jsonObject.optString("TAG_NUMBER"));
                    assetBean.setModelNumber(jsonObject.optString("MODEL_NUMBER"));
                    assetBean.setSerialNumber(jsonObject.optString("SERIAL_NUMBER"));
                    assetBean.setLocationId(jsonObject.optString("LOCATION_ID"));
                    assetBean.setLocationSegment(jsonObject.optString("LOC_SEG"));
                    assetBean.setBookName(jsonObject.optString("BOOK_NAME"));
                    assetBean.setProrateConvention(jsonObject.optString("PRORATE_CONVENTION"));
                    assetBean.setUnit(jsonObject.optString("UNIT"));
                    assetBean.setIntangible(jsonObject.optString("INTANGIBLE"));
                    assetBean.setInPhysicalInventory(jsonObject.optString("IN_PHYISCAL_INVENTORY"));
                    assetBean.setCreateDateSaaS(jsonObject.optString("CREATION_DATE"));
                    String category = jsonObject.optString("CATEGORY");
                    //                    String categoryFirstPart = category.split("\\-")[0];
                    //                    assetBean.setMajorCategory(categoryFirstPart);
                    String majorCategory = jsonObject.optString("SEGMENT1");
                    String majorCategoryWithOutSpecialChar =
                        majorCategory.replaceAll("[^a-zA-Z0-9]", " ");
                    String minotCategory = jsonObject.optString("SEGMENT2");
                    String minordCategoryWithOutSpecialChar =
                        minotCategory.replaceAll("[^a-zA-Z0-9]", " ");
                    assetBean.setMajorCategory(majorCategoryWithOutSpecialChar);
                    assetBean.setMinorCategory(minordCategoryWithOutSpecialChar);
                    JSONObject res =
                        getAssetAndDeviceIot(assetBean.getMinorCategory() +
                                             "--" + assetBean.getAssetNumber(),
                                             "getByName");
                    int count = res.optInt("count");
                    System.out.println(count);
                    if (count >= 1) {
                        JSONArray item = res.getJSONArray("items");
                        JSONObject insideItem = item.getJSONObject(0);
                        assetBean.setAssetIdIot(insideItem.optString("id"));
                        assetBean2 =
                                addAndDeleteAssetIot(assetBean.getAssetIdIot(),
                                                     "retirement");
                        if ("204".equals(assetBean2.getStatusCode())) {
                            //Insert To DB the sussecc with details
                            System.out.println("Deleted Success");
                            assetBean.setStatus("Success Deleted");
                            assetController.updateStatusDeleted(assetBean.getAssetNumber());
                            assetBean.setStatusCode(assetBean2.getStatusCode());
                            assetController.insertAsset(assetBean,
                                                        "RETIRED_ASSET");
                        } else {
                            System.out.println("The status is not 204");
                            assetBean.setStatus("The " +
                                                assetBean.getAssetNumber() +
                                                " Is not Deleted Please check");
                            assetBean.setStatusCode(assetBean2.getStatusCode());
                            assetController.insertAsset(assetBean,
                                                        "RETIRED_ASSET");
                        }
                    } else {
                        //Insert To DB the error with details
                        assetBean.setStatus("The " +
                                            assetBean.getAssetNumber() +
                                            " Is not exist in IOT");
                        assetBean.setStatusCode(assetBean2.getStatusCode());
                        assetController.insertAsset(assetBean,
                                                    "RETIRED_ASSET");
                        System.out.println("Nullll");
                    }
                }
            } else {
                System.out.print("There is no Asset");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return assetBean;
    }

    public static void main(String[] args) throws FileNotFoundException,
                                                  IOException {
        DeleteAsset deleteAsset = new DeleteAsset();
        deleteAsset.deleteAssetFromIOT();
    }


}
