package com.appspro.Helper;

import com.appspro.bean.AssetBean;
import com.appspro.database.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import java.util.ArrayList;

import java.util.List;

import javax.net.ssl.HostnameVerifier;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONException;
import org.json.JSONObject;

import org.xml.sax.InputSource;

/**
 *
 * @author Husam Al-Masri
 */
public class RestHelper {

    private static RestHelper instance = null;

    public static RestHelper getInstance() {
        if (instance == null)
            instance = new RestHelper();
        return instance;
    }

    private final String InstanceUrl =
        "https://emrb-test.fa.em3.oraclecloud.com:443";
    private final String orgnizationUrl =
        "//hcmCoreSetupApi/resources/latest/organizations";
    private final String biReportUrl =
        "/xmlpserver/services/PublicReportService";
    private final String employeeServiceUrl =
        "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private final String instanceName =
        "https://emrb-test.fa.em3.oraclecloud.com";
    public static String Schema_Name =
        CommonConfigReader.getValue("Schema_Name");
    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public static String getSchema_Name() {
        return Schema_Name;
    }

    public String getBiReportUrl() {
        return biReportUrl;
    }


    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            // SSLContext sc = SSLContext.getInstance("SSL");
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    public static AssetBean callPostRest(String serverUrl, String contentType,
                                         String body,
                                         String type) throws JSONException {
        JSONObject res = new JSONObject();
        AssetBean assetBean = new AssetBean();
        List<AssetBean> errorMsg = new ArrayList<AssetBean>();
        try {
            System.out.println("calling ::" + serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            if (type.equals("FIN")) {
                connection.setRequestProperty("Authorization",
                                              "Basic " + getAuth());
            } else if (type.equals("IOT")) {
                connection.setRequestProperty("Authorization",
                                              "Basic " + getAuthIot());
            }
            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println("code:" + connection.getResponseCode());
            InputStream is;
            if (connection.getResponseCode() >= 400) {
                is = connection.getErrorStream();
                System.out.println(connection.getResponseMessage());
                JSONObject respo = new JSONObject(connection.getErrorStream());
                System.out.println(respo.toString());
                assetBean.setStatusCode(Integer.toString(connection.getResponseCode()));

                res.put("status", "error");

            } else {
                res.put("status", "done");

                is = connection.getInputStream();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            StringBuilder response2 = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
                response2.append(inputLine);
                assetBean.setError(response2.toString());
                errorMsg.add(assetBean);
                if (connection.getResponseCode() != 201 ||
                    connection.getResponseCode() != 200) {
                    //                      System.out.println(response);
                    assetBean.setStatus(response.toString());
                    assetBean.setStatusCode(Integer.toString(connection.getResponseCode()));
                } else {
                    System.out.println(response2.toString());
                }


                //  itemWLotLogBean.setBodyStatus(response.toString());
                res.put("data", assetBean.getStatus());
            }
            assetBean.setError(errorMsg);
            in.close();
            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                try {
                    res.put("data", response.toString());
                } catch (JSONException f) {
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();

            res.put("status", "error");

            res.put("data", assetBean.getStatus());
            res.put("data", "Internal server error");
        }

        return assetBean;
    }


    //    public static String callPostRest(String serverUrl, String contentType,
    //                                      String body, String type) {
    //        try {
    //            System.out.println(body);
    //
    //            URL url =
    //                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
    //            HttpsURLConnection https = null;
    //            HttpURLConnection conn = null;
    //            if (url.getProtocol().toLowerCase().equals("https")) {
    //                trustAllHosts();
    //                https = (HttpsURLConnection)url.openConnection();
    //                https.setHostnameVerifier(DO_NOT_VERIFY);
    //                conn = https;
    //            } else {
    //                conn = (HttpURLConnection)url.openConnection();
    //            }
    //
    //            conn.setDoOutput(true);
    //            conn.setDoInput(true);
    //            conn.setRequestMethod("POST");
    //            conn.setRequestProperty("Content-Type", contentType);
    //            conn.setRequestProperty("Accept", "application/xml");
    //            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
    //            if (type.equals("FIN")) {
    //                conn.addRequestProperty("Authorization", "Basic " + getAuth());
    //            } else if (type.equals("IOT")) {
    //                conn.addRequestProperty("Authorization", "Basic " + getAuthIot());
    //            }
    //            OutputStream os = conn.getOutputStream();
    //            os.write(body.getBytes("UTF-8"));
    //            os.close();
    //
    //            int responseCode = conn.getResponseCode();
    //            BufferedReader br =
    //                new BufferedReader(new InputStreamReader((conn.getInputStream())));
    //
    //            StringBuilder output = new StringBuilder();
    //            String inputLine;
    //
    //            while ((inputLine = br.readLine()) != null) {
    //                output.append(inputLine);
    //            }
    //
    //            conn.disconnect();
    //            System.out.println(responseCode);
    //            if (responseCode == 200) {
    //                return output.toString();
    //            } else if (responseCode == 500) {
    //                return "{\"ReturnStatus\":\"INERTNAL SERVER ERROR\"}";
    //            } else if (responseCode == 201) {
    //                JSONObject reponseJson = null;
    //                try {
    //                    reponseJson = new JSONObject(output.toString());
    //                } catch (JSONException e) {
    //                    e.printStackTrace();
    //                }
    //                return "{\"ReturnStatus\":\"" +
    //                    reponseJson.optString("ReturnStatus") +
    //                    "\",\"ErrorCode\":\"" +
    //                    reponseJson.optString("ErrorCode") +
    //                    "\",\"ErrorExplanation\":\"" +
    //                    reponseJson.optString("ErrorCode") + "\"}";
    //            } else if (responseCode == 400) {
    //                return conn.getErrorStream().toString();
    //            }
    //
    //        } catch (MalformedURLException e) {
    //
    //            e.printStackTrace();
    //
    //        } catch (IOException e) {
    //
    //            e.printStackTrace();
    //
    //        }
    //        return null;
    //    }


    public static String callGetRest(String serverUrl) {

        try {
            System.out.println("calling ::" + serverUrl);
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.addRequestProperty("Authorization", "Basic " + getAuthIot());
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public static AssetBean callDeleteRest(String serverUrl) {
        AssetBean assetBean = new AssetBean();
        try {

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Accept", "application/json");
            conn.addRequestProperty("Authorization", "Basic " + getAuthIot());
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            assetBean.setStatusCode(Integer.toString(conn.getResponseCode()));
            in.close();
            //            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return assetBean;
    }


    public static String httpPostSoapReq(String destUrl,
                                         String postData) throws Exception {
        System.out.println(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte[postData.length()];
        buffer = postData.getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {
            trustAllHosts();
            java.net.HttpURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            //https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);


        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());


        if (http.getResponseCode() == 200) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);


            String line;
            String response = "";
            System.out.println("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }


            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();


            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            System.out.println(response);
            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();
            return response;
        } else {
            System.out.println("Failed");
        }
        return null;
    }

    public static String getRequest(String parameter, String type) {
        String soapRequest = null;
        if (type.equals("uploadToUCMTransfer")) {
            soapRequest =
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\" xmlns:fin=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <typ:uploadFileToUcm>\n" +
                    "         <typ:document>\n" +
                    "            <fin:Content>" + parameter +
                    "</fin:Content>\n" +
                    "            <fin:FileName>FaMassTransfer.zip</fin:FileName>\n" +
                    "            <fin:ContentType>zip</fin:ContentType>\n" +
                    "            <fin:DocumentTitle>Transfer Through web service</fin:DocumentTitle>\n" +
                    "            <fin:DocumentAuthor>Transfer WS</fin:DocumentAuthor>\n" +
                    "            <fin:DocumentSecurityGroup>FAFusionImportExport</fin:DocumentSecurityGroup>\n" +
                    "            <fin:DocumentAccount>fin$/assets$/import$</fin:DocumentAccount>\n" +
                    "         </typ:document>\n" +
                    "      </typ:uploadFileToUcm>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>\n";
        }
        if (type.equals("uploadToUCMDescriptive")) {
            soapRequest =
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\" xmlns:fin=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <typ:uploadFileToUcm>\n" +
                    "         <typ:document>\n" +
                    "            <fin:Content>" + parameter +
                    "</fin:Content>\n" +
                    "            <fin:FileName>FaDescriptiveDetail.zip</fin:FileName>\n" +
                    "            <fin:ContentType>zip</fin:ContentType>\n" +
                    "            <fin:DocumentTitle>Update</fin:DocumentTitle>\n" +
                    "            <fin:DocumentAuthor>Web service</fin:DocumentAuthor>\n" +
                    "            <fin:DocumentSecurityGroup>FAFusionImportExport</fin:DocumentSecurityGroup>\n" +
                    "            <fin:DocumentAccount>fin$/assets$/import$</fin:DocumentAccount>\n" +
                    "         </typ:document>\n" +
                    "      </typ:uploadFileToUcm>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";
        }

        else if (type.equals("uploadTOAssetTableDescriptive")) {
            soapRequest =
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\">\n" +
                    "<soapenv:Header/>\n" +
                    "<soapenv:Body>\n" +
                    "<typ:submitESSJobRequest>\n" +
                    "<typ:jobPackageName>/oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader</typ:jobPackageName>\n" +
                    "<typ:jobDefinitionName>InterfaceLoaderController</typ:jobDefinitionName>\n" +
                    "<typ:paramList>114</typ:paramList>\n" +
                    "<typ:paramList>" + parameter + "</typ:paramList>\n" +
                    "</typ:submitESSJobRequest>\n" +
                    "</soapenv:Body>\n" +
                    "</soapenv:Envelope>\n";
        } else if (type.equals("uploadTOAssetTableTransfer")) {
            soapRequest =
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/financialUtilService/types/\">\n" +
                    "<soapenv:Header/>\n" +
                    "<soapenv:Body>\n" +
                    "<typ:submitESSJobRequest>\n" +
                    "<typ:jobPackageName>/oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader</typ:jobPackageName>\n" +
                    "<typ:jobDefinitionName>InterfaceLoaderController</typ:jobDefinitionName>\n" +
                    "<typ:paramList>11</typ:paramList>\n" +
                    "<typ:paramList>" + parameter + "</typ:paramList>\n" +
                    "</typ:submitESSJobRequest>\n" +
                    "</soapenv:Body>\n" +
                    "</soapenv:Envelope>\n";
        }
        return soapRequest;
    }


    public static String getAuth() {
        byte[] message = ("FIN_IMPL" + ":" + "Oracle@123").getBytes();
        //        byte[] message = ("HCMUser" + ":" + "Oracle@123").getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public static String getAuthIot() {
        byte[] message =
            ("zaid.alfares@appspro-me.com" + ":" + "Appspro@1234").getBytes();
        //        byte[] message = ("HCMUser" + ":" + "Oracle@123").getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

    public String getInstanceUrl() {
        return InstanceUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getEmployeeServiceUrl() {
        return employeeServiceUrl;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getInstanceName() {
        return instanceName;
    }
}
