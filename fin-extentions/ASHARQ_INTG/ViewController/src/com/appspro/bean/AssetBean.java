package com.appspro.bean;

import java.util.List;

public class AssetBean {
    public AssetBean() {
        super();
    }

    private String assetId;
    private String assetNumber;
    private String tagNumber;
    private String modelNumber;
    private String serialNumber;
    private String unit;
    private String description;
    private String locationSegment;
    private String majorCategory;
    private String minorCategory;
    private String bookName;
    private String prorateConvention;
    private String createDateSaaS;
    private String locationId;
    private String inPhysicalInventory;
    private String intangible;
    private String statusCode;
    private String error;
    private String status;
    private String assetIdIot;
    private int batcheNum;


    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetNumber(String assetNumber) {
        this.assetNumber = assetNumber;
    }

    public String getAssetNumber() {
        return assetNumber;
    }

    public void setTagNumber(String tagNumber) {
        this.tagNumber = tagNumber;
    }

    public String getTagNumber() {
        return tagNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setLocationSegment(String locationSegment) {
        this.locationSegment = locationSegment;
    }

    public String getLocationSegment() {
        return locationSegment;
    }

    public void setMajorCategory(String majorCategory) {
        this.majorCategory = majorCategory;
    }

    public String getMajorCategory() {
        return majorCategory;
    }
    
    

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookName() {
        return bookName;
    }


    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setError(List<AssetBean> errorMSG) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setStatus(String bodyStatus) {
        this.status = bodyStatus;
    }

    public String getStatus() {
        return status;
    }


    public void setProrateConvention(String prorateConvention) {
        this.prorateConvention = prorateConvention;
    }

    public String getProrateConvention() {
        return prorateConvention;
    }

    public void setCreateDateSaaS(String createDateSaaS) {
        this.createDateSaaS = createDateSaaS;
    }

    public String getCreateDateSaaS() {
        return createDateSaaS;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setInPhysicalInventory(String inPhysicalInventory) {
        this.inPhysicalInventory = inPhysicalInventory;
    }

    public String getInPhysicalInventory() {
        return inPhysicalInventory;
    }

    public void setIntangible(String intangible) {
        this.intangible = intangible;
    }

    public String getIntangible() {
        return intangible;
    }


    public void setAssetIdIot(String assetIdIot) {
        this.assetIdIot = assetIdIot;
    }

    public String getAssetIdIot() {
        return assetIdIot;
    }


    public void setBatcheNum(int batcheNum) {
        this.batcheNum = batcheNum;
    }

    public int getBatcheNum() {
        return batcheNum;
    }

    public void setMinorCategory(String minorCategory) {
        this.minorCategory = minorCategory;
    }

    public String getMinorCategory() {
        return minorCategory;
    }
}
