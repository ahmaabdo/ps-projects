/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojmessages', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource',
    'ojs/ojoffcanvas', 'ojs/ojbutton', 'jquery', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojdialog'
],
        function (oj, ko, moduleUtils, app, services, commonhelper) {
            function ControllerViewModel() {
                var self = this;
                var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
                var mdQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.MD_UP);
                const getTranslation = oj.Translations.getTranslatedString;

                self.username = ko.observable();
                self.password = ko.observable();
                self.userLogin = ko.observable();
                self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);
                self.mdScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(mdQuery);
                self.loginUser = ko.observable();
                self.messagesDataProvider = ko.observableArray([]);
                self.loginBtnLbl = ko.observable("LOGIN");
                self.logoutLbl = ko.observable();
                self.loginWelcomeText = ko.observable("Welcome back! Please login to your account")
                self.personDetails = ko.observableArray([]);
                self.localeLogin = ko.observable();
                self.UsernameLabel = ko.observable();
                self.passwordLabel = ko.observable();
                self.loginword = ko.observable();
                self.forgotPasswordLabel = ko.observable();
                self.supplierArr = ko.observable();
                self.supplierSiteArr = ko.observable();
                self.businessUnitArr = ko.observable();
                self.invoicesArr = ko.observable();
                self.invoicesReportArr = ko.observable();
                self.message = ko.observable();
                self.isUserLoggedIn = ko.observable(false);
                self.Usernametext = ko.observableArray();
                self.SignIn = ko.observable();
                self.tracker = ko.observable();
                self.forgetPassword = ko.observable();
                self.signoLbl = ko.observable();
                self.changeLbl = ko.observable();
                self.passwordLabel = ko.observable();
                self.userName = ko.observable('');
                self.password = ko.observable('');
                self.loginFailureText = ko.observable();
                self.loginLabel = ko.observable();
                self.disOnLogin = ko.observable(false);
                self.languageSwitch_lng = ko.observable();
                self.confirmMessage = ko.observable();
                self.yes = ko.observable();
                self.no = ko.observable();
                self.loading = ko.observable(false);
                self.refreshViewForLanguage = ko.observable(false);
                self.showDashboard = ko.observable();
                self.language = ko.observable();
                self.languagelable = ko.observable("عربي");

                self.jwt = ko.observable('');
                self.hostUrl = ko.observable('');
                self.userName = ko.observable('');
                self.jwtinurl = ko.observable(false);
                self.isUserLoggedIn = ko.observable(false);
                self.paycount=ko.observable();
                self.isVisable=ko.observable(false);
                self.approveVisable=ko.observable(false);

                var storage = window.localStorage;

                self.paymentNumArr = ko.observableArray([]);
                self.supplierNumberArr = ko.observableArray([]);
                self.postPaymentArr = ko.observableArray([]);
                self.searchArr = ko.observableArray([]);
                self.paymentData = ko.observableArray([]);
  
                //     ----------------------------------------  loading function ------------------------ 

                self.loading = function (load) {
                    if (load) {
                        $('#loading').show();
                    } else {
                        $('#loading').hide();
                    }
                };

                //     ---------------------------------------- message function ------------------------ 

                self.createMessage = function (type, summary) {
                    return {
                        severity: type,
                        summary: summary,
                        autoTimeout: 4000
                    };
                };
              //--------------------------get Post Payment From DB---------------------------------

                self.getpostPaymentDB = function () {
                    self.loading(true);
                    var requestSucess = function (data) {
                        console.log(data);
                       
                        var seq = 0;
                        data.forEach(e =>
                            e.seq = seq++,  )
                        self.paymentData(data)
                        self.searchArr(data);
                      self.postPaymentArr(data)
                        self.loading(false);
                        var suppliersDup = data.map(el => ({SupplierNumber: el.supplierNumber, SupplierNumber: el.supplierNumber}));
                        var suppliersOptionArr = [...new Map(suppliersDup.map(item => [item['SupplierNumber'], item])).values()];
                        self.paymentNumArr(data.map(e => ({label: e.paymentNumber, value: e.paymentNumber})));
                        self.supplierNumberArr(suppliersOptionArr.map(el => ({label: el.SupplierNumber, value: el.SupplierNumber})).filter(el => el.label != ""));
                        console.log(suppliersOptionArr.map(el => ({label: el.SupplierNumber, value: el.SupplierNumber})));
                        
                 

                    };
                    var requestFail = function (data) {
                        app.loading(false);
                        app.messagesDataProvider.push(app.createMessage('error', `${data.statusText}`));
                    };

                    services.getGeneric("payment/postPaymentDB", {}).then(requestSucess, requestFail);

                };
                //     ---------------------------------------- home icon function------------------------ 

                $(document).on('click', '.home-icon', function () {
                    oj.Router.rootInstance.go('paymentDetails');
                });
                //     ---------------------------------------- MT940 icon function------------------------ 
                $(document).on('click', '.info-icon', function () {
                    oj.Router.rootInstance.go('MT940Screen');
                });
                //     ---------------------------------------- home icon function------------------------ 
                $(document).on('click', '.postPayments-icon', function () {
                    self.getpostPaymentDB(); 
                    oj.Router.rootInstance.go('postPayments');
                    
                });
                //     ---------------------------------------- paymentDetails icon function ------------------------ 

                $(document).on('click', '.notifiIcon', function () {
                    oj.Router.rootInstance.go('paymentDetails');
                });


                //     ---------------------------------------- enter to login  ------------------------ 

                $(function () {
                    $('#username').on('keydown', function (ev) {
                        var mobEvent = ev;
                        var mobPressed = mobEvent.keyCode || mobEvent.which;
                        if (mobPressed == 13) {
                            $('#username').blur();
                            $('#password').focus();
                        }
                        return true;
                    });
                });

                //     ---------------------------------------- call jwt from helper ------------------------ 

               


                //     ---------------------------------------- login button function------------------------ 

                self.onLogin = function () {
                                    
                    if (!self.userName() && !self.password()) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter username and password'));
                        return;
                    } else if (!self.userName()) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter user name'));
                        return;
                    } else if (!self.password()) {
                        self.messagesDataProvider.push(self.createMessage('error', 'Please enter password'));
                        return;
                    } 
                     self.disOnLogin(true);
                    $(".apLoginBtn").addClass("loading");
                    self.loginFailureText("");

                    var loginSuccessCbFn = function () {
                        //getAllPayments()
                        self.loading(false);
                        self.messagesDataProvider([]);
                        self.isUserLoggedIn(true);
                        var userAuth = btoa(self.userName()+":"+ self.password())
                        console.log(userAuth)
                        localStorage.jwtLogin = userAuth
//                        if (self.userName() != "appspro.projects") {
//                            oj.Router.rootInstance.go('approvalScreen')
//                            self.isVisable(false);
//                            self.approveVisable(true)
//                        } else {
//                            oj.Router.rootInstance.go('paymentDetails')
//                            self.isVisable(true);
//                            self.approveVisable(false)
//                        }
 
                        if ((self.userName() == "appspro.projects")) {
                            oj.Router.rootInstance.go('paymentDetails')
                            self.isVisable(true);
                            self.approveVisable(false);
                             self.isUserLoggedIn(true);
                             return
                        }
                        if ((self.userName() == "appspro.fin") || (self.userName() == "ibrahim.hassan@appspro-me.com")
                                || (self.userName() == "khadeejah.mahrous@tbc.sa") || (self.userName() == "amro.kalthom@tbc.sa")) {
                            oj.Router.rootInstance.go('approvalScreen')
                            self.isVisable(false);
                            self.approveVisable(true);
                             self.isUserLoggedIn(true);
                             return
                        } else {
                              self.isUserLoggedIn(false);
                            return;
                        }

                    };

                    var loginFailCbFn = function () {
                        self.loading(false);
                        $(".apLoginBtn").removeClass("loading");
                        self.disOnLogin(false);
                        self.isUserLoggedIn(false);
                        self.loginFailureText("error");
                        self.loginFailureText(getTranslation("login.loginFailureText"));
                    };

                    function authinticate(data) {
                        console.log(data)
                        var result = data;
                        var check = data.response;
                        var jwt = data.JWT;
                        console.log(jwt)
                        document.cookie = "JSESSIONSID=" + jwt;
                        if (check) {
                            loginSuccessCbFn();
                        } else {
                            loginFailCbFn();
                        }
                        $(".apLoginBtn").removeClass("loading");
                        self.disOnLogin(false);

                    }
                    self.failCbFn = function(error){
                        $(".apLoginBtn").removeClass("loading");
                        self.disOnLogin(false);
                        self.isUserLoggedIn(false);
                       // self.loginFailureText("error");
                        self.messagesDataProvider.push(self.createMessage('error', `${error.statusText}`))
                    };

                    var payload = {
                        "userName": btoa(self.userName()),
                        "password": btoa(self.password())
                    };
                    services.authenticate(payload).then(authinticate, self.failCbFn);
                   
                };
            
                //     ---------------------------------------- log out button function------------------------ 

                self.logOutBtn = function (event) {
                     location.reload();  
                      
                    self.isUserLoggedIn(false);
                    sessionStorage.setItem('username', '');
                    sessionStorage.setItem('isUserLoggedIn', '');
                      sessionStorage.clear();
                      localStorage.clear();
                      $(".apLoginBtn").removeClass("loading");
                    if (self.language() === 'english') {
                        $('html').attr({'dir': 'ltr'});
                    } else if (self.language() === 'arabic') {
                        $('html').attr({'dir': 'rtl'});
                    }
                


                };
                self.approvalAction = function () {

                    oj.Router.rootInstance.go('approvalScreen')
                };
                //     ---------------------------------------- Router setup function------------------------ 
                // Router setup
                self.router = oj.Router.rootInstance;
                self.router.dispose();

                self.router.configure({
                    'paymentDetails': {label: 'paymentDetailsScreen', value: "paymentDetails/paymentDetailsScreen", id: "paymentDetails", title: "B2B - Payment Details",isDefault: true},
                    'approvalScreen': {label: 'paymentDetailsScreen', value: "paymentDetails/approvalScreen", id: "approvalScreen", title: "B2B - Approval Details"},
                    'MT940Screen': {label: 'MT940Screen', value: "paymentDetails/MT940Screen", id: "MT940Screen", title: "B2B - MT940 Information"},
                    'postPayments': {label: 'postPaymentScreen', value: "paymentDetails/postPaymentScreen", id: "postPayments", title: "B2B - Payment PostPayments"},
                });
                oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

                self.langSelected = function (event) {
                    var valueObj = buildValueChange(event['detail']);
                    self.setPreferedLanguage(valueObj.value);
                };

                function buildValueChange(valueParam) {
                    var valueObj = {};
                    //valueObj.previousValue = valueParam.previousValue;
                    if (valueParam.previousValue) {
                        valueObj.value = valueParam.value;
                    } else {
                        valueObj.value = valueParam.previousValue;
                    }
                    //
                    return valueObj;
                }

                $(function () {
                    storage.setItem('setLang', 'english');
                    //                    storage.setItem('setLang', 'arabic');
                    preferedLanguage = storage.getItem('setLang');
                    self.language(preferedLanguage);
                    changeToArabic(preferedLanguage);
                    //document.addEventListener("deviceready", selectLanguage, false);
                });
                
              

                // Choose prefered language
                function selectLanguage() {
                    oj.Config.setLocale('english',
                            function () {
                                $('html').attr({'lang': 'en-us', 'dir': 'ltr'});
                                self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                self.loginword(oj.Translations.getTranslatedString('login'));
                                self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                            }
                    );
                }

                self.switchl = function () {
                    document.querySelector("#switch").open();
                };
                self.switchclose = function () {
                    document.querySelector("#switch").close();
                };
                self.switchLanguage = function () {
                    document.querySelector("#switch").close();
                    if (self.getLocale() === "ar") {
                        self.setLocale("en-US");
                        localStorage.setItem("selectedLanguage", "en-US");
                    } else if (self.getLocale() === "en-US") {
                        self.setLocale("ar");
                        localStorage.setItem("selectedLanguage", "ar");
                    }

                };

                // Set Prefered Language
                self.setPreferedLanguage = function (lang) {
                    var selecedLanguage = lang;
                    storage.setItem('setLang', selecedLanguage);
                    storage.setItem('setTrue', true);
                    preferedLanguage = storage.getItem('setLang');

                    // Call function to convert arabic
                    changeToArabic(preferedLanguage);
                };

                function changeToArabic(preferedLanguage) {
                    if (preferedLanguage == 'arabic') {

                        oj.Config.setLocale('ar-sa',
                                function () {
                                    $('html').attr({'lang': 'ar-sa', 'dir': 'rtl'});
                                    self.localeLogin(oj.Translations.getTranslatedString('Login'));
                                    self.UsernameLabel(oj.Translations.getTranslatedString('Username'));
                                    self.passwordLabel(oj.Translations.getTranslatedString('Password'));
                                    self.loginword(oj.Translations.getTranslatedString('login'));
                                    self.forgotPasswordLabel(oj.Translations.getTranslatedString('Forgot Password'));
                                }
                        );

                    } else if (preferedLanguage == 'english') {
                        selectLanguage();
                    }
                }

                $('.login-label').toggleClass('user-label-right');
                $('.input100').toggleClass('box-input-text');
                $('.wrap-input100').toggleClass('box-input-text');
                self.changeLanguage = function () {
                    //storage.setItem('setLang', 'arabic');
                     document.querySelector("#switch").close();
                    if (self.language() === 'english') {
                        self.language("arabic");
                        self.loginLabel("تسجيل دخول");
                           self.loginWelcomeText("مرحبا بعودتك! يرجى تسجيل الدخول إلى حسابك")
                        self.setPreferedLanguage(self.language());
                        self.languagelable("English");
                        $('.login-label').toggleClass('user-label-right');
                        $('.login-label').removeClass('user-label-left');
                        $('.input100').toggleClass('box-input-text');
                        $('.wrap-input100').toggleClass('box-input-text');
                    } else if (self.language() === 'arabic') {
                        self.language("english");
                        self.loginLabel("LOGIN")
                           self.loginWelcomeText("Welcome back! Please login to your account")
                        self.setPreferedLanguage(self.language());
                        self.languagelable("عربي");
                        $('.login-label').toggleClass('user-label-left');
                        $('.login-label').removeClass('user-label-right');
                        $('.input100').removeClass('box-input-text');
                        $('.wrap-input100').removeClass('box-input-text');
                    }
                };


                self.moduleConfig = ko.observable({'view': [], 'viewModel': null});

                self.loadModule = async function () {
                    ko.computed(function () {
                        var name = self.router.moduleConfig.name();
                        var viewPath = 'views/' + name + '.html';
                        var modelPath = 'viewModels/' + name;
                        var masterPromise = Promise.all([
                            moduleUtils.createView({'viewPath': viewPath}),
                            moduleUtils.createViewModel({'viewModelPath': modelPath})
                        ]);
                        masterPromise.then(
                                function (values) {
                                    self.moduleConfig({'view': values[0], 'viewModel': values[1]});
                                },
                                function (reason) {}
                        );
                    });
                
                };
   $.when(showEmplDetails(oj, ko, services, self, false, commonhelper)).done(function () {

                    if (self.jwtinurl()) {
                        
                          if ((self.userName() == "appspro.projects")) {
                       
                            self.isVisable(true);
                            self.approveVisable(false);
                            self.isUserLoggedIn(true);
                             self.router.go('paymentDetails');
                        }
                        if ((self.userName() == "appspro.fin") || (self.userName() == "ibrahim.hassan@appspro-me.com")
                                || (self.userName() == "khadeejah.mahrous@tbc.sa") || (self.userName() == "amro.kalthom@tbc.sa")) {
                            
                            self.isVisable(false);
                            self.approveVisable(true);
                             self.isUserLoggedIn(true);

                           self.router.go('approvalScreen');
                        }else{
                            return;
                        }
                    } else {
                        self.isUserLoggedIn(false);
                        self.userLogin(sessionStorage.Username);
                    }
                });
                function initTranslations() {
                    self.Usernametext(getTranslation("login.userName"));
                    self.passwordLabel(getTranslation("login.Password"));
                    self.loginLabel(getTranslation("login.loginLabel"));
                    self.SignIn(getTranslation("login.SignIn"));
                    self.forgetPassword(getTranslation("login.forgetPassword"));
                    self.changeLbl(getTranslation("common.changeLang"));
                    self.signoLbl(getTranslation("labels.signOut"));
                    if (self.loginFailureText() !== undefined && self.loginFailureText() !== "") {
                        self.loginFailureText(getTranslation("login.loginFailureText"));
                    }
                    self.languageSwitch_lng(getTranslation("common.switchLang"));
                    self.confirmMessage(getTranslation("common.confirmMessage"));
                    self.yes(getTranslation("common.yesLbl"));
                    self.no(getTranslation("common.noActionLbl"));
                     self.logoutLbl(getTranslation("common.logoutLbl"));
                }
                initTranslations();

            }

            return new ControllerViewModel();
        }
);