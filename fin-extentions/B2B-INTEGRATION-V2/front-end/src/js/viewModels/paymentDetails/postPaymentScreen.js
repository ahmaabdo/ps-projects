/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * postPaymentScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper, DataCollectionEditUtils) {
    /**
     * The view model for the main content view template
     */
    function postPaymentScreenContentViewModel() {
        var self = this;
        self.label = ko.observable();
        self.customsModel = {
            columnArray: ko.observableArray(),
            postPaymentArr: ko.observableArray(app.postPaymentArr()),
            searchArr: ko.observableArray(app.searchArr()),
            supplierNumberArr: ko.observableArray(app.supplierNumberArr()),
            paymentNumArr: ko.observableArray(app.paymentNumArr()),
            companyCodeVal: ko.observable(),
            SupplierNumberVal: ko.observable(),
            fileReferenceVal: ko.observable(),
            FTReferencVal: ko.observable(),
            transactionTypeVal: ko.observable(),
            debitAccountVal: ko.observable(),
            debitAccountNameVal: ko.observable(),
            creditAccountNameVal: ko.observable(),
            debitRefefenceVal: ko.observable(),
            transactionReferenceVal: ko.observable(),
            creditReferenceVal: ko.observable(),
            creditAccountVal: ko.observable(),
            creditValueDateVal: ko.observable(),
            amountDebitedVal: ko.observable(),
            amountCreditedVal: ko.observable(),
            paymentNumberVal: ko.observable(),
            PaymentNarrative1Val: ko.observable(),
            PaymentNarrative2Val: ko.observable(),
            PaymentNarrative3Val: ko.observable(),
            PaymentNarrative4Val: ko.observable(),
            rowSelected: ko.observable(),
            fieldsDiaDisable: ko.observable(true),
            postPaymentBtnDis: ko.observable(true),
            searchDis: ko.observable(false),
            cancelPaymentBtnDis: ko.observable(true),

        };
        self.dataprovider = ko.observable();
       /// self.dataprovider((new oj.ArrayTableDataSource(self.customsModel.postPaymentArr)));
       self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.postPaymentArr,{idAttribute:"seq"})));



        self.connected = function () {          
            ko.computed(function () {               
                if(app.postPaymentArr().length >0){
                self.customsModel.postPaymentArr(app.postPaymentArr());
                self.customsModel.searchArr(app.searchArr());
                self.customsModel.supplierNumberArr(app.supplierNumberArr());
                self.customsModel.paymentNumArr(app.paymentNumArr());
                }              
            });
        };
        
        
         //-----------------------------search button---------------------------------
          self.searchBtn = function () {     
            cond = function (el) {         
                var res = (self.customsModel.paymentNumberVal() ? el.paymentNumber == self.customsModel.paymentNumberVal() : true) &&
                        (self.customsModel.SupplierNumberVal() ? el.supplierNumber === self.customsModel.SupplierNumberVal() : true) 
                        
                return res;
            };
            filteredData = self.customsModel.searchArr().filter(el => cond(el))              
            self.customsModel.postPaymentArr(filteredData);        
             self.customsModel.cancelPaymentBtnDis(true);
             self.customsModel.postPaymentBtnDis(true);
        };
        
       
        //--------------------------Cancel Daialog ---------------------------------
        self.cancelDialBtn = function () {
            document.querySelector("#paymentDialog").close();
        };
        //--------------------------tableSelectionListener---------------------------------
        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.postPaymentArr()[[currentRow['rowIndex']]];
                if (self.customsModel.rowSelected.transactionStatus =="PAID") {
                    self.customsModel.postPaymentBtnDis(false);
                    self.customsModel.cancelPaymentBtnDis(false);
                    
                    console.log( self.customsModel.rowSelected );
                }else{
                    self.customsModel.postPaymentBtnDis(false);
                    self.customsModel.cancelPaymentBtnDis(true);
                     
                }
            }
        };
    //--------------------------Post Payment Request---------------------------------
        self.getPostPayments = function () {

            app.loading(true);
            var payload = {
                paymentId: self.customsModel.rowSelected.paymentId,
                fileReference: self.customsModel.rowSelected.fileReference,
                transactionReference: self.customsModel.rowSelected.transactionReference,
                username: app.userName(),
                transactionType:"POSTPAYMENTS"
            };
            console.log(payload)
            var requestSucess = function (data) {
                console.log(data)
                             
                if (data.bankResponse.message[0] == "success")
                {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('confirmation', `${data.bankResponse.message[0]}`));

                    var paymentResponse = data.bankResponse.postPaymentRes[0].PaymentMessageResponse.PaymentStatusResponse;
console.log(paymentResponse)
                    self.customsModel.companyCodeVal(data.bankResponse.postPaymentRes[0].PaymentMessageResponse.CompanyCode);
                    self.customsModel.fileReferenceVal(paymentResponse.FileReference);
                    self.customsModel.FTReferencVal(paymentResponse.FTReference);
                    self.customsModel.transactionTypeVal(paymentResponse.TransactionType);
                    self.customsModel.debitAccountVal(paymentResponse.DebitAccount);
                    self.customsModel.debitAccountNameVal(paymentResponse.DebitAccountName);
                    self.customsModel.debitRefefenceVal(paymentResponse.DebitRefefence);
                    self.customsModel.transactionReferenceVal(paymentResponse.TransactionReference);
                    self.customsModel.creditReferenceVal(paymentResponse.CreditReference);
                    self.customsModel.creditAccountVal(paymentResponse.CreditAccount);
                    self.customsModel.amountDebitedVal(paymentResponse.AmountDebited);
                    self.customsModel.amountCreditedVal(paymentResponse.AmountCredited);
                    self.customsModel.creditAccountNameVal(paymentResponse.CreditAccountName);
                    self.customsModel.creditValueDateVal(paymentResponse.CreditValueDate);
                    
                    self.customsModel.PaymentNarrative1Val(paymentResponse.PaymentNarrative1);
                    self.customsModel.PaymentNarrative2Val(paymentResponse.PaymentNarrative2);
                    self.customsModel.PaymentNarrative3Val(paymentResponse.PaymentNarrative3);
                    self.customsModel.PaymentNarrative4Val(paymentResponse.PaymentNarrative4);
                    
                     document.querySelector("#paymentDialog").open();

                } else {
                    app.loading(false);
                     app.messagesDataProvider.push(app.createMessage('error', `${data.bankResponse.message[0]}`));

                }

            };
            var requestFail = function (data) {
                app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', `${data.statusText}`));

            };
            services.postGeneric(commonhelper.createPayment + "paymenttransaction", payload, {"Authorization": commonhelper.authorization}).then(requestSucess, requestFail);
        };
         //------------------------yesNo Dialog Actions---------------------------------
         
        self.yesDialBtn = function () {
              document.querySelector("#yesNoDialog").close();
            callCancelPayment();
         
        };
        self.NoDialBtn = function () {
            document.querySelector("#yesNoDialog").close();
        };


        
        //------------------------Cancel payments Actions---------------------------------
        self.cancelPayments= function (){
            document.querySelector("#yesNoDialog").open();
            
        };
        var callCancelPayment =function(){
                
            
               app.loading(true);
            var payload={
                paymentId:self.customsModel.rowSelected.paymentId,
                username:app.userName(),
                fileReference: self.customsModel.rowSelected.fileReference,
                transactionReference:self.customsModel.rowSelected.transactionReference,
                valueDate:self.customsModel.rowSelected.valueDate,
                transactionType:"CANCELPAYMENT"
                
                
            };
            console.log(payload);
            console.log(self.customsModel.rowSelected)
           
            var requestSucess = function (data){
                console.log(data);            
                if (data.bankResponse.message[0] == "success")
                {
                     app.getpostPaymentDB();
                    app.messagesDataProvider.push(app.createMessage('confirmation', `${data.bankResponse.message[0]}`));

                } else {
                     app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', `${data.bankResponse.message[0]}`));
                }
           };
            var requestFail = function (data){
                app.loading(false);
                 app.messagesDataProvider.push(app.createMessage('error', `${data.statusText}`));
                
            };
            
            services.postGeneric(commonhelper.createPayment + "cancelpayment", payload, {"Authorization": commonhelper.authorization}).then(requestSucess, requestFail);
        }
         //------------------------Translations Actions---------------------------------
        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                getPostPaymentLbl: getTransaltion("paymentDetails.getPostPaymentLbl"),
                postPaymentScreenLbl: getTransaltion("paymentDetails.postPaymentScreenLbl"),
                companeCodeLbl: getTransaltion("paymentDetails.companeCodeLbl"),
                fileReferenceLbl: getTransaltion("paymentDetails.fileReferenceLbl"),
                transactionReferenceLbl: getTransaltion("paymentDetails.FTReferencLbl"),
                FTReferencLbl: getTransaltion("paymentDetails.transactionReferenceLbl"),
                TransactionTypeLbl: getTransaltion("paymentDetails.TransactionTypeLbl"),
                DebitAccountLbl: getTransaltion("paymentDetails.DebitAccountLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                DebitAccountNameLbl: getTransaltion("paymentDetails.DebitAccountNameLbl"),
                DebitReferenceLbl: getTransaltion("paymentDetails.DebitReferenceLbl"),
                CreditReferenceLbl: getTransaltion("paymentDetails.CreditReferenceLbl"),
                creditAccountNameLbl: getTransaltion("paymentDetails.creditAccountNameLbl"),
                creditAccountLbl: getTransaltion("paymentDetails.creditAccountLbl"),
                amountDebitedLbl: getTransaltion("paymentDetails.amountDebitedLbl"),
                amountCreditedLbl: getTransaltion("paymentDetails.amountCreditedLbl"),
                paymentIdLbl: getTransaltion("paymentDetails.paymentIdLbl"),
                cancelPaymentsLbl: getTransaltion("paymentDetails.cancelPaymentsLbl"),
                creditValueDateValLbl: getTransaltion("paymentDetails.creditValueDateValLbl"),
                valuDateLbl: getTransaltion("paymentDetails.valuDateLbl"),
                yesLbl: getTransaltion("common.yesLbl"),
                noActionLbl: getTransaltion("common.noActionLbl"),
                confirmationLbl: getTransaltion("common.confirmationLbl"),
                confirmationMsgLbl: getTransaltion("common.confirmationMsgLbl"),
                paymentNumberLbl: getTransaltion("paymentDetails.paymentNumberLbl"),
                SupplierNumberLbl: getTransaltion("paymentDetails.SupplierNumberLbl"),
                searchLbl: getTransaltion("common.searchLbl"),
                paymentNumberLbl: getTransaltion("paymentDetails.paymentNumberLbl"),
                SupplierNumberLbl: getTransaltion("paymentDetails.SupplierNumberLbl"),
                SupplierNameLbl: getTransaltion("paymentDetails.SupplierNameLbl"),
                placeHolder: getTransaltion("common.placeHolder"),
                transactionStatusLbl: getTransaltion("paymentDetails.transactionStatusLbl"),
                
                PaymentNarrative1Lbl: getTransaltion("paymentDetails.PaymentNarrative1Lbl"),
                PaymentNarrative2Lbl: getTransaltion("paymentDetails.PaymentNarrative2Lbl"),
                PaymentNarrative3Lbl: getTransaltion("paymentDetails.PaymentNarrative3Lbl"),
                PaymentNarrative4Lbl: getTransaltion("paymentDetails.PaymentNarrative4Lbl"),

            });
            self.customsModel.columnArray([               
                {
                    "headerText": self.label().paymentNumberLbl,
                    "field": "paymentNumber"
                },
                {
                    "headerText": self.label().SupplierNameLbl,
                    "field": "supplierName"
                },
                {
                    "headerText": self.label().SupplierNumberLbl,
                    "field": "supplierNumber"
                },
                {
                    "headerText": self.label().fileReferenceLbl,
                    "field": "fileReference"
                },
                {
                    "headerText": self.label().transactionReferenceLbl,
                    "field": "transactionReference"
                },
                {
                    "headerText":  self.label().valuDateLbl,
                    "field": "valueDate"
                },
                {
                    "headerText": self.label().transactionStatusLbl,
                    "field": "transactionStatus"
                }

            ]);
        }
        initTransaltion();
    }

    return postPaymentScreenContentViewModel;
});
