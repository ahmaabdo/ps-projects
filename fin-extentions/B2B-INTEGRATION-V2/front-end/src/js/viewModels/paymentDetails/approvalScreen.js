/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * approvalScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper, DataCollectionEditUtils) {
    /**
     * The view model for the main content view template
     */
    function approvalScreenContentViewModel() {
        var self = this;
        self.customsModel = {
            columnArray: ko.observableArray([]),
            paymentTableArr: ko.observableArray([]),
            notificationArr: ko.observableArray([]),
            attachmentsArr: ko.observableArray([]),
            rowSelected: ko.observable(),
            isDisabled: ko.observable(true),
            fieldsDiaDisable: ko.observable(true),
            paymentNumberVal: ko.observable(),
            paymentStatusVal: ko.observable(),
            SupplierNumberVal: ko.observable(),
            paymentAmountVal: ko.observable(),
            paymentCurrancyVal: ko.observable(),
            paymentDateVal: ko.observable(),
            paymentMethodVal: ko.observable(),
            paymentTypeVal: ko.observable(),
            createdByVal: ko.observable(),
            paymentIdVal: ko.observable(),
            selectedRowKey: ko.observable(),
            paymentDescriptionVal: ko.observable(),
        };
        self.label = ko.observable();
        self.dataTB2 = ko.observableArray([]);
        self.dataSourceTB2 = ko.observable();
        self.approvalList = ko.observable("Approval List");
        self.isLastApproved = ko.observable("appspro.fin");
        self.dataprovider = ko.observable();
        self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.paymentTableArr, {idAttribute: "seq"})));
        self.connected = function () {
            self.getAllPayments();
        };

        //--------------------------approve Action --------------------------
        self.approveBtnAction = function () {

            var headers = {
                "MSG_TITLE": self.customsModel.rowSelected.msgTitle,
                "MSG_BODY": self.customsModel.rowSelected.msgBody,
                "TRS_ID": self.customsModel.rowSelected.requestId,
                "PERSON_ID": app.userName(),
                "RESPONSE_CODE": "APPROVED",
                "ssType": self.customsModel.rowSelected.selfType,
                "PERSON_NAME": self.customsModel.rowSelected.personName
            };
            app.loading(true);
            var approvalActionCBFN = function (data) {

                app.messagesDataProvider.push(app.createMessage('confirmation', 'Approved'));
                

                if (app.userName() == "appspro.fin" || app.userName() == "amro.kalthom@tbc.sa") {
                     self.updatePaymentStatus(self.customsModel.rowSelected.requestId, "APPROVED")
                    app.loading(true);
                    var payload = {
                        paymentId: self.customsModel.rowSelected.paymentId.toString(),
                        username: app.userName(),
                        transactionType: "MT101",
                        paymentDate: self.customsModel.rowSelected.paymentDate,
                        paymentCode: self.customsModel.rowSelected.paymentCode,
                        toAccount: self.customsModel.rowSelected.toAccount,
                        bankCode:self.customsModel.rowSelected.bankCode,
                        bankName:self.customsModel.rowSelected.bankName,
                    };
                    var paymnetSucess = function (data) {
                        self.customsModel.paymentTableArr([]);
                        self.getAllPayments();
                        self.customsModel.isDisabled(true);
                       

                        //  app.messagesDataProvider.push(app.createMessage('information', `${data.bankResponse.message[0]}`))
                        if (data.bankResponse.message[0] == "success")
                        {
                            app.loading(false);
                            app.messagesDataProvider.push(app.createMessage('confirmation', `Payment Posted Successfully`));
                        } else {
                            app.loading(false);
                            app.messagesDataProvider.push(app.createMessage('error', `${data.bankResponse.message[0]}`));
                        }
                    };
                    var paymentFail = function (fail) {
                        app.loading(false)
                        app.messagesDataProvider.push(app.createMessage('error', `${fail.statusText}`));
                    };
                    services.postGeneric(commonhelper.createPayment + self.customsModel.rowSelected.paymentOperation, payload, {"Authorization": commonhelper.authorization}).then(paymnetSucess, paymentFail);
                } else {
                    app.loading(false)
                    self.customsModel.paymentTableArr([]);
                    self.getAllPayments();
                    self.customsModel.isDisabled(true);
                }
            };
            var failCbFn = function () {

            };
            services.postGeneric("workFlowNotification", headers).then(approvalActionCBFN, failCbFn);
        };
        //--------------Reject Action --------------------------
        self.rejectBtnAction = function () {
            var headers = {
                "MSG_TITLE": self.customsModel.rowSelected.msgTitle,
                "MSG_BODY": self.customsModel.rowSelected.msgBody,
                "TRS_ID": self.customsModel.rowSelected.requestId,
                "PERSON_ID": app.userName(),
                "RESPONSE_CODE": "REJECTED",
                "ssType": self.customsModel.rowSelected.selfType,
                "PERSON_NAME": self.customsModel.rowSelected.personName
            };
            app.loading(true)
            var approvalActionCBFN = function (data) {
                app.loading(false)
                app.messagesDataProvider.push(app.createMessage('confirmation', 'Rejected'));
                self.customsModel.paymentTableArr([]);
                self.getAllPayments();
                self.customsModel.isDisabled(true);
                self.updatePaymentStatus(self.customsModel.rowSelected.requestId, "REJECTED")
            };
            var failCbFn = function (fail) {
                app.loading(false)
                app.messagesDataProvider.push(app.createMessage('error', `${fail.statusText}`));
            };
            services.postGeneric("workFlowNotification", headers).then(approvalActionCBFN, failCbFn);
        };
        //-----------------Selection Action --------------------------

        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
            self.customsModel.rowSelected = self.customsModel.paymentTableArr()[index];
            self.customsModel.selectedRowKey(self.customsModel.paymentTableArr()[index].requestId);
            self.customsModel.isDisabled(false);
            console.log(self.customsModel.rowSelected);

        });

        //     --------------------------------------- view approval function button ------------------------ 

        self.viewApproval = function () {
            $(".apListBtn").addClass("loading");
            var getApprovalList = function (data) {
                console.log(data)
                $(".apListBtn").removeClass("loading");
                self.dataTB2([]);
                // var data = data ;
                // data.pop();
                console.log(data)
                var lineManger = data.find(e => e.rolrType == "LINE_MANAGER").roleId;
                var projectManager = data.find(e => e.rolrType == "PROJECT_MANAGER").roleId
                var checkStatusPending = false;
                $.each(data, function (i, item) {
                    var notificationStatus, bodyCardStatus, cardStatus;

                    if (data[i].notificationType == "FYA") {

                        if (data[i].responseCode == "REJECTED") {
                            bodyCardStatus = 'app-crd-bdy-border-fail';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-rj';

                        } else if (data[i].responseCode == null) {

                            bodyCardStatus = 'app-crd-bdy-border-pen';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-pen';

                        } else if (data[i].responseCode == "APPROVED") {
                            bodyCardStatus = 'app-crd-bdy-border-suc';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-app';
                        }
                    } else if (data[i].notificationType == "FYI") {

                        bodyCardStatus = 'app-crd-bdy-border-ntre';
                        notificationStatus = 'app-type-i';
                        cardStatus = 'app-card-prog-type-ntr';
                    }
                    if (!item.responseCode && !checkStatusPending) {
                        checkStatusPending = true;
                    }
                    self.dataTB2.push({

                        name: item.rolrType == 'EMP' ? "Appspro.projects"
                                : item.rolrType == 'LINE_MANAGER' ? lineManger
                                : item.rolrType == 'PROJECT_MANAGER' ? projectManager : item.roleId,

                        type: item.notificationType,
                        status: !item.responseCode && checkStatusPending && item.notificationType == 'FYA' ? 'PENDING' : item.responseCode,
                        bodyStatus: bodyCardStatus,
                        appCardStatus: cardStatus,
                        aistatus: notificationStatus,
                        approvalDate: item.responseDate

                    });
                });
                self.dataSourceTB2(new oj.ArrayTableDataSource(self.dataTB2));
                document.querySelector("#approvalDialog").open();
            };
            if (self.customsModel.selectedRowKey()) {
                var headers = {

                };
                services.getGeneric(commonhelper.getApprovalList + 'XX_PAYMENT_REQUEST/' + self.customsModel.selectedRowKey(), headers).then(getApprovalList, app.failCbFn);
            }
        };
        self.closeDialog = function () {
            document.querySelector("#approvalDialog").close();
        };
        //----------------------get All payments --------------------------

        self.getAllPayments = function () {
            app.loading(true);
            var getSuccs = function (data) {
                console.log(data)
                var requestIdArr = data.map(e => e.requestId - 0);
                var payload = [];
                for (var i in requestIdArr) {
                    payload.push({"id": requestIdArr[i]});
                }
                self.customsModel.notificationArr(data);

                var getSuccs = function (resData) {
                    console.log(resData)
                    var seq = 0
                    resData.forEach(e =>
                        e.seq = seq++, )
                    for (var i = 0; i < resData.length; i++) {
                        for (var x = 0; x < self.customsModel.notificationArr().length; x++) {
                            if (resData[i].id == self.customsModel.notificationArr()[x].requestId) {
                                var merge = {...resData[i], ...self.customsModel.notificationArr()[x]}
                                self.customsModel.paymentTableArr().push(merge);
                            }
                        }
                    }
                    self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.paymentTableArr, {idAttribute: "seq"})));
                    app.loading(false)
                };
                var failCbFn = function (fail) {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', `${fail.statusText}`));
                };
                services.postGeneric("payment/getPaymetById", payload).then(getSuccs, failCbFn);

            };
            var failCbFn = function () {

            };
            var payload = {
                "MANAGER_ID": app.userName(),
                "emp": app.userName(),
                "MANAGER_OF_MANAGER": app.userName()
            };
            services.postGeneric("workFlowNotification/allNotifications", payload).then(getSuccs, failCbFn);

        };
        //------------------View Button Actions --------------------------
        self.viewBtnAction = function () {
            $(".apLoginBtn").addClass("loading");
            console.log(self.customsModel.rowSelected.paymentId);
            var getSuccs = function (data) {
                console.log(data);
                $(".apLoginBtn").removeClass("loading");
                self.customsModel.paymentNumberVal(data[0].PaymentNumber);
                self.customsModel.paymentStatusVal(data[0].PaymentStatus);
                self.customsModel.SupplierNumberVal(data[0].SupplierNumber);
                self.customsModel.paymentAmountVal(data[0].PaymentAmount);
                self.customsModel.paymentCurrancyVal(data[0].PaymentCurrency);
                self.customsModel.paymentDateVal(data[0].PaymentDate);
                self.customsModel.paymentMethodVal(data[0].PaymentMethod);
                self.customsModel.paymentTypeVal(data[0].PaymentType);
                self.customsModel.createdByVal(data[0].CreatedBy);
                self.customsModel.paymentIdVal(data[0].PaymentId);
                self.customsModel.paymentDescriptionVal(data[0].PaymentDescription);
                document.querySelector("#viewDialog").open();
            };
            var failCbFn = function (fail) {
                console.log(fail);
            };
            services.postGeneric("paymentprocess/GetPayment/" + self.customsModel.rowSelected.paymentId, {}, {"Authorization": commonhelper.authorization}).then(getSuccs, failCbFn);
        };
        self.cancelViewDialBtn = function () {
            document.querySelector("#viewDialog").close();
        };

        self.viewAttachmentsBtnAction = function () {
            $(".apAttBtn").addClass("loading");
            var jwtLogin = localStorage.getItem("jwtLogin");
            var jwt = localStorage.getItem("jwt");
            if (jwtLogin) {
                var successCbFn = function (data) {
                    if (data.length == 0) {
                        app.messagesDataProvider.push(app.createMessage('error', `there is no attachment for this payment`));
                        $(".apAttBtn").removeClass("loading");
                        return;
                    } else {
                        console.log(data)
                        self.customsModel.attachmentsArr(data);
                        $(".apAttBtn").removeClass("loading");
                        document.querySelector("#attachmentsDialog").open();
                    }

                };
                var failCbFn = function () {
                };
                services.getGeneric("payment/getPaymentAttachmentBasic/" + self.customsModel.rowSelected.paymentId, {"jwt": jwtLogin}).then(successCbFn, failCbFn);

            }
            if (jwt) {
                var successCbFn = function (data) {
                    if (data.length == 0) {
                        app.messagesDataProvider.push(app.createMessage('warning', `there is no attachment for this payment`));
                        $(".apAttBtn").removeClass("loading");
                        return;
                    } else {
                        console.log(data)
                        self.customsModel.attachmentsArr(data);
                        $(".apAttBtn").removeClass("loading");
                        document.querySelector("#attachmentsDialog").open();
                    }
                };
                var failCbFn = function () {
                };
                services.getGeneric("payment/getPaymentAttachment/" + self.customsModel.rowSelected.paymentId, {"jwt": jwt}).then(successCbFn, failCbFn);
            }

        };
        self.cancelAttachments = function () {
            document.querySelector("#attachmentsDialog").close();
        };

        self.updatePaymentStatus = function (id, status) {
            var payload = {

                "id": id,
                "status": status
            };
            getSuccs = function () {

            };
            failCbFn = function () {

            };
            services.postGeneric("payment/updatePaymentApprovalStatus", payload, {"Authorization": commonhelper.authorization}).then(getSuccs, failCbFn);


        }
        //------------------Translation --------------------------
        var getTransaltion = oj.Translations.getTranslatedString;
        function initTransaltion() {
            self.label({
                paymentNumberLbl: getTransaltion("paymentDetails.paymentNumberLbl"),
                paymentStatusLbl: getTransaltion("paymentDetails.paymentStatusLbl"),
                SupplierNumberLbl: getTransaltion("paymentDetails.SupplierNumberLbl"),
                paymentAmountLbl: getTransaltion("paymentDetails.paymentAmountLbl"),
                paymentCurrancyLbl: getTransaltion("paymentDetails.paymentCurrancyLbl"),
                paymentDescriptionLbl: getTransaltion("paymentDetails.paymentDescriptionLbl"),
                paymentDateLbl: getTransaltion("paymentDetails.paymentDateLbl"),
                paymentMethodLbl: getTransaltion("paymentDetails.paymentMethodLbl"),
                paymentTypeLbl: getTransaltion("paymentDetails.paymentTypeLbl"),
                createdByLbl: getTransaltion("paymentDetails.createdByLbl"),
                paymentIdLbl: getTransaltion("paymentDetails.paymentIdLbl"),
                operationTypesLbl: getTransaltion("paymentDetails.operationTypesLbl"),
                approvalscreenLbl: getTransaltion("paymentDetails.approvalscreenLbl"),
                approveLbl: getTransaltion("common.approveLbl"),
                rejectLbl: getTransaltion("common.rejectLbl"),
                viewLbl: getTransaltion("paymentDetails.viewLbl"),
                screenLbl: getTransaltion("paymentDetails.screenLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                viewApprovalLabel: getTransaltion("common.viewApprovalLabel"),
                viewAttachmentsLbl: getTransaltion("paymentDetails.viewAttachmentsLbl"),
                paymentcodeLbl: getTransaltion("paymentDetails.paymentcodeLbl"),
                   bankNameLbl: getTransaltion("paymentDetails.bankNameLbl"),
                   IBANLbl: getTransaltion("paymentDetails.IBANLbl"),
            });

            self.customsModel.columnArray([

                {
                    "headerText": "NO",
                    "template": "seqTemplate"
                },
                {
                    "headerText": self.label().paymentDateLbl,
                    "field": "paymentDate"
                },
                {
                    "headerText": self.label().paymentNumberLbl,
                    "field": "paymentNumber"
                },
                {
                    "headerText": self.label().paymentcodeLbl,
                    "field": "paymentCode"
                },
                {
                    "headerText": self.label().bankNameLbl,
                    "field": "bankName"
                },
                {
                    "headerText": self.label().IBANLbl,
                    "field": "toAccount"
                },

                {
                    "headerText": self.label().paymentStatusLbl,
                    "field": "paymentStatus"
                },
                {
                    "headerText": self.label().paymentTypeLbl,
                    "field": "paymentType"
                },
                {
                    "headerText": self.label().SupplierNumberLbl,
                    "field": "supplierNumber"
                }
            ]);
        }
        initTransaltion();
    }

    return approvalScreenContentViewModel;
});
