/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * paymentDetailsScreen module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'xlsx','ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper,x, DataCollectionEditUtils) {
    /**
     * The view model for the main content view template
     */
    function paymentDetailsScreenContentViewModel() {
        var self = this;
        var filteredData;
        self.label = ko.observable();
        self.groupValid = ko.observable();
        self.customsModel = {
            paymentNumberVal: ko.observable(),
            paymentStatusVal: ko.observable(),
            SupplierNumberVal: ko.observable(),
            SupplierNameVal: ko.observable(),
            supplierNameSearch: ko.observable(),
            paymentAmountVal: ko.observable(),
            paymentCurrancyVal: ko.observable(),
            paymentDateVal: ko.observable(),
            paymentMethodVal: ko.observable(),
            paymentTypeVal: ko.observable(),
            transactionStatusVal: ko.observable(),
           bankNameVal: ko.observable(),
           bankCodeVal: ko.observable(),
            createdByVal: ko.observable(),
            paymentIdVal: ko.observable(),
            paymentDateVal: ko.observable(),
            paymentPurposeCodeVal: ko.observable("11"),
            sysDate: ko.observable(),
            maxPaymentDate: ko.observable(),
            selectedRowKey: ko.observable(),
            paymentDescriptionVal: ko.observable(),
            toAccountNumberVal: ko.observable(),
            approvalStatusVal: ko.observable(),
            operationTypesVal: ko.observable("mt101"),
            rowSelected: ko.observable(),
            searchDis: ko.observable(false),
            fieldsDiaDisable: ko.observable(true),
            payDisable: ko.observable(true),
            viewPayDisable: ko.observable(true),
            viewApprovalDisable: ko.observable(true),
            operationTypeArr: ko.observableArray([
                {label: "MT-101", value: "mt101"},          
              
            ]),
            approvalStatusArr: ko.observableArray([
                {label: "APPROVED", value: "APPROVED"},
                {label: "REJECTED", value: "REJECTED"},
                {label: "PENDING APPROVE", value: "PENDING APPROVE"},
            ]),
            paymentPurposeCode: ko.observableArray([
                {label: "01	Advance Payment", value: "01"},          
                {label: "02	Allowance", value: "02"},          
                {label: "03	Bonus", value: "03"},          
                {label: "04	Business Related payment", value: "04"},          
                {label: "05	Cargo Services", value: "05"},          
                {label: "06	Commission", value: "06"},          
                {label: "07	Compensation", value: "07"},          
                {label: "08	Credit Card Payment", value: "08"},          
                {label: "09	Down Payment  ", value: "09"},          
                {label: "10	End of Service / Final Settlement", value: "10"},          
                {label: "11	Invoice Settlement", value: "11"},          
                {label: "12	Loan repayment", value: "12"},          
                {label: "13	Miscellaneous1", value: "13"},          
                {label: "14	Overtime", value: "14"},          
                {label: "15	Payment for Investment", value: "15"},          
                {label: "16	Remittance/ Family Maintenance", value: "16"},          
                {label: "17	Salary", value: "17"},          
                {label: "18	Standing Order", value: "18"},          
                {label: "19	Tickets", value: "19"},                       
            ]),

            paymentTableArr: ko.observableArray([]),
            supplierNameArr: ko.observableArray([]),
            supplierNumberArr: ko.observableArray([]),
            paymentNumArr: ko.observableArray([]),
            paymentsArr: ko.observableArray([]),
            paymentStatusArr: ko.observableArray([]),
            bankAccountsNames: ko.observableArray([]),
            columnArray: ko.observableArray([]),
            toAccountNumArr: ko.observableArray(),
            attachmentsArr: ko.observableArray([]),
        };
        self.selectedItems = ko.observableArray([]);
        self.headerCheckStatus = ko.observable();
        self.dataproviderRetention = ko.observable();
        self.dataproviderRetention(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.paymentTableArr, {idAttribute: 'PaymentId'})));
        self.dataTB2 = ko.observableArray([]);
        self.dataSourceTB2 = ko.observable();
         self.approvalList = ko.observable("Approval List");

        self.connected = function () {
            app.loading(true);
            self.getAllPayments();
            
            var sysDate = new Date(new Date().setDate(new Date().getDate() -1)).toISOString()
            var maxPaymentDate = new Date(new Date().setDate(new Date().getDate() +14)).toISOString();
            self.customsModel.sysDate(sysDate);
            self.customsModel.maxPaymentDate(maxPaymentDate);
            console.log(sysDate);
            console.log(maxPaymentDate);
            
        };
         //-------------------------------- table Selection ----------------------------

        self.tableSelectionListener = function (event) {
            var data = event.detail;
            var currentRow = data.currentRow;
            if (currentRow) {
                self.customsModel.rowSelected = self.customsModel.paymentTableArr()[[currentRow['rowIndex']]];

                  self.customsModel.selectedRowKey(self.customsModel.paymentTableArr()[[currentRow['rowIndex']]].RequestId);

                  if( self.customsModel.selectedRowKey() == ""){
                      self.customsModel.viewApprovalDisable(true)
                  }else{
                     self.customsModel.viewApprovalDisable(false) 
                  }
                if (self.customsModel.rowSelected.TransactionBankStatus =="PAID" ||self.customsModel.rowSelected.ApprovalStatus =="PENDING APPROVE") {
                    self.customsModel.payDisable(true);
                    self.customsModel.viewPayDisable(false);
                }else{
                  self.customsModel.payDisable(false);  
                  self.customsModel.viewPayDisable(false);  
                }
            }

        };
         //-------------------------------- supplierName Value Changed----------------------------
        self.supplierNameValueChanged=function(event){
            if (event['detail'].originalEvent) {
                var label = event['detail'].originalEvent['srcElement'].innerText
                self.customsModel.supplierNameSearch(label)
                if (label == self.customsModel.SupplierNameVal()) {
                    self.customsModel.SupplierNumberVal('')
                } else {
                    self.customsModel.SupplierNumberVal(event['detail'].value)
                }
            }
        };
        //-------------------------------- Get ALl Payments----------------------------
        self.getAllPayments = function () {
            var paymentDetailsCbFn = function (data) {
                console.log(data);
                var suppliersDup = data.map(el => ({SupplierName: el.Payee, SupplierNmber: el.SupplierNumber}));
                var suppliersOptionArr = [...new Map(suppliersDup.map(item => [item['SupplierName'], item])).values()];
                self.customsModel.supplierNameArr(suppliersOptionArr.map(el => ({label: el.SupplierName, value: el.SupplierNmber == null ? el.SupplierName : el.SupplierNmber})));
                self.customsModel.supplierNumberArr(suppliersOptionArr.map(el => ({label: el.SupplierNmber, value: el.SupplierNmber})).filter(el => el.label != null));
                var trsStatus = new Set([... data.map(el => el.TransactionBankStatus)]);
                var paymentStatusOp = [...trsStatus].filter(el => el != '').map(el => ({label: el, value: el}));
                self.customsModel.paymentNumArr(data.map(e => ({label: e.PaymentNumber, value: e.PaymentNumber})));
                self.customsModel.paymentStatusArr(paymentStatusOp);
                self.customsModel.paymentsArr(data);
                self.customsModel.paymentTableArr(data);
                filteredData =self.customsModel.paymentsArr();
                self.dataproviderRetention(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.paymentTableArr, {idAttribute: 'PaymentId'})));
                app.loading(false);
            };
            var failCbFn = function (event) {
                if (event) {
                    app.messagesDataProvider.push(app.createMessage('error', `${event.statusText}`));
                }
            };
           // {"Authorization": commonhelper.authorization}
            services.getGeneric(commonhelper.paymentsUrl,{}).then(paymentDetailsCbFn, failCbFn);
        };
        //--------------------------------Search Function----------------------------
        self.searchBtn = function () {
            filteredData = [];
            cond = function (el) {
                var res = (self.customsModel.paymentNumberVal() ? el.PaymentNumber == self.customsModel.paymentNumberVal() : true) &&
                        (self.customsModel.paymentStatusVal() ? el.TransactionBankStatus === self.customsModel.paymentStatusVal() : true) &&
                        (self.customsModel.SupplierNumberVal() ? el.SupplierNumber === self.customsModel.SupplierNumberVal() : true) &&
                        (self.customsModel.approvalStatusVal() ? el.ApprovalStatus === self.customsModel.approvalStatusVal() : true) &&
                        (self.customsModel.supplierNameSearch() ? el.Payee == self.customsModel.supplierNameSearch() : true)
                return res;
            };
            filteredData = self.customsModel.paymentsArr().filter(el => cond(el))
            self.customsModel.paymentTableArr(filteredData);
            self.dataproviderRetention(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.paymentTableArr, {idAttribute: 'PaymentId'})));
            self.customsModel.payDisable(true);
            self.customsModel.viewPayDisable(true);
        };

        self.resetBtn = function () {
            self.customsModel.paymentNumberVal('');
            self.customsModel.paymentStatusVal('');
            self.customsModel.SupplierNumberVal('');
            self.customsModel.SupplierNameVal('');
            self.customsModel.supplierNameSearch('');
            self.customsModel.paymentDateVal('')
            self.customsModel.approvalStatusVal('')
            self.customsModel.paymentTableArr(self.customsModel.paymentsArr())
            filteredData = self.customsModel.paymentsArr()
        };
        //--------------------------------Submit Dialog Function----------------------------
        self.submitBtnAction = function () {

            $(".appSubmit").addClass("loading");
            self.customsModel.paymentNumberVal(self.customsModel.rowSelected.PaymentNumber);
            self.customsModel.paymentStatusVal(self.customsModel.rowSelected.PaymentStatus);
            self.customsModel.SupplierNumberVal(self.customsModel.rowSelected.SupplierNumber);
            self.customsModel.paymentAmountVal(self.customsModel.rowSelected.PaymentAmount);
            self.customsModel.paymentCurrancyVal(self.customsModel.rowSelected.PaymentCurrency);
            self.customsModel.paymentMethodVal(self.customsModel.rowSelected.PaymentMethod);
            self.customsModel.paymentTypeVal(self.customsModel.rowSelected.PaymentType);
            self.customsModel.createdByVal(self.customsModel.rowSelected.CreatedBy);
            self.customsModel.paymentIdVal(self.customsModel.rowSelected.PaymentId);
            self.customsModel.paymentDescriptionVal(self.customsModel.rowSelected.PaymentDescription);

     //--------------------------------iban Check----------------------------------
            if (self.customsModel.SupplierNumberVal() == null) {

                self.customsModel.toAccountNumArr([]);
                self.customsModel.bankAccountsNames([]);
                self.customsModel.toAccountNumberVal('');

                var submitEmpSucess = function (resData) {
                    
                    self.customsModel.toAccountNumArr.push({label: resData.IBAN, value: resData.IBAN});
                    self.customsModel.bankNameVal(resData.BankName);
                    self.customsModel.bankCodeVal(resData.BankCode);
                    self.customsModel.toAccountNumberVal(resData.IBAN)
                    
                    $(".appSubmit").removeClass("loading");
                    document.querySelector("#paymentDialog").open();
                    
                };
                var submitEMpFail = function () {
                    
                    $(".appSubmit").removeClass("loading");
                };

          services.getGeneric("payment/getEmployeeBankDetails/" + self.customsModel.rowSelected.ExternalBankAccountId,{}).then(submitEmpSucess, submitEMpFail);
            
            } else {
                
                self.customsModel.toAccountNumArr([])
                self.customsModel.toAccountNumberVal('');

                var submitSucess = function (data) {

                    var ibanArr = data.responseData.DATA_DS.G_1 ?data.responseData.DATA_DS.G_1:[];
                    
                    if (Array.isArray(ibanArr)) {
                        self.customsModel.bankNameVal('');
                        for (var i in ibanArr) {

                            self.customsModel.bankAccountsNames(ibanArr.map(e => ({IBAN: e.IBAN_NUMBER, bankName: e.BANK_NAME,bankCode:e.BANK_NAME_ALT})))
                            self.customsModel.toAccountNumArr.push({label: ibanArr[i].IBAN_NUMBER, value: ibanArr[i].IBAN_NUMBER});
                        }
                    } else {

                        if(ibanArr != []){
                           self.customsModel.bankAccountsNames([]);
                        self.customsModel.toAccountNumArr.push({label: ibanArr.IBAN_NUMBER, value: ibanArr.IBAN_NUMBER});
                        self.customsModel.bankNameVal(ibanArr.BANK_NAME);
                        self.customsModel.bankCodeVal(ibanArr.BANK_NAME_ALT);
                         self.customsModel.toAccountNumberVal(ibanArr.IBAN_NUMBER)
                        }
                       
                    }
                    $(".appSubmit").removeClass("loading");
                    document.querySelector("#paymentDialog").open();
                };
                submitFail = function () {
                };
                services.getGeneric("payment/getSupplierDetailsByNumber/" + self.customsModel.rowSelected.SupplierNumber, {}).then(submitSucess, submitFail);
            }
        };
         //--------------------------------Submit Dialog Action----------------------------------
        self.submitDialBtn = function () {
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }        
            app.loading(true);
            document.querySelector("#paymentDialog").close();
            self.customsModel.payDisable(true);
            self.customsModel.viewPayDisable(true);
            //------Submit payment Approval Cycle -------
            var payload = {
                "paymentId": self.customsModel.rowSelected.PaymentId,
                "paymentDate": self.customsModel.paymentDateVal(),//self.customsModel.rowSelected.PaymentDate,
                "paymentStatus": self.customsModel.rowSelected.PaymentStatus,
                "paymentType": self.customsModel.rowSelected.PaymentType,
                "supplierNumber": self.customsModel.rowSelected.SupplierNumber,
                "personName": app.userName(),
                "createdBy": app.userName(),
                "approvalStatus": "PENDING APPROVE",
                "paymentOperation": self.customsModel.operationTypesVal(),
                "paymentAmount":self.customsModel.rowSelected.PaymentAmount,
                "paymentCode":self.customsModel.paymentPurposeCodeVal(),
                "toAccount":self.customsModel.toAccountNumberVal(),
                "paymentNumber":self.customsModel.rowSelected.PaymentNumber.toString(),
                "bankName":self.customsModel.bankNameVal(),
                "bankCode":self.customsModel.bankCodeVal()
            };
            var submitSucess = function (data) {
            app.loading(false);
              app.messagesDataProvider.push(app.createMessage('confirmation', ` Submit Payment Success`));
                  self.resetBtn();
                  self.getAllPayments();
            };
            var submitFail = function (fail) {
               app.loading(false);
                 app.messagesDataProvider.push(app.createMessage('error', `${fail.statusText}`));
            };
            services.postGeneric("payment/submitRequest", payload, {"Authorization": commonhelper.authorization}).then(submitSucess, submitFail);

            self.customsModel.operationTypesVal('');
        };
        self.cancelDialBtn = function () {
            document.querySelector("#paymentDialog").close();
            self.resetBtn();

        };
         //--------------------------------toAccountValueChanged Function----------------------------
        self.toAccountValueChanged = function (event) {
            console.log(event.detail.value)
            if (event['detail'].originalEvent) {
                if (self.customsModel.bankAccountsNames().length > 0) {
                    var bankName = self.customsModel.bankAccountsNames().find(e => e.IBAN == event.detail.value).bankName;
                    var bankCode = self.customsModel.bankAccountsNames().find(e => e.IBAN == event.detail.value).bankCode;
                    self.customsModel.bankNameVal(bankName);
                    self.customsModel.bankCodeVal(bankCode);
                }
            };
        };
        //--------------------------------view Function----------------------------
        self.viewBtnAction = function () {

            self.customsModel.paymentNumberVal(self.customsModel.rowSelected.PaymentNumber);
            self.customsModel.paymentStatusVal(self.customsModel.rowSelected.PaymentStatus);
            self.customsModel.SupplierNumberVal(self.customsModel.rowSelected.SupplierNumber);
            self.customsModel.paymentAmountVal(self.customsModel.rowSelected.PaymentAmount);
            self.customsModel.paymentCurrancyVal(self.customsModel.rowSelected.PaymentCurrency);
            self.customsModel.paymentDateVal(self.customsModel.rowSelected.PaymentDate);
            self.customsModel.paymentMethodVal(self.customsModel.rowSelected.PaymentMethod);
            self.customsModel.paymentTypeVal(self.customsModel.rowSelected.PaymentType);
            self.customsModel.createdByVal(self.customsModel.rowSelected.CreatedBy);
            self.customsModel.paymentIdVal(self.customsModel.rowSelected.PaymentId);
            self.customsModel.paymentDescriptionVal(self.customsModel.rowSelected.PaymentDescription);
            self.customsModel.transactionStatusVal(self.customsModel.rowSelected.TransactionBankStatus);
            document.querySelector("#viewDialog").open();

        };
        self.cancelViewDialBtn = function () {
            document.querySelector("#viewDialog").close();
            self.resetBtn();
        };
            //--------------------------------Attachments Function----------------------------
        self.viewAttachmentsBtnAction = function () {
            $(".apListBtn").addClass("loading");
            var jwtLogin = localStorage.getItem("jwtLogin");
            var jwt = localStorage.getItem("jwt"); 
            if (jwtLogin) {
                var successCbFn = function (data) {
                    if (data.length == 0) {
                        app.messagesDataProvider.push(app.createMessage('error', `there is no attachment for this payment`));
                        $(".apListBtn").removeClass("loading");
                        return;
                    } else {
                        console.log(data)
                        self.customsModel.attachmentsArr(data);
                        $(".apListBtn").removeClass("loading");
                        document.querySelector("#attachmentsDialog").open();
                    }
                    
                };
                var failCbFn = function () {
                };
                services.getGeneric("payment/getPaymentAttachmentBasic/" + self.customsModel.rowSelected.PaymentId, {"jwt": jwtLogin}).then(successCbFn, failCbFn);

            }
            if (jwt) {
                var successCbFn = function (data) {
                    if (data.length == 0) {
                        app.messagesDataProvider.push(app.createMessage('warning', `there is no attachment for this payment`));
                        $(".apListBtn").removeClass("loading");
                        return;
                    } else {
                        console.log(data)
                        self.customsModel.attachmentsArr(data);
                        $(".apListBtn").removeClass("loading");
                        document.querySelector("#attachmentsDialog").open();
                    }
                };
                var failCbFn = function () {
                };
                services.getGeneric("payment/getPaymentAttachment/" + self.customsModel.rowSelected.PaymentId, {"jwt": jwt}).then(successCbFn, failCbFn);
            }

        };
        
        self.cancelAttachments = function(){
             document.querySelector("#attachmentsDialog").close();
        };
            //--------------------------------Excel Function----------------------------
        self.ecxelBtnAction=function(){

             var SheetData=[];
                  $.each(filteredData, function (i) {
                            SheetData.push({
                               SupplierName:filteredData[i].Payee,
                               SupplierNumber:filteredData[i].SupplierNumber,
                               PaymentNumber:filteredData[i].PaymentNumber,
                               PaymentAmount:filteredData[i].PaymentAmount,
                               PaymentCurrency:filteredData[i].PaymentCurrency,
                               PaymentDate:filteredData[i].PaymentDate,
                               PaymentDescription:filteredData[i].PaymentDescription,
                               PaymentNumber:filteredData[i].PaymentNumber,
                               PaymentId:filteredData[i].PaymentId,
                               TransactionBankStatus:filteredData[i].TransactionBankStatus,
                               
                        
                            });
                        });
            var ws = XLSX.utils.json_to_sheet(SheetData);
//            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Employee");
//            /* generate an XLSX file */
            XLSX.writeFile(wb, "Payments.xlsx");
          
            app.messagesDataProvider.push(app.createMessage('confirmation', 'Data Exported '));
        }
          //     --------------------------------------- view approval function button ------------------------ 

        self.viewApproval = function () {
            $(".approBtn").addClass("loading");
            var getApprovalList = function (data) {
                console.log(data)
                $(".approBtn").removeClass("loading");
                self.dataTB2([]);
                // var data = data ;
                // data.pop();
                console.log(data)
                var lineManger = data.find(e => e.rolrType == "LINE_MANAGER").roleId;
                var projectManager = data.find(e => e.rolrType == "PROJECT_MANAGER").roleId
                var checkStatusPending = false;
                $.each(data, function (i, item) {
                    var notificationStatus, bodyCardStatus, cardStatus;

                    if (data[i].notificationType == "FYA") {

                        if (data[i].responseCode == "REJECTED") {
                            bodyCardStatus = 'app-crd-bdy-border-fail';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-rj';

                        } else if (data[i].responseCode == null) {

                            bodyCardStatus = 'app-crd-bdy-border-pen';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-pen';

                        } else if (data[i].responseCode == "APPROVED") {
                            bodyCardStatus = 'app-crd-bdy-border-suc';
                            notificationStatus = 'app-type-a';
                            cardStatus = 'app-card-prog-type-app';
                        }
                    } else if (data[i].notificationType == "FYI") {

                        bodyCardStatus = 'app-crd-bdy-border-ntre';
                        notificationStatus = 'app-type-i';
                        cardStatus = 'app-card-prog-type-ntr';
                    }
                    if (!item.responseCode && !checkStatusPending) {
                        checkStatusPending = true;
                    }
                    self.dataTB2.push({

                        name: item.rolrType == 'EMP' ? "Appspro.projects"
                                : item.rolrType == 'LINE_MANAGER' ? lineManger
                                : item.rolrType == 'PROJECT_MANAGER' ? projectManager : item.roleId,

                        type: item.notificationType,
                        status: !item.responseCode && checkStatusPending && item.notificationType == 'FYA' ? 'PENDING' : item.responseCode,
                        bodyStatus: bodyCardStatus,
                        appCardStatus: cardStatus,
                        aistatus: notificationStatus,
                        approvalDate: item.responseDate

                    });
                });
                self.dataSourceTB2(new oj.ArrayTableDataSource(self.dataTB2));
                document.querySelector("#approvalDialog").open();
            };
            
                var headers = {

                };
                services.getGeneric(commonhelper.getApprovalList + 'XX_PAYMENT_REQUEST/' + self.customsModel.selectedRowKey(), headers).then(getApprovalList, app.failCbFn);
            
        };
        self.closeDialog = function () {
            document.querySelector("#approvalDialog").close();
        };
        //--------------------------------Translation Function----------------------------
        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                paymentNumberLbl: getTransaltion("paymentDetails.paymentNumberLbl"),
                paymentStatusLbl: getTransaltion("paymentDetails.paymentStatusLbl"),
                SupplierNumberLbl: getTransaltion("paymentDetails.SupplierNumberLbl"),
                paymentAmountLbl: getTransaltion("paymentDetails.paymentAmountLbl"),
                paymentCurrancyLbl: getTransaltion("paymentDetails.paymentCurrancyLbl"),
                paymentDescriptionLbl: getTransaltion("paymentDetails.paymentDescriptionLbl"),
                paymentDateLbl: getTransaltion("paymentDetails.paymentDateLbl"),
                paymentMethodLbl: getTransaltion("paymentDetails.paymentMethodLbl"),
                paymentTypeLbl: getTransaltion("paymentDetails.paymentTypeLbl"),
                createdByLbl: getTransaltion("paymentDetails.createdByLbl"),
                paymentIdLbl: getTransaltion("paymentDetails.paymentIdLbl"),
                operationTypesLbl: getTransaltion("paymentDetails.operationTypesLbl"),
                searchLbl: getTransaltion("common.searchLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                screenLbl: getTransaltion("paymentDetails.screenLbl"),
                payLbl: getTransaltion("paymentDetails.payLbl"),
                viewLbl: getTransaltion("paymentDetails.viewLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                submitLbl: getTransaltion("common.submitLbl"),
                placeHolder: getTransaltion("common.placeHolder"),
                submitLbl: getTransaltion("paymentDetails.submitLbl"),
                trsBankStatusLbl: getTransaltion("paymentDetails.trsBankStatusLbl"),
                SupplierNameLbl: getTransaltion("paymentDetails.SupplierNameLbl"),
                transactionStatusLbl: getTransaltion("paymentDetails.transactionStatusLbl"),
                paymentDateLbl: getTransaltion("paymentDetails.paymentDateLbl"),
                paymentPurposeCodeLbl: getTransaltion("paymentDetails.paymentPurposeCodeLbl"),
                excelLbl: getTransaltion("paymentDetails.excelLbl"),
                toAccountNumberLbl: getTransaltion("paymentDetails.toAccountNumberLbl"),
                viewAttachmentsLbl: getTransaltion("paymentDetails.viewAttachmentsLbl"),
                approvalStatus: getTransaltion("paymentDetails.approvalStatus"),
                viewApprovalLabel: getTransaltion("common.viewApprovalLabel"),
                bankNameLbl: getTransaltion("paymentDetails.bankNameLbl"),

            });

            self.customsModel.columnArray([
                {
                    "headerText": self.label().paymentDateLbl,
                    "field": "PaymentDate"
                },             
                {
                    "headerText": self.label().paymentNumberLbl,
                    "field": "PaymentNumber"
                },

                {
                    "headerText": self.label().paymentStatusLbl,
                    "field": "PaymentStatus"
                },                
                {
                    "headerText": self.label().SupplierNumberLbl,
                    "field": "SupplierNumber"
                },
                {
                    "headerText": self.label().trsBankStatusLbl,
                    "field": "TransactionBankStatus"
                },
                {
                    "headerText": self.label().SupplierNameLbl,
                    "field": "Payee"
                },
                {
                    "headerText": self.label().approvalStatus,
                    "field": "ApprovalStatus"
                },
            ]);
        }
        initTransaltion();
    }


    return paymentDetailsScreenContentViewModel;
});
