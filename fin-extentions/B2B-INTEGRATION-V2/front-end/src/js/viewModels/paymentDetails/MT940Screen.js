/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * paymentInformation module
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset', 'ojs/ojvalidationgroup'
], function (oj, ko, $, app, services, commonhelper, DataCollectionEditUtils) {
    /**
     * The view model for the main content view template
     */
    function MT940ScreenContentViewModel() {
        var self = this;
        self.label = ko.observable();
        self.groupValid = ko.observable();
        self.customsModel = {

            transactionDate: ko.observable(),
            accountNumVal: ko.observable(),
            referenceNumVal: ko.observable(),
            responseData: ko.observable(),
            openingBalanceVal: ko.observable(),
            closingBalanceVal: ko.observable(),
            closingAvilableBalanceVal: ko.observable(),
            statmentNumberVal: ko.observable(),
            responseArr: ko.observableArray([]),
            columnArray: ko.observableArray(),
            accountNumberArr: ko.observableArray([{label: "SA3345000000003455995001", value: "SA3345000000003455995001"}]),                    
            accountNumVal: ko.observable(),
            yesterDayDate: ko.observable(),
            monthAgoDate: ko.observable(),
            accountNumValSelected: ko.observable(),
            result: ko.observable(false)
        };
         self.dataprovider = ko.observable();
          self.dataprovider(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.responseArr)));
        self.connected = function () {
            var date = new Date();
            date.setDate(date.getDate() - 1);
            self.customsModel.yesterDayDate(date.toISOString());
//        new Date().setDate(new Date().getDate() - 1)
            var oneMonthAgo = new Date(
                    new Date().getFullYear(),
                    new Date().getMonth() - 1,
                    new Date().getDate()
                    );
            self.customsModel.monthAgoDate(oneMonthAgo.toISOString())

        };
        function download(filename, text) {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
            element.setAttribute('download', filename);

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
        }
     //------------------------MT940 Actions---------------------------------
        self.getMT940BtnAction = function () {
               
            var tracker = document.getElementById("tracker");
            if (tracker.valid != "valid")
            {
                tracker.showMessages();
                tracker.focusOn("@firstInvalidShown");
                return;
            }

            app.loading(true);
            var payload = {
                paymentId: "MT940",
                username: app.userName(),
                date:new Date(self.customsModel.transactionDate()).toISOString(),
                accountNumber:self.customsModel.accountNumValSelected(),
                transactionType:"MT940"
            };
            console.log(payload);
           self.customsModel.responseData('')
           console.log(self.customsModel.responseData())
            var requestSucess = function (data) {
                console.log(data)
                console.log(data.bankResponse.message[0])
              
                // app.loading(false);

                if (data.bankResponse.message[0] == "success")
                {

                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('confirmation', `${data.bankResponse.message[0]}`));
                    data.bankResponse.MT940response[0].transactions.forEach(el => {
                        el.comment = el.comment.substr(4);
                    });
                    self.customsModel.responseArr(data.bankResponse.MT940response[0].transactions);
                    self.customsModel.accountNumVal(data.bankResponse.MT940response[0].accountNumber.substr(5));
                    self.customsModel.referenceNumVal(data.bankResponse.MT940response[0].reference.substr(5));
                    self.customsModel.openingBalanceVal(data.bankResponse.MT940response[0].openingBlance ? data.bankResponse.MT940response[0].openingBlance.substr(5) : "");
                    self.customsModel.closingBalanceVal(data.bankResponse.MT940response[0].closingBalance.substr(5));
                    self.customsModel.statmentNumberVal(data.bankResponse.MT940response[0].statmentNumber.substr(5));
                    self.customsModel.closingAvilableBalanceVal(data.bankResponse.MT940response[0].closingAvilableBalance.substr(5));
                    self.customsModel.result(true);
                   
                  self.customsModel.responseData(data.bankResponse.responseData); 
                } else {
                    app.loading(false);
                    app.messagesDataProvider.push(app.createMessage('error', `${data.bankResponse.message[0]}`));
                   self.customsModel.result(false);
                }

            };
            var requestFail = function (fail) {
                app.loading(false);
                  app.messagesDataProvider.push(app.createMessage('confirmation', `${fail.statusText}`));

            };
            services.postGeneric(commonhelper.createPayment + "mt940", payload, {}).then(requestSucess, requestFail);
        };
        
        self.downloadMT940BtnAction=function(){
                       
                    var MT940text =  self.customsModel.responseData().substring( self.customsModel.responseData().indexOf(':20:'),  self.customsModel.responseData().indexOf('</TransactionData>'))
                    download("MT940.txt", MT940text.toString());
        }
        
        //------------------------Translations Actions---------------------------------
        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                transactionDateLbl: getTransaltion("paymentDetails.transactionDateLbl"),
                infoscreenLbl: getTransaltion("paymentDetails.infoscreenLbl"),
                getMT940Lbl: getTransaltion("paymentDetails.getMT940Lbl"),
                referenceNumLbl: getTransaltion("paymentDetails.referenceNumLbl"),
                statmentNumberLbl: getTransaltion("paymentDetails.statmentNumberLbl"),
                openingBalanceLbl: getTransaltion("paymentDetails.openingBalanceLbl"),
                closingAvilableBalanceLbl: getTransaltion("paymentDetails.closingAvilableBalanceLbl"),
                closingBalanceLbl: getTransaltion("paymentDetails.closingBalanceLbl"),
                accountNumLbl: getTransaltion("paymentDetails.accountNumLbl"),
                TransactionLbl: getTransaltion("paymentDetails.TransactionLbl"),
                commentLbl: getTransaltion("paymentDetails.commentLbl"),
                downloadMT940Lbl: getTransaltion("paymentDetails.downloadMT940Lbl"),

            });
            self.customsModel.columnArray([
                {
                    "headerText": self.label().TransactionLbl,
                    "field": "transaction"
                },
                {
                    "headerText": self.label().commentLbl,
                    "field": "comment"
                }
            ]);
        }
        initTransaltion();

    }

    return MT940ScreenContentViewModel;
});
