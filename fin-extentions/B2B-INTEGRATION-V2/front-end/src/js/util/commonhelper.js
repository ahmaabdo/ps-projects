define([], function() {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function() {
            var host = "";
            return host;
        };

        self.getPaaSHost = function() {
            var host = "";
            return host;
        };

        self.getBiReportServletPath = function() {
            var host = "report/commonbireport";
            return host;
        };
        self.authorization = "YXBwc3Byby5wcm9qZWN0czoxMjM0NTY3ODk=";
        self.paymentsUrl = "paymentprocess/GetAllPayment";
        self.createPayment = "paymentprocess/transactiontype/";
        self.getApprovalList='approval/approvaltype/';
         self.getInternalRest = function () {
         
 //    var host = "http://127.0.0.1:7101/b2b-integration-v2-fusion/rest/";
  var host = "https://digital.tbc.sa/b2b-integration-v2-fusion/rest/";

            return host;
        };


    }

    return new commonHelper();
});