define(['jquery'], function ($) {

    function serviceConfig() {

        var self = this;
        var authCode = "YXBwc3Byby5maW46b3JhY2xlQDEyMw==";
        self.contentTypeApplicationJSON = 'application/json';
        self.contentTypeApplicationXWWw = 'application/x-www-form-urlencoded; charset=UTF-8';
        self.headers = {};

        self.callGetService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            if (headers) {
               headers.Authorization = authCode;
                headers.xxx_data_decode = self.JCookie();
            }
            $.ajax({
                type: "GET",
                async: asynchronized, 
                url: serviceUrl,
                headers: headers,
                contentType: contentType,
                // async : false, 
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callDeleteService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            $.ajax({
                type: "DELETE",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                contentType: contentType,
                // async : false, 
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPostService = function (serviceUrl, payload,contentType,headers ,asynchronized ) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }          
            var defer = $.Deferred();
            if(headers){
                  headers.Authorization = authCode;
             headers.xxx_data_decode = self.JCookie();
            }           
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                    console.log(thrownError)
                }
            });
            return $.when(defer);
        };


        self.callPutService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }
            var defer = $.Deferred();
            $.ajax({
                type: "PUT",
//                dataType: 'jsonp',  
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };


        self.callGetApexService = function (serviceUrl, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            headers.Authorization = authCode;
            $.ajax({
                type: "GET",
                url: serviceUrl,
                headers: headers,
                async: false,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPostApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = authCode;
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };

        self.callPutApexService = function (serviceUrl, payload, contentType, asynchronized, headers) {
            var payloadStr = '';
            headers.Authorization = authCode;
            if (typeof payload === 'string' || payload instanceof String) {
                payloadStr = payload;
            } else {
                payloadStr = JSON.stringify(payload);
            }

            var defer = $.Deferred();
            $.ajax({
                type: "PUT",
                async: asynchronized,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payloadStr,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.callDeleteApexService = function (serviceUrl, payload, contentType, headers, asynchronized) {
            var defer = $.Deferred();
            headers.Authorization = authCode;
            $.ajax({
                type: "DELETE",
                async: asynchronized,
                url: serviceUrl,
                headers: headers,
                data: payload,
                contentType: contentType,
                success: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });

            return $.when(defer);
        };

        self.callPostServiceUncodeed = function (serviceUrl, payload, contentType, asynchronized, headers) {

            var defer = $.Deferred();
            $.ajax({
                type: "POST",
                async: false,
                headers: headers,
                url: serviceUrl,
                contentType: contentType,
                data: payload,
                success: function (data) {
                    defer.resolve(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    defer.reject(xhr);
                }
            });
            return $.when(defer);
        };
        self.JCookie = function getCookie() {
            var name = 'JSESSIONSID' + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                c = c.substring(1);
              }
              if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
              }
            }
            return "";
          }
    }

    return new serviceConfig();
});