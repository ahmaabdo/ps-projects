function showEmplDetails(oj,ko, services, app, mainData, commonhelper) {


    var rootViewModel = app;
// var yy ="https://digital.tbc.sa/front-end/?username=appspro.projects&host=https://eoay-test.fa.em2.oraclecloud.com&jwt=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6Imxyd1JBQlc4amIyN1lJelBxdkhQS3JQWnhMUSIsImtpZCI6InRydXN0c2VydmljZSJ9.eyJleHAiOjE1OTU0NTAzODUsInN1YiI6ImFwcHNwcm8ucHJvamVjdHMiLCJpc3MiOiJ3d3cub3JhY2xlLmNvbSIsInBybiI6ImFwcHNwcm8ucHJvamVjdHMiLCJpYXQiOjE1OTU0MzU5ODV9.RkJm317D9rNaR4uGTPt__RdAwKnN7cKHFfmqyFDTprEzDlYxV-vJ7zZGeLoxY7KFtFB-C9fg1Y_DHujfC-2xWEadqtiuD-SISzhaqc59za4KqvPT0ywzQRq00SLR18Sh6fPEC-fxUGnTYpFwBoRTTeZC-p7-Jhw0STqQe6VMAKyYKjeeqtDtp8SfGNXYolUjk3h6zNZwJb4wEfleBcClrWIQBWtqqwSivO4cYX9dqVDFH-Qo7bdbLMGQh_nDeakWchDPXYcUbNRE090fxy16ILij1XehQP6x7MTNUz4vpSwP64kEvZkw7_4k1f-OzIM38vJrRIT5-ZqUt1sAAPHoew"
    //var xx = "https://digital.tbc.sa/front-end/?username=Appspro.Fin&host=https://eoay-test.fa.em2.oraclecloud.com&jwt=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6Imxyd1JBQlc4amIyN1lJelBxdkhQS3JQWnhMUSIsImtpZCI6InRydXN0c2VydmljZSJ9.eyJleHAiOjE1OTU0NDgxOTIsInN1YiI6IkFwcHNwcm8uRmluIiwiaXNzIjoid3d3Lm9yYWNsZS5jb20iLCJwcm4iOiJBcHBzcHJvLkZpbiIsImlhdCI6MTU5NTQzMzc5Mn0.ki3QI3WlzFV62FPaEvz14FlRdAjnWQv-sP9dgRYogDvV6irIxELeT3KZSerQK6Iri5z5N2bhVh5s2gKm7I3GKMVpT-qCo07ESVbJbU8EJhqdqmNYTriHC3SJGsEmEvo9If2iM0KkdfSa_CG-_LcPBXIfKBosdflm2ChsqLwUFLODlriFgXHcLbw9YKqMn4rizF7KW63WE4C0mlDGpMh5wTB7Wn-Gqtnsy0mWgzqqQcI4SyARXheTzQ6lJBLMZ3fITulnINvH5GP9AvK-lnjTFDn-vnY7BcOm3y2_SzL2ZQ9jSfX7MBJC6dCN1n9uICb6koumIN2XQjzYBbFDcxxy_A"

      var url = new URL(window.location.href);
    console.log(url)
    var username; 
    var jwtusername;
    var hosturl;
    var jwt;
    var jwtinurl;
    var deviceIp;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var deviceAndBrowserDetails = navigator.userAgent; //browser and device details 
    var LogindateTime;
    var isJWTNotValid = false;


    if (url.searchParams.get("jwt")) {
        if (rootViewModel.hostUrl() == null || rootViewModel.hostUrl().length == 0) {
            hosturl = url.searchParams.get("host");
            rootViewModel.hostUrl(hosturl);
        }

        if (rootViewModel.jwt() == null || rootViewModel.jwt().length == 0) {
            jwt = url.searchParams.get("jwt");
         getToken(services,app,jwt);
            rootViewModel.jwt(jwt);
            console.log(jwt)
        }   

    

    if (rootViewModel.userName() == null || rootViewModel.userName().length == 0) {
        try {
            jwtJSON = jQuery.parseJSON(atob(jwt.split('.')[1]));
            jwtusername = jwtJSON.sub;
            username=jwtusername.toLowerCase()
            console.log(username)
            isJWTExpred = Math.round(Date.now() / 1000) > jwtJSON.exp;

            var isHasJwt = localStorage.jwt && localStorage.jwt == jwt;
            if ( isJWTExpred) {//used before
                isJWTNotValid = true;
                
            } else {
                rootViewModel.userName(username);

            }

        } catch (e) { };
    }

    if (url.searchParams.get("lang") && !isJWTNotValid) {

        var sessionLang = url.searchParams.get("lang");
        if (sessionLang == 'ar') {
            rootViewModel.setLocale('ar');
            localStorage.setItem("selectedLanguage", "ar");
        } else if (sessionLang == 'en') {
            // rootViewModel.setLocale("en-US");
            localStorage.setItem("selectedLanguage", "en-US");
        }
    }

    if (!isJWTNotValid) {   //its mean is JWT is vaild
        $(".apLoginBtn").addClass("loading");
        // rootViewModel.disOnLogin(true);
        LogindateTime = date + ' ' + time;
        // rootViewModel.loginDateFromSass(LogindateTime);
        var payloadLoginHistory = {};
        $.getJSON("https://api.ipify.org/?format=json", function (e) {
            deviceIp = e.ip;
            payloadLoginHistory = {
                "personNumber": username,
                "loginDate": LogindateTime,
                "browserAndDeviceDetails": deviceAndBrowserDetails,
                "deviceIp": deviceIp
            };
            sessionStorage.setItem("SShrLOginHistroy", JSON.stringify(payloadLoginHistory));
            localStorage.jwt = jwt;//save jwt into local storage to be checked next time
         });
        rootViewModel.jwtinurl(true)

    }

    else { //if jwt token not vaild 
         }

    }
    else { //if url not have jwt
        
    }
function getToken(services, app, userName) {
    var jwt = {"jwt" : userName};
    var getTokenData = function (data) {
        var jwt = data.jwt;
        document.cookie = "JSESSIONSID=" + jwt;
        console.log(document.cookie);
    };
    services.postGeneric("login/generatetoken", jwt).then(getTokenData, app.failCbFn);
    }
}