/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.restHelper;

import com.appspro.bean.SadadAndMoiBean;
import com.appspro.db.AppsproConnection;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.Key;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Random;

import javax.crypto.KeyGenerator;

import javax.net.ssl.HostnameVerifier;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.xml.bind.DatatypeConverter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Document;
import java.io.StringReader;

import java.util.Base64;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;

import org.json.XML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Husam Al-Masri
 */
public class RestHelper {

    private final String InstanceUrl =
        "https://ejvm-test.fa.em2.oraclecloud.com:443";
    private final String orgnizationUrl =
        "//hcmCoreSetupApi/resources/latest/organizations";
    private final String biReportUrl =
        "/xmlpserver/services/PublicReportService";
    private final String employeeServiceUrl =
        "/hcmCoreApi/resources/latest/emps/";
    public String protocol = "https";
    private final String instanceName =
        "https://ejvm-test.fa.em2.oraclecloud.com";

    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public String getBiReportUrl() {
        return biReportUrl;
    }
    public static void trustAllHostsFusion() {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts
                    = new TrustManager[]{new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                            String authType) throws CertificateException {
                    }
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                            String authType) throws CertificateException {
                    }
                }};
            // Install the all-trusting trust manager
            try {
                // SSLContext sc = SSLContext.getInstance("SSL");
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    public static void trustAllHosts() throws KeyStoreException, IOException,
                                              NoSuchAlgorithmException,
                                              CertificateException,
                                              UnrecoverableKeyException,
                                              KeyManagementException {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };


        // SSLContext sc = SSLContext.getInstance("SSL");
//        KeyStore keystore = KeyStore.getInstance("PKCS12");
//        InputStream in =
//            new FileInputStream("/u01/data/domains/TBCLoadJ_domain/certs/v2/identity");
//        keystore.load(in, "tbcloadjcsjcs-wls".toCharArray());
//
//        KeyManagerFactory keyManagerFactory =
//            KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
//        keyManagerFactory.init(keystore, "tbcloadjcsjcs-wls".toCharArray());
//
//
//        KeyStore keyStore = KeyStore.getInstance("PKCS12");
//        keyStore.load(null, null);

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
           keyStore.load(new FileInputStream("/u01/data/domains/TBCLoadJ_domain/certs/v2/identity.p12"), "tbcloadjcsjcs-wls".toCharArray());
            
           Key pvtKey = keyStore.getKey("tbcloadjcsjcs-wls", "tbcloadjcsjcs-wls".toCharArray());
           System.out.println(pvtKey.toString());
            
        KeyManagerFactory keyManagerFactory =
            KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, "tbcloadjcsjcs-wls".toCharArray());
        //          KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        //          keyGen.init(128);
        //          Key key = keyGen.generateKey();
        //          keyStore.setKeyEntry("tbcloadjcsjcs-wls", key, "tbcloadjcsjcs-wls".toCharArray(), null);
        //
        //          keyStore.store(new FileOutputStream("output.p12"), "password".toCharArray());

        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(keyManagerFactory.getKeyManagers(), trustAllCerts,
                new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    public static String callPostRest(String serverUrl, String contentType,
                                      String body, String autString) throws KeyStoreException,
                                                               NoSuchAlgorithmException,
                                                               CertificateException,
                                                               UnrecoverableKeyException,
                                                               KeyManagementException {
        try {
            System.out.println(body);

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", contentType);
            conn.setRequestProperty("Accept", "application/xml");
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            if (autString != null) {
                conn.addRequestProperty("Authorization", "Basic " + autString);
            }

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.close();

            int responseCode = conn.getResponseCode();
            BufferedReader br =
                new BufferedReader(new InputStreamReader((conn.getInputStream())));

            StringBuilder output = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }

            conn.disconnect();
            System.out.println(responseCode);
            if (responseCode == 200) {
                return output.toString();
            } else if (responseCode == 500) {
                return "{\"ReturnStatus\":\"INERTNAL SERVER ERROR\"}";
            } else if (responseCode == 201) {
                JSONObject reponseJson = new JSONObject(output.toString());
                return "{\"ReturnStatus\":\"" +
                    reponseJson.optString("ReturnStatus") +
                    "\",\"ErrorCode\":\"" +
                    reponseJson.optString("ErrorCode") +
                    "\",\"ErrorExplanation\":\"" +
                    reponseJson.optString("ErrorCode") + "\"}";
            } else if (responseCode == 400) {
                return conn.getErrorStream().toString();
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public static SadadAndMoiBean callPostRest(String serverUrl,
                                               String contentType,
                                               String body) throws KeyStoreException,
                                                                   NoSuchAlgorithmException,
                                                                   CertificateException,
                                                                   UnrecoverableKeyException,
                                                                   KeyManagementException {
        JSONObject res = new JSONObject();
        SadadAndMoiBean sadadBean = new SadadAndMoiBean();
        try {
            System.out.println("calling ::" + serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestProperty("Accept", "text/plain");
            connection.setRequestProperty("charset", "utf-8");
            //            connection.setRequestProperty("Authorization",
            //                    "Basic " + autString);
            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println("code:" + connection.getResponseCode());
            InputStream is;
            if (connection.getResponseCode() >= 400) {
                is = connection.getErrorStream();
                System.out.println(connection.getResponseMessage());
                JSONObject respo = new JSONObject(connection.getErrorStream());
                System.out.println(respo.toString());
                sadadBean.setCodeStatus(Integer.toString(connection.getResponseCode()));
                res.put("status", "error");
            } else {
                res.put("status", "done");
                is = connection.getInputStream();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuffer response = new StringBuffer();
            StringBuilder response2 = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
                response2.append(inputLine);
                if (connection.getResponseCode() != 201 ||
                    connection.getResponseCode() != 200) {
                    //                      System.out.println(response);
                    sadadBean.setBodyStatus(response.toString());
                    sadadBean.setCodeStatus(Integer.toString(connection.getResponseCode()));
                } else {
                    System.out.println(response2.toString());
                }

                //  itemWLotLogBean.setBodyStatus(response.toString());
                res.put("data", sadadBean.getBodyStatus());
            }
            in.close();
            try {
                res.put("data", new JSONObject(response.toString()));
            } catch (Exception e) {
                res.put("data", response.toString());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("status", "error");
            res.put("data", sadadBean.getBodyStatus());
            res.put("data", "Internal server error");
        }
        return sadadBean;
    }

    public static JSONObject callHttpPost(String serverUrl, String contentType,
                                          String body) throws KeyStoreException,
                                                              NoSuchAlgorithmException,
                                                              CertificateException,
                                                              UnrecoverableKeyException,
                                                              KeyManagementException {
        JSONObject res = new JSONObject();
        System.out.println(serverUrl);
        System.out.println(body);
        try {
            System.out.println("calling ::" + serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);

            connection.setRequestProperty("Content-Type", contentType);

            //authentication to be added
            //            connection.addRequestProperty("Authorization", "Basic " + getAuth());

            connection.setRequestProperty("Accept", "text/plain");
            connection.setRequestProperty("charset", "utf-8");

            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println("code:" + connection.getResponseCode());
            InputStream inputStream;
            if (connection.getResponseCode() == 200 ||
                connection.getResponseCode() == 201 ||
                connection.getResponseCode() == 204) {
                inputStream = connection.getInputStream();
            } else {
                inputStream = connection.getErrorStream();
            }

            res.put("responseCode", connection.getResponseCode());

            if (inputStream != null) {
                InputStreamReader iReader = new InputStreamReader(inputStream);
                BufferedReader bReader = new BufferedReader(iReader);
                String line;
                String response = "";
                while ((line = bReader.readLine()) != null) {
                    response += line;
                }
                iReader.close();
                bReader.close();
                inputStream.close();
                // store doc returned
                res.put("responseData", response);
            } else {
                res.put("responseData", "Internal Server Error");
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("responseCode", "500");
            res.put("responseData", ioe.getMessage());
        }
        return res;
    }
    public static JSONObject callHttpFusionPost(String serverUrl, String contentType,
                                          String body) throws KeyStoreException,
                                                              NoSuchAlgorithmException,
                                                              CertificateException,
                                                              UnrecoverableKeyException,
                                                              KeyManagementException {
        JSONObject res = new JSONObject();
        System.out.println(serverUrl);
        System.out.println(body);
        try {
            System.out.println("calling ::" + serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHostsFusion();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);

            connection.setRequestProperty("Content-Type", contentType);

            //authentication to be added
            //            connection.addRequestProperty("Authorization", "Basic " + getAuth());

            connection.setRequestProperty("Accept", "text/plain");
            connection.setRequestProperty("charset", "utf-8");

            if (body != null && body.length() > 0) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                byte[] input = body.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println("code:" + connection.getResponseCode());
            InputStream inputStream;
            if (connection.getResponseCode() == 200 ||
                connection.getResponseCode() == 201 ||
                connection.getResponseCode() == 204) {
                inputStream = connection.getInputStream();
            } else {
                inputStream = connection.getErrorStream();
            }

            res.put("responseCode", connection.getResponseCode());

            if (inputStream != null) {
                InputStreamReader iReader = new InputStreamReader(inputStream);
                BufferedReader bReader = new BufferedReader(iReader);
                String line;
                String response = "";
                while ((line = bReader.readLine()) != null) {
                    response += line;
                }
                iReader.close();
                bReader.close();
                inputStream.close();
                // store doc returned
                res.put("responseData", response);
            } else {
                res.put("responseData", "Internal Server Error");
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
            res.put("responseCode", "500");
            res.put("responseData", ioe.getMessage());
        }
        return res;
    }


    public static JSONObject callHttpGet(String serverUrl) throws KeyStoreException,
                                                                  NoSuchAlgorithmException,
                                                                  CertificateException,
                                                                  UnrecoverableKeyException,
                                                                  KeyManagementException {
        JSONObject res = new JSONObject();

        try {

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHostsFusion();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }

            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            connection.addRequestProperty("Authorization",
                                          "Basic " + getAuth());
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);

            InputStream inputStream;
            if (connection.getResponseCode() == 200 ||
                connection.getResponseCode() == 201 ||
                connection.getResponseCode() == 204) {
                inputStream = connection.getInputStream();
            } else {
                inputStream = connection.getErrorStream();
            }

            res.put("responseCode", connection.getResponseCode());

            if (inputStream != null) {
                InputStreamReader iReader = new InputStreamReader(inputStream);
                BufferedReader bReader = new BufferedReader(iReader);
                String line;
                String response = "";
                while ((line = bReader.readLine()) != null) {
                    response += line;
                }
                iReader.close();
                bReader.close();
                inputStream.close();
                // store doc returned
                res.put("responseData", response);
            } else {
                res.put("responseData", "Internal Server Error");
            }
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
            res.put("responseCode", "500");
            res.put("responseData", e.getMessage());

        }
        return res;
    }


    public static String callGetRest(String serverUrl) throws KeyStoreException,
                                                              NoSuchAlgorithmException,
                                                              CertificateException,
                                                              UnrecoverableKeyException,
                                                              KeyManagementException {

        try {

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.addRequestProperty("Authorization", "Basic " + getAuth());
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public static String getAuth() {
        byte[] message = ("appspro.projects" + ":" + "123456789").getBytes();
        //        byte[] message = ("HCMUser" + ":" + "Oracle@123").getBytes();
        String encoded = DatatypeConverter.printBase64Binary(message);
        return encoded;
    }

        
    public static String generateRandomNymber() {
        SimpleDateFormat formatter =
            new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String dateTime =
            formatter.format(date).replaceAll("/", "").replaceAll(":",
                                                                  "").replaceAll(" ",
                                                                                 "");

        Random rand = new Random();
        String randStr = Integer.toString(rand.nextInt(100000));


        System.out.println("R_" + dateTime + randStr);


        return "R_" + dateTime + randStr;

    }
    
    public static JSONObject callHttpPostLogin(String serverUrl, String username , String password ) throws KeyStoreException,
                                                                       NoSuchAlgorithmException,
                                                                       CertificateException,
                                                                       UnrecoverableKeyException,
                                                                       KeyManagementException {
            String body =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n" + 
                "   <soapenv:Header/>\n" + 
                "   <soapenv:Body>\n" + 
                "      <pub:validateLogin>\n" + 
                "         <pub:userID>"+username+"</pub:userID>\n" + 
                "         <pub:password>"+password+"</pub:password>\n" + 
                "      </pub:validateLogin>\n" + 
                "   </soapenv:Body>\n" + 
                "</soapenv:Envelope>";
            //appspro.projects  12345678
            JSONObject res = new JSONObject();
            try {
                System.out.println("calling ::" + serverUrl);
                HttpsURLConnection https = null;
                HttpURLConnection connection = null;
                URL url
                        = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
                if (url.getProtocol().toLowerCase().equals("https")) {
                    trustAllHostsFusion();
                    https = (HttpsURLConnection) url.openConnection();
                    https.setHostnameVerifier(DO_NOT_VERIFY);
                    connection = https;
                } else {
                    connection = (HttpURLConnection) url.openConnection();
                }
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                connection.setRequestProperty("SOAPAction", "");
                connection.setRequestMethod("POST");
                if (body != null && body.length() > 0) {
                    connection.setDoOutput(true);
                    OutputStream os = connection.getOutputStream();
                    byte[] input = body.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }
                System.out.println("code:" + connection.getResponseCode());
                InputStream inputStream;
                if (connection.getResponseCode() == 200 ||  connection.getResponseCode() == 201 || connection.getResponseCode() == 204) {
                            inputStream = connection.getInputStream();
                        } else {
                            inputStream = connection.getErrorStream();
                        }
                res.put("responseCode", connection.getResponseCode());
                if (inputStream != null) {
                            InputStreamReader iReader = new InputStreamReader(inputStream);
                            BufferedReader bReader = new BufferedReader(iReader);
                            String line;
                            String response = "";
                            while ((line = bReader.readLine()) != null) {
                                response += line;
                            }
                            iReader.close();
                            bReader.close();
                            inputStream.close();
                            // store doc returned 
                            res.put("responseData", response);
                        } else {
                            res.put("responseData", "Internal Server Error");
                        }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                res.put("responseCode", "500");
                res.put("responseData", ioe.getMessage());
            }
            return res;
        }
    
    
    public static Document convertStringToDocument(String xmlStr) {
              DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
              DocumentBuilder builder;  
              try  
              {  
                  builder = factory.newDocumentBuilder();  
                  Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) ); 
                  return doc;
              } catch (Exception e) {  
                  e.printStackTrace();  
              } 
              return null;
          }
    
    public  JSONObject callHttpPatch(String serverUrl, String method, String body) {
               JSONObject res = new JSONObject();
               try {
                   System.out.println("calling ::" + serverUrl);
                   System.out.println("method ::" + method);
                   System.out.println("payload ::" + body);
                   HttpsURLConnection https = null;
                   HttpURLConnection connection = null;
                   URL url =
                       new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
                   if (url.getProtocol().toLowerCase().equals("https")) {
                       trustAllHostsFusion();
                       https = (HttpsURLConnection)url.openConnection();
                       https.setHostnameVerifier(DO_NOT_VERIFY);
                       connection = https;
                   } else {
                       connection = (HttpURLConnection)url.openConnection();
                   }
                   String SOAPAction = getInstanceUrl();
                   connection.setDoOutput(true);
                   connection.setDoInput(true);
                   if (method.equalsIgnoreCase("PATCH")) {
                       connection.setRequestProperty("X-HTTP-Method-Override",
                                                     "PATCH");
                       connection.setRequestMethod("POST");
                   } else {
                       connection.setRequestMethod(method);
                   }
                   connection.setRequestProperty("Content-Type", "application/json;");
                   connection.setRequestProperty("Accept", "application/json");
                   connection.setConnectTimeout(6000000);
                   connection.setRequestProperty("SOAPAction", SOAPAction);
                   connection.setRequestProperty("charset", "utf-8");
                   connection.setRequestProperty("Authorization",
                                                 "Basic " + getAuth());
                   if (body != null && body.length() > 0) {
                       connection.setDoOutput(true);
                       OutputStream os = connection.getOutputStream();
                       byte[] input = body.getBytes("utf-8");
                       os.write(input, 0, input.length);
                   }
                   int code = connection.getResponseCode();
                   InputStream is;
                   if (code >= 400) {
                       is = connection.getErrorStream();
                       res.put("status", "error");
                   } else {
                       res.put("status", "done");
                       is = connection.getInputStream();
                   }
                   System.out.println(code);
                   System.out.println(is);
                   StringBuffer response = new StringBuffer();
                   if(is != null){
                   BufferedReader in = new BufferedReader(new InputStreamReader(is));
                   String inputLine;
                   while ((inputLine = in.readLine()) != null) {
                       response.append(inputLine);
                   }
                   in.close();
                   }
                   try {
                       res.put("data", new JSONObject(response.toString()));
                   } catch (Exception e) {
                       res.put("data", response.toString());
                   }
                   System.out.print("code:" + code);
                   if (code >= 400)
                       System.out.println(",Error:" + res.get("data").toString());
               } catch (IOException ioe) {
                   ioe.printStackTrace();
                   res.put("status", "error");
                   res.put("data", "Internal server error");
               }
               return res;
           }

    
    
    public static String decodeBase64(String encoded) {

          byte[] decoded = Base64.getDecoder().decode(encoded);
          String decodedString = new String(decoded);
          return decodedString;
      }
    public String getInstanceUrl() {
        return InstanceUrl;
    }

    public String getOrgnizationUrl() {
        return orgnizationUrl;
    }

    public String getEmployeeServiceUrl() {
        return employeeServiceUrl;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public String callSaaS(String serverUrl, String restFrameWorkVersion) {
        String newurl = serverUrl.replaceAll(" ", "%20");
        HttpsURLConnection https = null;
        HttpURLConnection connection = null;
        try {
            URL url =
                new URL(null, newurl, new sun.net.www.protocol.https.Handler());
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
            String SOAPAction = getInstanceUrl();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setRequestProperty("Accept", "application/json");
            //  connection.setRequestProperty("REST-Framework-Version", "3");
            connection.setConnectTimeout(6000000);
            connection.setRequestProperty("SOAPAction", SOAPAction);
            //connection.setRequestProperty("Authorization","Bearer " + jwttoken);
            connection.setRequestProperty("Authorization",
                                          "Basic " + getAuth());
            connection.setRequestProperty("REST-Framework-Version", "1");
            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject obj = new JSONObject(response.toString());
            //JSONArray arr = obj.getJSONArray("items");
            //AppsproConnection.LOG.info(response.toString());
            return response.toString();

        } catch (Exception e) {
            e.printStackTrace(); //AppsproConnection.LOG.error("ERROR", e);
        }
        return "";
    }

    public static Document httpPost(String destUrl,
                                    String postData) throws Exception {

        //        AppsproConnection.LOG.info(postData);
        System.setProperty("DUseSunHttpHandler", "true");
        byte[] buffer = new byte["".length()];
        buffer = "".getBytes();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(buffer);
        byte[] b = bout.toByteArray();
        trustAllHosts();
        java.net.URL url =
            new URL(null, destUrl, new sun.net.www.protocol.https.Handler());
        java.net.HttpURLConnection http;
        if (url.getProtocol().toLowerCase().equals("https")) {

            HttpsURLConnection https =
                (HttpsURLConnection)url.openConnection();
            //            System.setProperty("DUseSunHttpHandler", "true");
            https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
        } else {
            http = (HttpURLConnection)url.openConnection();
        }
        String SOAPAction = "";
        //            http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Length", String.valueOf(b.length));
        http.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        http.setRequestProperty("SOAPAction", SOAPAction);
        //        http.setRequestProperty("Authorization", "Basic " + getAuth());
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        OutputStream out = http.getOutputStream();
        out.write(b);

        System.out.println("connection status: " + http.getResponseCode() +
                           "; connection response: " +
                           http.getResponseMessage());

        if (http.getResponseCode() == 200 || http.getResponseCode() == 201 ||
            http.getResponseCode() == 204) {
            InputStream in = http.getInputStream();
            InputStreamReader iReader = new InputStreamReader(in);
            BufferedReader bReader = new BufferedReader(iReader);

            String line;
            String response = "";
            //            AppsproConnection.LOG.info("==================Service response: ================ ");
            while ((line = bReader.readLine()) != null) {
                response += line;
            }

            if (response.indexOf("<?xml") > 0)
                response =
                        response.substring(response.indexOf("<?xml"), response.indexOf("</env:Envelope>") +
                                           15);

            DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
            fact.setNamespaceAware(true);
            DocumentBuilder builder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(response));

            Document doc = builder.parse(src);


            iReader.close();
            bReader.close();
            in.close();
            http.disconnect();


            return doc;
        }


        return null;
    }
    public static JSONObject callHttpGetRelatedInvoicesAtt(String serverUrl, String jwtToken) {
        JSONObject res = new JSONObject();
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            HttpsURLConnection https = null;
            HttpURLConnection connection = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHostsFusion();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection)url.openConnection();
            }
    //            String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsIng1dCI6Imxyd1JBQlc4amIyN1lJelBxdkhQS3JQWnhMUSIsImtpZCI6InRydXN0c2VydmljZSJ9.eyJleHAiOjE1OTYzODU5NzYsInN1YiI6IkFwcHNwcm8uRmluIiwiaXNzIjoid3d3Lm9yYWNsZS5jb20iLCJwcm4iOiJBcHBzcHJvLkZpbiIsImlhdCI6MTU5NjM3MTU3Nn0.QixsCTGReVcWMktOnAe_VHQ2avpPRMgf3MpUEhdEoAhIBPkTxAt1Hc0CWATtqMF9MASdekZtda2WPInyiO4EMSEB71Yx7hxAbL7fOba6YLMAec-e2rzaCfTRs7LuRLZMxD-CTGN5eYjmK2sST8zA2RQtY_w-Lux3AFG2gfRr9qOESjBw3bwJ_C0SwU42xeb3sxf1dKHyCl91l2PaingPSCFewJ53A5Idf7mKQxRE2b_9ASZgGmpY999EKjjF4CqLQso0_iNcl0OSMCiAdTTSz-Zu0MHSXpJUaBe8r8edym-JTKAak-y50v3jTv3R3Rq3bfAzAnsslGY0VogSs5F13g";
            System.out.println("YEEEEEEEEEEEES TOKEN HERE");
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            connection.addRequestProperty("Authorization",
                                          "Bearer " + jwtToken);
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);
            InputStream inputStream;
            if (connection.getResponseCode() == 200 ||
                connection.getResponseCode() == 201 ||
                connection.getResponseCode() == 204) {
                inputStream = connection.getInputStream();
            } else {
                inputStream = connection.getErrorStream();
            }
            res.put("responseCode", connection.getResponseCode());
            if (inputStream != null) {
                InputStreamReader iReader = new InputStreamReader(inputStream);
                BufferedReader bReader = new BufferedReader(iReader);
                String line;
                String response = "";
                while ((line = bReader.readLine()) != null) {
                    response += line;
                }
                iReader.close();
                bReader.close();
                inputStream.close();
                // store doc returned
                res.put("responseData", response);
            } else {
                res.put("responseData", "Internal Server Error");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            res.put("responseCode", "500");
            res.put("responseData", e.getMessage());
        }
        return res;
    }
    public static JSONObject callHttpGetAttBasic(String serverUrl , String authToken) {
            JSONObject res = new JSONObject();
            try {
                URL url =
                    new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
                HttpsURLConnection https = null;
                HttpURLConnection connection = null;
                if (url.getProtocol().toLowerCase().equals("https")) {
                    trustAllHostsFusion();
                    https = (HttpsURLConnection)url.openConnection();
                    https.setHostnameVerifier(DO_NOT_VERIFY);
                    connection = https;
                } else {
                    connection = (HttpURLConnection)url.openConnection();
                }
                connection.setDoOutput(true);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Accept", "application/json");
                connection.addRequestProperty("Authorization",
                                              "Basic " + authToken);
                //            conn.addRequestProperty("REST-Framework-Version","2");
                //            conn.setRequestProperty("id", id);
                InputStream inputStream;
                if (connection.getResponseCode() == 200 ||
                    connection.getResponseCode() == 201 ||
                    connection.getResponseCode() == 204) {
                    inputStream = connection.getInputStream();
                } else {
                    inputStream = connection.getErrorStream();
                }
                res.put("responseCode", connection.getResponseCode());
                if (inputStream != null) {
                    InputStreamReader iReader = new InputStreamReader(inputStream);
                    BufferedReader bReader = new BufferedReader(iReader);
                    String line;
                    String response = "";
                    while ((line = bReader.readLine()) != null) {
                        response += line;
                    }
                    iReader.close();
                    bReader.close();
                    inputStream.close();
                    // store doc returned
                    res.put("responseData", response);
                } else {
                    res.put("responseData", "Internal Server Error");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                res.put("responseCode", "500");
                res.put("responseData", e.getMessage());
            }
            return res;
        }
    public static JSONObject callHttpSupplierIBAN(String serverUrl , String supplierNumber) throws ParserConfigurationException,
                                                                              SAXException {
               String body =
                   "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pub=\"http://xmlns.oracle.com/oxp/service/PublicReportService\">\n" + 
                   "   <soapenv:Header/>\n" + 
                   "   <soapenv:Body>\n" + 
                   "      <pub:runReport>\n" + 
                   "         <pub:reportRequest>\n" + 
                   "            <pub:attributeLocale>en-US</pub:attributeLocale>\n" + 
                   "            <pub:attributeTemplate>xml</pub:attributeTemplate>\n" + 
                   "            <pub:attributeFormat>xml</pub:attributeFormat>\n" + 
                   "            <pub:flattenXML>false</pub:flattenXML>\n" + 
                   "            <pub:reportAbsolutePath>/Custom/SAAS Extension /Reports/Supplier_Iban_Report.xdo</pub:reportAbsolutePath>\n" + 
                   "            <pub:parameterNameValues>\n" + 
                   "               <pub:item>\n" + 
                   "                  <pub:name>bind_partyNumber</pub:name>\n" + 
                   "                  <pub:values>\n" + 
                   "                     <pub:item>"+supplierNumber+"</pub:item>\n" + 
                   "                  </pub:values>\n" + 
                   "               </pub:item>\n" + 
                   "            </pub:parameterNameValues>\n" + 
                   "         </pub:reportRequest>\n" + 
                   "         <pub:userID>appspro.projects</pub:userID>\n" + 
                   "         <pub:password>123456789</pub:password>\n" + 
                   "      </pub:runReport>\n" + 
                   "   </soapenv:Body>\n" + 
                   "</soapenv:Envelope>";
               //appspro.projects  12345678
               JSONObject res = new JSONObject();
               try {
                   System.out.println("calling ::" + serverUrl);
                   HttpsURLConnection https = null;
                   HttpURLConnection connection = null;
                   URL url
                           = new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
                   if (url.getProtocol().toLowerCase().equals("https")) {
                       trustAllHostsFusion();
                       https = (HttpsURLConnection) url.openConnection();
                       https.setHostnameVerifier(DO_NOT_VERIFY);
                       connection = https;
                   } else {
                       connection = (HttpURLConnection) url.openConnection();
                   }
                   connection.setDoOutput(true);
                   connection.setDoInput(true);
                   connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                   connection.setRequestProperty("SOAPAction", "");
                   connection.setRequestMethod("POST");
                   if (body != null && body.length() > 0) {
                       connection.setDoOutput(true);
                       OutputStream os = connection.getOutputStream();
                       byte[] input = body.getBytes("utf-8");
                       os.write(input, 0, input.length);
                   }
                   System.out.println("code:" + connection.getResponseCode());
                   InputStream inputStream;
                   if (connection.getResponseCode() == 200 ||  connection.getResponseCode() == 201 || connection.getResponseCode() == 204) {
                               inputStream = connection.getInputStream();
                           } else {
                               inputStream = connection.getErrorStream();
                           }
                   res.put("responseCode", connection.getResponseCode());
                   if (inputStream != null) {
                               InputStreamReader iReader = new InputStreamReader(inputStream);
                               BufferedReader bReader = new BufferedReader(iReader);
                               String line;
                               String response = "";
                               while ((line = bReader.readLine()) != null) {
                                   response += line;
                               }
                               iReader.close();
                               bReader.close();
                               inputStream.close();
                               // store doc returned 
                               DocumentBuilder builder =
                                   DocumentBuilderFactory.newInstance().newDocumentBuilder();
                               InputSource src = new InputSource();
                               src.setCharacterStream(new StringReader(response ));
                               Document doc = builder.parse(src);
                               String data =
                                   doc.getElementsByTagName("reportBytes").item(0).getTextContent();
                               byte[] decodedBytes = Base64.getDecoder().decode(data);
                               String decodedString = new String(decodedBytes);
                               ;
                               res.put("responseData", XML.toJSONObject(decodedString));
                           } else {
                               res.put("responseData", "Internal Server Error");
                           }
               } catch (IOException ioe) {
                   ioe.printStackTrace();
                   res.put("responseCode", "500");
                   res.put("responseData", ioe.getMessage());
               }
               return res;
           }
}
