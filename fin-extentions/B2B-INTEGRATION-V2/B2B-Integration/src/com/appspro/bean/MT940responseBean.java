package com.appspro.bean;

import java.util.ArrayList;

public class MT940responseBean {
    
    private String reference;
    private String accountNumber;
    private String  statmentNumber;
    private String openingBlance;
    private String closingBalance;
    private String closingAvilableBalance;
   // private String transactions;
   private  ArrayList <MT940TransactionResponsseBean> transactions= new ArrayList<MT940TransactionResponsseBean>();

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setStatmentNumber(String statmentNumber) {
        this.statmentNumber = statmentNumber;
    }

    public String getStatmentNumber() {
        return statmentNumber;
    }

    public void setOpeningBlance(String openingBlance) {
        this.openingBlance = openingBlance;
    }

    public String getOpeningBlance() {
        return openingBlance;
    }

    public void setClosingBalance(String closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getClosingBalance() {
        return closingBalance;
    }

    public void setClosingAvilableBalance(String closingAvilableBalance) {
        this.closingAvilableBalance = closingAvilableBalance;
    }

    public String getClosingAvilableBalance() {
        return closingAvilableBalance;
    }


    public void setTransactions(ArrayList<MT940TransactionResponsseBean> transactions) {
        this.transactions = transactions;
    }

    public ArrayList<MT940TransactionResponsseBean> getTransactions() {
        return transactions;
    }
}
