package com.appspro.bean;

import java.util.Objects;

public class PaymentApprovalBean {

    private String paymentId;
    private String approvalStatus;
    private int id;

    public PaymentApprovalBean(String paymentId) {

        this.paymentId = paymentId;
    }

    public PaymentApprovalBean(String paymentId, String approvalStatus , int id) {

        this.paymentId = paymentId;
        this.approvalStatus = approvalStatus;
        this.id = id;
    }
    @Override
       public int hashCode() {
           return Objects.hash(paymentId);
       }
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof PaymentApprovalBean)) {
            return false;
        }
        PaymentApprovalBean paymentApprovalBean = (PaymentApprovalBean) obj;
        return Objects.equals(paymentId, paymentApprovalBean.paymentId);
    }
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }


    public int getId() {
        return id;
    }
}
