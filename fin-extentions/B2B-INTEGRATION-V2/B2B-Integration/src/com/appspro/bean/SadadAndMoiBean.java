/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bean;

/**
 *
 * @author Husam Al-Masri
 */
public class SadadAndMoiBean {

    private String sender;
    private String accountNumber;
    private String MessageType;
    private String fileRef;
    private String messageDescription;
    private String timeStamp;
    private String referenceNumber;
    private String billerID;
    private String subscriberNumber;
    private Long consumerID;
    private String amount;
    private String payExactDue;
    private String signatureValue;
    private String status;
    private String codeStatus;
    private String bodyStatus;
    private Long checkId;
    private String payload;
    private String paymentType;
    private String messageType;
    private String bankReference;
    private String sadadReference;
    private String requestType;
    private String citizenId;
    private String duration;
    private String sponserID;
    private String jobCategory;
    private String visaNumber;
    private String noOfDependent;
    private String licenseType;
    private String vehicleCustomCardNumber;
    private String registrationType;
    private String bodyType;
    private String vehicleSequence;
    private String newOwnerId;
    private String eventActualDate;
    private String violationID;
    private String passportType;
    private String visaCount;
    private String visaType;
    private String issuanceReason;
    private String cardVersionNumber;
    private String verdictNumber;
    private String currentAmount;
    private String previousAmount;
    private String paidAmount;
    private String fees;
    private String feeDetails;
    private String feeAmount;
    private String feeType;
    private String statusCode;
    private String errorCode;
    private String errorDesc;

    public SadadAndMoiBean() {
    }

    public SadadAndMoiBean(String referenceNumber, String billerID, String subscriberNumber, Long consumerID, String amount, String payExactDue, String paymentType) {
        this.referenceNumber = referenceNumber;
        this.billerID = billerID;
        this.subscriberNumber = subscriberNumber;
        this.consumerID = consumerID;
        this.amount = amount;
        this.payExactDue = payExactDue;
        this.paymentType = paymentType;
    }

    public SadadAndMoiBean(String referenceNumber, String billerID, String subscriberNumber, String paymentType) {
        this.referenceNumber = referenceNumber;
        this.billerID = billerID;
        this.subscriberNumber = subscriberNumber;
        this.paymentType = paymentType;
    }

    public SadadAndMoiBean(String referenceNumber, String billerID, String requestType, String visaNumber, String paymentType) {
        this.referenceNumber = referenceNumber;
        this.billerID = billerID;
        this.requestType = requestType;
        this.visaNumber = visaNumber;
        this.paymentType = paymentType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMessageType() {
        return MessageType;
    }

    public void setMessageType(String MessageType) {
        this.MessageType = MessageType;
    }

    public String getFileRef() {
        return fileRef;
    }

    public void setFileRef(String fileRef) {
        this.fileRef = fileRef;
    }

    public String getMessageDescription() {
        return messageDescription;
    }

    public void setMessageDescription(String messageDescription) {
        this.messageDescription = messageDescription;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getBillerID() {
        return billerID;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public String getSubscriberNumber() {
        return subscriberNumber;
    }

    public void setSubscriberNumber(String subscriberNumber) {
        this.subscriberNumber = subscriberNumber;
    }

    public Long getConsumerID() {
        return consumerID;
    }

    public void setConsumerID(Long consumerID) {
        this.consumerID = consumerID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayExactDue() {
        return payExactDue;
    }

    public void setPayExactDue(String payExactDue) {
        this.payExactDue = payExactDue;
    }

    public String getSignatureValue() {
        return signatureValue;
    }

    public void setSignatureValue(String signatureValue) {
        this.signatureValue = signatureValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(String codeStatus) {
        this.codeStatus = codeStatus;
    }

    public String getBodyStatus() {
        return bodyStatus;
    }

    public void setBodyStatus(String bodyStatus) {
        this.bodyStatus = bodyStatus;
    }

    public Long getCheckId() {
        return checkId;
    }

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBankReference() {
        return bankReference;
    }

    public void setBankReference(String bankReference) {
        this.bankReference = bankReference;
    }

    public String getSadadReference() {
        return sadadReference;
    }

    public void setSadadReference(String sadadReference) {
        this.sadadReference = sadadReference;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSponserID() {
        return sponserID;
    }

    public void setSponserID(String sponserID) {
        this.sponserID = sponserID;
    }

    public String getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(String jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getVisaNumber() {
        return visaNumber;
    }

    public void setVisaNumber(String visaNumber) {
        this.visaNumber = visaNumber;
    }

    public String getNoOfDependent() {
        return noOfDependent;
    }

    public void setNoOfDependent(String noOfDependent) {
        this.noOfDependent = noOfDependent;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getVehicleCustomCardNumber() {
        return vehicleCustomCardNumber;
    }

    public void setVehicleCustomCardNumber(String vehicleCustomCardNumber) {
        this.vehicleCustomCardNumber = vehicleCustomCardNumber;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getVehicleSequence() {
        return vehicleSequence;
    }

    public void setVehicleSequence(String vehicleSequence) {
        this.vehicleSequence = vehicleSequence;
    }

    public String getNewOwnerId() {
        return newOwnerId;
    }

    public void setNewOwnerId(String newOwnerId) {
        this.newOwnerId = newOwnerId;
    }

    public String getEventActualDate() {
        return eventActualDate;
    }

    public void setEventActualDate(String eventActualDate) {
        this.eventActualDate = eventActualDate;
    }

    public String getViolationID() {
        return violationID;
    }

    public void setViolationID(String violationID) {
        this.violationID = violationID;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public String getVisaCount() {
        return visaCount;
    }

    public void setVisaCount(String visaCount) {
        this.visaCount = visaCount;
    }

    public String getVisaType() {
        return visaType;
    }

    public void setVisaType(String visaType) {
        this.visaType = visaType;
    }

    public String getIssuanceReason() {
        return issuanceReason;
    }

    public void setIssuanceReason(String issuanceReason) {
        this.issuanceReason = issuanceReason;
    }

    public String getCardVersionNumber() {
        return cardVersionNumber;
    }

    public void setCardVersionNumber(String cardVersionNumber) {
        this.cardVersionNumber = cardVersionNumber;
    }

    public String getVerdictNumber() {
        return verdictNumber;
    }

    public void setVerdictNumber(String verdictNumber) {
        this.verdictNumber = verdictNumber;
    }

    public String getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(String currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getFeeDetails() {
        return feeDetails;
    }

    public void setFeeDetails(String feeDetails) {
        this.feeDetails = feeDetails;
    }

    public String getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    @Override
    public String toString() {
        if (paymentType.equalsIgnoreCase("payment")) {
            return "<Transaction>\n"
                    + "<Reference_number>" + referenceNumber + "</Reference_number>\n"
                    + "<BillerID>027</BillerID>\n"
                    + "<SubscriberNumber>4001451054</SubscriberNumber>\n"
                    + "<ConsumerID>" + consumerID + "</ConsumerID>\n"
                    + "<Amount>10</Amount>\n"
                    + "<PayExactDue>Y</PayExactDue>\n"
                    + "</Transaction>" + "\n";
        } else if (paymentType.equalsIgnoreCase("sadad inquiry")) {
            return "<Transaction>\n"
                    + "<Reference_number>" + referenceNumber + "</Reference_number>\n"
                    + "<BillerID>027</BillerID>\n"
                    + "<SubscriberNumber>4001451054</SubscriberNumber>\n"
                    + "</Transaction>" + "\n";
        } else if (paymentType.equalsIgnoreCase("moi")) {
            return "<Transaction>\n"
                    + "<Reference_number>" + referenceNumber + "</Reference_number>\n"
                    + "<BillerID>027</BillerID>\n"
                    + "<RequestType>006</RequestType>\n"
                    + "<ConsumerID>" + consumerID + "</ConsumerID>\n"
                    + "<VisaNumber>123456789</VisaNumber>\n"
                    + "</Transaction>" + "\n";
        } else if (paymentType.equalsIgnoreCase("moi inquiry")) {
            return "<Transaction>\n"
                    + "<Reference_number>" + referenceNumber + "</Reference_number>\n"
                    + "<BillerID>027</BillerID>\n"
                    + "<RequestType>006</RequestType>\n"
                    + "<ConsumerID>" + consumerID + "</ConsumerID>\n"
                    + "<Duration>12</Duration>\n"
                    + "</Transaction>" + "\n";
        } else if (paymentType.equalsIgnoreCase("moi refund")) {
            return "<Transaction>\n"
                    + "<Reference_number>" + referenceNumber + "</Reference_number>\n"
                    + "<BillerID>027</BillerID>\n"
                    + "<RequestType>006</RequestType>\n"
                    + "<ConsumerID>" + consumerID + "</ConsumerID>\n"
                    + "<Duration>12</Duration>\n"
                    + "<SponserID>1000000610</SponserID>\n"
                    + "<JobCategory>20</JobCategory>\n"
                    + "</Transaction>" + "\n";
        }
        return "";
    }

}
