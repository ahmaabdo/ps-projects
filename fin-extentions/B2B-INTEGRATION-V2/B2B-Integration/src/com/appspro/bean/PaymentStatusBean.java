package com.appspro.bean;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
public class PaymentStatusBean {
    private String paymentId;
    private String paymentSequance;
    private String paymentStatus;
    public PaymentStatusBean(String paymentId){
            this.paymentId = paymentId;
        }
   public PaymentStatusBean(String paymentId , String paymentSequance , String paymentStatus ){
       this.paymentId = paymentId;
       this.paymentSequance = paymentSequance;
       this.paymentStatus = paymentStatus;
       }
    @Override
       public int hashCode() {
           return Objects.hash(paymentId);
       }
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof PaymentStatusBean)) {
            return false;
        }
        PaymentStatusBean paymentStatusBean = (PaymentStatusBean) obj;
        return Objects.equals(paymentId, paymentStatusBean.paymentId);
    }
    public String getPaymentId() {
        return paymentId;
    }
    public String getPaymentSequance() {
        return paymentSequance;
    }
    public String getPaymentStatus() {
        return paymentStatus;
    }
}