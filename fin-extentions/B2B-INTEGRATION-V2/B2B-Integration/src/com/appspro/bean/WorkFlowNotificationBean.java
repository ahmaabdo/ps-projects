package com.appspro.bean;
public class WorkFlowNotificationBean {
    
    private String id;
    private String msgTitle;
    private String msgBody;
    private String creationDate;
    private String receiverType;
    private String receiverId;
    private String responsePersonId;
    private String type;
    private String responseDate;
    private String requestId;
    private String status;
    private String selfType;
    private String personNumber;
    private String personName;
    private String notificationType;
    private String responseCode;
    private String transactionId;
    
    private String Seen;

    public void setId(String ID) {
        this.id = ID;
    }

    public String getId() {
        return id;
    }

    public void setMsgTitle(String msgTitle) {
        this.msgTitle = msgTitle;
    }

    public String getMsgTitle() {
        return msgTitle;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setResponsePersonId(String responsePersonId) {
        this.responsePersonId = responsePersonId;
    }

    public String getResponsePersonId() {
        return responsePersonId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSelfType(String selfType) {
        this.selfType = selfType;
    }

    public String getSelfType() {
        return selfType;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setSeen(String Seen) {
        this.Seen = Seen;
    }

    public String getSeen() {
        return Seen;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }
}
