/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

/**
 *
 * @author HP
 */
public class MTFusionPaymentBean {

    private String paymentNumber;
    private long transactionReferenceNumber;
    private String paymentDate;
    private String disbursementBankAccountNumber;
    private String disbursementBankAccountName;
    private String paymentAmount;
    private String paymentType;
    private String paymentCurrency;
    private String supplierNumber;
    private String invoiceNumber;
    private int transactionType;
    private String remitToAccountNumber;
    private String sequenceNumber;
    private String paymentCode;
    private List<String> invoicesList;
    private String bankCode;
    private String bankName;

    public MTFusionPaymentBean(String paymentNumber, String paymentDate, String disbursementBankAccountNumber,
            String disbursementBankAccountName, String paymentAmount, String paymentType, String paymentCurrency,
            String supplierNumber, String invoiceNumber, int transactionType,String remitToAccountNumber,String sequenceNumber,String paymentCode , List<String> invoicesList
            ,String bankCode , String bankName ) {
        this.paymentNumber = paymentNumber;
        this.paymentDate = paymentDate;
        this.disbursementBankAccountNumber = disbursementBankAccountNumber;
        this.disbursementBankAccountName = disbursementBankAccountName;
        this.paymentAmount = paymentAmount;
        this.paymentType = paymentType;
        this.paymentCurrency = paymentCurrency;
        this.transactionReferenceNumber = new Date().getTime();
        this.supplierNumber = supplierNumber;
        this.invoiceNumber = invoiceNumber;
        this.transactionType = transactionType;
        this.remitToAccountNumber = remitToAccountNumber;
        this.sequenceNumber=sequenceNumber;
        this.paymentCode=paymentCode;
        this.invoicesList = invoicesList;
        this.bankCode= bankCode;
        this.bankName= bankName; 
    }

    public MTFusionPaymentBean(String disbursementBankAccountNumber, int transactionType) {
        this.disbursementBankAccountNumber = disbursementBankAccountNumber;
        this.transactionType = transactionType;
    }
    
    

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public String getDisbursementBankAccountNumber() {
        return disbursementBankAccountNumber;
    }

    public String getDisbursementBankAccountName() {
        return disbursementBankAccountName;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    @Override
    public String toString() {

  

        if (transactionType == 101) {
         String SwiftAMount = String.format("%.2f", Double.parseDouble(paymentAmount)).replace(".", ",");
//        return "<TransactionData>:20:" + sequenceNumber + "\n"+
//        ":21R:" + sequenceNumber + "\n"+
//        ":30:"+bankDateFormate(this.paymentDate)+"\n" + 
//        ":50H:SA3345000000003455995001\n" + // from account 
//        "TBC\n" + 
//        ":21:" + sequenceNumber + "\n"+
//       // ":32B:SAR12500,00\n" + 
//         ":32B:"+paymentCurrency+SwiftAMount+"\n" + 
//        ":52A:SABBSARI\n" +  //frombank
//        ":57A:SABBSARI\n" + //to which bank 
//        ":59:/"+this.remitToAccountNumber+"\n" +
//        "PAndiyan Mahalingam\n" +  // supplierName     
//        ":70:"+ this.paymentCode+"\n" + 
//          // supplierName      
//        ":71A:SHA</TransactionData>\n"   ;
        
        StringBuilder requestBuilder = new StringBuilder();
        
            requestBuilder.append("<TransactionData>:20:" + sequenceNumber + "\n");
            requestBuilder.append( ":21R:" + sequenceNumber + "\n");
            requestBuilder.append(":30:"+bankDateFormate(this.paymentDate)+"\n");
            requestBuilder.append(":50H:SA3345000000003455995001\n");
            requestBuilder.append("TBC\n");
            requestBuilder.append(":21:" + sequenceNumber + "\n");
            requestBuilder.append(":32B:"+paymentCurrency+SwiftAMount+"\n");
            requestBuilder.append(":52A:SABBSARI\n");
            requestBuilder.append(":57A:SABBSARI\n");
            // requestBuilder.append(":57A:"+  this.bankCode+"\n");
            //requestBuilder.append(":59:/"+this.remitToAccountNumber+"\n");
            requestBuilder.append(":59:/SA0645000000003455995002\n");
            requestBuilder.append("PAndiyan Mahalingam\n");
            requestBuilder.append(":70:"+ this.paymentCode+"\n");
            
            if ( this.invoicesList.size() > 2) {
                        requestBuilder.append( this.invoicesList.get(0).replaceAll("[^a-zA-Z0-9]", "") + "\n");
                        requestBuilder.append( this.invoicesList.get(1).replaceAll("[^a-zA-Z0-9]", "") + "\n");
                        requestBuilder.append( this.invoicesList.get(2).replaceAll("[^a-zA-Z0-9]", "") + "\n");
                    }else if ( this.invoicesList.size() == 2) {
                        requestBuilder.append( this.invoicesList.get(0).replaceAll("[^a-zA-Z0-9]", "") + "\n");
                        requestBuilder.append( this.invoicesList.get(1).replaceAll("[^a-zA-Z0-9]", "") + "\n");
                    }else if ( this.invoicesList.size() == 1){
                            requestBuilder.append( this.invoicesList.get(0).replaceAll("[^a-zA-Z0-9]", "") + "\n");
                        }
                    requestBuilder.append(":71A:SHA</TransactionData>\n");
        
        
        
        return  requestBuilder.toString();

        } else {
            return "<Account>"+disbursementBankAccountNumber+"</Account>\n";
                    
        }
    }
    public static String bankDateFormate(String fusionDateFormat) {
            try {
                Date date1;
                date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fusionDateFormat);
                SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
                String strDate = formatter.format(date1);
                return strDate.toString();
            } catch (ParseException e) {
                return null;
            }
        }

    public void setRemitToAccountNumber(String remitToAccountNumber) {
        this.remitToAccountNumber = remitToAccountNumber;
    }

    public String getRemitToAccountNumber() {
        return remitToAccountNumber;
    }
}
