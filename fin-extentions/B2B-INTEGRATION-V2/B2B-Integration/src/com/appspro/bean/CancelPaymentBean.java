/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bean;

/**
 *
 * @author hp
 */
public class CancelPaymentBean {
    
    private String paymentReferenceNumber;
    private String transactionReference;
    private String actionRequested;
    private String actionReason;
    private String paymentvalueDate;

    public CancelPaymentBean(String paymentReferenceNumber, String transactionReference ,String paymentValueDate) {
        this.paymentReferenceNumber = paymentReferenceNumber;
        this.transactionReference = transactionReference;
        this.actionRequested ="Stop Payment";
        this.actionReason ="Cancel Transaction Due to over paid";
        this.paymentvalueDate =paymentValueDate;
    }
    
    public String getPaymentReferenceNumber() {
        return paymentReferenceNumber;
    }

    public void setPaymentReferenceNumber(String paymentReferenceNumber) {
        this.paymentReferenceNumber = paymentReferenceNumber;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getActionRequested() {
        return actionRequested;
    }

    public void setActionRequested(String actionRequested) {
        this.actionRequested = actionRequested;
    }

    public String getActionReason() {
        return actionReason;
    }

    public void setActionReason(String actionReason) {
        this.actionReason = actionReason;
    }
    public void setPaymentvalueDate(String paymentvalueDate) {
        this.paymentvalueDate = paymentvalueDate;
    }

    public String getPaymentvalueDate() {
        return paymentvalueDate;
    }
    
    @Override
    public String toString(){
        return "<PaymentReferenceNumber>"+paymentReferenceNumber+"</PaymentReferenceNumber>\n"+
                "<TransactionReference>" +transactionReference+ "</TransactionReference>\n"+
                 "<PaymentValueDate>"+paymentvalueDate+"</PaymentValueDate>\n"+
                "<ActionRequested>" +actionRequested+"</ActionRequested>\n"+
                "<ActionReason>"+actionReason+"</ActionReason>\n";
    }


  
}
