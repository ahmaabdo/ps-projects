/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bean;

/**
 *
 * @author hp
 */
public class AccountBalanceBean {
    
    private String account;

    public AccountBalanceBean(String account) {
        this.account = account;
    }
    
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    
    @Override
    public String toString(){
        return "<Account>"+account+"</Account>\n";
    }
    
}
