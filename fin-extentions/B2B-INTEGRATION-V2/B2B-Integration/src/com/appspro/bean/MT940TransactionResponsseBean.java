package com.appspro.bean;

public class MT940TransactionResponsseBean {
    private String transaction;
    private String comment;

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }
}
