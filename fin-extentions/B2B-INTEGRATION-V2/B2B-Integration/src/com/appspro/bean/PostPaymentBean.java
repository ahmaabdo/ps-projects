/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bean;

/**
 *
 * @author hp
 */
public class PostPaymentBean {
        
    private String signatureValue;
    private String fileReference;
    private String transactionReference;

    public PostPaymentBean(String fileReference, String transactionReference) {
        this.fileReference = fileReference;
        this.transactionReference = transactionReference;
    }

    public String getSignatureValue() {
        return signatureValue;
    }

    public void setSignatureValue(String signatureValue) {
        this.signatureValue = signatureValue;
    }

    public String getFileReference() {
        return fileReference;
    }

    public void setFileReference(String fileReference) {
        this.fileReference = fileReference;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }
    
    @Override
    public String toString(){
        return "<FileReference>"+fileReference+"</FileReference>\n"+
                "<TransactionReference>" +transactionReference+ "</TransactionReference>\n";
    }

}
