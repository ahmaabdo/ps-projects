/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.PostPaymentBean;
import com.appspro.dao.LoggerDAO;
import com.appspro.restHelper.RestHelper;

import java.io.IOException;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author hp
 */
public class PostPaymentTransactionProcess extends MTFusionPaymentProcess{
    
    private String bankCode;
    private String paymentHeader;
    private StringBuilder paymentBody;
    private String paymentID;
    private String sequenceNum;
    private String referenceNum;

    public PostPaymentTransactionProcess(String paymentID  , String sequenceNum , String referenceNum) {
//        super(fusionPaymentURL);
        this.bankCode = "SABBSARI";
        this.paymentBody = new StringBuilder();
        this.paymentID = paymentID;
        this.sequenceNum = sequenceNum;
        this.referenceNum = referenceNum;
    }
    
    @Override
    public void buildTransactionHeader() {
        this.paymentHeader = "<PaymentMessage>\n"
                + "<CompanyCode>"+"TBD"+"</CompanyCode>\n"
                + "<PaymentStatusRequest>\n";
        this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildTransactionBody() {


                PostPaymentBean postPaymentBean = new PostPaymentBean(sequenceNum,referenceNum);

                this.paymentBody.append(postPaymentBean.toString());
   
    }

    @Override
    public void buildPaymentFooter() {
                this.paymentBody.append("</PaymentStatusRequest>\n"
                + "</PaymentMessage>\n");
    }

    @Override
    public StringBuilder buildFullRequest() {
        
         buildTransactionHeader();
         buildTransactionBody();
         buildPaymentFooter();
         
        String signature= buildTransactionSignatureBodyReturn(this.paymentBody.toString());
        
         
         StringBuilder builder = new StringBuilder(this.paymentBody.toString().replaceAll("</PaymentMessage>", signature.concat("</PaymentMessage>\n")));
         
         System.out.println(builder.toString());
         
         return builder;
    }
    
    public JSONObject getPostPaymentTransactionProcess(String bankServiceURL,
                                               String transactionId) throws IOException,
                                                                                    KeyStoreException,
                                                                                    NoSuchAlgorithmException,
                                                                                    CertificateException,
                                                                                    UnrecoverableKeyException,
                                                                                    KeyManagementException {

        JSONObject jsonResponse = new JSONObject();
        String encodedPayload = null;
        StringBuilder paymentBody;

        //add logger

        LoggerDAO log = new LoggerDAO();
  

            try {

                //add logger start building the request

                System.out.println("attemp creating the payload");

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "attemping to Create Transaction Payload");

                paymentBody = buildFullRequest();
                String currentDateTime = GetDataAndTime();

                System.out.println("success creating the payload");

                //add logger end building the request

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload created", "success");

                jsonResponse.put("message", "success creating the payload");
                jsonResponse.put("statue code", "200");


            } catch (Exception e) {
                System.out.print("faild creating the payload");
                System.out.print(e.getMessage());
                jsonResponse.put("message", e.getMessage());
                jsonResponse.put("statue code", "500");
                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload not created",
                                   "failed");

                return jsonResponse;
            }

        if ("200".equals(jsonResponse.get("statue code").toString()) 
    //            && encodedPayload != null
           ) {


            log.createLogEvent(transactionId, "Create Bank Transaction",
                               "attemping to Create Bank Transaction");

            System.out.println("hereeeeeeeeeeeeeeee every thing is done");

            JSONObject bankRes =
                RestHelper.callHttpPost(bankServiceURL, "text/xml",
                                        paymentBody.toString());


            if ("200".equals(bankRes.get("responseCode").toString()) ||
                "201".equals(bankRes.get("responseCode").toString()) ||
                "204".equals(bankRes.get("responseCode").toString())) {

                //here update the event_logger with reference number and sequance
                try {

                    JSONObject responseData =
                        XML.toJSONObject(bankRes.get("responseData").toString());
                    System.out.println(responseData);
                    if (responseData.getJSONObject("PaymentMessageResponse").getJSONObject("PaymentStatusResponse").get("Status").toString().equalsIgnoreCase("PROCESSED")) {
                        bankRes.append("message","success");
                        bankRes.append("postPaymentRes", responseData);
                        
                        log.createLogEvent(transactionId, "Create Bank Transaction",
                                           "Bank Transaction Created",
                                            bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "success", paymentBody.toString());
                    } else {
                        String postPaymentMessage =responseData.getJSONObject("PaymentMessageResponse").getJSONObject("PaymentStatusResponse").get("Description").toString();
                        bankRes.append("message",postPaymentMessage);
                        
                        log.createLogEvent(transactionId, "Create Bank Transaction",
                                           "Bank Transaction Created",
                                            bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "fail", paymentBody.toString());

                    }
                } catch (Exception e) {

                    bankRes.append("message", e.getMessage());

                }

                
            } else {


                log.createLogEvent(transactionId, "Create Bank Transaction",
                                   "Bank Transaction Failed",
                                   paymentBody.toString(),
                                   bankRes.get("responseCode").toString(),
                                   "failed", paymentBody.toString());
                
                bankRes.append("message",
                               bankRes.get("responseData").toString());

            }


            return jsonResponse.put("bankResponse", bankRes);

        } else {

            jsonResponse.put("message", "INTERNAL SERVER ERROR");
            jsonResponse.put("statue code", "500");

            return jsonResponse;
        }


    }
    
    
    
    
    
     public static void main(String[] args) {
//        PostPaymentTransactionProcess mt = new PostPaymentTransactionProcess("https://eoay-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff,relatedInvoices&onlyData=true&q=PaymentId=" +
//                                          "19001");
//        
//        mt.createTransactionTest();
         
    }
}
