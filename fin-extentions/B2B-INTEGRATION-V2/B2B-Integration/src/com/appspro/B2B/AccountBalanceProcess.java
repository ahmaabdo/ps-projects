/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.AccountBalanceBean;
import com.appspro.bean.CancelPaymentBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hp
 */
public class AccountBalanceProcess extends MTFusionPaymentProcess {

    private StringBuilder paymentBody;
    private String paymentHeader;

    public AccountBalanceProcess(String fusionPaymentURL) {
        //super(fusionPaymentURL);
        this.paymentBody = new StringBuilder();
    }

    @Override
    public void buildTransactionHeader() {
        this.paymentHeader = "<BalanceEnquiryRequestMessage>\n"
                + "<CompanyCode>" + "TBC" + "</CompanyCode>\n"
                + "<BalanceEnquiryRequest>\n";
        this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildTransactionBody() {
        List<AccountBalanceBean> fusionPaymentList = new ArrayList<>();
        int count = Integer.parseInt(getFusionJsonObj().get("count").toString());
        JSONArray items = getFusionJsonObj().getJSONArray("items");

        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject paymentJsonObj = items.getJSONObject(i);
                AccountBalanceBean accountBalanceBean = new AccountBalanceBean(paymentJsonObj.get("DisbursementBankAccountNumber").toString());

                this.paymentBody.append(accountBalanceBean.toString());

                fusionPaymentList.add(accountBalanceBean);
            }
        }
    }

    @Override
    public void buildPaymentFooter() {
        this.paymentBody.append("</BalanceEnquiryRequest>\n"
                + "</BalanceEnquiryRequestMessage>\n");
    }

    @Override
    public StringBuilder buildFullRequest() {
        buildTransactionHeader();
        buildTransactionBody();
        buildPaymentFooter();

        return this.paymentBody;
    }

    public static void main(String[] args) {

    }

}
