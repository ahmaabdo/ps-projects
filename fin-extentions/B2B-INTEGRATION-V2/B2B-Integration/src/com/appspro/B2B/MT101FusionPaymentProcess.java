/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.MTFusionPaymentBean;
import com.appspro.dao.LoggerDAO;
import com.appspro.rest.paymentRest;
import com.appspro.restHelper.RestHelper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.json.XML;

/**
 *
 * @author HP
 */
public class MT101FusionPaymentProcess extends MTFusionPaymentProcess {

    private String paymentHeader;
    private String companyCode;
    private long sequenceNumber;
    private StringBuilder paymentBody;
    private List<MTFusionPaymentBean> fusionPaymentList;
    private String fusionPaymentURL;
    private JSONObject paymentResponse;
    private String transactionId;
    private String paymentDate;
    private String paymentCode;
    private String toAccount ;
    private String bankCode;
    private String bankName;

    public MT101FusionPaymentProcess(String fusionPaymentURL , String transactionId,String paymentDate ,String paymentCode,String toAccount,String bankCode, String bankName) {
//        super(fusionPaymentURL);
        this.paymentHeader = null;
        this.companyCode = "TBD";
        this.sequenceNumber = System.currentTimeMillis()/100000;
        this.paymentBody = new StringBuilder();
        this.fusionPaymentList = new ArrayList<>();
        this.fusionPaymentURL = fusionPaymentURL;
        this.paymentResponse = new JSONObject();
        this.transactionId = transactionId;
        this.paymentDate= paymentDate;
        this.paymentCode =paymentCode;
        this.toAccount =toAccount;
        this.bankCode= bankCode;
        this.bankName =bankName;
      
    }

    @Override
    public void buildTransactionBody() {
        
        List<String> relatedInvList = new ArrayList<>();
        int count = Integer.parseInt( this.paymentResponse.get("count").toString());
        JSONArray items = this.paymentResponse.getJSONArray("items");
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject paymentJsonObj = items.getJSONObject(i);

                JSONArray relatedInvoices = new JSONArray(paymentJsonObj.get("relatedInvoices").toString());

                for (int j = 0; j < relatedInvoices.length(); j++) {

                    JSONObject relatedInvoicesJsonObj = relatedInvoices.getJSONObject(j);
                    
                    relatedInvList.add(relatedInvoicesJsonObj.get("InvoiceNumber").toString());

                    System.out.println(relatedInvoicesJsonObj.get("InvoiceNumber"));
                }
                    
                String seqStr = "X"+(this.sequenceNumber)+"X"+paymentJsonObj.get("PaymentNumber").toString();
                    
                    MTFusionPaymentBean fusionPaymentBean = new MTFusionPaymentBean(paymentJsonObj.get("PaymentNumber").toString(),
                         this.paymentDate,//  paymentJsonObj.get("PaymentDate").toString(),
                            paymentJsonObj.get("DisbursementBankAccountNumber").toString(),
                            paymentJsonObj.get("DisbursementBankAccountName").toString(),
                                                                               
                            paymentJsonObj.get("PaymentAmount").toString(),
                            paymentJsonObj.get("PaymentType").toString(),
                            paymentJsonObj.get("PaymentCurrency").toString(),
                            paymentJsonObj.get("SupplierNumber").toString(),
                            "",
                            101,
                             this.toAccount.toString(),seqStr ,this.paymentCode , relatedInvList,this.bankCode, this.bankName);
                    System.out.println("hereeee " + paymentJsonObj.get("PaymentNumber").toString() + " == " + this.sequenceNumber +" == ");
                    
                    
                    LoggerDAO log = new LoggerDAO();
                    
                    
                    log.updateLogTransaction(this.transactionId, String.valueOf(this.sequenceNumber),seqStr, this.paymentDate);
                    
                    

                    this.fusionPaymentList.add(fusionPaymentBean);

               // }

            }

        }
    }
    
    


    @Override
    public void buildTransactionHeader() {
        this.paymentHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<PaymentMessage>\n"
                + "<PaymentTransaction>\n"
                + "<CompanyCode>" + companyCode + "</CompanyCode>\n"
                + "<SequenceNum>" + sequenceNumber + "</SequenceNum>\n";
             //+ "<SequenceNum>" + "202012300105889"+ "</SequenceNum>\n";
        

        this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildPaymentFooter() {

        this.paymentBody.append("</PaymentTransaction>\n"
                + "</PaymentMessage>");

    }

    @Override
    public StringBuilder buildFullRequest() {

        buildTransactionHeader();
        buildTransactionBody();
        LoggerDAO log = new LoggerDAO();

        for (int i = 0; i < this.fusionPaymentList.size(); i++) {

            this.paymentBody.append(this.fusionPaymentList.get(i).toString());
        }

        buildPaymentFooter();
        
        
        log.createLogEventlogger( transactionId,"attemping to Create Bank Transaction",
                                       "Create Bank Transaction" ,"log" ,
                                      this.paymentBody.toString());
        
        
        String signature= buildTransactionSignature(this.paymentBody.toString());
        
        
         
         StringBuilder builder = new StringBuilder(signature);
         

        return builder;

    }
    
    
    public JSONObject createPaymentTransactionProcess(String bankServiceURL,
                                               String transactionId,
                                                      String paymentId, String ibanNumber) throws IOException,
                                                                                   KeyStoreException,
                                                                                   NoSuchAlgorithmException,
                                                                                   CertificateException,
                                                                                   UnrecoverableKeyException,
                                                                                   KeyManagementException {

        JSONObject jsonResponse = new JSONObject();
        String encodedPayload = null;
        StringBuilder paymentBody;

        //add logger

        LoggerDAO log = new LoggerDAO();
        System.out.println(this.fusionPaymentURL);
        System.out.println("attemping to get payment from fusion");

        log.createLogEvent(transactionId, "Get Fusion Payment",
                           "attemping to get payment from fusion");


         JSONObject json = RestHelper.callHttpGet(this.fusionPaymentURL);
                                      

        System.out.print( json.get("responseCode"));

        if ("200".equals( json.get("responseCode").toString())) {
        
        
        
            //add logger
            System.out.println("success get payment from fusion");

            log.createLogEvent(transactionId, "Get Fusion Payment",
                               "Getting Fusion Payment",
                                json.toString(), "200", "success");


            jsonResponse.put("message", "success get payment from fusion");
            jsonResponse.put("statue code", "200");
            
            if(checkIban(ibanNumber,paymentId) == false){
                

                    jsonResponse.put("message", "Invalid IBAN Number");
                    jsonResponse.put("statue code", "500");
                    log.createLogEvent(transactionId, "Validate IBAN Number",
                                       "IBAN "+ibanNumber+" IS NOT VALID",
                                       "failed");
                    
                    
                    return jsonResponse;
                
                
                }
            


            try {

                //add logger start building the request

                System.out.println("attemp creating the payload");

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "attemping to Create Transaction Payload");
                
                JSONObject respData = new JSONObject(json.get("responseData").toString());
                
                
                
                
                this.paymentResponse = respData ;


                paymentBody = buildFullRequest();
                String currentDateTime = GetDataAndTime();

                System.out.println("success creating the payload");

                //add logger end building the request

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload created", "success");

                jsonResponse.put("message", "success creating the payload");
                jsonResponse.put("statue code", "200");

  


            } catch (Exception e) {
                System.out.print("faild creating the payload");
                System.out.print(e.getMessage());
                jsonResponse.put("message", e.getMessage());
                jsonResponse.put("statue code", "500");
                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload not created",
                                   "failed");

                return jsonResponse;
            }


        } else {

            //add logger

            System.out.print("failed to get payment from fusion");

            jsonResponse.put("message", "failed to get payment from fusion");
            jsonResponse.put("statue code", "500");

            log.createLogEvent(transactionId, "Get Fusion Payment",
                               json.get("responseData").toString(),
                               json.toString(),
                               json.get("responseCode").toString(),
                               "failed");

            return jsonResponse;

        }

        //to be uncommented


        if ("200".equals(jsonResponse.get("statue code").toString()))
            //            && encodedPayload != null
        {


            log.createLogEvent(transactionId, "Create Bank Transaction",
                               "attemping to Create Bank Transaction");

            System.out.println("hereeeeeeeeeeeeeeee every thing is done");

            JSONObject bankRes =
                RestHelper.callHttpPost(bankServiceURL, "text/xml",
                                        paymentBody.toString());


            if ("200".equals(bankRes.get("responseCode").toString()) ||
                "201".equals(bankRes.get("responseCode").toString()) ||
                "204".equals(bankRes.get("responseCode").toString())) {

                
        
                try {

                    JSONObject responseData =
                        XML.toJSONObject(bankRes.get("responseData").toString());
                    System.out.println(responseData);
                    if (responseData.getJSONObject("PaymentMessageResponse").getJSONObject("PaymentTransactionResponse").get("StatusCode").toString().equalsIgnoreCase("OK")) {

                        log.createLogEvent(transactionId,
                                           "Create Bank Transaction",
                                           "Bank Transaction Created",
                                           bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "success", paymentBody.toString());
                        bankRes.append("message","success");
                        //updateDFF
                        Boolean updateStatus =
                            getPaymentAndUpdateCheckId(paymentId);
                        if (updateStatus) {
                            log.createLogEvent(transactionId,
                                               "Update Fusion DFF  Status",
                                               "Fusion DFF updated", "", "",
                                               "success", "");
                            log.updateDffLogTransaction(transactionId,"PAID");

                        } else {
                            log.createLogEvent(transactionId,
                                               "Update Fusion DFF  Status",
                                               "Fusion DFF Not updated", "",
                                               "", "failed", "");
                        }

                    } else {


                        String mt101failMessage =
                            responseData.getJSONObject("PaymentMessageResponse").getJSONObject("PaymentTransactionResponse").get("StatusDetail").toString();
                        bankRes.append("message", mt101failMessage);
                        log.createLogEvent(transactionId,
                                           "Create Bank Transaction",
                                           "Bank Transaction Created",
                                           bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "fail", paymentBody.toString());
                        log.updateDffLogTransaction(transactionId,mt101failMessage);

                    }
                } catch (Exception e) {

                    bankRes.append("message", e.getMessage());

                }


            } else {


                log.createLogEvent(transactionId, "Create Bank Transaction",
                                   "Bank Transaction Failed",
                                   paymentBody.toString(),
                                   bankRes.get("responseCode").toString(),
                                   "failed", paymentBody.toString());
                bankRes.append("message",
                               bankRes.get("responseData").toString());
            }


            return jsonResponse.put("bankResponse", bankRes);

        } else {

            jsonResponse.put("message", "INTERNAL SERVER ERROR");
            jsonResponse.put("statue code", "500");

            return jsonResponse;
        }


    }


    public static void main(String[] args) throws KeyStoreException,
                                                  NoSuchAlgorithmException,
                                                  CertificateException,
                                                  UnrecoverableKeyException,
                                                  KeyManagementException {
        
        
        String ibanString = "SA8365000000120199900001";
        
        List<String> ibanList = new ArrayList<String>();
        
        paymentRest pR = new paymentRest();

        JSONObject json =
            RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?q=PaymentId=6004");
        
        JSONObject itemsObj = new JSONObject(json.get("responseData").toString());

        JSONArray itemsArr = new JSONArray(itemsObj.get("items").toString());
        
        JSONObject itemObj = (JSONObject)itemsArr.get(0);
        System.out.println("null".equalsIgnoreCase(itemObj.get("SupplierNumber").toString()));

        if ("null".equalsIgnoreCase(itemObj.get("SupplierNumber").toString())) {

            Response res =
                pR.getEmployeeBankDetails(itemObj.get("ExternalBankAccountId").toString());




            JSONObject ibanRes = new JSONObject(res.getEntity().toString());


            ibanList.add(ibanRes.get("IBAN").toString());


            //go for employee
        } else {
            //go for supplier
            
            Response res = pR.getSupplierDetailsByNumber(itemObj.get("SupplierNumber").toString());
            
            JSONObject suppObj = new JSONObject(res.getEntity().toString());
            JSONObject data_DS = new JSONObject(suppObj.get("responseData").toString());
            JSONObject dataDsObj = new JSONObject(data_DS.get("DATA_DS").toString());
            
            try{
                
                if(dataDsObj.get("G_1") instanceof JSONObject){
                    
                    JSONObject g1 =(JSONObject) dataDsObj.get("G_1");
                    
                    System.out.println(g1);
                    
              ibanList.add(g1.get("IBAN_NUMBER").toString());
                    
                }
                
                if(dataDsObj.get("G_1") instanceof JSONArray){
                    
                        JSONArray g1Arr = new JSONArray(dataDsObj.get("G_1").toString());
                    
                        for(int i = 0 ; i < g1Arr.length() ; i++){
                            
                            
                                JSONObject g1Obj = (JSONObject)g1Arr.get(i);
                            
//                                System.out.println(g1Obj.get("IBAN_NUMBER"));
                            
                                ibanList.add(g1Obj.get("IBAN_NUMBER").toString());
                            
                            
                            
                            
                            
                            }
                    
                        System.out.println("=================");
                    
                    
                    }
            
//            System.out.println(dataDsObj.get("G_1") instanceof JSONArray );
            }catch(Exception e){
                
                    System.out.println(e.getMessage() );
                }
            

        }
        
        System.out.println("=================");
        System.out.println(ibanList.contains(ibanString));
        
        
        
        
    }


}
