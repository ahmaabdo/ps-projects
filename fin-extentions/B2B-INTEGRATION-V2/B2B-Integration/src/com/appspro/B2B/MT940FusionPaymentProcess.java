/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.MTFusionPaymentBean;
import com.appspro.dao.LoggerDAO;
import com.appspro.restHelper.RestHelper;

import java.io.IOException;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author HP
 */
public class MT940FusionPaymentProcess extends MTFusionPaymentProcess {

    private String bankCode;
    private String paymentHeader;
    private StringBuilder paymentBody;
    private String transactionDate;
    private String accountNumber;

    public MT940FusionPaymentProcess(String transactionDate, String accountNumber) {
//        super(fusionPaymentURL);
        this.bankCode = "SABBSARI";
        this.paymentBody = new StringBuilder();  
        this.transactionDate = transactionDate;
        this.accountNumber = accountNumber;

    }

    @Override
    public void buildTransactionHeader() {

      this.paymentHeader=  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<PaymentInfoMessage>\n"
                + "<PaymentInfoRequest>\n"
                + "<CompanyCode>" + "TBD" + "</CompanyCode>\n"
                + "<BankCode>" + bankCode + "</BankCode>\n"
                + "<Date>" + this.transactionDate + "</Date>\n"
                + "<TransactionType>940</TransactionType>\n"
                + "<AccountNumbers>\n";
      
      this.paymentBody.append(this.paymentHeader);

    }

    @Override
    public void  buildTransactionBody() {
        
        String heading = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<PaymentInfoMessage>\n"
                + "<PaymentInfoRequest>\n"
                + "<CompanyCode>" + "TBD" + "</CompanyCode>\n"
                + "<BankCode>" + bankCode + "</BankCode>\n"
                + "<Date>" + this.transactionDate + "</Date>\n"
                + "<TransactionType>940</TransactionType>\n"
                + "<AccountNumbers>\n";


                MTFusionPaymentBean fusionPaymentBean = new MTFusionPaymentBean(
                        this.accountNumber,
                        940);
                
                this.paymentBody.append(fusionPaymentBean.toString());

    }
    
    @Override
    public void buildPaymentFooter(){

        this.paymentBody.append("</AccountNumbers>\n"
                + "</PaymentInfoRequest>\n"
                + "</PaymentInfoMessage>");

    }

    @Override
    public StringBuilder buildFullRequest() {
         buildTransactionHeader();
         buildTransactionBody();
        LoggerDAO log = new LoggerDAO();
       

         buildPaymentFooter();
        
        
        String signature= buildTransactionSignatureBodyReturn(this.paymentBody.toString());
        
        
         
         StringBuilder builder = new StringBuilder(this.paymentBody.toString().replaceAll("</PaymentInfoMessage>", signature.concat("</PaymentInfoMessage>\n")));
         
        System.out.println(builder.toString());
         
         return builder;
    }
    
    public JSONObject getMt940TransactionProcess(String bankServiceURL,
                                               String transactionId) throws IOException,
                                                                              KeyStoreException,
                                                                              NoSuchAlgorithmException,
                                                                              CertificateException,
                                                                              UnrecoverableKeyException,
                                                                              KeyManagementException {

        JSONObject jsonResponse = new JSONObject();
        String encodedPayload = null;
        StringBuilder paymentBody;

        //add logger

        LoggerDAO log = new LoggerDAO();
    

            try {

                //add logger start building the request

                System.out.println("attemp creating the payload");

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "attemping to Create Transaction Payload");

                paymentBody = buildFullRequest();
                String currentDateTime = GetDataAndTime();

                System.out.println("success creating the payload");

                //add logger end building the request

                log.createLogEvent(transactionId, paymentBody.toString(),
                                   "Transaction Payload created", "success");

                jsonResponse.put("message", "success creating the payload");
                jsonResponse.put("statue code", "200");


            } catch (Exception e) {
                e.printStackTrace();
                System.out.print("faild creating the payload");
                System.out.print(e.getMessage());
                jsonResponse.put("message", e.getMessage());
                jsonResponse.put("statue code", "500");
                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload not created",
                                   "failed");

                return jsonResponse;
            }



        if ("200".equals(jsonResponse.get("statue code").toString()) 
    //            && encodedPayload != null
           ) {


            log.createLogEvent(transactionId, "Create Bank Transaction",
                               "attemping to Create Bank Transaction");

            System.out.println("hereeeeeeeeeeeeeeee every thing is done");

            JSONObject bankRes =
                RestHelper.callHttpPost(bankServiceURL, "text/xml",
                                        paymentBody.toString());

            if ("200".equals(bankRes.get("responseCode").toString()) ||
                "201".equals(bankRes.get("responseCode").toString()) ||
                "204".equals(bankRes.get("responseCode").toString())) {

           
                
                //here update the event_logger with reference number and sequance

                try {

                    JSONObject responseData =
                        XML.toJSONObject(bankRes.get("responseData").toString());
                    System.out.println(responseData);
                    if (responseData.getJSONObject("PaymentInfoMessageResponse").get("StatusCode").toString().equalsIgnoreCase("OK")) {
                        bankRes.append("MT940response",
                                       mt940Response(bankRes.get("responseData").toString()));
                        bankRes.append("message", "success");
                        
                        log.createLogEvent(transactionId, "Create Bank Transaction",
                                           "Bank Transaction Created",
                                            bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "success", paymentBody.toString());
                    } else {
                        String failcomment =
                            responseData.getJSONObject("PaymentInfoMessageResponse").getJSONObject("PaymentInfoResponse").has("Comment") ?
                            responseData.getJSONObject("PaymentInfoMessageResponse").getJSONObject("PaymentInfoResponse").get("Comment").toString() :
                            "";
                        String statusDetail =
                            responseData.getJSONObject("PaymentInfoMessageResponse").has("StatusDetail") ?
                            responseData.getJSONObject("PaymentInfoMessageResponse").get("StatusDetail").toString() :
                            "";

                        //  bankFailres.put
                        bankRes.append("message",
                                       "".equals(failcomment) ? statusDetail :
                                       failcomment);
                        log.createLogEvent(transactionId, "Create Bank Transaction",
                                           "Bank Transaction Created",
                                            bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "fail", paymentBody.toString());

                    }
                } catch (Exception e) {

                    bankRes.append("message", e.getMessage());

                }


            } else {


                log.createLogEvent(transactionId, "Create Bank Transaction",
                                   "Bank Transaction Failed",
                                   bankRes.get("responseData").toString(),
                                   bankRes.get("responseCode").toString(),
                                   "failed", paymentBody.toString());

                bankRes.append("message",
                               bankRes.get("responseData").toString());
                System.out.println(  bankRes.get("responseData").toString());


            }


            return jsonResponse.put("bankResponse", bankRes);

        } else {

            jsonResponse.put("message", "INTERNAL SERVER ERROR");
            jsonResponse.put("statue code", "500");

            return jsonResponse;
        }


    }

    public static void main(String[] args) {
String ff = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PaymentMessageResponse>\n" + 
"<PaymentTransactionResponse>\n" + 
"<CompanyCode>CORPLCB</CompanyCode>\n" + 
"<SequenceNum>201911100105916</SequenceNum>\n" + 
"<TransactionRef>20191105015</TransactionRef>\n" + 
"<StatusCode>DE</StatusCode>\n" + 
"<StatusDetail>Tag 28D (Message Index) Total Index value should be always same as no. of transactions</StatusDetail>\n" + 
"</PaymentTransactionResponse>\n" + 
"</PaymentMessageResponse>";
    JSONObject postPaymentobj = XML.toJSONObject(ff);
        System.out.println(postPaymentobj);

    }

}
