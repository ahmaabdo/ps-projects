/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.CancelPaymentBean;
import com.appspro.bean.PostPaymentBean;
import com.appspro.dao.LoggerDAO;
import com.appspro.restHelper.RestHelper;

import java.io.IOException;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author hp
 */
public class CancelPaymentTransactionProcess extends MTFusionPaymentProcess {

    private StringBuilder paymentBody;
    private String paymentHeader;
    private JSONObject payload;

    public CancelPaymentTransactionProcess(JSONObject payload) {
//        super(fusionPaymentURL);
        this.paymentBody = new StringBuilder();
        this.payload = payload;
    }
    
    @Override
    public void buildTransactionHeader() {
           this.paymentHeader=  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<StopPaymentRequestMessage>\n"
                + "<StopPaymentRequest>\n"
                + "<CompanyCode>" + "TBD" + "</CompanyCode>\n";
      this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildTransactionBody() {
        List<CancelPaymentBean> fusionPaymentList = new ArrayList<>();
        
        System.out.println(this.payload.toString());
                
                CancelPaymentBean cancelPaymentBean = new CancelPaymentBean(this.payload.get("fileReference").toString(),this.payload.get("transactionReference").toString(),this.payload.get("valueDate").toString());

                this.paymentBody.append(cancelPaymentBean.toString());
                
                fusionPaymentList.add(cancelPaymentBean);
        

    }

    @Override
    public void buildPaymentFooter() {
        this.paymentBody.append("</StopPaymentRequest>\n" + 
        "</StopPaymentRequestMessage>");
    }

    @Override
    public StringBuilder buildFullRequest() {
        buildTransactionHeader();
        buildTransactionBody();
        buildPaymentFooter();
        
        String signature= buildTransactionSignatureBodyReturn(this.paymentBody.toString());
        StringBuilder builder = new StringBuilder(this.paymentBody.toString().replaceAll("</StopPaymentRequestMessage>", signature.concat("</StopPaymentRequestMessage>\n")));
         
        return builder;    
    }
    
    
    public JSONObject cancelPaymentTransactionProcess(String bankServiceURL,
                                               String transactionId,
                                                      String fileReference,
                                                      String transactionReference) throws IOException,
                                                                                   KeyStoreException,
                                                                                   NoSuchAlgorithmException,
                                                                                   CertificateException,
                                                                                   UnrecoverableKeyException,
                                                                                   KeyManagementException {

        JSONObject jsonResponse = new JSONObject();
        String encodedPayload = null;
        StringBuilder paymentBody;

        //add logger

        LoggerDAO log = new LoggerDAO();
    

            try {

                //add logger start building the request

                System.out.println("attemp creating the payload");

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "attemping to Create Transaction Payload");

                paymentBody = buildFullRequest();
                log.updateLogTransaction(transactionId, transactionReference , fileReference, "");

                System.out.println(paymentBody.toString());
                String currentDateTime = GetDataAndTime();

                System.out.println("success creating the payload");

                //add logger end building the request

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload created", "success");

                jsonResponse.put("message", "success creating the payload");
                jsonResponse.put("statue code", "200");



            } catch (Exception e) {
                System.out.print("faild creating the payload");
                System.out.print(e.getMessage());
                jsonResponse.put("message", e.getMessage());
                jsonResponse.put("statue code", "500");
                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload not created",
                                   "failed");

                return jsonResponse;
            }


     

        //to be uncommented


        if ("200".equals(jsonResponse.get("statue code").toString()) 
    //            && encodedPayload != null
           ) {


            log.createLogEvent(transactionId, "Create Bank Transaction",
                               "attemping to Create Bank Transaction");

            System.out.println("hereeeeeeeeeeeeeeee every thing is done");
//
//            JSONObject bankRes =
//                RestHelper.callHttpPost(bankServiceURL, "text/xml",
//                                        paymentBody.toString());
            
JSONObject bankRes = new JSONObject();
            bankRes.put("responseCode", 200);
            bankRes.put("responseData", "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
           
            " <StopPaymentResponse>\n" + 
            " <CompanyCode>ALFARIS</CompanyCode>\n" + 
            " <PaymentReferenceNumber>201707170115464</PaymentReferenceNumber>\n" + 
            " <TransactionReference>201911210100630</TransactionReference>\n" + 
            " <PaymentValueDate>2017-07-19</PaymentValueDate>\n" + 
            " <Status>OK</Status>\n" + 
            " <Description>Payment Stopped Successfuly</Description>\n" + 
            " </StopPaymentResponse>\n" );
            if ("200".equals(bankRes.get("responseCode").toString()) ||
                "201".equals(bankRes.get("responseCode").toString()) ||
                "204".equals(bankRes.get("responseCode").toString())) {

//                log.createLogEvent(transactionId, "Create Bank Transaction",
//                                   "Bank Transaction Created",
//                                  "",
//                                   bankRes.get("responseCode").toString(),
//                                   "success", paymentBody.toString());
                
                //here update the event_logger with reference number and sequance
                
                try {

                    JSONObject responseData =
                        XML.toJSONObject(bankRes.get("responseData").toString());
                    
                    System.out.println(responseData);
                    if (responseData.getJSONObject("StopPaymentResponse").get("Status").toString().equalsIgnoreCase("OK")) {
                        bankRes.append("message","success");
                        log.createLogEvent(transactionId,
                                           "Create Bank Transaction",
                                           "Bank Transaction Created",
                                           bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "success", paymentBody.toString());
                        
                        //upadet status to canceled
                        log.updatecancelTransaction(this.payload.get("fileReference").toString(),this.payload.get("transactionReference").toString());

                    } else {


                        String cancelDescription =
                            responseData.getJSONObject("StopPaymentResponse").get("Description").toString();
                        bankRes.append("message", cancelDescription);
                        log.createLogEvent(transactionId,
                                           "Create Bank Transaction",
                                           "Bank Transaction Created",
                                           bankRes.get("responseData").toString(),
                                           bankRes.get("responseCode").toString(),
                                           "fail", paymentBody.toString());

                    }
                } catch (Exception e) {

                    bankRes.append("message", e.getMessage());

                }
                
                
            } else {


                log.createLogEvent(transactionId, "Create Bank Transaction",
                                   "Bank Transaction Failed",
                                   paymentBody.toString(),
                                   bankRes.get("responseCode").toString(),
                                   "failed", paymentBody.toString());
                bankRes.append("message",
                               bankRes.get("responseData").toString());

            }


            return jsonResponse.put("bankResponse", bankRes);

        } else {

            jsonResponse.put("message", "INTERNAL SERVER ERROR");
            jsonResponse.put("statue code", "500");

            return jsonResponse;
        }


    }
    
     public static void main(String[] args) {
         
  

    }
    
}
