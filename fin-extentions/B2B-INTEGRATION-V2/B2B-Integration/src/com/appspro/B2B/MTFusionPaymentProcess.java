/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.MT940TransactionResponsseBean;
import com.appspro.bean.MT940responseBean;
import static com.appspro.B2B.CreateB2BFile.GetDataAndTime;
import com.appspro.bean.MTFusionPaymentBean;
import com.appspro.bean.SadadAndMoiBean;
import com.appspro.dao.LoggerDAO;
import com.appspro.rest.paymentRest;
import com.appspro.restHelper.RestHelper;
import static com.appspro.B2B.CreateB2BFile.buildTransaction;
import static com.appspro.B2B.CreateB2BFile.keyStoreType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import java.math.BigInteger;

import java.security.Key;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONObject;

import org.json.XML;

import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;


import org.xml.sax.InputSource;

/**
 *
 * @author HP
 */
public abstract class MTFusionPaymentProcess {

    static String keyStore =
        "/u01/data/domains/TBCLoadJ_domain/certs/v2/identity.p12";
    static String keyPass = "tbcloadjcsjcs-wls";
    static String alias = "tbcloadjcsjcs-wls";
    static String keyStoreType = "PKCS12";

    private String fusionPaymentURL;
    private JSONObject fusionJsonObj;
    RestHelper rs = new RestHelper();

    public MTFusionPaymentProcess() {

    }

    public JSONObject getFusionJsonObj() {
        return fusionJsonObj;
    }

    public void setFusionJsonObj(JSONObject jsonObj) {
        this.fusionJsonObj = jsonObj;
    }

    private JSONObject getFusionPayments() throws KeyStoreException,
                                                  NoSuchAlgorithmException,
                                                  CertificateException,
                                                  UnrecoverableKeyException,
                                                  KeyManagementException {

        JSONObject json = RestHelper.callHttpGet(this.fusionPaymentURL);

        System.out.print(json.toString());

        if ("200".equals(json.get("responseCode").toString())) {

            fusionJsonObj =
                    new JSONObject(json.get("responseData").toString());
        }


        return json;
    }

    public abstract void buildTransactionBody();

    public abstract void buildTransactionHeader();

    public abstract void buildPaymentFooter();

    public abstract StringBuilder buildFullRequest();

    public String GetDataAndTime() {
        SimpleDateFormat formatter =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        String dateTime =
            formatter.format(date).replaceAll("/", "").replaceAll(":",
                                                                  "").replaceAll(" ",
                                                                                 "");

        return dateTime;
    }

    public String GetDataAndTimeForBody() {
        SimpleDateFormat formatter =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        String dateTime =
            formatter.format(date); //.replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        return dateTime;
    }

    public JSONObject createTransactionProcess(String bankServiceURL,
                                               String transactionId) throws IOException,
                                                                            KeyStoreException,
                                                                            NoSuchAlgorithmException,
                                                                            CertificateException,
                                                                            UnrecoverableKeyException,
                                                                            KeyManagementException {

        JSONObject jsonResponse = new JSONObject();
        String encodedPayload = null;
        StringBuilder paymentBody;

        //add logger

        LoggerDAO log = new LoggerDAO();

        System.out.println("attemping to get payment from fusion");

        log.createLogEvent(transactionId, "Get Fusion Payment",
                           "attemping to get payment from fusion");

        JSONObject fusionResponseObj = getFusionPayments();

        System.out.print(fusionResponseObj.get("responseCode"));

        if ("200".equals(fusionResponseObj.get("responseCode").toString())) {
            //add logger
            System.out.println("success get payment from fusion");

            log.createLogEvent(transactionId, "Get Fusion Payment",
                               "Getting Fusion Payment",
                               fusionResponseObj.toString(), "200", "success");


            jsonResponse.put("message", "success get payment from fusion");
            jsonResponse.put("statue code", "200");


            try {

                //add logger start building the request

                System.out.println("attemp creating the payload");

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "attemping to Create Transaction Payload");


                paymentBody = buildFullRequest();
                String currentDateTime = GetDataAndTime();

                System.out.println("success creating the payload");

                //add logger end building the request

                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload created", "success");

                jsonResponse.put("message", "success creating the payload");
                jsonResponse.put("statue code", "200");


                try {

                    //add logger start create the file

                    System.out.println("start creating the file");

                    log.createLogEvent(transactionId,
                                       "Create Transaction File",
                                       "attemping to Create Transaction File");

                    //                    File file = File.createTempFile(currentDateTime, ".txt");
                    //                    FileWriter writer = new FileWriter(file);
                    //                    writer.write(paymentBody.toString());
                    //
                    //                    writer.close();

                    log.createLogEvent(transactionId,
                                       "Create Transaction File",
                                       "Transaction File created", "success");

                    System.out.println("success creating the file");

                    jsonResponse.put("message", "success creating the file");
                    jsonResponse.put("statue code", "200");

                    //add logger end create the file


                    log.createLogEvent(transactionId,
                                       "Create Transaction Encoded File",
                                       "attemping to Create Transaction Encoded File");

                    encodedPayload = paymentBody.toString();

                    if ("File not found".equals(encodedPayload) ||
                        "Exception while reading the file".equals(encodedPayload)) {

                        System.out.println("failed to encode file");

                        jsonResponse.put("message", "failed to encode file");
                        jsonResponse.put("statue code", "500");

                        log.createLogEvent(transactionId,
                                           "Create Transaction Encoded File",
                                           "failed to encode file", "failed");

                        return jsonResponse;
                    } else {

                        System.out.println("success to encode file");
                        jsonResponse.put("message", "file encoded succefully");
                        jsonResponse.put("statue code", "200");

                        log.createLogEvent(transactionId,
                                           "Create Transaction Encoded File",
                                           "Transaction Encoded File created",
                                           "success");

                    }


                } catch (Exception ex) {

                    System.out.println("failed creating the file");
                    System.out.println(ex.getMessage());

                    jsonResponse.put("message",
                                     "failed creating the file: " + ex.getMessage());
                    jsonResponse.put("statue code", "500");

                    log.createLogEvent(transactionId,
                                       "Create Transaction File",
                                       "Transaction File Failed", "failed");

                    return jsonResponse;
                }


            } catch (Exception e) {
                System.out.print("faild creating the payload");
                System.out.print(e.getMessage());
                jsonResponse.put("message", e.getMessage());
                jsonResponse.put("statue code", "500");
                log.createLogEvent(transactionId, "Create Transaction Payload",
                                   "Transaction Payload not created",
                                   "failed");

                return jsonResponse;
            }


        } else {

            //add logger

            System.out.print("failed to get payment from fusion");

            jsonResponse.put("message", "failed to get payment from fusion");
            jsonResponse.put("statue code", "500");

            log.createLogEvent(transactionId, "Get Fusion Payment",
                               fusionResponseObj.get("responseData").toString(),
                               fusionResponseObj.toString(),
                               fusionResponseObj.get("responseCode").toString(),
                               "failed");

            return jsonResponse;

        }

        //to be uncommented


        if ("200".equals(jsonResponse.get("statue code").toString()) &&
            encodedPayload != null) {


            log.createLogEvent(transactionId, "Create Bank Transaction",
                               "attemping to Create Bank Transaction");

            System.out.println("hereeeeeeeeeeeeeeee every thing is done");

            JSONObject bankRes =
                RestHelper.callHttpPost(bankServiceURL, "text/xml",
                                        encodedPayload);


            if ("200".equals(bankRes.get("responseCode").toString()) ||
                "201".equals(bankRes.get("responseCode").toString()) ||
                "204".equals(bankRes.get("responseCode").toString())) {

                log.createLogEvent(transactionId, "Create Bank Transaction",
                                   "Bank Transaction Created",
                                   fusionResponseObj.toString(),
                                   bankRes.get("responseCode").toString(),
                                   "success", encodedPayload);
            } else {


                log.createLogEvent(transactionId, "Create Bank Transaction",
                                   "Bank Transaction Failed",
                                   paymentBody.toString(),
                                   bankRes.get("responseCode").toString(),
                                   "failed", encodedPayload);

            }


            return jsonResponse.put("bankResponse", bankRes);

        } else {

            jsonResponse.put("message", "INTERNAL SERVER ERROR");
            jsonResponse.put("statue code", "500");

            return jsonResponse;
        }


        //to be removed

        //        return jsonResponse;


        //        StringBuilder paymentBody = buildFullRequest();
        //        String currentDateTime = GetDataAndTime();
        //        System.out.println("");
        //
        //        System.out.println(paymentBody.toString());
        //
        //        File file = File.createTempFile(currentDateTime, ".txt");
        //        FileWriter writer = new FileWriter(file);
        //        writer.write(paymentBody.toString());
        //
        //        writer.close();
        //
        //
        //
        //
        //        return RestHelper.callHttpPost(bankServiceURL, "text/xml",
        //                                       encodedPayload);


    }

    public String buildTransactionSignature(String str) {
        String result = "";
        try {
            KeyStore clientStore = KeyStore.getInstance(keyStoreType);
            clientStore.load(new FileInputStream(keyStore),
                             keyPass.toCharArray());
            X509Certificate c =
                (X509Certificate)clientStore.getCertificate(alias);
            byte[] dataToSign = str.getBytes("cp1256");
            Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initSign((PrivateKey)clientStore.getKey(alias,
                                                              keyPass.toCharArray()));
            signature.update(dataToSign);
            byte[] signedData = signature.sign();
            X500Name xName = X500Name.asX500Name(c.getIssuerX500Principal());
            BigInteger serial = c.getSerialNumber();
            AlgorithmId digestAlgorithmId =
                new AlgorithmId(AlgorithmId.SHA_oid);
            AlgorithmId signAlgorithmId =
                new AlgorithmId(AlgorithmId.RSAEncryption_oid);
            SignerInfo sInfo =
                new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId,
                               signedData);
            ContentInfo cInfo =
                new ContentInfo(ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString,
                                                                            dataToSign));
            PKCS7 p7 =
                new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,
                          new java.security.cert.X509Certificate[] { c },
                          new SignerInfo[] { sInfo });
            ByteArrayOutputStream bOut = new DerOutputStream();
            p7.encodeSignedData(bOut);
            byte[] encodedPKCS7 = bOut.toByteArray();
            //result = Base64.getEncoder().encodeToString(encodedPKCS7);
            // result = new String(Base64.encode(encodedPKCS7));
            result = DatatypeConverter.printBase64Binary(encodedPKCS7);
        } catch (Exception e) {
            result = e.toString();
        }
//        return "<Signature>\n" +
//            "<SignatureValue>\n" +
//            result + "\n</SignatureValue>\n" +
//            "</Signature>\n";
        return result;
    }
    public String buildTransactionSignatureBodyReturn(String str) {
        String result = "";
        try {
            KeyStore clientStore = KeyStore.getInstance(keyStoreType);
            clientStore.load(new FileInputStream(keyStore),
                             keyPass.toCharArray());
            X509Certificate c =
                (X509Certificate)clientStore.getCertificate(alias);
            byte[] dataToSign = str.getBytes("cp1256");
            Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initSign((PrivateKey)clientStore.getKey(alias,
                                                              keyPass.toCharArray()));
            signature.update(dataToSign);
            byte[] signedData = signature.sign();
            X500Name xName = X500Name.asX500Name(c.getIssuerX500Principal());
            BigInteger serial = c.getSerialNumber();
            AlgorithmId digestAlgorithmId =
                new AlgorithmId(AlgorithmId.SHA_oid);
            AlgorithmId signAlgorithmId =
                new AlgorithmId(AlgorithmId.RSAEncryption_oid);
            SignerInfo sInfo =
                new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId,
                               signedData);
            ContentInfo cInfo =
                new ContentInfo(ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString,
                                                                            dataToSign));
            PKCS7 p7 =
                new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,
                          new java.security.cert.X509Certificate[] { c },
                          new SignerInfo[] { sInfo });
            ByteArrayOutputStream bOut = new DerOutputStream();
            p7.encodeSignedData(bOut);
            byte[] encodedPKCS7 = bOut.toByteArray();
            //result = Base64.getEncoder().encodeToString(encodedPKCS7);
            // result = new String(Base64.encode(encodedPKCS7));
            result = DatatypeConverter.printBase64Binary(encodedPKCS7);
        } catch (Exception e) {
            result = e.toString();
        }
            return "<Signature>\n" +
                "<SignatureValue>\n" +
                result + "\n</SignatureValue>\n" +
                "</Signature>\n";
        
    }

    public String buildTransactionSignature1(String body) {
        String result = "";
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(new FileInputStream("/u01/data/domains /TBCLoadJ_domain/certs/v2/keystoreSign.jks"),
                          "tbcloadjcsjcs-wls".toCharArray());

            Enumeration<String> aliases = keyStore.aliases();
            String aliaz = "tbcloadjcsjcs-wls";
            while (aliases.hasMoreElements()) {
                aliaz = aliases.nextElement();
                if (keyStore.isKeyEntry(aliaz)) {
                    break;
                }
            }
            X509Certificate c =
                (X509Certificate)keyStore.getCertificate(aliaz);


            byte[] dataToSign = body.getBytes();
            Signature rsa = Signature.getInstance("SHA1withRSA");

            PrivateKey pvtKey =
                (PrivateKey)keyStore.getKey("tbcloadjcsjcs-wls",
                                            "tbcloadjcsjcs-wls".toCharArray());
            rsa.initSign(pvtKey);
            rsa.update(body.getBytes());
            //            result = Base64.getEncoder().encodeToString(); // java 8

            byte[] signedData = rsa.sign();

            //load X500Name
            X500Name xName = X500Name.asX500Name(c.getSubjectX500Principal());
            //load serial number
            BigInteger serial = c.getSerialNumber();
            //laod digest algorithm
            AlgorithmId digestAlgorithmId =
                new AlgorithmId(AlgorithmId.SHA_oid);
            //load signing algorithm
            AlgorithmId signAlgorithmId =
                new AlgorithmId(AlgorithmId.RSAEncryption_oid);

            //Create SignerInfo:
            SignerInfo sInfo =
                new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId,
                               signedData);
            //Create ContentInfo:
            ContentInfo cInfo =
                new ContentInfo(ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString,
                                                                            dataToSign));
            //Create PKCS7 Signed data
            PKCS7 p7 =
                new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,
                          new java.security.cert.X509Certificate[] { c },
                          new SignerInfo[] { sInfo });
            //Write PKCS7 to bYteArray
            ByteArrayOutputStream bOut = new DerOutputStream();
            p7.encodeSignedData(bOut);
            byte[] encodedPKCS7 = bOut.toByteArray();
            result = new String(encodedPKCS7);
            //            result = DatatypeConverter.printBase64Binary(encodedPKCS7);  //  java 6
        } catch (Exception e) {
            //e.printStackTrace();
            result = e.toString();
        }
        return "<Signature>\n" +
            "<SignatureValue>\n" +
            result + "\n</SignatureValue>\n" +
            "</Signature>\n";

    }

    public String encode(File filePath) {
        String base64File = "";
        File file = filePath;
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int)file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
            return base64File;
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);

            return base64File = "File not found";
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file" + ioe);
            return base64File = "Exception while reading the file";
        }

    }


    public String generateRandomNymber() {
        SimpleDateFormat formatter =
            new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String dateTime =
            formatter.format(date).replaceAll("/", "").replaceAll(":",
                                                                  "").replaceAll(" ",
                                                                                 "");

        Random rand = new Random();
        String randStr = Integer.toString(rand.nextInt(100000));


        System.out.println("R_" + dateTime + randStr);


        return "R_" + dateTime + randStr;

    }

    public static JSONObject mt940Response(String MT940response) {

        MT940responseBean mt940bean = new MT940responseBean();
        ArrayList<MT940TransactionResponsseBean> list =
            new ArrayList<MT940TransactionResponsseBean>();


        JSONObject obj = XML.toJSONObject(MT940response);
        String TransactionData =
            obj.getJSONObject("PaymentInfoMessageResponse").getJSONObject("PaymentInfoResponse").get("TransactionData").toString();
        mt940bean.setReference(TransactionData.substring(0,
                                                         TransactionData.indexOf(":25:")));
        
        mt940bean.setAccountNumber(TransactionData.substring(TransactionData.indexOf(":25:"),
                                                             TransactionData.indexOf(":28")));
                
    
        mt940bean.setStatmentNumber(TransactionData.substring(TransactionData.indexOf(":28"),
                                                              TransactionData.indexOf(":60")));
        
        mt940bean.setClosingBalance(TransactionData.substring(TransactionData.indexOf(":62"),
                                                              TransactionData.indexOf(":64")));
        
        mt940bean.setClosingAvilableBalance(TransactionData.substring(TransactionData.indexOf(":62"),
                                                                      TransactionData.indexOf(":64")));
        if(TransactionData.contains(":61:")){
                mt940bean.setOpeningBlance(TransactionData.substring(TransactionData.indexOf(":60"),
                                                                     TransactionData.indexOf(":61")));        
        String[] transactionArr =
            TransactionData.substring(TransactionData.indexOf(":61:"),
                                      TransactionData.indexOf(":62")).split(":61:");
        for (int i = 1; i < transactionArr.length; i++) {
            MT940TransactionResponsseBean bean =
                new MT940TransactionResponsseBean();
            if (transactionArr.length > 0) {
                //  System.out.println(transactionArr[i].substring(0, transactionArr[i].indexOf(":86:")).replaceAll("\\s+",""));
                bean.setTransaction(transactionArr[i].substring(0,
                                                                transactionArr[i].indexOf(":86:")).replaceAll("\\s+",
                                                                                                              ""));
                bean.setComment(transactionArr[i].substring(transactionArr[i].indexOf(":86:")).replaceAll("\\s+",
                                                                                                          ""));
                // System.out.println(transactionArr[i].substring( transactionArr[i].indexOf(":86:")).replaceAll("\\s+",""));
                list.add(bean);

            }
        }
        mt940bean.setTransactions(list);
    }

        System.out.println(new JSONObject(mt940bean));
        return new JSONObject(mt940bean);

    }
    public boolean getPaymentAndUpdateCheckId(String paymentId) throws KeyStoreException,
                                                                           NoSuchAlgorithmException,
                                                                           CertificateException,
                                                                           UnrecoverableKeyException,
                                                                           KeyManagementException {
            JSONObject json =
                RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?onlyData=true&fields=CheckId&q=PaymentId=" +
                                       paymentId);
            System.out.println(json);
            String checkId = null;
            if ("200".equals(json.get("responseCode").toString())) {
                JSONObject respData =
                    new JSONObject(json.get("responseData").toString());
                int count = Integer.parseInt(respData.get("count").toString());
                JSONArray items = respData.getJSONArray("items");
                if (count >= 1) {
                    for (int i = 0; i < items.length(); ++i) {
                        JSONObject paymentJsonObj = items.getJSONObject(i);
                        checkId = paymentJsonObj.get("CheckId").toString();
                    }
                }
            }
            String body = "{\"paymentDff\":[{\"extraInfo\":\"PAID\"}]}";
            JSONObject patchReturn =
                rs.callHttpPatch("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments/" +
                              checkId, "PATCH", body);
            if ("done".equals(patchReturn.get("status").toString())) {
                return true;
            } else {
                return false;
            }
        }


    public static boolean checkIban(String ibanString, String paymentId) {
        
        List<String> ibanList = new ArrayList<String>();

        paymentRest pR = new paymentRest();

        JSONObject json;

        try {
            json =
RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?q=PaymentId=" +
                       paymentId);

            JSONObject itemsObj =
                new JSONObject(json.get("responseData").toString());

            JSONArray itemsArr =
                new JSONArray(itemsObj.get("items").toString());

            JSONObject itemObj = (JSONObject)itemsArr.get(0);

            if ("null".equalsIgnoreCase(itemObj.get("SupplierNumber").toString())) {

                Response res =
                    pR.getEmployeeBankDetails(itemObj.get("ExternalBankAccountId").toString());


                JSONObject ibanRes =
                    new JSONObject(res.getEntity().toString());

                System.out.println(ibanRes.get("IBAN").toString());


                ibanList.add(ibanRes.get("IBAN").toString());


                //go for employee
            } else {
                //go for supplier

                Response res =
                    pR.getSupplierDetailsByNumber(itemObj.get("SupplierNumber").toString());

                JSONObject suppObj =
                    new JSONObject(res.getEntity().toString());
                JSONObject data_DS =
                    new JSONObject(suppObj.get("responseData").toString());
                JSONObject dataDsObj =
                    new JSONObject(data_DS.get("DATA_DS").toString());


                if (dataDsObj.get("G_1") instanceof JSONObject) {

                    JSONObject g1 = (JSONObject)dataDsObj.get("G_1");

                    System.out.println(g1.get("IBAN_NUMBER").toString());

                    ibanList.add(g1.get("IBAN_NUMBER").toString());


                }

                if (dataDsObj.get("G_1") instanceof JSONArray) {

                    JSONArray g1Arr =
                        new JSONArray(dataDsObj.get("G_1").toString());

                    for (int i = 0; i < g1Arr.length(); i++) {

                        JSONObject g1Obj = (JSONObject)g1Arr.get(i);

                        ibanList.add(g1Obj.get("IBAN_NUMBER").toString());
                        
                        System.out.println(g1Obj.get("IBAN_NUMBER").toString());


                    }

                }

            }


            return ibanList.contains(ibanString);


        } catch (Exception e) {

            return false;
        }


    }

    public static void main(String[] args) throws KeyStoreException,
                                                  NoSuchAlgorithmException,
                                                  CertificateException,
                                                  UnrecoverableKeyException,
                                                  KeyManagementException {
System.out.println(checkIban("SA3345000000003455995001","6004"));
    }

}
