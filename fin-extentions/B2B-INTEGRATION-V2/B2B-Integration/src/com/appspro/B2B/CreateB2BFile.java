package com.appspro.B2B;

import com.appspro.bean.SadadAndMoiBean;
import com.appspro.rest.BankIntegrationRest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Base64;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import javax.xml.bind.DatatypeConverter;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;
//import java.util.Base64;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Husam Al-Masri
 */
public class CreateB2BFile {

    static String keyStore = "C:\\Users\\HP\\Desktop\\TBCBankCert.jks";
    static String keyPass = "TBC@123";
    static String alias = "bankCertificate";
    static String keyStoreType = "JKS";

    /*
    
    https://127.0.0.1:8443/TAWAL_B2B_INTG/StartUpServlet
    
     */
    public void CreateAndWriteToFile() {
//        System.out.println(listOfTransaction.toString().replaceAll("[\\[\\]]", "").replaceAll(",", ""););
        String transactionBody = getDataAndBuildTransactionBody().toString().replaceAll("[\\[\\]]", "").replaceAll(",", "");
        PostFileToBank postFileToBank = new PostFileToBank();
        try {
            FileWriter myWriter = new FileWriter("C:\\Users\\HP\\Desktop" + GetDataAndTime() + ".txt");
            myWriter.write(buildTransactionHeader() + "\n" + transactionBody + "\n" + "<Signature>" + "\n" + "<SignatureValue>" + buildTransactionSignature(transactionBody) + "</SignatureValue>" + "\n" + "</Signature>" + "\n" + "</Message>");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
//            System.out.println(encoder("C:\\Users\\DELL\\Desktop\\" + GetDataAndTime() + ".txt"));
            String endodeFile = encode("C:\\Users\\DELL\\Desktop\\" + GetDataAndTime() + ".txt");
            System.out.println(endodeFile);
            postFileToBank.postFileSadad(endodeFile);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static String buildTransactionHeader() {
        SadadAndMoiBean sadadBean = new SadadAndMoiBean();
        String transactionTag = "<Message>\n"
                + "<Header>\n"
                + "<Sender>SABB000001</Sender>\n"
                + "<AccountNumber>061000248032</AccountNumber>\n"
                + "<MessageType>SADADTRN</MessageType>\n"
                + "<FileRef>" + GetDataAndTime().replaceAll("-", "") + "00" + "</FileRef>\n"
                + "<MessageDescription>SADAD Transaction</MessageDescription>\n"
                + "<TimeStamp>" + GetDataAndTimeForBody() + "</TimeStamp>\n"
                + "</Header>";
        return transactionTag;
    }

    public String getDataAndBuildTransactionBody() {
        BankIntegrationRest appsProPaasRest = new BankIntegrationRest();
        SadadAndMoiBean sadadBean = new SadadAndMoiBean();
        String bodyTransaction;
        List<String> listOfBodyTransaction = new ArrayList<>();
        CreateB2BFile createB2BFile = new CreateB2BFile();
        JSONObject jsonObj = new JSONObject(appsProPaasRest.getAllPaymentForSadad());
        int count = Integer.parseInt(jsonObj.get("count").toString());
        JSONArray items = jsonObj.getJSONArray("items");
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject rec = items.getJSONObject(i);
                JSONArray paymentDff = rec.getJSONArray("paymentDff");
                for (int j = 0; j < paymentDff.length(); j++) {
                    JSONObject paymentDffObj = paymentDff.getJSONObject(j);
                    Long CheckId = paymentDffObj.getLong("CheckId");
                    String typeOfPayment = paymentDffObj.optString("__FLEX_Context_DisplayValue");
                    if (typeOfPayment.equalsIgnoreCase("SADAD ")) {
                        String latterReceiverStatues = paymentDffObj.optString("latterReceiverStatues");
                        if ("".equals(latterReceiverStatues)) {
                            String sadadNumber = paymentDffObj.optString("sadadNumber");
                            String receiverConfirmation = paymentDffObj.optString("receiverConfirmation");
//                            System.out.println(sadadNumber);
                            sadadBean.setBillerID(rec.optString("City"));
                         //   sadadBean.setConsumerID(rec.optString("PayeeSite"));
                            sadadBean.setReferenceNumber(rec.optString("VoucherNumber"));
                            sadadBean.setSubscriberNumber(paymentDffObj.optString("sadadNumber"));
                            bodyTransaction = buildTransaction(sadadBean);
//                            System.out.println(bodyTransaction);
                            listOfBodyTransaction.add(bodyTransaction);

                        }
                    }
                }
            }
            return "<Body>\n" + listOfBodyTransaction.toString().replaceAll(" ", "") + "</Body>";
//            System.out.println(listOfBodyTransaction);
            //    CreateAndWriteToFile(listOfBodyTransaction);

        }
        return "";
    }

    public static String buildTransaction(SadadAndMoiBean sadadBean) {
//        String transactionTag = "<Transaction>\n"
//                + "<Reference_number>" + sadadBean.getReferenceNumber() + "</Reference_number>\n"
//                + "<BillerID>" + sadadBean.getBillerID() + "</BillerID>\n"
//                + "<SubscriberNumber>" + sadadBean.getSubscriberNumber() + "</SubscriberNumber>\n"
//                + "<ConsumerID>" + sadadBean.getConsumerID() + "</ConsumerID>\n"
//                + "<Amount>10</Amount>\n"
//                + "<PayExactDue>Y</PayExactDue>\n"
//                + "</Transaction>\n";
//        return transactionTag;
        String transactionTag
                = "<Transaction>\n"
                + "<Reference_number>1</Reference_number>\n"
                + "<BillerID>027</BillerID>\n"
                + "<SubscriberNumber>4001451054</SubscriberNumber>\n"
                + "<ConsumerID>2113441233</ConsumerID>\n"
                + "<Amount>10</Amount>\n"
                + "<PayExactDue>Y</PayExactDue>\n"
                + "</Transaction>" + "\n";
        return transactionTag;
    }

    public static String buildTransactionSignature(String body) {
        String result = "";
        try {
            KeyStore clientStore = KeyStore.getInstance(keyStoreType);
            clientStore.load(new FileInputStream(keyStore),
                    keyPass.toCharArray());
            X509Certificate c
                    = (X509Certificate) clientStore.getCertificate(alias);
            byte[] dataToSign = body.getBytes("cp1256");
            Signature signature = Signature.getInstance("SHA1withRSA");
            System.out.println(clientStore.getCertificate(alias).getPublicKey());
            //   LOF.writeln("--------------------");
            //     LOF.writeln(clientStore.getCertificate(alias).getPublicKey().toString());
            //LOF.writeln();
//             LOF.writeln("+++++++++++++++++++++++++ 0001");
//             PrivateKey pk = (PrivateKey)clientStore.getKey(alias, keyPass.toCharArray());
//            LOF.writeln("+++++++++++++++++++++++++ 0000 2");
//            LOF.writeln("+++++++++++++++++++++++++ 0000 3"+pk);
            signature.initSign((PrivateKey) clientStore.getKey(alias,
                    keyPass.toCharArray()));
            signature.update(dataToSign);
            byte[] signedData = signature.sign();
            X500Name xName = X500Name.asX500Name(c.getIssuerX500Principal());
            BigInteger serial = c.getSerialNumber();
            AlgorithmId digestAlgorithmId
                    = new AlgorithmId(AlgorithmId.SHA_oid);
            AlgorithmId signAlgorithmId
                    = new AlgorithmId(AlgorithmId.RSAEncryption_oid);
            SignerInfo sInfo
                    = new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId,
                            signedData);
            ContentInfo cInfo
                    = new ContentInfo(ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString,
                            dataToSign));
            PKCS7 p7
                    = new PKCS7(new AlgorithmId[]{digestAlgorithmId}, cInfo,
                    new java.security.cert.X509Certificate[]{c},
                    new SignerInfo[]{sInfo});
            ByteArrayOutputStream bOut = new DerOutputStream();
            p7.encodeSignedData(bOut);
            byte[] encodedPKCS7 = bOut.toByteArray();
            result = Base64.getEncoder().encodeToString(encodedPKCS7);  // java 8
            // result = new String(Base64.encode(encodedPKCS7));
//            result = DatatypeConverter.printBase64Binary(encodedPKCS7);  //  java 6
        } catch (Exception e) {
//            e.printStackTrace();
            result = e.toString();
        }
        return result;
    }

    public static String GetDataAndTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        System.out.println(formatter.format(date));
        String dateTime = formatter.format(date).replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        System.out.println("R_" + dateTime);
        return dateTime;
    }

    public static String GetDataAndTimeForBody() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        String dateTime = formatter.format(date);//.replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        return dateTime;
    }

    public String encodeDecodeString(String stringToEncode) {
        // encode string using Base64 encoder
        Base64.Encoder encoder = Base64.getEncoder();
        String encoded = encoder.encodeToString(stringToEncode.getBytes());
        // System.out.println("Encoded Data: " + encoded);

        // decode the encoded data
        Base64.Decoder decoder = Base64.getDecoder();
        String decoded = new String(decoder.decode(encoded));
        // System.out.println("Decoded Data: " + decoded);
        return encoded;
    }

    public static String encode(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return base64File;
    }
    public static void main(String args[]) {
//        CreateB2BFile createB2BFile = new CreateB2BFile();
//        createB2BFile.CreateAndWriteToFile();
        
        buildTransactionSignature("test");
        
        
        
        
    }
}
