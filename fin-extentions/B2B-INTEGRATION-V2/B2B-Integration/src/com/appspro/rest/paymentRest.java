package com.appspro.rest;

import com.appspro.dao.PaymentsDAO;

import com.appspro.filter.JWTTokenNeeded;
import com.appspro.restHelper.RestHelper;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/payment")
@JWTTokenNeeded

public class paymentRest {
    @POST
    @Path("/submitRequest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitPayment(String body) {
        PaymentsDAO dao = new PaymentsDAO();
        JSONObject obj = new JSONObject(body);

        dao.insertPayment(obj);
        return Response.ok().build();

    }
    @POST
    @Path("/getPaymetById")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getPayment(String body) {
        PaymentsDAO dao = new PaymentsDAO();
        JSONArray response = new JSONArray(body);
        JSONArray serviceResponse = new JSONArray();
        
        for(int i = 0 ; i < response.length() ; i++){          
            JSONObject obj = response.getJSONObject(i);          
                serviceResponse.put( dao.getPaymentById( obj.getInt("id")));
                       
            }
        return serviceResponse.toString();

    }
    @GET
    @Path("/postPaymentDB")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response DBpostPayment() throws KeyStoreException,
                                           NoSuchAlgorithmException,
                                           CertificateException,
                                           UnrecoverableKeyException,
                                           KeyManagementException {
        PaymentsDAO dao = new PaymentsDAO();
        JSONArray arr = new JSONArray();

        arr = dao.getpostPaymentsrequests();
        System.out.println(arr);
        return Response.ok(arr.toString()).build();

    }
    @GET
       @Path("/getPaymentAttachment/{paymentid}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public Response getPaymentAttachment(@HeaderParam("jwt") String jwtToken  ,@PathParam("paymentid")
           String paymentId) {
           System.out.println("jwt = "+ jwtToken);
           JSONObject fusionResponse = new JSONObject();
           JSONArray serviceResponseArr = new JSONArray();
           try {
               fusionResponse =
                       RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=relatedInvoices&q=PaymentId=" +
                                              paymentId);
               if ("200".equals(fusionResponse.get("responseCode").toString())) {
                   JSONObject respData =
                       new JSONObject(fusionResponse.get("responseData").toString());
                   JSONArray items = respData.getJSONArray("items");
                   JSONObject paymentJsonObj = items.getJSONObject(0);
                   JSONArray relatedInvoices =
                       new JSONArray(paymentJsonObj.get("relatedInvoices").toString());
                   if (relatedInvoices.length() > 0) {
                       for (int i = 0; i < relatedInvoices.length(); i++) {
                           JSONObject relatedInvoicesJsonObj =
                               relatedInvoices.getJSONObject(i);
                           System.out.println(relatedInvoicesJsonObj.get("InvoiceId"));
                           JSONObject invoiceAttachmentFusionResponse =
                               RestHelper.callHttpGetRelatedInvoicesAtt("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/invoices/"+relatedInvoicesJsonObj.get("InvoiceId").toString()+"/child/attachments",jwtToken);
                           if ("200".equals(invoiceAttachmentFusionResponse.get("responseCode").toString())) {
                               JSONObject attRespData =
                                   new JSONObject(invoiceAttachmentFusionResponse.get("responseData").toString());
                               JSONArray attItems = attRespData.getJSONArray("items");
                               for(int j = 0 ; j < attItems.length() ; j++){
                                       JSONObject attItemsObj = attItems.getJSONObject(j);
                                       JSONObject serviceResponseObj = new JSONObject();
                                       serviceResponseObj.put("FileName", attItemsObj.get("FileName").toString());
                                       serviceResponseObj.put("FileUrl", "https://eoay-test.fa.em2.oraclecloud.com/fscmUI"+attItemsObj.get("FileUrl").toString());
                                       serviceResponseObj.put("InvoiceId" , relatedInvoicesJsonObj.get("InvoiceId").toString());
                                       serviceResponseObj.put("AttachedDocumentId", attItemsObj.get("AttachedDocumentId").toString());
                                       serviceResponseObj.put("Type", attItemsObj.get("Type").toString());
                                       serviceResponseArr.put(serviceResponseObj);                                                                  
                                   }
                           }else{
                               return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                   "    \"errorCode\": \"500\",\n" +
                                   "    \"errorMessage\": \""+invoiceAttachmentFusionResponse.get("responseData").toString()+"\"\n" +
                                   "}").build();
                               }
                       }
                       return Response.ok(serviceResponseArr.toString()).build();
                   } else {
                       return Response.ok(serviceResponseArr.toString()).build();
                   }
               } else {
                   return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                       "    \"errorCode\": \"500\",\n" +
                       "    \"errorMessage\": \""+fusionResponse.get("responseData").toString()+"\"\n" +
                       "}").build();
               }
           } catch (Exception e) {
               e.printStackTrace();
               return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                   "    \"errorCode\": \"500\",\n" +
                   "    \"errorMessage\": \""+e.getMessage()+"\"\n" +
                   "}").build();
           }
       }
       @GET
       @Path("/getPaymentAttachmentBasic/{paymentid}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public Response getPaymentAttachmentBasicAuth(@HeaderParam("jwt") String jwtToken  ,@PathParam("paymentid")
           String paymentId) {
           System.out.println("jwt = "+ jwtToken);
           JSONObject fusionResponse = new JSONObject();
           JSONArray serviceResponseArr = new JSONArray();
           try {
               fusionResponse =
                       RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=relatedInvoices&q=PaymentId=" +
                                              paymentId);
               if ("200".equals(fusionResponse.get("responseCode").toString())) {
                   JSONObject respData =
                       new JSONObject(fusionResponse.get("responseData").toString());
                   JSONArray items = respData.getJSONArray("items");
                   JSONObject paymentJsonObj = items.getJSONObject(0);
                   JSONArray relatedInvoices =
                       new JSONArray(paymentJsonObj.get("relatedInvoices").toString());
                   if (relatedInvoices.length() > 0) {
                       for (int i = 0; i < relatedInvoices.length(); i++) {
                           JSONObject relatedInvoicesJsonObj =
                               relatedInvoices.getJSONObject(i);
                           System.out.println(relatedInvoicesJsonObj.get("InvoiceId"));
                           JSONObject invoiceAttachmentFusionResponse =
                               RestHelper.callHttpGetAttBasic("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/invoices/"+relatedInvoicesJsonObj.get("InvoiceId").toString()+"/child/attachments",jwtToken);
                           if ("200".equals(invoiceAttachmentFusionResponse.get("responseCode").toString())) {
                               JSONObject attRespData =
                                   new JSONObject(invoiceAttachmentFusionResponse.get("responseData").toString());
                               JSONArray attItems = attRespData.getJSONArray("items");
                               for(int j = 0 ; j < attItems.length() ; j++){
                                       JSONObject attItemsObj = attItems.getJSONObject(j);
                                       JSONObject serviceResponseObj = new JSONObject();
                                       serviceResponseObj.put("FileName", attItemsObj.get("FileName").toString());
                                       serviceResponseObj.put("FileUrl", "https://eoay-test.fa.em2.oraclecloud.com/fscmUI"+attItemsObj.get("FileUrl").toString());
                                       serviceResponseObj.put("InvoiceId" , relatedInvoicesJsonObj.get("InvoiceId").toString());
                                       serviceResponseObj.put("AttachedDocumentId", attItemsObj.get("AttachedDocumentId").toString());
                                       serviceResponseObj.put("Type", attItemsObj.get("Type").toString());
                                       serviceResponseArr.put(serviceResponseObj);                                                                  
                                   }
                           }else{
                               return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                   "    \"errorCode\": \"500\",\n" +
                                   "    \"errorMessage\": \""+invoiceAttachmentFusionResponse.get("responseData").toString()+"\"\n" +
                                   "}").build();
                               }
                       }
                       return Response.ok(serviceResponseArr.toString()).build();
                   } else {
                       return Response.ok(serviceResponseArr.toString()).build();
                   }
               } else {
                   return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                       "    \"errorCode\": \"500\",\n" +
                       "    \"errorMessage\": \""+fusionResponse.get("responseData").toString()+"\"\n" +
                       "}").build();
               }
           } catch (Exception e) {
               e.printStackTrace();
               return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                   "    \"errorCode\": \"500\",\n" +
                   "    \"errorMessage\": \""+e.getMessage()+"\"\n" +
                   "}").build();
           }
       }
    @POST
    @Path("/updatePaymentApprovalStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
       public Response updatePaymentApprovalStatus (String body){
               PaymentsDAO dao = new PaymentsDAO();
               JSONObject obj = new JSONObject(body);
           dao.updateApprovalRequestStatus(obj.get("id").toString(), obj.get("status").toString());
           
               return Response.ok("{\n" + 
               "    \"status\" : \"success\"\n" + 
               "}").build();
           }
    
 
    @GET
       @Path("/getSupplierDetailsByNumber/{supplierNumber}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public Response getSupplierDetailsByNumber(@PathParam("supplierNumber")
           String supplierNumber) {
               try {
                   return Response.ok(RestHelper.callHttpSupplierIBAN("https://eoay-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService",supplierNumber).toString()).build();
    //                System.out.println(RestHelper.callHttpSupplierIBAN("https://eoay-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService",supplierNumber));
               } catch (Exception e) {
                   return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                       "    \"errorCode\": \"500\",\n" +
                       "    \"errorMessage\": \""+e.getMessage()+"\"\n" +
                       "}").build();
               } 
           }
    
    @GET
       @Path("/getEmployeeBankDetails/{externalBankAccountId}")
       @Consumes(MediaType.APPLICATION_JSON)
       @Produces(MediaType.APPLICATION_JSON)
       public Response getEmployeeBankDetails(@PathParam("externalBankAccountId")
           String externalBankAccountId) {
           JSONObject serviceResponseObj = new JSONObject();
           try {
               JSONObject extBankAccResponseData =
                   RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/externalBankAccounts/" +
                                          externalBankAccountId +
                                          "?onlyData=true&fields=IBAN,BankIdentifier");
               if ("200".equals(extBankAccResponseData.get("responseCode").toString())) {
                   JSONObject externalBankAccObj =
                       new JSONObject(extBankAccResponseData.get("responseData").toString());
                   serviceResponseObj.put("IBAN",
                                          externalBankAccObj.get("IBAN").toString());
                   JSONObject cashBanksResponseData =
                       RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/cashBanks?q=BankPartyId=" +
                                              externalBankAccObj.get("BankIdentifier").toString() +
                                              "&onlyData=true&fields=BankName,BankNameAlt");
                   if ("200".equals(cashBanksResponseData.get("responseCode").toString())) {
                       JSONObject cashBanksObj =
                           new JSONObject(cashBanksResponseData.get("responseData").toString());
                       JSONObject cashBanksItemsObj =
                           new JSONObject(cashBanksResponseData.get("responseData").toString());
                       JSONArray cashBanksItemsArr =
                           new JSONArray(cashBanksItemsObj.get("items").toString());
                       JSONObject cashBankItemObj =
                           cashBanksItemsArr.getJSONObject(0);
                       serviceResponseObj.put("BankName",
                                              cashBankItemObj.get("BankName").toString());
                       serviceResponseObj.put("BankCode",
                                              cashBankItemObj.get("BankNameAlt").toString());
                       return Response.ok(serviceResponseObj.toString()).build();
                   } else {
                       return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                               "    \"errorCode\": \"500\",\n" +
                               "    \"errorMessage\": \"" +
                               cashBanksResponseData.get("responseData") +
                               "\"\n" +
                               "}").build();
                   }
               } else {
                   return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                           "    \"errorCode\": \"500\",\n" +
                           "    \"errorMessage\": \"" +
                           extBankAccResponseData.get("responseData").toString() +
                           "\"\n" +
                           "}").build();
                   //return error
               }
           } catch (Exception e) {
               return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                       "    \"errorCode\": \"500\",\n" +
                       "    \"errorMessage\": \"" + e.getMessage() + "\"\n" +
                       "}").build();
           }
       }
    
}
