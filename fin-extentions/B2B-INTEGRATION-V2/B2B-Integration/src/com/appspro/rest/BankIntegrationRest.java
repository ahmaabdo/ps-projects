/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.rest;

import com.appspro.B2B.AccountBalanceProcess;
import com.appspro.B2B.CancelPaymentTransactionProcess;
import com.appspro.B2B.MT101FusionPaymentProcess;
import com.appspro.B2B.MT940FusionPaymentProcess;
import com.appspro.B2B.MoiFusionInquiryProcess;
import com.appspro.B2B.MoiFusionPaymentProcess;
import com.appspro.B2B.MoiRefundFusionPaymentProcess;
import com.appspro.B2B.PayrollFusionPaymentProcess;
import com.appspro.B2B.PostPaymentTransactionProcess;
import com.appspro.B2B.SadadFusionInquiryProcess;
import com.appspro.B2B.SadadFusionPaymentProcess;
import com.appspro.bean.MT940TransactionResponsseBean;
import com.appspro.bean.MT940responseBean;
import com.appspro.bean.PaymentApprovalBean;
import com.appspro.bean.PaymentStatusBean;
import com.appspro.bean.SadadAndMoiBean;
import com.appspro.dao.LoggerDAO;
import com.appspro.dao.PaymentsDAO;
import com.appspro.filter.JWTTokenNeeded;
import com.appspro.restHelper.RestHelper;

import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.util.ArrayList;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author 
 */
@Path("/paymentprocess")
@JWTTokenNeeded
public class BankIntegrationRest {
    
    private final String paymentUrl ="https://eoay-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff,relatedInvoices&onlyData=true&q=PaymentId=";
       @GET
          @Path("/GetAllPayment")
          @Consumes(MediaType.APPLICATION_JSON)
          @Produces(MediaType.APPLICATION_JSON)
          public Response getAllPaymentForSadad() {
              try{
                     PaymentsDAO log = new PaymentsDAO();
                     List<PaymentStatusBean> al = log.getvalidPayments();
                          List<PaymentApprovalBean> paymentApprovalList =   log.getApprovalPayments();
                  System.out.println(paymentApprovalList);
                     JSONArray respArr = new JSONArray ();
                     JSONObject json =
                         RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?q=PaymentStatus=Negotiable&limit=500&fields=CreatedBy,PaymentId,PaymentNumber,PaymentAmount,PaymentDate,PaymentDescription,PaymentStatus,PaymentType,PaymentCurrency,SupplierNumber,Payee,PaymentMethod,RemitToAccountNumber,ExternalBankAccountId&onlyData=true");
                     String responseCode = json.get("responseCode").toString();
                     if ("200".equals(responseCode)) {
                      //   System.out.println(json.get("responseData"));
                         JSONObject items =
                             new JSONObject(json.get("responseData").toString());
                         JSONArray itemsArray = items.getJSONArray("items");
                         System.out.println("================================");
                         for (int i = 0; i < itemsArray.length(); i++) {
                             JSONObject paymentJsonObj = itemsArray.getJSONObject(i);
                          //   System.out.println(paymentJsonObj.get("PaymentId").toString());
                             if(al.contains(new PaymentStatusBean(paymentJsonObj.get("PaymentId").toString()))){
                               //  System.out.println("index = "+ al.get(al.indexOf(new PaymentStatusBean(paymentJsonObj.get("PaymentId").toString()))).getPaymentStatus());
                                 paymentJsonObj.put("TransactionBankStatus", al.get(al.indexOf(new PaymentStatusBean(paymentJsonObj.get("PaymentId").toString()))).getPaymentStatus());
                             }else{
                                     paymentJsonObj.put("TransactionBankStatus", "UnPaid");
                                 }
                             
                             if(paymentApprovalList.contains(new PaymentApprovalBean(paymentJsonObj.get("PaymentId").toString()))){
                               //  System.out.println("index = "+ al.get(al.indexOf(new PaymentStatusBean(paymentJsonObj.get("PaymentId").toString()))).getPaymentStatus());
                                 paymentJsonObj.put("ApprovalStatus", paymentApprovalList.get(paymentApprovalList.indexOf(new PaymentApprovalBean(paymentJsonObj.get("PaymentId").toString()))).getApprovalStatus());
                                 paymentJsonObj.put("RequestId", paymentApprovalList.get(paymentApprovalList.indexOf(new PaymentApprovalBean(paymentJsonObj.get("PaymentId").toString()))).getId());
                             }else{
                                     paymentJsonObj.put("ApprovalStatus", "");
                                     paymentJsonObj.put("RequestId", "");
                                 }
                             
                             
                             respArr.put(paymentJsonObj);
                         }
                         return Response.ok().entity(respArr.toString()).build();
                     }
                     return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                         "    \"errorCode\": \"500\",\n" +
                         "    \"errorMessage\": "+json.get("responseData").toString()+"\n" +
                         "}").build();
                     } catch(Exception e){
                             return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                 "    \"errorCode\": \"500\",\n" +
                                 "    \"errorMessage\": "+e.getMessage()+"\n" +
                                 "}").build();
                         }
          }

    @GET
    @Path("/testConnect")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response testConnection() throws IOException {
        
        PaymentsDAO log = new PaymentsDAO();
           List<PaymentStatusBean> al =   log.getvalidPayments();        
        return Response.ok().entity(al.toString()).build();

    }

    @POST
    @Path("/transactiontype/{transactionType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response paymentConnection(@PathParam("transactionType")
        String transactionType, String body) throws IOException,
                                                    KeyStoreException,
                                                    NoSuchAlgorithmException,
                                                    CertificateException,
                                                    UnrecoverableKeyException,
                                                    KeyManagementException {

           
        LoggerDAO log = new LoggerDAO();
        JSONObject jsonBody = new JSONObject(body);
        JSONObject bankResponse;


        if (jsonBody.get("username").toString() != null &&
            jsonBody.get("paymentId").toString() != null &&
            transactionType != null) {

            String randomId = RestHelper.generateRandomNymber();



            log.createLogTransaction(randomId,
                                     jsonBody.get("paymentId").toString(),
                                     jsonBody.get("username").toString(),
                                     log.getLastLogin(jsonBody.get("username").toString()),
                                     jsonBody.get("transactionType").toString());
            switch (transactionType) {
                       case "mt101":
                           MT101FusionPaymentProcess mT101FusionPaymentProcess =
                               new MT101FusionPaymentProcess(this.paymentUrl +
                                                             jsonBody.get("paymentId") , randomId,
                                                            jsonBody.get("paymentDate").toString(),
                                                             jsonBody.get("paymentCode").toString(),
                                                             jsonBody.get("toAccount").toString(),
                                                            jsonBody.get("bankCode").toString(),
                                                             jsonBody.get("bankName").toString());
                           bankResponse =
                                   mT101FusionPaymentProcess.createPaymentTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/paymentMT101",
                                                                                             randomId,
                                                                                               jsonBody.get("paymentId").toString(),jsonBody.get("toAccount").toString());
                           break;
                       case "mt940":
                           MT940FusionPaymentProcess mT940FusionPaymentProcess =
                               new MT940FusionPaymentProcess(jsonBody.get("date").toString(),
                                                             jsonBody.get("accountNumber").toString());
                           bankResponse =
                                   mT940FusionPaymentProcess.getMt940TransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/mt940",
                                                                                        randomId);
                
                
                           break;
                       case "paymenttransaction":
                           PostPaymentTransactionProcess postPaymentTransactionProcess =
                               new PostPaymentTransactionProcess(jsonBody.get("paymentId").toString(),
                                                                 jsonBody.get("fileReference").toString(),
                                                                 jsonBody.get("transactionReference").toString());
                           bankResponse =
                                   postPaymentTransactionProcess.getPostPaymentTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/paymentStatus",
                                                                                                  randomId);
                           break;
                       case "cancelpayment":
                           CancelPaymentTransactionProcess cancelPaymentTransactionProcess =
                               new CancelPaymentTransactionProcess(jsonBody);
                           bankResponse =
                                   cancelPaymentTransactionProcess.cancelPaymentTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/stopPayment",
                                                                                        randomId,
                                                                                        jsonBody.get("fileReference").toString(),
                                                                                        jsonBody.get("transactionReference").toString());
                break;
                       case "sadadpayment":
                           SadadFusionPaymentProcess sadadFusionPaymentProcess =
                               new SadadFusionPaymentProcess(this.paymentUrl +
                                                             jsonBody.get("paymentId"));
                           bankResponse =
                                   sadadFusionPaymentProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/sadadTransaction",
                                                                                      randomId);
                           break;
                       case "sadadinquiry":
                           SadadFusionInquiryProcess sadadFusionInquiryProcess =
                               new SadadFusionInquiryProcess(this.paymentUrl +
                                                             jsonBody.get("paymentId"));
                           bankResponse =
                                   sadadFusionInquiryProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/sadadEnquiry",
                                                                                      randomId);
                           break;
                       case "payrollmessage":
                           PayrollFusionPaymentProcess payrollFusionPaymentProcess =
                               new PayrollFusionPaymentProcess(this.paymentUrl +
                                                               jsonBody.get("paymentId"));
                           bankResponse =
                                   payrollFusionPaymentProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/payrollMessage",
                                                                                        randomId);
                           break;
                       case "moirefund":
                           MoiRefundFusionPaymentProcess moiRefundFusionPaymentProcess =
                               new MoiRefundFusionPaymentProcess(this.paymentUrl +
                                                                 jsonBody.get("paymentId"));
                           bankResponse =
                                   moiRefundFusionPaymentProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/moiRefund",
                                                                                          randomId);
                           break;
                       case "moipayment":
                           MoiFusionPaymentProcess moiFusionPaymentProcess =
                               new MoiFusionPaymentProcess(this.paymentUrl +
                                                           jsonBody.get("paymentId"));
                           bankResponse =
                                   moiFusionPaymentProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/sadadMOIPayment",
                                                                                    randomId);
                           break;
                       case "moiinquiry":
                           MoiFusionInquiryProcess moiFusionInquiryProcess =
                               new MoiFusionInquiryProcess(this.paymentUrl +
                                                           jsonBody.get("paymentId"));
                           bankResponse =
                                   moiFusionInquiryProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/sadadMOIEnquiry",
                                                                                    randomId);
                           break;
                       case "accountbalance":
                           AccountBalanceProcess accountBalanceProcess =
                               new AccountBalanceProcess(this.paymentUrl +
                                                         jsonBody.get("paymentId"));
                           bankResponse =
                                   accountBalanceProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/balanceEnquiry",
                                                                                  randomId);
                           break;
                       default:
                           log.createLogEvent(randomId, "Create Transaction",
                                              transactionType +
                                              " is invalid transaction type", "failed");
                           return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                   "    \"statue code\": \"500\",\n" +
                                   "    \"message\": \"" + transactionType +
                                   " is invalid transaction type\"\n" +
                                   "}").build();
                       }

            

//            MT101FusionPaymentProcess mT101FusionPaymentProcess =
//                new MT101FusionPaymentProcess(this.paymentUrl +
//                                              jsonBody.get("paymentId"));
//
//
//            bankResponse =
//                    mT101FusionPaymentProcess.createTransactionProcess("https://www.b2bdev.sabb.com/B2BService/epayments/paymentMT101",
//                                                                       randomId);


            return Response.ok().entity(bankResponse.toString()).build();
        }


        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                "    \"statue code\": \"500\",\n" +
                "    \"message\": \"invalid payload\"\n" +
                "}").build();


    }
         @POST
          @Path("/GetPayment/{paymentId}")
          @Consumes(MediaType.APPLICATION_JSON)
          @Produces(MediaType.APPLICATION_JSON)
          public Response getPayment(@PathParam("paymentId") String paymentId) {
              try{
                  
              String   serverUrl ="https://eoay-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?fields=CreatedBy,PaymentId,PaymentNumber,PaymentAmount,PaymentDate,PaymentDescription,PaymentStatus,PaymentType,PaymentCurrency,SupplierNumber,PaymentMethod&onlyData=true&q=PaymentId="+paymentId;
                  System.out.println(serverUrl);
                     JSONObject json =
                         RestHelper.callHttpGet(serverUrl);
                     String responseCode = json.get("responseCode").toString();
                     if ("200".equals(responseCode)) {
                         System.out.println(json.get("responseData"));
                         JSONObject items =
                             new JSONObject(json.get("responseData").toString());
                         JSONArray itemsArray = items.getJSONArray("items");
                         System.out.println("================================");
                         
                         return Response.ok().entity(itemsArray.toString()).build();
                     }
                     return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                         "    \"errorCode\": \"500\",\n" +
                         "    \"errorMessage\": "+json.get("responseData").toString()+"\n" +
                         "}").build();
                     } catch(Exception e){
                             return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                 "    \"errorCode\": \"500\",\n" +
                                 "    \"errorMessage\": "+e.getMessage()+"\n" +
                                 "}").build();
                         }
          }
    @GET
        @Path("/getPaymentAttachment/{paymentid}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response getPaymentAttachment(@PathParam("paymentid")
            String paymentId) {
            JSONObject fusionResponse = new JSONObject();
            JSONArray serviceResponseArr = new JSONArray();
            try {
                fusionResponse =
                        RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=relatedInvoices&q=PaymentId=" +
                                               paymentId);
                if ("200".equals(fusionResponse.get("responseCode").toString())) {
                    JSONObject respData =
                        new JSONObject(fusionResponse.get("responseData").toString());
                    JSONArray items = respData.getJSONArray("items");
                    JSONObject paymentJsonObj = items.getJSONObject(0);
                    JSONArray relatedInvoices =
                        new JSONArray(paymentJsonObj.get("relatedInvoices").toString());
                    if (relatedInvoices.length() > 0) {
                        for (int i = 0; i < relatedInvoices.length(); i++) {
                            JSONObject relatedInvoicesJsonObj =
                                relatedInvoices.getJSONObject(i);
                            System.out.println(relatedInvoicesJsonObj.get("InvoiceId"));
                            JSONObject invoiceAttachmentFusionResponse =
                                RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/invoices/"+relatedInvoicesJsonObj.get("InvoiceId").toString()+"/child/attachments");
                            if ("200".equals(invoiceAttachmentFusionResponse.get("responseCode").toString())) {
                                JSONObject attRespData =
                                    new JSONObject(invoiceAttachmentFusionResponse.get("responseData").toString());
                                JSONArray attItems = attRespData.getJSONArray("items");
                                for(int j = 0 ; j < attItems.length() ; j++){
                                        JSONObject attItemsObj = attItems.getJSONObject(j);
                                        JSONObject serviceResponseObj = new JSONObject();
                                        serviceResponseObj.put("FileName", attItemsObj.get("FileName").toString());
                                        serviceResponseObj.put("FileUrl", "https://eoay-test.fa.em2.oraclecloud.com/fscmUI"+attItemsObj.get("FileUrl").toString());
                                        serviceResponseArr.put(serviceResponseObj);                                                                  
                                    }
                            }else{
                                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                                    "    \"errorCode\": \"500\",\n" +
                                    "    \"errorMessage\": \""+invoiceAttachmentFusionResponse.get("responseData").toString()+"\"\n" +
                                    "}").build();
                                }
                        }
                        return Response.ok(serviceResponseArr.toString()).build();
                    } else {
                        return Response.ok(serviceResponseArr.toString()).build();
                    }
                } else {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                        "    \"errorCode\": \"500\",\n" +
                        "    \"errorMessage\": \""+fusionResponse.get("responseData").toString()+"\"\n" +
                        "}").build();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                    "    \"errorCode\": \"500\",\n" +
                    "    \"errorMessage\": \""+e.getMessage()+"\"\n" +
                    "}").build();
            }
        }

}
