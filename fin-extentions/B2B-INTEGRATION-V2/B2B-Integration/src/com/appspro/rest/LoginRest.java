package com.appspro.rest;

import com.appspro.dao.LoggerDAO;
import com.appspro.restHelper.RestHelper;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.util.Base64;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONObject;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path("/login")
public class LoginRest {
    @POST
    @Path("/userLogin")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateLogin(String body) throws KeyStoreException,
                                                      NoSuchAlgorithmException,
                                                      CertificateException,
                                                      UnrecoverableKeyException,
                                                      KeyManagementException {
        JSONObject resultResponse = new JSONObject();
        JSONObject loginDetails = new JSONObject(body);
        LoggerDAO dao = new LoggerDAO ();
        
        String userName =
            RestHelper.decodeBase64(loginDetails.getString("userName"));
        String password =
            RestHelper.decodeBase64(loginDetails.getString("password"));

        String result = "";
        JSONObject json =
            RestHelper.callHttpPostLogin("https://eoay-test.fa.em2.oraclecloud.com/xmlpserver/services/PublicReportService",
                                         userName, password);
        if ("200".equals(json.get("responseCode").toString())) {
            Document doc =
                RestHelper.convertStringToDocument(json.get("responseData").toString());

            NodeList tasksList =
                doc.getElementsByTagName("validateLoginResponse");
            Element element = (Element)tasksList.item(0);


            result =
                    element.getElementsByTagName("validateLoginReturn").item(0).getTextContent();
            
            if (result.equalsIgnoreCase("true")) {
                dao.loginEventLogger(userName, "userIp", "success");
                
                String token = issueToken(RestHelper.decodeBase64(loginDetails.getString("userName")));
                
                resultResponse.put("JWT", token);
            }
            if (result.equalsIgnoreCase("false")) {
                dao.loginEventLogger(userName, "userIp", "failed");
            }
            
            resultResponse.put("response", result);
            return Response.ok( resultResponse.toString()).build();
        } else {
            
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n" +
                "    \"errorCode\": \"500\",\n" +
                "    \"errorMessage\": \"invalid payload\"\n" +
                "}").build();

        }

    }
    
    @POST
        @Consumes("application/json")
        @Path("/generatetoken")
        public Response generateToken(String body, @Context
            HttpServletRequest request, @Context
            HttpServletResponse response, @HeaderParam("Authorization")  String authString) {
            JSONObject json = new JSONObject();
            try {
                JSONObject jwtJson = new JSONObject(body);
                String saasJWT = jwtJson.getString("jwt");
                String[] split_string = saasJWT.split("\\.");
                String base64EncodedBody = split_string[1];
                String tokenBody = RestHelper.decodeBase64(base64EncodedBody);
                System.out.println("JWT Body : " + tokenBody);
                JSONObject paasTokernJSON = new JSONObject(tokenBody);
                String username = paasTokernJSON.getString("sub");
                String token = issueToken(username);
                json.put("jwt", token);
                return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
            } catch (Exception e) {
                e.printStackTrace();
                return Response.status(500).entity(e.getMessage()).build();
            }
        }
    
    private static String issueToken(String username) throws NoSuchAlgorithmException {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MINUTE, 30);
            String jwtToken = Jwts.builder().setSubject(username).setIssuer("Abdullah Oweidi").setIssuedAt(new Date()).setExpiration(calendar.getTime()).signWith(SignatureAlgorithm.HS512,"P@$$w0rdOW".getBytes()).compact();
            System.out.println("#### generating token for a key : " + jwtToken);
            
            System.out.println( "=================");
            System.out.println( jwtToken);
            return jwtToken;
        }
    
    
    public static void main(String [] args){


        try {
            
            System.out.println(issueToken("Loay"));
            ;
        } catch (NoSuchAlgorithmException e) {
        }
    }
}
