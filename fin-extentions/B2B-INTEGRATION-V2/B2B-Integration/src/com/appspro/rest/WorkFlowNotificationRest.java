package com.appspro.rest;



import com.appspro.bean.ApprovalListBean;

import com.appspro.bean.WorkFlowNotificationBean;
import com.appspro.dao.ApprovalListDAO;

import com.appspro.dao.WorkFlowNotificationDao;

import com.appspro.filter.JWTTokenNeeded;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/workFlowNotification")
@JWTTokenNeeded
public class WorkFlowNotificationRest {
    WorkFlowNotificationDao service = new WorkFlowNotificationDao();

    @POST
    @Path("/insert")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertOrUpdateExit(String body) {
        WorkFlowNotificationBean list = new WorkFlowNotificationBean();
        try {
            ObjectMapper mapper = new ObjectMapper();
            WorkFlowNotificationBean bean =
                mapper.readValue(body, WorkFlowNotificationBean.class);
            list = service.insertIntoWorkflow(bean);
            return new JSONObject(list).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
    @POST
    @Path("/allNotifications")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllNotificationsDetails(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);
        String MANAGER_ID = jsonObj.getString("MANAGER_ID");
        String emp = jsonObj.getString("emp");    
        String managerOfManager = jsonObj.getString("MANAGER_OF_MANAGER");
      
        WorkFlowNotificationDao det = new WorkFlowNotificationDao();
        ArrayList<WorkFlowNotificationBean> list = new ArrayList<WorkFlowNotificationBean>();

        list = det.getAllNotification(MANAGER_ID, emp, managerOfManager);

        return  new  JSONArray(list).toString();
    }
    @POST
    @Path("/updateNotifi")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String UpdateExist(String body) {
        WorkFlowNotificationBean list = new WorkFlowNotificationBean();
        try {
            JSONObject obj = new JSONObject(body);
            String TRS_ID = obj.has("requestId")?obj.getString("requestId"):null;
             service.updatelocal(TRS_ID);
         
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject(list).toString();
    }
   
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String worwflowApproval(String bean, @Context HttpServletRequest request, @Context HttpServletResponse response, @HeaderParam("Authorization") String authString) {
        JSONObject jsonObj = new JSONObject(bean);

        WorkFlowNotificationDao det = new WorkFlowNotificationDao();

        det.workflowAction(bean);
        jsonObj.put("STATUS", "Success");
        return jsonObj.toString();

    }
}
