package com.appspro.dao;

import com.appspro.db.AppsproConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.json.JSONObject;

public class LoggerDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;

    public void testConnection() {


        try {

            connection = getConnection();
            String query = "SELECT * FROM EVENT_LOG_DETAILS WHERE TRANSACTION_ID ='R_2020062511343957709'";
            
//            String query = "SELECT * FROM EVENT_LOGGER WHERE ID ='R_2020062511163231421'";

            ps = connection.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                
//                System.out.print(rs.getString("ID"));

                System.out.print(rs.getString("EVENT_MESSAGE"));
                
                System.out.print(" , ");
                System.out.print(rs.getString("EVENT_NAME"));
                System.out.print(" , ");
                System.out.print(rs.getString("EVENT_DATE_TIME"));
                System.out.print(" , ");
                
                System.out.print(rs.getString("EVENT_REQUEST"));
                System.out.print(" , ");
                System.out.print(rs.getString("EVENT_RESPONSE"));
                System.out.print(" , ");
                System.out.println(rs.getString("STATUS"));
                System.out.println("=============================================");


            }

        } catch (SQLException e) {

            String message = e.getMessage();
            System.out.println(message);


        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    
    public void createLogTransaction(String id , String paymentId , String username, int loginId, String transactionType){
            try {

                connection = getConnection();
                String query = "INSERT INTO EVENT_LOGGER (ID , PAYMENT_ID , USERNAME,LOGIN_ID,TRANSACTION_TYPE) VALUES ( ?, ? ,? ,?,? )";

                ps = connection.prepareStatement(query);
                
                ps.setString(1, id);
                ps.setString(2, paymentId);
                ps.setString(3, username);
                ps.setString(4, String.valueOf(loginId));
                ps.setString(5, transactionType);

                ps.executeUpdate();

            } catch (SQLException e) {

                String message = e.getMessage();
                System.out.println(message);


            } finally {
                closeResources(connection, ps, rs);
            }
        
        }


    public void createLogEvent(String transactionId, String eventName,
                               String eventMessage) {
        try {

            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            connection = getConnection();
            String query =
                "INSERT INTO EVENT_LOG_DETAILS (TRANSACTION_ID , EVENT_DATE_TIME , EVENT_NAME , EVENT_MESSAGE) VALUES ( ?, ? ,? ,?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, transactionId);
            ps.setString(2, formatter.format(new Date()).toString());
            ps.setString(3, eventName);
            ps.setString(4, eventMessage);

            ps.executeUpdate();

        } catch (SQLException e) {

            String message = e.getMessage();
            System.out.println(message);


        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    
    public void createLogEvent(String transactionId, String eventName,
                               String eventMessage , String status) {
        try {

            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            connection = getConnection();
            String query =
                "INSERT INTO EVENT_LOG_DETAILS (TRANSACTION_ID , EVENT_DATE_TIME , EVENT_NAME , EVENT_MESSAGE , STATUS) VALUES ( ?, ? ,? ,? , ?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, transactionId);
            ps.setString(2, formatter.format(new Date()).toString());
            ps.setString(3, eventName);
            ps.setString(4, eventMessage);
            ps.setString(5, status);

            ps.executeUpdate();

        } catch (SQLException e) {

            String message = e.getMessage();
            System.out.println(message);


        } finally {
            closeResources(connection, ps, rs);
        }

    }
    public void createLogEventlogger(String transactionId, String eventName,
                               String eventMessage , String status,
                               String eventRequest) {
        try {

            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            connection = getConnection();
            String query =
                "INSERT INTO EVENT_LOG_DETAILS (TRANSACTION_ID , EVENT_DATE_TIME , EVENT_NAME , EVENT_MESSAGE , STATUS,EVENT_REQUEST) VALUES ( ?, ? ,? ,? , ? , ?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, transactionId);
            ps.setString(2, formatter.format(new Date()).toString());
            ps.setString(3, eventName);
            ps.setString(4, eventMessage);
            ps.setString(5, status);
            ps.setString(6, eventRequest);

            ps.executeUpdate();

        } catch (SQLException e) {

            String message = e.getMessage();
            System.out.println(message);


        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    
    public void createLogEvent(String transactionId, String eventName,
                               String eventMessage, String eventResponse , String statusCode , String status) {
        try {

            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            connection = getConnection();
            String query =
                "INSERT INTO EVENT_LOG_DETAILS (TRANSACTION_ID , EVENT_DATE_TIME , EVENT_NAME , EVENT_MESSAGE , EVENT_RESPONSE , STATUS_CODE , STATUS) VALUES ( ?, ? ,? ,?,? , ? , ?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, transactionId);
            ps.setString(2, formatter.format(new Date()).toString());
            ps.setString(3, eventName);
            ps.setString(4, eventMessage);
            ps.setString(5, eventResponse);
            ps.setString(6, statusCode);
            ps.setString(7, status);

            ps.executeUpdate();

        } catch (SQLException e) {

            String message = e.getMessage();
            System.out.println(message);


        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    public void createLogEvent(String transactionId, String eventName,
                               String eventMessage, String eventResponse , String statusCode , String status , String request) {
        try {

            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            connection = getConnection();
            String query =
                "INSERT INTO EVENT_LOG_DETAILS (TRANSACTION_ID , EVENT_DATE_TIME , EVENT_NAME , EVENT_MESSAGE , EVENT_RESPONSE , STATUS_CODE , STATUS , EVENT_REQUEST) VALUES ( ?, ? ,? ,?,? , ? , ? , ?)";

            ps = connection.prepareStatement(query);

            ps.setString(1, transactionId);
            ps.setString(2, formatter.format(new Date()).toString());
            ps.setString(3, eventName);
            ps.setString(4, eventMessage);
            ps.setString(5, eventResponse);
            ps.setString(6, statusCode);
            ps.setString(7, status);
            ps.setString(8, request);

            ps.executeUpdate();

        } catch (SQLException e) {

            String message = e.getMessage();
            System.out.println(message);


        } finally {
            closeResources(connection, ps, rs);
        }

    }
    
    public void loginEventLogger(String userName ,String ip ,String Status) {
        
        try {

            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            connection = getConnection();
            String query ="INSERT INTO EVENT_LOGIN_LOGGER(USERNAME,DATE_TIME,IP,STATUS) VALUES (?,?,?,?)";
            ps = connection.prepareStatement(query);

            ps.setString(1, userName);
            ps.setString(2, formatter.format(new Date()).toString());
            ps.setString(3, ip);
            ps.setString(4, Status);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
            String message = e.getMessage();
            System.out.println(message);

        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public int getLastLogin(String userName) {

        int lastID = 0;
        try {
            connection = getConnection();
            String query =
                "select  MAX(ID) from EVENT_LOGIN_LOGGER where USERNAME = ? and status = 'success'";
            ps = connection.prepareStatement(query);
            ps.setString(1, userName);
            rs = ps.executeQuery();

            while (rs.next()) {
                lastID = rs.getInt("MAX(ID)");
                System.out.println(lastID);
            }
            return lastID;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            closeResources(connection, ps, rs);
        }

    }

    public JSONObject getUserCredentials() {
        JSONObject credentials = new JSONObject();
        try {
            connection = getConnection();
            String query = "select * from USERS";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                credentials.put("username", rs.getString("USERNAME"));
                credentials.put("password", rs.getString("PASSWORD"));
            }
            return credentials;
        } catch (SQLException e) {
            e.printStackTrace();
            return new JSONObject("Error"+e);
        }


    }
    public void updateLogTransaction(String id ,String fileReferencer, String transactionReferencer , String valueDate){
               try {
                   connection = getConnection();
    //                String query = "INSERT INTO EVENT_LOGGER (ID , PAYMENT_ID , USERNAME,LOGIN_ID) VALUES ( ?, ? ,? ,? )";
                   String query
                       = "UPDATE EVENT_LOGGER \n"
                       + "set FILE_REFERENCE=? , TRANSACTION_REFERENCE = ? , VALUE_DATE=? \n"
                       + "WHERE ID = ? ";
                   ps = connection.prepareStatement(query);
                   ps.setString(1, fileReferencer);
                   ps.setString(2, transactionReferencer);
                   ps.setString(3, valueDate);
                   ps.setString(4, id);
                   ps.executeUpdate();
               } catch (SQLException e) {
                   String message = e.getMessage();
                   System.out.println(message);
               } finally {
                   closeResources(connection, ps, rs);
               }
           }
    public void updatecancelTransaction(String fileReferencer, String transactionReferencer){
               try {
                   connection = getConnection();
    //                String query = "INSERT INTO EVENT_LOGGER (ID , PAYMENT_ID , USERNAME,LOGIN_ID) VALUES ( ?, ? ,? ,? )";
                   String query
                       = " update event_logger set TRANSACTION_STATUS = 'Canceled' where transaction_reference = ? and FILE_REFERENCE=? and transaction_type = 'CANCELPAYMENT'\n";
                   ps = connection.prepareStatement(query);
                   ps.setString(1, transactionReferencer);
                   ps.setString(2, fileReferencer);
 
                   ps.executeUpdate();
               } catch (SQLException e) {
                   String message = e.getMessage();
                   System.out.println(message);
               } finally {
                   closeResources(connection, ps, rs);
               }
           }
    
    public void updateDffLogTransaction(String id ,String transactionStatus ){
               try {
                   connection = getConnection();

                   String query   = "UPDATE event_logger SET TRANSACTION_STATUS =? where ID =?";
                   ps = connection.prepareStatement(query);
                   ps.setString(1, transactionStatus);
                   ps.setString(2, id);
                   ps.executeUpdate();
               } catch (Exception e) {
                   e.printStackTrace();
                   String message = e.getMessage();
                   System.out.println(message);
               } finally {
                   closeResources(connection, ps, rs);
               }
           }

}

