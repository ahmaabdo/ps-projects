package com.appspro.dao;


import com.appspro.restHelper.RestHelper;

import com.appspro.db.AppsproConnection;

import com.appspro.bean.ApprovalListBean;
import com.appspro.bean.WorkFlowNotificationBean;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;

import org.json.JSONObject;

public class WorkFlowNotificationDao extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    RestHelper rh = new RestHelper();
    public WorkFlowNotificationBean insertIntoWorkflow(WorkFlowNotificationBean bean) {
        try {         
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    " insert into XX_WORKFLOW_NOTIFICATION (MSG_TITLE,MSG_BODY,CREATION_DATE,RECEIVER_TYPE,RECEIVER_ID,RESPONSE_PERSON_ID,TYPE,RESPONSE_DATE,REQUEST_ID,STATUS,SELF_TYPE,PERSON_NAME) " +
                    "VALUES(?,?,SYSDATE,?,?,?,?,?,?,?,?,?) ";
            ps = connection.prepareStatement(query);
            ps.setString(1, bean.getMsgTitle());
            ps.setString(2, bean.getMsgBody());
            ps.setString(3, bean.getReceiverType());
            ps.setString(4, bean.getReceiverId());
            ps.setString(5, bean.getResponsePersonId());
            ps.setString(6, bean.getType());
            ps.setString(7, bean.getResponseDate());
            ps.setString(8, bean.getRequestId());
            ps.setString(9, bean.getStatus());
            ps.setString(10, bean.getSelfType());
            ps.setString(11, bean.getPersonName());
        
            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return bean;
    }
    public ArrayList getAllNotification(String MANAGER_ID,
                                         String emp,
                                        String managerOfManager
                                        ) {



        ArrayList<WorkFlowNotificationBean> notificationList =
            new ArrayList<WorkFlowNotificationBean>();

        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query = "SELECT\n" +
                    "    *\n" +
                    "FROM\n" +
                    "    (\n" +
                    "        SELECT\n" +
                    "            nt.*,\n" +
                    "            ss.person_number\n" +
                    "        FROM\n" +
                    "            xx_workflow_notification nt,\n" +
                    "            xx_approval_list ss\n" +
                    "        WHERE\n" +
                    "            nt.status = 'OPEN'\n" +
                    "            AND ss.service_type = nt.self_type\n" +
                    "            AND ss.transaction_id = nt.request_id\n" +
                    "            AND ( ( nt.receiver_type = 'LINE_MANAGER'\n" +
                    "                    AND nt.receiver_id = ?  )\n" +
                    "                  OR ( nt.receiver_type = 'EMP'\n" +
                    "                       AND nt.receiver_id = ? )\n" +
                    "                  OR ( nt.receiver_type = 'PROJECT_MANAGER'\n" +
                    "                       AND nt.receiver_id = ?  )\n" +
                    "                   )\n" +
                    "        UNION\n" +
                    "        SELECT\n" +
                    "            nt.*,\n" +
                    "            ss.person_number\n" +
                    "        FROM\n" +
                    "            xx_workflow_notification nt,\n" +
                    "            xx_approval_list ss\n" +
                    "        WHERE\n" +
                    "            ss.service_type = nt.self_type\n" +
                    "            AND ss.transaction_id = nt.request_id\n" +
                    "            AND ( nt.type = 'FYI'\n" +
                    "                  OR ( nt.type = 'FYA'\n" +
                    "                       AND nt.status = 'OPEN' ) )\n" +
                    "            AND ( ( nt.receiver_type = 'LINE_MANAGER'\n" +
                    "                    AND nt.receiver_id = ?  )\n" +
                    "                  OR ( nt.receiver_type = 'EMP'\n" +
                    "                       AND nt.receiver_id =? )\n" +
                    "                  OR ( nt.receiver_type = 'PROJECT_MANAGER'\n" +
                    "                       AND nt.receiver_id = ? )\n" +
                    "                )\n" +
                    "    )\n" +
                    "ORDER BY\n" +
                    "    id DESC";

            ps = connection.prepareStatement(query);
            ps.setString(1, MANAGER_ID);
            ps.setString(2, emp);
            ps.setString(3,  managerOfManager);

            
            ps.setString(4, MANAGER_ID);
            ps.setString(5, emp);
            ps.setString(6,  managerOfManager);
   

            rs = ps.executeQuery();

            while (rs.next()) {
                WorkFlowNotificationBean obj = new WorkFlowNotificationBean();
                obj.setId(rs.getString("ID"));
                obj.setMsgTitle(rs.getString("MSG_TITLE"));
                obj.setMsgBody(rs.getString("MSG_BODY"));
                obj.setCreationDate(rs.getString("CREATION_DATE"));
                obj.setReceiverType(rs.getString("RECEIVER_TYPE"));
                obj.setReceiverId(rs.getString("RECEIVER_ID"));
                obj.setResponsePersonId(rs.getString("RESPONSE_PERSON_ID"));
                obj.setType(rs.getString("TYPE"));
                obj.setResponseDate(rs.getString("RESPONSE_DATE"));
                obj.setRequestId(rs.getString("REQUEST_ID"));
                obj.setStatus(rs.getString("STATUS"));
                obj.setPersonNumber(rs.getString("PERSON_NUMBER"));
                obj.setSelfType(rs.getString("SELF_TYPE"));
                obj.setPersonName(rs.getString("PERSON_NAME"));         
                notificationList.add(obj);
            }
          
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

        return notificationList;
    }
    
    public void updatelocal( String TRS_ID) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE XX_WORKFLOW_NOTIFICATION \n" +
                    "    SET STATUS = 'CLOSED'" +
                    "     WHERE REQUEST_ID = ?";
                
            ps = connection.prepareStatement(query);
          
            ps.setString(1, TRS_ID);
            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    public void updateWorkflownotification(String PERSON_ID, String STATUS,
                                           String TRS_ID, String ssType) {
        try {
            connection = AppsproConnection.getConnection();
            String query = null;
            query =
                    "UPDATE XX_WORKFLOW_NOTIFICATION \n" +
                    "    SET\n" +
                    "        RESPONSE_PERSON_ID = ?,\n" +
                    "        RESPONSE_DATE = SYSDATE,\n" +
                    "        STATUS = ?\n" +
                    "     WHERE\n" +
                    "         REQUEST_ID = ?\n" +
                    "         AND SELF_TYPE = ?\n"+
                             "AND RECEIVER_ID = ?\n";
            ps = connection.prepareStatement(query);
            ps.setString(1, PERSON_ID);
            ps.setString(2, STATUS);
            ps.setString(3, TRS_ID);
            ps.setString(4, ssType);
            ps.setString(5, PERSON_ID);
            //(" query = " + query);

            ps.executeUpdate();
        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }
    }
    public void workflowAction(String action) {
        JSONObject jsonObj = new JSONObject(action);
        String v_check = "YES";
        boolean v_check2 = true;
        String V_STEP_LEVEL;
        ApprovalListBean approvalListBean = new ApprovalListBean();
        ApprovalListDAO approvalDao = new ApprovalListDAO();
        
        try {
            V_STEP_LEVEL =
                    approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"));
          
            approvalListBean =
                    approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                V_STEP_LEVEL,
                                                jsonObj.getString("ssType"));

            approvalDao.updateApprovalList(jsonObj.getString("RESPONSE_CODE"),
                                           jsonObj.getString("TRS_ID"),
                                           V_STEP_LEVEL,
                                           jsonObj.getString("ssType"));
            this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                            "CLOSED",
                                            jsonObj.getString("TRS_ID"),
                                            jsonObj.getString("ssType"));
//            if (jsonObj.getString("RESPONSE_CODE").equals("REJECTED")) {
//                selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
//                                                 jsonObj.getString("ssType"),
//                                                 jsonObj.getString("TRS_ID"));
//               extraInformation.updateStatus("REJECTED",
//                                    Integer.parseInt(jsonObj.getString("TRS_ID")) );
//            }
            V_STEP_LEVEL =
                    approvalDao.getMinStepLeval(jsonObj.getString("TRS_ID"),
                                                jsonObj.getString("ssType"));
            
            approvalListBean =
                    approvalDao.getNextApproval(jsonObj.getString("TRS_ID"),
                                                V_STEP_LEVEL,
                                                jsonObj.getString("ssType"));

//            if (approvalListBean.getId() == null) {
//                //("Inside Null ");
//                selfServiceDao.updateSelfService(jsonObj.getString("RESPONSE_CODE"),
//                                                 jsonObj.getString("ssType"),
//                                                 jsonObj.getString("TRS_ID"));
//                v_check = "NO";
//            }
            if (!v_check.equalsIgnoreCase("NO")) {
                if (jsonObj.getString("RESPONSE_CODE").equals("APPROVED")) {
                    if (approvalListBean.getNotificationType().equals("FYI")) {
                        while (v_check2) {
                            if (approvalListBean.getNotificationType().equals("FYI")) {
                                WorkFlowNotificationBean wfnBean =
                                    new WorkFlowNotificationBean();
                                wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                wfnBean.setReceiverType(approvalListBean.getRolrType());
                                wfnBean.setReceiverId(approvalListBean.getRoleId());
                                wfnBean.setType(approvalListBean.getNotificationType());
                                wfnBean.setRequestId(approvalListBean.getTransActionId());
                                wfnBean.setStatus("OPEN");
                                wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                wfnBean.setSelfType(approvalListBean.getServiceType());

                                this.insertIntoWorkflow(wfnBean);
                                approvalDao.updateReqeustDate(approvalListBean.getId());
                                approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                            approvalListBean.getServiceType());
                                approvalDao.updateApprovalList("Delivered",
                                                               approvalListBean.getTransActionId(),
                                                               approvalListBean.getStepLeval(),
                                                               approvalListBean.getServiceType());
                                this.updateWorkflownotification(jsonObj.getString("PERSON_ID"),
                                                                "CLOSED",
                                                                jsonObj.getString("TRS_ID"),
                                                                jsonObj.getString("ssType"));
                                V_STEP_LEVEL =
                                        approvalDao.getMinStepLeval(approvalListBean.getTransActionId(),
                                                                    approvalListBean.getServiceType());
                                approvalListBean =
                                        approvalDao.getNextApproval(approvalListBean.getTransActionId(),
                                                                    V_STEP_LEVEL,
                                                                    approvalListBean.getServiceType());

                                if (approvalListBean.getId() == null) {
                                    v_check2 = false;
                                }


                            } else {
                                v_check2 = false;
                                WorkFlowNotificationBean wfnBean =
                                    new WorkFlowNotificationBean();
                                wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                                wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                                wfnBean.setReceiverType(approvalListBean.getRolrType());
                                wfnBean.setReceiverId(approvalListBean.getRoleId());
                                wfnBean.setType(approvalListBean.getNotificationType());
                                wfnBean.setRequestId(approvalListBean.getTransActionId());
                                wfnBean.setStatus("OPEN");
                                wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                                wfnBean.setSelfType(approvalListBean.getServiceType());
                                this.insertIntoWorkflow(wfnBean);
                              
                                approvalDao.updateReqeustDate(approvalListBean.getId());
                            }
                        }
                    } else {
                        WorkFlowNotificationBean wfnBean =
                            new WorkFlowNotificationBean();
                        wfnBean.setMsgTitle(jsonObj.getString("MSG_TITLE"));
                        wfnBean.setMsgBody(jsonObj.getString("MSG_BODY"));
                        wfnBean.setReceiverType(approvalListBean.getRolrType());
                        wfnBean.setReceiverId(approvalListBean.getRoleId());
                        wfnBean.setType(approvalListBean.getNotificationType());
                        wfnBean.setRequestId(approvalListBean.getTransActionId());
                        wfnBean.setStatus("OPEN");
                        wfnBean.setPersonName(jsonObj.get("PERSON_NAME").toString());
                        wfnBean.setSelfType(approvalListBean.getServiceType());
                        this.insertIntoWorkflow(wfnBean);
                   
                        
                        
                        approvalDao.updateReqeustDate(approvalListBean.getId());

                    }
                }
            }

        } catch (Exception e) {
            //("Error: ");
            e.printStackTrace();
        } finally {
            closeResources(connection, ps, rs);
        }

    }
}
