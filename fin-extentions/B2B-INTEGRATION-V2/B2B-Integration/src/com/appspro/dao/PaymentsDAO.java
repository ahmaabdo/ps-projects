package com.appspro.dao;

import com.appspro.bean.ApprovalListBean;
import com.appspro.bean.PaymentApprovalBean;
import com.appspro.bean.PaymentStatusBean;
import com.appspro.bean.WorkFlowNotificationBean;
import com.appspro.db.AppsproConnection;

import com.appspro.restHelper.RestHelper;

import java.security.KeyManagementException;
import java.security.KeyStoreException;

import java.security.NoSuchAlgorithmException;

import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class PaymentsDAO extends AppsproConnection {
    Connection connection;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    ApprovalListDAO ald = new ApprovalListDAO();
    ApprovalListBean al = new ApprovalListBean();
    ApprovalListBean al2 = new ApprovalListBean();
    ApprovalListBean al3 = new ApprovalListBean();
    WorkFlowNotificationBean wf = new WorkFlowNotificationBean();
    WorkFlowNotificationDao wfd = new WorkFlowNotificationDao();
    public void insertPayment(JSONObject payload){
            SimpleDateFormat formatter =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try {
                connection = getConnection();
                
                
                int request_id = 0;


                String sql =
                    "Select NVL(MAX(ID),0)+1 as request_id from B2B_PAYMENTS";
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    request_id = rs.getInt("request_id");
                }
                System.out.println(request_id);
                
                String query = "INSERT INTO B2B_PAYMENTS ( PAYMENT_ID , PAYMENT_DATE, PAYMENT_STATUS, PAYMENT_TYPE , SUPPLIER_NUMBER, PERSON_NAME ,CREATED_BY , CREATED_DATE , APPROVAL_STATUS,PAYMENT_OPERATION,ID,PAYMENT_CODE,TO_ACCOUNT,PAYMENT_NUMBER,BANK_NAME,BANK_CODE) VALUES ( ?,?,?,?, ? ,? ,? , ? , ? , ? , ?, ?, ?,?,?,?)";
                ps = connection.prepareStatement(query);
                ps.setString(1, payload.get("paymentId").toString());
                ps.setString(2, payload.get("paymentDate").toString());
                ps.setString(3, payload.get("paymentStatus").toString());
                ps.setString(4, payload.get("paymentType").toString());
                ps.setString(5, payload.get("supplierNumber").toString());
                ps.setString(6, payload.get("personName").toString());
                ps.setString(7, payload.get("createdBy").toString());
                ps.setString(8, formatter.format(new Date()).toString());
                ps.setString(9, payload.get("approvalStatus").toString());
                ps.setString(10, payload.get("paymentOperation").toString());
                ps.setString(11, String.valueOf(request_id));
                ps.setString(12, payload.get("paymentCode").toString());
                ps.setString(13, payload.get("toAccount").toString());
                
                ps.setString(14, payload.get("paymentNumber").toString());
                ps.setString(15, payload.get("bankName").toString());
                ps.setString(16, payload.get("bankCode").toString());
                //formatter.format(new Date()).toString()
                ps.executeUpdate();
                
                System.out.println(payload.get("paymentAmount").toString());
            if (Double.parseDouble(payload.get("paymentAmount").toString()) <=
                2000) {
                al.setStepLeval("0");
                al.setServiceType("XX_PAYMENT_REQUEST");
                al.setTransActionId(String.valueOf(request_id));
                al.setRolrType("EMP");
                al.setRoleId(payload.get("personName").toString());
                al.setResponseCode("SUBMIT");
                al.setNotificationType("FYI");
                al.setResponseDate(payload.get("createdBy").toString());
                al.setPersonName(payload.get("personName").toString());
                ald.insertIntoApprovalList(al);

                al2.setStepLeval("1");
                al2.setServiceType("XX_PAYMENT_REQUEST");
                al2.setTransActionId(String.valueOf(request_id));
                al2.setRolrType("LINE_MANAGER");
                al2.setRoleId("ibrahim.hassan@appspro-me.com");
                al2.setNotificationType("FYA");
                al2.setPersonName(payload.get("personName").toString());
                ald.insertIntoApprovalList(al2);

                al3.setStepLeval("2");
                al3.setServiceType("XX_PAYMENT_REQUEST");
                al3.setTransActionId(String.valueOf(request_id));
                al3.setRolrType("PROJECT_MANAGER");
                al3.setRoleId("appspro.fin");
                al3.setNotificationType("FYA");
                al3.setPersonName(payload.get("personName").toString());
                ald.insertIntoApprovalList(al3);

                wf.setMsgTitle("Payment Request");
                wf.setMsgBody("Payment Request / " +
                              payload.get("personName").toString());
                wf.setReceiverType(al2.getRolrType());
                wf.setReceiverId(al2.getRoleId());
                wf.setType("FYA");
                wf.setRequestId(String.valueOf(request_id));
                wf.setStatus("OPEN");
                wf.setSelfType("XX_PAYMENT_REQUEST");
                wf.setPersonName(payload.get("personName").toString());
                wfd.insertIntoWorkflow(wf);
            }else{
                
                    al.setStepLeval("0");
                    al.setServiceType("XX_PAYMENT_REQUEST");
                    al.setTransActionId(String.valueOf(request_id));
                    al.setRolrType("EMP");
                    al.setRoleId(payload.get("personName").toString());
                    al.setResponseCode("SUBMIT");
                    al.setNotificationType("FYI");
                    al.setResponseDate(payload.get("createdBy").toString());
                    al.setPersonName(payload.get("personName").toString());
                    ald.insertIntoApprovalList(al);
                    
                    al2.setStepLeval("1");
                    al2.setServiceType("XX_PAYMENT_REQUEST");
                    al2.setTransActionId(String.valueOf(request_id));
                    al2.setRolrType("LINE_MANAGER");
                    al2.setRoleId("khadeejah.mahrous@tbc.sa");
                    al2.setNotificationType("FYA");
                    al2.setPersonName(payload.get("personName").toString());
                    ald.insertIntoApprovalList(al2);
                    
                    al3.setStepLeval("2");
                    al3.setServiceType("XX_PAYMENT_REQUEST");
                    al3.setTransActionId(String.valueOf(request_id));
                    al3.setRolrType("PROJECT_MANAGER");
                    al3.setRoleId("amro.kalthom@tbc.sa");
                    al3.setNotificationType("FYA");
                    al3.setPersonName(payload.get("personName").toString());
                    ald.insertIntoApprovalList(al3);
                    
                    wf.setMsgTitle("Payment Request");
                    wf.setMsgBody("Payment Request / " +payload.get("personName").toString());
                    wf.setReceiverType(al2.getRolrType());
                    wf.setReceiverId(al2.getRoleId());
                    wf.setType("FYA");
                    wf.setRequestId(String.valueOf(request_id));
                    wf.setStatus("OPEN");
                    wf.setSelfType("XX_PAYMENT_REQUEST");
                    wf.setPersonName(payload.get("personName").toString());
                    wfd.insertIntoWorkflow(wf);
                }
               
                
                
            } catch (SQLException e) {
                e.printStackTrace();
                String message = e.getMessage();
                System.out.println(message);
            } finally {
                closeResources(connection, ps, rs);
            }
        }
    public JSONObject getPaymentById(int paymentId) {
            JSONObject obj = new JSONObject();
            String lastID = null;
            try {
                connection = getConnection();
                String query =
                    "select  * from B2B_PAYMENTS where ID = ?";
                ps = connection.prepareStatement(query);
                ps.setInt(1, paymentId);
                rs = ps.executeQuery();
                while (rs.next()) {
                    obj.put("paymentId", rs.getString("PAYMENT_ID"));
                    obj.put("paymentDate", rs.getString("PAYMENT_DATE"));
                    obj.put("paymentStatus", rs.getString("PAYMENT_STATUS"));
                    obj.put("paymentType", rs.getString("PAYMENT_TYPE"));
                    obj.put("supplierNumber", rs.getString("SUPPLIER_NUMBER"));
                    obj.put("personName", rs.getString("PERSON_NAME"));
                    obj.put("createdBy", rs.getString("CREATED_BY"));
                    obj.put("createdDate", rs.getString("CREATED_DATE"));
                    obj.put("approvalStatus", rs.getString("APPROVAL_STATUS"));
                    obj.put("id", rs.getString("ID"));
                    obj.put("paymentOperation", rs.getString("PAYMENT_OPERATION"));
                    obj.put("paymentCode", rs.getString("PAYMENT_CODE"));
                    obj.put("toAccount", rs.getString("TO_ACCOUNT"));
                    
                    obj.put("paymentNumber", rs.getString("PAYMENT_NUMBER"));
                    obj.put("bankName", rs.getString("BANK_NAME"));
                    obj.put("bankCode", rs.getString("BANK_CODE"));
                }
                return obj;
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            } finally {
                closeResources(connection, ps, rs);
            }
        }

        public JSONArray getpostPaymentsrequests() throws KeyStoreException,
                                                            NoSuchAlgorithmException,
                                                            CertificateException,
                                                            UnrecoverableKeyException,
                                                            KeyManagementException {
              JSONArray list = new JSONArray();
              try {
                  connection = getConnection();
                  String query =
                 "SELECT PAYMENT_ID,FILE_REFERENCE,TRANSACTION_REFERENCE,EVENT_REQUEST,VALUE_DATE,TRANSACTION_TYPE,TRANSACTION_STATUS\n" + 
                 "                FROM event_logger EL  \n" + 
                 "                INNER JOIN event_log_details ELD  \n" + 
                 "                ON EL.id  = ELD.transaction_id and ELD.status ='success' AND ELD.EVENT_NAME ='Create Bank Transaction' And \n" + 
                 "                EL.FILE_REFERENCE IS NOT NULL and  TRANSACTION_REFERENCE IS NOT NULL AND ( EL.TRANSACTION_TYPE ='CANCELPAYMENT' OR EL.TRANSACTION_TYPE ='MT101')";
                  ps = connection.prepareStatement(query);
                  rs = ps.executeQuery();
                  while (rs.next()) {
                      JSONObject obj = new JSONObject();
                      obj.put("paymentId", rs.getString("PAYMENT_ID"));
                      JSONObject json = RestHelper.callHttpGet("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?onlyData=true&fields=PaymentNumber,Payee,SupplierNumber&q=PaymentId="+rs.getString("PAYMENT_ID"));
                      String responseCode = json.get("responseCode").toString();
                      System.out.println(responseCode);
                      if("200".equals(responseCode)){
                              JSONObject items =
                                  new JSONObject(json.get("responseData").toString());
                              JSONArray itemsArray = items.getJSONArray("items");
                             
                              for (int i = 0; i < itemsArray.length(); i++) {
                                  JSONObject paymentJsonObj = itemsArray.getJSONObject(i);
                                  System.out.println(paymentJsonObj.get("PaymentNumber").toString());
                                  obj.put("paymentNumber", paymentJsonObj.get("PaymentNumber").toString());
                                  obj.put("supplierNumber", "null".equals(paymentJsonObj.get("SupplierNumber").toString()) ? "" : paymentJsonObj.get("SupplierNumber").toString());
                                  obj.put("supplierName", paymentJsonObj.get("Payee").toString());
                              }
                          }
                      obj.put("fileReference", rs.getString("FILE_REFERENCE"));
                      obj.put("transactionReference",
                              rs.getString("TRANSACTION_REFERENCE"));
                      obj.put("valueDate",
                              rs.getString("VALUE_DATE"));
                      obj.put("transactionStatus",
                              rs.getString("TRANSACTION_STATUS"));
                      list.put(obj);
                  }
                  System.out.println(list);
                  return list;
              } catch (SQLException e) {
                  e.printStackTrace();
                  return null;
              } finally {
                  closeResources(connection, ps, rs);
              }
          }
        public List<PaymentStatusBean> getPaymentPaymentStatus() {
                List<PaymentStatusBean> paymentStatusList = new ArrayList<>();
                JSONArray arr = new JSONArray();
                String lastID = null;
                try {
                    connection = getConnection();
                    String query =
                        "select  * from EVENT_LOGGER where TRANSACTION_TYPE ='MT101'";
                    ps = connection.prepareStatement(query);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        JSONObject obj = new JSONObject();
                        obj.put("paymentId", rs.getString("PAYMENT_ID"));
                        obj.put("SEQUENCE_ID", rs.getString("SEQUENCE_ID"));
                        obj.put("TRANSACTION_STATUS",
                                rs.getString("TRANSACTION_STATUS") == null ? "" :
                                rs.getString("TRANSACTION_STATUS"));
                        paymentStatusList.add(new PaymentStatusBean(rs.getString("PAYMENT_ID"),
                                                                    rs.getString("SEQUENCE_ID"),
                                                                    rs.getString("TRANSACTION_STATUS") ==
                                                                    null ? "" :
                                                                    rs.getString("TRANSACTION_STATUS")));
                        arr.put(obj);
                    }
                    return paymentStatusList;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                } finally {
                    closeResources(connection, ps, rs);
                }
            }

    public List<PaymentStatusBean> getvalidPayments() {
        JSONArray arr = new JSONArray();
        JSONArray arr2 = new JSONArray();
        List<PaymentStatusBean> paymentStatusList = new ArrayList<>();

        try {
            connection = getConnection();
            String query = "select DISTINCT PAYMENT_ID from EVENT_LOGGER";

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                JSONObject obj = new JSONObject();
                obj.put("paymentId", rs.getString("PAYMENT_ID"));
                arr.put(obj);
                System.out.println(arr.toString());
            }

            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                String paymentId = obj.get("paymentId").toString();
                System.out.println(obj);
                String sql =
                    "select * from EVENT_LOGGER where SEQUENCE_ID = (\n" +
                    "select max(SEQUENCE_ID) from EVENT_LOGGER \n" +
                    "where  TRANSACTION_TYPE= 'MT101'  OR  TRANSACTION_TYPE= 'CANCELPAYMENT' and PAYMENT_ID = ?)";
                ps = connection.prepareStatement(sql);
                ps.setString(1, paymentId);
                rs = ps.executeQuery();
                while (rs.next()) {
                   
                    paymentStatusList.add(new PaymentStatusBean(rs.getString("PAYMENT_ID"),
                                                                rs.getString("SEQUENCE_ID"),
                                                                rs.getString("TRANSACTION_STATUS") ==
                                                                null ? "" :
                                                                rs.getString("TRANSACTION_STATUS")));             
                }
                

            }
            System.out.println(arr.toString());
            return paymentStatusList;


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
        public List<PaymentApprovalBean> getApprovalPayments() {
            JSONArray arr = new JSONArray();
            JSONArray arr2 = new JSONArray();
            List<PaymentApprovalBean> paymentList = new ArrayList<>();

            try {
                connection = getConnection();
                String query = "SELECT  DISTINCT PAYMENT_ID FROM B2B_PAYMENTS";

                ps = connection.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    JSONObject obj = new JSONObject();
                    obj.put("paymentId", rs.getString("PAYMENT_ID"));
                    arr.put(obj);
                }
System.out.println(arr.toString());
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    String paymentId = obj.get("paymentId").toString();
                    System.out.println(obj);
                    String sql =
                        "select PAYMENT_ID,APPROVAL_STATUS , ID from B2B_PAYMENTS where ID = ( \n" + 
                        "                    select max(ID) from B2B_PAYMENTS \n" + 
                        "                    where   PAYMENT_ID =? )";
                    ps = connection.prepareStatement(sql);
                    ps.setString(1, paymentId);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                       
                        paymentList.add(new PaymentApprovalBean(rs.getString("PAYMENT_ID"),
                                                                    rs.getString("APPROVAL_STATUS") , rs.getInt("ID")));
                            
                    }
                    

                }
                System.out.println(arr.toString());
                return paymentList;


            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }

        }
        public void updateApprovalRequestStatus(String id , String status){
                          try {
                              connection = getConnection();
                              String query
                                  = "UPDATE B2B_PAYMENTS \n"
                                  + "set APPROVAL_STATUS=?\n"
                                  + "WHERE ID = ? ";
                              ps = connection.prepareStatement(query);
                              ps.setString(1, status); 
                              ps.setString(2, id);
                              ps.executeUpdate();
                          } catch (SQLException e) {
                              String message = e.getMessage();
                              System.out.println(message);
                          } finally {
                              closeResources(connection, ps, rs);
                          }
                      }
  
    }