/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.schedule;

import com.appspro.B2B.CreateB2BFile;
import java.util.Date;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Husam Al-Masri
 */
public class QuartzJob implements Job {

    public static boolean FIRST_TIME = true;

    @Override
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        CreateB2BFile createB2BFile = new CreateB2BFile();
        System.out.println("here" + new Date());
        createB2BFile.CreateAndWriteToFile();
        System.out.println("END" + new Date());

    }

    public static boolean runJobs() {

        CreateB2BFile createB2BFile = new CreateB2BFile();
        System.out.println("RUN JOB OF lOT!!!!!!!");
        System.out.println("here" + new Date());
        createB2BFile.CreateAndWriteToFile();
        System.out.println("END" + new Date());
        return true;
    }

    public static void main(String args[]) {
        CreateB2BFile createB2BFile = new CreateB2BFile();
        createB2BFile.CreateAndWriteToFile();
    }
}
