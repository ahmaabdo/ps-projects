/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.bean;

import java.util.Date;

/**
 *
 * @author HP
 */
public class MTFusionPaymentBean {

    private String paymentNumber;
    private long transactionReferenceNumber;
    private String paymentDate;
    private String disbursementBankAccountNumber;
    private String disbursementBankAccountName;
    private String paymentAmount;
    private String paymentType;
    private String paymentCurrency;
    private String supplierNumber;
    private String invoiceNumber;
    private int transactionType;

    public MTFusionPaymentBean(String paymentNumber, String paymentDate, String disbursementBankAccountNumber,
            String disbursementBankAccountName, String paymentAmount, String paymentType, String paymentCurrency,
            String supplierNumber, String invoiceNumber, int transactionType) {
        this.paymentNumber = paymentNumber;
        this.paymentDate = paymentDate;
        this.disbursementBankAccountNumber = disbursementBankAccountNumber;
        this.disbursementBankAccountName = disbursementBankAccountName;
        this.paymentAmount = paymentAmount;
        this.paymentType = paymentType;
        this.paymentCurrency = paymentCurrency;
        this.transactionReferenceNumber = new Date().getTime();
        this.supplierNumber = supplierNumber;
        this.invoiceNumber = invoiceNumber;
        this.transactionType = transactionType;
    }

    public MTFusionPaymentBean(String disbursementBankAccountNumber, int transactionType) {
        this.disbursementBankAccountNumber = disbursementBankAccountNumber;
        this.transactionType = transactionType;
    }
    
    

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public String getDisbursementBankAccountNumber() {
        return disbursementBankAccountNumber;
    }

    public String getDisbursementBankAccountName() {
        return disbursementBankAccountName;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    @Override
    public String toString() {

        if (transactionType == 101) {
            return "<TransactionData>:20:" + paymentNumber + "\n"
                    + ":21R:" + transactionReferenceNumber + "\n"
                    //                + ":28D:1/1\n"
                    + ":30:" + paymentDate + "\n"
                    + ":50H:" + disbursementBankAccountNumber + "\n"
                    + "" + disbursementBankAccountName + "\n"
                    //                + "iNVOICE 2545776HJG45\n"
                    //                + "Riyadh Murabba -KSA\n"
                    + ":21:" + paymentNumber + "\n"
                    + ":32B:" + paymentCurrency + paymentAmount + "\n"
                    + ":52A:SABBSARI\n"
                    + ":57A:SABBSARI\n"
                    + ":59:/" + "SA3345000000001028612002" + "\n"
                    + "" + supplierNumber + "\n"
                    + "Almarai Invoice \n"
                    + "Riyadh Murabba\n"
                    + ":70:01\n"//code will mapped based on paymentType
                    //                + "PAndiyan Mahalingam\n"
                    + "" + invoiceNumber + "\n"
                    + "Riyadh Murabba-KSA4564\n"
                    + ":71A:SHA</TransactionData>\n";
        } else {
            return "<Account>"+disbursementBankAccountNumber+"</Account>\n";
                    
        }
    }

}
