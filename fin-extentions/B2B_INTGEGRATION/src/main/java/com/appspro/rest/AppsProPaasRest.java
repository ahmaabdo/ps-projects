/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.rest;

import com.appspro.B2B.AccountBalanceProcess;
import com.appspro.B2B.CancelPaymentTransactionProcess;
import com.appspro.B2B.MT101FusionPaymentProcess;
import com.appspro.B2B.MT940FusionPaymentProcess;
import com.appspro.B2B.MoiFusionInquiryProcess;
import com.appspro.B2B.MoiFusionPaymentProcess;
import com.appspro.B2B.MoiRefundFusionPaymentProcess;
import com.appspro.B2B.PayrollFusionPaymentProcess;
import com.appspro.B2B.PostPaymentTransactionProcess;
import com.appspro.B2B.SadadFusionInquiryProcess;
import com.appspro.B2B.SadadFusionPaymentProcess;
import com.appspro.restHelper.RestHelper;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Husam Al-Masri
 */
@Path("/paymentprocess")
public class AppsProPaasRest {

    @GET
    @Path("/GetAllPayment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPaymentForSadad() {
        try {
            String json = RestHelper.callGetRest("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?q=PaymentDate=2020-04-12;PaymentStatus=Cleared&expand=relatedInvoices");
            return json;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @GET
    @Path("/mt101")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response MT101FusionPaymentProcessRest() {

        try {

            MT101FusionPaymentProcess mT101FusionPaymentProcess = new MT101FusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2&expand=relatedInvoices");

            mT101FusionPaymentProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/mt940")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response MT940FusionPaymentProcessRest() {

        try {

            MT940FusionPaymentProcess mT940FusionPaymentProcess = new MT940FusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");

            mT940FusionPaymentProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/sadadpayment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sadadFusionPaymentProcessRest() {

        try {

            SadadFusionPaymentProcess sadadFusionPaymentProcess = new SadadFusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500");

            sadadFusionPaymentProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/sadadinquiry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sadadFusionInquiryProcessRest() {

        try {

            SadadFusionInquiryProcess sadadFusionInquiryProcess = new SadadFusionInquiryProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500");

            sadadFusionInquiryProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/paymenttransaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postPaymentTransactionProcessRest() {

        try {

            PostPaymentTransactionProcess postPaymentTransactionProcess = new PostPaymentTransactionProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");

            postPaymentTransactionProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/payrollpayment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response payrollFusionPaymentProcessRest() {

        try {

            PayrollFusionPaymentProcess payrollFusionPaymentProcess = new PayrollFusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");

            payrollFusionPaymentProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/refund")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response moiRefundFusionPaymentProcessRest() {

        try {

            MoiRefundFusionPaymentProcess moiRefundFusionPaymentProcess = new MoiRefundFusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500");

            moiRefundFusionPaymentProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/moipayment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response moiFusionPaymentProcessRest() {

        try {

            MoiFusionPaymentProcess moiFusionPaymentProcess = new MoiFusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500");

            moiFusionPaymentProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/moiinquiry")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response moiFusionInquiryProcessRest() {

        try {

            MoiFusionInquiryProcess moiFusionInquiryProcess = new MoiFusionInquiryProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500");

            moiFusionInquiryProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/cancelpayment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelPaymentTransactionProcessRest() {

        try {

            CancelPaymentTransactionProcess cancelPaymentTransactionProcess = new CancelPaymentTransactionProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");

            cancelPaymentTransactionProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

    @GET
    @Path("/accountbalance")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response accountBalanceProcessRest() {

        try {

            AccountBalanceProcess accountBalanceProcess = new AccountBalanceProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");

            accountBalanceProcess.createFile();

            //then call com.appspro.B2B.PostFileToBank top post the transaction to the bank
            return Response.ok().type(MediaType.APPLICATION_JSON).entity("{\n"
                    + "\"process status\" : \"done\"\n"
                    + "}").build();

        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\n"
                    + "     \"errorCode\": \"500\",\n"
                    + "     \"errerMessege\": \"internal server error\"\n"
                    + "     \"errorDescription\": \"internal server error\"\n"
                    + "}").build();
        }

    }

}
