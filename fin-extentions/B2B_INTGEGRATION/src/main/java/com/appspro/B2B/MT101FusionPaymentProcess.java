/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.MTFusionPaymentBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author HP
 */
public class MT101FusionPaymentProcess extends MTFusionPaymentProcess {

    private String paymentHeader;
    private String companyCode;
    private long sequenceNumber;
    private StringBuilder paymentBody;
    private List<MTFusionPaymentBean> fusionPaymentList;

    public MT101FusionPaymentProcess(String fusionPaymentURL) {
        super(fusionPaymentURL);
        this.paymentHeader = null;
        this.companyCode = "TBCTest";
        this.sequenceNumber = System.currentTimeMillis();
        this.paymentBody = new StringBuilder();
        this.fusionPaymentList = new ArrayList<>();
    }

    @Override
    public void buildTransactionBody() {
        int count = Integer.parseInt(getJsonObj().get("count").toString());
        JSONArray items = getJsonObj().getJSONArray("items");
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject paymentJsonObj = items.getJSONObject(i);

                JSONArray relatedInvoices = new JSONArray(paymentJsonObj.get("relatedInvoices").toString());

                for (int j = 0; j < relatedInvoices.length(); j++) {

                    JSONObject relatedInvoicesJsonObj = relatedInvoices.getJSONObject(j);

                    System.out.println(relatedInvoicesJsonObj.get("InvoiceNumber"));
                    MTFusionPaymentBean fusionPaymentBean = new MTFusionPaymentBean(paymentJsonObj.get("PaymentNumber").toString(),
                            paymentJsonObj.get("PaymentDate").toString(),
                            paymentJsonObj.get("DisbursementBankAccountNumber").toString(),
                            paymentJsonObj.get("DisbursementBankAccountName").toString(),
                            paymentJsonObj.get("PaymentAmount").toString(),
                            paymentJsonObj.get("PaymentType").toString(),
                            paymentJsonObj.get("PaymentCurrency").toString(),
                            paymentJsonObj.get("SupplierNumber").toString(),
                            relatedInvoicesJsonObj.get("InvoiceNumber").toString(),
                            101);

                    this.fusionPaymentList.add(fusionPaymentBean);

                }

            }

        }
    }

    @Override
    public void buildTransactionHeader() {
        this.paymentHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<PaymentMessage>\n"
                + "<PaymentTransaction>\n"
                + "<CompanyCode>" + companyCode + "</CompanyCode>\n"
                + "<SequenceNum>" + sequenceNumber + "</SequenceNum>\n";

        this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildPaymentFooter() {

        this.paymentBody.append("</PaymentTransaction>\n"
                + "</PaymentMessage>");

    }

    @Override
    public StringBuilder buildFullRequest() {

        buildTransactionHeader();
        buildTransactionBody();

        for (int i = 0; i < this.fusionPaymentList.size(); i++) {

            this.paymentBody.append(this.fusionPaymentList.get(i).toString());
        }

        buildPaymentFooter();

        return this.paymentBody;

    }

    public static void main(String[] args) {

        MT101FusionPaymentProcess mT101FusionPaymentProcess = new MT101FusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2&expand=relatedInvoices");
        try {
            mT101FusionPaymentProcess.createFile();
        } catch (IOException ex) {
            Logger.getLogger(MT101FusionPaymentProcess.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
