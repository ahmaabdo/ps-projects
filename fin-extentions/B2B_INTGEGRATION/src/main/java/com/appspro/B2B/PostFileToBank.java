/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.SadadAndMoiBean;
import com.appspro.restHelper.RestHelper;

/**
 *
 * @author Husam Al-Masri
 */
public class PostFileToBank {

    public SadadAndMoiBean postFileSadad(String payload) {
        SadadAndMoiBean sadadBean = new SadadAndMoiBean();
        SadadAndMoiBean sadadBeanTwo = new SadadAndMoiBean();
        try {
            sadadBeanTwo = RestHelper.callPostRest("https://www.b2bdev.sabb.com/B2BService/epayments/sadadTransaction", "text/xml", payload);
            sadadBean.setCodeStatus(sadadBeanTwo.getCodeStatus());
            if (!sadadBeanTwo.getCodeStatus().equals("200") && !sadadBeanTwo.getCodeStatus().equals("201")) {
                sadadBean.setStatus("false");
                sadadBean.setBodyStatus(sadadBeanTwo.getBodyStatus());
                sadadBean.setPayload(sadadBeanTwo.getPayload());

                /*
                Here will call updateDffPaymentSaas function to update status on saas
                 */
                return sadadBean;
            } else {
                sadadBean.setStatus("true");
                sadadBean.setBodyStatus(sadadBeanTwo.getBodyStatus());
                sadadBean.setPayload(sadadBeanTwo.getPayload());
                /*
                Here will call updateDffPaymentSaas function to update status on saas
                 */
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sadadBean;
    }

    public void updateDffPaymentSaas(Long checkId, String status) {
        SadadAndMoiBean sadadBean = new SadadAndMoiBean();
        String payload = "{\n" + "\"receiverConfirmation\":" + '"' + status + '"' + "\n" + "}";
        String postDff = RestHelper.callPostRest("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments/" + checkId + "/child/paymentDff", "application/xml", payload, "d2FsZWVkLmVsa2FzaGVmQGFwcHNwcm8tbWUuY29tOkFwcHNwcm9AMTIzNDU2");
    }

}
