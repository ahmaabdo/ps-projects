/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.MTFusionPaymentBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author HP
 */
public class MT940FusionPaymentProcess extends MTFusionPaymentProcess {

    private String bankCode;
    private String paymentHeader;
    private StringBuilder paymentBody;

    public MT940FusionPaymentProcess(String fusionPaymentURL) {
        super(fusionPaymentURL);
        this.bankCode = "SABBSARI";
        this.paymentBody = new StringBuilder();      
    }

    @Override
    public void buildTransactionHeader() {

      this.paymentHeader=  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<PaymentInfoMessage>\n"
                + "<PaymentInfoRequest>\n"
                + "<CompanyCode>" + "Tes" + "</CompanyCode>\n"
                + "<BankCode>" + bankCode + "</BankCode>\n"
                + "<Date>" + GetDataAndTimeForBody() + "</Date>\n"
                + "<TransactionType>940</TransactionType>\n"
                + "<AccountNumbers>\n";
      
      this.paymentBody.append(this.paymentHeader);

    }

    @Override
    public void  buildTransactionBody() {
        
//        this.paymentBody.append("<AccountNumbers>");

        List<MTFusionPaymentBean> fusionPaymentList = new ArrayList<>();

        int count = Integer.parseInt(getJsonObj().get("count").toString());
        JSONArray items = getJsonObj().getJSONArray("items");
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject paymentJsonObj = items.getJSONObject(i);

                MTFusionPaymentBean fusionPaymentBean = new MTFusionPaymentBean(
                        paymentJsonObj.get("DisbursementBankAccountNumber").toString(),
                        940);
                
                this.paymentBody.append(fusionPaymentBean.toString());

                fusionPaymentList.add(fusionPaymentBean);

            }
            
            
        }

    }
    
    @Override
    public void buildPaymentFooter(){

        this.paymentBody.append("</AccountNumbers>\n"
                + "</PaymentInfoRequest>\n"
                + "</PaymentInfoMessage>");

    }

    @Override
    public StringBuilder buildFullRequest() {
         buildTransactionHeader();
         buildTransactionBody();
         buildPaymentFooter();
         
         return this.paymentBody;
    }

    public static void main(String[] args) {

        MT940FusionPaymentProcess x = new MT940FusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");
        try {
            x.createFile();
        } catch (IOException ex) {
            Logger.getLogger(MT940FusionPaymentProcess.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
