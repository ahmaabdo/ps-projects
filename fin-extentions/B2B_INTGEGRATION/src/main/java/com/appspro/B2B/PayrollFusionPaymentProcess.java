/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class PayrollFusionPaymentProcess extends MTFusionPaymentProcess {

    private String paymentHeader;
    private StringBuilder paymentBody;

    public PayrollFusionPaymentProcess(String fusionPaymentURL) {
        super(fusionPaymentURL);
        paymentBody = new StringBuilder();
    }

    @Override
    public void buildTransactionBody() {
        this.paymentBody.append("<PayrollTransaction> \n"
                + "<SequenceNum>1015952240111111</SequenceNum> \n"
                + "<TransactionData>:20:1015952240111111 \n"
                + ":32A:191111SAR6000,00 \n"
                + ":50:LuLu Hyper market\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":52A:SABBSARI\n"
                + ":53B:/SA1845000000021001821938\n"
                + ":57A:SAMBSARI \n"
                + ":59:/SA2640000000001001810503 \n"
                + "Abhimanyu Chaudri\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":70:Salary credit\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":72:2229996660 \n"
                + "6000,00 \n"
                + "500,00 \n"
                + "1000,00 \n"
                + "1500,00 \n"
                + "</TransactionData> \n"
                + "</PayrollTransaction> \n"
                + "<PayrollTransaction> \n"
                + "<SequenceNum>1015952240111112</SequenceNum> \n"
                + "<TransactionData>:20:1015952240111112 \n"
                + ":32A:191029SAR6000,00\n"
                + ":50:LuLu Hyper market\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":52A:SABBSARI\n"
                + ":53B:/SA1845000000021001821938\n"
                + ":57A:AAALSARI \n"
                + ":59:/SA7550000000010235508003\n"
                + "Harish rammoorty\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":70:Salary credit\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":72:2229997770 \n"
                + "6000,00 \n"
                + "0,00 \n"
                + "1000,00 \n"
                + "1000,00\n"
                + "</TransactionData> \n"
                + "</PayrollTransaction> \n"
                + "<PayrollTransaction> \n"
                + "<SequenceNum>1015952240111113</SequenceNum> \n"
                + "<TransactionData>:20:1015952240111113 \n"
                + ":32A:191111SAR6000,00 \n"
                + ":50:LuLu Hyper market\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":52A:SABBSARI\n"
                + ":53B:/SA1845000000021001821938\n"
                + ":57A:SABBSARI\n"
                + ":59:/SA7445000000050187400150\n"
                + "Manikandan\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":70:Salary credit\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":72:2229998880 \n"
                + "6000,00 \n"
                + "0,00 \n"
                + "1000,00 \n"
                + "1000,00\n"
                + "</TransactionData> \n"
                + "</PayrollTransaction> \n"
                + "<PayrollTransaction> \n"
                + "<SequenceNum>1015952240111114</SequenceNum> \n"
                + "<TransactionData>:20:1015952240111114\n"
                + ":32A:191111SAR6000,00 \n"
                + ":50:LuLu Hyper market\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":52A:SABBSARI\n"
                + ":53B:/SA1845000000021001821938\n"
                + ":57A:SABBSARI \n"
                + ":59:/003544418001\n"
                + "Gopi krishna \n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":70:Salary credit\n"
                + "Murabba Riyadh\n"
                + "Saudi arabia\n"
                + "Payroll settlement\n"
                + ":72:2229998881\n"
                + "6000,00 \n"
                + "0,00 \n"
                + "1000,00 \n"
                + "1000,00 \n"
                + "</TransactionData> \n"
                + "</PayrollTransaction> ");
    }

    @Override
    public void buildTransactionHeader() {
        this.paymentHeader = "<?xml version=\"1.0\"?> \n"
                + "<Message> \n"
                + "<Header> \n"
                + "<Sender>ODNEW</Sender> \n"
                + "<Receiver>SABB</Receiver> \n"
                + "<WpsMessage>True</WpsMessage>\n"
                + "<MolEstablishmentID></MolEstablishmentID>\n"
                + "<MessageType>WPSPRMSG</MessageType> \n"
                + "<MessageDescription>Payroll Message -Diffvaldate</MessageDescription> \n"
                + "<TimeStamp>2019-11-10T00:00:00</TimeStamp> \n"
                + "</Header> \n"
                + "<Body> \n"
                + "<PayrollMessage> \n"
                + "<PayrollMessageRef>20191110173416</PayrollMessageRef> \n"
                + "<PayrollMessageType>MT100-Payroll</PayrollMessageType> \n"
                + "<PayrollTransactionCount>4</PayrollTransactionCount> \n"
                + "<PayrollTransactionAmount>24000.00</PayrollTransactionAmount> ";
    }

    @Override
    public void buildPaymentFooter() {
        this.paymentBody.append("</PayrollMessage> \n"
                + "</Body> \n"
                + "</Message>");
    }

    @Override
    public StringBuilder buildFullRequest() {
        buildTransactionHeader();
        buildTransactionBody();
        buildPaymentFooter();

        return this.paymentBody;

    }

    public static void main(String[] args) {

        PayrollFusionPaymentProcess payrollFusionPaymentProcess = new PayrollFusionPaymentProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");
        try {
            payrollFusionPaymentProcess.createFile();
        } catch (IOException ex) {
            Logger.getLogger(PayrollFusionPaymentProcess.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
