/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.CancelPaymentBean;
import com.appspro.bean.PostPaymentBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hp
 */
public class CancelPaymentTransactionProcess extends MTFusionPaymentProcess {

    private StringBuilder paymentBody;
    private String paymentHeader;

    public CancelPaymentTransactionProcess(String fusionPaymentURL) {
        super(fusionPaymentURL);
        this.paymentBody = new StringBuilder();
    }
    
    @Override
    public void buildTransactionHeader() {
           this.paymentHeader=  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
                + "<StopPaymentRequestMessage>\n"
                + "<StopPaymentRequest>\n"
                + "<CompanyCode>" + "Tes" + "</CompanyCode>\n";
      this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildTransactionBody() {
        List<CancelPaymentBean> fusionPaymentList = new ArrayList<>();
        int count = Integer.parseInt(getJsonObj().get("count").toString());
        JSONArray items = getJsonObj().getJSONArray("items");
        
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject paymentJsonObj = items.getJSONObject(i);
                CancelPaymentBean cancelPaymentBean = new CancelPaymentBean("2019070112865","2019070112865");

                this.paymentBody.append(cancelPaymentBean.toString());
                
                fusionPaymentList.add(cancelPaymentBean);
            }
        }
    }

    @Override
    public void buildPaymentFooter() {
        this.paymentBody.append("</StopPaymentRequestMessage>\n"
                + "</StopPaymentRequest>\n");
    }

    @Override
    public StringBuilder buildFullRequest() {
        buildTransactionHeader();
        buildTransactionBody();
        buildPaymentFooter();
         
        return this.paymentBody;    
    }
    
     public static void main(String[] args) {

        CancelPaymentTransactionProcess x = new CancelPaymentTransactionProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");
        try {
            x.createFile();
        } catch (IOException ex) {
            Logger.getLogger(CancelPaymentTransactionProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
