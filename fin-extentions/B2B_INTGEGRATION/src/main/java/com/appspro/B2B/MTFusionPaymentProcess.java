/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import static com.appspro.B2B.CreateB2BFile.GetDataAndTime;
import com.appspro.bean.MTFusionPaymentBean;
import com.appspro.restHelper.RestHelper;
import static com.appspro.B2B.CreateB2BFile.buildTransaction;
import static com.appspro.B2B.CreateB2BFile.keyStoreType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

/**
 *
 * @author HP
 */
public abstract class MTFusionPaymentProcess {

    static String keyStore = "C:\\Users\\DELL\\Desktop\\.keystore";
    static String keyPass = "123456";
    static String alias = "C:\\Users\\DELL\\Desktop\\.keystore";
    static String keyStoreType = "JKS";

    private String fusionPaymentURL;
    private JSONObject jsonObj;

    public MTFusionPaymentProcess(String fusionPaymentURL) {
        this.fusionPaymentURL = fusionPaymentURL;

    }

    public JSONObject getJsonObj() {
        return jsonObj;
    }

    public void setJsonObj(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }

    private void getFusionPayments() {

        String json = RestHelper.callGetRest(this.fusionPaymentURL);

        jsonObj = new JSONObject(json);
    }

    public abstract void buildTransactionBody();

    public abstract void buildTransactionHeader();

    public abstract void buildPaymentFooter();

    public abstract StringBuilder buildFullRequest();

    public String GetDataAndTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        System.out.println(formatter.format(date));
        String dateTime = formatter.format(date).replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        System.out.println("R_" + dateTime);
        return dateTime;
    }

    public String GetDataAndTimeForBody() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        String dateTime = formatter.format(date);//.replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        return dateTime;
    }

    public void createFile() throws IOException {
        getFusionPayments();

//        getPaymentRequest();
        StringBuilder paymentBody = buildFullRequest();
        String currentDateTime = GetDataAndTime();
        System.out.println("");

        System.out.println(paymentBody.toString());
        
        String fileName =  System.getProperty("user.home") + "\\Downloads\\"+ currentDateTime + ".txt";
        

        FileWriter myWriter = new FileWriter(fileName);
        myWriter.write(paymentBody.toString());
        myWriter.close();
//        System.out.println("Successfully wrote to the file.");
//        encode("C:\\Users\\DELL\\Desktop\\" + fileName + ".txt");
//        System.out.println(encode("C:\\Users\\DELL\\Desktop\\" + fileName + ".txt"));

    }

    public String buildTransactionSignature(String body) {
        String result = "";
        try {
            KeyStore clientStore = KeyStore.getInstance(keyStoreType);
            clientStore.load(new FileInputStream(keyStore),
                    keyPass.toCharArray());
            X509Certificate c
                    = (X509Certificate) clientStore.getCertificate(alias);
            byte[] dataToSign = body.getBytes("cp1256");
            Signature signature = Signature.getInstance("SHA1withRSA");
            System.out.println(clientStore.getCertificate(alias).getPublicKey());
            //   LOF.writeln("--------------------");
            //     LOF.writeln(clientStore.getCertificate(alias).getPublicKey().toString());
            //LOF.writeln();
//             LOF.writeln("+++++++++++++++++++++++++ 0001");
//             PrivateKey pk = (PrivateKey)clientStore.getKey(alias, keyPass.toCharArray());
//            LOF.writeln("+++++++++++++++++++++++++ 0000 2");
//            LOF.writeln("+++++++++++++++++++++++++ 0000 3"+pk);
            signature.initSign((PrivateKey) clientStore.getKey(alias,
                    keyPass.toCharArray()));
            signature.update(dataToSign);
            byte[] signedData = signature.sign();
            X500Name xName = X500Name.asX500Name(c.getIssuerX500Principal());
            BigInteger serial = c.getSerialNumber();
            AlgorithmId digestAlgorithmId
                    = new AlgorithmId(AlgorithmId.SHA_oid);
            AlgorithmId signAlgorithmId
                    = new AlgorithmId(AlgorithmId.RSAEncryption_oid);
            SignerInfo sInfo
                    = new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId,
                            signedData);
            ContentInfo cInfo
                    = new ContentInfo(ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString,
                            dataToSign));
            PKCS7 p7
                    = new PKCS7(new AlgorithmId[]{digestAlgorithmId}, cInfo,
                    new java.security.cert.X509Certificate[]{c},
                    new SignerInfo[]{sInfo});
            ByteArrayOutputStream bOut = new DerOutputStream();
            p7.encodeSignedData(bOut);
            byte[] encodedPKCS7 = bOut.toByteArray();
            result = Base64.getEncoder().encodeToString(encodedPKCS7);  // java 8
            // result = new String(Base64.encode(encodedPKCS7));
//            result = DatatypeConverter.printBase64Binary(encodedPKCS7);  //  java 6
        } catch (Exception e) {
            //e.printStackTrace();
            result = e.toString();
        }
        return "<Signature>\n" + "<SignatureValue>\n" + result + "\n</SignatureValue>\n" + "</Signature>\n";
    }

    public String encode(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
        } catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return base64File;
    }

    public static void main(String[] args) {

//        MTFusionPaymentProcess fpp = new MTFusionPaymentProcess("https://eoay-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2",101);
//
//        try {
//            fpp.createFile();
//            
//        } catch (IOException ex) {
//            Logger.getLogger(MTFusionPaymentProcess.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

}
