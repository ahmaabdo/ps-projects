/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.MTFusionPaymentBean;
import com.appspro.bean.SadadAndMoiBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Husam Al-Masri
 */
public class SadadFusionInquiryProcess extends MTFusionPaymentProcess {

    private String paymentHeader;
    private StringBuilder paymentBody;
    private String paymentSignature;
    private String sender;
    private String accountNumber;
    private String messageType;
    private String messageDescription;
    private StringBuilder paymentRequest;
    private List<MTFusionPaymentBean> fusionPaymentList;
    private JSONObject payment;

    public SadadFusionInquiryProcess(String fusionPaymentURL) {
        super(fusionPaymentURL);
        this.paymentHeader = null;
        this.paymentBody = new StringBuilder();
        this.paymentSignature = null;
        this.sender = "TAWAL";
        this.accountNumber = "123456789";
        this.messageType = "SADADENQ";
        this.messageDescription = "SADAD Enqury";
        this.paymentRequest = new StringBuilder();
        this.fusionPaymentList = new ArrayList<>();
        this.payment = getJsonObj();
    }

    @Override
    public void buildTransactionHeader() {
        SadadAndMoiBean sadadAndMoiBean = new SadadAndMoiBean();
        this.paymentHeader = "<Message>\n"
                + "<Header>\n"
                + "<Sender>" + sender + "</Sender>\n"
                + "<AccountNumber>" + accountNumber + "</AccountNumber>\n"
                + "<MessageType>" + messageType + "</MessageType>\n"
                + "<FileRef>" + GetDataAndTime().replaceAll("-", "") + "00" + "</FileRef>\n"
                + "<MessageDescription>" + messageDescription + "</MessageDescription>\n"
                + "<TimeStamp>" + GetDataAndTimeForBody() + "</TimeStamp>\n"
                + "</Header>\n"
                + "<Body>\n";
        this.paymentRequest.append(this.paymentHeader);
    }

    @Override
    public void buildTransactionBody() {
        try {
            int count = Integer.parseInt(getJsonObj().get("count").toString());
            JSONArray items = getJsonObj().getJSONArray("items");
            if (count >= 1) {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject rec = items.getJSONObject(i);
//                    System.out.println(rec.get("CheckId"));
                    JSONArray paymentDff = rec.getJSONArray("paymentDff");
                    for (int j = 0; j < paymentDff.length(); j++) {
                        JSONObject paymentDffObj = paymentDff.getJSONObject(j);
                        Long CheckId = paymentDffObj.getLong("CheckId");
                        String typeOfPayment = paymentDffObj.optString("__FLEX_Context_DisplayValue");
                        if (typeOfPayment.equalsIgnoreCase("SADAD ")) {
                            String latterReceiverStatues = paymentDffObj.optString("latterReceiverStatues");
                            if ("".equals(latterReceiverStatues)) {
                                String sadadNumber = paymentDffObj.optString("sadadNumber");
                                System.out.println(sadadNumber);
                                String receiverConfirmation = paymentDffObj.optString("receiverConfirmation");
                                SadadAndMoiBean sadadAndMoiBean = new SadadAndMoiBean(paymentDffObj.optString("sadadNumber"), "test", "test", paymentDffObj.optLong("CheckId"), "test", "test", "sadad inquiry");
//                            System.out.println(sadadNumber);
                                sadadAndMoiBean.toString();
                                this.paymentBody.append(sadadAndMoiBean.toString());
                            }
                        }
                    }
                }
            } else {
                System.out.println("There is no payment");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void buildPaymentFooter() {
        this.paymentRequest.append("</Body>\n</Message>");
    }

    @Override
    public StringBuilder buildFullRequest() {
        buildTransactionHeader();
        buildTransactionBody();
        this.paymentRequest.append(this.paymentBody);
        paymentRequest.append(buildTransactionSignature("<body>" + this.paymentBody.toString() + "</body>"));
        buildPaymentFooter();

        return this.paymentRequest;

    }

    public static void main(String[] args) {
        SadadFusionInquiryProcess sadadFusionInquiryProcess = new SadadFusionInquiryProcess("https://ejvm-test.fa.em2.oraclecloud.com/fscmRestApi/resources/11.13.18.05/payablesPayments?expand=paymentDff&onlyData=true&limit=500");
        try {
            //        sadadFusionPaymentProcess.buildTransactionBody();
            sadadFusionInquiryProcess.createFile();
        } catch (IOException ex) {
            Logger.getLogger(SadadFusionPaymentProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
