/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.B2B;

import com.appspro.bean.PostPaymentBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hp
 */
public class PostPaymentTransactionProcess extends MTFusionPaymentProcess{
    
    private String bankCode;
    private String paymentHeader;
    private StringBuilder paymentBody;

    public PostPaymentTransactionProcess(String fusionPaymentURL) {
        super(fusionPaymentURL);
        this.bankCode = "SABBSARI";
        this.paymentBody = new StringBuilder();
    }
    
    @Override
    public void buildTransactionHeader() {
        this.paymentHeader = "<PaymentMessage>\n"
                + "<CompanyCode>"+"testCompanyCode"+"</CompanyCode>\n"
                + "<PaymentStatusRequest>\n";
        this.paymentBody.append(this.paymentHeader);
    }

    @Override
    public void buildTransactionBody() {
       List<PostPaymentBean> fusionPaymentList = new ArrayList<>();
        int count = Integer.parseInt(getJsonObj().get("count").toString());
        JSONArray items = getJsonObj().getJSONArray("items");
        
        if (count >= 1) {
            for (int i = 0; i < items.length(); ++i) {
                JSONObject paymentJsonObj = items.getJSONObject(i);
                PostPaymentBean postPaymentBean = new PostPaymentBean("2019070112865","2019070112865");

                this.paymentBody.append(postPaymentBean.toString());

                fusionPaymentList.add(postPaymentBean);
            }
        }
    }

    @Override
    public void buildPaymentFooter() {
                this.paymentBody.append("</PaymentStatusRequest>\n"
                + "</PaymentMessage>\n");
    }

    @Override
    public StringBuilder buildFullRequest() {
        
         buildTransactionHeader();
         buildTransactionBody();
         buildPaymentFooter();
         
         return this.paymentBody;
    }
    
     public static void main(String[] args) {

        PostPaymentTransactionProcess x = new PostPaymentTransactionProcess("https://ejvm-test.fa.em2.oraclecloud.com:443/fscmRestApi/resources/11.13.18.05/payablesPayments?limit=2");
        try {
            x.createFile();
        } catch (IOException ex) {
            Logger.getLogger(PostPaymentTransactionProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
