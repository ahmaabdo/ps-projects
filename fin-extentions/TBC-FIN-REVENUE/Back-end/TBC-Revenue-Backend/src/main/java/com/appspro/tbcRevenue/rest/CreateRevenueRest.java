package com.appspro.tbcRevenue.rest;

import com.appspro.tbcRevenue.dao.CreateRevenueDao;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;

import org.json.JSONObject;


@Path("/projectBillingEvent")
public class CreateRevenueRest {

    @POST
    @Path("/createEvent")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String createEvent(String eventDetails) {

        CreateRevenueDao dao = new CreateRevenueDao();
        JSONObject obj = new JSONObject(eventDetails);
        System.out.println(obj);
        JSONObject responseObj = new JSONObject();

        responseObj = dao.createProjectBillingEvent(obj);
        
        return responseObj.toString();
    }
    
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String updateEventDFF(String eventDetailsDff) {

        CreateRevenueDao dao = new CreateRevenueDao();
        JSONObject obj = new JSONObject(eventDetailsDff);
        JSONObject responseObj = new JSONObject();

        responseObj = dao.updateEventDff(obj);
        
        return responseObj.toString();
    }

}
