/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appspro.helper;


import com.appspro.db.CommonConfigReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;

/**
 *
 * @author Anas Alghawi
 */
public class Helper {

    public static String saasURL = "https://ejdg-test.fa.em2.oraclecloud.com:443/";
    //    public static String paasURL = "http://10.10.10.142:8060/ords/bsbc_ws/PInv/";
    public static String paasURL =
        "https://bsbcdb-a527636.db.us2.oraclecloudapps.com/apex/ProducRegis/";

    public static String username =  CommonConfigReader.getValue("USER_NAME");
    public static String password = CommonConfigReader.getValue("PASSWORD");


    public static String callPostRest(String serverUrl, String body) {
        try {
            System.out.println(body);

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            //            URL url = new URL(serverUrl);
            HttpURLConnection conn = null;
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                Helper.trustAllHosts();
                HttpsURLConnection https =
                    (javax.net.ssl.HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                https.setHostnameVerifier(Helper.DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            //            if (autString != null) {
            //                conn.addRequestProperty("Authorization", "Basic " + autString);
            //            }

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.close();

            int responseCode = conn.getResponseCode();
            BufferedReader br =
                new BufferedReader(new InputStreamReader((conn.getInputStream())));

            StringBuilder output = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }

            conn.disconnect();
            System.out.println(output);
            System.out.println(responseCode);
            if (responseCode == 200) {
                return output.toString();
            } else if (responseCode == 500) {
                return "{\"ReturnStatus\":\"INERTNAL SERVER ERROR\"}";
            } else if (responseCode == 201) {
                JSONObject reponseJson = new JSONObject(output.toString());
                return "{\"ReturnStatus\":\"" +
                    reponseJson.optString("ReturnStatus") +
                    "\",\"ErrorCode\":\"" +
                    reponseJson.optString("ErrorCode") +
                    "\",\"ErrorExplanation\":\"" +
                    reponseJson.optString("ErrorCode") + "\"}";
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public static String callPostRestSaaS(String serverUrl, String body) {
        try {
            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            //                    URL url = new URL(serverUrl);
            HttpURLConnection conn = null;
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                Helper.trustAllHosts();
                HttpsURLConnection https =
                    (javax.net.ssl.HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                https.setHostnameVerifier(Helper.DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization",
                                    "Basic U0NNX0lNUEw6QlNCQ0AxMjM=");
            //        conn.setRequestProperty("Authorization", "Basic QXBwc3Byby5maW5hbmNlOkJTQkNAMTIzNA==");
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            //            if (autString != null) {
            //                conn.addRequestProperty("Authorization", "Basic " + autString);
            //            }

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.close();

            int responseCode = conn.getResponseCode();
            BufferedReader br =
                new BufferedReader(new InputStreamReader((conn.getInputStream())));

            StringBuilder output = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }

            conn.disconnect();
            System.out.println(output);
            System.out.println(responseCode);
            if (responseCode == 200 || responseCode == 201) {
                return output.toString();
            } else {
                return "{\"ReturnStatus\":\"INERTNAL SERVER ERROR\"}";
            }


        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    public static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts =
            new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] { };
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final static HostnameVerifier DO_NOT_VERIFY =
        new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    public static String callGetRest(String serverUrl) {
        try {

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            //             URL url = new URL(serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            //conn.setRequestProperty("id", id);
            //            conn.addRequestProperty("Authorization", "Basic U0NNLkFETUlOOkJTQkNAMTIz");
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

    //    public static void main(String ar[]) {
    //
    //            String url = saasURL+"fscmRestApi/resources/11.13.18.05/receivablesInvoices/1082/child/receivablesInvoiceDFF";
    //            String bean = "{\"piNumber\": \"123123\"}";
    //            String returnValue = callPostRestSaaS(url, bean);
    //
    //            System.out.println(returnValue);
    //        }


    public static String callPutRest(String serverUrl, String body) {
        try {
            System.out.println(body);

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            //            URL url = new URL(serverUrl);
            HttpURLConnection conn = null;
            java.net.HttpURLConnection http;
            if (url.getProtocol().toLowerCase().equals("https")) {
                Helper.trustAllHosts();
                HttpsURLConnection https =
                    (javax.net.ssl.HttpsURLConnection)url.openConnection();
                System.setProperty("DUseSunHttpHandler", "true");
                https.setHostnameVerifier(Helper.DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            //connection.addRequestProperty("Authorization","Bearer " + jwttoken);
            //            if (autString != null) {
            //                conn.addRequestProperty("Authorization", "Basic " + autString);
            //            }

            OutputStream os = conn.getOutputStream();
            os.write(body.getBytes("UTF-8"));
            os.close();

            int responseCode = conn.getResponseCode();
            BufferedReader br =
                new BufferedReader(new InputStreamReader((conn.getInputStream())));

            StringBuilder output = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                output.append(inputLine);
            }

            conn.disconnect();
            System.out.println(output);
            System.out.println(responseCode);
            if (responseCode == 200) {
                return output.toString();
            } else if (responseCode == 500) {
                return "{\"ReturnStatus\":\"INERTNAL SERVER ERROR\"}";
            } else if (responseCode == 201) {
                JSONObject reponseJson = new JSONObject(output.toString());
                return "{\"ReturnStatus\":\"" +
                    reponseJson.optString("ReturnStatus") +
                    "\",\"ErrorCode\":\"" +
                    reponseJson.optString("ErrorCode") +
                    "\",\"ErrorExplanation\":\"" +
                    reponseJson.optString("ErrorCode") + "\"}";
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }
    
    
    
    public static String callDeleteRest(String serverUrl, String id) {
        try {

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            //             URL url = new URL(serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("id", id);
            //            conn.addRequestProperty("Authorization", "Basic U0NNLkFETUlOOkJTQkNAMTIz");
            //            conn.addRequestProperty("REST-Framework-Version","2");
            //            conn.setRequestProperty("id", id);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }
    
    
    
    public static String callGetRestWithParms(String serverUrl,String registerNumber, String supplierName,
                                              String siteRegistrationNumber, String companyName, 
                                              String productName) {
        try {

            URL url =
                new URL(null, serverUrl, new sun.net.www.protocol.https.Handler());
            //             URL url = new URL(serverUrl);
            HttpsURLConnection https = null;
            HttpURLConnection conn = null;
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                https = (HttpsURLConnection)url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                conn = https;
            } else {
                conn = (HttpURLConnection)url.openConnection();
            }

            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            //conn.setRequestProperty("id", id);
            //            conn.addRequestProperty("Authorization", "Basic U0NNLkFETUlOOkJTQkNAMTIz");
            //            conn.addRequestProperty("REST-Framework-Version","2");
                        conn.setRequestProperty("registerNumber", registerNumber);
            conn.setRequestProperty("supplierName", supplierName);
            conn.setRequestProperty("siteRegistrationNumber", siteRegistrationNumber);
            conn.setRequestProperty("companyName", companyName);
            conn.setRequestProperty("productName", productName);

            BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

}
