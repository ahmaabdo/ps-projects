package com.appspro.tbcRevenue.dao;

import biPReports.RestHelper;

import org.json.JSONObject;

import org.w3c.dom.Document;

public class CreateRevenueDao extends RestHelper {

    RestHelper rs = new RestHelper();


    public JSONObject createProjectBillingEvent(JSONObject eventDetails) {
        JSONObject resObj = new JSONObject();

        try {
            String destUrl = getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/projectBillingEvents";
            JSONObject res = rs.callRest(destUrl, "POST", eventDetails.toString());
          
            resObj.put("Status", "OK");
            resObj.put("Data", res);

        } catch (Exception e) {
            e.printStackTrace();
            resObj.put("Status", "ERROR");
            resObj.put("Data", e.getMessage());
        }

        return resObj;
    }
    
    public JSONObject updateEventDff(JSONObject eventDetailsDff) {
        JSONObject DFFObj = new JSONObject();
        JSONObject resObj = new JSONObject();
//        String ob = eventDetails.get("data").toString();

        try {
            String EventId = eventDetailsDff.get("eventId").toString();
            String destUrl = getInstanceUrl() + "/fscmRestApi/resources/11.13.18.05/projectBillingEvents";


            DFFObj.put("receiptNumber", eventDetailsDff.get("receiptNumber").toString());
            DFFObj.putOpt("eventStatus", "Created");

            String DffUrl = destUrl + "/" + EventId + "/child/billingEventDFF";

            JSONObject dffres = rs.callRest(DffUrl, "POST", DFFObj.toString());

            resObj.put("Status", "OK");
            resObj.put("Data", dffres);

        } catch (Exception e) {
            e.printStackTrace();
            resObj.put("Status", "ERROR");
            resObj.put("Data", e.getMessage());
        }

        return resObj;
    }
    
    

}
