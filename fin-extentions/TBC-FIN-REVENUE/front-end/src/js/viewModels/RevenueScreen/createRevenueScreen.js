define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'config/services', 'util/commonhelper', 'ojs/ojdatacollection-utils', 'ojs/ojselectcombobox', 'ojs/ojarraydataprovider',
    'ojs/ojknockout', 'ojs/ojlabelvalue', 'ojs/ojtable', 'ojs/ojdatetimepicker', 'ojs/ojarraytabledatasource', 'ojs/ojpagingtabledatasource', 'ojs/ojpagingcontrol', 'ojs/ojcheckboxset'
], function(oj, ko, $, app, services, commonhelper) {
    function createRetentionContentViewModel() {
        var self = this;
        self.label = ko.observable();

        self.customsModel = {
            bussinesUnitVal: ko.observable(),
            projectNameVal: ko.observable(),
            contractNumberVal: ko.observable(),
            taskNameVal: ko.observable(),
            contractLineVal: ko.observable(),
            amountVal: ko.observable(),
            projectNumber: ko.observable(),
            taskNumber: ko.observable(),
            completionDateVal: ko.observable(),
            receiptNumber: ko.observable(),
            rowSelected: ko.observable(),
            lgScreenLbl: ko.observable(),
            contractNumberVal: ko.observable(),
            revenueDateVal: ko.observable(),
            bussinesUnitVal: ko.observable(),
            receiptNumVal: ko.observable(),
            projectNumVal: ko.observable(),
            contractNumberVal2: ko.observable(),
            taskNumberVal: ko.observable(),
            rowSelected: ko.observable(),
            eventPercentageVal: ko.observable(12),

            contractsArr: ko.observableArray([]),
            columnArray: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),
            bussinesUnitArr: ko.observableArray([]),
            contractNumberArr: ko.observableArray([]),
            receiptNumArr: ko.observableArray([]),
            projectNumArr: ko.observableArray([]),
            contractLineArr: ko.observableArray([]),
            rowSelectedArr: ko.observableArray([]),

            resetDis: ko.observable(false),
            isDisabled: ko.observable(true),
            createEventDis: ko.observable(true),
            searchDis: ko.observable(true),
            createEventDailDis: ko.observable(false),
            contractDis: ko.observable(false),
            modifyEventDailDis: ko.observable(false),
            eventPercentageDis: ko.observable(false),
        };

        self.getIndexInSelectedItems = ko.observableArray();
        self.selectedItems = ko.observableArray();
        self.dataproviderRevenue = ko.observable();
        self.dataproviderRevenue(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.contractsArr, { idAttribute: 'srNo' })));

        ko.computed(function() {
            if (app.contractsArr().length > 0) {

                console.log(app.contractsArr())
                self.customsModel.searchDis(false);

                var BU_NAME = [...new Set(app.contractsArr().map(el => el.BU_NAME))];
                var CONTRACT_NUMBER = [...new Set(app.contractsArr().map(el => el.CONTRACT_NUMBER))];
                var RECEIPT_NUM = [...new Set(app.contractsArr().map(el => el.RECEIPT_NUM))];
                var PROJECT_NUMBER = [...new Set(app.contractsArr().map(el => el.PROJECT_NUMBER))];

                self.customsModel.bussinesUnitArr(BU_NAME.map(el => ({ label: el, value: el })));
                self.customsModel.contractNumberArr(CONTRACT_NUMBER.map(el => ({ label: el, value: el })));
                self.customsModel.receiptNumArr(RECEIPT_NUM.map(el => ({ label: el, value: el })));
                self.customsModel.projectNumArr(PROJECT_NUMBER.map(el => ({ label: el, value: el })));
            }
        })

        $("body").delegate("#table .oj-table-body-row", "click", (e) => {
            var index = $(e.currentTarget).find(".hiddenkeyHolder").val();
            self.customsModel.rowSelected = self.customsModel.contractsArr()[index];
            self.customsModel.bussinesUnitVal(self.customsModel.rowSelected.BU_NAME)
            self.customsModel.projectNameVal(self.customsModel.rowSelected.PROJECT_NAME)
            self.customsModel.contractNumberVal(self.customsModel.rowSelected.CONTRACT_NUMBER)
            self.customsModel.contractNumberVal2(self.customsModel.rowSelected.CONTRACT_NUMBER)
            self.customsModel.taskNumberVal(self.customsModel.rowSelected.TASK_NUMBER)
            self.customsModel.amountVal(self.customsModel.rowSelected.REVENUE_AMOUNT)
            self.customsModel.completionDateVal(self.customsModel.rowSelected.REVENUE_DATE);
            self.customsModel.projectNumber(self.customsModel.rowSelected.PROJECT_NUMBER);
            self.customsModel.taskNameVal(self.customsModel.rowSelected.TASK_NAME);
            self.customsModel.receiptNumber(self.customsModel.rowSelected.RECEIPT_NUM);
            self.customsModel.projectNumVal(self.customsModel.rowSelected.PROJECT_NUMBER);
            self.customsModel.receiptNumVal(self.customsModel.rowSelected.RECEIPT_NUM);

            if (e.target.type == 'button') {
                if (Array.isArray(self.customsModel.rowSelected.G_2)) {
                    self.customsModel.contractDis(false);
                    var x = self.customsModel.rowSelected.G_2.map(el => ({ label: el.LINE_NUMBER, value: el.LINE_NUMBER }))
                    self.customsModel.contractLineArr(x)
                } else {
                    self.customsModel.contractDis(true);
                    var x = [self.customsModel.rowSelected.G_2].map(el => ({ label: el.LINE_NUMBER, value: el.LINE_NUMBER }))
                    self.customsModel.contractLineArr(x)
                    self.customsModel.contractLineVal(self.customsModel.rowSelected.G_2.LINE_NUMBER)
                }

                document.querySelector("#revenueDialog").open();
                if (self.customsModel.rowSelected.EVENT_STATUS && self.customsModel.rowSelected.EVENT_STATUS == "Created") {
                    self.customsModel.createEventDailDis(true);
                } else {
                    self.customsModel.createEventDailDis(false);
                }
            } else if (e.target.type == 'checkbox') {
                if (self.customsModel.rowSelected) {
                    if (e.target.checked) {
                        self.customsModel.rowSelectedArr().push(self.customsModel.rowSelected)
                        self.customsModel.createEventDis(false);
                    } else {
                        self.customsModel.rowSelectedArr(self.customsModel.rowSelectedArr().filter(el => el.srNo != self.customsModel.rowSelected.srNo))
                        if (self.customsModel.rowSelectedArr().length == 0) {}
                        self.customsModel.createEventDis(true);
                    }
                }
            }
            console.log(self.customsModel.rowSelectedArr())
        });

        self.searchBtn = function() {
            cond = function(el) {
                var res = (
                    (self.customsModel.bussinesUnitVal() ? el.BU_NAME == self.customsModel.bussinesUnitVal() : true) &&
                    (self.customsModel.contractNumberVal() ? el.CONTRACT_NUMBER == self.customsModel.contractNumberVal() : true) &&
                    (self.customsModel.revenueDateVal() ? el.REVENUE_DATE == self.customsModel.revenueDateVal() : true) &&
                    (self.customsModel.receiptNumVal() ? el.RECEIPT_NUM == self.customsModel.receiptNumVal() : true) &&
                    (self.customsModel.projectNumVal() ? el.PROJECT_NUMBER == self.customsModel.projectNumVal() : true)
                );
                return res;
            };
            app.contractsArr().forEach(el => el.REVENUE_AMOUNT = numberWithCommas(el.REVENUE_AMOUNT))
            var find = app.contractsArr().filter(el => cond(el));
            self.customsModel.contractsArr(app.contractsArr().filter(el => cond(el)));
            self.dataproviderRevenue(new oj.PagingTableDataSource(new oj.ArrayTableDataSource(self.customsModel.contractsArr, { idAttribute: 'srNo' })));
        }

        self.resetBtnAction = function() {
            self.customsModel.contractNumberVal('')
            self.customsModel.bussinesUnitVal('')
            self.customsModel.revenueDateVal('')
            self.customsModel.projectNumVal('')
            self.customsModel.receiptNumVal('')
            self.customsModel.contractsArr([]);
        }

        self.createEventBtn = function() {
            app.loading(true);
            document.querySelector("#revenueDialog").close();
            var payload = {
                // "SourceName": null,
                // "SourceReference": null,
                "ContractTypeName": "Revenue Contract",
                "ContractNumber": self.customsModel.contractNumberVal(),
                "ContractLineNumber": self.customsModel.contractLineVal().toString(),
                // "OrganizationName": "Construction",
                "EventTypeName": "Revenue Milestone",
                "EventDescription": "Event to recognize revenue for a specified amount based on progress made",
                "ProjectNumber": self.customsModel.projectNumber().toString(),
                "CompletionDate": self.customsModel.completionDateVal(),
                "TaskNumber": self.customsModel.taskNumberVal(),
                "BillTrnsAmount": self.customsModel.amountVal().toString().replace(/,/g, ""),
                "BillTrnsCurrencyCode": "SAR",
                "BusinessUnitName": self.customsModel.bussinesUnitVal()
            };

            var createEventsucess = function(res) {
                if (res.Status == "OK") {
                    if (res.Data.status == "done") {
                        var updateDff = {
                            "eventId": res.Data.data.EventId,
                            "receiptNumber": self.customsModel.receiptNumber().toString()
                        }
                        services.postGeneric(commonhelper.updateEventDff, updateDff).then(data => {
                            app.messagesDataProvider.push(app.createMessage('confirmation', `Event Creat Successfully`))
                            app.getAllContarcts();
                            self.customsModel.searchDis(false);
                            self.customsModel.contractsArr([])
                        }, error => {
                            app.loading(false)
                        })
                    } else {
                        app.messagesDataProvider.push(app.createMessage('error', `Error in create event,${res.Data.data}`))
                        app.loading(false)
                    }
                } else {
                    app.messagesDataProvider.push(app.createMessage('error', `Error in create event`))
                }
            }
            var createEventfail = function() {
                document.querySelector("#revenueDialog").close();
                self.customsModel.contractsArr([])
                app.loading(false)
                app.messagesDataProvider.push(app.createMessage('error', `Event Not Created`))
            }
            services.postGeneric(commonhelper.createEvent, payload).then(createEventsucess, createEventfail)
        };

        self.createEventBtnAction = function() {

        };

        self.modifyEventBtn = function() {

        }

        self.getIndexInSelectedItems = function(id) {
            for (var i = 0; i < self.selectedItems().length; i++) {
                var range = self.selectedItems()[i];
                var startIndex = range.startIndex.row;
                var endIndex = range.endIndex.row;
                if (id >= startIndex && id <= endIndex) {
                    return i;
                }
            }
            return -1;
        };

        self.handleCheckbox = function(id, srN) {
            var isChecked = self.getIndexInSelectedItems(id) != -1 || (srN ? self.customsModel.rowSelectedArr().find(e => e.srN == srN) : false);
            return isChecked ? ['checked'] : [];
        }


        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        self.createEventBtnAction = function() {
            document.querySelector("#revenueDialog").open();
        }

        self.cancelDialBtn = function() {
            document.querySelector("#revenueDialog").close();
        };

        var getTransaltion = oj.Translations.getTranslatedString;

        function initTransaltion() {
            self.label({
                bussinesUnitLbl: getTransaltion("revenueScreen.bussinesUnitLbl"),
                projectNameLbl: getTransaltion("revenueScreen.projectNameLbl"),
                contractNumberLbl: getTransaltion("revenueScreen.contractNumberLbl"),
                taskNameLbl: getTransaltion("revenueScreen.taskNameLbl"),
                contractLineLbl: getTransaltion("revenueScreen.contractLineLbl"),
                completionDateLbl: getTransaltion("revenueScreen.completionDateLbl"),
                amountLbl: getTransaltion("revenueScreen.amountLbl"),
                calculateLbl: getTransaltion("revenueScreen.calculateLbl"),
                searchLbl: getTransaltion("common.searchLbl"),
                resetLbl: getTransaltion("common.resetLbl"),
                supplierNameLbl: getTransaltion("revenueScreen.supplierNameLbl"),
                revenueAmount: getTransaltion("revenueScreen.revenueAmount"),
                PONumberLbl: getTransaltion("revenueScreen.PONumberLbl"),
                projectDescriptionLbl: getTransaltion("revenueScreen.projectDescriptionLbl"),
                revenueDate: getTransaltion("revenueScreen.revenueDate"),
                receiptNumberLbl: getTransaltion("revenueScreen.receiptNumberLbl"),
                contractNumber: getTransaltion("revenueScreen.contractNumber"),
                retentionPercLbl: getTransaltion("revenueScreen.retentionPercLbl"),
                createRevenueLbl: getTransaltion("revenueScreen.createRevenueLbl"),
                amountReleaseLbl: getTransaltion("revenueScreen.amountReleaseLbl"),
                changeLbl: getTransaltion("common.changeLbl"),
                cancelLbl: getTransaltion("common.cancelLbl"),
                creteEventBtnLbl: getTransaltion("revenueScreen.creteEventBtnLbl"),
                status: getTransaltion("revenueScreen.status"),
                revenueDateLbl: getTransaltion("revenueScreen.revenueDateLbl"),
                contractNumberLbl: getTransaltion("revenueScreen.contractNumberLbl"),
                receiptNumberLbl: getTransaltion("revenueScreen.receiptNumberLbl"),
                bussinesUnitLbl: getTransaltion("revenueScreen.bussinesUnitLbl"),
                resetBtnLbl: getTransaltion("revenueScreen.resetBtnLbl"),
                receiptNumLbl: getTransaltion("revenueScreen.receiptNumLbl"),
                projectNumLbl: getTransaltion("revenueScreen.projectNumLbl"),
                createViewBtnLbl: getTransaltion("revenueScreen.createViewBtnLbl"),
                revenueScreenLbl: getTransaltion("revenueScreen.revenueScreenLbl"),
                taskNumberLbl: getTransaltion("revenueScreen.taskNumberLbl"),
                modifyEventBtnLbl: getTransaltion("revenueScreen.modifyEventBtnLbl"),
                eventPercatgeLbl: getTransaltion("revenueScreen.eventPercatgeLbl"),
            });

            self.customsModel.columnArray([{
                    "headerText": "Select",
                    "template": 'checkTemplate'
                },
                {
                    "headerText": "NO.",
                    "template": 'seqTemplate'
                },
                {
                    "headerText": "Business Unit",
                    "field": "BU_NAME",
                },
                {
                    "headerText": self.label().receiptNumberLbl,
                    "field": "RECEIPT_NUM"
                },
                {
                    "headerText": self.label().revenueAmount,
                    "field": "REVENUE_AMOUNT"
                },
                {
                    "headerText": self.label().revenueDate,
                    "field": "REVENUE_DATE"
                },
                {
                    "headerText": self.label().projectNumLbl,
                    "field": "PROJECT_NUMBER"
                },
                {
                    "headerText": self.label().contractNumber,
                    "field": "CONTRACT_NUMBER"
                },
                {
                    "headerText": self.label().status,
                    "field": "EVENT_STATUS"
                },
                {
                    "headerText": "Action",
                    "template": 'buttonTemplate'
                }
            ]);
        }


        initTransaltion();
    }


    return createRetentionContentViewModel;
});