define([], function() {

    function commonHelper() {
        var self = this;

        self.getSaaSHost = function() {
            var host = "http://192.168.1.118:7101/EBSMobile/rest";
            return host;
        };

        self.getPaaSHost = function() {
            var host = "https://apex-hcuk.db.em2.oraclecloudapps.com/apex/xx_selfService/";
            return host;
        };

        self.getBiReportServletPath = function() {
            var host = "report/commonbireport";
            return host;
        };


        self.getAllContarcts = "report/getAllContarcts";
        self.createEvent = "projectBillingEvent/createEvent";
        self.updateEventDff = "projectBillingEvent/update";


        self.getInternalRest = function() {

            if (window.location.hostname == "digital.tbc.sa") {

                var host = "https://digital.tbc.sa/TBC-FIN-REV-BACKEND-TEST/webapi/";

            } else {

                var host = " http://127.0.0.1:7101/TBC-FIN-REV-BACKEND-TEST/webapi/";
            }


            return host;
        };

    }

    return new commonHelper();
});