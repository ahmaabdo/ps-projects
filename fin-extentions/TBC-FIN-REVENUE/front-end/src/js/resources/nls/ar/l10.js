define({

    "login": {
        "userName": "اسم المستخدم",
        "Password": "كلمه السر ",
        "loginLabel": "تسجيل الدخول ",
        "resetLabel": "Reset",
        "loginFailureText": "Invalid User Name or Password",
        "SignOut": "تسجيل الخروج",
        "Name": "Employee Name: "
    },

    "revenueScreen": {
        "bussinesUnitLbl": "وحدة عمل",
        "projectNameLbl": "اسم المشروع",
        "contractNumberLbl": "رقم العقد",
        "taskNameLbl": "اسم التاسك",
        "amountLbl": "القيمه",
        "contractLineLbl": "Contract Line",
        "completionDateLbl": "موعد الإكمال",
        "calculateLbl": "Calculate & Create Debit Memo",
        "supplierNameLbl": "اسم المورد",
        "revenueAmount": "الإيرادات المعترف بها",
        "creteEventBtnLbl": "انشاء حدث",
        "receiptNumberLbl": "رقم الايصال",
        "PONumberLbl": "PO Number",
        "projectDescriptionLbl": "وصف المشروع",
        "revenueDate": "تاريخ الإيرادات",
        "contractNumber": "رقم العقد",
        "createRevenueLbl": "إنشاء حدث الإيرادات",
        "status": "الحالة",
        "bussinesUnitLbl": "وحدة عمل",
        "receiptNumberLbl": "رقم الايصال",
        "revenueDateLbl": "تاريخ الإيرادات",
        "resetBtnLbl": "اعاده",
        "receiptNumLbl": "رقم الايصال",
        "projectNumLbl": "رقم المشروع",
        "createViewBtnLbl": "انشاء & عرض",
        "revenueScreenLbl": "إنشاء أحداث الإيرادات"

    },

    "common": {
        "inputPlaceholder": "ادخل فيمة",
        "selectPlaceholder": "اختر قيمة",
        "addLbl": "إضافه",
        "editLbl": "تعديل",
        "backLbl": "رجوع",
        "viewLbl": "عرض",
        "searchLbl": "بحث",
        "saveLbl": "حفظ و إغلاق",
        "clearLbl": "مسح",
        "deleteLbl": "حذف",
        "contractTypeLbl": "نوع العقد :",
        "noActionLbl": "لا",
        "yesLbl": "نعم",
        "confirmationTextLbl": "Confirm Dialog",
        "confirmLanguageLbl": "Are you sure to change language?"
    }

});